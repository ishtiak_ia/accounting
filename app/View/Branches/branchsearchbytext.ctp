<?php
	echo $Utilitys->paginationcommon();
					echo $Utilitys->tablestart();
						echo $this->Html->tableHeaders(
							array(
								__('SL#'), 
								array(
									__('company')  => array('class' => 'highlight 
										sortable')
								), 
								array(
									__('Name (EN)')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Name (BN)')  => array('class' => 'highlight sortable')
								),
								array(
									__('code')  => array('class' => 'highlight 
										sortable')
								), 
								array(
									__('Is Active')  => array('class' => 'highlight sortable')
								),
								__('Action')
							)
						);
						$branchrows = array();
						$countBranch = 0-1+$this->Paginator->counter('%start%');
						foreach($branch as $Thisbranch):
							$countBranch++;
							$branchrows[]= array($countBranch,$Thisbranch["Branch"]["company_name"],$Thisbranch["Branch"]["branchname"],$Thisbranch["Branch"]["branchnamebn"],$Thisbranch["Branch"]["branchcode"],$Utilitys->statusURL($this->request["controller"], 'branch', $Thisbranch["Branch"]["id"], $Thisbranch["Branch"]["branchuuid"], $Thisbranch["Branch"]["branchisactive"]),$Utilitys->cusurl($this->request['controller'], 'branch', $Thisbranch["Branch"]["id"], $Thisbranch["Branch"]["branchuuid"]));
						endforeach;
						echo $this->Html->tableCells(
							$branchrows
						);
					echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#ProductSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#productsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#productsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>