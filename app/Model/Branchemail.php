<?php

App::uses('AppModel', 'Model');

class Branchemail extends AppModel {

    public $name = 'Branchemail';
    public $usetables = 'branchemails';
    var $belongsTo = array(
	'Branch' => array(
			'fields' =>array('branchemail'),
			'className'    => 'Branch',
			'foreignKey'    => 'branch_id'
		)
	);
    
}

?>