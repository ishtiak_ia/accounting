<label for="OrderbankAccountId">Bank Account</label>
<select name="OrderbankAccountId" id="OrderbankAccountId" class="form-control">
								<option value="0">Select Bank Account</option>
					<?php if(isset($bankaccount)){
							foreach($bankaccount as $key => $value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
				</select>

<?php echo "
<script>
	$(document).ready(function() {
		$(\"#OrderbankAccountId\").change(function(){
				var BankId = $(\"#bankId\").val();
				var BankaccountId = $(this).val();
				$(\"#OrderbankAccountIdspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."banks/bankchequebookseriesbyaccount',
					data:{BankId:BankId,BankaccountId:BankaccountId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderBanktransactionchequebookseries\").html(result.bankchequebook_options);
						//$(\"#BanksPaymentrecive\").val(result.banktransactiontotalamount);
						$(\"#BanktransactionBankId\").val(result.bank_id);
						$(\"#BanktransactionBankbranchId\").val(result.bankbranch_id);
						$(\"#BanktransactionBankaccounttypeId\").val(result.bankaccounttype_id);
						$(\"#OrderbankAccountIdspan\").html('');
						if(result.banktransactiontotalamount>0){
							$(\"#accountbalancespan\").attr('class','label label-success');
						}else if(result.banktransactiontotalamount<0){
							$(\"#accountbalancespan\").attr('class','label label-danger');
						}else{
							$(\"#accountbalancespan\").attr('class','label label-info');
						}
						$(\"#accountbalancespan\").html(result.banktransactiontotalamount);
					}
				});
		});

	});	
</script>
";?>
			