<?php
 echo $this->Html->script('order/order', array('inline' => true));
?>
<?php echo $this->Form->create('Orders', array('action' => 'orderinsertupdatection', 'type' => 'file'));
echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$sales[0]['Order']['id']));
echo $this->Form->input('orderuuid', array('type'=>'hidden', 'value'=>@$sales[0]['Order']['orderuuid']));
$options = array(1 => __("Yes"), 0 =>__("No"));
$attributes = array(
	'legend' => false,
	'label' => true,
	'value' => (@$sales[0]['Order']['orderisactive'] ? $sales[0]['Order']['orderisactive'] : 0),
	'class' => 'form-inline'
);

?>
<style>
	.order_page label {
		display: block;
	}
</style>
<?php
if(empty($voucher_no[0]['voucher_no']))
	$voucher_no = "S-".date('Y-m')."-0001";
else{
	$voucher_no_array = explode("-", $voucher_no[0]['voucher_no']);
	$voucher_no_new_last_dgits = $voucher_no_array[3]+1;
	if(strlen($voucher_no_new_last_dgits)!=4){
		$add_digits = 4 - strlen($voucher_no_new_last_dgits);
		if($add_digits == 3)
			$voucher_no_new_last_dgits = '000'.$voucher_no_new_last_dgits;
		else if($add_digits == 2)
			$voucher_no_new_last_dgits = '00'.$voucher_no_new_last_dgits;
		else if($add_digits == 1)
			$voucher_no_new_last_dgits = '0'.$voucher_no_new_last_dgits;

	}
	$voucher_no = "S-".date('Y-m')."-$voucher_no_new_last_dgits";

}
if((isset($sales[0]["Order"]["id"]))){
	$orderreferenceno = @$sales[0]['Order']['orderreferenceno'];
}else {
	$orderreferenceno = $voucher_no;
}
	echo"<h3>".__("Add New Sales Products")."</h3>";
?>
	<div class="row text-center" style='overflow-x:scroll;'>

		<table id="hajji-meta-table" class="table table-condensed table-hover">

			<tbody  >
			<tr class="sales-item-row order_page" >
				<td>
					<?php
					echo $this->Form->input(
						"Order.orderdate",
						array(
							'type'=>'text',
							'placeholder'=>"",
							'label'=>__('Date'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'value' => @$sales[0]['Order']['orderdate']
						)
					);
					?>
				</td>
				<td>
					<?php
					echo $this->Form->input(
						"Order.orderreferenceno",
						array(
							'type'=>'text',
							'label'=>__('Invoice'),
							'readonly' => 'readonly',
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'value'=>$orderreferenceno
						)
					);
					?>
				</td>
				<td>
					<?php
					 echo $this->Form->input(
						"Order.clientcode",
						array(
							'type'=>'select',
							"options"=>array($customerId),
							'empty'=>__('Select'),
							'label'=>__('Customer ID'),
							'data-validation-engine'=>'validate[required]',
							"selected" => (@$sales[0]['Order']['clientcode'] ? $sales[0]['Order']['clientcode'] : 0)
						)
					);
					?>

				</td>
				<td>
					<?php
					echo $this->Form->input(
						"Order.client_id",
						array(
							'type'=>'select',
							"options"=>array($customerlist),
							'empty'=>__('Select'),
							'label'=>__('Customer Name'),
							'data-validation-engine'=>'validate[required]',
							"selected" => (@$sales[0]['Order']['client_id'] ? $sales[0]['Order']['client_id'] : 0)
						)
					);
					?>
				</td>
				<td>
					<?php
					echo $this->Form->input(
						"Order.branch_id",
						array(
							'type'=>'select',
							"options"=>array($salescenter),
							'empty'=>__('Select'),
							'label'=>__('Sales Center'),
							'onchange'=>'getItem(1,false)',
							/*'class'=> 'form-control',*/
							'data-validation-engine'=>'validate[required]',
							"selected" => (@$sales[0]['Order']['branch_id'] ? $sales[0]['Order']['branch_id'] : 0)
						)
					);
					?>
				</td>

				<td>
					<?php
					 echo $this->Form->input(
						"Order.salesmancode",
						array(
							'type'=>'select',
							"options"=>array($salesmanId),
							'empty'=>__('Select'),
							'label'=>__('Salesman ID'),
							'data-validation-engine'=>'validate[required]',
							"selected" => (@$sales[0]['Order']['salesmancode'] ? $sales[0]['Order']['salesmancode'] : 0)
						)
					);
					?>
				</td>
				<td>
					<?php
					echo $this->Form->input(
						"Order.salesman_id",
						array(
							'type'=>'select',
							"options"=>array($salesmanlist),
							'empty'=>__('Select'),
							'label'=>__('Salesman Name'),
							'data-validation-engine'=>'validate[required]',
							"selected" => (@$sales[0]['Order']['salesman_id'] ? $sales[0]['Order']['salesman_id'] : 0)
						)
					);
					?>
				</td>

			</tr>


			<!-- /.sales-item-row -->


			</tbody>
		</table>






	<!-- /.col-sm-2 -->

	</div>
	<!-- .row -->

	<div class="row">
		<div class="table-responsive">
			
			<div style='overflow-x:scroll;'>

				<table id="hajji-meta-table" class="table table-condensed table-hover"  >
				<thead>
					<tr>
						<th class="text-center">Item Code</th>
						<th class="text-center">Item Name</th>
						<th class="text-center">Quantity</th>
						<th class="text-center">Stock</th>
						<th class="text-center">Unit Price</th>
						<th class="text-center">Price</th>
						<th class="text-center">Discount Rate(%)</th>
						<th class="text-center"> Discount Amount</th>
						<th class="text-center">Total Amount</th>
					</tr>
				</thead>
				<tbody id='content' >

				<?php
					$count_orderdetails_data = count(@$orderdetails_data);
				if(isset($orderdetails_data[0]["Orderdetail"]["id"])){
					/*echo "<pre>";
					print_r($orderdetails_data);die();
					echo "<pre>";*/
				$count=0;
				foreach ($orderdetails_data as $this_orderdetails_data) {
					/*echo "<pre>";

					print_r ($this_orderdetails_data['Orderdetail']['product_id']);

					echo "<pre>";

					die();*/
					$count++;

				?>
					<tr class="sales-item-row" id='mydivcontent<?php echo $count; ?>'>
						<td>
							<?php
							 echo $this->Form->input(
								"Orderdetail.productcode{$count}",
								array(
									'type'=>'select',
									"options"=>$product_code_edit,
									//"options"=>array($productcodelist),
									'empty'=>__('Select'),
									'label'=>false,
									'data-validation-engine'=>'validate[required]',
									'onchange'=>"getProductName($count); stockCount($count)",
									"selected" => (@$this_orderdetails_data['Orderdetail']['productcode'] ? $this_orderdetails_data['Orderdetail']['productcode'] : 0)


								)
							);
							?>
						</td>
						<td>
							<?php
							echo $this->Form->input(
								"Orderdetail.product_id{$count}",
								array(
									'type'=>'select',
									"options"=>$product_list_edit,
									//"options"=>array($productlist),
									'empty'=>__('Select'),
									'data-validation-engine'=>'validate[required]',
									'onchange'=>"getProductCode($count); stockCount($count)",
									"selected" => (@$this_orderdetails_data['Orderdetail']['product_id'] ? $this_orderdetails_data['Orderdetail']['product_id'] : 0),

									'label'=>false
									)
							);
							?>
						</td>
						<td>
							<?php
							echo $this->Form->input(
								"Orderdetail.quantity{$count}",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'onkeyup'=> "priceandamountcalculation($count); stockLimitAlert($count)",
									'value' => @$this_orderdetails_data['Orderdetail']['quantity'],
									'label'=>false
								)
							);
							?>
						</td>
						<td>
							<?php
							echo $this->Form->input(
								"Orderdetail.stock{$count}",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'readonly' => 'readonly',
									'label'=>false
								)
							);
							?>
						</td>
						<td>
							<?php
							echo $this->Form->input(
								"Orderdetail.unitprice{$count}",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'onkeyup'=> "priceandamountcalculation($count)",
									'value' => @$this_orderdetails_data['Orderdetail']['unitprice'],
									'label'=>false
								)
							);
							?>
						</td>
						<td>
							<?php
							echo $this->Form->input(
								"Orderdetail.price{$count}",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'label'=>false,
									'readonly' => 'readonly',
									'value' => @$this_orderdetails_data['Orderdetail']['price']
								)
							);
							?>
						</td>
						<td>
							<?php
							echo $this->Form->input(
								"Orderdetail.discount{$count}",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'onkeyup'=> "priceandamountcalculation($count)",
									'label'=>false,
									'value' => @$this_orderdetails_data['Orderdetail']['discount'],
								)
							);
							?>
						</td>
						<td>
							<?php
							echo $this->Form->input(
								"Orderdetail.discountvalue{$count}",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'readonly' => 'readonly',
									'label'=>false,
									'value' => @$this_orderdetails_data['Orderdetail']['discountvalue']
								)
							);
							?>
						</td>
						<td>
							<?php
							echo $this->Form->input(
								"Orderdetail.transactionamount{$count}",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'readonly' => 'readonly',
									'value' => @$this_orderdetails_data['Orderdetail']['transactionamount'],
									'label'=>false
								)
							);
							?>
						</td>
					</tr>
					<?php
				}
				}else{
				?>

					<tr class="sales-item-row" id='mydivcontent1'>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.productcode1",
								array(
									'type'=>'select',
									'empty'=>__('Select'),
									'data-validation-engine'=>'validate[required]',
									'onchange'=>'getProductName(1); stockCount(1);',
									'label'=>false
								)
							);
						?>
						</td>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.product_id1",
								array(
									'type'=>'select',
									'empty'=>__('Select'),
									'data-validation-engine'=>'validate[required]',
									'onchange'=>'getProductCode(1); stockCount(1);',
									'label'=>false
								)
							);
							?>
						</td>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.quantity1",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'onkeyup'=> 'priceandamountcalculation(1); stockLimitAlert(1)',
									'label'=>false
								)
                               );
                         ?>
						</td>
						<td>
							<?php
							echo $this->Form->input(
								"Orderdetail.stock1",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'readonly' => 'readonly',
									'label'=>false
								)
							);
							?>
						</td>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.unitprice1",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'onkeyup'=> 'priceandamountcalculation(1)',
									'label'=>false
								)
							   );
						 ?>
						</td>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.price1",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'label'=>false,
									'readonly' => 'readonly',
								)
							   );
						 ?>
						</td>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.discount1",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'onkeyup'=> 'priceandamountcalculation(1)',
									'label'=>false,
								)
							   );
						 ?>
						</td>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.discountvalue1",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'readonly' => 'readonly',
									'label'=>false
								)
							   );
						 ?>
						</td>
						<td>
							<?php
							echo $this->Form->input(
								"Orderdetail.transactionamount1",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'readonly' => 'readonly',
									'label'=>false
								)
							   );
						 ?>
						</td>
					</tr>
					<?php
				}
				?>
				</tbody>
			</table>

			</div>
			<table class='table table-condensed table-hover table-responsive'>

				<tr>
					<td colspan="8">
						<input type="button" id="more_fields"  value="Add a Row" />
						<input type="button" id="anc_rem" value="Remove a Row" />
						<input type="hidden" style="width:48px;" name="totlarow" id="totlarow" value="<?php echo @$count_orderdetails_data?$count_orderdetails_data:1  ?>" />
					</td>
				</tr>


			</table>
<!--table for total amount-->
			<table class="table table-condensed table-hover table-responsive">

					<tr>
						<th class="text-right" style="width:85%">Total Price</th>
						<td>
							<?php
								echo $this->Form->input(
									"Order.ordertotal",
									array(
										'type'=>'text',
										'class'=> 'form-control',
										'readonly' => 'readonly',
										'label'=>false,
										'value' => @$sales[0]['Order']['ordertotal']

									)
								);
							?>

						</td>

					</tr>
					<tr>
						<th class="text-right" style="width:85%">Total Discount</th>
						<td>
							<?php
							echo $this->Form->input(
								"Order.orderdiscount",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'readonly' => 'readonly',
									'label'=>false,
									'value' => @$sales[0]['Order']['orderdiscount']
								)
							);
							?>
						</td>

					</tr>
					<tr>
						<th class="text-right" style="width:85%">Price After Discount</th>
						<td>
							<?php
							echo $this->Form->input(
								"Order.priceafterdiscount",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'readonly' => 'readonly',
									'label'=>false,
									'value' => @$sales[0]['Order']['priceafterdiscount']
								)
							);
							?>
						</td>

					</tr>
<!--				<tr>-->
					<!--<th class="text-right" style="width:85%"> Vat (%)</th>-->
<!--					<td>-->
						<?php
						echo $this->Form->input(
							"Order.vat",
							array(
								'type'=>'hidden',
								'class'=> 'form-control',
								'onkeyup'=>'vatCalculation()',
								'label'=>false,
								'value' => @$sales[0]['Order']['vat']
							)
						);
						?>
<!--					</td>-->
<!--				</tr>-->
<!--					<tr>-->
						<!--<th class="text-right" style="width:85%">Total Vat</th>-->
<!--						<td>-->
							<?php
							echo $this->Form->input(
								"Order.vatvalue",
								array(
									'type'=>'hidden',
									'class'=> 'form-control',
									'readonly' => 'readonly',
									'label'=>false,
									'value' => @$sales[0]['Order']['vatvalue']
								)
							);
							?>
<!--						</td>-->
<!---->
<!--					</tr>-->
					<tr>
						<th class="text-right" style="width:85%">Transportation Cost</th>
						<td>
							<?php
							echo $this->Form->input(
								"Order.transportation_cost",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'onkeyup'=>'getTransportCost()',
									'label'=>false,
									'value' => @$sales[0]['Order']['transportation_cost']
								)
							);
							?>
						</td>

					</tr>
					<tr>
						<th class="text-right" style="width:85%">Less</th>
						<td>
							<?php
							echo $this->Form->input(
								"Order.less",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'onkeyup'=>'getLess()',
									'label'=>false,
									'value' => @$sales[0]['Order']['less']
								)
							);
							?>
						</td>

					</tr>
					<tr>
						<th class="text-right" style="width:85%">Grand Total</th>
						<td>
							<?php
							echo $this->Form->input(
								"Order.transactionamount",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'readonly' => 'readonly',
									'label'=>false,
									'value' => @$sales[0]['Order']['transactionamount']
								)
							);
							?>
						</td>

					</tr>
					<tr>
						<th class="text-right" style="width:85%">Paid Amount</th>
						<td>
							<?php
							echo $this->Form->input(
								"Order.orderpayment",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'onkeyup'=>'getDue()',
									'label'=>false,
									'value' => @$sales[0]['Order']['orderpayment']
								)
							);
							?>
						</td>

					</tr>

					<tr>
						<th class="text-right" style="width:85%">Due</th>
						<td>
							<?php
							echo $this->Form->input(
								"Order.orderdue",
								array(
									'type'=>'text',
									'class'=> 'form-control',
									'readonly' => 'readonly',
									'label'=>false,
									'value' => @$sales[0]['Order']['orderdue']
								)
							);
							?>
						</td>
					</tr>
			</table>

			<table class="table table-condensed table-hover table-responsive">
				<tr>
					<th class="text-right" style="width:85%; text-align:center; ">COA Code</th>
					<th class="text-right" style="width:85%; text-align:center">COA Name</th>
				</tr>
				<tr>
					<td style="width:85%; text-align:center; ">
						<?php
						echo $this->Form->input(
							"Order.coacode",
							array(
								'type' => 'select',
								'empty'=>__('Select Coa Code'),
								'options'=>array($coa_code),
								'label'=>false,
								'tabindex'=>21,
								'style'=>'width:120px',
								'style' => '',
								'selected' => (@$sales[0]['Order']['coacode'] ? $sales[0]['Order']['coacode'] : 0)
							)
						);
						?>
					</td>
					<td style="width:85%; text-align:center; ">
						<?php
						echo $this->Form->input(
							"Order.coa_id",
							array(
								'type' => 'select',
								'empty'=>__('Select Coa Name'),
								'options'=>array($coa),
								'label'=>false,
								'tabindex'=>22,
								'style'=>'width:120px',
								'style' => '',
								'selected' => (@$sales[0]['Order']['coa_id'] ? $sales[0]['Order']['coa_id'] : 0)
							)
						);
						?>
					</td>
				</tr>
			</table>


		</div>
	</div>
	<!-- /.row -->

	<fieldset>



	<!-- /.row.check-enabled -->


		<!-- /.col-sm-6 -->
		<div class="col-sm-6">
			<div class="form-group">
				<?php
				echo $this->Form->input(
					"Order.ordercomment",
					array(
						'type' => 'textarea',
						'label'=>__('Comment'),
						'cols'=>"30",
						'rows'=>"4",
						'class'=> 'form-control',
						'value' => @$sales[0]['Order']['ordercomment']
					)
				);
				?>


			</div>




			<?php

			echo $Utilitys->allformbutton('',$this->request["controller"],$page='order',1);

			echo $this->Form->end();



			?>
<div/>
	<!-- /.row -->
	</fieldset>
</form>





<script>

	$(document).ready(function() {

		var host = "<?php echo $this->webroot; ?>";
		var room = 0;

		$("#OrdersOrderinsertupdatectionForm").validationEngine();
		$("#OrderOrderdate").datetimepicker({language: "bn",format: "yyyy-mm-dd HH:mm:ss", use24hours: true, showMeridian: false , startView: 2, minView: 2, autoclose: 1});


// Start adding search option with dropdown

		$('#OrderClientId').select2();
		$('#OrderClientcode').select2();
		$('#OrderBranchId').select2();
		$('#OrderSalesmanId').select2();
		$('#OrderSalesmancode').select2();
		/*$('#OrderdetailProductcode1' ).select2();
		$('#OrderdetailProductId1').select2();*/
		$('#OrderCoacode').select2();
		$('#OrderCoaId').select2();
		$('[id^="OrderdetailProductcode"]').select2();
		$('[id^="OrderdetailProductId"]').select2();

// End adding search option with dropdown
//----------------------------------------------------------------------------------------------------------------------
// Start variable for adding dynamic porduct list from the controller to the dynamically created Order Form input

		var productCodeList = "<?php
								if(isset($productcodelist)){
									foreach($productcodelist as $key => $value){?>"+
										'<option value="<?php echo $key ?>"><?php echo $value?></option>'+
						      "<?php
												}
											}
				 				?>";
		var productNameList = "<?php
								if(isset($productNameList)){
									foreach($productNameList as $key => $value){?>"+
										'<option value="<?php echo $key ?>"><?php echo $value?></option>'+
							  "<?php
												}
											}
				 			  ?>";

// End variable for adding dynamic porduct list from the controller to the dynamically created Order Form input
//-----------------------------------------------------------------------------------------------------------------------
//Start creating dynamically created OrderDetail Input row

		$('#more_fields').click(function(){

			room = parseInt(document.getElementById("totlarow").value);
			room = room+1;

			$("#content").append(
				'<tr id="mydivcontent'+room+'" class="order_page">' +

				'<td><div class="input select"><select name="data[Orderdetail][productcode'+room+']'+'" id="OrderdetailProductcode'+room+'" onchange="getProductName('+room+');  stockCount('+room+')" data-validation-engine="validate[required]" ><option>Select</option></select></div></td>' +
				'<td><div class="input select"><select name="data[Orderdetail][product_id'+room+']'+'" id="OrderdetailProductId'+room+'"  onchange="getProductCode('+room+');  stockCount('+room+')" data-validation-engine="validate[required]" ><option >Select</option></select></div></td>' +
				'<td><input type="text" class="form-control" name="data[Orderdetail][quantity'+room+']'+'" onkeyup="priceandamountcalculation('+room+'); stockLimitAlert('+room+')"   id="OrderdetailQuantity'+room+'"></td>' +
				'<td><input type="text" class="form-control" name="data[Orderdetail][stock'+room+']'+'"   id="OrderdetailStock'+room+'" readonly="readonly"></td>' +
				'<td><input type="text" class="form-control" name="data[Orderdetail][unitprice'+room+']'+'" onkeyup="priceandamountcalculation('+room+')"  id="OrderdetailUnitprice'+room+'"></td>' +
				'<td><input type="text" class="form-control" name="data[Orderdetail][price'+room+']'+'"   id="OrderdetailPrice'+room+'" readonly="readonly" ></td>' +
				'<td><input type="text" class="form-control" id="OrderdetailDiscount'+room+'" name="data[Orderdetail][discount'+room+']'+'" onkeyup="priceandamountcalculation('+room+')"  ></td>' +
				'<td><input type="text" class="form-control" id="OrderdetailDiscountvalue'+room+'" name="data[Orderdetail][discountvalue'+room+']'+'" readonly="readonly" ></td>' +
				'<td><input type="text" class="form-control" id="OrderdetailTransactionamount'+room+'" name="data[Orderdetail][transactionamount'+room+']'+'"  readonly="readonly" ></td>' +

				'</tr>');

			document.getElementById("totlarow").value=room;

			$('#OrderdetailProductcode'+room).select2();
			$('#OrderdetailProductId'+room).select2();

			//getItem(room) called to populate items into dynamic orderdetail inlput for onchange event in branchid
			getItem(room,true);

		});

//End creating dynamically created OrderDetail Input row
//---------------------------------------------------------------------------------------------------------------------------------------------
//Start removing dynamically created OrderDetail Input row

		$("#anc_rem").click(function(){
			if($('#content tr').size()>1){
				$('#content tr:last-child').remove();
				room = room-1;
				alert(room);
				document.getElementById("totlarow").value=room;
			}else{
				alert('One row should be present in table');
			}
		});

//End removing dynamically created OrderDetail Input row
//--------------------------------------------------------------------------------------------------------------------------------------------
//Start mutual oncahnge event between dropdown list with search option

		$('#OrderClientcode').change(function () {

			var clientId = $(this).find('option:selected').val();

				$('#OrderClientId option').each(function() {
					if($(this).val() == clientId) {

							$(this).prop("selected", true);
							$('#s2id_OrderClientId .select2-chosen' ).text( $(this).text());
					}
				});



				/*$('#OrderClientName option').prop('selected', false)
					.filter('[value="data"]')
					.prop('selected', true);*/
		})

		$('#OrderClientId').change(function () {

			var clientId = $(this).find('option:selected').val();

			$('#OrderClientcode option').each(function() {

					if($(this).val() == clientId) {

							$(this).prop("selected", true);
							$('#s2id_OrderClientcode .select2-chosen' ).text( $(this).text());
				}

			});
		})

		$('#OrderSalesmancode').change(function () {

			var clientId = $(this).find('option:selected').val();

			$('#OrderSalesmanId option').each(function() {
				if($(this).val() == clientId) {

						$(this).prop("selected", true);
						$('#s2id_OrderSalesmanId .select2-chosen' ).text( $(this).text());
				}
			});
		})

		$('#OrderSalesmanId').change(function () {

			var clientId = $(this).find('option:selected').val();

			$('#OrderSalesmancode option').each(function() {
				if($(this).val() == clientId) {

						$(this).prop("selected", true);
						$('#s2id_OrderSalesmancode .select2-chosen').text($(this).text());
				}
			});
		})

		$('#OrderCoacode').change(function () {

			var clientId = $(this).find('option:selected').val();

			$('#OrderCoaId option').each(function() {
				if($(this).val() == clientId) {

						$(this).prop("selected", true);
						$('#s2id_OrderCoaId .select2-chosen' ).text( $(this).text());
				}
			});
		})

		$('#OrderCoaId').change(function () {

			var clientId = $(this).find('option:selected').val();

			$('#OrderCoacode option').each(function() {
				if($(this).val() == clientId) {

					$(this).prop("selected", true);
					$('#s2id_OrderCoacode .select2-chosen' ).text( $(this).text());
				}
			});
		})

	})

//End mutual onchange event between dropdoen list with search option
//----------------------------------------------------------------------------------------------------------
//Start Auto Calculation for Order and Orderdetail Input

	function priceandamountcalculation(fieldid){

		var quantity = $("#OrderdetailQuantity"+fieldid).val();
		if(quantity ==''){
			quantity = 0;
		}else{
			quantity = parseFloat(quantity);
		}
		var unit_price = $("#OrderdetailUnitprice"+fieldid).val();
		if(unit_price ==''){
			unit_price = 0;
		}else{
			unit_price = parseFloat(unit_price);
		}

		$("#OrderdetailPrice"+fieldid).val(quantity * unit_price);

		var price = $("#OrderdetailPrice"+fieldid).val();

		var discount_rate = $("#OrderdetailDiscount"+fieldid).val();
		if(discount_rate =='' || discount_rate == 'NaN'){
			discount_rate = 0;
		}else{
			discount_rate = parseFloat(discount_rate);
		}
		var discount_amount = ((price*discount_rate)/100);
		var amount_value = price - discount_amount;
		$("#OrderdetailDiscountvalue"+fieldid).val(discount_amount);
		$("#OrderdetailTransactionamount"+fieldid).val(amount_value);

		var total_amount = 0;
		row = parseInt(document.getElementById("totlarow").value);
		for(i = 1; i<=row; i++){
			total_amount += parseFloat(document.getElementById("OrderdetailPrice"+i).value);
		}
		$("#OrderOrdertotal").val(total_amount);

		var total_discount_amount = 0
		for(j =1; j <= row; j++){
			total_discount_amount += parseFloat(document.getElementById("OrderdetailDiscountvalue"+j).value);
		}
		 $("#OrderOrderdiscount").val(total_discount_amount);

		var bonus_discount = $("#OrderOrderdiscount").val();
		if(bonus_discount ==''){
			bonus_discount = 0;
		}else{
			bonus_discount = parseFloat(bonus_discount);
		}
		var price_after_discount = total_amount - bonus_discount;
		$("#OrderPriceafterdiscount").val(price_after_discount);

		var priceAfterDiscount = $("#OrderPriceafterdiscount").val();



		$('#OrderVatvalue').val(vatCalculation());

		var vatTotal = $('#OrderVatvalue').val();

		var transportCost = getTransportCost();
		var less = getLess();


		$('#OrderTransactionamount').val(parseFloat(vatTotal)+parseFloat(priceAfterDiscount)+parseFloat(transportCost)-parseFloat(less));

			var payment = $("#OrderOrderpayment").val();
			if(payment == ''){
				payment = 0;
			}else{
				payment = parseFloat(payment);
			}

			var grand_total = $("#OrderTransactionamount").val();
			var due = grand_total - payment;
			$("#OrderOrderdue").val(due);
	}



	function vatCalculation () {

		var vatRate = $('#OrderVat').val();

		if(vatRate == ''){
			vatRate = 0;
		}else{
			vatRate = parseFloat(vatRate);
		}
		var priceAfterDiscount = $("#OrderPriceafterdiscount").val();
		var vatTotal = ((priceAfterDiscount*vatRate)/100);
		$('#OrderVatvalue').val(vatTotal);
		vatTotal = $('#OrderVatvalue').val()
		$('#OrderTransactionamount').val(parseFloat(vatTotal)+parseFloat(priceAfterDiscount));
		return parseFloat(vatTotal);

	}

	function getTransportCost() {

		var transportCost = $('#OrderTransportationCost').val();
		if(transportCost == ''){
			transportCost = 0;
		}else{
			transportCost = parseFloat(transportCost);
		}
		var paidAmount = $('#OrderOrderpayment').val();
		if(paidAmount == ''){
			paidAmount = 0;
		}else{
			paidAmount = parseFloat(paidAmount);
		}
		var priceAfterDiscount = $('#OrderPriceafterdiscount').val();
//		var less = $('#OrderLess').val(getLess());

		var grandTotal = parseFloat(priceAfterDiscount) + parseFloat(transportCost)-parseFloat(getLess());


		$('#OrderTransactionamount').val(grandTotal);

		$("#OrderOrderdue").val(grandTotal-paidAmount);
		return  transportCost;

	}
	function getLess() {

		var transportCost = $('#OrderTransportationCost').val();
		if(transportCost == ''){
			transportCost = 0;
		}else{
			transportCost = parseFloat(transportCost);
		}

		var less = $('#OrderLess').val();
		if(less == ''){
			less = 0;
		}else{
			less = parseFloat(less);
		}

		var paidAmount = $('#OrderOrderpayment').val();
		if(paidAmount == ''){
			paidAmount = 0;
		}else{
			paidAmount = parseFloat(paidAmount);
		}
		var priceAfterDiscount = $('#OrderPriceafterdiscount').val();

		var grandTotal = parseFloat(priceAfterDiscount) + parseFloat(transportCost) - parseFloat(less);
		$('#OrderTransactionamount').val(grandTotal);
		$("#OrderOrderdue").val(grandTotal-paidAmount);
		return less;
	}

	function getDue() {

		var grand_total = $("#OrderTransactionamount").val();
		var payment = $('#OrderOrderpayment').val();
		var due = grand_total - payment;
		$("#OrderOrderdue").val(due);

	}

//End Auto Calculation for Order and Orderdetail Input
//------------------------------------------------------------------------------------
//Start-- onchange event between dynamically created OrderDetail fields

	function getProductName(rowNumber) {

		var productId = $('#OrderdetailProductcode'+rowNumber).find('option:selected').val();

		$('#OrderdetailProductId' + rowNumber + ' option').each(function () {
			if ($(this).val() == productId) {

				$(this).prop("selected", true);
				$('#s2id_OrderdetailProductId' + rowNumber + ' .select2-chosen').text($(this).text());

			}

		});

	}

	function getProductCode(rowNumber){

		var productId = $('#OrderdetailProductId'+rowNumber).find('option:selected').val();

		$('#OrderdetailProductcode'+rowNumber+' option').each(function() {

			if($(this).val() == productId) {

					$(this).prop("selected", true);
					$('#s2id_OrderdetailProductcode'+rowNumber+' .select2-chosen' ).text( $(this).text());
			}
		});

	}

//End-- onchange event between dynamically created OrderDetail fields
//---------------------------------------------------------------------------------------
//Start onchange event between branch id and item

	function getItem(rowNumber,flag) {

		var host = "<?php echo $this->webroot; ?>";

			var brachId = $('#OrderBranchId').find('option:selected').val();


			$.ajax({


				'url': host + 'orders/getProduct/' + brachId,
				'dataType': 'json'

			}).success(function (data) {

				var itemCode = "<option value=''>Select </option>";
				var itemName = "<option value=''>Select </option>";

				$(data).each(function(index,item){

					itemCode += "<option value ="+ item.Product.id +">"+item.Productbranch.product_code+" </option>";
					itemName += "<option value ="+ item.Product.id +">"+item.Product.productname+" </option>";


				});
					var totalRow = $('#totlarow').val();

				if(flag == false){

					for(var row = 1; row<=totalRow; row++){
						$('#s2id_OrderdetailProductcode'+row+' .select2-chosen' ).text( 'Select');
						$('#s2id_OrderdetailProductId'+row+' .select2-chosen' ).text( 'Select');
						$('#OrderdetailProductcode'+row ).html(itemCode);
						$('#OrderdetailProductId'+row ).html(itemName);
						$('#OrderdetailStock'+row).val('');
					}
				}

				if(flag == true){

					$('#OrderdetailProductcode'+rowNumber ).html(itemCode);
					$('#OrderdetailProductId'+rowNumber ).html(itemName);
				}

			})

	}

//End onchange event between branch id and item
//----------------------------------------------------------------------------------------------------------------------------------------------
//Start Count Stock as per the quantity of items sold and puchased

	function stockCount(rowNumber){

		var host = "<?php echo $this->webroot; ?>";
		var branchId 	= $('#OrderBranchId').find('option:selected').val();
		var productId 	= $('#OrderdetailProductId'+rowNumber).find('option:selected').val();

		$.ajax({

			'url': host + 'orders/getStock/' + branchId +'/'+productId ,
			'dataType': 'json'

		}).success(function (stockItem) {

			$('#OrderdetailStock'+rowNumber).val(parseInt(stockItem));

		});
	}


	function stockLimitAlert (rowNumber) {

			stock = parseInt($('#OrderdetailStock'+rowNumber).val());

			inputQuantity = parseInt($('#OrderdetailQuantity'+rowNumber).val());

		if(stock != ''){

			if(inputQuantity>stock){
				alert('quantity exceeds stock');

			}
			if(inputQuantity===stock){
				alert('quantity equals stock');
			}

		}
	}



</script>



