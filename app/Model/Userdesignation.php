<?php
App::uses('AppModel', 'Model');
class Userdesignation extends AppModel {
	public $name = 'Userdesignation';
	public $usetables = 'userdesignations';

	var $belongsTo = array(
		'User' => array(
			'fields' =>array('User.*'),
			'className'    => 'User',
			'foreignKey'    => 'user_id'
		),
		'Designation' => array(
			'fields' =>array('Designation.*'),
			'className'    => 'Designation',
			'foreignKey'    => 'designation_id'
		)
	);
	var $virtualFields = array(
		'user_name' => 'CONCAT(User.username)',
		'user_fullname' => 'CONCAT(User.userfirstname, "   ", User.usermiddlename, "   ", User.userlastname)',
		'designation_name' => 'CONCAT(Designation.designationname, " / ",  Designation.designationnamebn)',
		'isActive' => 'IF(Userdesignation.userdesignationisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Userdesignation.userdesignationisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
}