<?php
$array_order=0;
$serial_no=1;
$net_change_purchase_rate=0;
$opening_balance_purchase_price = $opening_balance_purchase_rate = $opening_balance_purchase_quantity =0;
$closing_balance_price = $closing_balance_rate = $closing_balance_quantity =0;
echo "<div class='center-aligned-texts'><h1>".$company_info[0]["Company"]["companyname"]."</h1></div>";
echo "<div class='center-aligned-texts'><b>".$company_info[0]["Company"]["companypresentaddress"]."</b></div>";
echo "<br><br>";
echo "<div  class='left-aligned-texts'><b>Date From:</b>".$form_date."<b> To: </b>".$to_date."</div><br>";

echo $Utilitys->tablestart($class=null, $id=null);
echo $this->Html->tableHeaders(
    array(
        array(
            __('Sl#')  => array('class' => '')
        ),
        array(
            __('Client Code')  => array('class' => '')
        ),
        array(
            __('Client Name')  => array('class' => '')
        ),
        array(
            __('Product Code')  => array('class' => '')
        ),
        array(
            __('Product Name')  => array('class' => '')
        ),
        array(
            __('Product Type')  => array('class' => '')
        ),
        array(
            __('Product Category')  => array('class' => '')
        ),
        array(
            __('Quantity')  => array('class' => '','colspan'=>'3')
        ),
        array(
            __('Price')  => array('class' => '','colspan'=>'3')
    ),
    )
);
echo $this->Html->tableHeaders(
    array(
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        ),

        array(
            __('Sale')  => array('class' => '')
        ),
        array(
            __('Return')  => array('class' => '')
        ),
        array(
            __('Net Quantity')  => array('class' => '')
        ), array(
        __('Sale')  => array('class' => '')
    ),
        array(
            __('Back')  => array('class' => '')
        ),
        array(
            __('Net Sale')  => array('class' => '')
        )
    )
);
//print_r($net_change_sales);
foreach($product_info as $product):
    $client_name = $client_info[$array_order][0]["User"]["userfirstname"].' '.$client_info[0][0]["User"]["usermiddlename"].' '.$client_info[0][0]["User"]["userlastname"];
    //opening balance start
    $opening_balance_purchase_price = $opening_balance_purchase[$array_order][0][0]["total_opening_price"] - $opening_balance_sales[$array_order][0][0]["total_opening_price"]-$opening_balance_purchase_return[$array_order][0][0]["total_opening_price"]+$opening_balance_sales_return[$array_order][0][0]["total_opening_price"];
    $opening_balance_purchase_quantity = $opening_balance_purchase[$array_order][0][0]["total_opening_quantity"] - $opening_balance_sales[$array_order][0][0]["total_opening_quantity"]-$opening_balance_purchase_return[$array_order][0][0]["total_opening_quantity"]+$opening_balance_sales_return[$array_order][0][0]["total_opening_quantity"];
    $opening_balance_purchase_rate = $opening_balance_purchase_price/$opening_balance_purchase_quantity;
    //opening balance end

    //closing balance start
    $closing_balance_price = $opening_balance_purchase[$array_order][0][0]["total_opening_price"]+$net_change_purchase[$array_order][0][0]["total_net_change_price"]-$net_change_sales[$array_order][0][0]["total_net_change_price"]-$net_change_purchase_return[$array_order][0][0]["total_opening_price"]+$net_change_sales_return[$array_order][0][0]["total_opening_price"];
    $closing_balance_quantity = $opening_balance_purchase[$array_order][0][0]["total_opening_quantity"]+$net_change_purchase[$array_order][0][0]["total_net_change_quantity"]-$net_change_sales[$array_order][0][0]["total_net_change_quantity"]-$net_change_purchase_return[$array_order][0][0]["total_opening_quantity"]+$net_change_sales_return[$array_order][0][0]["total_opening_quantity"];
    $closing_balance_rate = $closing_balance_price/$closing_balance_quantity;
    //closing balance end

    $coatyperows[]= array(
        $serial_no,
        $product["Order"]["client_id"],
        $client_name,
        $product["Product"]["productcode"],
        $product["Product"]["productname"],
        $product["Producttype"]["producttypename"],
        $product["Productcategory"]["productcategoryname"],
        $net_change_sales[$array_order][0][0]["total_net_change_quantity"],
        $net_change_sales_return[$array_order][0][0]["total_opening_quantity"],
        $net_change_sales[$array_order][0][0]["total_net_change_quantity"]- $net_change_sales_return[$array_order][0][0]["total_opening_quantity"],
        round($net_change_sales[$array_order][0][0]["total_net_change_price"],2),
        round($net_change_sales_return[$array_order][0][0]["total_opening_price"],2),
        round(($net_change_sales[$array_order][0][0]["total_net_change_price"] - $net_change_sales_return[$array_order][0][0]["total_opening_price"]),2)
    );


    $serial_no++;
    $array_order++;
endforeach;
$rows[]= array(array("Total","colspan='3'"),
    $sum_of_debit ,
    $sum_of_credit,
    ''
);
echo $this->Html->tableCells(
    $coatyperows
);
//echo $this->Html->tableCells(
//    $rows
//);
echo $Utilitys->tableend();
?>
