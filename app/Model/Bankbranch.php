<?php
App::uses('AppModel', 'Model');
class Bankbranch extends AppModel {
	public $name = 'Bankbranch';
	public $usetables = 'bankbranches';
	var $belongsTo  = array(
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'bankbranchinsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'bankbranchupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'bankbranchdeleteid'
		)
	);
	var $virtualFields = array(
		'bankbranch_name' => 'CONCAT(Bankbranch.bankbranchname, " / ", Bankbranch.bankbranchnamebn)',
		'isActive' => 'IF(Bankbranch.bankbranchisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Bankbranch.bankbranchisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
	public $validate = array(
		'bankbranchname' => array(
			'rule' => 'notEmpty',
			'allowEmpty' => false,
			'required' => true,
			'message' => 'Enter a valid Bank Branch Name'
		),
		'bankbranchnamebn' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'allowEmpty' => false,
			'message' => 'Enter a valid Bank Branch Name in Bangla'
		),
	);
}

?>