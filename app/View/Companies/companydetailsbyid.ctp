<?php
echo $Utilitys->tablestart($class=null, $id=null);
	echo"
	<tr>
		<td colspan=\"3\">
			<h2>".__('Company  Information')."</h2>
		</td>
	</tr>
	<tr>
		<th>
			".__('Company Name')."
		</th>
		<td>
			".$company[0]["Company"]["companyname"]."
		</td>
		<td rowspan=\"5\">
			".$this->html->image('/img/company/small/'.$company[0]["Company"]["companylogo"], array('alt'=> 'Logo of '.$company[0]["Company"]["companyname"]))."
		</td>
	</tr>
	<tr>
		<th>
			".__('Company Name (BN)')."
		</th>
		<td>
			".$company[0]["Company"]["companynamebn"]."
		</td>
	</tr>
	<tr>
		<th>
			".__("Company Present Address")."
		</th>
		<td>
			".$company[0]["Company"]["companypresentaddress"]."
		</td>
	</tr>
	<tr>
		<th>
			".__("Company Permanent Address")."
		</th>
		<td>
			".$company[0]["Company"]["companypermanentaddress"]."
		</td>
	</tr>
	<tr>
		<th>
			".__("Company Email")."
		</th>
		<td>
			".$company[0]["Company"]["company_email"]."
		</td>
	</tr>
	<tr>
		<th>
			".__("Company Phone")."
		</th>
		<td>
			".$company[0]["Company"]["company_phone"]."
		</td>
	</tr>
	<!--<tr>
		<th>
			".__("Company Startdate")."
		</th>
		<td>
			".$company[0]["Company"]["companystartdate"]."
		</td>
	</tr>-->
	<tr>
		<th>
			".__("Active")."
		</th>
		<td>
			".$company[0]["Company"]["isActive"]."
		</td>
	</tr>
	";
echo $Utilitys->tableend();
?>