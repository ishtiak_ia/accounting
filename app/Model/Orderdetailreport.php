<?php
App::uses('AppModel', 'Model');
class Orderdetailreport extends AppModel {
	public $name = 'Orderdetailreport';
	public $usetables = 'orderdetails';

	var $belongsTo  = array(
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'orderdetailinsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'orderdetailupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'orderdetaildeleteid'
		),
		'Order' => array(
			'fields' =>array('Order.*'),
			'className'    => 'Order',
			'foreignKey'    => 'transaction_id'
		),
		'User' => array(
			'fields' =>array('User.*'),
			'className'	=> 'User',
			'foreignKey'	=> false,
			'conditions'	=> 'Order.user_id = User.id'
		),
		'Client' => array(
			'fields' =>array('User.*'),
			'className'    => 'User',
			'foreignKey'	=> false,
			'conditions'	=> 'Order.client_id = Client.id'
		),
		'Salesman' => array(
			'fields' =>array('User.*'),
			'className'    => 'User',
			'foreignKey'	=> false,
			'conditions'	=> 'Order.salesman_id = Salesman.id'
		),
		'Coa' => array(
			'fields' =>array('Coa.*'),
			'className'    => 'Coa',
			'foreignKey'    => 'coa_id'
		),
		'Transactiontype' => array(
			'fields' =>array('Transactiontype.*'),
			'className'    => 'Transactiontype',
			'foreignKey'    => 'transactiontype_id'
		),
		'Banktransaction' => array(
			'fields' =>array('Banktransaction.*'),
			'className'	=> 'Banktransaction',
			'foreignKey'	=> false,
			'conditions'	=> 'Banktransaction.transaction_id = Orderdetailreport.transaction_id AND Banktransaction.transactiontype_id = Orderdetailreport.transactiontype_id'
		)		
	);

	var $virtualFields = array(
		'orderdetail_particulars' =>  'CONCAT(
			IF(Orderdetailreport.transactiontype_id=0, "", Transactiontype.transactiontypename), 
			IF(Order.client_id=0, "", CONCAT("/", Client.userfirstname, " ", Client.usermiddlename, " ", Client.userlastname)), 
			IF(Order.user_id=0, "", CONCAT("/", User.userfirstname, " ", User.usermiddlename, " ", User.userlastname)), 
			IF(Orderdetailreport.coa_id=0, " ", CONCAT("/", Coa.coaname)),
			IF(Order.salesman_id=0, "", CONCAT("/", Salesman.userfirstname, " ", Salesman.usermiddlename, " ", Salesman.userlastname))
		)',
		'orderdetail_balance' => 'IF(Orderdetailreport.transactionamount=0, "0.00", ROUND(Orderdetailreport.transactionamount, 2))',
		'orderdetail_withdraw' => 'IF(Orderdetailreport.transactionamount<0,ROUND(Orderdetailreport.transactionamount, 2), "0.00")',
		'orderdetail_deposit' => 'IF(Orderdetailreport.transactionamount>0,ROUND(Orderdetailreport.transactionamount, 2), "0.00")',
		'orderdetail_chequenumber' =>'CONCAT( Banktransaction.banktransactionchequebookseries, " - ", Banktransaction.banktransactionchequesequencynumber )',
	);
	
}