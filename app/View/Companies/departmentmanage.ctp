<?php
//pr($product);
	echo "<h2>".__('Department'). $Utilitys->addurl($title=__("Add New Department"), $this->request['controller'], $page="department")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
		"Department.group_id",
		array(
			'type'=>'select',
			"options"=>array($group),
			'empty'=>__('Select Group'),
			'div'=>false,
			'tabindex'=>1,
			'label'=>false,
			'class'=> 'form-control',
			'style'=>''
			)
		)
		.$this->Form->input(
			"Department.company_id",
			array(
				'type'=>'select',
				"options"=>array($company),
				'class'=> 'form-control',
				'empty'=>__('Select Company'),
				'div'=>false,
				'div'=>false,
				'tabindex'=>2,
				'label'=>false,
				'style'=>''
				)
			)
		.$this->Form->input(
			"Department.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'placeholder' => __("Search Word"),
				'label' => false,
				'tabindex'=>5,
				'class'=> 'form-control'
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"departmentsarchloading\">
	";
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
		echo $this->Html->tableHeaders(
			array(
				__('SL#'),
				array(
					__('Department Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Department Name BN')  => array('class' => 'highlight sortable')
				),
				array(
					__('Company')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Group')  => array('class' => 'highlight sortable')
				),
				array(
					__('Is Active')  => array('class' => 'highlight sortable')
				),
				__('Action')
			)
		);
		$departmentrows = array();
		$countDepartment = 0-1+$this->Paginator->counter('%start%');
		
		foreach($departments as $Thisdepartment):
			$countDepartment++;
			$departmentrows[]= array($countDepartment,$Thisdepartment["Department"]["departmentname"],$Thisdepartment["Department"]["departmentnamebn"],$Thisdepartment["Department"]["company_fullname"],$Thisdepartment["Group"]["groupname"],$Utilitys->statusURL($this->request["controller"], 'department', $Thisdepartment["Department"]["id"], $Thisdepartment["Department"]["departmentuuid"], $Thisdepartment["Department"]["departmentisactive"]),$Utilitys->cusurl($this->request['controller'], 'department', $Thisdepartment["Department"]["id"], $Thisdepartment["Department"]["departmentuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$departmentrows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();

	echo"
				</div>
		<script>
			$(document).ready(function() {
				$(\"#DepartmentSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#departmentsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."companies/departmentsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#departmentsarchloading\").html(result);
						}
					);
				});
				$(\"#DepartmentGroupId\").change(function(){
					var DepartmentGroupId = $(\"#DepartmentGroupId\").val();
					var DepartmentCompanyId = $(\"#DepartmentCompanyId\").val();
					$(\"#departmentsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."companies/departmentsearchbytext',
						{DepartmentGroupId:DepartmentGroupId,DepartmentCompanyId:DepartmentCompanyId},
						function(result) {
							$(\"#departmentsarchloading\").html(result);
						}
					);
				});
				$(\"#DepartmentCompanyId\").change(function(){
					var DepartmentGroupId = $(\"#DepartmentGroupId\").val();
					var DepartmentCompanyId = $(\"#DepartmentCompanyId\").val();
					$(\"#departmentsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."companies/departmentsearchbytext',
						{DepartmentGroupId:DepartmentGroupId, DepartmentCompanyId:DepartmentCompanyId},
						function(result) {
							$(\"#departmentsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>