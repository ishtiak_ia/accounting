<?php
App::import('Controller', 'Logins');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');

class StocklocationsController extends AppController {
	public $name = 'Stocklocations';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'Form' => array('className' => 'BootstrapForm'));
	public $components = array('RequestHandler','Session', 'Image');
	public $uses = array('Product', 'Productcategory', 'Producttype', 'Unit', 'User', 'Stocklocation');

	var $Logins;
	function beforeFilter(){
		$this->Logins =& new LoginsController;
        $this->Logins->constructClasses();
        $this->Logins->__validateLoginStatus();
		//$this->recordActivity();
		// $this->Auth->allow('register');
		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	public  function beforeRender(){
		$this->response->disableCache();//prevent useless warnings for Ajax
		if($this->RequestHandler->isAjax()){
			Configure::write('debug', 0);
		}
	}
	public function stocklocationmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Stocklocation').' | '.__(Configure::read('site_name')));

		$this->paginate = array(
			'fields' => 'Stocklocation.*',
			'order'  =>'Stocklocation.id ASC',
			'limit' => 20
		);
		$stocklocations = $this->paginate('Stocklocation');
		$this->set('stocklocations',$stocklocations);
	}
	public function stocklocationinsertupdate($id=null,$uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Insert/Edit Stocklocation').' | '.__(Configure::read('site_name')));
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
			$stocklocations=$this->Stocklocation->find('all',array("fields" => "Stocklocation.*","conditions"=>array('Stocklocation.id'=>$id,'Stocklocation.stocklocationuuid'=>$uuid)));
			$this->set('stocklocations',$stocklocations);
		endif;
	}
	public function  stocklocationinsertupdateaction($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		$id = @$this->request->data['Stocklocations']['id'];
		$uuid = @$this->request->data['Stocklocations']['stocklocationuuid'];
		if ($this->request->is('post')) {
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
				$this->request->data['Stocklocation']['stocklocationupdateid']=$userID;
				$this->request->data['Stocklocation']['stocklocationupdatedate']=date('Y-m-d H:i:s');
				$message = __('Update Successfully.');
				$this->Stocklocation->id = $id;
			else:
				$this->request->data['Stocklocation']['stocklocationinsertid']=$userID;
				$this->request->data['Stocklocation']['stocklocationinsertdate']=date('Y-m-d H:i:s');
				$this->request->data['Stocklocation']['stocklocationuuid']=String::uuid();
				$message = __('Save Successfully.');
			endif;
			if ($this->Stocklocation->save($this->request->data, array('validate' => 'only'))) {
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				$this->redirect("/stocklocations/stocklocationmanage");
			}else{
				$errors = $this->Stocklocation->invalidFields();
				$this->set('errors', $this->Stocklocation->invalidFields());
				//$this->Session->setFlash('The post was not saved... Please try again!', 'default', array('class' => 'error-message'));
				$this->redirect("/stocklocations/stocklocationinsertupdate");
			}
		}
	}
	public function stocklocationdetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$stocklocation=$this->Stocklocation->find('all',array("fields" => "Stocklocation.*","conditions"=>array('Stocklocation.id'=>$id,'Stocklocation.stocklocationuuid'=>$uuid)));
		$this->set('stocklocation',$stocklocation);
	}
	public function stocklocationdelete($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		if ($this->Stocklocation->updateAll(array('stocklocationdeleteid'=>$userID, 'stocklocationdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'stocklocationisactive'=>2), array('AND' => array('Stocklocation.id'=>$id,'Stocklocation.stocklocationuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/stocklocations/stocklocationmanage");
	}
	public function stocklocationsearchbytext($Searchtext=null){
		$this->layout='ajax';

		$Searchtext = @$this->request->data['Searchtext'];
		$stocklocation_id = array();
		if(!empty($Searchtext) && $Searchtext!=''):
			$stocklocation_id[] = array(
				'OR' => array(
					'Stocklocation.stocklocationname LIKE ' => '%'.$Searchtext.'%',
					'Stocklocation.stocklocationnamebn LIKE ' => '%'.$Searchtext.'%',
					'Stocklocation.stocklocationaddress LIKE ' => '%'.$Searchtext.'%'
				)
			);
		else:
			$stocklocation_id[] = array();
		endif;

		$this->paginate = array(
			'fields' => 'Stocklocation.*',
			'order'  =>'Stocklocation.stocklocationname ASC',
			"conditions"=>$stocklocation_id,
			'limit' => 20
		);
		$stocklocations = $this->paginate('Stocklocation');
		$this->set('stocklocations',$stocklocations);
	}
}