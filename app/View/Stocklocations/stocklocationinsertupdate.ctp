<?php echo $this->Form->create('Stocklocations', array('action' => 'stocklocationinsertupdateaction', 'type' => 'file'));
echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$stocklocations[0]['Stocklocation']['id']));
echo $this->Form->input('stocklocationuuid', array('type'=>'hidden', 'value'=>@$stocklocations[0]['Stocklocation']['stocklocationuuid']));
 ?>
<fieldset>
	<legend>Add Stocklocation Information</legend>
	<table>
		<tr>
            <td>Stocklocation Name:</td>
            <td><?php echo $this->Form->input("Stocklocation.stocklocationname", array('type' => 'text', 'div' => false, 'label' => false, 'value'=>@$stocklocations[0]['Stocklocation']['stocklocationname'])); ?>

            </td>
        </tr>
        <tr>
            <td>Stocklocation Name BN:</td>
            <td><?php echo $this->Form->input("Stocklocation.stocklocationnamebn", array(
                'type' => 'text', 
                'div' => false, 
                'label' => false, 
                'value'=>@$stocklocations[0]['Stocklocation']['stocklocationnamebn']
                )); ?>
            </td>
        </tr>
        <tr>
            <td>Stocklocation Address :</td>
            <td><?php echo $this->Form->input("Stocklocation.stocklocationaddress", array(
                'type' => 'textarea', 
                'div' => false, 
                'label' => false,
                'data-validation-engine'=>'validate[required]',
                'value' =>@$stocklocations[0]['Stocklocation']['stocklocationaddress']
                )); ?>
            </td>
        </tr>
        <tr>
			<td>Is Active :</td>
			<td><?php
                $options = array( 1 => __("Yes"), 0 => __("No"));
                $attributes = array(
                	'legend' => false, 
                	'value' => (@$stocklocations[0]['Stocklocation']['stocklocationisactive'] ? $stocklocations[0]['Stocklocation']['stocklocationisactive'] : 0), 
						'class' => 'radio inline'
                	);
                echo $this->Form->radio('Stocklocation.stocklocationisactive', $options, $attributes);
                ?></td>
		</tr>
        <tr>
            <td>&nbsp;</td>
            <td><?php echo $this->Form->submit('Save', array('div' => false, 'align' => 'right', 'formnovalidate' => true)); ?>
               <?php echo $this->Form->input('stocklocationsubmit', array('type' => 'hidden','value'=>'stocklocationsubmit'));?>
            </td>
        </tr>
	</table>
</fieldset>
<?php echo $this->Form->end(); ?> 