<?php 
    echo"
        <table>
            <tr>
                <td>
                    <h2>".__('Add Designation Information')."</h2>
                </td>
            </tr>
            <tr>
                <td>
    ";
    echo $this->Form->create('Companies', array('action' => 'designationinsertupdateaction', 'type' => 'file'));
    echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$designations[0]['Designation']['id']));
    echo $this->Form->input('designationuuid', array('type'=>'hidden', 'value'=>@$designations[0]['Designation']['designationuuid']));
    $options = array(1 => __("Yes"), 0 =>__("No"));
            $attributes = array(
                'legend' => false, 
                'label' => true, 
                'value' => (@$designations[0]['Designation']['designationisactive'] ? $designations[0]['Designation']['designationisactive'] : 0), 
                'class' => 'form-inline'
            ); 
    $options_parent = array(1 => __("Yes"), 0 =>__("No"));
            $attributes_parent = array(
                'legend' => false, 
                'label' => true, 
                'value' => (@$designations[0]['Designation']['designationisparent'] ? $designations[0]['Designation']['designationisparent'] : 0), 
                'class' => 'form-inline'
            );  			
echo $Utilitys->tablestart();
echo"
        <tr>
            <td>".__("Company")."</td>
            <td>
                ".$this->Form->input(
                                    "Designation.company_id",
                                    array(
                                        'type' => 'select',
                                        "options" => $company, 
                                        'empty' => 'Select Company',
                                        'div' => false, 
                                        'label' => false,
                                        "optgroup label" => false,
                                        'data-validation-engine'=>'validate[required]',
                                        'selected'=> (@$designations[0]['Designation']['company_id'] ? $designations[0]['Designation']['company_id'] : 0)
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("Group")."</td>
            <td>
                ".$this->Form->input(
                                    "Designation.group_id",
                                    array(
                                        'type' => 'select',
                                        "options" => $group, 
                                        'empty' => 'Select Group',
                                        'div' => false, 
                                        'label' => false,
                                        "optgroup label" => false,
                                        'data-validation-engine'=>'validate[required]',
                                        'selected'=> (@$designations[0]['Designation']['group_id'] ? $designations[0]['Designation']['group_id'] : 0)
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("Designation Name")."</td>
            <td>
                ".$this->Form->input(
                                    "Designation.designationname",
                                    array(
                                        'type' => 'text',
                                        'div' => false, 
                                        'label' => false,
                                        'data-validation-engine'=>'validate[required]',
                                        'value'=> @$designations[0]['Designation']['designationname']
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("Designation Name BN")."</td>
            <td>
                ".$this->Form->input(
                                    "Designation.designationnamebn",
                                    array(
                                        'type' => 'text',
                                        'div' => false, 
                                        'label' => false,
                                        'value'=> @$designations[0]['Designation']['designationnamebn']
                                    )
                                )."
            </td>
        </tr>
		<tr>
            <td>".__("is Parent")."</td>
            <td>
                ".$this->Form->radio(
                'Designation.designationisparent',
                $options_parent, 
                $attributes_parent
                )."
            </td>
        </tr>
		<tr>
            <td>".__("Sub Designation Of")."</td>
            <td>
                ".$this->Form->input(
                                    "Designation.designationparentid",
                                    array(
                                        'type' => 'select',
                                        "options" => $parentDesignation, 
                                        'empty' => 'Select Parent Designation',
                                        'div' => false, 
                                        'label' => false,
                                        "optgroup label" => false,
                                        'selected'=> (@$designations[0]['Designation']['designationparentid'] ? $designations[0]['Designation']['designationparentid'] : 0)
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("isActive")."</td>
            <td>
                ".$this->Form->radio(
                'Designation.designationisactive',
                $options, 
                $attributes
                )."
            </td>
        </tr>
        <tr>
            <td colspan=\"2\">
                ".$Utilitys->allformbutton('',$this->request["controller"],$page='designation')."
            </td>
        </tr>
    </table>
    ";
    echo $this->Form->end();
    echo"
                </td>
            </tr>
        </table>
    ";
?>   

<?php echo $this->Form->end(); ?> 
<script type="text/javascript">
 jQuery(document).ready(function(){
    jQuery("#CompaniesDesignationinsertupdateactionForm").validationEngine();
 });
</script>