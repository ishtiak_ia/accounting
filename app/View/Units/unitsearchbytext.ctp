<?php
	echo $Utilitys->paginationcommon();
					echo $Utilitys->tablestart();
						echo $this->Html->tableHeaders(
							array(
								__('SL#'), 
								 
								array(
									__('Unit Name')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Unit Name BN')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Unit Type')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Unit Symbol')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Mon')  => array('class' => 'highlight sortable')
								),
								array(
									__('Kilogram')  => array('class' => 'highlight sortable')
								),
								array(
									__('Gram')  => array('class' => 'highlight sortable')
								),
								array(
									__('Is Active')  => array('class' => 'highlight sortable')
								),
								__('Action')
							)
						);
						$unitrows = array();
						$countUnit = 0-1+$this->Paginator->counter('%start%');
						foreach($units as $Thisunit):
							$countUnit++;
							$unitrows[]= array($countUnit,$Thisunit["Unit"]["unitname"],$Thisunit["Unit"]["unitnamebn"],$Thisunit["Unittype"]["unittypename"]. "/" . $Thisunit["Unittype"]["unittypenamebn"],$Thisunit["Unit"]["unit_symbol"],array(number_format($Thisunit["Unit"]["unitmon"],10),'colspan="1" align="right"'),array(number_format($Thisunit["Unit"]["unitkilogram"],10),'colspan="1" align="right"'),array(number_format($Thisunit["Unit"]["unitgram"],10),'colspan="1" align="right"'),$Utilitys->statusURL($this->request["controller"], 'unit', $Thisunit["Unit"]["id"], $Thisunit["Unit"]["unituuid"], $Thisunit["Unit"]["unitisactive"]),$Utilitys->cusurl($this->request['controller'], 'unit', $Thisunit["Unit"]["id"], $Thisunit["Unit"]["unituuid"]));
						endforeach;
						echo $this->Html->tableCells(
							$unitrows
						);
					echo $Utilitys->tableend();
				echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#UnitSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#unitsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#unitsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>