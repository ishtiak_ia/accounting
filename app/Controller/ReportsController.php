<?php
App::import('Controller', 'Logins');
App::import('Controller', 'Transactions');
App::import('Controller', 'Users');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');

class ReportsController extends AppController {
	public $name = 'Reports';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session');
	public $components = array('RequestHandler','Session');
	public $uses = array('Gltransaction');

	var $Transactions;
	var $Users;
	var $Logins;

	function beforeFilter(){
		$this->Logins =& new LoginsController;
		$this->Logins->constructClasses();
		$this->Logins->__validateLoginStatus();

		$this->Transactions =& new TransactionsController;
		$this->Transactions->constructClasses();

		$this->Users =& new UsersController;
		$this->Users->constructClasses();

		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	public  function beforeRender(){
		$this->response->disableCache();//prevent useless warnings for Ajax
		if($this->RequestHandler->isAjax()){
			Configure::write('debug', 0);
		}
	}

	public function journalmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Journal').' - '.__('Report').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$gltransaction_array = array();
		if($group_id==1):
			$gltransaction_array[] = array('');
		elseif($group_id==2):
			$gltransaction_array[] = array('Gltransaction.company_id'=>$company_id);
		else:
			$gltransaction_array[] = array('Gltransaction.company_id'=>$company_id);
			$gltransaction_array[] = array('Gltransaction.branch_id'=>$branch_id);
		endif;
		$gltransaction_array[] =  array("DATE(Gltransaction.transactiondate) BETWEEN '".date('Y-m-01 00:00:01')."' AND '".date('Y-m-t 23:59:59')."'");
		
		$countGltransaction=$this->Gltransaction->find('count',array("conditions"=>$gltransaction_array));
		$this->paginate = array(
			'fields' => 'Gltransaction.*',
			'order'  => array(
				"Gltransaction.transactiondate" => "ASC, IF(Gltransaction.transactionamount>0, 'Gltransaction.transactionamount ASC', 'Gltransaction.transactionamount DESC')",
				"Coa.coatype_id" => "ASC",
				"Coa.coagroup_id" => "ASC",
				"Gltransaction.coa_id" => "ASC"
			),
			'conditions' => $gltransaction_array,
			'limit' => $countGltransaction
		);
		$gltransaction = $this->paginate('Gltransaction');
		$this->set('gltransaction',$gltransaction);
	}

	public function journalsearchbydate(){
		$this->layout='ajax';
		$this->set('title_for_layout', __('Journal').' - '.__('Report').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$JournalFormdate=$this->request->data['JournalFormdate'];
		$JournalTodate=$this->request->data['JournalTodate'];

		$gltransaction_array = array();
		if($group_id==1):
			$gltransaction_array[] = array('');
		elseif($group_id==2):
			$gltransaction_array[] = array('Gltransaction.company_id'=>$company_id);
		else:
			$gltransaction_array[] = array('Gltransaction.company_id'=>$company_id);
			$gltransaction_array[] = array('Gltransaction.branch_id'=>$branch_id);
		endif;

		if((!empty($JournalFormdate) || $JournalFormdate!='0000-00-00') && (empty($JournalTodate) || $JournalTodate=='0000-00-00')):
			$gltransaction_array[] = array("DATE(Gltransaction.transactiondate) BETWEEN '".$JournalFormdate."' AND '".$JournalFormdate."'");
		elseif((!empty($JournalTodate) || $JournalTodate!='0000-00-00') && (empty($JournalFormdate) || $JournalFormdate=='0000-00-00')):
			$gltransaction_array[] =  array("DATE(Gltransaction.transactiondate) BETWEEN '".$JournalTodate."' AND '".$JournalTodate."'");
		elseif(!empty($JournalFormdate) && !empty($JournalTodate)):
			$gltransaction_array[] =  array("DATE(Gltransaction.transactiondate) BETWEEN '".$JournalFormdate."' AND '".$JournalTodate."'");
		else:
			$gltransaction_array[] =  array("DATE(Gltransaction.transactiondate) BETWEEN '".date('Y-m-01 00:00:01')."' AND '".date('Y-m-t 23:59:59')."'");
		endif;
		$countGltransaction=$this->Gltransaction->find('count',array("conditions"=>$gltransaction_array));
		$this->paginate = array(
			'fields' => 'Gltransaction.*',
			'conditions' => $gltransaction_array,
			'order'  => array(
				"Gltransaction.transactiondate" => "ASC, IF(Gltransaction.transactionamount>0, 'Gltransaction.transactionamount ASC', 'Gltransaction.transactionamount DESC')",
				"Coa.coatype_id" => "ASC",
				"Coa.coagroup_id" => "ASC",
				"Gltransaction.coa_id" => "ASC"
			),
			'limit' => $countGltransaction
		);
		$gltransaction = $this->paginate('Gltransaction');
		$this->set('gltransaction',$gltransaction);
	}

	public function balancesheetmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Balance Sheet').' - '.__('Report').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$BalancesheetFormdate=@$this->request->data['BalancesheetFormdate']?$this->request->data['BalancesheetFormdate']:'0000-00-00 00:00:00';
		$BalancesheetTodate=@$this->request->data['BalancesheetTodate']?$this->request->data['BalancesheetTodate']:'0000-00-00 00:00:00';

		$gltransaction_array = "";
		$gltransaction_array .=  "Coatype.id IN (1,2)";
		if($group_id==1):
		elseif($group_id==2):
			$gltransaction_array .= " AND Gltransaction.company_id={$company_id}";
		else:
			$gltransaction_array .= " AND Gltransaction.company_id={$company_id}";
			$gltransaction_array .= " AND Gltransaction.branch_id={$branch_id}";
		endif;

		if($BalancesheetFormdate!='0000-00-00 00:00:00' && $BalancesheetTodate=='0000-00-00 00:00:00'):
			$gltransaction_array .= " AND DATE(Gltransaction.transactiondate) BETWEEN '".$BalancesheetFormdate."' AND '".$BalancesheetFormdate."'";
		elseif($BalancesheetTodate!='0000-00-00 00:00:00' && $BalancesheetFormdate=='0000-00-00 00:00:00'):
			$gltransaction_array .=  " AND DATE(Gltransaction.transactiondate) BETWEEN '".$BalancesheetTodate."' AND '".$BalancesheetTodate."'";
		elseif($BalancesheetFormdate!='0000-00-00 00:00:00' && $BalancesheetTodate!='0000-00-00 00:00:00'):
			$gltransaction_array .=  " AND DATE(Gltransaction.transactiondate) BETWEEN '".$BalancesheetFormdate."' AND '".$BalancesheetTodate."'";
		else:
			$gltransaction_array .=  " AND DATE(Gltransaction.transactiondate) BETWEEN '".date("Y-m-01 00:00:01")."' AND '".date("Y-m-t 23:59:59")."'";
			$BalancesheetFormdate = new DateTime(date("Y-m-01 00:00:01"));
			$BalancesheetTodate = new DateTime(date("Y-m-t 23:59:59"));
		endif;

		$balancesheet = $this->Gltransaction->query("
			SELECT
				Coatype.id,
				Coatype.coatypename,
				Coatype.coatypenamebn,
				Coagroup.id,
				Coagroup.coagroupname,
				Coagroup.coagroupnamebn,
				Coa.id,
				Coa.coacode,
				Coa.coaname,
				Coa.coanamebn,
				Gltransaction.transaction_id,
				Gltransaction.transactiontype_id,
				SUM(Gltransaction.transactionamount) AS gltransaction_totalbalance,
				YEAR(Gltransaction.transactiondate) AS gltransaction_date
			FROM
				gltransactions AS Gltransaction INNER JOIN 
				coas AS Coa ON Gltransaction.coa_id = Coa.id INNER JOIN
				coagroups AS Coagroup ON Coa.coagroup_id = Coagroup.id INNER JOIN
				coatypes AS Coatype ON Coagroup.coatype_id = Coatype.id
			WHERE {$gltransaction_array}
			GROUP BY
				YEAR(Gltransaction.transactiondate),
				Coa.id
			ORDER BY
				YEAR(Gltransaction.transactiondate) ASC,
				Coatype.id ASC,
				Coagroup.id ASC,
				Coa.coacode ASC
		");
		$this->set('balancesheet',$balancesheet);
		$this->set('daterange',date_format($BalancesheetFormdate, "Y-m-d")." TO ".date_format($BalancesheetTodate, "Y-m-d"));
		$totalbalancesheet = $this->Gltransaction->query("
			SELECT
				Coatype.id,
				Coatype.coatypename,
				Coatype.coatypenamebn,
				SUM(Gltransaction.transactionamount) AS gltransaction_totalbalance,
				YEAR(Gltransaction.transactiondate) AS gltransaction_date
			FROM
				gltransactions AS Gltransaction INNER JOIN 
				coas AS Coa ON Gltransaction.coa_id = Coa.id INNER JOIN
				coagroups AS Coagroup ON Coa.coagroup_id = Coagroup.id INNER JOIN
				coatypes AS Coatype ON Coagroup.coatype_id = Coatype.id
			WHERE {$gltransaction_array}
			GROUP BY
				YEAR(Gltransaction.transactiondate),
				Coatype.id
			ORDER BY
				YEAR(Gltransaction.transactiondate) ASC,
				Coatype.id ASC,
				Coagroup.id ASC,
				Coa.coacode ASC
		");
		$this->set('totalbalancesheet',$totalbalancesheet);
	}

	public function balancesheetsearchbydate(){
		$this->layout='ajax';
		$this->set('title_for_layout', __('Balance Sheet').' - '.__('Report').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$BalancesheetFormdate=@$this->request->data['BalancesheetFormdate']?$this->request->data['BalancesheetFormdate']:'0000-00-00 00:00:00';
		$BalancesheetTodate=@$this->request->data['BalancesheetTodate']?$this->request->data['BalancesheetTodate']:'0000-00-00 00:00:00';

		$gltransaction_array = "";
		$gltransaction_array .=  "Coatype.id IN (1,2)";
		if($group_id==1):
		elseif($group_id==2):
			$gltransaction_array .= " AND Gltransaction.company_id={$company_id}";
		else:
			$gltransaction_array .= " AND Gltransaction.company_id={$company_id}";
			$gltransaction_array .= " AND Gltransaction.branch_id={$branch_id}";
		endif;

		if($BalancesheetFormdate!='0000-00-00 00:00:00' && $BalancesheetTodate=='0000-00-00 00:00:00'):
			$gltransaction_array .= " AND DATE(Gltransaction.transactiondate) BETWEEN '".$BalancesheetFormdate."' AND '".$BalancesheetFormdate."'";
			$BalancesheetFormdate = $BalancesheetFormdate;
			$BalancesheetTodate = $BalancesheetFormdate;
		elseif($BalancesheetTodate!='0000-00-00 00:00:00' && $BalancesheetFormdate=='0000-00-00 00:00:00'):
			$gltransaction_array .=  " AND DATE(Gltransaction.transactiondate) BETWEEN '".$BalancesheetTodate."' AND '".$BalancesheetTodate."'";
			$BalancesheetFormdate = $BalancesheetTodate;
			$BalancesheetTodate = $BalancesheetTodate;
		elseif($BalancesheetFormdate!='0000-00-00 00:00:00' && $BalancesheetTodate!='0000-00-00 00:00:00'):
			$gltransaction_array .=  " AND DATE(Gltransaction.transactiondate) BETWEEN '".$BalancesheetFormdate."' AND '".$BalancesheetTodate."'";
			$BalancesheetFormdate = $BalancesheetFormdate;
			$BalancesheetTodate = $BalancesheetTodate;
		else:
			$gltransaction_array .=  " AND DATE(Gltransaction.transactiondate) BETWEEN '".date("Y-m-01 00:00:01")."' AND '".date("Y-m-t 23:59:59")."'";
			$BalancesheetFormdate = date("Y-m-01 00:00:01");
			$BalancesheetTodate = date("Y-m-t 23:59:59");
		endif;

		$balancesheet = $this->Gltransaction->query("
			SELECT
				Coatype.id,
				Coatype.coatypename,
				Coatype.coatypenamebn,
				Coagroup.id,
				Coagroup.coagroupname,
				Coagroup.coagroupnamebn,
				Coa.id,
				Coa.coacode,
				Coa.coaname,
				Coa.coanamebn,
				Gltransaction.transaction_id,
				Gltransaction.transactiontype_id,
				SUM(Gltransaction.transactionamount) AS gltransaction_totalbalance,
				YEAR(Gltransaction.transactiondate) AS gltransaction_date
			FROM
				gltransactions AS Gltransaction INNER JOIN 
				coas AS Coa ON Gltransaction.coa_id = Coa.id INNER JOIN
				coagroups AS Coagroup ON Coa.coagroup_id = Coagroup.id INNER JOIN
				coatypes AS Coatype ON Coagroup.coatype_id = Coatype.id
			WHERE {$gltransaction_array}
			GROUP BY
				YEAR(Gltransaction.transactiondate),
				Coa.id
			ORDER BY
				YEAR(Gltransaction.transactiondate) ASC,
				Coatype.id ASC,
				Coagroup.id ASC,
				Coa.coacode ASC
		");
		$this->set('balancesheet',$balancesheet);
		$this->set('daterange',date_format(new DateTime($BalancesheetFormdate), "Y-m-d")." TO ".date_format(new DateTime($BalancesheetTodate), "Y-m-d"));
		$totalbalancesheet = $this->Gltransaction->query("
			SELECT
				Coatype.id,
				Coatype.coatypename,
				Coatype.coatypenamebn,
				SUM(Gltransaction.transactionamount) AS gltransaction_totalbalance,
				YEAR(Gltransaction.transactiondate) AS gltransaction_date
			FROM
				gltransactions AS Gltransaction INNER JOIN 
				coas AS Coa ON Gltransaction.coa_id = Coa.id INNER JOIN
				coagroups AS Coagroup ON Coa.coagroup_id = Coagroup.id INNER JOIN
				coatypes AS Coatype ON Coagroup.coatype_id = Coatype.id
			WHERE {$gltransaction_array}
			GROUP BY
				YEAR(Gltransaction.transactiondate),
				Coatype.id
			ORDER BY
				YEAR(Gltransaction.transactiondate) ASC,
				Coatype.id ASC,
				Coagroup.id ASC,
				Coa.coacode ASC
		");
		$this->set('totalbalancesheet',$totalbalancesheet);
	}

	public function incomeexpencesmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Income Expences Statement').' - '.__('Report').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$BalancesheetFormdate=@$this->request->data['BalancesheetFormdate']?$this->request->data['BalancesheetFormdate']:'0000-00-00 00:00:00';
		$BalancesheetTodate=@$this->request->data['BalancesheetTodate']?$this->request->data['BalancesheetTodate']:'0000-00-00 00:00:00';

		$gltransaction_array = "";
		$gltransaction_array .=  "Coatype.id IN (4, 6)";
		if($group_id==1):
		elseif($group_id==2):
			$gltransaction_array .= " AND Gltransaction.company_id={$company_id}";
		else:
			$gltransaction_array .= " AND Gltransaction.company_id={$company_id}";
			$gltransaction_array .= " AND Gltransaction.branch_id={$branch_id}";
		endif;

		if($BalancesheetFormdate!='0000-00-00 00:00:00' && $BalancesheetTodate=='0000-00-00 00:00:00'):
			$gltransaction_array .= " AND DATE(Gltransaction.transactiondate) BETWEEN '".$BalancesheetFormdate."' AND '".$BalancesheetFormdate."'";
		elseif($BalancesheetTodate!='0000-00-00 00:00:00' && $BalancesheetFormdate=='0000-00-00 00:00:00'):
			$gltransaction_array .=  " AND DATE(Gltransaction.transactiondate) BETWEEN '".$BalancesheetTodate."' AND '".$BalancesheetTodate."'";
		elseif($BalancesheetFormdate!='0000-00-00 00:00:00' && $BalancesheetTodate!='0000-00-00 00:00:00'):
			$gltransaction_array .=  " AND DATE(Gltransaction.transactiondate) BETWEEN '".$BalancesheetFormdate."' AND '".$BalancesheetTodate."'";
		else:
			$gltransaction_array .=  " AND DATE(Gltransaction.transactiondate) BETWEEN '".date("Y-m-01 00:00:01")."' AND '".date("Y-m-t 23:59:59")."'";
			$BalancesheetFormdate = new DateTime(date("Y-m-01 00:00:01"));
			$BalancesheetTodate = new DateTime(date("Y-m-t 23:59:59"));
		endif;

		$balancesheet = $this->Gltransaction->query("
			SELECT
				Coatype.id,
				Coatype.coatypename,
				Coatype.coatypenamebn,
				Coagroup.id,
				Coagroup.coagroupname,
				Coagroup.coagroupnamebn,
				Coa.id,
				Coa.coacode,
				Coa.coaname,
				Coa.coanamebn,
				Gltransaction.transaction_id,
				Gltransaction.transactiontype_id,
				SUM(Gltransaction.transactionamount) AS gltransaction_totalbalance,
				YEAR(Gltransaction.transactiondate) AS gltransaction_date
			FROM
				gltransactions AS Gltransaction INNER JOIN 
				coas AS Coa ON Gltransaction.coa_id = Coa.id INNER JOIN
				coagroups AS Coagroup ON Coa.coagroup_id = Coagroup.id INNER JOIN
				coatypes AS Coatype ON Coagroup.coatype_id = Coatype.id
			WHERE {$gltransaction_array}
			GROUP BY
				YEAR(Gltransaction.transactiondate),
				Coa.id
			ORDER BY
				YEAR(Gltransaction.transactiondate) ASC,
				Coatype.id ASC,
				Coagroup.id ASC,
				Coa.coacode ASC
		");
		$this->set('balancesheet',$balancesheet);
		$this->set('daterange',date_format($BalancesheetFormdate, "Y-m-d")." TO ".date_format($BalancesheetTodate, "Y-m-d"));
		$totalbalancesheet = $this->Gltransaction->query("
			SELECT
				Coatype.id,
				Coatype.coatypename,
				Coatype.coatypenamebn,
				SUM(Gltransaction.transactionamount) AS gltransaction_totalbalance,
				YEAR(Gltransaction.transactiondate) AS gltransaction_date
			FROM
				gltransactions AS Gltransaction INNER JOIN 
				coas AS Coa ON Gltransaction.coa_id = Coa.id INNER JOIN
				coagroups AS Coagroup ON Coa.coagroup_id = Coagroup.id INNER JOIN
				coatypes AS Coatype ON Coagroup.coatype_id = Coatype.id
			WHERE {$gltransaction_array}
			GROUP BY
				YEAR(Gltransaction.transactiondate),
				Coatype.id
			ORDER BY
				YEAR(Gltransaction.transactiondate) ASC,
				Coatype.id ASC,
				Coagroup.id ASC,
				Coa.coacode ASC
		");
		$this->set('totalbalancesheet',$totalbalancesheet);
	}

	public function incomeexpencessearchbydate(){
		$this->layout='ajax';
		$this->set('title_for_layout', __('Income Expences Statement').' - '.__('Report').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$BalancesheetFormdate=@$this->request->data['BalancesheetFormdate']?$this->request->data['BalancesheetFormdate']:'0000-00-00 00:00:00';
		$BalancesheetTodate=@$this->request->data['BalancesheetTodate']?$this->request->data['BalancesheetTodate']:'0000-00-00 00:00:00';

		$gltransaction_array = "";
		$gltransaction_array .=  "Coatype.id IN (4, 6)";
		if($group_id==1):
		elseif($group_id==2):
			$gltransaction_array .= " AND Gltransaction.company_id={$company_id}";
		else:
			$gltransaction_array .= " AND Gltransaction.company_id={$company_id}";
			$gltransaction_array .= " AND Gltransaction.branch_id={$branch_id}";
		endif;

		if($BalancesheetFormdate!='0000-00-00 00:00:00' && $BalancesheetTodate=='0000-00-00 00:00:00'):
			$gltransaction_array .= " AND DATE(Gltransaction.transactiondate) BETWEEN '".$BalancesheetFormdate."' AND '".$BalancesheetFormdate."'";
			$BalancesheetFormdate = $BalancesheetFormdate;
			$BalancesheetTodate = $BalancesheetFormdate;
		elseif($BalancesheetTodate!='0000-00-00 00:00:00' && $BalancesheetFormdate=='0000-00-00 00:00:00'):
			$gltransaction_array .=  " AND DATE(Gltransaction.transactiondate) BETWEEN '".$BalancesheetTodate."' AND '".$BalancesheetTodate."'";
			$BalancesheetFormdate = $BalancesheetTodate;
			$BalancesheetTodate = $BalancesheetTodate;
		elseif($BalancesheetFormdate!='0000-00-00 00:00:00' && $BalancesheetTodate!='0000-00-00 00:00:00'):
			$gltransaction_array .=  " AND DATE(Gltransaction.transactiondate) BETWEEN '".$BalancesheetFormdate."' AND '".$BalancesheetTodate."'";
			$BalancesheetFormdate = $BalancesheetFormdate;
			$BalancesheetTodate = $BalancesheetTodate;
		else:
			$gltransaction_array .=  " AND DATE(Gltransaction.transactiondate) BETWEEN '".date("Y-m-01 00:00:01")."' AND '".date("Y-m-t 23:59:59")."'";
			$BalancesheetFormdate = date("Y-m-01 00:00:01");
			$BalancesheetTodate = date("Y-m-t 23:59:59");
		endif;

		$balancesheet = $this->Gltransaction->query("
			SELECT
				Coatype.id,
				Coatype.coatypename,
				Coatype.coatypenamebn,
				Coagroup.id,
				Coagroup.coagroupname,
				Coagroup.coagroupnamebn,
				Coa.id,
				Coa.coacode,
				Coa.coaname,
				Coa.coanamebn,
				Gltransaction.transaction_id,
				Gltransaction.transactiontype_id,
				SUM(Gltransaction.transactionamount) AS gltransaction_totalbalance,
				YEAR(Gltransaction.transactiondate) AS gltransaction_date
			FROM
				gltransactions AS Gltransaction INNER JOIN 
				coas AS Coa ON Gltransaction.coa_id = Coa.id INNER JOIN
				coagroups AS Coagroup ON Coa.coagroup_id = Coagroup.id INNER JOIN
				coatypes AS Coatype ON Coagroup.coatype_id = Coatype.id
			WHERE {$gltransaction_array}
			GROUP BY
				YEAR(Gltransaction.transactiondate),
				Coa.id
			ORDER BY
				YEAR(Gltransaction.transactiondate) ASC,
				Coatype.id ASC,
				Coagroup.id ASC,
				Coa.coacode ASC
		");
		$this->set('balancesheet',$balancesheet);
		$this->set('daterange',date_format(new DateTime($BalancesheetFormdate), "Y-m-d")." TO ".date_format(new DateTime($BalancesheetTodate), "Y-m-d"));
		$totalbalancesheet = $this->Gltransaction->query("
			SELECT
				Coatype.id,
				Coatype.coatypename,
				Coatype.coatypenamebn,
				SUM(Gltransaction.transactionamount) AS gltransaction_totalbalance,
				YEAR(Gltransaction.transactiondate) AS gltransaction_date
			FROM
				gltransactions AS Gltransaction INNER JOIN 
				coas AS Coa ON Gltransaction.coa_id = Coa.id INNER JOIN
				coagroups AS Coagroup ON Coa.coagroup_id = Coagroup.id INNER JOIN
				coatypes AS Coatype ON Coagroup.coatype_id = Coatype.id
			WHERE {$gltransaction_array}
			GROUP BY
				YEAR(Gltransaction.transactiondate),
				Coatype.id
			ORDER BY
				YEAR(Gltransaction.transactiondate) ASC,
				Coatype.id ASC,
				Coagroup.id ASC,
				Coa.coacode ASC
		");
		$this->set('totalbalancesheet',$totalbalancesheet);
	}

}
?>