<?php
		//strtotime("2015-04-08")."<br>";
		$today  = mktime(0, 0, 0, date("m")  , date("d"), date("Y"));
		$tomorrow  = mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"));
		$nextday  = mktime(0, 0, 0, date("m")  , date("d")+2, date("Y"));
//pr($userlist);
echo "<h2>".__('User Notification')."</h2>";
/*echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Usergroup.group_id",
			array(
				'type'=>'select',
				"options"=>array($group),
				'empty'=>__('Select Group'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
				)
			)
		.$this->Form->input(
			"Usergroup.company_id",
			array(
				'type'=>'select',
				"options"=>array($company),
				'class'=> 'form-control',
				'empty'=>__('Select Company'),
				'div'=>false,
				'tabindex'=>2,
				'label'=>false,
				'style'=>''
				)
			)
		.$this->Form->input(
			"Usergroup.branch_id",
			array(
				'type'=>'select',
				"options"=>array($branch),
				'class'=> 'form-control',
				'empty'=>__('Select Branch'),
				'div'=>false,
				'tabindex'=>3,
				'label'=>false,
				'style'=>''
				)
			)
                .$this->Form->input(
			"Usergroup.user_id",
			array(
				'type'=>'select',
				"options"=>array($user),
				'class'=> 'form-control',
				'empty'=>__('Select User'),
				'div'=>false,
				'tabindex'=>3,
				'label'=>false,
				'style'=>''
				)
			);
	echo $Utilitys->filtertagend();*/
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-12\">";
			echo"<div class=\"row\">";
				echo"<div class=\"col-md-3\">";
					echo "<div id=\"usergroupsarchloading\">";
		echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'),
					array(
						__('Today')  => array('class' => 'highlight sortable')
					),
				)
			);
			$usergrouprows = array();
			$countUserListToday = 0;
			foreach($userlist as $Thisuserlist):
				if(strtotime("{$Thisuserlist["User"]["next_notification_date_actual"]}") == $today):
				$countUserListToday++;
				$usergrouprows[]= array($countUserListToday,$Thisuserlist["User"]["user_fullname"]);
				endif;
			endforeach;
			echo $this->Html->tableCells(
				$usergrouprows
			);
		echo $Utilitys->tableend();
					echo"</div>";
				echo"</div>";
////////////////////////////////////////
				echo"<div class=\"col-md-3\">";
					echo "<div id=\"usergroupsarchloading\">";
		echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'),
					array(
						__('Tomorrow')  => array('class' => 'highlight sortable')
					),
				)
			);
			$countUserListTomorrow = 0;
			$usergrouprows = array();
			foreach($userlist as $Thisuserlist):
				if(strtotime("{$Thisuserlist["User"]["next_notification_date_actual"]}") == $tomorrow):
				$countUserListTomorrow++;
				$usergrouprows[]= array($countUserListTomorrow,$Thisuserlist["User"]["user_fullname"]);
				endif;
			endforeach;
			echo $this->Html->tableCells(
				$usergrouprows
			);
		echo $Utilitys->tableend();
					echo"</div>";
				echo"</div>";
////////////////////////////////////////
				echo"<div class=\"col-md-3\">";
					echo "<div id=\"usergroupsarchloading\">";
		echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'),
					array(
						__('Next Day')  => array('class' => 'highlight sortable')
					),
				)
			);
			$countUserListNextDay = 0;
			$usergrouprows = array();
			foreach($userlist as $Thisuserlist):
				if(strtotime("{$Thisuserlist["User"]["next_notification_date_actual"]}") == $nextday):
				$countUserListNextDay++;
				$usergrouprows[]= array($countUserListNextDay,$Thisuserlist["User"]["user_fullname"]);
				endif;
			endforeach;
			echo $this->Html->tableCells(
				$usergrouprows
			);
		echo $Utilitys->tableend();
					echo"</div>";
				echo"</div>";
////////////////////////////////////////
				echo"<div class=\"col-md-3\">";
					echo "<div id=\"usergroupsarchloading\">";
		echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'),
					array(
						__('Time Over')  => array('class' => 'highlight sortable')
					)
				)
			);
			$countUserList = 0;
			$usergrouprows = array();
			foreach($userlist as $Thisuserlist):
				if(strtotime("{$Thisuserlist["User"]["next_notification_date_actual"]}") < $today):
				$countUserList++;
				$usergrouprows[]= array($countUserList,$Thisuserlist["User"]["user_fullname"]);
				endif;
			endforeach;
			echo $this->Html->tableCells(
				$usergrouprows
			);
		echo $Utilitys->tableend();
					echo"</div>";
				echo"</div>";
			echo"</div>";
		echo"</div>";
	echo"</div>";
	echo"
		<script>
			$(document).ready(function() {
				$(\"#UsergroupGroupId\").change(function(){
					var UsergroupGroupId = $(\"#UsergroupGroupId\").val();
					var UsergroupCompanyId = $(\"#UsergroupCompanyId\").val();
					var UsergroupBranchId = $(\"#UsergroupBranchId\").val();
                                        var UsergroupUserId = $(\"#UsergroupUserId\").val();
					$(\"#usergroupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usergroupsearchbytext',
						{UsergroupGroupId:UsergroupGroupId,UsergroupCompanyId:UsergroupCompanyId,UsergroupBranchId:UsergroupBranchId, UsergroupUserId:UsergroupUserId},
						function(result) {
							$(\"#usergroupsarchloading\").html(result);
						}
					);
				});
				$(\"#UsergroupCompanyId\").change(function(){
					var UsergroupGroupId = $(\"#UsergroupGroupId\").val();
					var UsergroupCompanyId = $(\"#UsergroupCompanyId\").val();
					var UsergroupBranchId = $(\"#UsergroupBranchId\").val();
                                        var UsergroupUserId = $(\"#UsergroupUserId\").val();
					$(\"#usergroupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usergroupsearchbytext',
						{UsergroupGroupId:UsergroupGroupId,UsergroupCompanyId:UsergroupCompanyId,UsergroupBranchId:UsergroupBranchId, UsergroupUserId:UsergroupUserId},
						function(result) {
							$(\"#usergroupsarchloading\").html(result);
						}
					);
				});
				$(\"#UsergroupBranchId\").change(function(){
					var UsergroupGroupId = $(\"#UsergroupGroupId\").val();
					var UsergroupCompanyId = $(\"#UsergroupCompanyId\").val();
					var UsergroupBranchId = $(\"#UsergroupBranchId\").val();
                                        var UsergroupUserId = $(\"#UsergroupUserId\").val();
					$(\"#usergroupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usergroupsearchbytext',
						{UsergroupGroupId:UsergroupGroupId,UsergroupCompanyId:UsergroupCompanyId,UsergroupBranchId:UsergroupBranchId, UsergroupUserId:UsergroupUserId},
						function(result) {
							$(\"#usergroupsarchloading\").html(result);
						}
					);
				});
                                $(\"#UsergroupUserId\").change(function(){
					var UsergroupGroupId = $(\"#UsergroupGroupId\").val();
					var UsergroupCompanyId = $(\"#UsergroupCompanyId\").val();
					var UsergroupBranchId = $(\"#UsergroupBranchId\").val();
                                        var UsergroupUserId = $(\"#UsergroupUserId\").val();
					$(\"#usergroupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usergroupsearchbytext',
						{UsergroupGroupId:UsergroupGroupId,UsergroupCompanyId:UsergroupCompanyId,UsergroupBranchId:UsergroupBranchId, UsergroupUserId:UsergroupUserId},
						function(result) {
							$(\"#usergroupsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>