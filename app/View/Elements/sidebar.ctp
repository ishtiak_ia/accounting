<?php
// Integrating some visual features with PHP
$active = 'class="active"';
?>

<?php if( isset( $_SESSION['User']['group_id'] ) ) { ?>
<div class="sidebar-menu-toggle-handle" data-toggle="collapse" data-target="#sidebar-collapse">
    <span class="glyphicon glyphicon-sort"></span>
     <?php __('Menu') ?>
</div>
<div id="sidebar-collapse" class="col-sm-2 left-sidebar">
    <ul class="nav nav-pills nav-stacked" role="tablist">
        <?php echo $this->element('menu'); ?>
    </ul>
</div>
<!-- /.left-sidebar -->

<?php
	$offset_class = '';
	$offset_class = 'col-sm-10 col-sm-offset-2';
} else {
	$offset_class = 'col-sm-12';
}
?>

<!--
### DON'T REMOVE THIS COMMENT, PLEASE ###
@Devs:
Please note that, the following <div> is a part of every content CTP,
and NOT a part of the Sidebar
And the end of the div is in 'footer.php' (find the </div> /.main)

It's technically added here to make the content CTP clean and readable
### DON'T REMOVE THIS COMMENT, PLEASE ###
-->
<div class=" <?php echo $offset_class; ?> main">