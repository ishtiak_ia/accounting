<?php 
echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Employee Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Exam/Degree Title')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Institute Name')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Result')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Passing Year')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Duration')  => array('class' => 'highlight sortable')
					),
					__('Active'), 
					__('Action')
				)
			);
			$usereducationrows = array();
			$countUsereducationrows = 0-1+$this->Paginator->counter('%start%');
			foreach($usereducation as $Thisusereducation):
				$countUsereducationrows++;
				$usereducationrows[]= array($countUsereducationrows,$Thisusereducation["Usereducation"]["user_fullname"],$Thisusereducation["Usereducation"]["usereducationdegreename"],$Thisusereducation["Usereducation"]["usereducationinstitute"],$Thisusereducation["Usereducation"]["usereducationresult"],$Thisusereducation["Usereducation"]["usereducationpassing"],$Thisusereducation["Usereducation"]["usereducationduration"],$Utilitys->statusURL($this->request["controller"], 'usereducation', $Thisusereducation["Usereducation"]["id"], $Thisusereducation["Usereducation"]["usereducationuuid"], $Thisusereducation["Usereducation"]["usereducationisactive"]),$Utilitys->cusurl($this->request["controller"], 'usereducation', $Thisusereducation["Usereducation"]["id"], $Thisusereducation["Usereducation"]["usereducationuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$usereducationrows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#UsereducationSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#usereducationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#usereducationsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>