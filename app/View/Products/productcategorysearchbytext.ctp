<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('Product Category Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Product Category Name BN')  => array('class' => 'highlight sortable')
				), 
				__('Active'), 
				__('Action')
			)
		);
		$productcategoryrows = array();
		$countProductcategory = 0-1+$this->Paginator->counter('%start%');
		foreach($productcategory as $Thisproductcategory):
			$countProductcategory++;
			$productcategoryrows[]= array($countProductcategory,$Thisproductcategory["Productcategory"]["productcategoryname"],$Thisproductcategory["Productcategory"]["productcategorynamebn"],$Utilitys->statusURL($this->request["controller"], 'productcategory', $Thisproductcategory["Productcategory"]["id"], $Thisproductcategory["Productcategory"]["productcategoryuuid"], $Thisproductcategory["Productcategory"]["productcategoryisactive"]),$Utilitys->cusurl($this->request["controller"], 'productcategory', $Thisproductcategory["Productcategory"]["id"], $Thisproductcategory["Productcategory"]["productcategoryuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$productcategoryrows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#ProducttypeSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#producttypesarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#producttypesarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>