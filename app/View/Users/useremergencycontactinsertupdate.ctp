<?php
//print_r($user_list);
echo $this->Form->create('Users', array('action' => 'useremergencycontactinsertupdateaction', 'type' => 'file'));
	echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$useremergencycontact[0]["Useremergencycontact"]['id']));
	echo $this->Form->input('useremergencycontactuuid', array('type'=>'hidden', 'value'=>@$useremergencycontact[0]["Useremergencycontact"]['useremergencycontactuuid']?$useremergencycontact[0]["Useremergencycontact"]['useremergencycontactuuid']:0, 'class'=>''));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$useremergencycontact[0]['Useremergencycontact']['useremergencycontactisactive'] ? $useremergencycontact[0]['Useremergencycontact']['useremergencycontactisactive'] : 0),
			'class' => 'form-inline'
		);
	echo"<h3>".__("Add Emergency Contact")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-4\">";
			echo $this->Form->input(
				"Useremergencycontact.department_id",
				array(
					'type'=>'select',
					"options"=>array($department_list),
					'empty'=>__('Select Department'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>1,
					'label'=>__('Select Department&nbsp;<span style=\'color:#cc0000\'>*</span>'),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style'=>'',
					"selected" => (@$useremergencycontact[0]['Useremergencycontact']['department_id'] ? $useremergencycontact[0]['Useremergencycontact']['department_id'] : 0)
				)
			);
			echo "<div class=\"form-group\" id=\"salaryuserchangediv\">";
			echo $this->Form->input(
				"Useremergencycontact.user_id",
				array(
					'type'=>'select',
					"options"=>array($user_list),
					'empty'=>__('Select Employee'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>2,
					'label'=>__('Select Employee&nbsp;<span style=\'color:#cc0000\'>*</span>'),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style'=>'',
					"selected" => (@$useremergencycontact[0]['Useremergencycontact']['user_id'] ? $useremergencycontact[0]['Useremergencycontact']['user_id'] : 0)
				)
			);
            echo "</div>";
			echo $this->Form->input(
				"Useremergencycontact.useremergencycontactname",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Emergency Contact Name&nbsp;<span style=\'color:#cc0000\'>*</span>'),
					'tabindex'=>3,
					'placeholder' => __("Emergency Contact Name"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style' => '',
					'value' => @$useremergencycontact[0]['Useremergencycontact']['useremergencycontactname']
				)
			);
			echo $this->Form->input(
				"Useremergencycontact.useremergencycontactnamebn",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Emergency Contact Name BN&nbsp;<span style=\'color:#cc0000\'>*</span>'),
					'tabindex'=>4,
					'placeholder' => __("Emergency Contact Name BN"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style' => '',
					'value' => @$useremergencycontact[0]['Useremergencycontact']['useremergencycontactnamebn']
				)
			);
			echo $this->Form->input(
				"Useremergencycontact.useremergencycontactrelationship",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Relationship&nbsp;<span style=\'color:#cc0000\'>*</span>'),
					'tabindex'=>5,
					'placeholder' => __("Relationship"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style' => '',
					'value' => @$useremergencycontact[0]['Useremergencycontact']['useremergencycontactrelationship']
				)
			);
			echo $this->Form->input(
				"Useremergencycontact.useremergencycontacthomephone",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Home Telephone'),
					'tabindex'=>6,
					'placeholder' => __("Home Telephone"),
					'class'=> 'form-control',
					'value' => @$useremergencycontact[0]['Useremergencycontact']['useremergencycontacthomephone']
				)
			);
			echo $this->Form->input(
				"Useremergencycontact.useremergencycontactmobilephone",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Mobile'),
					'tabindex'=>7,
					'placeholder' => __("Mobile"),
					'class'=> 'form-control',
					'style' => '',
					'value' => @$useremergencycontact[0]['Useremergencycontact']['useremergencycontactmobilephone']
				)
			);
			echo $this->Form->input(
				"Useremergencycontact.useremergencycontactofficephone",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Work Telephone'),
					'tabindex'=>8,
					'placeholder' => __("Work Telephone"),
					'class'=> 'form-control',
					'style' => '',
					'value' => @$useremergencycontact[0]['Useremergencycontact']['useremergencycontactofficephone']
				)
			);
			echo"<div class=\"form-group\">";
				echo"<label>isActive?</label><br>";
				echo $this->Form->radio(
					'Useremergencycontact.useremergencycontactisactive', 
					$options,
					$attributes
				);
			echo"</div>";
			echo $Utilitys->allformbutton('',$this->request["controller"],$page='useremergencycontact');
		echo"</div>";
	echo"</div>";
echo $this->Form->end();
echo"
<script>
			$(document).ready(function() {
				$(\"#UsersUseremergencycontactinsertupdateactionForm\").validationEngine();
				$(\"#InstituteDivisionid\").change(function(){
					var InstituteDivisionid = $(this).val();
					$(\"#locationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."institutes/districbydividion',
						{InstituteDivisionid:InstituteDivisionid},
						function(result) {
							$(\"#InstituteDistrictid\").html(result);
							$(\"#locationsarchloading\").html('');
						}
					);
				});
				$(\"#InstituteDistrictid\").change(function(){
					var InstituteDistrictid = $(this).val();
					$(\"#locationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."institutes/upazilabydistric',
						{InstituteDistrictid:InstituteDistrictid},
						function(result) {
							$(\"#InstituteUpazilaid\").html(result);
							$(\"#locationsarchloading\").html('');
						}
					);
				});
				$(\"#InstituteUpazilaid\").change(function(){
					var InstituteUpazilaid = $(this).val();
					$(\"#locationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."institutes/unionparishadbyupazila',
						{InstituteUpazilaid:InstituteUpazilaid},
						function(result) {
							$(\"#InstituteUnionparishadid\").html(result);
							$(\"#locationsarchloading\").html('');
						}
					);
				});
				$(\"#InstituteUnionparishadid\").change(function(){
					var InstituteUnionparishadid = $(this).val();
					$(\"#locationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."institutes/paraareabyunionparishad',
						{InstituteUnionparishadid:InstituteUnionparishadid},
						function(result) {
							$(\"#InstituteParaid\").html(result);
							$(\"#locationsarchloading\").html('');
						}
					);
				});
				$(\"#UseremergencycontactDepartmentId\").change(function(){
					var UseremergencycontactDepartmentId = $(this).val();
					$(\"#salaryuserchangediv\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/useremergencycontactlistbydepartmentid',
						{UseremergencycontactDepartmentId:UseremergencycontactDepartmentId},
						function(result) {
							$(\"#salaryuserchangediv\").html(result);
						}
					);
				});
			});
		</script>
	";
?>