<?php
App::import('Controller', 'Logins');
App::import('Controller', 'Transactions');
App::import('Controller', 'Users');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');

class BanksController extends AppController {
	public $name = 'Banks';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'JqueryValidation');
	public $components = array('RequestHandler','Session');
	public $uses = array('Banktype', 'Bank', 'Bankbranch', 'Bankaccounttype', 'Bankaccount', 'Bankchequebook', 'Banktransaction', 'Company', 'Group', 'User', 'Usergroup', 'Usertransaction');

	var $Transactions;
	var $Users;

	var $Logins;
	/*public function index() {
		$this->redirect("login");
	}*/
	function beforeFilter(){
		$this->Logins =& new LoginsController;
		$this->Logins->constructClasses();
		$this->Logins->__validateLoginStatus();
		//$Logins = new LoginsController;
		//$this->set("Logins',$Logins);
		$this->Transactions =& new TransactionsController;
		$this->Transactions->constructClasses();
		$this->Users =& new UsersController;
		$this->Users->constructClasses();


		$Utilitys = new UtilitysController;
		$this->set("Utilitys",$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	public  function beforeRender(){
		$this->response->disableCache();//prevent useless warnings for Ajax
		if($this->RequestHandler->isAjax()){
			Configure::write("debug", 0);
		}
	}

	//Start Bank//
	public function bankmanage(){
		$this->layout='default';
		$this->set("title_for_layou", __("Bank")." | ".__(Configure::read("site_name")));

		$this->paginate = array(
			'fields' => 'Bank.*,Banktype.*',
			'conditions'  =>array('Bank.bankisactive IN (0,1)'),
			'order'  =>'Bank.bankname ASC',
			'limit' => 20
		);
		$bank = $this->paginate('Bank');
		$this->set('bank',$bank);
	}

	public function banksearchbytext($Searchtext=null){
		$this->layout='ajax';
		$Searchtext = @$this->request->data['Searchtext'];
		$bank_id = array();
		if(!empty($Searchtext) && $Searchtext!=''):
			$bank_id[] = array(
				'OR' => array(
					'Bank.bankname LIKE ' => '%'.$Searchtext.'%',
					'Bank.banknamebn LIKE ' => '%'.$Searchtext.'%'
				)
			);
		endif;
		$bank_id[] = array('Bank.bankisactive IN (0,1)');

		$this->paginate = array(
			'fields' => 'Bank.*,Banktype.*',
			'order'  =>'Bank.bankname ASC',
			"conditions"=>$bank_id,
			'limit' => 20
		);
		$bank = $this->paginate('Bank');
		$this->set('bank',$bank);
	}

	public function bankdetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$bank=$this->Bank->find('all',array("fields" => "Bank.*","conditions"=>array('Bank.id'=>$id,'Bank.bankuuid'=>$uuid)));
		$this->set('bank',$bank);
	}

	public function bankinsertupdate($id=null,$uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Bank').' | '.__(Configure::read('site_name')));
		$banktype=$this->Banktype->find('list',array("fields" => array("id","banktype_name")));
		$this->set('banktype',$banktype);
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
			$bank=$this->Bank->find('all',array("fields" => "Bank.*","conditions"=>array('Bank.id'=>$id,'Bank.bankuuid'=>$uuid)));
			$this->set('bank',$bank);
		endif;
	}

	public function  bankinsertupdateaction($id=null,$uuid=null){
		$userID = $_SESSION["User"]["id"];
		$id = @$this->request->data['Banks']['id'];
		$uuid = @$this->request->data['Banks']['bankuuid'];
		$banktype_id = $this->request->data['Bank']['banktype_id'];
		$bankname = $this->request->data['Bank']['bankname'];
		$banknamebn = $this->request->data['Bank']['banknamebn'];
		$countbank=$this->Bank->find('all',array("fields" => "Bank.*","conditions"=>array('Bank.banktype_id'=>$banktype_id,'Bank.bankname'=>$bankname,'Bank.banknamebn'=>$banknamebn)));
		//echo count($countbankaccount);
		//pr($_POST);exit();
		if(count($countbank)>0):
			$this->Session->setFlash(__("This Bank Name Already Exists"), 'default', array('class' => 'alert alert-danger'));
			$this->redirect("/banks/bankinsertupdate");
		else:
			if ($this->request->is('post')) {
				if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
					$this->request->data['Bank']['bankupdateid']=$userID;
					$this->request->data['Bank']['bankupdatedate']=date('Y-m-d H:i:s');
					$message = __('Update Successfully.');
					$this->Bank->id = $id;
				else:
					$this->request->data['Bank']['bankinsertid']=$userID;
					$this->request->data['Bank']['bankinsertdate']=date('Y-m-d H:i:s');
					$this->request->data['Bank']['bankuuid']=String::uuid();
					$message = __('Save Successfully.');
				endif;
				if ($this->Bank->save($this->request->data, array('validate' => 'only'))) {
					$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
					$this->redirect("/banks/bankmanage");
				}else{
					$errors = $this->Bank->invalidFields();
					$this->set('alert alert-danger', $this->Bank->invalidFields());
					$this->redirect("/banks/bankinsertupdate");
				}
			}
		endif;
	}

	public function bankdelete($id=null,$uuid=null){
		$userID = $_SESSION["User"]["id"];
		if ($this->Bank->updateAll(array('bankdeleteid'=>$userID, 'bankdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'bankisactive'=>2), array('AND' => array('Bank.id'=>$id,'Bank.bankuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/banks/bankmanage");
	}

	//End Bank//

	//Start Bank Branch//
	public function bankbranchmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Bank Branch').' | '.__(Configure::read('site_name')));

		$this->paginate = array(
			'fields' => 'Bankbranch.*',
			'conditions' => array('Bankbranch.bankbranchisactive IN (0,1)'),
			'order'  =>'Bankbranch.bankbranchname ASC',
			'limit' => 20
		);
		$bankbranch = $this->paginate('Bankbranch');
		$this->set('bankbranch',$bankbranch);
	}

	public function bankbranchsearchbytext($Searchtext=null){
		$this->layout='ajax';

		$Searchtext = @$this->request->data['Searchtext'];
		$bankbranch_id = array();
		if(!empty($Searchtext) && $Searchtext!=''):
			$bankbranch_id[] = array(
				'OR' => array(
					'Bankbranch.bankbranchname LIKE ' => '%'.$Searchtext.'%',
					'Bankbranch.bankbranchnamebn LIKE ' => '%'.$Searchtext.'%'
				)
			);
		endif;
		$bankbranch_id[] = array('Bankbranch.bankbranchisactive IN (0,1)');

		$this->paginate = array(
			'fields' => 'Bankbranch.*',
			'order'  =>'Bankbranch.bankbranchname ASC',
			"conditions"=>$bankbranch_id,
			'limit' => 20
		);
		$bankbranch = $this->paginate('Bankbranch');
		$this->set('bankbranch',$bankbranch);
	}

	public function bankbranchdetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$bankbranch=$this->Bankbranch->find('all',array("fields" => "Bankbranch.*","conditions"=>array('Bankbranch.id'=>$id,'Bankbranch.bankbranchuuid'=>$uuid)));
		$this->set('bankbranch',$bankbranch);
	}

	public function bankbranchinsertupdate($id=null,$uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Bank Branch').' | '.__(Configure::read('site_name')));
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
			$bankbranch=$this->Bankbranch->find('all',array("fields" => "Bankbranch.*","conditions"=>array('Bankbranch.id'=>$id,'Bankbranch.bankbranchuuid'=>$uuid)));
			$this->set('bankbranch',$bankbranch);
		endif;
	}

	public function  bankbranchinsertupdateaction($id=null,$uuid=null){
		$userID = $_SESSION["User"]["id"];
		$id = @$this->request->data['Banks']['id'];
		$uuid = @$this->request->data['Banks']['bankbranchuuid'];
		$bankbranchname = $this->request->data['Bankbranch']['bankbranchname'];
		$bankbranchnamebn = $this->request->data['Bankbranch']['bankbranchnamebn'];
		$countbankname=$this->Bankbranch->find('all',array("fields" => "Bankbranch.*","conditions"=>array('Bankbranch.bankbranchname'=>$bankbranchname,'Bankbranch.bankbranchnamebn'=>$bankbranchnamebn)));
		//echo count($countbankaccount);
		//pr($_POST);exit();
		if(count($countbankname)>0):
			$this->Session->setFlash(__("This Bank Branch Name Already Exists"), 'default', array('class' => 'alert alert-warning'));
			$this->redirect("/banks/bankbranchinsertupdate");
		else:
			if ($this->request->is('post')) {
				if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
					$this->request->data['Bankbranch']['bankbranchupdateid']=$userID;
					$this->request->data['Bankbranch']['bankbranchupdatedate']=date('Y-m-d H:i:s');
					$message = __('Update Successfully.');
					$this->Bankbranch->id = $id;
				else:
					$this->request->data['Bankbranch']['bankbranchinsertid']=$userID;
					$this->request->data['Bankbranch']['bankbranchinsertdate']=date('Y-m-d H:i:s');
					$this->request->data['Bankbranch']['bankbranchuuid']=String::uuid();
					$message = __('Save Successfully.');
				endif;

				if ($this->Bankbranch->save($this->request->data, array('validate' => 'only'))) {
					$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
					$this->redirect("/banks/bankbranchmanage");
				}else{
					$errors = $this->Bankbranch->invalidFields();
					$this->set('alert alert-danger', $this->Bankbranch->invalidFields());
					$this->redirect("/banks/bankbranchinsertupdate");
				}
			}
		endif;
	}

	public function bankbranchdelete($id=null,$uuid=null){
		$userID = $_SESSION["User"]["id"];
		if ($this->Bankbranch->updateAll(array('bankbranchdeleteid'=>$userID, 'bankbranchdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'bankbranchisactive'=>2), array('AND' => array('Bankbranch.id'=>$id,'Bankbranch.bankbranchuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/banks/bankbranchmanage");
	}

	//End Bank Branch//

	//Start Bank Account Type//
	public function bankaccounttypemanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Bank Account Type').' | '.__(Configure::read('site_name')));

		$this->paginate = array(
			'fields' => 'Bankaccounttype.*',
			'conditions' => array('Bankaccounttype.bankaccounttypeisactive IN (0,1)'),
			'order'  =>'Bankaccounttype.bankaccounttypename ASC',
			'limit' => 20
		);
		$bankaccounttype = $this->paginate('Bankaccounttype');
		$this->set('bankaccounttype',$bankaccounttype);
	}

	public function bankaccounttypesearchbytext($Searchtext=null){
		$this->layout='ajax';

		$Searchtext = @$this->request->data['Searchtext'];
		$bankaccounttype_id = array();
		if(!empty($Searchtext) && $Searchtext!=''):
			$bankaccounttype_id[] = array(
				'OR' => array(
					'Bankaccounttype.bankaccounttypename LIKE ' => '%'.$Searchtext.'%',
					'Bankaccounttype.bankaccounttypenamebn LIKE ' => '%'.$Searchtext.'%'
				)
			);
		endif;
		$bankaccounttype_id[] = array('Bankaccounttype.bankaccounttypeisactive IN (0,1)');

		$this->paginate = array(
			'fields' => 'Bankaccounttype.*',
			'order'  =>'Bankaccounttype.bankaccounttypename ASC',
			"conditions"=>$bankaccounttype_id,
			'limit' => 20
		);
		$bankaccounttype = $this->paginate('Bankaccounttype');
		$this->set('bankaccounttype',$bankaccounttype);
	}

	public function bankaccounttypedetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$bankaccounttype=$this->Bankaccounttype->find('all',array("fields" => "Bankaccounttype.*","conditions"=>array('Bankaccounttype.id'=>$id,'Bankaccounttype.bankaccounttypeuuid'=>$uuid)));
		$this->set('bankaccounttype',$bankaccounttype);
	}

	public function bankaccounttypeinsertupdate($id=null,$uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Bank Account Type').' | '.__(Configure::read('site_name')));
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
			$bankaccounttype=$this->Bankaccounttype->find('all',array("fields" => "Bankaccounttype.*","conditions"=>array('Bankaccounttype.id'=>$id,'Bankaccounttype.bankaccounttypeuuid'=>$uuid)));
			$this->set('bankaccounttype',$bankaccounttype);
		endif;
	}

	public function  bankaccounttypeinsertupdateaction($id=null,$uuid=null){
		$userID = $_SESSION["User"]["id"];
		$id = @$this->request->data['Banks']['id'];
		$uuid = @$this->request->data['Banks']['bankaccounttypeuuid'];
		$bankaccounttypename = $this->request->data['Bankaccounttype']['bankaccounttypename'];
		$bankaccounttypenamebn = $this->request->data['Bankaccounttype']['bankaccounttypenamebn'];
		$countbankaccounttypename=$this->Bankaccounttype->find('all',array("fields" => "Bankaccounttype.*","conditions"=>array('Bankaccounttype.bankaccounttypename'=>$bankaccounttypename,'Bankaccounttype.bankaccounttypenamebn'=>$bankaccounttypenamebn)));
		//echo count($countbankaccounttypename);
		//pr($_POST);exit();
		if(count($countbankaccounttypename)>0):
			$this->Session->setFlash(__("This Bank Account Type Name Already Exists"), 'default', array('class' => 'alert alert-warning'));
			$this->redirect("/banks/bankaccounttypeinsertupdate");
		else:
			if ($this->request->is('post')) {
				if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
					$this->request->data['Bankaccounttype']['bankaccounttypeupdateid']=$userID;
					$this->request->data['Bankaccounttype']['bankaccounttypeupdatedate']=date('Y-m-d H:i:s');
					$message = __('Update Successfully.');
					$this->Bankaccounttype->id = $id;
				else:
					$this->request->data['Bankaccounttype']['bankaccounttypeinsertid']=$userID;
					$this->request->data['Bankaccounttype']['bankaccounttypeinsertdate']=date('Y-m-d H:i:s');
					$this->request->data['Bankaccounttype']['bankaccounttypeuuid']=String::uuid();
					$message = __('Save Successfully.');
				endif;
				if ($this->Bankaccounttype->save($this->request->data, array('validate' => 'only'))) {
					$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
					$this->redirect("/banks/bankaccounttypemanage");
				}else{
					$errors = $this->Bankaccounttype->invalidFields();
					$this->set('alert alert-danger', $this->Bankaccounttype->invalidFields());
					$this->redirect("/banks/bankaccounttypeinsertupdate");
				}
			}
		endif;
	}

	public function bankaccounttypedelete($id=null,$uuid=null){
		$userID = $_SESSION["User"]["id"];
		if ($this->Bankaccounttype->updateAll(array('bankaccounttypedeleteid'=>$userID, 'bankaccounttypedeletedate'=>"'".date('Y-m-d H:i:s')."'", 'bankaccounttypeisactive'=>2), array('AND' => array('Bankaccounttype.id'=>$id,'Bankaccounttype.bankaccounttypeuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/banks/bankaccounttypemanage");
	}

	//End Bank Account Type//

	//Start Bank Account//
	public function bankaccountmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Bank Account').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$bankchequebook_id = array('');
		$branch_id = array('');
		$bankaccount_id = array('');
		$bankaccount_companyid = array();

		if($group_id==1):
			$bankaccount_id[] = array('');
		elseif($group_id==2):
			$bankaccount_id[] = array('Company.id'=>$company_id);
			$bankaccount_companyid[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.bankaccountisactive IN (0,1)');
		else:
			$bankaccount_id[] = array('Company.id'=>$company_id);
			$branch_id[] = array('Branch.id'=>$branch_id);
			$bankaccount_id[] = array('Bankaccount.bankaccountisactive IN (1)');
		endif;
		$bank=$this->Bank->find('list',array("fields" => array("id","bank_name")));
		$this->set('bank',$bank);
		$bankbranch=$this->Bankbranch->find('list',array("fields" => array("id","bankbranch_name")));
		$this->set('bankbranch',$bankbranch);
		$bankaccounttype=$this->Bankaccounttype->find('list',array("fields" => array("id","bankaccounttype_name")));
		$this->set('bankaccounttype',$bankaccounttype);
		$bankaccountlist=$this->Bankaccount->find('list',array("fields" => array("id","bankaccount_name"),"conditions"=>$bankaccount_companyid));
		$this->set('bankaccountlist',$bankaccountlist);

		$this->paginate = array(
			'fields' => 'Bankaccount.*',
			"conditions" => array_merge($bankaccount_id,$branch_id),
			'order'  =>'Bankaccount.bankaccountname ASC',
			'limit' => 20
		);
		$bankaccount = $this->paginate('Bankaccount');
		$this->set('bankaccount',$bankaccount);
	}

	public function bankaccountsearchbytext($Searchtext=null){
		$this->layout='ajax';

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$Searchtext = @$this->request->data['Searchtext'];
		$BankaccountBankId = @$this->request->data['BankaccountBankId'];
		$BankaccountBankbranchId = @$this->request->data['BankaccountBankbranchId'];
		$BankaccountBankaccounttypeId = @$this->request->data['BankaccountBankaccounttypeId'];
		$BankaccountBankaccountId = @$this->request->data['BankaccountBankaccountId'];
		$bankaccount_id = array();
		if($group_id==1):
			$bankaccount_id[] = array('');
		elseif($group_id==2):
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.bankaccountisactive IN (0,1)');
		else:
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.branch_id'=>$branch_id);
			$bankaccount_id[] = array('Bankaccount.bankaccountisactive IN (1)');
		endif;
		if(!empty($BankaccountBankId) && $BankaccountBankId!=''):
			$bankaccount_id[] = array('Bankaccount.bank_id'=>$BankaccountBankId);
		endif;
		if(!empty($BankaccountBankbranchId) && $BankaccountBankbranchId!=''):
			$bankaccount_id[] = array('Bankaccount.bankbranch_id'=>$BankaccountBankbranchId);
		endif;
		if(!empty($BankaccountBankaccounttypeId) && $BankaccountBankaccounttypeId!=''):
			$bankaccount_id[] = array('Bankaccount.bankaccounttype_id'=>$BankaccountBankaccounttypeId);
		endif;
		if(!empty($BankaccountBankaccountId) && $BankaccountBankaccountId!=''):
			$bankaccount_id[] = array('Bankaccount.id'=>$BankaccountBankaccountId);
		endif;
		if(!empty($Searchtext) && $Searchtext!=''):
			$bankaccount_id[] = array(
				'OR' => array(
					'Bankaccount.bankaccountname LIKE ' => '%'.$Searchtext.'%',
					'Bankaccount.bankaccountnamebn LIKE ' => '%'.$Searchtext.'%',
					'Bankaccount.bankaccountnumber LIKE ' => '%'.$Searchtext.'%',
					'Bankaccount.bank_name LIKE ' => '%'.$Searchtext.'%',
					'Bankaccount.bankbranch_name LIKE ' => '%'.$Searchtext.'%',
					'Bankaccount.bankaccounttype_name LIKE ' => '%'.$Searchtext.'%'
				)
			);
		else:
			$bankaccount_id[] = array();
		endif;

		$this->paginate = array(
			'fields' => 'Bankaccount.*',
			'order'  =>'Bankaccount.bankaccountname ASC',
			"conditions"=>$bankaccount_id,
			'limit' => 20
		);
		$bankaccount = $this->paginate('Bankaccount');
		$this->set('bankaccount',$bankaccount);
	}

	public function bankaccountdetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$bankaccount=$this->Bankaccount->find('all',array("fields" => "Bankaccount.*","conditions"=>array('Bankaccount.id'=>$id,'Bankaccount.bankaccountuuid'=>$uuid)));
		$this->set('bankaccount',$bankaccount);
	}

	public function bankaccountinsertupdate($id=null,$uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Bank Account').' | '.__(Configure::read('site_name')));
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];
		$bankaccount_id = array();
		$bank_id = array();
		$bankbaranch_id = array();
		$bankaccounttype_id = array();
		if($group_id==1):
		elseif($group_id==2):
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.bankaccountisactive IN (0,1)');
			$bank_id[] = array('Bank.bankisactive IN (0,1)');
			$bankbaranch_id[] = array('Bankbranch.bankbranchisactive IN (0,1)');
			$bankaccounttype_id[] = array('Bankaccounttype.bankaccounttypeisactive IN (0,1)');
		else:
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.branch_id'=>$branch_id);
			$bankaccount_id[] = array('Bankaccount.bankaccountisactive IN (1)');
			$bank_id[] = array('Bank.bankisactive IN (1)');
			$bankbaranch_id[] = array('Bankbranch.bankbranchisactive IN (1)');
			$bankaccounttype_id[] = array('Bankaccounttype.bankaccounttypeisactive IN (1)');
		endif;
		$bank=$this->Bank->find(
			'list',
			array(
				"fields" => array("id","bank_name"),
				"conditions" => $bank_id
			)
		);
		$this->set('bank',$bank);
		$bankbranch=$this->Bankbranch->find(
			'list',
			array(
				"fields" => array("id","bankbranch_name"),
				"conditions" => $bankbaranch_id
			)
		);
		$this->set('bankbranch',$bankbranch);
		$bankaccounttype=$this->Bankaccounttype->find(
			'list',
			array(
				"fields" => array("id","bankaccounttype_name"),
				"conditions" => $bankaccounttype_id
			)
		);
		$this->set('bankaccounttype',$bankaccounttype);
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
			$bankaccount=$this->Bankaccount->find('all',array("fields" => "Bankaccount.*","conditions"=>array('Bankaccount.id'=>$id,'Bankaccount.bankaccountuuid'=>$uuid)));
			$this->set('bankaccount',$bankaccount);
		endif;
	}

	public function  bankaccountinsertupdateaction($id=null,$uuid=null){
		$company_id = $_SESSION["User"]["company_id"];
		$branch_id = $_SESSION["User"]["branch_id"];
		$userID = $_SESSION["User"]["id"];
		$id = @$this->request->data['Banks']['id'];
		$uuid = @$this->request->data['Banks']['bankaccountuuid'];
		$bank_id = $this->request->data['Bankaccount']['bank_id'];
		$bankbranch_id = $this->request->data['Bankaccount']['bankbranch_id'];
		$bankaccounttype_id = $this->request->data['Bankaccount']['bankaccounttype_id'];
		$bankaccountname = $this->request->data['Bankaccount']['bankaccountname'];
		$bankaccountnamebn = $this->request->data['Bankaccount']['bankaccountnamebn'];
		$bankaccountnumber = $this->request->data['Bankaccount']['bankaccountnumber'];
		$countbankaccount=$this->Bankaccount->find('all',array("fields" => "Bankaccount.*","conditions"=>array('Bankaccount.bank_id'=>$bank_id,'Bankaccount.bankbranch_id'=>$bankbranch_id,'Bankaccount.bankaccounttype_id'=>$bankaccounttype_id,'Bankaccount.bankaccountname'=>$bankaccountname,'Bankaccount.bankaccountnamebn'=>$bankaccountnamebn,'Bankaccount.bankaccountnumber'=>$bankaccountnumber)));
		//echo count($countbankaccount);
		//pr($_POST);exit();
		if(count($countbankaccount)>0):
			$this->Session->setFlash(__("This Bank Account Already Exists"), 'default', array('class' => 'alert alert-warning'));
			$this->redirect("/banks/bankaccountinsertupdate");
		else:
			if ($this->request->is('post')) {
				if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
					$this->request->data['Bankaccount']['bankaccountupdateid']=$userID;
					$this->request->data['Bankaccount']['bankaccountupdatedate']=date('Y-m-d H:i:s');
					$message = __('Update Successfully.');
					$this->Bankaccount->id = $id;
				else:
					$this->request->data['Bankaccount']['company_id']=$company_id;
					$this->request->data['Bankaccount']['branch_id']=$branch_id;
					$this->request->data['Bankaccount']['bankaccountinsertid']=$userID;
					$this->request->data['Bankaccount']['bankaccountinsertdate']=date('Y-m-d H:i:s');
					$this->request->data['Bankaccount']['bankaccountuuid']=String::uuid();
					$message = __('Save Successfully.');
				endif;
				if ($this->Bankaccount->save($this->request->data, array('validate' => 'only'))) {
					$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
					$this->redirect("/banks/bankaccountmanage");
				}else{
					$errors = $this->Bankaccount->invalidFields();
					$this->set('error', $this->Bankaccount->invalidFields());
					$this->redirect("/banks/bankaccountinsertupdate");
				}
			}
		endif;
	}

	public function bankaccountdelete($id=null,$uuid=null){
		$userID = $_SESSION["User"]["id"];
		if ($this->Bankaccount->updateAll(array('bankaccountdeleteid'=>$userID, 'bankaccountdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'bankaccountisactive'=>2), array('AND' => array('Bankaccount.id'=>$id,'Bankaccount.bankaccountuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/banks/bankaccountmanage");
	}

	//End Bank Account//

	//Start Bank Cheque Book//

	public function bankchequebookmanage(){
		//$Logins->__validateLoginStatus();
		$this->layout='default';
		$this->set('title_for_layout', __('Bank Cheque Book').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$bankchequebook_id = array('');
		$bankaccount_id = array('');
		if($group_id==1):
			$bankchequebook_id[] = array('');
		elseif($group_id==2):
			$bankchequebook_id[] = array('Company.id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.bankaccountisactive IN (0,1)');
		else:
			$bankchequebook_id[] = array('Company.id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.bankaccountisactive IN (1)');
		endif;

		$bank=$this->Bank->find('list',array("fields" => array("id","bank_name")));
		$this->set('bank',$bank);
		$bankbranch=$this->Bankbranch->find('list',array("fields" => array("id","bankbranch_name")));
		$this->set('bankbranch',$bankbranch);
		$bankaccounttype=$this->Bankaccounttype->find('list',array("fields" => array("id","bankaccounttype_name")));
		$this->set('bankaccounttype',$bankaccounttype);
		$bankaccount=$this->Bankaccount->find(
			'list',
			array(
				"fields" => array("id","bankaccount_name"),
				"conditions" => $bankaccount_id
			)
		);
		$this->set('bankaccount',$bankaccount);
		$this->paginate = array(
			'fields' => 'Bankchequebook.*',
			'conditions' => $bankchequebook_id,
			'order'  =>'Bankchequebook.bankchequebookseries ASC',
			'limit' => 20
		);
		$bankchequebook = $this->paginate('Bankchequebook');
		$this->set('bankchequebook',$bankchequebook);
	}

	public function bankchequebooksearchbytext($Searchtext=null){
		$this->layout='ajax';

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$Searchtext = @$this->request->data['Searchtext'];
		$BankchequebookBankId = @$this->request->data['BankchequebookBankId'];
		$BankchequebookBankbranchId = @$this->request->data['BankchequebookBankbranchId'];
		$BankchequebookBankaccounttypeId = @$this->request->data['BankchequebookBankaccounttypeId'];
		$BankchequebookBankaccountId = @$this->request->data['BankchequebookBankaccountId'];
		$bankchequebook_id = array();
		$bankaccount_id = array();
		if($group_id==1):
			$bankchequebook_id[] = array('');
		elseif($group_id==2):
			$bankchequebook_id[] = array('Bankchequebook.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.bankaccountisactive IN (0,1)');
		else:
			$bankchequebook_id[] = array('Bankchequebook.company_id'=>$company_id);
			$bankaccount_companyid[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_branchid[] = array('Bankaccount.branch_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.bankaccountisactive IN (1)');
		endif;
		if(!empty($BankchequebookBankId) && $BankchequebookBankId!=''):
			$bankchequebook_id[] = array('Bankchequebook.bank_id'=>$BankchequebookBankId);
		endif;
		if(!empty($BankchequebookBankbranchId) && $BankchequebookBankbranchId!=''):
			$bankchequebook_id[] = array('Bankchequebook.bankbranch_id'=>$BankchequebookBankbranchId);
		endif;
		if(!empty($BankchequebookBankaccounttypeId) && $BankchequebookBankaccounttypeId!=''):
			$bankchequebook_id[] = array('Bankchequebook.bankaccounttype_id'=>$BankchequebookBankaccounttypeId);
		endif;
		if(!empty($BankchequebookBankaccountId) && $BankchequebookBankaccountId!=''):
			$bankchequebook_id[] = array('Bankchequebook.bankaccount_id'=>$BankchequebookBankaccountId);
		endif;
		if(!empty($Searchtext) && $Searchtext!=''):
			$bankchequebook_id[] = array(
				'OR' => array(
					'Bankchequebook.bankchequebookseries LIKE ' => '%'.$Searchtext.'%',
					'Bankchequebook.bankchequebooksequencyfrom LIKE ' => '%'.$Searchtext.'%',
					'Bankchequebook.bankchequebooksequencyto LIKE ' => '%'.$Searchtext.'%',
					'Bank.bankname LIKE ' => '%'.$Searchtext.'%',
					'Bank.banknamebn LIKE ' => '%'.$Searchtext.'%',
					'Bankbranch.bankbranchname LIKE ' => '%'.$Searchtext.'%',
					'Bankbranch.bankbranchnamebn LIKE ' => '%'.$Searchtext.'%',
					'Bankaccounttype.bankaccounttypename LIKE ' => '%'.$Searchtext.'%',
					'Bankaccounttype.bankaccounttypenamebn LIKE ' => '%'.$Searchtext.'%',
					'Bankaccount.bankaccountname LIKE ' => '%'.$Searchtext.'%',
					'Bankaccount.bankaccountnamebn LIKE ' => '%'.$Searchtext.'%'
				)
			);
		endif;

		$this->paginate = array(
			'fields' => 'Bankchequebook.*',
			'order'  =>'Bankchequebook.bankchequebookseries ASC',
			"conditions"=>$bankchequebook_id,
			'limit' => 20
		);
		$bankchequebook = $this->paginate('Bankchequebook');
		$this->set('bankchequebook',$bankchequebook);
	}

	public function bankchequebookdetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$bankchequebook=$this->Bankchequebook->find('all',array("fields" => "Bankchequebook.*","conditions"=>array('Bankchequebook.id'=>$id,'Bankchequebook.bankchequebookuuid'=>$uuid)));
		$this->set('bankchequebook',$bankchequebook);
	}

	public function bankchequebookinsertupdate($id=null,$uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Bank Cheque Book').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$bankchequebook_id = array('');
		$bankaccount_companyid = array();
		$bankaccount_branchid = array();
		if($group_id==1):
			$bankchequebook_id[] = array('');
		elseif($group_id==2):
			$bankchequebook_id[] = array('Bankchequebook.company_id'=>$company_id);
			$bankaccount_companyid[] = array('Bankaccount.company_id'=>$company_id);
		else:
			$bankchequebook_id[] = array('Bankchequebook.company_id'=>$company_id);
			$bankchequebook_id[] = array('Bankchequebook.branch_id'=>$branch_id);
			$bankaccount_companyid[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_branchid[] = array('Bankaccount.branch_id'=>$company_id);
		endif;
		$bank=$this->Bank->find('list',array("fields" => array("id","bank_name")));
		$this->set('bank',$bank);
		$bankbranch=$this->Bankbranch->find('list',array("fields" => array("id","bankbranch_name")));
		$this->set('bankbranch',$bankbranch);
		$bankaccounttype=$this->Bankaccounttype->find('list',array("fields" => array("id","bankaccounttype_name")));
		$this->set('bankaccounttype',$bankaccounttype);
		$bankaccount=$this->Bankaccount->find('list',array("fields" => array("id","bankaccount_name"),'conditions'=>array_merge($bankaccount_companyid,$bankaccount_branchid)));
		$this->set('bankaccount',$bankaccount);
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
			$bankchequebook=$this->Bankchequebook->find('all',array("fields" => "Bankchequebook.*","conditions"=>array('Bankchequebook.id'=>$id,'Bankchequebook.bankchequebookuuid'=>$uuid)));
			$this->set('bankchequebook',$bankchequebook);
		endif;
	}

	public function  bankchequebookinsertupdateaction($id=null,$uuid=null){
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$userID = $_SESSION["User"]["id"];
		$id = @$this->request->data['Banks']['id'];
		$uuid = @$this->request->data['Banks']['bankchequebookuuid'];
		$bankchequebookseries = $this->request->data['Bankchequebook']['bankchequebookseries'];
		$bankchequebooksequencyfrom = $this->request->data['Bankchequebook']['bankchequebooksequencyfrom'];
		$bankchequebooksequencyto = $this->request->data['Bankchequebook']['bankchequebooksequencyto'];
		$countbankchequebook=$this->Bankchequebook->find('all',array("fields" => "Bankchequebook.*","conditions"=>array('Bankchequebook.bankchequebookseries'=>$bankchequebookseries,'Bankchequebook.bankchequebooksequencyfrom'=>$bankchequebooksequencyfrom,'Bankchequebook.bankchequebooksequencyto'=>$bankchequebooksequencyto)));
		if(count($countbankchequebook)>0):
			$this->Session->setFlash(__("This Bank Cheque Book Series & Sequency Already Exists"), 'default', array('class' => 'alert alert-warning'));
			$this->redirect("/banks/bankchequebookinsertupdate");
		else:
			if ($this->request->is('post')) {
				if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
					$this->request->data['Bankchequebook']['bankchequebookupdateid']=$userID;
					$this->request->data['Bankchequebook']['bankchequebookupdatedate']=date('Y-m-d H:i:s');
					$message = __('Update Successfully.');
					$this->Bankchequebook->id = $id;
					$banktransaction=$this->Banktransaction->find(
						'count',
						array(
							"conditions" => array(
								"bankchequebook_id"=>$id,
								"banktransactionisactive"=>1
							)
						)
					);
				else:
					$this->request->data['Bankchequebook']['company_id']=$company_id;
					$this->request->data['Bankchequebook']['branch_id']=$branch_id;
					$this->request->data['Bankchequebook']['bankchequebookinsertid']=$userID;
					$this->request->data['Bankchequebook']['bankchequebookinsertdate']=date('Y-m-d H:i:s');
					$this->request->data['Bankchequebook']['bankchequebookuuid']=String::uuid();
					$message = __('Save Successfully.');
					$banktransaction=0;
				endif;
				$sequencynumberfrom=$this->request->data['Bankchequebook']['bankchequebooksequencyfrom'];
				$sequencynumberto=$this->request->data['Bankchequebook']['bankchequebooksequencyto'];
				if($banktransaction>0):
					$this->Session->setFlash(__("You Cannot modify any values"), 'default', array('class' => 'denger'));
					$this->redirect("/banks/bankchequebookmanage");
				else:
					if ($this->Bankchequebook->save($this->request->data, array('validate' => 'only'))) {
						if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
							$bankchequebook_id=$id;
						else:
							$bankchequebook_id = $this->Bankchequebook->getLastInsertID();
						endif;

						$this->set('banktransaction',$banktransaction);
						$this->Banktransaction->deleteAll(array('bankchequebook_id' => $bankchequebook_id));
						for($sequencynumber=$sequencynumberfrom; $sequencynumber<=$sequencynumberto; $sequencynumber++):
							$this->request->data['Banktransaction']['banktransactionuuid']=String::uuid();
							$this->request->data['Banktransaction']['company_id']=$company_id;
							$this->request->data['Banktransaction']['branch_id']=$branch_id;
							$this->request->data['Banktransaction']['bank_id']=$this->request->data['Bankchequebook']['bank_id'];
							$this->request->data['Banktransaction']['bankbranch_id']=$this->request->data['Bankchequebook']['bankbranch_id'];
							$this->request->data['Banktransaction']['bankaccounttype_id']=$this->request->data['Bankchequebook']['bankaccounttype_id'];
							$this->request->data['Banktransaction']['bankaccount_id']=$this->request->data['Bankchequebook']['bankaccount_id'];
							$this->request->data['Banktransaction']['bankchequebook_id']=$bankchequebook_id;
							$this->request->data['Banktransaction']['banktransactionchequebookseries']=$this->request->data['Bankchequebook']['bankchequebookseries'];
							$this->request->data['Banktransaction']['banktransactionchequesequencynumber']=$sequencynumber;
							$this->Banktransaction->create();
							$this->Banktransaction->save($this->request->data);
						endfor;
						$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
						$this->redirect("/banks/bankchequebookmanage");
					}else{
						$errors = $this->Bankchequebook->invalidFields();
						$this->set('alert alert-danger', $this->Bankchequebook->invalidFields());
						$this->redirect("/banks/bankchequebookinsertupdate");
					}
				endif;
			}
		endif;
	}

	public function bankchequebookdelete($id=null,$uuid=null){
		$userID = $_SESSION["User"]["id"];
		if ($this->Bankchequebook->updateAll(array('bankchequebookdeleteid'=>$userID, 'bankchequebookdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'bankchequebookisactive'=>2), array('AND' => array('Bankchequebook.id'=>$id,'Bankchequebook.bankchequebookuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/banks/bankchequebookmanage");
	}

	public function listbankbranchbybankid($id=null){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$BankchequebookBankId = @$this->request->data['BankchequebookBankId'];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$bankaccount_id = array();
		if($group_id==1):
			$bankaccount_id[] = array('');
		elseif($group_id==2):
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
		else:
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.branch_id'=>$branch_id);
		endif;
		if(!empty($BankchequebookBankId) && $BankchequebookBankId!=0):
			$bankaccount_id[] = array('Bankaccount.bank_id' => $BankchequebookBankId);
		endif;
		$bankbranc=$this->Bankaccount->find(
			'all',array(
				"fields" => array("Bankaccount.bankbranch_id","bankbranch_name"),
				"conditions" => $bankaccount_id
			)
		);
			echo "<option value=\"0\">Select Branch</option>";
		foreach($bankbranc as $thisbankbranc) {
			echo "<option value=\"".$thisbankbranc['Bankaccount']['bankbranch_id']."\">".$thisbankbranc['Bankaccount']['bankbranch_name']."</option>";
		}
	}

	public function listbankaccounttypebybankbranchid($id=null){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$BankchequebookBankbranchId = @$this->request->data['BankchequebookBankbranchId'];
		$BankchequebookBankId = @$this->request->data['BankchequebookBankId'];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$bankaccount_id = array();
		if($group_id==1):
			$bankaccount_id[] = array('');
		elseif($group_id==2):
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
		else:
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.branch_id'=>$branch_id);
		endif;
		if(!empty($BankchequebookBankId) && $BankchequebookBankId!=0):
			$bankaccount_id[] = array('Bankaccount.bank_id' => $BankchequebookBankId);
		endif;
		if(!empty($BankchequebookBankbranchId) && $BankchequebookBankbranchId!=0):
			$bankaccount_id[] = array('Bankaccount.bankbranch_id' => $BankchequebookBankbranchId);
		endif;
		$bankaccounttype=$this->Bankaccount->find(
			'all',array(
				"fields" => array("Bankaccount.bankaccounttype_id","bankaccounttype_name"),
				"conditions" => $bankaccount_id
			)
		);
			echo "<option value=\"0\">Select Branch</option>";
		foreach($bankaccounttype as $thisbankaccounttype) {
			echo "<option value=\"".$thisbankaccounttype['Bankaccount']['bankaccounttype_id']."\">".$thisbankaccounttype['Bankaccount']['bankaccounttype_name']."</option>";
		}
	}

	public function listbankaccountbybankaccounttypeid($id=null){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$BankchequebookBankbranchId = @$this->request->data['BankchequebookBankbranchId'];
		$BankchequebookBankId = @$this->request->data['BankchequebookBankId'];
		$BankchequebookBankaccounttypeId = @$this->request->data['BankchequebookBankaccounttypeId'];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$bankaccount_id = array();
		if($group_id==1):
			$bankaccount_id[] = array('');
		elseif($group_id==2):
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
		else:
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.branch_id'=>$branch_id);
		endif;

		if(!empty($BankchequebookBankId) && $BankchequebookBankId!=0):
			$bankaccount_id[] = array('Bankaccount.bank_id' => $BankchequebookBankId);
		endif;
		if(!empty($BankchequebookBankbranchId) && $BankchequebookBankbranchId!=0):
			$bankaccount_id[] = array('Bankaccount.bankbranch_id' => $BankchequebookBankbranchId);
		endif;
		if(!empty($BankchequebookBankaccounttypeId) && $BankchequebookBankaccounttypeId!=0):
			$bankaccount_id[] = array('Bankaccount.bankaccounttype_id' => $BankchequebookBankaccounttypeId);
		endif;
		$bankaccount=$this->Bankaccount->find(
			'all',array(
				"fields" => array("Bankaccount.id","bankaccount_name"),
				"conditions" => $bankaccount_id
			)
		);
			echo "<option value=\"0\">Select Branch</option>";
		foreach($bankaccount as $thisbankaccount) {
			echo "<option value=\"".$thisbankaccount['Bankaccount']['id']."\">".$thisbankaccount['Bankaccount']['bankaccount_name']."</option>";
		}
	}

	//End Bank Cheque Book//

	//Start Bank Transaction//

	public function banktransactionmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Bank Transaction').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$banktransaction_id = array();
		$bankaccount_id = array();
		if($group_id==1):
		elseif($group_id==2):
			$banktransaction_id[] = array('Banktransaction.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$banktransaction_id[] = array('Banktransaction.banktransactionisactive IN (0,1)');
		else:
			$banktransaction_id[] = array('Banktransaction.company_id'=>$company_id);
			$banktransaction_id[] = array('Banktransaction.branch_id'=>$branch_id);
			$banktransaction_id[] = array('Banktransaction.banktransactionisactive IN (1)');
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.branch_id'=>$branch_id);
		endif;

		$bank=$this->Bank->find('list',array("fields" => array("id","bank_name")));
		$this->set('bank',$bank);

		$bankaccount=$this->Bankaccount->find(
			'list',
			array(
				"fields" => array("id","bankaccount_name"),
				"conditions" => $bankaccount_id
			)
		);
		$this->set('bankaccount',$bankaccount);

			$banktransaction_id[] = array('Banktransaction.banktransactionisactive'=>1);
		$this->paginate = array(
			'fields' => 'Banktransaction.*',
			'conditions'=>array_merge($banktransaction_id),
			'order'  =>'Banktransaction.transactiondate DESC',
			'limit' => 20
		);
		$banktransaction = $this->paginate('Banktransaction');
		$this->set('banktransaction',$banktransaction);
	}

	public function banktransactionsearchbytext(){
		$this->layout='ajax';

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$Searchtext = @$this->request->data['Searchtext'];
		$BanktransactionBankaccountId = @$this->request->data['BanktransactionBankaccountId'];
		$banktransaction_id = array();
		if($group_id==1):
		elseif($group_id==2):
			$banktransaction_id[] = array('Banktransaction.company_id'=>$company_id);
			$banktransaction_id[] = array('Banktransaction.banktransactionisactive IN (0,1)');
		else:
			$banktransaction_id[] = array('Banktransaction.company_id'=>$company_id);
			$banktransaction_id[] = array('Banktransaction.branch_id'=>$branch_id);
			$banktransaction_id[] = array('Banktransaction.banktransactionisactive IN (1)');
		endif;
		if(!empty($BanktransactionBankaccountId) && $BanktransactionBankaccountId!=0):
			$banktransaction_id[] = array('Banktransaction.bankaccount_id'=>$BanktransactionBankaccountId);
		endif;
		$banktransaction_id[] = array('Banktransaction.banktransactionisactive'=>1);
		if(!empty($Searchtext) && $Searchtext!=''):
			$banktransaction_id[] = array(
				'OR' => array(
					'Banktransaction.banktransactionchequebookseries LIKE ' => '%'.$Searchtext.'%',
					'Banktransaction.banktransactionchequesequencynumber LIKE ' => '%'.$Searchtext.'%',
					'Bankchequebook.bankchequebookseries LIKE ' => '%'.$Searchtext.'%',
					'Bankchequebook.bankchequebooksequencyfrom LIKE ' => '%'.$Searchtext.'%',
					'Bankchequebook.bankchequebooksequencyto LIKE ' => '%'.$Searchtext.'%',
					'Bank.bankname LIKE ' => '%'.$Searchtext.'%',
					'Bank.banknamebn LIKE ' => '%'.$Searchtext.'%',
					'Bankbranch.bankbranchname LIKE ' => '%'.$Searchtext.'%',
					'Bankbranch.bankbranchnamebn LIKE ' => '%'.$Searchtext.'%',
					'Bankaccounttype.bankaccounttypename LIKE ' => '%'.$Searchtext.'%',
					'Bankaccounttype.bankaccounttypenamebn LIKE ' => '%'.$Searchtext.'%',
					'Bankaccount.bankaccountname LIKE ' => '%'.$Searchtext.'%',
					'Bankaccount.bankaccountnamebn LIKE ' => '%'.$Searchtext.'%',
					'Banktransaction.banktransactionothernote LIKE ' => '%'.$Searchtext.'%',
					'User.username LIKE ' => '%'.$Searchtext.'%',
					'User.userfirstname LIKE ' => '%'.$Searchtext.'%',
					'User.usermiddlename LIKE ' => '%'.$Searchtext.'%',
					'User.userlastname LIKE ' => '%'.$Searchtext.'%',
					'Client.username LIKE ' => '%'.$Searchtext.'%',
					'Client.userfirstname LIKE ' => '%'.$Searchtext.'%',
					'Client.usermiddlename LIKE ' => '%'.$Searchtext.'%',
					'Client.userlastname LIKE ' => '%'.$Searchtext.'%',
					'Salesman.username LIKE ' => '%'.$Searchtext.'%',
					'Salesman.userfirstname LIKE ' => '%'.$Searchtext.'%',
					'Salesman.usermiddlename LIKE ' => '%'.$Searchtext.'%',
					'Salesman.userlastname LIKE ' => '%'.$Searchtext.'%',
					'Coa.coaname LIKE ' => '%'.$Searchtext.'%',
					'Coa.coanamebn LIKE ' => '%'.$Searchtext.'%'
				)
			);
		endif;

		$this->paginate = array(
			'fields' => 'Banktransaction.*',
			'conditions'=>array_merge($banktransaction_id),
			'order'  =>'Banktransaction.transactiondate DESC',
			'limit' => 20
		);
		$banktransaction = $this->paginate('Banktransaction');
		$this->set('banktransaction',$banktransaction);
	}

	public function banktransactioninsertupdate($id=null,$uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Bank Transaction').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$banktransaction_id = array();
		$bank_id = array();
		$bankaccount_id = array();
		$bankchequebook_id = array();
		$group_array = array();
		$user_array = array();
		if($group_id==1):
		elseif($group_id==2):
			$banktransaction_id[] = array('Banktransaction.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankchequebook_id[] = array('Bankchequebook.company_id'=>$company_id);
			$bank_id[] = array('Bankaccount.company_id'=>$company_id);
			$group_array[] = array('NOT'=>array('Group.id'=>1));
			$user_array[] = array('NOT'=>array('User.group_id'=>1));
			$user_array[] = array('User.company_id'=>$company_id);
		else:
			$banktransaction_id[] = array('Banktransaction.company_id'=>$company_id);
			$banktransaction_id[] = array('Banktransaction.branch_id'=>$branch_id);
			$bank_id[] = array('Bankaccount.company_id'=>$company_id);
			$bank_id[] = array('Bankaccount.branch_id'=>$branch_id);
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.branch_id'=>$branch_id);
			$bankchequebook_id[] = array('Bankchequebook.company_id'=>$company_id);
			$bankchequebook_id[] = array('Bankchequebook.branch_id'=>$branch_id);
			$group_array[] = array('NOT'=>array('Group.id'=>array(1, 2)));
			$user_array[] = array('NOT'=>array('User.group_id'=>array(1, 2)));
			$user_array[] = array('User.company_id'=>$company_id);
			$user_array[] = array('User.branch_id'=>$branch_id);
		endif;

		$bank=$this->Bankaccount->find('list',array("fields" => array("bank_id","bank_name"),"conditions" => $bank_id, 'recursive'=>1));
		$this->set('bank',$bank);

		$group=$this->Group->find(
			'list',
			array(
				"fields" => array("id","groupname"),
				"conditions" => $group_array
			)
		);
		$this->set('group',$group);

		$user=$this->User->find(
			'list',
			array(
				"fields" => array("id","user_fullname"),
				"conditions" => $user_array
			)
		);
		$this->set('user',$user);

		$bankaccount=$this->Bankaccount->find(
			'list',
			array(
				"fields" => array("id","bankaccount_name"),
				"conditions" => $bankaccount_id
			)
		);
		$this->set('bankaccount',$bankaccount);

		$bankchequebook=$this->Bankchequebook->find(
			'list',
			array(
				"fields" => array("id","bankchequebook_name"),
				"conditions" => $bankchequebook_id
			)
		);
		$this->set('bankchequebook',$bankchequebook);
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
			$banktransaction=$this->Banktransaction->find('all',array("fields" => "Banktransaction.*,User.*,Usergroup.*","conditions"=>array('Banktransaction.id'=>$id,'Banktransaction.banktransactionuuid'=>$uuid)));
			$this->set('banktransaction',$banktransaction);
			$this->set('id',$id);
		endif;
	}

	public function banktransctionuserlistbygroup($groupID=null, $transactionothernote=null){
		$this->layout = 'ajax';
		$GroupId = @$this->request->data['GroupId']?$this->request->data['GroupId']:$groupID;
		$transactionothernote = @$this->request->data['BanktransactionBanktransactionothernote'];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$user_array = array();
		if($group_id==1):
		elseif($group_id==2):
			$user_array[] = array('NOT'=>array('User.group_id'=>1));
			$user_array[] = array('User.company_id'=>$company_id);
			$user_array[] = array('User.company_id'=>$company_id);
			$user_array[] = array('User.userisactive'=>1);
		else:
			$user_array[] = array('NOT'=>array('User.group_id'=>array(1, 2)));
			$user_array[] = array('User.company_id'=>$company_id);
			$user_array[] = array('User.userisactive'=>1);
		endif;
		$this->set('GroupId',$GroupId);
		$user=$this->Usergroup->find(
			'list',
			array(
				"fields" => array("user_id", "user_fullname"),
				"conditions"=>array('Usergroup.group_id'=>$GroupId),
				"recursive"=>1
			)
		);
		/*$user=$this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions"=>array('User.group_id'=>$GroupId,$user_array))
		);*/
		$this->set('user',$user);
		$salesman=$this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions"=>array('User.group_id'=>5,'User.userisactive'=>1)
			)
		);
		$this->set('salesman',$salesman);
		$this->set('transactionothernote',$transactionothernote);
	}

	public function salespersonbyuser($ClientId=null){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$ClientId = @$this->request->data['ClientId'];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$json_array = array();
		$user_array = array();
		if($group_id==1):
		elseif($group_id==2):
			$user_array[] = array('NOT'=>array('User.group_id'=>1));
			$user_array[] = array('User.company_id'=>$company_id);
			$user_array[] = array('User.company_id'=>$company_id);
			$user_array[] = array('User.userisactive'=>1);
		else:
			$user_array[] = array('NOT'=>array('User.group_id'=>array(1, 2)));
			$user_array[] = array('User.company_id'=>$company_id);
			$user_array[] = array('User.userisactive'=>1);
		endif;
		$this->set('ClientId',$ClientId);
		$user=$this->User->find(
			'first',
			array(
				"fields" => array("salespersonid"),
				"conditions"=>array('User.id'=>$ClientId)
			)
		);
		$this->set('user',$user);
		$json_array["usertotalamount"] = $this->Users->usertotalamountbyid($ClientId);
		$json_array["salesperson_id"] = $user["User"]["salespersonid"]?$user["User"]["salespersonid"]:0;
		return json_encode($json_array);
	}

	public function bankchequebookseriesbybank(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$json_array = array();
		$BankId = @$this->request->data['BankaccountId'];
		$bank_id="";
		$bankbranch_id="";
		$bankaccounttype_id="";
		$bankaccount = $this->Bankaccount->find(
			'all',
			array(
				"fields" => array("id","bankaccount_name"),
				"conditions" => array('Bankaccount.bank_id'=>$BankId)
			)
		);
		
		$bankchequebook_options.="<option value=\"\">".__("Select Account")."</option>";
		foreach ($bankaccount as $Thisbankaccount) {
			# code...
			$bankchequebook_options.="<option value=\"{$Thisbankaccount["Bankaccount"]["id"]}\">{$Thisbankaccount["Bankaccount"]["bankaccount_name"]}</option>";
		}
		$bankchequebook_options.="";
		$json_array["bankchequebook_options"] = "{$bankchequebook_options}";
		
		return json_encode($json_array);
	}

	public function bankchequebookseriesbyaccount(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$json_array = array();
		$BankId = @$this->request->data['BankId'];
		$BankaccountId = @$this->request->data['BankaccountId'];
		$bank_id="";
		$bankbranch_id="";
		$bankaccounttype_id="";
		$bankaccount = $this->Bankaccount->find(
			'first',
			array(
				"fields" => array("bank_id","bankbranch_id","bankaccounttype_id"),
				"conditions" => array('Bankaccount.id'=>$BankaccountId,'Bankaccount.bank_id'=>$BankId)
			)
		);
		//foreach ($bankaccount as $Thisbankaccount) {
			# code...
			$bank_id.="{$bankaccount["Bankaccount"]["bank_id"]}";
			$bankbranch_id.="{$bankaccount["Bankaccount"]["bankbranch_id"]}";
			$bankaccounttype_id.="{$bankaccount["Bankaccount"]["bankaccounttype_id"]}";
		//}
		$json_array["bank_id"] = "{$bank_id}";
		$json_array["bankbranch_id"] = "{$bankbranch_id}";
		$json_array["bankaccounttype_id"] = "{$bankaccounttype_id}";

		$bankchequebook=$this->Bankchequebook->find(
			'all',
			array(
				"fields" => array("id","bankchequebook_name"),
				"conditions" => array('Bankchequebook.bankaccount_id'=>$BankaccountId)
			)
		);
		$bankchequebook_options="";
		$bankchequebook_options.="<option value=\"\">Select Chequebook</option>";
		foreach ($bankchequebook as $Thisbankchequebook) {
			# code...
			$bankchequebook_options.="<option value=\"{$Thisbankchequebook["Bankchequebook"]["id"]}\">{$Thisbankchequebook["Bankchequebook"]["bankchequebook_name"]}</option>";
		}
		$bankchequebook_options.="";
		$json_array["bankchequebook_options"] = "{$bankchequebook_options}";
		$banktransaction=$this->Banktransaction->find(
			'all',
			array(
				"fields" => array("SUM(Banktransaction.transactionamount)"),
				"conditions" => array(
					'Banktransaction.bankaccount_id'=>$BankaccountId,
					'Banktransaction.banktransactionisactive'=>1
				)
			)
		);
		$json_array["banktransactiontotalamount"] = $banktransaction[0][0]["SUM(`Banktransaction`.`transactionamount`)"]?$banktransaction[0][0]["SUM(`Banktransaction`.`transactionamount`)"]:0;
		return json_encode($json_array);
	}

	public function bankchequebooksequencybyseries(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		//print_r($this->request);
		$json_array = array();
		$BanktransactionBanktransactionchequesequencynumber = @$this->request->data['BanktransactionBanktransactionchequesequencynumber'];
		$banktransaction=$this->Banktransaction->find(
			'all',
			array(
				"fields" => array("id","banktransactionuuid","banktransaction_chequenumber"),
				"conditions" => array(
					'Banktransaction.bankchequebook_id'=>$BanktransactionBanktransactionchequesequencynumber,
					'Banktransaction.banktransactionisactive'=>0
				),
				"limit" => 1
			)
		);
		$banktransaction_options="";
		$banktransaction_id="";
		$banktransaction_uuid="";
		foreach ($banktransaction as $Thisbanktransaction) {
			# code...
			//$banktransaction_options.="<option value=\"{$Thisbanktransaction["Banktransaction"]["id"]}\">{$Thisbanktransaction["Banktransaction"]["banktransaction_chequenumber"]}</option>";
			$banktransaction_options.="{$Thisbanktransaction["Banktransaction"]["banktransaction_chequenumber"]}";
			$banktransaction_id.="{$Thisbanktransaction["Banktransaction"]["id"]}";
			$banktransaction_uuid.="{$Thisbanktransaction["Banktransaction"]["banktransactionuuid"]}";
		}
		$banktransaction_options.="";
		$json_array["banktransaction_options"] = "{$banktransaction_options}";
		$json_array["banktransaction_id"] = "{$banktransaction_id}";
		$json_array["banktransaction_uuid"] = "{$banktransaction_uuid}";
		return json_encode($json_array);
	}

	public function banktransactioninsertupdateaction(){
		//$Transactions = new TransactionsController;
		$GroupId = @$this->request->data['GroupId'];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];
		$id = @$this->request->data['Banks']['id'];
		$uuid = @$this->request->data['Banks']['banktransactionuuid'];
		$this->request->data['Banktransaction']['bankchequebook_id']=@$this->request->data['Banktransaction']['banktransactionchequebookseries']?$this->request->data['Banktransaction']['banktransactionchequebookseries']:0;
		$this->request->data['Banktransaction']['transactiontype_id']=@$this->request->data['Banktransaction']['banktransactiontype_id']?$this->request->data['Banktransaction']['banktransactiontype_id']:0;
		$this->request->data['Banktransaction']['transaction_id']=0;
		$banktransactionchequesequencynumber = explode("-", @$this->request->data['Banktransaction']['banktransactionchequesequencynumber']?$this->request->data['Banktransaction']['banktransactionchequesequencynumber']:'0-0');

		$this->request->data['Banktransaction']['banktransactionchequebookseries']=$banktransactionchequesequencynumber[0];
		$this->request->data['Banktransaction']['banktransactionchequesequencynumber']=$banktransactionchequesequencynumber[1];
		$this->request->data['Banktransaction']['banktransactionisactive']=1;
		$this->request->data['Banktransaction']['company_id'] = $company_id;
		$this->request->data['Banktransaction']['branch_id'] = $branch_id;
		if($this->request->data['Banktransaction']['banktransactiontype_id']==$_SESSION["ST_BANKPAYMENT"]):
			$transactionamount=$this->request->data['Banktransaction']['transactionamount'] = -$this->request->data['Banktransaction']['banktransactionamount'];
		else:
			$transactionamount=$this->request->data['Banktransaction']['transactionamount'] = $this->request->data['Banktransaction']['banktransactionamount'];
		endif;
		if($this->request->data['Banktransaction']['salesman_id']==""):
			$this->request->data['Banktransaction']['salesman_id'] = 0;
		endif;
		if($this->request->data['Banktransaction']['bankchequebook_id']==""):
			$this->request->data['Banktransaction']['bankchequebook_id'] = 0;
		endif;
		$this->request->data['Banktransaction']['transactiondate'] = $this->request->data['Banktransaction']['banktransactiondate'];
		//print_r($_POST);exit();
		if ($this->request->is('post')) {
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
				$this->request->data['Banktransaction']['banktransactionupdateid']=$user_id;
				$this->request->data['Banktransaction']['banktransactionupdatedate']=date('Y-m-d H:i:s');
				$message = __('Update Successfully.');
				$this->Banktransaction->id = $id;
			else:
				$this->request->data['Banktransaction']['banktransactioninsertid']=$user_id;
				$this->request->data['Banktransaction']['banktransactioninsertdate']=date('Y-m-d H:i:s');
				$this->request->data['Banktransaction']['banktransactionuuid']=String::uuid();
				$message = __('Save Successfully.');
			endif;
			if ($this->Banktransaction->save($this->request->data, array('validate' => 'only'))) {
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				$transaction_id = $this->Banktransaction->getLastInsertId()?$this->Banktransaction->getLastInsertId():$id;
				$transactiontype_id = $this->request->data['Banktransaction']['banktransactiontype_id'];
				$clientsuppler_id = @$this->request->data['Banktransaction']['user_id']?$this->request->data['Banktransaction']['user_id']:0;
				//$transactionamount = $this->request->data['Banktransaction']['transactionamount'];
				$transactiondate = $this->request->data['Banktransaction']['banktransactiondate'];
				$clientsupplergroup_id = $this->request->data['Banktransaction']['group_id'];
				if($clientsupplergroup_id==4):
					$coa_id=3;
				else:
					$coa_id=5;
				endif;
				$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $transactionamount, 1, $clientsuppler_id, $product_id=0, $transactiondate, $isActive=1, 0);

				$this->Transactions->usertransaction($transaction_id, $transactiontype_id, (0+$transactionamount), $coa_id, $clientsuppler_id, $clientsupplergroup_id, $transactiondate, $isActive=1, 1);
				$this->Transactions->gltransaction($transaction_id, $transactiontype_id, (0+$transactionamount), $coa_id, $clientsuppler_id, $product_id=0, $transactiondate, $isActive=1, 1);

				$this->redirect("/banks/banktransactionmanage");
			}else{
				$errors = $this->Banktransaction->invalidFields();
				$this->set('alert alert-danger', $this->Banktransaction->invalidFields());
				$this->redirect("/banks/banktransactioninsertupdate");
			}
		}
		$this->redirect("/banks/banktransactioninsertupdate");		
	}
	
	//End Bank Transaction//
}
?>