<?PHP
echo $this->Form->create('Cashs', array('action' => 'cashtransactioninsertupdateaction', 'type' => 'file'));
	echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$cashtransaction[0]['Cashtransaction']['id']));
	echo $this->Form->input('cashtransactionuuid', array('type'=>'hidden', 'value'=>@$cashtransaction[0]['Cashtransaction']['cashtransactionuuid']?$cashtransaction[0]['Cashtransaction']['cashtransactionuuid']:0, 'class'=>''));
	echo $this->Form->input('Cashtransaction.coa_id', array('type'=>'hidden', 'value'=>@$cashtransaction[0]['Cashtransaction']['coa_id']?$cashtransaction[0]['Cashtransaction']['coa_id']:1, 'class'=>''));
	echo $this->Form->input('Cashtransaction.salesman_id', array('type'=>'hidden', 'value'=>@$cashtransaction[0]['Cashtransaction']['salesman_id']?$cashtransaction[0]['Cashtransaction']['salesman_id']:0, 'class'=>''));
	
	$options = array($group, 999 =>__("Others"));
	$attributes = array(
		'legend' => false, 
		'label' => true, 
		'value' => '', 
		'class' => 'form-inline'
	);
	if(@$cashtransaction[0]['User']['id'] == 0 && @$id !=0):
		$cashtransaction[0]['Cashtransaction']['group_id']=999;
	endif;
	echo"<h3>".__("Add New Cash Transaction")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-11\">";
			echo"<div class=\"row\">";
				echo"<div class=\"col-md-4\">";
					echo $this->Form->input(
						"Cashtransaction.transactiondate",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Date'),
							'tabindex'=>1,
							'placeholder' => __("Date"),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style' => '',
							'value' => @$cashtransaction[0]["Cashtransaction"]["transactiondate"]
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-4\">";
					echo $this->Form->input(
						"Cashtransaction.group_id",
						array(
							'type'=>'select',
							"options"=>$options,
							'empty'=>__('Select Group'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>2,
							'label'=>__('Select Group'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$cashtransaction[0]['User']['group_id'] ? $cashtransaction[0]['User']['group_id'] : 0)
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-4\" id=\"groupuserchangediv\">";
					echo $this->Form->input(
						"Banktransaction.user_id",
						array(
							'type'=>'select',
							"options"=>array($user),
							'empty'=>__('Select Member'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>3,
							'label'=>__('Select Member'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$cashtransaction[0]['User']['id'] ? $cashtransaction[0]['User']['id'] : 0)
						)
					);
				echo"</div>";
			echo"</div>";
			echo"<div class=\"row\">";
				echo"<div class=\"col-md-4\">";
					echo $this->Form->input(
						"Cashtransaction.cashtransactiontype_id",
						array(
							'type'=>'select',
							"options"=>array($_SESSION["ST_CASHDEPOSIT"]=>"Deposit",$_SESSION["ST_CASHPAYMENT"]=>"Withdraw"),
							'empty'=>__('Select Transaction Type'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>7,
							'label'=>__('Select Type'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$cashtransaction[0]['Cashtransaction']['transactiontype_id'] ? $cashtransaction[0]['Cashtransaction']['transactiontype_id'] : 0)
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-4\">";
					echo"<div class=\"col-md-6\">";
					echo $this->Form->input(
						"paymentrecive",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Payment/Recived'),
							'tabindex'=>8,
							'placeholder' => __("Payment/Recived"),
							'readonly' => 'readonly',
							'class'=> 'form-control',
							'style' => '',
							'value'=>@$totalcashtransaction[0]["totalcashtransaction"]?$totalcashtransaction[0]["totalcashtransaction"]:0
						)
					);
					echo"</div>";
					echo"<div class=\"col-md-6\">";
						echo $this->Form->input(
							"usertotalamount",
							array(
								'type' => 'text',
								'div' => array('class'=> 'form-group'),
								'label' => __('Client/Suppler/User'),
								'tabindex'=>8,
								'placeholder' => __("Client/Suppler/User"),
								'readonly' => 'readonly',
								'class'=> 'form-control',
								'style' => '',
								'value'=>0.00
							)
						);
					echo"</div>";
				echo"</div>";
				echo"<div class=\"col-md-4\">";
					echo"<div class=\"col-md-6\">";
					echo $this->Form->input(
						"remainigbalance",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Remainig'),
							'tabindex'=>9,
							'placeholder' => __("Remainig"),
							'readonly' => 'readonly',
							'class'=> 'form-control',
							'style' => ''
						)
					);
					echo"</div>";
					echo"<div class=\"col-md-6\">";
					echo $this->Form->input(
						"userremainigbalance",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Client/Suppler/User Remainig'),
							'tabindex'=>9,
							'placeholder' => __("Client/Suppler/User Remainig"),
							'readonly' => 'readonly',
							'class'=> 'form-control',
							'style' => ''
						)
					);
					echo"</div>";
				echo"</div>";
			echo"</div>";
			echo $this->Form->input(
				"Cashtransaction.transactionamount",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Amount'),
					'tabindex'=>10,
					'placeholder' => __("Amount"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style' => '',
					"value" => (@$cashtransaction[0]['Cashtransaction']['transactionamount'] ? str_replace("-", "", $cashtransaction[0]['Cashtransaction']['transactionamount']) : 0)
				)
			);
			echo $Utilitys->allformbutton('',$this->request["controller"],$page='cashtransaction');
		echo"</div>";
	echo"</div>";
	echo $this->Form->end();
	echo"
	<script>
		$(document).ready(function() {
			CashtransactionGroupId();
			$(\"#CashsCashtransactioninsertupdateactionForm\").validationEngine();
			var totalRemainig = 0.00;
			var CashtransactionTransactionamount = 0.00;
			var cashtransactiontotalamount = ".(@$totalcashtransaction[0]["totalcashtransaction"]?$totalcashtransaction[0]["totalcashtransaction"]:0).";
			var totalRemainig = cashtransactiontotalamount - $(\"CashtransactionTransactionamount\").val();
			//alert(cashtransactiontotalamount);
			var ST_CASHDEPOSIT = ".$_SESSION['ST_CASHDEPOSIT'].";
			var ST_CASHPAYMENT = ".$_SESSION['ST_CASHPAYMENT'].";
								

			$(\"#CashtransactionTransactiondate\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd HH:mm:ss\", use24hours: true, showMeridian: false, startView: 2, minView: 2, autoclose: 1});

			$(\"#CashtransactionGroupId\").change(function(){
				CashtransactionGroupId();
			});

			$(\"#BanktransactionUserId\").change(function(){
				var ClientId = $(this).val();
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."banks/salespersonbyuser',
					data:{ClientId:ClientId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#CashtransactionSalesmanId\").val(result.salesperson_id);
						$(\"#CashsUsertotalamount\").val(result.usertotalamount);
					}
				});
			});

			
			$(\"#CashtransactionTransactionamount\").keyup(function(){
				var CashtransactionTransactionamount = $(this).val();
				var CashsPaymentrecive = $(\"#CashsPaymentrecive\").val();
				if(CashsPaymentrecive == \"\"){
					CashsPaymentrecive = 0.00;
				}
				var CashtransactionCashtransactiontypeId = $(\"#CashtransactionCashtransactiontypeId\").val();
				if(CashtransactionCashtransactiontypeId==ST_CASHDEPOSIT){
					totalRemainig = parseFloat(CashsPaymentrecive) + parseFloat(CashtransactionTransactionamount);
				}else if(CashtransactionCashtransactiontypeId==ST_CASHPAYMENT){
					totalRemainig = parseFloat(CashsPaymentrecive) - parseFloat(CashtransactionTransactionamount);
				}else{
					alert('".__("Please Select Transaction Type")."');
					totalRemainig = 0.00;
				}
				$(\"#CashsRemainigbalance\").val(totalRemainig);
			});
			$(\"#CashtransactionCashtransactiontypeId\").change(function(){
				var totalRemainig = 0.00;
				var CashtransactionTransactionamount = $(\"#CashtransactionTransactionamount\").val();
				var CashsPaymentrecive = $(\"#CashsPaymentrecive\").val();
				var CashtransactionCashtransactiontypeId = $(this).val();
				if(CashtransactionCashtransactiontypeId==ST_CASHDEPOSIT){
					totalRemainig = parseFloat(CashsPaymentrecive) + parseFloat(CashtransactionTransactionamount);
				}else if(CashtransactionCashtransactiontypeId==ST_CASHPAYMENT){
					totalRemainig = parseFloat(CashsPaymentrecive) - parseFloat(CashtransactionTransactionamount);
				}else{
					alert('".__("Please Select Transaction Type")."');
					totalRemainig = 0.00;					
				}
				$(\"#CashsRemainigbalance\").val(totalRemainig);
			});
		});
		function CashtransactionGroupId(){
			var GroupId = $(\"#CashtransactionGroupId\").val();
			var BanktransactionBanktransactionothernote = '".(@$cashtransaction[0]['Cashtransaction']['cashtransactionothernote'] ? $cashtransaction[0]['Cashtransaction']['cashtransactionothernote'] : '')."';
			var userid=  '".(@$cashtransaction[0]['Cashtransaction']['user_id'] ? $cashtransaction[0]['Cashtransaction']['user_id'] : 0)."';
			if(userid!=0){
				userothernote = userid;
			}else{
				userothernote = BanktransactionBanktransactionothernote;
			}
			if(GroupId==5){
				$(\"#CashtransactionSalesmanId\").attr(\"disabled\", \"disabled\");
			}else{
				$(\"#CashtransactionSalesmanId\").removeAttr(\"disabled\");
			}
			$(\"#groupuserchangediv\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
			$.post(
				'".$this->webroot."banks/banktransctionuserlistbygroup',
				{GroupId:GroupId,BanktransactionBanktransactionothernote:userothernote},
				function(result) {
					$(\"#groupuserchangediv\").html(result);
				}
			);
		}
	</script>
	";
?>