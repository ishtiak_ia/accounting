<?php
App::import('Controller', 'Logins');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');
class LocationsController extends AppController {
	public $name = 'Locations';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'Form' => array('className' => 'BootstrapForm'));
	public $components = array('RequestHandler','Session');
	public $uses = array('Location', 'LocationParent','User');
	var $Logins;

	function beforeFilter(){
		$this->Logins =& new LoginsController;
		$this->Logins->constructClasses();
		$this->Logins->__validateLoginStatus();
		$Logins = new LoginsController;
		$this->set('Logins',$Logins);
		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	//Start Location//

	public function locationmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Location').' | '.__(Configure::read('site_name')));

		$divisionlist=$this->Location->find(
			'list',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions"=>array(
					'Location.locationstep'=>0,
					'Location.locationisactive'=>1,
				)
			)
		);
		$this->set('divisionlist',$divisionlist);

		$districlist=$this->Location->find(
			'list',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions"=>array(
					'Location.locationstep'=>1,
					'Location.locationisactive'=>1,
				)
			)
		);
		$this->set('districlist',$districlist);

		$upazilalist=$this->Location->find(
			'list',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions"=>array(
					'Location.locationstep'=>2,
					'Location.locationisactive'=>1,
				)
			)
		);
		$this->set('upazilalist',$upazilalist);

		$unionparishadlist=$this->Location->find(
			'list',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions"=>array(
					'Location.locationstep'=>3,
					'Location.locationisactive'=>1,
				)
			)
		);
		$this->set('unionparishadlist',$unionparishadlist);

		$paralist=$this->Location->find(
			'list',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions"=>array(
					'Location.locationstep'=>4,
					'Location.locationisactive'=>1,
				)
			)
		);
		$this->set('paralist',$paralist);

		$this->paginate = array(
			'fields' => 'Location.*,LocationParent.*',
			//'group' => '`Location`.`locationparentid`',
			'order'  =>'Location.locationparentid ASC, Location.locationname ASC',
			'limit' => 20
		);
		$location = $this->paginate('Location');
		$this->set('location',$location);
	}

	public function locationsearchbytext($Searchtext=null){
		$this->layout='ajax';

		$Searchtext = @$this->request->data['Searchtext'];
		$divisionid = @$this->request->data['LocationDivisionid'];
		$districid = @$this->request->data['LocationDistricid'];
		$upazilaid = @$this->request->data['LocationUpazilaid'];
		$unionparishadid = @$this->request->data['LocationUnionparishadid'];

		$location_id = array();
		if(!empty($Searchtext) && $Searchtext!=''):
			$location_id[] = array(
				'OR' => array(
					'Location.locationname LIKE ' => '%'.$Searchtext.'%',
					'Location.locationnamebn LIKE ' => '%'.$Searchtext.'%'
				)
			);
		endif;
		if(!empty($divisionid) && $divisionid!=''):
			$location_id[] = array(
				'Location.locationparentid ' => $divisionid
			);
		endif;
		if(!empty($districid) && $districid!=''):
			$location_id[] = array(
				'Location.locationparentid ' => $districid
			);
		endif;
		if(!empty($upazilaid) && $upazilaid!=''):
			$location_id[] = array(
				'Location.locationparentid ' => $upazilaid
			);
		endif;
		if(!empty($unionparishadid) && $unionparishadid!=''):
			$location_id[] = array(
				'Location.locationparentid ' => $unionparishadid
			);
		endif;

		$this->paginate = array(
			'fields' => 'Location.*,LocationParent.*',
			'order'  =>'Location.locationname ASC',
			"conditions"=>$location_id,
			'limit' => 20
		);
		$location = $this->paginate('Location');
		$this->set('location',$location);
	}

	public function locationdetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$location=$this->Location->find('all',array("fields" => "Location.*","conditions"=>array('Location.id'=>$id,'Location.locationuuid'=>$uuid)));
		$this->set('location',$location);
	}

	public function locationinsertupdate($id=null,$uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Location').' | '.__(Configure::read('site_name')));
		$locationparentlist=$this->Location->find(
			'list',
			array(
				"fields" => array("id","location_name"),
				"conditions" => array("Location.locationisparent !="=>0)
			)
		);
		$this->set('locationparentlist',$locationparentlist);
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
			$location=$this->Location->find('all',array("fields" => "Location.*","conditions"=>array('Location.id'=>$id,'Location.locationuuid'=>$uuid)));
			$this->set('location',$location);
		endif;
	}

	public function  locationinsertupdateaction($id=null,$uuid=null){
		$userID = $_SESSION["User"]["id"];
		$id = @$this->request->data['Locations']['id'];
		$uuid = @$this->request->data['Locations']['locationuuid'];
		$this->request->data['Location']['locationparentid'] = $this->request->data['Location']['locationparentid']?$this->request->data['Location']['locationparentid']:0;
		if($this->request->data['Location']['locationparentid'] != 0):
			$location=$this->Location->find('all',array("fields" => "Location.*","conditions"=>array('Location.id'=>$this->request->data['Location']['locationparentid'])));
			$locationstep=(int)$location[0]["Location"]["locationstep"] + 1;
		else:
			$locationstep=0;
		endif;
		$this->request->data['Location']['locationstep'] = $locationstep;
		$locationname = $this->request->data['Location']['locationname'];
		$locationnamebn = $this->request->data['Location']['locationnamebn'];
		$locationparentid = $this->request->data['Location']['locationparentid'];
		$countLocation=$this->Location->find('all',array("fields" => "Location.*","conditions"=>array('Location.locationname'=>$locationname,'Location.locationnamebn'=>$locationnamebn,'Location.locationparentid'=>$locationparentid)));
		//echo count($countbankaccount);
		//pr($_POST);exit();
		if(count($countLocation)>0):
			$this->Session->setFlash(__("This Location Already Exists"), 'default', array('class' => 'alert alert-warning'));
			$this->redirect("/locations/locationinsertupdate");
		else:
			if ($this->request->is('post')) {
				if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
					$this->request->data['Location']['locationupdateid']=$userID;
					$this->request->data['Location']['locationupdatedate']=date('Y-m-d H:i:s');
					$message = __('Update Successfully.');
					$this->Location->id = $id;
				else:
					$this->request->data['Location']['locationinsertid']=$userID;
					$this->request->data['Location']['locationinsertdate']=date('Y-m-d H:i:s');
					$this->request->data['Location']['locationuuid']=String::uuid();
					$message = __('Save Successfully.');
				endif;
				if ($this->Location->save($this->request->data, array('validate' => 'only'))) {
					$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
					$this->redirect("/locations/locationmanage");
				}else{
					$errors = $this->Location->invalidFields();
					$this->set('alert alert-danger', $this->Location->invalidFields());
					$this->redirect("/locations/locationinsertupdate");
				}
			}
		endif;
	}

	public function locationdelete($id=null,$uuid=null){
		$userID = $_SESSION["User"]["id"];
		if ($this->Location->updateAll(array('locationdeleteid'=>$userID, 'locationdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'locationisactive'=>2), array('AND' => array('Bank.id'=>$id,'Location.locationuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/locations/locationmanage");
	}

	public function districbydividion(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$divisionid = @$this->request->data['LocationDivisionid'];

		$districlist=$this->Location->find(
			'all',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions"=>array(
					'Location.locationparentid' => $divisionid,
					'Location.locationstep'=>1,
					'Location.locationisactive'=>1,
				)
			)
		);
			echo "<option value=\"0\">Distric</option>";
		foreach($districlist as $thisdistriclist) {
			echo "<option value=\"".$thisdistriclist['Location']['id']."\">".$thisdistriclist['Location']['location_name']."</option>";
		}
	}

	public function upazilabydistric(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$districid = @$this->request->data['LocationDistricid'];

		$upazilalist=$this->Location->find(
			'all',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions"=>array(
					'Location.locationparentid' => $districid,
					'Location.locationstep'=>2,
					'Location.locationisactive'=>1,
				)
			)
		);
			echo "<option value=\"0\">Upazila</option>";
		foreach($upazilalist as $thisupazilalist) {
			echo "<option value=\"".$thisupazilalist['Location']['id']."\">".$thisupazilalist['Location']['location_name']."</option>";
		}
	}

	public function unionparishadbyupazila(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$upazilaid = @$this->request->data['LocationUpazilaid'];

		$unionparishadlist=$this->Location->find(
			'all',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions"=>array(
					'Location.locationparentid' => $upazilaid,
					'Location.locationstep'=>3,
					'Location.locationisactive'=>1,
				)
			)
		);
			echo "<option value=\"0\">Union Parishad</option>";
		foreach($unionparishadlist as $thisunionparishadlist) {
			echo "<option value=\"".$thisunionparishadlist['Location']['id']."\">".$thisunionparishadlist['Location']['location_name']."</option>";
		}
	}

	public function paraareabyunionparishad(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$unionparishadid = @$this->request->data['LocationUnionparishadid'];

		$paraarealist=$this->Location->find(
			'all',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions"=>array(
					'Location.locationparentid' => $unionparishadid,
					'Location.locationstep'=>4,
					'Location.locationisactive'=>1,
				)
			)
		);
		print_r($paraarealist);
			echo "<option value=\"0\">Para / Area</option>";
		foreach($paraarealist as $thisparaarealist) {
			echo "<option value=\"".$thisparaarealist['Location']['id']."\">".$thisparaarealist['Location']['location_name']."</option>";
		}
	}
	//End Location//
}
?>