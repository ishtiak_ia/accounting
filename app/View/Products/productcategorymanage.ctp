<?php
	echo "<h2>".__('Product Category'). $Utilitys->addurl($title=__("Add New Product Category"), $this->request['controller'], $page="productcategory")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Productcategory.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'tabindex'=>3,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"productcategorysarchloading\">
	";
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('Product Category Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Product Category Name BN')  => array('class' => 'highlight sortable')
				), 
				__('Active'), 
				__('Action')
			)
		);
		$productcategoryrows = array();
		$countProductcategory = 0-1+$this->Paginator->counter('%start%');
		foreach($productcategory as $Thisproductcategory):
			$countProductcategory++;
			$productcategoryrows[]= array($countProductcategory,$Thisproductcategory["Productcategory"]["productcategoryname"],$Thisproductcategory["Productcategory"]["productcategorynamebn"],$Utilitys->statusURL($this->request["controller"], 'productcategory', $Thisproductcategory["Productcategory"]["id"], $Thisproductcategory["Productcategory"]["productcategoryuuid"], $Thisproductcategory["Productcategory"]["productcategoryisactive"]),$Utilitys->cusurl($this->request["controller"], 'productcategory', $Thisproductcategory["Productcategory"]["id"], $Thisproductcategory["Productcategory"]["productcategoryuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$productcategoryrows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();

	echo"
				</div>
		<script>
			$(document).ready(function() {
				$(\"#ProductcategorySearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#productcategorysarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."products/productcategorysearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#productcategorysarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>