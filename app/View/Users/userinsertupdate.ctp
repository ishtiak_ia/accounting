<script type="text/javascript">
 jQuery(document).ready(function(){
 });
</script>
<?php
//pr($_SESSION);
//pr($user);
//$baseurl = $this->base;
echo $this->Form->create('Users', array('action' => 'userinsertupdateaction', 'type' => 'file'));
echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$user[0]['User']['id']));

echo $this->Form->input('useruuid', array('type'=>'hidden', 'value'=>@$user[0]['User']['useruuid'], 'class'=>''));
$u_image = @$user[0]['User']['userimage']; 
 ?>
<script type="text/javascript">
	//jQuery(document).ready(function() {
     //   var baseurl = '<?php echo $this->base; ?>';
    //});    
</script>
	<h3>Add User</h3>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label for="data[User][company_id]">Select a Company <span style=color:red;>*</span></label>
			<?php if($group_id != 1) {
echo $this->Form->input('User.company_id', array('type'=>'hidden', 'value'=>@$_SESSION["User"]["company_id"])); echo "<br />".$_SESSION["User"]["company_name"];
	        } else {
	            $option1 = array($companyname);
	            echo $this->Form->input("User.company_id", array(
	            	'type' => 'select',
	            	 "options" => $option1, 
	            	 'empty' => 'Select Company',
	            	 'div' => false,
	            	 'label' => false, 
	            	 "optgroup label" => false,
	            	 'style' => '',
	            	 'data-validation-engine'=>'validate[required]',
	            	'selected'=> (@$user[0]['User']['company_id'] ? $user[0]['User']['company_id'] : 0),
					'class'=> 'form-control'
	            	));
	        } ?>
            </div>
            <div class="form-group"  id="branchchangediv">
				<label for="data[User][branch_id]">Select a Branch <span style=color:red;>*</span></label>
            <?php if($group_id != 1 && $group_id != 2 ) {
            	echo $this->Form->input('User.branch_id', array('type'=>'hidden', 'value'=>@$_SESSION["User"]["branch_id"])); echo "<br />".$_SESSION["User"]["branch_name"];
            }else { 
	            $option3 = array($branchname);
	            echo $this->Form->input("User.branch_id", array(
	            	'type' => 'select', 
	            	"options" => $option3,
	            	'empty' => 'Select Branch', 
	            	'div' => false, 
	            	'label' => false,
	            	"optgroup label" => false, 
	            	'style' => '',
	            	'data-validation-engine'=>'validate[required]',
	            	'selected'=> (@$user[0]['User']['branch_id'] ? $user[0]['User']['branch_id'] : 0),
					'class'=> 'form-control'
	            	));
	             } ?>
            </div>
            <div class="form-group">
				<label for="data[User][group_id]">Select a Group <span style=color:red;>*</span></label>
	            <?php
	            $option2 = array($groupname);
	            echo $this->Form->input("User.group_id", array(
	            	'type' => 'select', 
	            	"options" => $option2, 
	            	'empty' => 'Select Group', 
	            	'div' => false, 
	            	'label' => false, 
	            	"optgroup label" => false, 
	            	//'onChange' => "getdata(0,'$baseurl');",
	            	'style' => '',
	            	'data-validation-engine'=>'validate[required]',
	            	'selected'=> (@$user[0]['User']['group_id'] ? $user[0]['User']['group_id'] : 0),
					'class'=> 'form-control'
	            	));
	            ?>
            </div>
            <div class="form-group" id="groupchangediv" style="display:no ne;">
            	
            </div>
            	<?php
	            $option8 = array($category_list);
	            echo $this->Form->input("User.category_id", array(
	            	'type' => 'select', 
	            	"options" => $option8, 
	            	'empty' => __('Select Category'), 
	            	'div' => array('class'=>'form-group'),
	            	'label' => __('Select a Category'),
	            	'style' => '',
	            	'selected'=> (@$user[0]['User']['category_id'] ? $user[0]['User']['category_id'] : 0),
					'class'=> 'form-control'
	            	));
	            ?>
	            <?php
	            $option9 = array($subject_list);
	            echo $this->Form->input("User.subject_id", array(
	            	'type' => 'select', 
	            	"options" => $option9, 
	            	'empty' => __('Select Subject'), 
	            	'div' => array('class'=>'form-group'),
	            	'label' => __('Select a Subject'),
	            	'style' => '',
	            	'selected'=> (@$user[0]['User']['subject_id'] ? $user[0]['User']['subject_id'] : 0),
					'class'=> 'form-control'
	            	));
	            ?>
	            <?php
	            $option4 = array($department);
	            echo $this->Form->input("User.department_id", array(
	            	'type' => 'select', 
	            	"options" => $option4, 
	            	'empty' => __('Select Department'), 
	            	'div' => array('class'=>'form-group'),
	            	'label' => __('Select a Department <span style=color:red;>*</span>'),
	            	'style' => '',
	            	'data-validation-engine'=>'validate[required]',
	            	'selected'=> (@$user[0]['User']['department_id'] ? $user[0]['User']['department_id'] : 0),
					'class'=> 'form-control'
	            	));
	            ?>
	            <?php
	            //$option4 = array($shift);
	            echo $this->Form->input("User.shift_id", array(
	            	'type' => 'select', 
	            	"options" => array($shiftname),
	            	'empty' => __('Select Shift'), 
	            	'div' => array('class'=>'form-group'),
	            	'label' => __('Select a Shift <span style=color:red;>*</span>'),
	            	'style' => '',
	            	'data-validation-engine'=>'validate[required]',
	            	'selected'=> (@$user[0]['User']['shift_id'] ? $user[0]['User']['shift_id'] : 0),
					'class'=> 'form-control'
	            	));
	            ?>
	            <?php
	            $option5 = array($designation);
	            echo $this->Form->input("User.designation_id", array(
	            	'type' => 'select', 
	            	"options" => $option5, 
	            	'empty' => __('Select Designation'), 
	            	'div' => array('class'=>'form-group'), 
	            	'label'=> __('Select a Designation <span style=color:red;>*</span>'), 
	            	'style' => '',
	            	'data-validation-engine'=>'validate[required]',
	            	'selected'=> (@$user[0]['User']['designation_id'] ? $user[0]['User']['designation_id'] : 0),
					'class'=> 'form-control'
	            	));
	            ?>
            <div class="form-group">
				<label for="data[User][institute_id]">Select a Institute</label>
	            <?php
	            $option6 = array($institute);
	            echo $this->Form->input("User.institute_id", array(
	            	'type' => 'select', 
	            	"options" => $option6, 
	            	'empty' => 'Select Institute', 
	            	'div' => false, 
	            	'label' => false, 
	            	"optgroup label" => false,
	            	'style' => '',
	            	//'data-validation-engine'=>'validate[required]',
	            	'selected'=> (@$user[0]['User']['institute_id'] ? $user[0]['User']['institute_id'] : 0),
					'class'=> 'form-control'
	            	));
	            ?>
            </div>
            <?php
            	if(@$user[0]['User']['id'] ==null &&  @$user[0]['User']['useruuid'] ==null){
            		echo $this->Form->input("User.userjoiningsalary", array(
	            	'type' => 'text', 
	            	'div' => array('class'=>'form-group'), 
	            	'label'=> __('Joining Salary <span style=color:red;>*</span>'), 
	            	'placeholder' => __("Joining Salary"), 
	            	'style' => '',
	            	'data-validation-engine'=>'validate[custom[onlyNumber]]',
	            	'value' =>(@$user[0]['User']['userjoiningsalary']),
					'class'=> 'form-control'
	            	));
	        
	            echo $this->Form->input("User.userjoiningdate", array(
	            	'type' => 'text', 
	            	'div' => array('class'=>'form-group'), 
	            	'label'=> __('Joining Date '), 
	            	'placeholder' => __("Joining Date"),
	            	'style' => '',
	            	//'data-validation-engine'=>'validate[required]',
	            	'value' =>(@$user[0]['User']['userjoiningdate']),
					'class'=> 'form-control'
	            	));
	        } else{
	        	echo "<strong>Joining Salary</strong>"; echo "<br>";
	        	echo $user[0]['User']['userjoiningsalary']; echo "<br>";
	        	echo "<strong>Joining Date</strong>"; echo "<br>";
	        	echo $user[0]['User']['userjoiningdate']; echo "<br>";
	        }
	            
	        ?>
	        <?php
	            echo $this->Form->input("User.userfiredate", array(
	            	'type' => 'text', 
	            	'div' => array('class'=>'form-group'), 
	            	'label'=> __('Termination Date'), 
	            	'placeholder' => __("Termination Date"), 
	            	'style' => '',
	            	'value' =>(@$user[0]['User']['userfiredate']),
					'class'=> 'form-control'
	            	));
	        ?>
	        <?php
	            $option7 = array($authorized_superior);
	            echo $this->Form->input("User.parentid", array(
	            	'type' => 'select', 
	            	"options" => $option7, 
	            	'empty' => __('Select Authorized Superior'), 
	            	'div' => array('class'=>'form-group'), 
	            	'label'=> __('Select Authorized Superior'), 
	            	'style' => '',
	            	//'data-validation-engine'=>'validate[required]',
	            	'selected'=> (@$user[0]['User']['parentid'] ? $user[0]['User']['parentid'] : 0),
					'class'=> 'form-control'
	            	));
	            ?>
	        <?php
	            echo $this->Form->input("User.userfacilities", array(
	            	'type' => 'text', 
	            	'div' => array('class'=>'form-group'), 
	            	'label'=> __('Facilities'),
	            	'placeholder' => __("Facilities"), 
	            	'style' => '',
	            	//'data-validation-engine'=>'validate[required]',
	            	'value' =>(@$user[0]['User']['userfacilities']),
					'class'=> 'form-control'
	            	));
	        ?>   
	        <?php
	            echo $this->Form->input("User.usermeritposition", array(
	            	'type' => 'text', 
	            	'div' => array('class'=>'form-group'), 
	            	'label'=> __('Merit Position'), 
	            	'style' => '',
	            	'placeholder' => __("Merit Position"),
	            	//'data-validation-engine'=>'validate[required]',
	            	'value' =>(@$user[0]['User']['usermeritposition']),
					'class'=> 'form-control'
	            	));
	        ?>  
	        <?php
	            echo $this->Form->input("User.referenceinstitution", array(
	            	'type' => 'text', 
	            	'div' => array('class'=>'form-group'), 
	            	'label'=> __('Reference Institution'),
	            	'placeholder' => __("Reference Institution"),
	            	'style' => '',
	            	//'data-validation-engine'=>'validate[required]',
	            	'value' =>(@$user[0]['User']['referenceinstitution']),
					'class'=> 'form-control'
	            	));
	        ?> 
		</div>
		<div class="col-md-4">
            <div class="form-group">
				<label for="data[User][userfirstname]">First name <span style=color:red;>*</span></label>
				<?php echo $this->Form->input("User.userfirstname", array(
				'type' => 'text', 
				'div' => false, 
				'label' => false,
				'placeholder' => __("First Name"),
				'value' =>@$user[0]['User']['userfirstname'],
				'data-validation-engine'=>'validate[required]',
				'class'=> 'form-control'
				)); ?>
            </div>
            <div class="form-group">
				<label for="data[User][usermiddlename]">Middle Name</label>
				<?php echo $this->Form->input("User.usermiddlename", array(
				'type' => 'text', 
				'div' => false, 
				'label' => false,
				'placeholder' => __("Middle Name"),
				'value' =>@$user[0]['User']['usermiddlename'],
				'class'=> 'form-control'
				)); ?>
            </div>
            <div class="form-group">
				<label for="data[User][userlastname]">Last Name <span style=color:red;>*</span></label>
				<?php echo $this->Form->input("User.userlastname", array(
				'type' => 'text', 
				'div' => false, 
				'label' => false,
				'placeholder' => __("Last Name"),
				'value' =>@$user[0]['User']['userlastname'],
				'data-validation-engine'=>'validate[required]',
				'class'=> 'form-control'
				)); ?>
            </div>
            <div class="form-group">
				<label for="data[User][userfathername]">Father's Name</label>
				<?php echo $this->Form->input("User.userfathername", array(
				'type' => 'text', 
				'div' => false, 
				'label' => false,
				'placeholder' => __("Father's Name"),
				'value' =>@$user[0]['User']['userfathername'],
				//'data-validation-engine'=>'validate[required]',
				'class'=> 'form-control'
				)); ?>
            </div>
            <div class="form-group">
				<label for="data[User][usermothername]">Mother's Name</label>
				<?php echo $this->Form->input("User.usermothername", array(
				'type' => 'text', 
				'div' => false, 
				'label' => false,
				'placeholder' => __("Mother's Name"),
				'value' =>@$user[0]['User']['usermothername'],
				//'data-validation-engine'=>'validate[required]',
				'class'=> 'form-control'
				)); ?>
            </div>
            <?php echo $this->Form->input(
				"User.divisionid",
				array(
					'type'=>'select',
					"options"=>array($divisionlist),
					'empty'=>__('Select Division'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>1,
					'label'=>__('Select Division <span style=color:red;>*</span>'),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style'=>'',
					"selected" => (@$user[0]['User']['divisionid'] ? $user[0]['User']['divisionid'] : 0)
				)
			); ?>
			<?php
			echo $this->Form->input(
				"User.districtid",
				array(
					'type'=>'select',
					"options"=>array($districlist),
					'empty'=>__('Select District'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>1,
					'label'=>__('Select District <span style=color:red;>*</span>'),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style'=>'',
					"selected" => (@$user[0]['User']['districtid'] ? $user[0]['User']['districtid'] : 0)
				)
			);
			 ?>
			<?php 
			echo $this->Form->input(
				"User.upazilaid",
				array(
					'type'=>'select',
					"options"=>array($upazilalist),
					'empty'=>__('Select Upazila'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>1,
					'label'=>__('Select Upazila <span style=color:red;>*</span>'),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style'=>'',
					"selected" => (@$user[0]['User']['upazilaid'] ? $user[0]['User']['upazilaid'] : 0)
				)
			); 
            ?>
            <div id="locationsarchloading">

			</div>
            <div class="form-group">
				<label for="data[User][userpermanentaddress]">Permanent Address <span style=color:red;>*</span></label>
				<?php echo $this->Form->input("User.userpermanentaddress", array(
				'type' => 'textarea', 
				'div' => false, 
				'label' => false,
				'placeholder' => __("Permanent Address"),
				'data-validation-engine'=>'validate[required]',
				'value' =>@$user[0]['User']['userpermanentaddress'],
				'class'=> 'form-control'
				)); ?>
            </div>
            <div class="form-group">
				<label for="data[User][userpresentaddress]">Residential Address <span style=color:red;>*</span></label>
				<?php echo $this->Form->input("User.userpresentaddress", array(
				'type' => 'textarea', 
				'div' => false, 
				'label' => false,
				'placeholder' => __("Residential Address"),
				'data-validation-engine'=>'validate[required]',
				'value' =>@$user[0]['User']['userpresentaddress'],
				'class'=> 'form-control'
				)); ?>
            </div>
		</div>

		<div class="col-md-4">
			<?php if(@$user[0]['User']['id'] ==null &&  @$user[0]['User']['useruuid'] ==null) { ?>
            <div class="form-group">
				<label for="data[User][username]">Username <span style=color:red;>*</span></label>
				<?php echo $this->Form->input("User.username", array(
				'type' => 'text', 
				'div' => false,  
				'label' => false,
				'placeholder' => __("Username"),
				'data-validation-engine'=>'validate[required]',
				'value' =>@$user[0]['User']['username'],
				'class'=> 'form-control'
				)); ?>
            </div>
            <div class="form-group">
				<label for="data[User][userpassword]">Password <span style=color:red;>*</span></label>
				<?php echo $this->Form->input("User.userpassword", array(
				'type' => 'password', 
				'div' => false, 
				'label' => false,
				'placeholder' => __("Password"),
				'data-validation-engine'=>'validate[required]',
				'value' =>@$user[0]['User']['userpassword'],
				'class'=> 'form-control'
				)); ?>
            </div>
            <?php } ?>
            <div class="form-group">
				<label for="data[User][userdob]">Date of birth</label>
				<?php echo $this->Form->input("User.userdob", array(
				'type' => 'text', 
				'div' => false, 
				'label' => false,
				'placeholder' => __("Date of birth"),
				'value' =>@$user[0]['User']['userdob'],
				//'data-validation-engine'=>'validate[required]',
				'class'=> 'form-control'
				)); ?>
            </div>
            <div class="form-group">
				<label for="data[User][userdoboriginal]">Date of birth (Original)</label>
				<?php echo $this->Form->input("User.userdoboriginal", array(
				'type' => 'text', 
				'div' => false, 
				'label' => false,
				'placeholder' => __("Date of birth (Original)"),
				'value' =>@$user[0]['User']['userdoboriginal'],
				//'data-validation-engine'=>'validate[required]',
				'class'=> 'form-control'
				)); ?>
            </div>
            <div class="form-group">
				<label for="data[User][usermarriageday]">Marriage Day</label>
				<?php echo $this->Form->input("User.usermarriageday", array(
				'type' => 'text', 
				'div' => false, 
				'label' => false,
				'placeholder' => __("Marriage Day"),
				'value' =>@$user[0]['User']['usermarriageday'],
				'class'=> 'form-control'
				)); ?>
            </div>
            <?php
	            echo $this->Form->input("User.userchilddob", array(
	            	'type' => 'text', 
	            	'div' => array('class'=>'form-group'), 
	            	'label'=> __('Child\'s Birthday'), 
	            	'placeholder' => __("Child's Birthday"),
	            	'style' => '',
	            	'value' =>(@$user[0]['User']['userchilddob']),
					'class'=> 'form-control'
	            	));
	        ?>
            <div class="form-group">
				<label for="data[User][usergender]">Gender <span style=color:red;>*</span></label>
				<?php echo $this->Form->input("User.usergender", array(
				'type' => 'text', 
				'div' => false, 
				'label' => false,
				'placeholder' => __("Gender"),
				'value' =>@$user[0]['User']['usergender'],
				'data-validation-engine'=>'validate[required]',
				'class'=> 'form-control'
				)); ?>
            </div>
            <div class="form-group">
				<label for="data[User][userbloodgroup]">Blood Group</label>
				<?php echo $this->Form->input("User.userbloodgroup", array(
				'type' => 'text', 
				'div' => false, 
				'label' => false,
				'placeholder' => __("Blood Group"),
				'value' =>@$user[0]['User']['userbloodgroup'],
				'class'=> 'form-control'
				)); ?>
            </div>
            <div class="form-group">
				<label for="data[User][usertelephone]">Mobile Number <span style=color:red;>*</span></label>
				<?php echo $this->Form->input("User.usertelephone", array(
				'type' => 'text', 
				'div' => false, 
				'label' => false,
				'placeholder' => __("Mobile Number, Personal"),
				'data-validation-engine'=>'validate[custom[onlyNumber]]',
				'value' =>@$user[0]['User']['usertelephone'],
				'class'=> 'form-control'
				)); ?>
            </div>
            <?php
	            echo $this->Form->input("User.usermobileofficial", array(
	            	'type' => 'text', 
	            	'div' => array('class'=>'form-group'), 
	            	'label'=> __('Mobile Number Official <span style=color:red;>*</span>'),
	            	'placeholder' => __("Mobile Number, Official"), 
	            	'style' => '',
	            	'data-validation-engine'=>'validate[custom[onlyNumber]]',
	            	'value' =>(@$user[0]['User']['usermobileofficial']),
					'class'=> 'form-control'
	            	));
	        ?>
	        <?php
	            echo $this->Form->input("User.userstaffid", array(
	            	'type' => 'text', 
	            	'div' => array('class'=>'form-group'), 
	            	'label'=> __('Employee Id'), 
	            	'placeholder' => __("Employee Id"),
	            	'style' => '',
	            	'value' =>(@$user[0]['User']['userstaffid']),
					'class'=> 'form-control'
	            	));
	        ?>
	        <?php
	            echo $this->Form->input("User.userpunchmachineid", array(
	            	'type' => 'text', 
	            	'div' => array('class'=>'form-group'), 
	            	'label'=> __('Punchmachine Id'), 
	            	'placeholder' => __("Punchmachine Id"),
	            	'style' => '',
	            	'value' =>(@$user[0]['User']['userpunchmachineid']),
					'class'=> 'form-control'
	            	));
	        ?>
            <div class="form-group">
				<label for="data[User][useremailaddress]">Email Address </label>
				<?php echo $this->Form->input("User.useremailaddress", array(
				'type' => 'email', 
				'div' => false, 
				'label' => false,
				'placeholder'=> 'someone@example.com',
				//'data-validation-engine'=>'validate[custom[email]]',
				'value' =>@$user[0]['User']['useremailaddress'],
				'class'=> 'form-control'
				)); ?>
            </div>
            <div class="form-group">
				<label for="data[User][usercareof]">C/O</label>
				<?php echo $this->Form->input("User.usercareof", array(
				'type' => 'text', 
				'div' => false, 
				'label' => false,
				'placeholder' => __("Care Of"),
				'value' =>@$user[0]['User']['usercareof'],
				'class'=> 'form-control'
				)); ?>
            </div>
            <div class="form-group form-inline">
				<?php
				$options = array(1 => __("Yes"), 0 =>__("No"));
				$attributes = array(
					'legend' => false, 
					'label' => true, 
					'value' => (@$user[0]['User']['userisactive'] ? $user[0]['User']['userisactive'] : 0),
					'class' => 'form-inline'
				);
				echo"<label>isActive?</label><br>";
				echo $this->Form->radio(
					'User.userisactive', 
					$options,
					$attributes
				);?>
            </div>
            <div class="form-group form-inline">
            	<?php
            	echo"<label>User Image</label>";
            	echo $this->Form->file('User.photo1', array(
            		'label'=>false,
                    'div'=>false
            		)
				);
            	?>
            	<input type="hidden" name="data[User][photo2]" value="<?php echo $u_image ?>"  />
            </div>	
            <hr>
			<?php echo $Utilitys->allformbutton('',$this->request["controller"],$page='user'); ?>
		</div>
	</div>
<?php print $this->Form->end(); ?> 
<?php 
echo "
<script type=\"text/javascript\">

	$(document).ready(function() {
		$(\"#UsersUserinsertupdateactionForm\").validationEngine();
		var GroupId = ".(@$user[0]["User"]["group_id"]?$user[0]["User"]["group_id"]:0).";
		if(GroupId == 1){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").show();
				$(\"#UserShiftId\").show();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").show();
				$(\"#UserUserjoiningdate\").show();
				$(\"#UserUserfiredate\").show();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").show();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").show();
				$(\"#UserUserpunchmachineid\").show();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
		if(GroupId == 2){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").show();
				$(\"#UserShiftId\").show();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").show();
				$(\"#UserUserjoiningdate\").show();
				$(\"#UserUserfiredate\").show();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").show();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").show();
				$(\"#UserUserpunchmachineid\").show();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
		if(GroupId == 3){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").show();
				$(\"#UserShiftId\").show();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").show();
				$(\"#UserUserjoiningdate\").show();
				$(\"#UserUserfiredate\").show();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").show();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").show();
				$(\"#UserUserpunchmachineid\").show();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}	
		if(GroupId == 4){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").hide();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUserdob\").hide();
				$(\"#UserUserdoboriginal\").hide();
				$(\"#UserUsermarriageday\").hide();
				$(\"#UserUserchilddob\").hide();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
		if(GroupId == 5){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").hide();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUserdob\").hide();
				$(\"#UserUserdoboriginal\").hide();
				$(\"#UserUsermarriageday\").hide();
				$(\"#UserUserchilddob\").hide();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}		
		if(GroupId == 6){
			$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").hide();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").show();
				$(\"#UserUserdob\").hide();
				$(\"#UserUserdoboriginal\").hide();
				$(\"#UserUsermarriageday\").hide();
				$(\"#UserUserchilddob\").hide();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").show();
				$(\"#UserUserpunchmachineid\").show();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
		}
		if(GroupId == 7){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").hide();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserdob\").hide();
				$(\"#UserUserdoboriginal\").hide();
				$(\"#UserUsermarriageday\").hide();
				$(\"#UserUserchilddob\").hide();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 8){
				$(\"#UserReferenceinstitution\").show();
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").hide();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").hide();
				$(\"#UserUserbloodgroup\").hide();
				$(\"#UserUsertelephone\").hide();
				$(\"#UserUsermobileofficial\").hide();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").show();
				$(\"#UserSubjectId\").show();
				$(\"#UserInstituteId\").show();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserParentid\").hide();
			}
		if(GroupId == 9){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").hide();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").hide();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").hide();
				$(\"#UserUserchilddob\").hide();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").hide();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").show();
				$(\"#UserUsercareof\").show();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").show();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
		if(GroupId == 10){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").show();
				$(\"#UserShiftId\").show();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").show();
				$(\"#UserUserjoiningdate\").show();
				$(\"#UserUserfiredate\").show();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").show();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").show();
				$(\"#UserUserpunchmachineid\").show();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}	
		if(GroupId == 11){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").show();
				$(\"#UserShiftId\").show();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").show();
				$(\"#UserUserjoiningdate\").show();
				$(\"#UserUserfiredate\").show();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").show();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").show();
				$(\"#UserUserpunchmachineid\").show();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").show();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").show();
			}
		if(GroupId == 12){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").hide();
				$(\"#UserUsermothername\").hide();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").hide();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").hide();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").show();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 13){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").hide();
				$(\"#UserUsermothername\").hide();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").hide();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").hide();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").show();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 14){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").show();
				$(\"#UserShiftId\").show();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").show();
				$(\"#UserUserjoiningdate\").show();
				$(\"#UserUserfiredate\").show();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").show();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").show();
				$(\"#UserUserpunchmachineid\").show();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 15){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").hide();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").hide();
				$(\"#UserUsermothername\").hide();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").hide();
				$(\"#UserUserdob\").hide();
				$(\"#UserUserdoboriginal\").hide();
				$(\"#UserUsermarriageday\").hide();
				$(\"#UserUserchilddob\").hide();
				$(\"#UserUsergender\").hide();
				$(\"#UserUserbloodgroup\").hide();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").hide();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").show();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 16){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").hide();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").hide();
				$(\"#UserUsermothername\").hide();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").hide();
				$(\"#UserUserdob\").hide();
				$(\"#UserUserdoboriginal\").hide();
				$(\"#UserUsermarriageday\").hide();
				$(\"#UserUserchilddob\").hide();
				$(\"#UserUsergender\").hide();
				$(\"#UserUserbloodgroup\").hide();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").hide();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").show();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 17){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").hide();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").hide();
				$(\"#UserUsermothername\").hide();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").hide();
				$(\"#UserUserdob\").hide();
				$(\"#UserUserdoboriginal\").hide();
				$(\"#UserUsermarriageday\").hide();
				$(\"#UserUserchilddob\").hide();
				$(\"#UserUsergender\").hide();
				$(\"#UserUserbloodgroup\").hide();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").hide();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").show();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 18){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").show();
				$(\"#UserShiftId\").show();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").show();
				$(\"#UserUserjoiningdate\").show();
				$(\"#UserUserfiredate\").show();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").show();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").show();
				$(\"#UserUserpunchmachineid\").show();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").show();
			}
		$(\"#UserUserdob\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd\", showMeridian: false , startView: 2, minView: 2, autoclose: 1
		});
		$(\"#UserUserdoboriginal\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd\", showMeridian: false , startView: 2, minView: 2, autoclose: 1
		});
		$(\"#UserUsermarriageday\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd\", showMeridian: false , startView: 2, minView: 2, autoclose: 1
		});
		$(\"#UserUserchilddob\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd\", showMeridian: false , startView: 2, minView: 2, autoclose: 1
		});
		$(\"#UserUserjoiningdate\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd\", showMeridian: false , startView: 2, minView: 2, autoclose: 1
		});
		$(\"#UserUserfiredate\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd\", showMeridian: false , startView: 2, minView: 2, autoclose: 1
		});
		$.post(
			'".$this->webroot."users/userlistbygroupid',
			{GroupId:".(@$user[0]["User"]["group_id"]?$user[0]["User"]["group_id"]:0).",salespersonid:".(@$user[0]["User"]["salespersonid"]?$user[0]["User"]["salespersonid"]:0)."},
			function(result) {
				$(\"#groupchangediv\").html(result);
			}
		);

		$(\"#UserCompanyId\").change(function(){
			var CompanyId = $(this).val();
			$(\"#branchchangediv\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
			$.post(
				'".$this->webroot."users/userbranchlistbycompany',
				{CompanyId:CompanyId},
				function(result) {
					$(\"#branchchangediv\").html(result);
					//$(\"#branchchangediv\").html('');
				}
			);
		});
		$(\"#UserGroupId\").change(function(){
			var GroupId = $(this).val();
			var CompanyId = $(\"#UserCompanyId\").val();
			$(\"#groupchangediv\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
			if(GroupId == 1){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").show();
				$(\"#UserShiftId\").show();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").show();
				$(\"#UserUserjoiningdate\").show();
				$(\"#UserUserfiredate\").show();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").show();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").show();
				$(\"#UserUserpunchmachineid\").show();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 2){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").show();
				$(\"#UserShiftId\").show();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").show();
				$(\"#UserUserjoiningdate\").show();
				$(\"#UserUserfiredate\").show();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").show();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").show();
				$(\"#UserUserpunchmachineid\").show();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}	
			if(GroupId == 3){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").show();
				$(\"#UserShiftId\").show();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").show();
				$(\"#UserUserjoiningdate\").show();
				$(\"#UserUserfiredate\").show();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").show();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").show();
				$(\"#UserUserpunchmachineid\").show();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 4){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").hide();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUserdob\").hide();
				$(\"#UserUserdoboriginal\").hide();
				$(\"#UserUsermarriageday\").hide();
				$(\"#UserUserchilddob\").hide();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 5){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").hide();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUserdob\").hide();
				$(\"#UserUserdoboriginal\").hide();
				$(\"#UserUsermarriageday\").hide();
				$(\"#UserUserchilddob\").hide();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 6){
			$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").hide();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").show();
				$(\"#UserUserdob\").hide();
				$(\"#UserUserdoboriginal\").hide();
				$(\"#UserUsermarriageday\").hide();
				$(\"#UserUserchilddob\").hide();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").show();
				$(\"#UserUserpunchmachineid\").show();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
		}
		if(GroupId == 7){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").hide();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUserdob\").hide();
				$(\"#UserUserdoboriginal\").hide();
				$(\"#UserUsermarriageday\").hide();
				$(\"#UserUserchilddob\").hide();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 8){
				$(\"#UserReferenceinstitution\").show();
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").hide();
				$(\"#UserUsermothername\").hide();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").hide();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").hide();
				$(\"#UserUserbloodgroup\").hide();
				$(\"#UserUsertelephone\").hide();
				$(\"#UserUsermobileofficial\").hide();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").show();
				$(\"#UserSubjectId\").show();
				$(\"#UserInstituteId\").show();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 9){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").hide();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").hide();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").hide();
				$(\"#UserUserchilddob\").hide();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").hide();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").show();
				$(\"#UserUsercareof\").show();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").show();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 10){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").show();
				$(\"#UserShiftId\").show();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").show();
				$(\"#UserUserjoiningdate\").show();
				$(\"#UserUserfiredate\").show();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").show();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").show();
				$(\"#UserUserpunchmachineid\").show();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 11){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").show();
				$(\"#UserShiftId\").show();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").show();
				$(\"#UserUserjoiningdate\").show();
				$(\"#UserUserfiredate\").show();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").show();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").show();
				$(\"#UserUserpunchmachineid\").show();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").show();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").show();
			}
			if(GroupId == 12){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").hide();
				$(\"#UserUsermothername\").hide();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").hide();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").hide();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").show();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 13){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").hide();
				$(\"#UserUsermothername\").hide();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").hide();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").hide();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").show();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 14){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").show();
				$(\"#UserShiftId\").show();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").show();
				$(\"#UserUserjoiningdate\").show();
				$(\"#UserUserfiredate\").show();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").show();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").show();
				$(\"#UserUserpunchmachineid\").show();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 15){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").hide();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").hide();
				$(\"#UserUsermothername\").hide();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").hide();
				$(\"#UserUserdob\").hide();
				$(\"#UserUserdoboriginal\").hide();
				$(\"#UserUsermarriageday\").hide();
				$(\"#UserUserchilddob\").hide();
				$(\"#UserUsergender\").hide();
				$(\"#UserUserbloodgroup\").hide();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").hide();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").show();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 16){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").hide();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").hide();
				$(\"#UserUsermothername\").hide();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").hide();
				$(\"#UserUserdob\").hide();
				$(\"#UserUserdoboriginal\").hide();
				$(\"#UserUsermarriageday\").hide();
				$(\"#UserUserchilddob\").hide();
				$(\"#UserUsergender\").hide();
				$(\"#UserUserbloodgroup\").hide();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").hide();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").show();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 17){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").hide();
				$(\"#UserShiftId\").hide();
				$(\"#UserDesignationId\").hide();
				$(\"#UserUserjoiningsalary\").hide();
				$(\"#UserUserjoiningdate\").hide();
				$(\"#UserUserfiredate\").hide();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").hide();
				$(\"#UserUsermothername\").hide();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").hide();
				$(\"#UserUserdob\").hide();
				$(\"#UserUserdoboriginal\").hide();
				$(\"#UserUsermarriageday\").hide();
				$(\"#UserUserchilddob\").hide();
				$(\"#UserUsergender\").hide();
				$(\"#UserUserbloodgroup\").hide();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").hide();
				$(\"#UserUserstaffid\").hide();
				$(\"#UserUserpunchmachineid\").hide();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").show();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").hide();
			}
			if(GroupId == 18){
				$(\"#UserCompanyId\").show();
				$(\"#UserBranchId\").show();
				$(\"#UserGroupId\").show();
				$(\"#UserDepartmentId\").show();
				$(\"#UserShiftId\").show();
				$(\"#UserDesignationId\").show();
				$(\"#UserUserjoiningsalary\").show();
				$(\"#UserUserjoiningdate\").show();
				$(\"#UserUserfiredate\").show();
				$(\"#UserUserfirstname\").show();
				$(\"#UserUsermiddlename\").show();
				$(\"#UserUserlastname\").show();
				$(\"#UserUserfathername\").show();
				$(\"#UserUsermothername\").show();
				$(\"#UserDivisionid\").show();
				$(\"#UserDistrictid\").show();
				$(\"#UserUpazilaid\").show();
				$(\"#UserUserpermanentaddress\").show();
				$(\"#UserUserpresentaddress\").show();
				$(\"#UserUsername\").show();
				$(\"#UserUserpassword\").show();
				$(\"#UserUserdob\").show();
				$(\"#UserUserdoboriginal\").show();
				$(\"#UserUsermarriageday\").show();
				$(\"#UserUserchilddob\").show();
				$(\"#UserUsergender\").show();
				$(\"#UserUserbloodgroup\").show();
				$(\"#UserUsertelephone\").show();
				$(\"#UserUsermobileofficial\").show();
				$(\"#UserUserstaffid\").show();
				$(\"#UserUserpunchmachineid\").show();
				$(\"#UserUseremailaddress\").show();
				$(\"#UserCategoryId\").hide();
				$(\"#UserSubjectId\").hide();
				$(\"#UserInstituteId\").hide();
				$(\"#UserUsercareof\").hide();
				$(\"#UserUserfacilities\").hide();
				$(\"#UserUsermeritposition\").hide();
				$(\"#UserReferenceinstitution\").hide();
				$(\"#UserParentid\").show();
			}
			$.post(
				'".$this->webroot."users/userlistbygroupid',
				{GroupId:GroupId,salespersonid:0},
				function(result) {
					$(\"#groupchangediv\").html(result);
				}
			);
			$.post(
				'".$this->webroot."users/designationlistbygroupid',
					{GroupId:GroupId,CompanyId:CompanyId},
					function(result) {
						$(\"#UserDesignationId\").html(result);
						$(\"#groupchangediv\").html('');
					}
			);
			$.post(
				'".$this->webroot."users/departmentlistbygroupid',
					{GroupId:GroupId,CompanyId:CompanyId},
					function(result) {
						$(\"#UserDepartmentId\").html(result);
						$(\"#groupchangediv\").html('');
					}
			);
			if(GroupId == 11 || GroupId == 18){
				$.post(
				'".$this->webroot."users/supervisorlistbygroupid',
					{GroupId:GroupId},
					function(result) {
						$(\"#UserParentid\").html(result);
						$(\"#groupchangediv\").html('');
					}
				);
			}
		});
		$(\"#UserDepartmentId\").change(function(){
			var GroupId = $(\"#UserGroupId\").val();
			var UserDepartmentId = $(\"#UserDepartmentId\").val();
			if(GroupId == 11 || GroupId == 18){
				$(\"#groupchangediv\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
			}
			if(GroupId == 11 || GroupId == 18){
				$.post(
					'".$this->webroot."users/supervisorlistbygroupidanddepartmentid',
					{GroupId:GroupId, UserDepartmentId:UserDepartmentId},
					function(result) {
						$(\"#UserParentid\").html(result);
						$(\"#groupchangediv\").html('');
					}
				);
			}
		});
		$(\"#UserDivisionid\").change(function(){
					var UserDivisionid = $(this).val();
					$(\"#locationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/districbydividion',
						{UserDivisionid:UserDivisionid},
						function(result) {
							$(\"#UserDistrictid\").html(result);
							$(\"#locationsarchloading\").html('');
						}
					);
		});
		$(\"#UserDistrictid\").change(function(){
					var UserDistrictid = $(this).val();
					$(\"#locationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/upazilabydistric',
						{UserDistrictid:UserDistrictid},
						function(result) {
							$(\"#UserUpazilaid\").html(result);
							$(\"#locationsarchloading\").html('');
						}
					);
		});
		
	});	
</script>
";
?>
	
