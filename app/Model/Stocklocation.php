<?php
App::uses('AppModel', 'Model');
class Stocklocation extends AppModel {
	public $name = 'Stocklocation';
	public $usetables = 'stocklocations';

	
	var $virtualFields = array(
		'stocklocation_name' => 'CONCAT(Stocklocation.stocklocationname, " / ", Stocklocation.stocklocationnamebn)',
		'isActive' => 'IF(Stocklocation.stocklocationisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Stocklocation.stocklocationisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
}