<?php echo $this->Form->create('Menu', array('action' => 'menuaddeditaction/'.@$menu[0]['Menu']['id'], 'type' => 'file')); ?>
<fieldset>
	<legend><h3>Add Menu</h3></legend>
	<table>
		<tr>
			<td>Name</td>
			<td>
				<?php

				 echo $this->Form->input("Menu.menuname", array(
				 	'type' => 'text',
				 	'maxlength' => '70',
				 	'div' => array('class'=>'form-group'),
				 	'class'=> 'form-control',
				 	'label' => false,
				 	'data-validation-engine'=>'validate[required]',
					'value' =>@$menu[0]['Menu']['menuname']
				 	));

				?>
			</td>
		</tr>
		<tr>
			<td>URL</td>
			<td>
				<?php

				 echo $this->Form->input("Menu.menuurl", array(
				 	'type' => 'text',
				 	'maxlength' => '100',
				 	'div' => array('class'=>'form-group'),
				 	'label' => false,
				 	'data-validation-engine'=>'validate[required]',
				 	'class'=> 'form-control',
					'value' =>@$menu[0]['Menu']['menuurl']
				 	));

				?>
			</td>
		</tr>
		<tr>
			<td>Is Parent</td>
			<td>
				<?php
					 $options = array( 1 => __(" Yes "), 0 => __(" No "));
					 $attributes = array(
					 	'legend' => false, 
					 	'value' => (@$menu[0]['Menu']['menuisparent'] ? $menu[0]['Menu']['menuisparent'] : 0), 
						'class' => 'form-inline'
					 	);
					 echo $this->Form->radio('Menu.menuisparent', $options, $attributes);
				?>
			</td>
		</tr>
		<tr>
            <td>Sub Menu Of:</td>
            <td>
                <?php
                $option1 = array($parentmenu);
                echo $this->Form->input("Menu.parentmenu", array(
                	'type' => 'select', 
                	"options" => $option1, 
                	'empty' => 'Select Parent Menu', 
                	'div' => array('class'=>'form-group'), 
                	'label' => false, 
                	"optgroup label" => false, 
                	'class'=> 'form-control',
                	'selected'=> (@$menu[0]['Menu']['parentmenu'] ? $menu[0]['Menu']['parentmenu'] : 0)
                	));
                ?>
            </td>
        </tr>
        <tr>
			<td>Is Permanent</td>
			<td>
				<?php
					 $options = array( 1 => __(" Yes "), 0 => __(" No "));
					 $attributes = array(
					 	'legend' => false,
					 	'value' => (@$menu[0]['Menu']['menuispermanent'] ? $menu[0]['Menu']['menuispermanent'] : 0), 
						'class' => 'form-inline');
					 echo $this->Form->radio('Menu.menuispermanent', $options, $attributes);
				?>
			</td>
		</tr>
		<tr>
			<td>Is Hidden</td>
			<td>
				<?php
					 $options = array( 1 => __(" Yes "), 0 => __(" No "));
					 $attributes = array(
					 	'legend' => false, 
					 	'value' => (@$menu[0]['Menu']['menuishidden'] ? $menu[0]['Menu']['menuishidden'] : 0), 
						'class' => 'form-inline');
					 echo $this->Form->radio('Menu.menuishidden', $options, $attributes);
				?>
			</td>
		</tr>
		<tr>
			<td>Is Active</td>
			<td>
				<?php
					 $options = array( 1 => __(" Yes "), 0 => __(" No "));
					 $attributes = array(
					 	'legend' => false, 
					 	'value' => (@$menu[0]['Menu']['menuisactive'] ? $menu[0]['Menu']['menuisactive'] : 0), 
						'class' => 'form-inline');
					 echo $this->Form->radio('Menu.menuisactive', $options, $attributes);
				?>
			</td>
		</tr>
		<tr>
            <td>&nbsp;</td>
            <td><?php echo $Utilitys->allformbutton('',$this->request["controller"],$page='menu');?>
            </td>
        </tr>
	</table>
</fieldset>


<?php echo $this->Form->end(); ?> 