<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart($class=null, $id=null);
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('Branch')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Branch BN')  => array('class' => 'highlight sortable')
				), 
				__('Active'), 
				__('Action')
			)
		);
		$bankbranchrows = array();
		$countBankbranch = 0;
		foreach($bankbranch as $Thisbankbranch):
			$countBankbranch++;
			$bankbranchrows[]= array($countBankbranch,$Thisbankbranch["Bankbranch"]["bankbranchname"],$Thisbankbranch["Bankbranch"]["bankbranchnamebn"],$Utilitys->statusURL($this->request["controller"], 'Bankbranch', $Thisbankbranch["Bankbranch"]["id"], $Thisbankbranch["Bankbranch"]["bankbranchuuid"],$Thisbankbranch["Bankbranch"]["bankbranchisactive"]),$Utilitys->cusurl($this->request["controller"], 'Bankbranch', $Thisbankbranch["Bankbranch"]["id"], $Thisbankbranch["Bankbranch"]["bankbranchuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$bankbranchrows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#BankbranchSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#bankbranchsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#bankbranchsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>