<?php
//pr($product);
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"3\">
				<h2>".__('Product Information')."</h2>
			</td>
		</tr>
		<tr>
			<th>
				".__('Product Code')."
			</th>
			<td>
				".$product[0]["Product"]["productcode"]."
			</td>
			<td rowspan=\"6\">
				".$this->html->image('/img/product/small/'.$product[0]["Product"]["productimage"], array('alt'=> 'Logo of '.$product[0]["Product"]["product_name"]))."
			</td>
		</tr>
		<tr>
			<th>
				".__('Product Name')."
			</th>
			<td>
				".$product[0]["Product"]["product_name"]."
			</td>
		</tr>
		
		<tr>
			<th>
				".__('Product Category')."
			</th>
			<td>
				".$product[0]["Product"]["productcategory_name"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Product Type')."
			</th>
			<td>
				".$product[0]["Product"]["producttype_name"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Product Price')."
			</th>
			<td>
				".$product[0]["Product"]["productprice"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Format')."
			</th>
			<td>
				".$product[0]["Product"]["format"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Unit')."
			</th>
			<td>
				".$product[0]["Product"]["unit_name"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Active")."
			</th>
			<td>
				".$product[0]["Product"]["isActive"]."
			</td>
		</tr>
	";
	echo $Utilitys->tableend($class=null, $id=null);
?>