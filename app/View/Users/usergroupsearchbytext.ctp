<?php
    echo $Utilitys->paginationcommon();
		echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'),
					array(
						__('User Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Group Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Company Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Branch Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Is Active')  => array('class' => 'highlight sortable')
					),
					__('Action')
				)
			);
			$usergrouprows = array();
			$countUserGroup = 0-1+$this->Paginator->counter('%start%');
			
			foreach($usergroup as $Thisusergroup):
				$countUserGroup++;
				$usergrouprows[]= array($countUserGroup,$Thisusergroup["User"]["userfirstname"]." ".$Thisusergroup["User"]["usermiddlename"]." ".$Thisusergroup["User"]["userlastname"],$Thisusergroup["Group"]["groupname"], $Thisusergroup["Company"]["companyname"]." / ".$Thisusergroup["Company"]["companynamebn"], $Thisusergroup["Branch"]["branchname"]." / ". $Thisusergroup["Branch"]["branchnamebn"], $Utilitys->statusURL($this->request["controller"], 'usergroup', $Thisusergroup["Usergroup"]["id"], $Thisusergroup["Usergroup"]["usergroupuuid"], $Thisusergroup["Usergroup"]["usergroupisactive"]),$Utilitys->cusurl($this->request['controller'], 'usergroup', $Thisusergroup["Usergroup"]["id"], $Thisusergroup["Usergroup"]["usergroupuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$usergrouprows
			);
		echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();

 ?>