	function cancelForm(url){
		window.location=url;
	}

/*! LEFT SIDEBAR AND SIDBEAR MENU */
!function ($) {
    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
        $(this).find('em:first').toggleClass("glyphicon-chevron-up");      
    }); 
    $(".left-sidebar span.icon").find('em:first').addClass("glyphicon-chevron-down");
}(window.jQuery);
$(window).on('resize', function () {
	if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
})
$(window).on('resize', function () {
	if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
})
