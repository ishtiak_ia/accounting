<?php
App::uses('AppModel', 'Model');
class Unittype extends AppModel {
	public $name = 'Unittype';
	public $usetables = 'unittypes';

	var $virtualFields = array(
		'unittype_name' => 'CONCAT(Unittype.unittypename, " / ", Unittype.unittypenamebn)',
		'isActive' => 'IF(Unittype.unittypeisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Unittype.unittypeisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
}