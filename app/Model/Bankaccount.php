<?php
App::uses('AppModel', 'Model');
class Bankaccount extends AppModel {
	public $name = 'Bankaccount';
	public $usetables = 'bankaccounts';
	var $belongsTo  = array(
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'bankaccountinsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'bankaccountupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'bankaccountdeleteid'
		),
		'Bank' => array(
			'fields' =>array('Bank.*'),
			'className'    => 'Bank',
			'foreignKey'    => 'bank_id'
		),
		'Bankbranch' => array(
			'fields' =>array('Bankbranch.*'),
			'className'    => 'Bankbranch',
			'foreignKey'    => 'bankbranch_id'
		),
		'Bankaccounttype' => array(
			'fields' =>array('Bankaccounttype.*'),
			'className'    => 'Bankaccounttype',
			'foreignKey'    => 'bankaccounttype_id'
		),
		'Company' => array(
			'fields' =>array('Company.*'),
			'className'    => 'Company',
			'foreignKey'    => 'company_id'
		),
		'Branch' => array(
			'fields' =>array('Branch.*'),
			'className'    => 'Branch',
			'foreignKey'    => 'branch_id'
		)
	);
	var $virtualFields = array(
		'company_name' => 'CONCAT(Company.companyname, " / ", Company.companynamebn)',
		'branch_name' => 'CONCAT(Branch.branchname, " / ", Branch.branchnamebn)',
		'bank_name' => 'CONCAT(Bank.bankname, " / ", Bank.banknamebn)',
		'bankbranch_name' => 'CONCAT(Bankbranch.bankbranchname, " / ", Bankbranch.bankbranchnamebn)',
		'bankaccounttype_name' => 'CONCAT(Bankaccounttype.bankaccounttypename, " / ", Bankaccounttype.bankaccounttypenamebn)',
		'bankaccount_name' => 'CONCAT(Bankaccount.bankaccountname, " / ", Bankaccount.bankaccountnamebn," - ", Bankaccount.bankaccountnumber)',
		'bankaccount_totalamount' => '(SELECT ROUND(SUM(banktransactions.transactionamount),2) FROM banktransactions WHERE banktransactions.bankaccount_id = Bankaccount.id)',
		'isActive' => 'IF(Bankaccount.bankaccountisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Bankaccount.bankaccountisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
	public $validate = array(
		'bankaccountname' => array(
			'bankaccountname_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This field is required',
				'last' => true
			)
		),
		'bankaccountnamebn' => array(
			'bankaccountnamebn_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This field is required',
				'last' => true
			)
		),
		'bankaccountnumber' => array(
			'bankaccountnumber_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This field is required',
				'last' => true
			)
		),
		'bank_id' => array(
			'bank_id_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This field is required',
				'last' => true
			)
		),
		'bankbranch_id' => array(
			'bankbranch_id_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This field is required',
				'last' => true
			)
		),
		'bankaccounttype_id' => array(
			'bankaccounttype_id_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This field is required',
				'last' => true
			)
		)
	);
}

?>