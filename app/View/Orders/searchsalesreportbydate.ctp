<?php
/*echo "<pre>";
print_r($orders);

echo "<pre>";
die();*/

echo $Utilitys->tablestart();
echo $this->Html->tableHeaders(
    array(
        __('SL#'),
		__('Date'),
        array(
            __('Client Name')  => array('class' => 'highlight sortable')
        ),
        array(
            __('Total Price')  => array('class' => 'highlight sortable')
        ),
		array(
			__('Bonus')  => array('class' => 'highlight sortable')
		),
		array(
			__('Original Price')  => array('class' => 'highlight sortable')
		),
		array(
			__('Transport Cost')  => array('class' => 'highlight sortable')
		),
		array(
			__('Grand Total')  => array('class' => 'highlight sortable')
		),
		array(
			__('Paid Amount')  => array('class' => 'highlight sortable')
		),
		array(
			__('Due')  => array('class' => 'highlight sortable')
		),


    )
);
$orderRows = array();
$countOrder = 0;

foreach($orders as $salesOrder):
	$countOrder++;

	$orderRows[]= array(
		$countOrder,
		$salesOrder["Order"]["orderdate"],
		$salesOrder["Order"]["client_name"],
		$salesOrder["Order"]["priceafterdiscount"],
		$salesOrder["Order"]["less"],
		$salesOrder["Order"]["priceafterdiscount"]-$salesOrder["Order"]["less"],
		$salesOrder["Order"]["transportation_cost"],
		$salesOrder["Order"]["transactionamount"],
		$salesOrder["Order"]["order_paymentamount"],
		$salesOrder["Order"]["order_dueamount"],
	);
endforeach;
echo $this->Html->tableCells(
	$orderRows
);
echo $Utilitys->tableend();

?>