<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart($class=null, $id=null);
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('CODE')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Type')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Group')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Group BN')  => array('class' => 'highlight sortable')
				), 
				__('Active'), 
				__('Action')
			)
		);
	if(count($coagroup)>0):
		$coagrouprows = array();
		$countCoagroup = 0-1+$this->Paginator->counter('%start%');
		foreach($coagroup as $Thiscoagroup):
			$countCoagroup++;
					$coagrouprows[]= array(
						$countCoagroup,
						$Thiscoagroup["Coagroup"]["coagroupcode"],
						$Thiscoagroup["Coatype"]["coatypename"]."/".$Thiscoagroup["Coatype"]["coatypenamebn"],
						$Thiscoagroup["Coagroup"]["coagroupname"],$Thiscoagroup["Coagroup"]["coagroupnamebn"],
						$Utilitys->statusURL($this->request["controller"], 'coagroup', $Thiscoagroup["Coagroup"]["id"], 
							$Thiscoagroup["Coagroup"]["coagroupuuid"], $Thiscoagroup["Coagroup"]["coagroupisactive"]),
						$Utilitys->cusurl($this->request["controller"], 'coagroup', $Thiscoagroup["Coagroup"]["id"], 
							$Thiscoagroup["Coagroup"]["coagroupuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$coagrouprows
		);
	else:
	echo"
		<tr><td colspan=\"6\">".__("Data Not Found!!!")."</td></tr>
	";
	endif;
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var coagroupname = $('#CoagroupCoagroupname').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{coagroupname:coagroupname},
						async: true,
						beforeSend: function(){
							$(\"#groupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#groupsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>