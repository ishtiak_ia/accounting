<?php
App::import('Controller', 'Logins');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');
class ShiftsController extends AppController {
	public $name = 'Shifts';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'Form' => array('className' => 'BootstrapForm'));
	public $components = array('RequestHandler','Session');
	public $uses = array('User', 'Departmenttime', 'Shift');

	var $Logins;
	
	function beforeFilter(){
	        $this->Logins =& new LoginsController;
	        $this->Logins->constructClasses();
	        $this->Logins->__validateLoginStatus();
	        $Utilitys = new UtilitysController;
	        $this->set('Utilitys',$Utilitys);
	        if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
    }
    public  function beforeRender(){
		$this->response->disableCache();//prevent useless warnings for Ajax
		if($this->RequestHandler->isAjax()){
			Configure::write('debug', 0);
		}
	}
	public function shiftinsertupdate($id = null, $uuid=null) {
		$this->layout='default';
		$this->set('title_for_layout', __('Insert/Update Shift').' | '.__(Configure::read('site_name')));
        /*if(!empty($id) && $id!=0):
			$coatype=$this->Coatype->find('all',array("fields" => "Coatype.*","conditions"=>array('Coatype.id'=>$id)));
			$this->set('coatype',$coatype);
		endif;*/
		if(!empty($id) && $id!=0) {
            $shift = $this->Shift->find('all', array("fields" => "Shift.*", "conditions" => array('Shift.id' => $id, 'Shift.shiftuuid'=>$uuid)));
            $this->set('shift', $shift);
        }
	}

	public function shiftinsertupdateaction($id=null, $uuid = null){
		$userID = $this->Session->read('User.id');
		$id = @$this->request->data['Shifts']['id'];
        $uuid = @$this->request->data['Shifts']['shiftuuid'];
		if ($this->request->is('post')) {
			//print_r($_POST); exit();
			if(!empty($id) && $id!=0):
                $this->request->data['Shift']['shiftupdateid']=$userID;
                $this->request->data['Shift']['shiftupdatedate']=date('Y-m-d H:i:s');
                $message = __('Update Successfully.');
                $this->Shift->id = $id;
               // $this->Session->setFlash('Update Successfully.', 'default', array('class' => 'alert alert-success'));
            else:
	            $now = date('Y-m-d H:i:s');
	        	$this->request->data['Shift']['shiftinsertid']=$userID;
	            $this->request->data['Shift']['shiftinsertdate'] = $now;
	            $this->request->data['Shift']['shiftuuid']=String::uuid();
	            $message = __('Save Successfully.');
            endif;
            if ($this->Shift->save($this->data)) {
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
			}
		}
		$this->redirect("/shifts/shiftmanage");

	}

	public function shiftmanage() {
		$this->layout = 'default';
		$this->set('title_for_layout', __('Shift Manage').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$ShiftGrid = array();
		if($group_id==1):			
		elseif($group_id==2):
			//$ShiftGrid[] = array('User.company_id'=>$company_id);
//			$departmentcompany[] = array('Department.company_id'=>$company_id);
			$ShiftGrid[] = array('NOT'=>array('Shift.shiftisactive'=>2));
		else:
			//$ShiftGrid[] = array('User.company_id'=>$company_id);
//			$departmentcompany[] = array('Department.company_id'=>$company_id);
			//$ShiftGrid[] = array('User.branch_id'=>$branch_id);
			//$ShiftGrid[] = array('User.group_id'=>$group_id);
			$ShiftGrid[] = array('NOT'=>array('Shift.shiftisactive'=>0));
		$ShiftGrid[] = array('NOT'=>array('Shift.shiftisactive'=>2));
		endif;

		$shifts_list=$this->Shift->find(
			'list', 
			array(
				"fields" => array("id","shiftname")
			)
		);
		$this->set('shifts_list',$shifts_list);

		$this->paginate = array(
            'fields' => 'Shift.*',
            'conditions' => $ShiftGrid,
            'order'  =>'Shift.id ASC',
            'limit' => 20
        );
        $shifts = $this->paginate('Shift');
        $this->set('shifts',$shifts);
	}

	public function shiftdetailsbyid($id=null,$uuid=null){
        $this->layout='ajax';
        $shifts=$this->Shift->find('all',array("fields" => "Shift.*","conditions"=>array('Shift.id'=>$id,'Shift.shiftuuid'=>$uuid)));
        $this->set('shifts',$shifts);
    }

    public function shiftsearchbytext($Searchtext=null){
		$this->layout='ajax';

		$Searchtext = @$this->request->data['Searchtext'];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$groupId=$_SESSION["User"]["group_id"];
		$userId=$_SESSION["User"]["id"];

		$shift_id = array();
		if($groupId==1):
			$shift_id[] = array('');
		elseif($groupId==2):
			$shift_id[] = array('Group.id'=>$groupId);
		else:
			$shift_id[] = array('Group.id'=>$groupId);
			//$user_id[] = array('User.branch_id'=>$branch_id);
		endif;
		if(!empty($Searchtext) && $Searchtext!=''):
			$shift_id[] = array(
				'OR' => array(
					'Shift.shiftname LIKE ' => '%'.$Searchtext.'%'
				)
			);
		else:
			$shift_id[] = array();
		endif;

		$this->paginate = array(
			'fields' => 'Shift.*',
			'order'  =>'Shift.shiftname ASC',
			"conditions"=>$shift_id,
			'limit' => 20
		);
		$shifts = $this->paginate('Shift');
		$this->set('shifts',$shifts);
	}

	public function shiftdelete($id = null,$uuid=null) {
		$this->layout='ajax';
		$userID = $this->Session->read('User.id');
		$this->request->data['Shift']['shiftdeleteid']=$userID;
		$this->request->data['Shift']['shiftdeletedate'] = date('Y-m-d H:i:s');
		$this->request->data['Shift']['shiftisactive']=2;
		$this->Shift->id = $id;
		$this->Shift->shiftuuid = $uuid;
		if ($this->Shift->save($this->data)) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/shifts/shiftmanage");
	}
}
