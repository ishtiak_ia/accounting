<?php 
	echo"
		<table>
			<tr>
				<td>
					<h2>".__('Add Product Type')."</h2>
				</td>
			</tr>
			<tr>
				<td>
	";
	echo $this->Form->create('Products', array('action' => 'producttypeinsertupdateaction'));
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$producttype[0]['Producttype']['id']));
		echo $this->Form->input('producttypeuuid', array('type'=>'hidden', 'value'=>@$producttype[0]['Producttype']['producttypeuuid']));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$producttype[0]['Producttype']['producttypeisactive'] ? $producttype[0]['Producttype']['producttypeisactive'] : 0), 
			'class' => 'form-inline'
		);
		echo $Utilitys->tablestart();
	echo"
						<tr>
							<td>".__("Product Type Name")."</td>
							<td>
								".$this->Form->input(
									"Producttype.producttypename",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'data-validation-engine'=>'validate[required]',
										'value' =>@$producttype[0]['Producttype']['producttypename']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("Product Type Name BN")."</td>
							<td>
								".$this->Form->input(
									"Producttype.producttypenamebn",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'value' => @$producttype[0]['Producttype']['producttypenamebn']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("Is Active")."</td>
							<td>
								".$this->Form->radio(
									'Producttype.producttypeisactive', 
									$options, 
									$attributes
								)."
							</td>
						</tr>
						<tr>
							<td colspan=\"2\">
							".$Utilitys->allformbutton('',$this->request["controller"],$page='producttype')."
							</td>
						</tr>
					</table>
	";
	echo $this->Form->end();
	echo"
				</td>
			</tr>
		</table>
	";
?>
<script type="text/javascript">
 jQuery(document).ready(function(){
    jQuery("#ProductsProducttypeinsertupdateactionForm").validationEngine();
 });
</script>