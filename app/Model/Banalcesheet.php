<?php
App::uses('AppModel', 'Model');
class Balancesheet extends AppModel {
	public $name = 'Balancesheet';
	public $usetables = 'gltransactions';
	var $belongsTo  = array(
		'Coa' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'Coa',
			'foreignKey'    => 'coa_id'
		),
		'Coagroup' => array(
			'fields' =>array('coagroup_name'),
			'className'    => 'Coagroup',
			'foreignKey'    => 'coagroup_id'
		),
		'Coatype' => array(
			'fields' =>array('coatype_name'),
			'className'    => 'Coatype',
			'foreignKey'    => 'coatype_id'
		)
	);
	var $virtualFields = array(
		'coa_name' => 'CONCAT(Coa.coaname, " / ", Coa.coanamebn)',
		'coatype_name' => 'CONCAT(Coatype.coatypename, " / ", Coatype.coatypenamebn)',
		'coagroup_name' => 'CONCAT(Coagroup.coagroupname, " / ", Coagroup.coagroupnamebn)',
	);
}

?>