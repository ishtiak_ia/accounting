<?php 
	echo $this->Form->create('Banks', array('action' => 'bankaccountinsertupdateaction'));
		echo $this->Form->inpute('id', array('type'=>'hidden', 'value'=>@$bankaccount[0]['Bankaccount']['id']));
		echo $this->Form->inpute('bankaccountuuid', array('type'=>'hidden', 'value'=>@$bankaccount[0]['Bankaccount']['bankaccountuuid']));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$bankaccount[0]['Bankaccount']['bankaccountisactive'] ? $bankaccount[0]['Bankaccount']['bankaccountisactive'] : 0), 
			'class' => 'form-inline'
		);
	echo"<h3>".__("Add Bank Account Type Information")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-8\">";
			echo $this->Form->input(
				"Bankaccount.bank_id",array(
					'type'=>'select',
					'div'=>array('class'=> 'form-group'),
					'label'=>__('Select Bank'),
					"options"=>array($bank),
					'empty'=>__('Select Bank'),
					'tabindex'=>1,
					'style'=>'',
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'selected'=> (@$bankaccount[0]['Bankaccount']['bank_id'] ? $bankaccount[0]['Bankaccount']['bank_id'] : 0)
				)
			);
			echo $this->Form->input(
				"Bankaccount.bankbranch_id",array(
					'type'=>'select',
					"options"=>array($bankbranch),
					'empty'=>__('Select Branch'),
					'div'=>array('class'=> 'form-group'),
					'tabindex'=>2,
					'label'=>__("Branch"),
					'style'=>'',
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'selected'=> (@$bankaccount[0]['Bankaccount']['bankbranch_id'] ? $bankaccount[0]['Bankaccount']['bankbranch_id'] : 0)
				)
			);
			echo $this->Form->input(
					"Bankaccount.bankaccounttype_id",array(
						'type'=>'select',
						"options"=>array($bankaccounttype),
						'empty'=>__('Select Account Type'),
						'div'=>array('class'=> 'form-group'),
						'tabindex'=>3,
						'label'=>__("Account Type"),
						'style'=>'',
						'class'=> 'form-control',
						'data-validation-engine'=>'validate[required]',
						'selected'=> (@$bankaccount[0]['Bankaccount']['bankaccounttype_id'] ? $bankaccount[0]['Bankaccount']['bankaccounttype_id'] : 0)
					)
				);
			echo $this->Form->input(
				"Bankaccount.bankaccountname",
				array(
					'type' => 'text', 
					'div' => array('class'=> 'form-group'), 
					'label' => __("Account"),
					'tabindex'=>4,
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'value' =>@$bankaccount[0]['Bankaccount']['bankaccountname']
				)
			);
			echo $this->Form->input(
				"Bankaccount.bankaccountnamebn",
				array(
					'type' => 'text', 
					'div' => array('class'=> 'form-group'), 
					'label' => __("Account BN"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'value' => @$bankaccount[0]['Bankaccount']['bankaccountnamebn']
				)
			);
			echo $this->Form->input(
				"Bankaccount.bankaccountnumber",
				array(
					'type' => 'text', 
					'div' => array('class'=> 'form-group'), 
					'label' => __("Number"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'value' => @$bankaccount[0]['Bankaccount']['bankaccountnumber']
				)
			);
			echo"<div class=\"form-group\">";
				echo"<label>isActive?</label><br>";
				echo $this->Form->radio(
					'Bankaccount.bankaccountisactive', 
					$options,
					$attributes
				);
			echo"</div>";
			echo $Utilitys->allformbutton('',$this->request["controller"],$page='bankaccount');
	echo $this->Form->end();
	echo"
		</div>
	</div>
		<script type=\"text/javascript\">
			jQuery(document).ready(function(){
				jQuery(\"#BanksBankaccountinsertupdateactionForm\").validationEngine();
			});
		</script>
	";
?>