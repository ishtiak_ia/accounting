<?php
	//echo "test";
	//echo $purchasedData["Order"]['id']; echo "<br>";
	//echo $purchasedData["Order"]['orderdate'];
	echo "<pre>";
	print_r($purchasedData);
	echo "</pre>";
	echo "<pre>";
	print_r($purchasedDetailsData);
	echo "</pre>";
	print_r($purchasedData['Order']['orderdate']);
?> 
<?php
echo $this->Form->create('Purchases', array('action' => 'purchaseinsertupdateaction', 'type' => 'file'));
// echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$purchase[0]['Order']['id']));
// echo $this->Form->input('orderuuid', array('type'=>'hidden', 'value'=>@$purchase[0]['Order']['orderuuid']));
echo"<h3>".__("Add New Purchase Return")."</h3>";
?>
<style>
.purchase_page label {
    display: block;
}
</style>
<div class="row text-center" style='overflow-x:scroll;'>
		<table id="hajji-meta-table" class="table table-condensed table-hover">
			<tbody>
				<tr class="sales-item-row purchase_page">
					<td>
						<?php
						echo $this->Form->input(
							"Order.orderdate",
							array(
								'type' => 'text',
								'label' =>__('Date'),
								'tabindex'=>1,
								'placeholder' => __("Date"),
								'class'=> 'form-control',
								'data-validation-engine'=>'validate[required]',
								'value' => $purchasedData['Order']['orderdate']
							)
						);
						?>
					</td>
					<td>
						<?php
						echo $this->Form->input(
							"Order.orderreferenceno",
							array(
								'type' => 'text',
								'label' =>__('Invoice'),
								'tabindex'=>2,
								'readonly' => 'readonly',
								'placeholder' => __("Invoice"),
								'class'=> 'form-control',
								'data-validation-engine'=>'validate[required]',
								'value' => $purchasedData['Order']['orderreferenceno']
							)
						);
						?>
					</td>
					<td>
						<?php
						echo $this->Form->input(
							"Order.clientcode",
							array(
								'type'=>'select',
								"options"=>array($supplierId),
								//'empty'=>__('Select Supplier'),
								'disabled'=>'disabled',
								'tabindex'=>3,
								'label'=>__('Supplier ID'),
								'data-validation-engine'=>'validate[required]',
								"selected" => $purchasedData['Order']['clientcode']
								//'value' => $purchasedData['Order']['clientcode']
							)
						);
						?>
					</td>
					<td>
						<?php
						echo $this->Form->input(
							"Order.client_id",
							array(
								'type'=>'select',
								"options"=>array($supplierlist),
								//'empty'=>__('Select Supplier'),
								'disabled'=>'disabled',
								'tabindex'=>4,
								'label'=>__('Supplier Name'),
								//'data-validation-engine'=>'validate[required]',
								"selected" => $purchasedData['Order']['client_id']
								//'value' => $purchasedData['Order']['client_id']
							)
						);
						?>
					</td>
					<td>
						<?php
						echo $this->Form->input(
							"Order.branch_id",
							array(
								'type'=>'select',
								"options"=>array($salescenter),
								//'empty'=>__('Select Stock Center'),
								'disabled'=>'disabled',
								'tabindex'=>5,
								'label'=>__('Stock Center'),
								//'onchange'=>'getItem(1)',
								'data-validation-engine'=>'validate[required]',
								"selected" => $purchasedData['Order']['branch_id']
								//'value' => $purchasedData['Order']['branch_id']
							)
						);
						?>
					</td>
				</tr>	
			</tbody>
		</table>	
			
	</div>
	<!-- .row -->

	<div class="row">
		<div class="table-responsive">
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>Item ID</th>
						<th>Item Name</th>
						<th>Quantity</th>
						<th>Unit Price</th>
						<th>Price</th>
						<th>Unit</th>
						<th>Discount (%)</th>
						<th>Discount Value</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody id='content'>

				</tbody>	
			</table>	
		</div>
	</div>	
	<!-- .row -->