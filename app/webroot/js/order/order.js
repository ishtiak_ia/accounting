$(function() {

    $("#OrderdetailQuantity").keyup(function(){
        var quantity = this.value; 

        if(quantity ==''){
            quantity = 0;
        }else{
            quantity = parseFloat(quantity);
        }

        var unitmon = $("#unitmon").val();
        var unitkilogram = $("#unitkilogram").val();
        var unitgram = $("#unitgram").val();

        $("#quantityspan").html('Input Stock : '+quantity * unitmon+' MON / '+quantity * unitkilogram+' KG / '+quantity * unitgram+' GRAM');

        $("#orderdetailmon").val(unitmon * quantity);  
        $("#orderdetailkg").val(unitkilogram * quantity);  
        $("#orderdetailgram").val(unitgram * quantity);  

        var price = $("#OrderdetailPrice").val();
        if(price ==''){
            price = 0;
        }else{
            price = parseFloat(price);
        }
        $("#orderdetailmonunitprice").val(price * quantity);
        $("#orderdetailkgunitprice").val(price * quantity);           
        $("#orderdetailgramunitprice").val(price * quantity);

        var discount_rate = $("#OrderdetailDiscount").val();
        if(discount_rate =='' || discount_rate == 'NaN'){
            discount_rate = 0;
        }else{
            discount_rate = parseFloat(discount_rate);
        }

        var total_price = (quantity * price);
        var discount = (discount_rate * total_price) / 100;
        var amount = (total_price - discount);
        $("#OrderdetailPrice").val(price);
        $("#OrderdetailAmount").val(amount);

        var Totalamount = $("#OrderdetailAmount").val();
        $("#OrderTotalAmount").val(Totalamount);               
    }); 
    $("#OrderdetailPrice").keyup(function(){
        var price = this.value; 

        if(price ==''){
                    price = 0;
                }else{
                    price = parseFloat(price);
                }
        var quantity = $("#OrderdetailQuantity").val();
                if(quantity ==''){
                    quantity = 0;
                }else{
                    quantity = parseFloat(quantity);
                } 

        $("#orderdetailmonunitprice").val(price * quantity);
        $("#orderdetailkgunitprice").val(price * quantity);           
        $("#orderdetailgramunitprice").val(price * quantity);

        var discount_rate = $("#OrderdetailDiscount").val();
        if(discount_rate =='' || discount_rate == 'NaN'){
            discount_rate = 0;
        }else{
            discount_rate = parseFloat(discount_rate);
        }

        var total_price = (quantity * price);
        var discount = (discount_rate * total_price) / 100;
        var amount = (total_price - discount);
        $("#OrderdetailAmount").val(amount);

        //var Totalamount = $("#OrderdetailAmount").val();
        $("#OrderTotalAmount").val(amount);        
    });
    $("#OrderdetailDiscount").keyup(function(){
        var discount_rate = this.value; 

        if(discount_rate =='' || discount_rate == 'NaN'){
                    discount_rate = 0;
                }else{
                    discount_rate = parseFloat(discount_rate);
                }
        var quantity = $("#OrderdetailQuantity").val();
                if(quantity ==''){
                    quantity = 0;
                }else{
                    quantity = parseFloat(quantity);
                }  
        var price = $("#OrderdetailPrice").val();
        if(price ==''){
            price = 0;
        }else{
            price = parseFloat(price);
        }

        var total_price = (quantity * price);
        var discount = (discount_rate * total_price) / 100;
        var amount = (total_price - discount);
        $("#OrderdetailAmount").val(amount);

        var Totalamount = $("#OrderdetailAmount").val();
        $("#OrderTotalAmount").val(Totalamount);               
    });

    $("#OrderBonusDiscount").keyup(function(){
        var bonus_discount = this.value;
        if(bonus_discount =='' || bonus_discount == 'NaN'){
            bonus_discount = 0;
        }else{
            bonus_discount = parseFloat(bonus_discount);
        }

        var total_amount = $("#OrderTotalAmount").val();
        if(total_amount =='' || total_amount == 'NaN'){
            total_amount = 0;
        }else{
            total_amount = parseFloat(total_amount);
        }

        //var g_total = (total_amount - bonus_discount);
        var g_discount = (bonus_discount * total_amount) / 100;
        var grand_total = (total_amount - g_discount);            
        $("#OrderGrandTotal").val(grand_total);
        if(bonus_discount>0){
        grand_total = (total_amount - g_discount);            
        }
        $("#OrderDue").val(grand_total);
    }); 
     
    $("#OrderPayment").keyup(function(){
        var payment = this.value;
        if(payment ==''){
            payment = 0;
        }else{
            payment = parseFloat(payment);
        }
        var grand_total = document.getElementById("OrderGrandTotal").value;
        var due = grand_total - payment;

        $("#OrderDue").val(due);
        if(due<0){
            alert('Due Amount Overflow!!');
        }else if(due==0){
            $("#OrderDuePaymentdate").css("display","none");            
            $("#OrderDuePaymentdate").val("0000-00-00 00:00:00");            
        }else{
            $("#OrderDuePaymentdate").css("display","block"); 
            $("#OrderDuePaymentdate").val("");            
        }
    });   
    
 });    