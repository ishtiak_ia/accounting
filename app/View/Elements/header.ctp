<?php //pr($this->request); ?>
<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="bn-BD"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="bn-BD"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="bn-BD"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="bn-BD"> <!--<![endif]-->
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $title_for_layout; ?>
        <?php //echo $cakeDescription ?>:
        <?php //echo $this->fetch('title'); ?>
    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Amanat Trading Accounting Software">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <!--[if lt IE 9]>
        <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php
    echo $this->fetch('meta');

    //echo $this->Html->css('cake.generic');
    echo $this->Html->css('bootstrap.min');
    echo $this->Html->css('bootstrap-switch.min');
    echo $this->Html->css('style');
    echo $this->Html->css('bootstrap-datetimepicker.min');
    echo $this->Html->css('prettyPhoto');
    echo $this->Html->meta('icon');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->css('select2');
    echo $this->fetch('css');
    echo $this->Html->script('jquery-1.11.1.min');
    echo $this->Html->script('libs/require/require');
    echo $this->Html->script('select2');


    //conditionally loading CSS scripts
    /*if( $this->CCC->is_page( 'Taxpayees', 'payeeassessment', $current_contoller, $current_action ) ) {
        echo $this->Html->css('datepicker3.min');
    }*/
    ?>
</head>
<body>
    
    <?php if( isset( $_SESSION['User']['id'] ) ) {
    //YES, the user is Logged in
    ?>
    <header>
        <nav class="navbar navbar-default navbar-fixed-top navbar-inverse" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php //echo $this->webroot->'/home'; ?>#">
                        <?php echo $this->html->image('/img/logo.svg', array('alt'=> 'Amanat Trading Accounting Software')); ?>
                        <span class="site-brand for-site">Amanat Trading Accounting Software</span>
                        <span class="site-brand for-mobile">isa</span>
                    </a>
                </div>
        
                <div class="collapse navbar-collapse" id="top-nav">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php //echo $this->webroot.$current_contoller.'/home'; ?>#"><span class="glyphicon glyphicon-dashboard"></span> <?php echo __('Dashboard'); ?></a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-question-sign"></span><?php echo __(' Help'); ?></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> <?php echo @$_SESSION['User']['username']; ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <?php echo $this->html->link(
                                            $this->html->tag(
                                                'span',
                                                '',
                                                array('class' => 'glyphicon glyphicon-th-large')
                                            ). __(' My Profile'),
                                            array(
                                                'controller' => 'Users',
                                                'action' => 'userprofile'
                                                ),
                                            array(
                                                'escape' => false
                                            )
                                        ); ?>
                                </li>
                                <li>
                                <?php echo $this->html->link(
                                            $this->html->tag(
                                                'span',
                                                '',
                                                array('class' => 'glyphicon glyphicon-cog')
                                            ). __(' Change Password'),
                                            array(
                                                'controller' => 'Users',
                                                'action' => 'userchangepassword'
                                                ),
                                            array(
                                                'escape' => false
                                            )
                                        ); ?>
                                </li>
                                <li class="divider"></li>
                                <li>
                                <?php echo $this->html->link(
                                            $this->html->tag(
                                                'span',
                                                '',
                                                array('class' => 'glyphicon glyphicon-off')
                                            ). __(' Log out'),
                                            array(
                                                'controller' => 'Logins',
                                                'action' => 'logout'
                                                ),
                                            array(
                                                'escape' => false
                                            )
                                        ); ?>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <?php if( isset( $_SESSION['User']['user_groupname'] ) ) { ?>
                        <p class="navbar-text navbar-right">[<?php echo $_SESSION['User']['user_groupname']; ?>]</p>
                    <?php } ?>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </header>
    <?php } //endif( isset( $_SESSION['User']['id'] ) ) ?>

    <div class="container-fluid">
        <div class="row">
        <!-- END OF HEADER.ctp -->
