<?php echo $this->Form->create('journalreports', array('default' => false, 'class'=>' ')); ?>
<fieldset>
    <legend><h3>Party Ledger</h3></legend>
    <table  border="0">
        <table  border="0">
            <tr>
                <td>User Group:</td>
                <td>
                    <?php
                    echo $this->Form->input(
                        "Partyledger.group_id",
                        array(
                            'type'=>'select',
                            'options'=>array($group_id),
                            'empty'=>__('Select Code'),
                            'div'=> array('class'=> 'form-group'),
                            'tabindex'=>7,
                            'label'=>'',
                            'class'=> '',
                            'style'=>'width:250px;',
                            'data-validation-engine'=>'validate[required]'
                        )
                    );
                    ?>
                </td>
                <td>&nbsp;</td>
                <td>
                    <?php
                    echo $this->Form->input(
                        "Partyledger.group",
                        array(
                            'type'=>'select',
                            'options'=>array($group),
                            'empty'=>__('Select Group'),
                            'div'=> array('class'=> 'form-group'),
                            'class'=> '',
                            'label'=>'',
                            'style'=>'width:250px;',
                            'data-validation-engine'=>'validate[required]'
                        )
                    );
                    ?>
                </td>
            </tr>
            <tr>
                <td>Member:</td>
                <td>
                    <?php
                     echo $this->Form->input(
                        "Partyledger.user_id",
                        array(
                            'type'=>'select',
                            'options'=>'',
                            'empty'=>__('Select Code'),
                            'div'=> array('class'=> 'form-group'),
                            'label'=>'',
                            'class'=> '',
                            'style'=>'width:250px;',
                            'data-validation-engine'=>'validate[required]'
                        )
                    );

                    ?>
                </td>
                <td>&nbsp;</td>
                <td>
                    <?php
                    echo $this->Form->input(
                        "Partyledger.user",
                        array(
                            'type'=>'select',
                            'empty'=>__('Select Member'),
                            'div'=> array('class'=> 'form-group'),
                            'class'=> '',
                            'label'=>'',
                            'style'=>'width:250px;',
                            'data-validation-engine'=>'validate[required]'
                        )
                    );
                    ?>
                </td>
            </tr>
            <tr>
                <td>Date:</td>
                <td>
                    <?php
                    echo $this->Form->input(
                        "Partyledger.fromdate",
                        array(
                            'type' => 'text',
                            'div' => array('class'=> 'form-group'),
                            'label' =>'',
                            'placeholder' => 'From',
                            'class'=> 'form-control',
                            'data-validation-engine'=>'validate[required]',
                            'style' => 'width:250px;'
                        )
                    );
                    ?>
                </td>
                <td>&nbsp;</td>
                <td>
                    <?php
                    echo $this->Form->input(
                        "Partyledger.todate",
                        array(
                            'type' => 'text',
                            'div' => array('class'=> 'form-group'),
                            'label' =>'',
                            'placeholder' => 'To',
                            'class'=> 'form-control',
                            'data-validation-engine'=>'validate[required]',
                            'style' => ''
                        )
                    );
                    ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php
                    echo $this->Form->submit('View', array('div' => array('class'=> 'form-group'), 'align' => 'right', 'formnovalidate' => 'true', 'id' => 'submit_button', 'class'=> ''));
                    ?>
                </td>
            </tr>
        </table>
</fieldset>
<br>
<br>
<?php echo $this->form->end(); ?>

<?php
echo "<div id='print_page' class='right-aligned-texts'>";
echo $Utilitys->printbutton ($printableid="loadvoucher", $searchfield=null, $pageformat="c3", $orientation="l", $unit=null, $companyname=$_SESSION["User"]["company_name"], $tabletitle="yes", $tablemonth="yes", $tabledepartment="yes", $printsectoin="printtwotable");
echo "</div>";

?>

<div id="loadvoucher">

</div>


<script type="text/javascript">
    $(document).ready(function() {
        var host = "<?php echo $this->webroot; ?>";
        $("#PartyledgerGroupId").select2();
        $("#PartyledgerGroup").select2();
        $("#PartyledgerUserId").select2();
        $("#PartyledgerUser").select2();
        $("#print_page").hide();
        $("#PartyledgerFromdate").datetimepicker({language: "bn",format: "yyyy-mm-dd", use24hours: true, showMeridian: false, startView: 2, minView: 2, autoclose: 1});
        $("#PartyledgerTodate").datetimepicker({language: "bn",format: "yyyy-mm-dd", use24hours: true, showMeridian: false, startView: 2, minView: 2, autoclose: 1});

        $("#PartyledgerGroupId").change(function(){
            var group_code = $(this).val(),group;
            $('#PartyledgerGroup option[value='+group_code+']').attr('selected', 'selected');
            group = $('#PartyledgerGroup option:selected').text();
            $('#s2id_PartyledgerGroup .select2-chosen').text(group);
            $.post(
                host + 'journalreports/userlistbygroup',
                {GroupId:group_code},
                function(result) {
                    $("#PartyledgerUser").html(result);

                }
            );


        });

        $("#PartyledgerGroup").change(function(){
            var group_id = $(this).val(),group;
            $('#PartyledgerGroupId option[value='+group_id+']').attr('selected', 'selected');
            group = $('#PartyledgerGroupId option:selected').text();
            $('#s2id_PartyledgerGroupId .select2-chosen').text(group);
            $.post(
                host + 'journalreports/userlistbygroup',
                {GroupId:group_id},
                function(result) {
                    $("#PartyledgerUser").html(result);

                }
            );


        });

        $("#submit_button").click(function(){
            var user_id = $("#PartyledgerUser").val();
            var from_date = $("#PartyledgerFromdate").val();
            var to_date = $("#PartyledgerTodate").val();
            if(user_id!='' && from_date!='' && to_date!=''){
                $("#print_page").hide();
                $("#loadvoucher").html("<img  src="+host+ "img/indicator.gif />");
                $.post(
                    host + 'journalreports/partyledgergenerate',
                    {user_id:user_id,from_date:from_date,to_date:to_date},
                    function(result) {
                        $("#print_page").show();
                        $("#loadvoucher").html(result);
                    }
                );

            }else
                alert('User, Form date and To date can not be empty');
        });

    });
</script>
