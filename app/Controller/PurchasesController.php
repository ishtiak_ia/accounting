<?php
App::import('Controller', 'Logins');
App::import('Controller', 'Transactions');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');

class PurchasesController extends AppController {
	public $name = 'Purchases';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'Form' => array('className' => 'BootstrapForm'));
	public $components = array('RequestHandler','Session', 'Image');
	public $uses = array('Company','Bankaccount','Bank', 'Banktransaction', 'Bankchequebook','Location','Product', 'Productbranch', 'User', 'Usergroup','Branch', 'Order','Orderdetail', 'Unit', 'Cashtransaction', 'Coa', 'Usertransaction', 'Gltransaction', 'Journaltransaction');
	var $Logins;
	var $Transactions;

	function beforeFilter(){
		$this->Logins =& new LoginsController;
		$this->Logins->constructClasses();
		$this->Logins->__validateLoginStatus();
		//$this->__validateLoginStatus();
		//$this->recordActivity();
		// $this->Auth->allow('register');
		$this->Transactions =& new TransactionsController;
		$this->Transactions->constructClasses();
		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	public function purchaseinsertupdate($id = null, $uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Purchase Order').' | '.__(Configure::read('site_name')));
		//print_r($_SESSION); //exit();
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$bankaccount_id = array();
		$bankchequebook_id = array();
		$productlist_id = array();
		$customerlist_id = array();
		$supplierlist_id = array();
		$salescenter_id = array();
		$salesmanlist_id = array();
		if($group_id==1):
		elseif($group_id==2):
			//$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			//$bankchequebook_id[] = array('Bankchequebook.company_id'=>$company_id);
			$productlist_id[] = array('Product.company_id'=>$company_id);
			$customerlist_id[] = array('User.company_id'=> $company_id);
			$supplierlist_id[] = array('Usergroup.company_id'=> $company_id);
			$salescenter_id[] = array('Branch.company_id'=> $company_id);
			$salesmanlist_id[] = array('User.company_id'=> $company_id);
		else:
			// $bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			// $bankaccount_id[] = array('Bankaccount.branch_id'=>$branch_id);
			// $bankchequebook_id[] = array('Bankchequebook.company_id'=>$company_id);
			// $bankchequebook_id[] = array('Bankchequebook.branch_id'=>$branch_id);
			$productlist_id[] = array('Product.company_id'=>$company_id);
			$productlist_id[] = array('Product.branch_id'=>$branch_id);
			$customerlist_id[] = array('User.company_id'=> $company_id);
			$customerlist_id[] = array('User.branch_id'=> $branch_id);
			$supplierlist_id[] = array('Usergroup.company_id'=> $company_id);
			$supplierlist_id[] = array('Usergroup.branch_id'=> $branch_id);
			$salescenter_id[] = array('Branch.company_id'=> $company_id);
			$salescenter_id[] = array('Branch.id'=> $branch_id);
			$salesmanlist_id[] = array('User.company_id'=> $company_id);
			$salesmanlist_id[] = array('User.branch_id'=> $branch_id);
		endif;
		$supplierlist_id[] = array('Usergroup.group_id'=> 7);
		$supplierlist_id[] = array('Usergroup.usergroupisactive'=> 1);
		$salesmanlist_id[] = array('User.group_id'=> 5);
		$supplierlist = $this->Usergroup->find(
	    		'list',array(
	    			"fields" => array("Usergroup.user_id", "Usergroup.user_fullname"),
	    			"conditions" =>$supplierlist_id,
	    			'recursive' => 1
	    		)
	    	);
		$this->set('supplierlist',$supplierlist);

		$supplierId = $this->Usergroup->find(
			'list',
			array(
				"fields" => array("Usergroup.user_id", "Usergroup.user_id"),
				"conditions" =>$supplierlist_id,
				"order" => "Usergroup.user_fullname ASC",
				'recursive' => 1
			)
		);
		$this->set('supplierId',$supplierId);
		$salescenter = $this->Branch->find(
			'list',
			array(
				"fields" => array("branch_name"),
				"conditions" =>$salescenter_id
			)
		);
		$this->set('salescenter',$salescenter);

		$salesmanlist = $this->User->find(
			'list',
			array(
				"fields" => array("User.id","user_fullname"),
				"conditions" =>$salesmanlist_id
			)
		);
		$this->set('salesmanlist',$salesmanlist);
		$coa_code=$this->Coa->find(
			'list',
			array(
				"fields" => array("id","coacode"),
				"conditions" => array('NOT'=>array('Coa.coaisactive'=>array(2)), 'Coa.coaclass_id' => 1)
			)
		);
		$this->set('coa_code',$coa_code);
		$coa=$this->Coa->find(
			'list',
			array(
				"fields" => array("id","coa_name"),
				"conditions" => array('NOT'=>array('Coa.coaisactive'=>array(2)), 'Coa.coaclass_id' => 1)
			)
		);
		$this->set('coa',$coa);

		//$productlist_id[] = array('Product.producttype_id'=>4);

		$productlist = $this->Product->find(
			'list', array(
				"fields" => array("Product.id", "Product.product_name"),
				"conditions" => $productlist_id
			)
		);
		$this->set('productlist',$productlist);

	    $productcodelist = $this->Product->find(
	    	'list',
	    	array(
	    		"fields" => array("Product.id","Product.productcode"),
				"conditions" => $productlist_id,
	    	)
	    );
	    $this->set('productcodelist',$productcodelist);

	    $unitlistInProductTable = $this->Product->find(
	    	'list',
	    	array(
	    		"fields" => array("Product.unit_id")
	    	)
	    );
	    $this->set('unitlistInProductTable',$unitlistInProductTable);
	    //print_r($unitlistInProductTable);
	    //var_dump($unitlistInProductTable);
	    //print_r(array_unique($unitlistInProductTable));

	    $condition_productunit = array();
		if($group_id==1):
			$condition_productunit[] = array('');
		elseif($group_id==2):
			$condition_productunit[] = array('Unit.company_id'=>$company_id);
		else:
			$condition_productunit[] = array('Unit.company_id'=>$company_id);
			//$condition_productunit[] = array('Product.branch_id'=>$branch_id);
		endif;

	    $unitlist = $this->Unit->find(
			'list',
			array(
				"fields" => array("Unit.id","Unit.unit_name"),
				"conditions" => $condition_productunit

				//"conditions" => array('Unit.id'=>array(1,2,3)),
			)
		);
		$this->set('unitlist',$unitlist);
	    
	    $locationlist = $this->Location->find(
	    	'list',
	    	array(
	    		"fields" => array("Location.id","Location.location_name"),
                "conditions" =>array('Location.locationstep'=>4)
            )
        );
        $this->set('locationlist',$locationlist);

        //get voucher no code start
		$current_month = date('Y-m');
		$voucher_no = $this->Journaltransaction->find('first', array('fields' => array("max(voucher_no) as voucher_no"),'conditions' => array('Journaltransaction.voucher_no LIKE' => '%'.$current_month.'%'))); 
		$this->set('voucher_no',$voucher_no);
		//get voucher no code end


		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
	    	$purchase = $this->Order->find('all', array("fields" => "Order.*", "conditions" => array('Order.id' => $id, 'Order.orderuuid'=>$uuid)));
	    	$this->set('purchase', $purchase);

    		$orderdetails_data = $this->Orderdetail->find('all', array('fields' => 'Orderdetail.*', 'conditions'=>array('Orderdetail.transaction_id'=>$id)));
			$this->set('orderdetails_data', $orderdetails_data);

			$product_code_edit = $this->Productbranch->find(
				'list',
				array(
					"fields" => array("Productbranch.id","Productbranch.product_code"),
					'conditions'=>array('Productbranch.branch_id'=> $purchase[0]['Order']['branch_id']),
					'recursive' => 1
				)
			);
			$this->set('product_code_edit', $product_code_edit);

			$product_list_edit = $this->Productbranch->find(
				'list',
				array(
					"fields" => array("Productbranch.id","Productbranch.product_name"),
					'conditions'=>array('Productbranch.branch_id'=> $purchase[0]['Order']['branch_id']),
					'recursive' => 1
				)
			);
			$this->set('product_list_edit', $product_list_edit);

			//print_r($product_code_edit);
	    }
	}
	public function purchasereturnmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Purchase Return Manage').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$condition = array();
		
		if($group_id==1):
		elseif($group_id==2):
			$condition[] = array('Order.company_id'=>$company_id);			
		else:
			$condition[] = array('Order.company_id'=>$company_id);
			$condition[] = array('Order.branch_id'=>$branch_id);			
		endif;
		$condition[] = array('Order.transactiontype_id'=>$_SESSION["ST_PURCHASERETURN"]);
		$this->paginate = array(
			'fields' => 'Order.*',
			'conditions'=> $condition ,
			'order'  =>'Order.id DESC',
			'limit' => 20
		);
		$order = $this->paginate('Order');
		$this->set('order',$order);
	}
	public function purchasereturninsertupdate($id = null, $uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Purchase Return').' | '.__(Configure::read('site_name')));
		//print_r($_SESSION); //exit();
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$bankaccount_id = array();
		$bankchequebook_id = array();
		$productlist_id = array();
		$customerlist_id = array();
		$supplierlist_id = array();
		$salescenter_id = array();
		$salesmanlist_id = array();
		if($group_id==1):
		elseif($group_id==2):
			//$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			//$bankchequebook_id[] = array('Bankchequebook.company_id'=>$company_id);
			$productlist_id[] = array('Product.company_id'=>$company_id);
			$customerlist_id[] = array('User.company_id'=> $company_id);
			$supplierlist_id[] = array('Usergroup.company_id'=> $company_id);
			$salescenter_id[] = array('Branch.company_id'=> $company_id);
			$salesmanlist_id[] = array('User.company_id'=> $company_id);
		else:
			// $bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			// $bankaccount_id[] = array('Bankaccount.branch_id'=>$branch_id);
			// $bankchequebook_id[] = array('Bankchequebook.company_id'=>$company_id);
			// $bankchequebook_id[] = array('Bankchequebook.branch_id'=>$branch_id);
			$productlist_id[] = array('Product.company_id'=>$company_id);
			$productlist_id[] = array('Product.branch_id'=>$branch_id);
			$customerlist_id[] = array('User.company_id'=> $company_id);
			$customerlist_id[] = array('User.branch_id'=> $branch_id);
			$supplierlist_id[] = array('Usergroup.company_id'=> $company_id);
			$supplierlist_id[] = array('Usergroup.branch_id'=> $branch_id);
			$salescenter_id[] = array('Branch.company_id'=> $company_id);
			$salescenter_id[] = array('Branch.id'=> $branch_id);
			$salesmanlist_id[] = array('User.company_id'=> $company_id);
			$salesmanlist_id[] = array('User.branch_id'=> $branch_id);
		endif;
		$supplierlist_id[] = array('Usergroup.group_id'=> 7);
		$supplierlist_id[] = array('Usergroup.usergroupisactive'=> 1);
		$salesmanlist_id[] = array('User.group_id'=> 5);
		$supplierlist = $this->Usergroup->find(
	    		'list',array(
	    			"fields" => array("Usergroup.user_id", "Usergroup.user_fullname"),
	    			"conditions" =>$supplierlist_id,
	    			'recursive' => 1
	    		)
	    	);
		$this->set('supplierlist',$supplierlist);

		$supplierId = $this->Usergroup->find(
			'list',
			array(
				"fields" => array("Usergroup.user_id", "Usergroup.user_id"),
				"conditions" =>$supplierlist_id,
				"order" => "Usergroup.user_fullname ASC",
				'recursive' => 1
			)
		);
		$this->set('supplierId',$supplierId);
		$salescenter = $this->Branch->find(
			'list',
			array(
				"fields" => array("branch_name"),
				"conditions" =>$salescenter_id
			)
		);
		$this->set('salescenter',$salescenter);

		$salesmanlist = $this->User->find(
			'list',
			array(
				"fields" => array("User.id","user_fullname"),
				"conditions" =>$salesmanlist_id
			)
		);
		$this->set('salesmanlist',$salesmanlist);
		$coa_code=$this->Coa->find(
			'list',
			array(
				"fields" => array("id","coacode"),
				"conditions" => array('NOT'=>array('Coa.coaisactive'=>array(2)), 'Coa.coaclass_id' => 1)
			)
		);
		$this->set('coa_code',$coa_code);
		$coa=$this->Coa->find(
			'list',
			array(
				"fields" => array("id","coa_name"),
				"conditions" => array('NOT'=>array('Coa.coaisactive'=>array(2)), 'Coa.coaclass_id' => 1)
			)
		);
		$this->set('coa',$coa);

		//$productlist_id[] = array('Product.producttype_id'=>4);

		$productlist = $this->Product->find(
			'list', array(
				"fields" => array("Product.id", "Product.product_name"),
				"conditions" => $productlist_id
			)
		);
		$this->set('productlist',$productlist);

	    $productcodelist = $this->Product->find(
	    	'list',
	    	array(
	    		"fields" => array("Product.id","Product.productcode"),
				"conditions" => $productlist_id,
	    	)
	    );
	    $this->set('productcodelist',$productcodelist);

	    $unitlistInProductTable = $this->Product->find(
	    	'list',
	    	array(
	    		"fields" => array("Product.unit_id")
	    	)
	    );
	    $this->set('unitlistInProductTable',$unitlistInProductTable);
	    //print_r($unitlistInProductTable);
	    //var_dump($unitlistInProductTable);
	    //print_r(array_unique($unitlistInProductTable));

	    $condition_productunit = array();
		if($group_id==1):
			$condition_productunit[] = array('');
		elseif($group_id==2):
			$condition_productunit[] = array('Unit.company_id'=>$company_id);
		else:
			$condition_productunit[] = array('Unit.company_id'=>$company_id);
			//$condition_productunit[] = array('Product.branch_id'=>$branch_id);
		endif;

	    $unitlist = $this->Unit->find(
			'list',
			array(
				"fields" => array("Unit.id","Unit.unit_name"),
				"conditions" => $condition_productunit

				//"conditions" => array('Unit.id'=>array(1,2,3)),
			)
		);
		$this->set('unitlist',$unitlist);
	    
	    $locationlist = $this->Location->find(
	    	'list',
	    	array(
	    		"fields" => array("Location.id","Location.location_name"),
                "conditions" =>array('Location.locationstep'=>4)
            )
        );
        $this->set('locationlist',$locationlist);

        //get voucher no code start
		$current_month = date('Y-m');
		$voucher_no = $this->Journaltransaction->find('first', array('fields' => array("max(voucher_no) as voucher_no"),'conditions' => array('Journaltransaction.voucher_no LIKE' => 'PR-'.$current_month.'%'))); 
		$this->set('voucher_no',$voucher_no);
		//get voucher no code end


		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
	    	$purchase = $this->Order->find('all', array("fields" => "Order.*", "conditions" => array('Order.id' => $id, 'Order.orderuuid'=>$uuid)));
	    	$this->set('purchase', $purchase);

    		$orderdetails_data = $this->Orderdetail->find('all', array('fields' => 'Orderdetail.*', 'conditions'=>array('Orderdetail.transaction_id'=>$id)));
			$this->set('orderdetails_data', $orderdetails_data);

			$product_code_edit = $this->Productbranch->find(
				'list',
				array(
					"fields" => array("Productbranch.id","Productbranch.product_code"),
					'conditions'=>array('Productbranch.branch_id'=> $purchase[0]['Order']['branch_id']),
					'recursive' => 1
				)
			);
			$this->set('product_code_edit', $product_code_edit);

			$product_list_edit = $this->Productbranch->find(
				'list',
				array(
					"fields" => array("Productbranch.id","Productbranch.product_name"),
					'conditions'=>array('Productbranch.branch_id'=> $purchase[0]['Order']['branch_id']),
					'recursive' => 1
				)
			);
			$this->set('product_list_edit', $product_list_edit);

	    }
	}
	public function purchasereturninsertupdateaction($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		$companyID = $this->Session->read('User.company_id');
		$branch_id=$_SESSION["User"]["branch_id"];

		$id = @$this->request->data['Purchases']['id'];
		$uuid = @$this->request->data['Purchases']['orderuuid'];

		$transactiontype_id=$_SESSION["ST_PURCHASERETURN"];
		$comment = $this->request->data['Order']['ordercomment'];
		$orderreferenceno = $this->request->data["Order"]["orderreferenceno"];
		$client_id = $this->request->data["Order"]["client_id"];
		$transactiondate = $this->request->data["Order"]["orderdate"];
		$grand_total = $this->request->data['Order']['ordergrandtotal'];
		$paid_amount = $this->request->data['Order']['orderpayment'];
		$user_coa = $this->request->data['Order']['coa_id'];

		if($this->request->is('post')){
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
				$this->request->data['Order']['company_id'] = $companyID;
				$this->request->data['Order']['branch_id'] = $this->request->data["Order"]["branch_id"];
				$this->request->data['Order']['clientcode'] = $this->request->data["Order"]["clientcode"];
				$this->request->data['Order']['transactiontype_id'] = $transactiontype_id;
				$this->request->data['Order']['client_id'] = $client_id;
				$this->request->data['Order']['user_id'] = $userID;
				$this->request->data['Order']['orderreferenceno']=$orderreferenceno;
				$this->request->data['Order']['orderdate']=$transactiondate;
				//$this->request->data['Order']['salesman_id']=$this->request->data["Order"]["salesman_id"];
				//$this->request->data['Order']['ordercustomerphone']=$this->request->data["Order"]["ordercustomerphone"]?$this->request->data["Order"]["ordercustomerphone"]:0;
				$this->request->data['Order']['ordertotal']=$this->request->data["Order"]["ordertotal"];
				$this->request->data['Order']['orderdiscount']=$this->request->data['Order']['orderdiscount'];
				$this->request->data['Order']['transactionamount']=$grand_total;
				$this->request->data['Order']['orderpayment']=$paid_amount;
				$this->request->data['Order']['orderdue']=$this->request->data['Order']['orderdue'];
				$this->request->data['Order']['orderduepaymentdate']=$this->request->data['Order']['orderduepaymentdate']?$this->request->data['Order']['orderduepaymentdate']:"0000-00-00 00:00:00";
				$this->request->data['Order']['ordercomment']=$comment;
				$this->request->data['Order']['orderisactive'] = 1;
				$this->request->data['Order']['orderupdateid'] = $userID;
				$this->request->data['Order']['orderupdatedate']=date('Y-m-d H:i:s');
				$this->Order->id = $id;
			}else{
				$this->request->data['Order']['orderuuid'] = String::uuid();
				$this->request->data['Order']['company_id'] = $companyID;
				$this->request->data['Order']['branch_id'] = $this->request->data["Order"]["branch_id"];
				$this->request->data['Order']['clientcode'] = $this->request->data["Order"]["clientcode"];
				$this->request->data['Order']['transactiontype_id'] = $transactiontype_id;
				$this->request->data['Order']['client_id'] = $client_id;
				$this->request->data['Order']['user_id'] = $userID;
				$this->request->data['Order']['orderreferenceno']=$orderreferenceno;
				$this->request->data['Order']['orderdate']=$transactiondate;
				//$this->request->data['Order']['ordercustomerphone']=$this->request->data["Order"]["ordercustomerphone"]?$this->request->data["Order"]["ordercustomerphone"]:0;
				$this->request->data['Order']['ordertotal']=$this->request->data["Order"]["ordertotal"];
				$this->request->data['Order']['orderdiscount']=$this->request->data['Order']['orderdiscount'];
				$this->request->data['Order']['transactionamount']=$grand_total;
				$this->request->data['Order']['orderpayment']=$paid_amount;
				$this->request->data['Order']['orderdue']=$this->request->data['Order']['orderdue'];
				$this->request->data['Order']['orderduepaymentdate']=$this->request->data['Order']['orderduepaymentdate']?$this->request->data['Order']['orderduepaymentdate']:"0000-00-00 00:00:00";
				$this->request->data['Order']['ordercomment']=$comment;
				$this->request->data['Order']['orderisactive'] = 1;
				$this->request->data['Order']['orderinsertid'] = $userID;
				$this->request->data['Order']['orderinsertdate']=date('Y-m-d H:i:s');
			}	
		}
		if( $this->Order->save($this->data)){
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
				$transaction_id = @$this->request->data['Purchases']['id'];
			} else {
				$transaction_id = $this->Order->getLastInsertId();
			}
			$Orderdetail_data = $this->Orderdetail->find(
				'all', array(
					'conditions' => array('Orderdetail.transaction_id' => $transaction_id),
				)
			);
			if(!empty($Orderdetail_data)){
				$this->Orderdetail->deleteAll(array('Orderdetail.transaction_id' => $transaction_id));
			}
			$count = $this->request->data["totlarow"];
			for($i = 1 ;$i<=$count; $i++){
				$this->request->data['Orderdetail']['orderdetailuuid']=String::uuid();	
				$this->request->data['Orderdetail']['transaction_id'] = $transaction_id;
				$this->request->data['Orderdetail']['company_id']=$companyID;
				$this->request->data['Orderdetail']['branch_id']=$this->request->data["Order"]["branch_id"];
				$this->request->data['Orderdetail']['transactiontype_id'] = $transactiontype_id;
				$this->request->data['Orderdetail']['product_id']=$this->request->data["Orderdetail"]["product_id{$i}"];
				$this->request->data['Orderdetail']['coa_id'] = $user_coa;
				$this->request->data['Orderdetail']['productcode'] = $this->request->data["Orderdetail"]["productcode{$i}"];
				$this->request->data['Orderdetail']['quantity'] = $this->request->data["Orderdetail"]["quantity{$i}"];
				$this->request->data['Orderdetail']['unit_id'] = $this->request->data["Orderdetail"]["unit_id{$i}"];
				$this->request->data['Orderdetail']['unitprice'] = $this->request->data["Orderdetail"]["unitprice{$i}"];
				$this->request->data['Orderdetail']['discount'] = $this->request->data["Orderdetail"]["discount{$i}"];
				$this->request->data['Orderdetail']['discountvalue'] = $this->request->data["Orderdetail"]["discountvalue{$i}"];
				$this->request->data['Orderdetail']['price'] = $this->request->data["Orderdetail"]["price{$i}"];
				$this->request->data['Orderdetail']['transactionamount'] = $this->request->data["Orderdetail"]["transactionamount{$i}"];
				$this->request->data['Orderdetail']['transactiondate'] = $transactiondate;
				$this->request->data['Orderdetail']['orderdetailisactive'] = 1;
				$this->request->data['Orderdetail']['orderdetailinsertid'] = $userID;
				$this->request->data['Orderdetail']['orderdetailinsertdate'] = date('Y-m-d H:i:s');
				$this->Orderdetail->create();
				$this->Orderdetail->save($this->data);
			}
			$this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $coa_id=28, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=7, $salesman_id=0, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, -$grand_total, $journaltransactionothernote=0, $isActive=1, $step=0);
			$this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $coa_id=19, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=7, $salesman_id=0, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, -$grand_total, $journaltransactionothernote=0, $isActive=1, $step=1);
			$this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $coa_id=28, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=7, $salesman_id=0, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, $paid_amount, $journaltransactionothernote=0, $isActive=1, $step=2);
			$this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $user_coa, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=7, $salesman_id=0, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, -$paid_amount, $journaltransactionothernote=0, $isActive=1, $step=3);
			$this->Transactions->gltransaction($transaction_id, $transactiontype_id, -$grand_total, $coa_id=28, $client_id, $group_id = 7, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=0);
			$this->Transactions->gltransaction($transaction_id, $transactiontype_id, -$grand_total, $coa_id=19, $client_id, $group_id = 7, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=1);
			$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $paid_amount, $coa_id=28, $client_id, $group_id = 7, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=2);
			$this->Transactions->gltransaction($transaction_id, $transactiontype_id, -$paid_amount, $user_coa, $client_id, $group_id = 7, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=3);
			$this->Session->setFlash("Save Successfully", "default", array("class" => "alert alert-success"));
		}
		$this->redirect("/purchases/purchasereturnmanage");	
	}
	//-----------------------------------Start Waste/Lost Product --------------------------------------//
	public function wastelostproductmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Waste/Lost Product Manage').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$condition = array();
		
		if($group_id==1):
		elseif($group_id==2):
			$condition[] = array('Order.company_id'=>$company_id);			
		else:
			$condition[] = array('Order.company_id'=>$company_id);
			$condition[] = array('Order.branch_id'=>$branch_id);			
		endif;
		$condition[] = array('Order.transactiontype_id'=>14);
		$this->paginate = array(
			'fields' => 'Order.*',
			'conditions'=> $condition ,
			'order'  =>'Order.id DESC',
			'limit' => 20
		);
		$order = $this->paginate('Order');
		$this->set('order',$order);
	}
	public function wastelostproductinsertupdate($id = null, $uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Waste/Lost Product').' | '.__(Configure::read('site_name')));
		//print_r($_SESSION); //exit();
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$bankaccount_id = array();
		$bankchequebook_id = array();
		$productlist_id = array();
		$customerlist_id = array();
		$supplierlist_id = array();
		$salescenter_id = array();
		$salesmanlist_id = array();
		if($group_id==1):
		elseif($group_id==2):
			$productlist_id[] = array('Product.company_id'=>$company_id);
			$customerlist_id[] = array('User.company_id'=> $company_id);
			$supplierlist_id[] = array('Usergroup.company_id'=> $company_id);
			$salescenter_id[] = array('Branch.company_id'=> $company_id);
			$salesmanlist_id[] = array('User.company_id'=> $company_id);
		else:
			$productlist_id[] = array('Product.company_id'=>$company_id);
			$productlist_id[] = array('Product.branch_id'=>$branch_id);
			$customerlist_id[] = array('User.company_id'=> $company_id);
			$customerlist_id[] = array('User.branch_id'=> $branch_id);
			$supplierlist_id[] = array('Usergroup.company_id'=> $company_id);
			$supplierlist_id[] = array('Usergroup.branch_id'=> $branch_id);
			$salescenter_id[] = array('Branch.company_id'=> $company_id);
			$salescenter_id[] = array('Branch.id'=> $branch_id);
			$salesmanlist_id[] = array('User.company_id'=> $company_id);
			$salesmanlist_id[] = array('User.branch_id'=> $branch_id);
		endif;
		$supplierlist_id[] = array('Usergroup.group_id'=> 7);
		$supplierlist_id[] = array('Usergroup.usergroupisactive'=> 1);
		$salesmanlist_id[] = array('User.group_id'=> 5);
		$supplierlist = $this->Usergroup->find(
	    		'list',array(
	    			"fields" => array("Usergroup.user_id", "Usergroup.user_fullname"),
	    			"conditions" =>$supplierlist_id,
	    			'recursive' => 1
	    		)
	    	);
		$this->set('supplierlist',$supplierlist);

		$supplierId = $this->Usergroup->find(
			'list',
			array(
				"fields" => array("Usergroup.user_id", "Usergroup.user_id"),
				"conditions" =>$supplierlist_id,
				"order" => "Usergroup.user_fullname ASC",
				'recursive' => 1
			)
		);
		$this->set('supplierId',$supplierId);
		$salescenter = $this->Branch->find(
			'list',
			array(
				"fields" => array("branch_name"),
				"conditions" =>$salescenter_id
			)
		);
		$this->set('salescenter',$salescenter);

		$salesmanlist = $this->User->find(
			'list',
			array(
				"fields" => array("User.id","user_fullname"),
				"conditions" =>$salesmanlist_id
			)
		);
		$this->set('salesmanlist',$salesmanlist);
		$coa_code=$this->Coa->find(
			'list',
			array(
				"fields" => array("id","coacode"),
				"conditions" => array('NOT'=>array('Coa.coaisactive'=>array(2)), 'Coa.coaclass_id' => 1)
			)
		);
		$this->set('coa_code',$coa_code);
		$coa=$this->Coa->find(
			'list',
			array(
				"fields" => array("id","coa_name"),
				"conditions" => array('NOT'=>array('Coa.coaisactive'=>array(2)), 'Coa.coaclass_id' => 1)
			)
		);
		$this->set('coa',$coa);

		//$productlist_id[] = array('Product.producttype_id'=>4);

		$productlist = $this->Product->find(
			'list', array(
				"fields" => array("Product.id", "Product.product_name"),
				"conditions" => $productlist_id
			)
		);
		$this->set('productlist',$productlist);

	    $productcodelist = $this->Product->find(
	    	'list',
	    	array(
	    		"fields" => array("Product.id","Product.productcode"),
				"conditions" => $productlist_id,
	    	)
	    );
	    $this->set('productcodelist',$productcodelist);

	    $unitlistInProductTable = $this->Product->find(
	    	'list',
	    	array(
	    		"fields" => array("Product.unit_id")
	    	)
	    );
	    $this->set('unitlistInProductTable',$unitlistInProductTable);
	    //print_r($unitlistInProductTable);
	    //var_dump($unitlistInProductTable);
	    //print_r(array_unique($unitlistInProductTable));

	    $condition_productunit = array();
		if($group_id==1):
			$condition_productunit[] = array('');
		elseif($group_id==2):
			$condition_productunit[] = array('Unit.company_id'=>$company_id);
		else:
			$condition_productunit[] = array('Unit.company_id'=>$company_id);
			//$condition_productunit[] = array('Product.branch_id'=>$branch_id);
		endif;

	    $unitlist = $this->Unit->find(
			'list',
			array(
				"fields" => array("Unit.id","Unit.unit_name"),
				"conditions" => $condition_productunit

				//"conditions" => array('Unit.id'=>array(1,2,3)),
			)
		);
		$this->set('unitlist',$unitlist);
	    
	    $locationlist = $this->Location->find(
	    	'list',
	    	array(
	    		"fields" => array("Location.id","Location.location_name"),
                "conditions" =>array('Location.locationstep'=>4)
            )
        );
        $this->set('locationlist',$locationlist);

        //get voucher no code start
		$current_month = date('Y-m');
		$voucher_no = $this->Order->find('first', array('fields' => array("max(orderreferenceno) as orderreferenceno"),'conditions' => array('Order.orderreferenceno LIKE' => 'WL-'.$current_month.'%'))); 
		$this->set('voucher_no',$voucher_no);
		//get voucher no code end


		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
	    	$purchase = $this->Order->find('all', array("fields" => "Order.*", "conditions" => array('Order.id' => $id, 'Order.orderuuid'=>$uuid)));
	    	$this->set('purchase', $purchase);

    		$orderdetails_data = $this->Orderdetail->find('all', array('fields' => 'Orderdetail.*', 'conditions'=>array('Orderdetail.transaction_id'=>$id)));
			$this->set('orderdetails_data', $orderdetails_data);

			$product_code_edit = $this->Productbranch->find(
				'list',
				array(
					"fields" => array("Productbranch.id","Productbranch.product_code"),
					'conditions'=>array('Productbranch.branch_id'=> $purchase[0]['Order']['branch_id']),
					'recursive' => 1
				)
			);
			$this->set('product_code_edit', $product_code_edit);

			$product_list_edit = $this->Productbranch->find(
				'list',
				array(
					"fields" => array("Productbranch.id","Productbranch.product_name"),
					'conditions'=>array('Productbranch.branch_id'=> $purchase[0]['Order']['branch_id']),
					'recursive' => 1
				)
			);
			$this->set('product_list_edit', $product_list_edit);	
	    }
	}
	public function wastelostproductinsertupdateaction($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		$companyID = $this->Session->read('User.company_id');
		$branch_id=$_SESSION["User"]["branch_id"];

		$id = @$this->request->data['Purchases']['id'];
		$uuid = @$this->request->data['Purchases']['orderuuid'];

		$transactiontype_id=$_SESSION["ST_WASTELOST"];
		$comment = $this->request->data['Order']['ordercomment'];
		$orderreferenceno = $this->request->data["Order"]["orderreferenceno"];
		$client_id = $this->request->data["Order"]["client_id"];
		$transactiondate = $this->request->data["Order"]["orderdate"];
		$grand_total = $this->request->data['Order']['ordergrandtotal'];
		$paid_amount = $this->request->data['Order']['orderpayment'];
		//$user_coa = $this->request->data['Order']['coa_id'];
		//print_r($_POST); exit();
		if($this->request->is('post')){
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
				$this->request->data['Order']['company_id'] = $companyID;
				$this->request->data['Order']['branch_id'] = $this->request->data["Order"]["branch_id"];
				$this->request->data['Order']['clientcode'] = $this->request->data["Order"]["clientcode"];
				$this->request->data['Order']['coa_id'] = 0;
				$this->request->data['Order']['coacode'] = 0;
				$this->request->data['Order']['transactiontype_id'] = $transactiontype_id;
				$this->request->data['Order']['client_id'] = $client_id;
				$this->request->data['Order']['user_id'] = $userID;
				$this->request->data['Order']['orderreferenceno']=$orderreferenceno;
				$this->request->data['Order']['orderdate']=$transactiondate;
				//$this->request->data['Order']['salesman_id']=$this->request->data["Order"]["salesman_id"];
				//$this->request->data['Order']['ordercustomerphone']=$this->request->data["Order"]["ordercustomerphone"]?$this->request->data["Order"]["ordercustomerphone"]:0;
				$this->request->data['Order']['ordertotal']=$this->request->data["Order"]["ordertotal"];
				$this->request->data['Order']['orderdiscount']=$this->request->data['Order']['orderdiscount'];
				$this->request->data['Order']['transactionamount']=$grand_total;
				$this->request->data['Order']['orderpayment']=$paid_amount;
				$this->request->data['Order']['orderdue']=$this->request->data['Order']['orderdue'];
				$this->request->data['Order']['orderduepaymentdate']=$this->request->data['Order']['orderduepaymentdate']?$this->request->data['Order']['orderduepaymentdate']:"0000-00-00 00:00:00";
				$this->request->data['Order']['ordercomment']=$comment;
				$this->request->data['Order']['orderisactive'] = 1;
				$this->request->data['Order']['orderupdateid'] = $userID;
				$this->request->data['Order']['orderupdatedate']=date('Y-m-d H:i:s');
				$this->Order->id = $id;
			}else{
				$this->request->data['Order']['orderuuid'] = String::uuid();
				$this->request->data['Order']['company_id'] = $companyID;
				$this->request->data['Order']['branch_id'] = $this->request->data["Order"]["branch_id"];
				$this->request->data['Order']['clientcode'] = $this->request->data["Order"]["clientcode"];
				$this->request->data['Order']['transactiontype_id'] = $transactiontype_id;
				$this->request->data['Order']['client_id'] = $client_id;
				$this->request->data['Order']['coa_id'] = 0;
				$this->request->data['Order']['coacode'] = 0;
				$this->request->data['Order']['user_id'] = $userID;
				$this->request->data['Order']['orderreferenceno']=$orderreferenceno;
				$this->request->data['Order']['orderdate']=$transactiondate;
				//$this->request->data['Order']['ordercustomerphone']=$this->request->data["Order"]["ordercustomerphone"]?$this->request->data["Order"]["ordercustomerphone"]:0;
				$this->request->data['Order']['ordertotal']=$this->request->data["Order"]["ordertotal"];
				$this->request->data['Order']['orderdiscount']=$this->request->data['Order']['orderdiscount'];
				$this->request->data['Order']['transactionamount']=$grand_total;
				$this->request->data['Order']['orderpayment']=$paid_amount;
				$this->request->data['Order']['orderdue']=$this->request->data['Order']['orderdue'];
				$this->request->data['Order']['orderduepaymentdate']=$this->request->data['Order']['orderduepaymentdate']?$this->request->data['Order']['orderduepaymentdate']:"0000-00-00 00:00:00";
				$this->request->data['Order']['ordercomment']=$comment;
				$this->request->data['Order']['orderisactive'] = 1;
				$this->request->data['Order']['orderinsertid'] = $userID;
				$this->request->data['Order']['orderinsertdate']=date('Y-m-d H:i:s');
			}	
		}
		if( $this->Order->save($this->data)){
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
				$transaction_id = @$this->request->data['Purchases']['id'];
			} else {
				$transaction_id = $this->Order->getLastInsertId();
			}
			$Orderdetail_data = $this->Orderdetail->find(
				'all', array(
					'conditions' => array('Orderdetail.transaction_id' => $transaction_id),
				)
			);
			if(!empty($Orderdetail_data)){
				$this->Orderdetail->deleteAll(array('Orderdetail.transaction_id' => $transaction_id));
			}
			$count = $this->request->data["totlarow"];
			for($i = 1 ;$i<=$count; $i++){
				$this->request->data['Orderdetail']['orderdetailuuid']=String::uuid();	
				$this->request->data['Orderdetail']['transaction_id'] = $transaction_id;
				$this->request->data['Orderdetail']['company_id']=$companyID;
				$this->request->data['Orderdetail']['branch_id']=$this->request->data["Order"]["branch_id"];
				$this->request->data['Orderdetail']['transactiontype_id'] = $transactiontype_id;
				$this->request->data['Orderdetail']['product_id']=$this->request->data["Orderdetail"]["product_id{$i}"];
				$this->request->data['Orderdetail']['coa_id'] = 0;
				$this->request->data['Orderdetail']['productcode'] = $this->request->data["Orderdetail"]["productcode{$i}"];
				$this->request->data['Orderdetail']['quantity'] = $this->request->data["Orderdetail"]["quantity{$i}"];
				$this->request->data['Orderdetail']['unit_id'] = $this->request->data["Orderdetail"]["unit_id{$i}"];
				$this->request->data['Orderdetail']['unitprice'] = $this->request->data["Orderdetail"]["unitprice{$i}"];
				$this->request->data['Orderdetail']['discount'] = $this->request->data["Orderdetail"]["discount{$i}"];
				$this->request->data['Orderdetail']['discountvalue'] = $this->request->data["Orderdetail"]["discountvalue{$i}"];
				$this->request->data['Orderdetail']['price'] = $this->request->data["Orderdetail"]["price{$i}"];
				$this->request->data['Orderdetail']['transactionamount'] = $this->request->data["Orderdetail"]["transactionamount{$i}"];
				$this->request->data['Orderdetail']['transactiondate'] = $transactiondate;
				$this->request->data['Orderdetail']['orderdetailisactive'] = 1;
				$this->request->data['Orderdetail']['orderdetailinsertid'] = $userID;
				$this->request->data['Orderdetail']['orderdetailinsertdate'] = date('Y-m-d H:i:s');
				$this->Orderdetail->create();
				$this->Orderdetail->save($this->data);
			}
			// $this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $coa_id=28, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=7, $salesman_id=0, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, $grand_total, $journaltransactionothernote=0, $isActive=1, $step=0);
			// $this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $coa_id=19, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=7, $salesman_id=0, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, $grand_total, $journaltransactionothernote=0, $isActive=1, $step=1);
			// $this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $coa_id=28, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=7, $salesman_id=0, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, -$paid_amount, $journaltransactionothernote=0, $isActive=1, $step=2);
			// $this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $user_coa, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=7, $salesman_id=0, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, $paid_amount, $journaltransactionothernote=0, $isActive=1, $step=3);
			// $this->Transactions->gltransaction($transaction_id, $transactiontype_id, $grand_total, $coa_id=28, $client_id, $group_id = 7, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=0);
			// $this->Transactions->gltransaction($transaction_id, $transactiontype_id, $grand_total, $coa_id=19, $client_id, $group_id = 7, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=1);
			// $this->Transactions->gltransaction($transaction_id, $transactiontype_id, -$paid_amount, $coa_id=28, $client_id, $group_id = 7, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=2);
			// $this->Transactions->gltransaction($transaction_id, $transactiontype_id, $paid_amount, $user_coa, $client_id, $group_id = 7, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=3);
			$this->Session->setFlash("Save Successfully", "default", array("class" => "alert alert-success"));
		}
		$this->redirect("/purchases/wastelostproductmanage");	
	}
	//------------------------------------End Waste/Lost Product ---------------------------------------//
	public function purchasereturn(){
		$this->layout='default';
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$invoice_no = $this->Order->find(
			'list',
			array(
				"fields" => array("id", "orderreferenceno")
			)
		);
		$this->set('invoice_no', $invoice_no);
	}
	public function purchasedDataByInvoiceNo(){
		$this->layout = 'ajax';
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];
		$supplierlist_id = array();

		if($group_id==1):
		elseif($group_id==2):
			$productlist_id[] = array('Product.company_id'=>$company_id);
			//$customerlist_id[] = array('User.company_id'=> $company_id);
			$supplierlist_id[] = array('Usergroup.company_id'=> $company_id);
			$salescenter_id[] = array('Branch.company_id'=> $company_id);
			$salesmanlist_id[] = array('User.company_id'=> $company_id);
		else:
			$productlist_id[] = array('Product.company_id'=>$company_id);
			$productlist_id[] = array('Product.branch_id'=>$branch_id);
			// $customerlist_id[] = array('User.company_id'=> $company_id);
			// $customerlist_id[] = array('User.branch_id'=> $branch_id);
			$supplierlist_id[] = array('Usergroup.company_id'=> $company_id);
			$supplierlist_id[] = array('Usergroup.branch_id'=> $branch_id);
			$salescenter_id[] = array('Branch.company_id'=> $company_id);
			$salescenter_id[] = array('Branch.id'=> $branch_id);
			// $salesmanlist_id[] = array('User.company_id'=> $company_id);
			// $salesmanlist_id[] = array('User.branch_id'=> $branch_id);
		endif;
		$supplierlist_id[] = array('Usergroup.group_id'=> 7);
		$supplierlist_id[] = array('Usergroup.usergroupisactive'=> 1);
		$salesmanlist_id[] = array('User.group_id'=> 5);

		$Searchtext = @$this->request->data['Searchtext'];
		$salescenter = $this->Branch->find(
			'list',
			array(
				"fields" => array("branch_name")
			)
		);
		$this->set('salescenter',$salescenter);
		$supplierId = $this->Usergroup->find(
			'list',
			array(
				"fields" => array("Usergroup.user_id", "Usergroup.user_id"),
				"conditions" =>$supplierlist_id,
				"order" => "Usergroup.user_fullname ASC",
				'recursive' => 1
			)
		);
		$this->set('supplierId',$supplierId);
		$supplierlist = $this->Usergroup->find(
    		'list',array(
    			"fields" => array("Usergroup.user_id", "Usergroup.user_fullname"),
    			"conditions" =>$supplierlist_id,
    			'recursive' => 1
    		)
    	);
		$this->set('supplierlist',$supplierlist);
		$purchasedData = $this->Order->find(
										'first',
										array(
											"fields" => "Order.*",
											"conditions" => array('Order.id'=>$Searchtext )
										)
							);
		$this->set('purchasedData', $purchasedData);
		$purchasedDetailsData = $this->Orderdetail->find(
										'all',
										array(
											"fields" => "Orderdetail.*",
											"conditions" => array('Orderdetail.transaction_id'=>$Searchtext )
										)
							);
		$this->set('purchasedDetailsData', $purchasedDetailsData);
		//print_r($purchasedData);
	}
	public function bankaccountlistbybankid(){
		$this->layout = 'ajax';
		//$this->beforeRender();
		//$this->autoRender = false;
		//$json_array = array();
		$bankId = @$this->request->data['bankId'];
		$this->set('bankId',$bankId);
		$bankaccount=$this->Bankaccount->find(
			'list',
			array(
				"fields" => array("id", "bankaccount_name"),
				"conditions"=>array('Bankaccount.bank_id'=>$bankId))
		);
		$this->set('bankaccount',$bankaccount);
		//var_dump($bankaccount);
	}
	

	
	public function purchasepricebyproductname($OrderdetailProductId=null){
		$this->layout = 'ajax';
		$ProductId = @$this->request->data['OrderdetailProductId'];
		$this->set('ProductId', $ProductId);
		//echo $ProductId;
		$productprice = $this->Product->find(
			'first',
			array(
				"fields" => array("id", "productprice"),
				"conditions" => array("Product.id" => $ProductId)
			)
		);
		$this->set('productprice', $productprice);
		//var_dump($productprice);
		//print_r($productprice);
	}
	public function purchasdetailsinsertupdate($id){
	    //print_r($this->data);exit();
	  $userID = $this->Session->read('User.id'); 
	  $companyID = $this->Session->read('User.company_id');
      $branch_id=$_SESSION["User"]["branch_id"]; 
	  $transactiontype_id=$_SESSION["ST_PURCHASEORDER"];  
	  $this->request->data['Orderdetail']['transaction_id']=$id;
	  $this->request->data['Orderdetail']['company_id']=$companyID;
	  $this->request->data['Orderdetail']['branch_id']=$this->data['OrderBranchId'];
	  $this->request->data['Orderdetail']['productcode']=$this->data['OrderdetailProductCode'];
	  $this->request->data['Orderdetail']['product_id']=$this->data['OrderdetailProductId'];
	  $this->request->data['Orderdetail']['transactiontype_id'] = $transactiontype_id;
	  $this->request->data['Orderdetail']['coa_id'] = 8;
	  $this->request->data['Orderdetail']['quantity']=$this->data['OrderdetailQuantity'];
	  $this->request->data['Orderdetail']['unit_id']=$this->data['OrderdetailUnitId'];
	  //$this->request->data['Orderdetail']['quantity']=$this->data['OrderUnit'];
	  $this->request->data['Orderdetail']['unitprice']=$this->data['OrderdetailPrice'];
	  $this->request->data['Orderdetail']['discount']=$this->data['OrderdetailDiscount']?$this->data['OrderdetailDiscount']:0;
	  $this->request->data['Orderdetail']['transactionamount']=$this->data['OrderdetailTransactionamount'];
	  $this->request->data['Orderdetail']['transactiondate']= date('Y-m-d H:i:s');
	  $this->request->data['Orderdetail']['orderdetailuuid']=String::uuid();
	  $this->request->data['Orderdetail']['orderdetailisactive']=1;
	  $this->request->data['Orderdetail']['orderdetailinsertid']= $userID;
	  $this->request->data['Orderdetail']['orderdetailinsertdate']= date('Y-m-d H:i:s');

	  $orderdetailmon = $this->data['orderdetailmon'];
	  $this->request->data['Orderdetail']['orderdetailmon']= $orderdetailmon;
	  $orderdetailkg = $this->data['orderdetailkg'];
	  $this->request->data['Orderdetail']['orderdetailkg']= $orderdetailkg;
	  $orderdetailgram = $this->data['orderdetailgram'];
	  $this->request->data['Orderdetail']['orderdetailgram']= $orderdetailgram;

	  $productdafultprice = $this->data['productdafultprice'];
	  $price = $this->data['OrderdetailPrice'];
	  $quantity = $this->data['OrderdetailQuantity'];
	  if($price == 0){
        		$this->request->data['Orderdetail']['orderdetailmonunitprice'] = $quantity *  $productdafultprice;
        	    $this->request->data['Orderdetail']['orderdetailkgunitprice'] = $quantity *  $productdafultprice;
        	    $this->request->data['Orderdetail']['orderdetailgramunitprice'] = $quantity *  $productdafultprice;
        }  else{
        		$this->request->data['Orderdetail']['orderdetailmonunitprice'] = $quantity *  $price;
        		$this->request->data['Orderdetail']['orderdetailkgunitprice'] = $quantity *  $price;
        		$this->request->data['Orderdetail']['orderdetailgramunitprice'] = $quantity *  $price;
        }

	  //$this->request->data['Orderdetail']['orderdetailmonunitprice'] = $this->request->data['Orderdetail']['quantity'] * $productdafultprice;
	  //$this->request->data['Orderdetail']['orderdetailkgunitprice'] = $this->request->data['Orderdetail']['quantity'] * $productdafultprice;
	  //$this->request->data['Orderdetail']['orderdetailgramunitprice'] = $this->request->data['Orderdetail']['quantity'] * $productdafultprice;

	  $transaction_id = $id;
	  $transactionamount = $this->data['OrderdetailTransactionamount']; 
	  $clientsuppler_id = $this->data['OrderClientId']; 
	  $product_id = $this->data['OrderdetailProductId'];
	  $transactiondate = date('Y-m-d H:i:s');
	  $paymentType = $this->data['paymenttermId']; 
	  $this->Orderdetail->save($this->data);
	  $this->Transactions->gltransaction($transaction_id, $transactiontype_id, $transactionamount, 8, $clientsuppler_id, $product_id, $transactiondate, $isActive=1);
	  /*if( $paymentType == 1){
	  	$bank_id = $this->data['bankId'];
	  	$bankaccount_id = $this->data['OrderbankAccountId'];
	  	$this->Transactions->banktransaction($bank_id, $bankbranch_id, $bankaccounttype_id, $bankaccount_id, $bankchequebook_id, $chequebookseries, $chequesequencynumber, $banktransactiondate, $banktransactionothernote, $transaction_id, $transactiontype_id, $transactionamount, $coa_id, $clientsuppler_id, $salesman_id, $product_id, $isActive);	
	  } else {

	  }*/
	}
	public function purchaseinsertupdateaction($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		$companyID = $this->Session->read('User.company_id');
		$branch_id=$_SESSION["User"]["branch_id"];

		$id = @$this->request->data['Purchases']['id'];
		$uuid = @$this->request->data['Purchases']['orderuuid'];

		$transactiontype_id=$_SESSION["ST_PURCHASEORDER"];
		$comment = $this->request->data['Order']['ordercomment'];
		$orderreferenceno = $this->request->data["Order"]["orderreferenceno"];
		$client_id = $this->request->data["Order"]["client_id"];
		$transactiondate = $this->request->data["Order"]["orderdate"];
		$grand_total = $this->request->data['Order']['ordergrandtotal'];
		$paid_amount = $this->request->data['Order']['orderpayment'];
		$user_coa = $this->request->data['Order']['coa_id'];
		//print_r($_POST); exit();
		if($this->request->is('post')){
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
				$this->request->data['Order']['company_id'] = $companyID;
				$this->request->data['Order']['branch_id'] = $this->request->data["Order"]["branch_id"];
				$this->request->data['Order']['clientcode'] = $this->request->data["Order"]["clientcode"];
				$this->request->data['Order']['transactiontype_id'] = $transactiontype_id;
				$this->request->data['Order']['client_id'] = $client_id;
				$this->request->data['Order']['user_id'] = $userID;
				$this->request->data['Order']['orderreferenceno']=$orderreferenceno;
				$this->request->data['Order']['orderdate']=$transactiondate;
				//$this->request->data['Order']['salesman_id']=$this->request->data["Order"]["salesman_id"];
				//$this->request->data['Order']['ordercustomerphone']=$this->request->data["Order"]["ordercustomerphone"]?$this->request->data["Order"]["ordercustomerphone"]:0;
				$this->request->data['Order']['ordertotal']=$this->request->data["Order"]["ordertotal"];
				$this->request->data['Order']['orderdiscount']=$this->request->data['Order']['orderdiscount'];
				$this->request->data['Order']['transactionamount']=$grand_total;
				$this->request->data['Order']['orderpayment']=$paid_amount;
				$this->request->data['Order']['orderdue']=$this->request->data['Order']['orderdue'];
				$this->request->data['Order']['orderduepaymentdate']=$this->request->data['Order']['orderduepaymentdate']?$this->request->data['Order']['orderduepaymentdate']:"0000-00-00 00:00:00";
				$this->request->data['Order']['ordercomment']=$comment;
				$this->request->data['Order']['orderisactive'] = 1;
				$this->request->data['Order']['orderupdateid'] = $userID;
				$this->request->data['Order']['orderupdatedate']=date('Y-m-d H:i:s');
				$this->Order->id = $id;
			}else{
				$this->request->data['Order']['orderuuid'] = String::uuid();
				$this->request->data['Order']['company_id'] = $companyID;
				$this->request->data['Order']['branch_id'] = $this->request->data["Order"]["branch_id"];
				$this->request->data['Order']['clientcode'] = $this->request->data["Order"]["clientcode"];
				$this->request->data['Order']['transactiontype_id'] = $transactiontype_id;
				$this->request->data['Order']['client_id'] = $client_id;
				$this->request->data['Order']['user_id'] = $userID;
				$this->request->data['Order']['orderreferenceno']=$orderreferenceno;
				$this->request->data['Order']['orderdate']=$transactiondate;
				//$this->request->data['Order']['ordercustomerphone']=$this->request->data["Order"]["ordercustomerphone"]?$this->request->data["Order"]["ordercustomerphone"]:0;
				$this->request->data['Order']['ordertotal']=$this->request->data["Order"]["ordertotal"];
				$this->request->data['Order']['orderdiscount']=$this->request->data['Order']['orderdiscount'];
				$this->request->data['Order']['transactionamount']=$grand_total;
				$this->request->data['Order']['orderpayment']=$paid_amount;
				$this->request->data['Order']['orderdue']=$this->request->data['Order']['orderdue'];
				$this->request->data['Order']['orderduepaymentdate']=$this->request->data['Order']['orderduepaymentdate']?$this->request->data['Order']['orderduepaymentdate']:"0000-00-00 00:00:00";
				$this->request->data['Order']['ordercomment']=$comment;
				$this->request->data['Order']['orderisactive'] = 1;
				$this->request->data['Order']['orderinsertid'] = $userID;
				$this->request->data['Order']['orderinsertdate']=date('Y-m-d H:i:s');
			}	
		}
		if( $this->Order->save($this->data)){
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
				$transaction_id = @$this->request->data['Purchases']['id'];
			} else {
				$transaction_id = $this->Order->getLastInsertId();
			}
			$Orderdetail_data = $this->Orderdetail->find(
				'all', array(
					'conditions' => array('Orderdetail.transaction_id' => $transaction_id),
				)
			);
			if(!empty($Orderdetail_data)){
				$this->Orderdetail->deleteAll(array('Orderdetail.transaction_id' => $transaction_id));
			}
			$count = $this->request->data["totlarow"];
			for($i = 1 ;$i<=$count; $i++){
				$this->request->data['Orderdetail']['orderdetailuuid']=String::uuid();	
				$this->request->data['Orderdetail']['transaction_id'] = $transaction_id;
				$this->request->data['Orderdetail']['company_id']=$companyID;
				$this->request->data['Orderdetail']['branch_id']=$this->request->data["Order"]["branch_id"];
				$this->request->data['Orderdetail']['transactiontype_id'] = $transactiontype_id;
				$this->request->data['Orderdetail']['product_id']=$this->request->data["Orderdetail"]["product_id{$i}"];
				$this->request->data['Orderdetail']['coa_id'] = $user_coa;
				$this->request->data['Orderdetail']['productcode'] = $this->request->data["Orderdetail"]["productcode{$i}"];
				$this->request->data['Orderdetail']['quantity'] = $this->request->data["Orderdetail"]["quantity{$i}"];
				$this->request->data['Orderdetail']['unit_id'] = $this->request->data["Orderdetail"]["unit_id{$i}"];
				$this->request->data['Orderdetail']['unitprice'] = $this->request->data["Orderdetail"]["unitprice{$i}"];
				$this->request->data['Orderdetail']['discount'] = $this->request->data["Orderdetail"]["discount{$i}"];
				$this->request->data['Orderdetail']['discountvalue'] = $this->request->data["Orderdetail"]["discountvalue{$i}"];
				$this->request->data['Orderdetail']['price'] = $this->request->data["Orderdetail"]["price{$i}"];
				$this->request->data['Orderdetail']['transactionamount'] = $this->request->data["Orderdetail"]["transactionamount{$i}"];
				$this->request->data['Orderdetail']['transactiondate'] = $transactiondate;
				$this->request->data['Orderdetail']['orderdetailisactive'] = 1;
				$this->request->data['Orderdetail']['orderdetailinsertid'] = $userID;
				$this->request->data['Orderdetail']['orderdetailinsertdate'] = date('Y-m-d H:i:s');
				$this->Orderdetail->create();
				$this->Orderdetail->save($this->data);
			}
			$this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $coa_id=28, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=7, $salesman_id=0, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, $grand_total, $journaltransactionothernote=0, $isActive=1, $step=0);
			$this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $coa_id=19, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=7, $salesman_id=0, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, $grand_total, $journaltransactionothernote=0, $isActive=1, $step=1);
			$this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $coa_id=28, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=7, $salesman_id=0, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, -$paid_amount, $journaltransactionothernote=0, $isActive=1, $step=2);
			$this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $user_coa, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=7, $salesman_id=0, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, $paid_amount, $journaltransactionothernote=0, $isActive=1, $step=3);
			$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $grand_total, $coa_id=28, $client_id, $group_id = 7, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=0);
			$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $grand_total, $coa_id=19, $client_id, $group_id = 7, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=1);
			$this->Transactions->gltransaction($transaction_id, $transactiontype_id, -$paid_amount, $coa_id=28, $client_id, $group_id = 7, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=2);
			$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $paid_amount, $user_coa, $client_id, $group_id = 7, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=3);
			$this->Session->setFlash("Save Successfully", "default", array("class" => "alert alert-success"));
		}
		$this->redirect("/purchases/purchasemanage");	
	}
	public function purchaseinsertupdateaction_old(){
		$userID = $this->Session->read('User.id');
		$companyID = $this->Session->read('User.company_id');
		$branch_id=$_SESSION["User"]["branch_id"];
		$transactiontype_id=$_SESSION["ST_PURCHASEORDER"];
		if($this->request->is('post')){
			$this->request->data['Order']['company_id']=$companyID;
			//$this->request->data['Order']['branch_id']=$branch_id;
			$this->request->data['Order']['branch_id']=$this->data['OrderBranchId'];
			$this->request->data['Order']['user_id']=$userID;
			$this->request->data['Order']['client_id']=$this->data['OrderClientId'];
			$this->request->data['Order']['orderreferenceno']=$this->data['OrderOrderreferenceno'];
			$this->request->data['Order']['orderdate']=$this->data['OrderOrderdate'];
			//$this->request->data['Order']['ordercheckdate']=$this->data['checkdate']?$this->data['checkdate']:"0000-00-00 00:00:00";
			//$this->request->data['Order']['bank_id']=$this->data['bankId'];
			//$this->request->data['Order']['bankaccount_id']=$this->data['OrderbankAccountId'];
			//$this->request->data['Order']['bankbranch_id']=$this->data['BanktransactionBankbranchId']?$this->data['BanktransactionBankbranchId']:0;
			//$this->request->data['Order']['bankaccounttype_id']=$this->data['BanktransactionBankaccounttypeId']?$this->data['BanktransactionBankaccounttypeId']:0;
			//$this->request->data['Order']['bankaccount_id']=$this->data['OrderbankAccountId'];
			//$this->request->data['Order']['bankchequekbook_id']=$this->data['OrderbankAccountId'];
			$this->request->data['Order']['salesman_id']=$this->data['OrderSalesmanId'];
			$this->request->data['Order']['ordercustomerphone']=$this->data['OrderCustomerPhone']?$this->data['OrderCustomerPhone']:0;
			//$this->request->data['Order']['shipping_id']=$this->data['OrderShippingId']?$this->data['OrderShippingId']:0;
			//$this->request->data['Order']['location_id']=$this->data['OrderLocationId']?$this->data['OrderLocationId']:0;
			$this->request->data['Order']['coa_id'] = 9;
			//$this->request->data['Order']['paymentterm_id']=$this->data['paymenttermId'];
			$this->request->data['Order']['ordertotal']=$this->data['OrderOrdertotal'];
			$this->request->data['Order']['orderdiscount']=$this->data['OrderOrderdiscount']?$this->data['OrderOrderdiscount']:0;
			$this->request->data['Order']['ordergrandtotal']=$this->data['OrderOrdergrandtotal'];
			$this->request->data['Order']['orderpayment']=$this->data['OrderOrderpayment'];
			$this->request->data['Order']['orderdue']=$this->data['OrderOrderdue'];
			$this->request->data['Order']['orderduepaymentdate']=$this->data['OrderOrderduepaymentdate']?$this->data['OrderOrderduepaymentdate']:"0000-00-00 00:00:00";
			$this->request->data['Order']['orderuuid']=String::uuid();
			$this->request->data['Order']['transactiontype_id']=$transactiontype_id;
			$this->request->data['Order']['ordercomment']=$this->data['OrderOrdercomment'];
			$transactiondate = date('Y-m-d H:i:s');
			$orderdetailmon = $this->data['orderdetailmon'];
			$orderdetailkg = $this->data['orderdetailkg'];
			$orderdetailgram = $this->data['orderdetailgram'];
			$productdafultprice = $this->data['productdafultprice'];
			$quantity = $this->data['OrderdetailQuantity'];
			$price = $this->data['OrderdetailPrice'];
			if($price == 0){
				$orderdetailmonunitprice = $quantity *  $productdafultprice;
				$orderdetailkgunitprice = $quantity *  $productdafultprice;
				$orderdetailgramunitprice = $quantity *  $productdafultprice;
			}  else{
				$orderdetailmonunitprice = $quantity *  $price;
				$orderdetailkgunitprice = $quantity *  $price;
				$orderdetailgramunitprice = $quantity *  $price;
			}

			if( $this->Order->save($this->data)){
				$id = $this->Order->getLastInsertId();
				$OrderGrandTotal = $this->data['OrderGrandTotal'];
				$this->Transactions->gltransaction($id, $transactiontype_id, $OrderGrandTotal, 9, $this->data['OrderClientId'], $group_id = 7, $product_id=0, $transactiondate, $isActive=1, 0);
				if($this->request->data['Order']['paymentterm_id'] == 1){
					$banktransactionnn_id = $this->request->data['Bank']['banktransactionId'];
					$bank_id =$this->request->data['Order']['bank_id'];
					$bankbranch_id  = $this->request->data['Order']['bankbranch_id'];
					$bankaccounttype_id = $this->request->data['Order']['bankaccounttype_id'];
					$bankaccount_id = $this->request->data['Order']['bankaccount_id'];
					$bankchequebook_id = $this->data['OrderBanktransactionchequebookseries'];
					$banktransactionchequesequencynumber = explode("-", $this->data['OrderCheckNo']);
					$chequebookseries = $banktransactionchequesequencynumber[0];
					$chequesequencynumber =$banktransactionchequesequencynumber[1];
					$banktransactiondate = $this->data['checkdate'];
					$transaction_id = $id;
					$transactiontype_id = $transactiontype_id;
					$transactionamount = $this->data['OrderPayment'];
					$clientsuppler_id = $this->data['OrderClientId'];
					$salesman_id = $this->data['OrderSalesmanId'];
					$this->Transactions->banktransaction($bank_id, $bankbranch_id, $bankaccounttype_id, $bankaccount_id, $bankchequebook_id, $chequebookseries, $chequesequencynumber, $banktransactiondate, $banktransactionothernote=0, $transaction_id, $transactiontype_id, -$transactionamount, 1, $clientsuppler_id, $group_id = 7, $salesman_id, $product_id=0, $banktransactionnn_id, $isActive=1);
					$this->Transactions->gltransaction($transaction_id, $transactiontype_id, -$transactionamount, 1, $clientsuppler_id, $group_id = 7, $product_id=0, $transactiondate, $isActive=1, 1);
				}
				if($this->request->data['Order']['paymentterm_id'] == 2){
					$transaction_id = $id;
					$transactionamount = $this->data['OrderPayment'];
					$clientsuppler_id = $this->data['OrderClientId'];
					$this->Transactions->cashtransaction($transaction_id, $transactiontype_id, -$transactionamount, 2, $clientsuppler_id, $group_id = 7, $product_id=0, $transactiondate, $banktansaction_id=0, $isActive=1);	
					$this->Transactions->gltransaction($transaction_id, $transactiontype_id, -$transactionamount, 2, $clientsuppler_id, $group_id = 7, $product_id=0, $transactiondate, $isActive=1, 2);
				}

				$transaction_id = $id;
				$transactiontype_id = $transactiontype_id;
				$grand_total = @$this->data['OrderGrandTotal']?$this->data['OrderGrandTotal']:0;
				$transactionamount = $this->data['OrderPayment'];
				$clientsuppler_id = $this->data['OrderClientId'];
				$clientsupplergroup_id = $this->Session->read('User.group_id');
				$transactiondate = date('Y-m-d H:i:s');
				$payment = @$this->data['OrderPayment']?$this->data['OrderPayment']:0;
				$this->Transactions->usertransaction($transaction_id, $transactiontype_id, $grand_total, 5, $clientsuppler_id, $clientsupplergroup_id, $transactiondate,  $isActive=1, 1);
				$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $grand_total, 5, $clientsuppler_id, $group_id = 7, $product_id=0, $transactiondate, $isActive=1, 3);
//exit();
				$this->Transactions->usertransaction($transaction_id, $transactiontype_id, -$payment, 5, $clientsuppler_id, $clientsupplergroup_id, $transactiondate,  $isActive=1, 2);
				$this->Transactions->gltransaction($transaction_id, $transactiontype_id, -$payment, 5, $clientsuppler_id, $group_id = 7, $product_id=0, $transactiondate, $isActive=1, 4);

				$message = 'Save successfully';
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				$this->purchasdetailsinsertupdate($id);
			}
			//$this->redirect("/purchases/purchasemanage");
			echo"<script>window.location = '".$this->webroot."purchases/purchasemanage';</script>";
		}else{
			//$this->redirect("/purchases/purchaseinsertupdate");
			echo"<script>window.location = '".$this->webroot."purchases/purchaseinsertupdate';</script>";

		}	
	}
	public function purchasemanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Purchase Manage').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$condition = array();
		
		if($group_id==1):
		elseif($group_id==2):
			$condition[] = array('Order.company_id'=>$company_id);			
		else:
			$condition[] = array('Order.company_id'=>$company_id);
			$condition[] = array('Order.branch_id'=>$branch_id);			
		endif;
		$condition[] = array('Order.transactiontype_id'=>$_SESSION["ST_PURCHASEORDER"]);
		$this->paginate = array(
			'fields' => 'Order.*',
			'conditions'=> $condition ,
			'order'  =>'Order.id DESC',
			'limit' => 20
		);
		$order = $this->paginate('Order');
		$this->set('order',$order);
	}
	/*public function productlistbybranchid($OrderBranchId=null){
		$this->layout = 'ajax';
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];

		$company_condition = array();
		if($group_id==1):
			$company_condition[] = array('');
			$branch_condition[] = array('');
		elseif($group_id==2):
			$company_condition[] = array('Productbranch.company_id' => $company_id);
		else:
			$company_condition[] = array('Productbranch.company_id' => $company_id);
		endif;

		$OrderBranchId = @$this->request->data['OrderBranchId'];
		$this->set('OrderBranchId',$OrderBranchId);
		$productlist=$this->Productbranch->find(
			'list',
			array(
				"fields" => array("product_id","product_name"),
				"conditions"=>array('Productbranch.branch_id'=>$OrderBranchId, 'Productbranch.productbranchisactive'=>1, $company_condition))
		);
		$this->set('productlist',$productlist);
	}*/
	public function productlistbybranchid(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$json_array = array();
		$OrderBranchId = @$this->request->data['OrderBranchId'];
		$product=$this->Productbranch->find(
			'all',
			array(
				"fields" => array('Productbranch.*'),
				"conditions" => array('Productbranch.branch_id'=>$OrderBranchId)
			)
		);
		$product_options="";
		$product_code_options="";
		$product_options.="<option value=\"\">Select Product</option>";
		$product_code_options.="<option value=\"\">Select Product Code</option>";
		foreach ($product as $Thisproduct) {
			# code...
			$product_options.="<option value=\"{$Thisproduct["Productbranch"]["product_id"]}\">{$Thisproduct["Productbranch"]["product_name"]}</option>";
			$product_code_options.="<option value=\"{$Thisproduct["Productbranch"]["product_id"]}\">{$Thisproduct["Productbranch"]["product_code"]}</option>";
		}
		$product_options.="";
		$product_code_options.="";
		$json_array["product_options"] = "{$product_options}";
		$json_array["product_code_options"] = "{$product_code_options}";
		return json_encode($json_array);
	}
	public function manufactureproductlistbybranchid(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$json_array = array();
		$OrderdetailBranchId = @$this->request->data['OrderdetailBranchId'];
		$product_list=$this->Productbranch->find(
			'all',
			array(
				"fields" => array('Productbranch.*'),
				"conditions" => array('Productbranch.branch_id'=>$OrderdetailBranchId)
			)
		);
		$purchased_product_options="";
		$manufactured_product_options="";
		$purchased_product_options.="<option value=\"\">Select Purchased Product</option>";
		$manufactured_product_options.="<option value=\"\">Select Manufactured Product</option>";
		foreach ($product_list as $Thisproduct_list) {
			# code...
			if($Thisproduct_list["Productbranch"]["product_type"] == 4){
				$purchased_product_options.="<option value=\"{$Thisproduct_list["Productbranch"]["product_id"]}\">{$Thisproduct_list["Productbranch"]["product_name"]}</option>";	
			} elseif($Thisproduct_list["Productbranch"]["product_type"] == 1){
				$manufactured_product_options.="<option value=\"{$Thisproduct_list["Productbranch"]["product_id"]}\">{$Thisproduct_list["Productbranch"]["product_name"]}</option>";	
			}
		}
		$purchased_product_options.="";
		$manufactured_product_options.="";
		$json_array["purchased_product_options"] = "{$purchased_product_options}";
		$json_array["manufactured_product_options"] = "{$manufactured_product_options}";
		return json_encode($json_array);
	}
	public function salesmanbysupplierid(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$json_array = array();
		$OrderClientId = @$this->request->data['OrderClientId'];
		$user=$this->User->find(
			'first',
			array(
				"fields" => array('User.*'),
				"conditions" => array('User.id'=>$OrderClientId)
			)
		);
		$salespersonid = @$user["User"]["salespersonid"];
		$salesman = $this->User->find(
			'first',
			array(
				"fields" => array('User.*'),
				"conditions" => array('User.id'=>$salespersonid)
			)
		);
		$salesman_options="";
		$salesman_options.="<option value=\"\">Select Salesman</option>";
		$salesman_options.="<option value=\"{$salesman["User"]["id"]}\">{$salesman["User"]["user_fullname"]}</option>";
		$salesman_options.="";
		$json_array["salesman_options"] = "{$salesman_options}";
		return json_encode($json_array);

	}

	function getProduct ($branchId=null)
	{
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;

		if(!is_null($branchId)){
			$productNameList=$this->Productbranch->find(
				'all',
				array(
					"conditions" => ['Productbranch.branch_id'=>$branchId]
				)
			);
		}
		return json_encode($productNameList);
	}
	public function getStockForPurchaseReturn($branchId,$productId)
	{
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;

		$puchasedItemQuantity = $this->Orderdetail->find(
			'all',
			array(
				"fields" => array("SUM(Orderdetail.quantity) AS totalproductquantity"),
				"conditions" => array(
					'Orderdetail.product_id'=>$productId,
					'Orderdetail.branch_id'=>$branchId,
					'Orderdetail.transactiontype_id'=>8
				)
			)
		);

		$stock = @$puchasedItemQuantity[0][0]["totalproductquantity"];
		//$json_array["totalproductquantity"] = $stock;
		return json_encode($stock);
	}
}