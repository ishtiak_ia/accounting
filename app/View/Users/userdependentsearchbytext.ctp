<?php 
echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Employee Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Dependent Name')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Dependent Name BN')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Relationship')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Other')  => array('class' => 'highlight sortable')
					),
					array(
						__('Date of Birth')  => array('class' => 'highlight sortable')
					),
					__('Active'), 
					__('Action')
				)
			);
			$userdependentrows = array();
			$countUserdependentrows = 0-1+$this->Paginator->counter('%start%');
			foreach($userdependent as $Thisuserdependent):
				$countUserdependentrows++;
				$userdependentrows[]= array($countUserdependentrows,$Thisuserdependent["Userdependent"]["user_fullname"],$Thisuserdependent["Userdependent"]["userdependentname"],$Thisuserdependent["Userdependent"]["userdependentnamebn"],$Thisuserdependent["Userdependent"]["userrelationship_name"],$Thisuserdependent["Userdependent"]["userdependentrelationship"],$Thisuserdependent["Userdependent"]["userdependentdob"],$Utilitys->statusURL($this->request["controller"], 'userdependent', $Thisuserdependent["Userdependent"]["id"], $Thisuserdependent["Userdependent"]["userdependentuuid"], $Thisuserdependent["Userdependent"]["userdependentisactive"]),$Utilitys->cusurl($this->request["controller"], 'userdependent', $Thisuserdependent["Userdependent"]["id"], $Thisuserdependent["Userdependent"]["userdependentuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$userdependentrows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#UserdependentSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#userdependentsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#userdependentsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>