<?php
echo $this->Form->create('Shifts', array('action' => 'shiftinsertupdateaction', 'type' => 'file'));
echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$shift[0]['Shift']['id']));

echo $this->Form->input('shiftuuid', array('type'=>'hidden', 'value'=>@$shift[0]['Shift']['shiftuuid']));
$options = array(1 => __("Yes"), 0 =>__("No"));
        $attributes = array(
            'legend' => false, 
            'label' => true, 
            'value' => (@$shift[0]['Shift']['shiftisactive'] ? $shift[0]['Shift']['shiftisactive'] : 0),
            'class' => 'form-inline'
        );
        $attributes_permanent = array(
                'legend' => false, 
                'label' => true, 
                'value' => (@$shift[0]['Shift']['shiftispermanent'] ? $shift[0]['Shift']['shiftispermanent'] : 0),
                'class' => 'form-inline'
            );
echo"<h3>".__("Add Shift information")."</h3>";  
echo"<div class=\"row\">";
    echo $this->Form->input(
            "Shift.shiftname",
            array(
                'type' => 'text',
                'div' => array('class'=> 'form-group'),
                'label' => __('Shift'),
                'tabindex'=>2,
                'placeholder' => __("Shift"),
                'class'=> 'form-control',
                'style' => '',
                'data-validation-engine'=>'validate[required]',
                'value' => @$shift[0]['Shift']['shiftname']
            )
        );
    echo"<div class=\"form-group\">";
            echo"<label>isParmanent?</label><br>";
            echo $this->Form->radio(
                'Shift.shiftispermanent', 
                $options,
                $attributes_permanent
            );
    echo"</div>";
    echo"<div class=\"form-group\">";
            echo"<label>isActive?</label><br>";
            echo $this->Form->radio(
                'Shift.shiftisactive', 
                $options,
                $attributes
            );
    echo"</div>";
    echo $Utilitys->allformbutton('',$this->request["controller"],$page='shift');
echo"</div>";
echo $this->Form->end();      
 ?>

