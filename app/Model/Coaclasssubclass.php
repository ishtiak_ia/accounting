<?php
App::uses('AppModel', 'Model');
class Coaclasssubclass extends AppModel {
	public $name = 'Coaclasssubclass';
	public $usetables = 'coaclasssubclasses';
	var $belongsTo = array(
		'Coatype' => array(
			'fields' =>array('coatypename','coatypenamebn'),
			'className'    => 'Coatype',
			'foreignKey'    => 'coatype_id'
		),
		'Coagroup' => array(
			'fields' =>array('coagroupname','coagroupnamebn'),
			'className'    => 'Coagroup',
			'foreignKey'    => 'coagroup_id'
		),
		'Coaclass' => array(
			'fields' =>array('coaclassname','coaclassnamebn'),
			'className'    => 'Coaclass',
			'foreignKey'    => 'coaclass_id'
		),
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'coaclasssubclassinsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'coaclasssubclassupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'coaclasssubclassdeleteid'
		)
	);
	var $virtualFields = array(
		'coaclasssubclass_name' => 'CONCAT(Coaclasssubclass.coaclasssubclassname, " / ", Coaclasssubclass.coaclasssubclassnamebn)',
		'coaclass_name' => 'CONCAT(Coaclass.coaclassname, " / ", Coaclass.coaclassnamebn)',
		'coagroup_name' => 'CONCAT(Coagroup.coagroupname, " / ", Coagroup.coagroupnamebn)',
		'coatype_name' => 'CONCAT(Coatype.coatypename, " / ", Coatype.coatypenamebn)',
		'isActive' => 'IF(Coaclasssubclass.coaclasssubclassisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Coaclasssubclass.coaclasssubclassisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
	public $validate = array(
		'coatype_id' => array(
			'coatype_id_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Type field is required',
				'last' => true
			)
		),
		'coagroup_id' => array(
			'coagroup_id_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Group field is required',
				'last' => true
			)
		),
		'coaclass_id' => array(
			'coaclass_id_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Class field is required',
				'last' => true
			)
		),
		'coaclasssubclassname' => array(
			'coaclasssubclassname_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Subclass Name field is required',
				'last' => true
			)
		),
		'coaclasssubclassnamebn' => array(
			'coaclasssubclassnamebn_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Subclass Name BN field is required',
				'last' => true
			)
		),
		'coaclasssubclasscode' => array(
			'	coaclasssubclasscode_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Subclass Code field is required',
				'last' => true
			)
		)
	);
}

?>