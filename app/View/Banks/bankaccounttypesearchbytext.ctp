<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart($class=null, $id=null);
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('Account Type')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Account Type BN')  => array('class' => 'highlight sortable')
				), 
				__('Active'), 
				__('Action')
			)
		);
		$bankaccounttyperows = array();
		$countBankaccounttype = 0-1+$this->Paginator->counter('%start%');
		foreach($bankaccounttype as $Thisbankaccounttype):
			$countBankaccounttype++;
			$bankaccounttyperows[]= array($countBankaccounttype,$Thisbankaccounttype["Bankaccounttype"]["bankaccounttypename"],$Thisbankaccounttype["Bankaccounttype"]["bankaccounttypenamebn"],$Utilitys->statusURL($this->request["controller"], 'bankaccounttype', $Thisbankaccounttype["Bankaccounttype"]["id"], $Thisbankaccounttype["Bankaccounttype"]["bankaccounttypeuuid"], $Thisbankaccounttype["Bankaccounttype"]["bankaccounttypeisactive"]),$Utilitys->cusurl($this->request["controller"], 'bankaccounttype', $Thisbankaccounttype["Bankaccounttype"]["id"], $Thisbankaccounttype["Bankaccounttype"]["bankaccounttypeuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$bankaccounttyperows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#BankaccounttypeSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#bankaccounttypesarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#bankaccounttypesarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>