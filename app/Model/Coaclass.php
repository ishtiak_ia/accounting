<?php
App::uses('AppModel', 'Model');
class Coaclass extends AppModel {
	public $name = 'Coaclass';
	public $usetables = 'coaclasses';
	var $belongsTo = array(
		'Coatype' => array(
			'fields' =>array('coatypename','coatypenamebn'),
			'className'    => 'Coatype',
			'foreignKey'    => 'coatype_id'
		),
		'Coagroup' => array(
			'fields' =>array('coagroupname','coagroupnamebn'),
			'className'    => 'Coagroup',
			'foreignKey'    => 'coagroup_id'
		),
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'coaclassinsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'coaclassupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'coaclassdeleteid'
		)
	);
	var $virtualFields = array(
		'coaclass_name' => 'CONCAT(Coaclass.coaclassname, " / ", Coaclass.coaclassnamebn)',
		'coagroup_name' => 'CONCAT(Coagroup.coagroupname, " / ", Coagroup.coagroupnamebn)',
		'coatype_name' => 'CONCAT(Coatype.coatypename, " / ", Coatype.coatypenamebn)',
		'isActive' => 'IF(Coaclass.coaclassisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Coaclass.coaclassisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
	public $validate = array(
		'coatype_id' => array(
			'coatype_id_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Type field is required',
				'last' => true
			)
		),
		'coagroup_id' => array(
			'coagroup_id_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Group field is required',
				'last' => true
			)
		),
		'coaclassname' => array(
			'coaclassname_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Class Name field is required',
				'last' => true
			)
		),
		'coaclassnamebn' => array(
			'coaclassnamebn_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Name Name BN field is required',
				'last' => true
			)
		)
	);
}

?>