<?php
App::uses('AppController', 'Controller');
class LoginsController extends AppController {
	public $name = 'Logins';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'Form' => array('className' => 'BootstrapForm'));
	public $components = array('RequestHandler','Session');
	public $uses = array('User', 'Usernarration', 'Group', 'Groupnarration', 'Menu', 'Menunarration', 'Menuprivilege', 'Company', 'Branch', 'Transactiontype');

	public function beforeFilter() {
		$this->__validateLoginStatus();
	}
	public  function beforeRender(){
		//$this->redirect("logout");
	}

	public function index() {
		$this->redirect("login");
	}

	public function login(){
		$this->layout='default';
		$this->set('title_for_layout', __('Login').' | '.__(Configure::read('site_name')));
		if(isset($_POST['loginsubmit'])){
			if (empty($this->data['User']['username'])) {
				$this->User->validationErrors['username'] = "Please enter a username";
			}
			if (empty($this->data['User']['password'])) {
				$this->User->validationErrors['password'] = "Please enter a password";
			}
			if (!empty($this->data['User']['username']) && !empty($this->data['User']['password'])) {
				$user = $this->User->find('first', array('conditions' => array('User.username' => $this->data['User']['username'],'User.userisactive'=>1)));
				if (!empty($user))  {
					if($user['User']['username'] == $this->data['User']['username'] && $user['User']['userpassword'] == md5($this->data['User']['password'])){
						$this->Session->write('User', $user['User']);
						$this->Session->write('Screenwidth', $this->data['Logins']['screenwidth']);
						$this->Session->write('Screenheight', $this->data['Logins']['screenheight']);
						$userGroup = $this->Session->read('User.group_id');
						$menu_p = $this->Menuprivilege->find('all', array("fields"=>"Menu.menuurl",'conditions' => array('Menuprivilege.group_id' => $userGroup)));
						foreach ($menu_p as $Thismenu_p) {
							if($Thismenu_p["Menu"]["menuurl"]=='#'){

							}else{
								$Menuexplode=explode("/", $Thismenu_p["Menu"]["menuurl"]);
								if(isset($Menuexplode[1])){
									$this->Session->write($Menuexplode[1],$Thismenu_p["Menu"]["menuurl"]);
								}
								
							}
						}
						/////////
						$transactiontype = $this->Transactiontype->find('all', array("fields"=>"Transactiontype.id, Transactiontype.transactiontypename",'conditions' => array('Transactiontype.transactiontypeisactive' => 1)));
						foreach ($transactiontype as $Thistransactiontype) {
							$this->Session->write($Thistransactiontype["Transactiontype"]["transactiontypename"],$Thistransactiontype["Transactiontype"]["id"]);
								
						}
						$this->redirect("/dashboards/index");
					} else{
						$this->Session->setFlash('Sorry, the information you\'ve entered is incorrect.', 'default', array('class' => 'alert alert-danger'));		
					}

					
				} else {
					$this->Session->setFlash('Sorry, the information you\'ve entered is incorrect.', 'default', array('class' => 'alert alert-warning'));
				}
			}
		}else{
			if(isset($_SESSION["User"]["id"])){
				//pr($_SESSION);
				$this->redirect("/dashboards/index");
			}
		}
	}

	public function logout(){
		$this->Session->delete('User');
		$this->Session->destroy();
		$this->redirect("login");
	}

	public function logoutm(){
		$this->layout='default';
	}

	public function __validateLoginStatus() {
		if ($this->action != 'login' || $this->action != 'logout') {
			if ($this->Session->check('User') == false) {
				$this->Session->setFlash('The URL you\'ve followed requires you login.', 'default', array('class' => 'alert alert-warning','role'=>'alert'));
				//$this->redirect("login");
			}
		}
	}
}
