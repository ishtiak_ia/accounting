<?php
	echo "<h2>".__('Menu Manage'). $Utilitys->addurl($title=__("Add New Menu"), $this->request["controller"], $page="menu")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
		"Menu.parentmenu",
		array(
			'type'=>'select',
			"options"=>array($parentmenu),
			'empty'=>__('Select Parent Menu'),
			'div'=>false,
			'tabindex'=>1,
			'label'=>false,
			'class'=> 'form-control',
			'style'=>''
			)
		)
		.$this->Form->input(
			"Menu.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
			)
		);
	echo $Utilitys->filtertagend();
	echo"
		<div id=\"menusarchloading\">
	";
			echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id=null);
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('Menu Name')  => array('class' => 'highlight sortable')
						), 
						array(
							__('URL')  => array('class' => 'highlight sortable')
						),
						array(
							__('Is Parent')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Is Permanent')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Is Active')  => array('class' => 'highlight sortable')
						), 
						__('Action')
					)
				);
				$menurows = array();
				$countMenu  = 0-1+$this->Paginator->counter('%start%');
				foreach($menus as $Thismenu):
					$countMenu ++;
					$menurows[]= array($countMenu ,$Thismenu["Menu"]["menuname"],$Thismenu["Menu"]["menuurl"],$Thismenu["Menu"]["isParent"],$Thismenu["Menu"]["isPermanent"],$Utilitys->statusURL($this->request["controller"], 'menu', $Thismenu["Menu"]["id"], $Thismenu["Menu"]["menuuuid"], $Thismenu["Menu"]["menuisactive"]),$Utilitys->cusurl($this->request["controller"], 'menu', $Thismenu["Menu"]["id"], $Thismenu["Menu"]["menuuuid"]));
				endforeach;
				echo $this->Html->tableCells(
					$menurows
				);
			echo $Utilitys->tableend();
			echo $Utilitys->paginationcommon();

	echo"
				</div>
		<script>
			$(document).ready(function() {
				$(\"#MenuSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#menusarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."menus/menusearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#menusarchloading\").html(result);
						}
					);
				});
				$(\"#MenuParentmenu\").change(function(){
					var MenuParentmenu = $(\"#MenuParentmenu\").val();
					$(\"#menusarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."menus/menusearchbytext',
						{MenuParentmenu:MenuParentmenu},
						function(result) {
							$(\"#menusarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>