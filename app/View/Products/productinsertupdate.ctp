<?php 
    echo"
        <table>
            <tr>
                <td>
                    <h2>".__('Add Product Information')."</h2>
                </td>
            </tr>
            <tr>
                <td>
    ";
echo $this->Form->create('Products', array('action' => 'productinsertupdateaction', 'type' => 'file'));
echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$product[0]['Product']['id']));
echo $this->Form->input('productuuid', array('type'=>'hidden', 'value'=>@$product[0]['Product']['productuuid']));
$options = array(1 => __("Yes"), 0 =>__("No"));
        $attributes = array(
            'legend' => false, 
            'label' => true, 
            'value' => (@$product[0]['Product']['productisactive'] ? $product[0]['Product']['productisactive'] : 0), 
            'class' => 'form-inline'
        );
$options_format = array(0 => __("KG"), 1 =>__("High"));
        $attributes_format = array(
            'legend' => false, 
            'label' => true, 
            'value' => (@$product[0]['Product']['productformat'] ? $product[0]['Product']['productformat'] : 0), 
            'class' => 'form-inline'
        );         
 $p_image = @$product[0]['Product']['productimage'];       
echo $Utilitys->tablestart();
echo"
        <tr>
            <td>".__("Product Category")."</td>
            <td>
                ".$this->Form->input(
                                    "Product.productcategory_id",
                                    array(
                                        'type' => 'select',
                                        "options" => $productcategory, 
                                        'empty' => 'Select Product Category',
                                        'div' => false, 
                                        'label' => false,
                                        "optgroup label" => false,
                                        'data-validation-engine'=>'validate[required]',
                                        'selected'=> (@$product[0]['Product']['productcategory_id'] ? $product[0]['Product']['productcategory_id'] : 0)
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("Product Type")."</td>
            <td>
                ".$this->Form->input(
                                    "Product.producttype_id",
                                    array(
                                        'type' => 'select',
                                        "options" => $producttype, 
                                        'empty' => 'Select Product Type',
                                        'div' => false, 
                                        'label' => false,
                                        "optgroup label" => false,
                                        'data-validation-engine'=>'validate[required]',
                                        'selected'=> (@$product[0]['Product']['producttype_id'] ? $product[0]['Product']['producttype_id'] : 0)
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("Format")."</td>
            <td>
                ".$this->Form->radio(
                'Product.productformat',
                $options_format, 
                $attributes_format
                )."
            </td>
        </tr>
        <tr>
            <td>".__("Product Unit")."</td>
            <td>
                ".$this->Form->input(
                                    "Product.unit_id",
                                    array(
                                        'type' => 'select',
                                        "options" => $productunit, 
                                        'empty' => 'Select Unit',
                                        'div' => false, 
                                        'label' => false,
                                        "optgroup label" => false,
                                        'data-validation-engine'=>'validate[required]',
                                        'selected'=> (@$product[0]['Product']['unit_id'] ? $product[0]['Product']['unit_id'] : 0)
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("Product Code")."</td>
            <td>
                ".$this->Form->input(
                                    "Product.productcode",
                                    array(
                                        'type' => 'text',
                                        'div' => false, 
                                        'label' => false,
                                        'data-validation-engine'=>'validate[required]',
                                        'value'=> @$product[0]['Product']['productcode']
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("Product Name")."</td>
            <td>
                ".$this->Form->input(
                                    "Product.productname",
                                    array(
                                        'type' => 'text',
                                        'div' => false, 
                                        'label' => false,
                                        'data-validation-engine'=>'validate[required]',
                                        'value'=> @$product[0]['Product']['productname']
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("Product Name BN")."</td>
            <td>
                ".$this->Form->input(
                                    "Product.productnamebn",
                                    array(
                                        'type' => 'text',
                                        'div' => false, 
                                        'label' => false,
                                        'value'=> @$product[0]['Product']['productnamebn']
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("Product Price")."</td>
            <td>
                ".$this->Form->input(
                                    "Product.productprice",
                                    array(
                                        'type' => 'text',
                                        'div' => false, 
                                        'label' => false,
                                        'data-validation-engine'=>'validate[required]',
                                        'value'=> @$product[0]['Product']['productprice']
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("Product Discount")."</td>
            <td>
                ".$this->Form->input(
                                    "Product.productdiscount",
                                    array(
                                        'type' => 'text',
                                        'div' => false, 
                                        'label' => false,
                                        'value'=> @$product[0]['Product']['productdiscount']
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("isActive")."</td>
            <td>
                ".$this->Form->radio(
                'Product.productisactive',
                $options, 
                $attributes
                )."
            </td>
        </tr>
        <tr>
            <td>".__("Product Image")."</td>
            <td id=\"area\">
                ".$this->Form->file(
                'Product.photo1',
                array(
                    'label'=>false,
                    'div'=>false
                )
            )."
                <input type=\"hidden\" name=\"data[Product][photo2]\" value=\"$p_image\"  />
            </td>
        </tr>
        <tr>
            <td colspan=\"2\">
                ".$Utilitys->allformbutton('',$this->request["controller"],$page='product')."
            </td>
        </tr>
    </table>
    ";
    echo $this->Form->end();
    echo"
                </td>
            </tr>
        </table>
    ";
?>   

<?php echo $this->Form->end(); ?> 
<script type="text/javascript">
 jQuery(document).ready(function(){
    jQuery("#ProductsProductinsertupdateactionForm").validationEngine();
 });
</script>