<?php
	echo "<h2>".__('Bank Account'). $Utilitys->addurl($title=__("Add New Bank Account"), $this->request["controller"], $page="bankaccount")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Bankaccount.bank_id",
			array(
				'type'=>'select',
				"options"=>array($bank),
				'empty'=>__('Select Bank'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
			)
		)
		.$this->Form->input(
			"Bankaccount.bankbranch_id",
			array(
				'type'=>'select',
				"options"=>array($bankbranch),
				'empty'=>__('Select Branch'),
				'div'=>false,
				'tabindex'=>2,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
			)
		)
		.$this->Form->input(
			"Bankaccount.bankaccounttype_id",
			array(
				'type'=>'select',
				"options"=>array($bankaccounttype),
				'empty'=>__('Select Account Type'),
				'div'=>false,
				'tabindex'=>3,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
			)
		)
		.$this->Form->input(
			"Bankaccount.bankaccount_id",
			array(
				'type'=>'select',
				"options"=>array($bankaccountlist),
				'empty'=>__('Select Account'),
				'div'=>false,
				'tabindex'=>4,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
			)
		)
		.$this->Form->input(
			"Bankaccount.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => ''
			)
		);
		$Utilitys->printbutton ($printableid='bankaccountsarchloading', $searchfield=null);
	echo $Utilitys->filtertagend();
	echo"
		<div id=\"bankaccountsarchloading\" class=\"table-responsive\">
	";
			echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id='printtable');
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('Balance')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Account')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Account BN')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Number')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Bank')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Branch')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Account Type')  => array('class' => 'highlight sortable')
						), 
						__('Active'), 
						__('Action')
					)
				);
				$bankaccountrows = array();
				$countBankaccount = 0-1+$this->Paginator->counter('%start%');
				foreach($bankaccount as $Thisbankaccount):
					$countBankaccount++;
					$bankaccountrows[]= array($countBankaccount,$Thisbankaccount["Bankaccount"]["bankaccount_totalamount"]?$Thisbankaccount["Bankaccount"]["bankaccount_totalamount"]:'0.00',$Thisbankaccount["Bankaccount"]["bankaccountname"],$Thisbankaccount["Bankaccount"]["bankaccountnamebn"],$Utilitys->amountDetailsURL($Thisbankaccount["Bankaccount"]["bankaccountnumber"], $Thisbankaccount["Bankaccount"]["id"], "Banktransaction","bankaccount_id"),$Utilitys->amountDetailsURL($Thisbankaccount["Bankaccount"]["bank_name"], $Thisbankaccount["Bankaccount"]["bank_id"], "Banktransaction","bank_id"),$Utilitys->amountDetailsURL($Thisbankaccount["Bankaccount"]["bankbranch_name"], $Thisbankaccount["Bankaccount"]["bankbranch_id"], "Banktransaction","bankbranch_id"),$Thisbankaccount["Bankaccount"]["bankaccounttype_name"],$Utilitys->statusURL($this->request["controller"], 'bankaccount', $Thisbankaccount["Bankaccount"]["id"], $Thisbankaccount["Bankaccount"]["bankaccountuuid"], $Thisbankaccount["Bankaccount"]["bankaccountisactive"]),$Utilitys->cusurl($this->request["controller"], 'bankaccount', $Thisbankaccount["Bankaccount"]["id"], $Thisbankaccount["Bankaccount"]["bankaccountuuid"]));
				endforeach;
				echo $this->Html->tableCells(
					$bankaccountrows
				);
			echo $Utilitys->tableend();
			echo $Utilitys->paginationcommon();
	echo"
		</div>
		<script>
			$(document).ready(function() {
				$(\"#BankaccountSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#bankaccountsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/bankaccountsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#bankaccountsarchloading\").html(result);
						}
					);
				});
				$(\"#BankaccountBankId\").change(function(){
					var BankaccountBankId = $(\"#BankaccountBankId\").val();
					var BankaccountBankbranchId = $(\"#BankaccountBankbranchId\").val();
					var BankaccountBankaccounttypeId = $(\"#BankaccountBankaccounttypeId\").val();
					var BankaccountBankaccountId = $(\"#BankaccountBankaccountId\").val();
					$(\"#bankaccountsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/bankaccountsearchbytext',
						{BankaccountBankId:BankaccountBankId,BankaccountBankbranchId:BankaccountBankbranchId,BankaccountBankaccounttypeId:BankaccountBankaccounttypeId,BankaccountBankaccountId:BankaccountBankaccountId},
						function(result) {
							$(\"#bankaccountsarchloading\").html(result);
						}
					);
				});
				$(\"#BankaccountBankbranchId\").change(function(){
					var BankaccountBankId = $(\"#BankaccountBankId\").val();
					var BankaccountBankbranchId = $(\"#BankaccountBankbranchId\").val();
					var BankaccountBankaccounttypeId = $(\"#BankaccountBankaccounttypeId\").val();
					var BankaccountBankaccountId = $(\"#BankaccountBankaccountId\").val();
					$(\"#bankaccountsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/bankaccountsearchbytext',
						{BankaccountBankId:BankaccountBankId,BankaccountBankbranchId:BankaccountBankbranchId,BankaccountBankaccounttypeId:BankaccountBankaccounttypeId,BankaccountBankaccountId:BankaccountBankaccountId},
						function(result) {
							$(\"#bankaccountsarchloading\").html(result);
						}
					);
				});
				$(\"#BankaccountBankaccounttypeId\").change(function(){
					var BankaccountBankId = $(\"#BankaccountBankId\").val();
					var BankaccountBankbranchId = $(\"#BankaccountBankbranchId\").val();
					var BankaccountBankaccounttypeId = $(\"#BankaccountBankaccounttypeId\").val();
					var BankaccountBankaccountId = $(\"#BankaccountBankaccountId\").val();
					$(\"#bankaccountsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/bankaccountsearchbytext',
						{BankaccountBankId:BankaccountBankId,BankaccountBankbranchId:BankaccountBankbranchId,BankaccountBankaccounttypeId:BankaccountBankaccounttypeId,BankaccountBankaccountId:BankaccountBankaccountId},
						function(result) {
							$(\"#bankaccountsarchloading\").html(result);
						}
					);
				});
				$(\"#BankaccountBankaccountId\").change(function(){
					var BankaccountBankId = $(\"#BankaccountBankId\").val();
					var BankaccountBankbranchId = $(\"#BankaccountBankbranchId\").val();
					var BankaccountBankaccounttypeId = $(\"#BankaccountBankaccounttypeId\").val();
					var BankaccountBankaccountId = $(\"#BankaccountBankaccountId\").val();
					$(\"#bankaccountsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/bankaccountsearchbytext',
						{BankaccountBankId:BankaccountBankId,BankaccountBankbranchId:BankaccountBankbranchId,BankaccountBankaccounttypeId:BankaccountBankaccounttypeId,BankaccountBankaccountId:BankaccountBankaccountId},
						function(result) {
							$(\"#bankaccountsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>
