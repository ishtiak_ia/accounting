<?php
echo $this->Form->create('Groups', array('action' => 'groupaddeditaction', 'type' => 'file'));
echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$group[0]['Group']['id']));

echo $this->Form->input('groupuuid', array('type'=>'hidden', 'value'=>@$group[0]['Group']['groupuuid']));
$options = array(1 => __("Yes"), 0 =>__("No"));
        $attributes = array(
            'legend' => false, 
            'label' => true, 
            'value' => (@$group[0]['Group']['groupisactive'] ? $group[0]['Group']['groupisactive'] : 0),
            'class' => 'form-inline'
        );
        $attributes_permanent = array(
                'legend' => false, 
                'label' => true, 
                'value' => (@$group[0]['Group']['groupispermanent'] ? $group[0]['Group']['groupispermanent'] : 0),
                'class' => 'form-inline'
            );
echo"<h3>".__("Add Group information")."</h3>";  
echo"<div class=\"row\">";
    echo $this->Form->input(
            "Group.groupname",
            array(
                'type' => 'text',
                'div' => array('class'=> 'form-group'),
                'label' => __('Group'),
                'tabindex'=>2,
                'placeholder' => __("Group"),
                'class'=> 'form-control',
                'style' => '',
                'data-validation-engine'=>'validate[required]',
                'value' => @$group[0]['Group']['groupname']
            )
        );
    echo"<div class=\"form-group\">";
            echo"<label>isParmanent?</label><br>";
            echo $this->Form->radio(
                'Group.groupispermanent', 
                $options,
                $attributes_permanent
            );
    echo"</div>";
    echo"<div class=\"form-group\">";
            echo"<label>isActive?</label><br>";
            echo $this->Form->radio(
                'Group.groupisactive', 
                $options,
                $attributes
            );
    echo"</div>";
    echo $Utilitys->allformbutton('',$this->request["controller"],$page='group');
echo"</div>";
echo $this->Form->end();      
 ?>

