<?php
//pr($banktransaction);
	echo "<h2>".__('Bank Transaction'). $Utilitys->addurl($title=__("Add New Bank Transaction"), $this->request["controller"], $page="banktransaction")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Banktransaction.bankaccount_id",
			array(
				'type'=>'select',
				"options"=>array($bankaccount),
				'empty'=>__('Select Account'),
				'div'=>array('class'=>'form-group'),
				'tabindex'=>4,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
			)
		)
		.$this->Form->input(
			"Banktransaction.searchtext",
			array(
				'type' => 'text',
				'div'=>array('class'=>'form-group'),
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => ''
			)
		);
		$Utilitys->printbutton ($printableid='banktransactionsarchloading', $searchfield=null);
	echo $Utilitys->filtertagend();
	echo"
		<div id=\"banktransactionsarchloading\">
	";
		echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id='printtable');
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('Date')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Particulars')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Chq.No')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Withdraw')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Deposits')  => array('class' => 'highlight sortable')
						), 
						__('Active'), 
						__('Action')
					)
				);
				$bankrows = array();
				$countBanktransaction = 0-1+$this->Paginator->counter('%start%');
				foreach($banktransaction as $Thisbanktransaction):
					$countBanktransaction++;
					$bankrows[]= array($countBanktransaction,$Thisbanktransaction["Banktransaction"]["transactiondate"],$Thisbanktransaction["Banktransaction"]["banktransaction_particulars"],$Thisbanktransaction["Banktransaction"]["banktransaction_chequenumber"],array($Thisbanktransaction["Banktransaction"]["banktransaction_withdraw"],'colspan="1" align="right"'),array($Thisbanktransaction["Banktransaction"]["banktransaction_deposit"],'colspan="1" align="right"'),$Utilitys->statusURL($this->request["controller"], 'banktransaction', $Thisbanktransaction["Banktransaction"]["id"], $Thisbanktransaction["Banktransaction"]["banktransactionuuid"], $Thisbanktransaction["Banktransaction"]["banktransactionisactive"], $Thisbanktransaction["Banktransaction"]["transaction_id"]),$Utilitys->cusurl($this->request["controller"], 'banktransaction', $Thisbanktransaction["Banktransaction"]["id"], $Thisbanktransaction["Banktransaction"]["banktransactionuuid"], $Thisbanktransaction["Banktransaction"]["transaction_id"]));
				endforeach;
				echo $this->Html->tableCells(
					$bankrows
				);
			echo $Utilitys->tableend();
		echo $Utilitys->paginationcommon();
	echo"
		</div>
		<script>
			$(document).ready(function() {
				$(\"#BanktransactionSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#banktransactionsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/banktransactionsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#banktransactionsarchloading\").html(result);
						}
					);
				});
				$(\"#BanktransactionBankaccountId\").change(function(){
					var BanktransactionBankaccountId = $(\"#BanktransactionBankaccountId\").val();
					$(\"#banktransactionsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/banktransactionsearchbytext',
						{BanktransactionBankaccountId:BanktransactionBankaccountId},
						function(result) {
							$(\"#banktransactionsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>
