<?php
echo "<script>
$(document).ready(function() {

$(\"#BranchCompanyId\").change(function(){
					var BranchCompanyId = $(\"#BranchCompanyId\").val();
					
					$(\"#branchsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."branches/branchsearchbytext',
						{BranchCompanyId:BranchCompanyId},
						function(result) {
							$(\"#branchsarchloading\").html(result);
						}
					);
				});	

$(\"#BranchBranchId\").change(function(){
					var BranchBranchId = $(\"#BranchBranchId\").val();
					
					$(\"#branchsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."branches/branchsearchbytext',
						{BranchBranchId:BranchBranchId},
						function(result) {
							$(\"#branchsarchloading\").html(result);
						}
					);
				});
});
</script>"
?>
<?php
	echo "<h2>".__('List of Branch'). $Utilitys->addurl($title=__("Add New Branch"), $this->request["controller"], $page="branch")."</h2>";
	echo $Utilitys->filtertagstart();
	echo $this->Form->input(
			"Branch.company_id",
			array(
				'type'=>'select',
				"options"=>array($company),
				'empty'=>__('Select Company'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
			)
		)
		.$this->Form->input(
			"Branch.branch_id",
			array(
				'type'=>'select',
				"options"=>array($branchlist),
				'empty'=>__('Select Branch'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
			)
		);
		/*.$this->Form->input(
			"BCompany.companygroupname",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'tabindex'=>2,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => ''
			)
		);*/
		$Utilitys->printbutton ($printableid='salarysearchloading', $searchfield=null);
	echo $Utilitys->filtertagend();
	echo"
		<div id=\"branchsarchloading\">
	";
		echo $Utilitys->paginationcommon();
		echo $Utilitys->tablestart($class=null, $id=null);
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Company Name')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Branch Name (EN)')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Branch Name (BN)')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Branch Code')  => array('class' => 'highlight sortable')
					), 
					__('Active'), 
					__('Action')
				)
			);
			$branchrows = array();
			$countBranch = 0-1+$this->Paginator->counter('%start%');
			foreach($branches as $branch):
				$countBranch++;
				$branchrows[]= array($countBranch,$branch["Branch"]["company_name"],$branch["Branch"]["branchname"],$branch["Branch"]["branchnamebn"],$branch["Branch"]["branchcode"],$Utilitys->statusURL($this->request["controller"],'branch',$branch["Branch"]["id"],$branch["Branch"]["branchuuid"], $branch["Branch"]["branchisactive"]),$Utilitys->cusurl($this->request["controller"],'branch',$branch["Branch"]["id"],$branch["Branch"]["branchuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$branchrows
			);
		echo $Utilitys->tableend();
		echo $Utilitys->paginationcommon();
	echo"
		</div>
	";
?>
