<?php
App::uses('AppModel', 'Model');
class Location extends AppModel {
	public $name = 'Location';
	public $usetables = 'locations';
	var $belongsTo  = array(
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'locationinsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'locationupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'locationdeleteid'
		),
		'LocationParent' => array(
			'fields' =>array('LocationParent.Locationname AS Parentlocationname'),
			'className'    => 'Location',
			'foreignKey'    => false,
			'conditions'	=> array(
				'LocationParent.id=Location.locationparentid'
			)
		)
	);
	var $virtualFields = array(
		'locationparent_name' => 'CONCAT(LocationParent.locationname, " / ", LocationParent.locationnamebn)',
		'location_name' => 'CONCAT(Location.locationname, " / ", Location.locationnamebn)',
		'isActive' => 'IF(Location.locationisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Location.locationisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
	public $validate = array(
		'locationname' => array(
			'locationname_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This Location Name field is required',
				'last' => true
			)
		),
		'locationnamebn' => array(
			'locationnamebn_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This Location Name in Bangla field is required',
				'last' => true
			)
		),
	);
}

?>