<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('Stocklocation Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Stocklocation Name BN')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Stocklocation Address')  => array('class' => 'highlight sortable')
				),
				__('Active'), 
				__('Action')
			)
		);
		$stocklocationrows = array();
		$countStocklocation = 0-1+$this->Paginator->counter('%start%');
		foreach($stocklocations as $Thisstocklocation):
			$countStocklocation++;
			$stocklocationrows[]= array($countStocklocation,$Thisstocklocation["Stocklocation"]["stocklocationname"],$Thisstocklocation["Stocklocation"]["stocklocationnamebn"],$Thisstocklocation["Stocklocation"]["stocklocationaddress"],$Utilitys->statusURL($this->request["controller"], 'stocklocation', $Thisstocklocation["Stocklocation"]["id"], $Thisstocklocation["Stocklocation"]["stocklocationuuid"], $Thisstocklocation["Stocklocation"]["stocklocationisactive"]),$Utilitys->cusurl($this->request["controller"], 'stocklocation', $Thisstocklocation["Stocklocation"]["id"], $Thisstocklocation["Stocklocation"]["stocklocationuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$stocklocationrows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#StocklocationSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#stocklocationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#stocklocationsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>