<?php 
	echo $this->Form->create('Banks', array('action' => 'bankinsertupdateaction'));
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$bank[0]['Bank']['id']));
		echo $this->Form->input('bankuuid', array('type'=>'hidden', 'value'=>@$bank[0]['Bank']['bankuuid']));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$bank[0]['Bank']['bankisactive'] ? $bank[0]['Bank']['bankisactive'] : 0),
			'class' => 'form-inline'
		);
	echo"<h3>".__("Add Bank information")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-8\">";
			echo $this->Form->input(
				"Bank.banktype_id",
				array(
					'type'=>'select',
					"options"=>$banktype,
					'empty'=>__('Select Bank Type'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>1,
					'label'=>__('Select Bank Type'),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style'=>'',
					"selected" => (@$bank[0]['Bank']['banktype_id'] ? $bank[0]['Bank']['banktype_id'] : 0)
				)
			);
			echo $this->Form->input(
				"Bank.bankname",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Bank'),
					'tabindex'=>2,
					'placeholder' => __("Bank"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style' => '',
					'value' => @$bank[0]['Bank']['bankname']
				)
			);
			echo $this->Form->input(
				"Bank.banknamebn",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Bank BN'),
					'tabindex'=>3,
					'placeholder' => __("Bank BN"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style' => '',
					'value' => @$bank[0]['Bank']['banknamebn']
				)
			);
			echo"<div class=\"form-group\">";
				echo"<label>isActive?</label><br>";
				echo $this->Form->radio(
					'Bank.bankisactive', 
					$options,
					$attributes
				);
			echo"</div>";
			echo $Utilitys->allformbutton('',$this->request["controller"],$page='bank');
		echo"</div>";
	echo"</div>";
	echo $this->Form->end();
	echo"
	<script>
		$(document).ready(function() {
			$(\"#BanksBankinsertupdateactionForm\").validationEngine();
		});
	</script>
	";
?>