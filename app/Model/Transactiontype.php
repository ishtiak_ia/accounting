<?php
App::uses('AppModel', 'Model');
class Transactiontype extends AppModel {
	public $name = 'Transactiontype';
	public $usetables = 'transactiontypes';
	var $belongsTo  = array(
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'transactiontypeinsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'transactiontypeupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'transactiontypedeleteid'
		)
	);
	var $virtualFields = array(
		'transactiontype_name' => 'CONCAT(Transactiontype.transactiontypename, " / ", Transactiontype.transactiontypenamebn)',
		'isActive' => 'IF(Transactiontype.transactiontypeisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Transactiontype.transactiontypeisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
	public $validate = array(
		'transactiontypename' => array(
			'transactiontypename_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This Transaction Type Name field is required',
				'last' => true
			)
		),
		'transactiontypenamebn' => array(
			'transactiontypenamebn_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This Transaction Type Name BN field is required',
				'last' => true
			)
		),
	);
}

?>