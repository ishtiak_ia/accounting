<?php
	echo "<h2 class=\"page-header\">".__(@$users[0]["Group"]["groupname"]). $Utilitys->addurl($title=__("Add New User"), $this->request['controller'], $page="user")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"User.company_id",
			array(
				'type'=>'select',
				"options"=>array($company),
				'class'=> 'form-control',
				'empty'=>__('Select Company'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'style'=>''
				)
			)
			.$this->Form->input(
			"User.branch_id",
			array(
				'type'=>'select',
				"options"=>array($branch),
				'class'=> 'form-control',
				'empty'=>__('Select Branch'),
				'div'=>false,
				'tabindex'=>2,
				'label'=>false,
				'style'=>''
				)
			)
			.$this->Form->input(
			"User.group_id",
			array(
				'type'=>'select',
				"options"=>array($group),
				'empty'=>__('Select Group'),
				'div'=>false,
				'tabindex'=>3,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>'',
				'selected'=>$sgroup_id
				)
			)
			.$this->Form->input(
			"User.department_id",
			array(
				'type'=>'select',
				"options"=>array($department),
				'empty'=>__('Select Department'),
				'div'=>false,
				'tabindex'=>4,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
				)
			)
			.$this->Form->input(
	        "User.category_id",
	        array(
	            'type' => 'select', 
            	"options" => $category, 
				'class'=> 'form-control',
				'empty'=>__('Select Category'),
				'div'=>false,
				'tabindex'=>5,
				'label'=>false,
				'style'=>''
	            )
	        )
			.$this->Form->input(
	        "User.subject_id",
	        array(
	            'type' => 'select', 
            	"options" => $subject, 
				'class'=> 'form-control',
				'empty'=>__('Select Subject'),
				'div'=>false,
				'tabindex'=>6,
				'label'=>false,
				'style'=>''
	            )
	        )
			.$this->Form->input(
	        "User.designation_id",
	        array(
	            'type' => 'select', 
            	"options" => $designation, 
				'class'=> 'form-control',
				'empty'=>__('Select Designation'),
				'div'=>false,
				'tabindex'=>7,
				'label'=>false,
				'style'=>''
	            )
	        )
			.$this->Form->input(
	        "User.institute_id",
	        array(
	            'type' => 'select', 
            	"options" => $institute, 
				'class'=> 'form-control',
				'empty'=>__('Select Institute'),
				'div'=>false,
				'tabindex'=>8,
				'label'=>false,
				'style'=>''
	            )
	        )
			.$this->Form->input(
			"User.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'placeholder' => __("Search Word"),
				'label' => false,
				'tabindex'=>5,
				'class'=> 'form-control'
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"usersarchloading\">
	";
	echo $Utilitys->paginationcommon();	
	echo $Utilitys->tablestart();
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('User Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('User Fullname')  => array('class' => 'highlight sortable')
				),
				array(
					__('Group Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Company Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Branch Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Joining Salary')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Joining Date')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Email')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Phone')  => array('class' => 'highlight sortable')
				),
				array(
					__('Is Active')  => array('class' => 'highlight sortable')
				),
				__('Action')
			)
		);
		$userrows = array();
		$countUser = 0-1+$this->Paginator->counter('%start%');

		foreach($users as $Thisuser):
			$countUser++;
			$userrows[]= array(
				$countUser,
				$Thisuser["User"]["username"],
				$Utilitys->userprofileView($Thisuser["User"]["user_fullname"], $Thisuser["User"]["id"], $Thisuser["User"]["useruuid"], "User"),
				$Thisuser["Group"]["groupname"],
				$Thisuser["Company"]["companyname"]. " / " .$Thisuser["Company"]["companynamebn"],
				$Thisuser["Branch"]["branchname"]. " / " .$Thisuser["Branch"]["branchnamebn"],
				$Thisuser["User"]["userjoiningsalary"],
				$Thisuser["User"]["userjoiningdate"],
				$Thisuser["User"]["useremailaddress"],
				$Thisuser["User"]["usertelephone"],
				$Utilitys->statusURL(
					$this->request["controller"], 
					'user', 
					$Thisuser["User"]["id"], 
					$Thisuser["User"]["useruuid"], 
					$Thisuser["User"]["userisactive"]
				),
				$Utilitys->cusurl(
					$this->request['controller'], 
					'user', 
					$Thisuser["User"]["id"], 
					$Thisuser["User"]["useruuid"]
				)
			);
		endforeach;
		echo $this->Html->tableCells(
			$userrows
		);
	echo $Utilitys->tableend();
			
	echo $Utilitys->paginationcommon();

	echo"
				</div>
		<script>
			$(document).ready(function() {
				$(\"#UserSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#usersarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usersearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#usersarchloading\").html(result);
						}
					);
				});
				$(\"#UserGroupId\").change(function(){
					var UserGroupId = $(\"#UserGroupId\").val();
					var UserCompanyId = $(\"#UserCompanyId\").val();
					var UserBranchId = $(\"#UserBranchId\").val();
					var UserCategoryId = $(\"#UserCategoryId\").val();
					var UserDepartmentId = $(\"#UserDepartmentId\").val();
					var UserSubjectId = $(\"#UserSubjectId\").val();
					var UserDesignationId = $(\"#UserDesignationId\").val();
					var UserInstituteId = $(\"#UserInstituteId\").val();
					$(\"#usersarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usersearchbytext',
						{UserGroupId:UserGroupId,UserCompanyId:UserCompanyId,UserBranchId:UserBranchId, UserCategoryId:UserCategoryId, UserDepartmentId:UserDepartmentId, UserSubjectId:UserSubjectId, UserDesignationId:UserDesignationId, UserInstituteId:UserInstituteId},
						function(result) {
							$(\"#usersarchloading\").html(result);
						}
					);
					$.post(
						'".$this->webroot."users/designationlistbygroupidsearch',
							{UserGroupId:UserGroupId},
							function(result) {
								$(\"#UserDesignationId\").html(result);
								$(\"#usersarchloading\").html('');
							}
					);
				});
				$(\"#UserCompanyId\").change(function(){
					var UserGroupId = $(\"#UserGroupId\").val();
					var UserCompanyId = $(\"#UserCompanyId\").val();
					var UserBranchId = $(\"#UserBranchId\").val();
					var UserCategoryId = $(\"#UserCategoryId\").val();
					var UserDepartmentId = $(\"#UserDepartmentId\").val();
					var UserSubjectId = $(\"#UserSubjectId\").val();
					var UserDesignationId = $(\"#UserDesignationId\").val();
					var UserInstituteId = $(\"#UserInstituteId\").val();
					$(\"#usersarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usersearchbytext',
						{UserGroupId:UserGroupId,UserCompanyId:UserCompanyId,UserBranchId:UserBranchId, UserCategoryId:UserCategoryId, UserDepartmentId:UserDepartmentId, UserSubjectId:UserSubjectId, UserDesignationId:UserDesignationId, UserInstituteId:UserInstituteId},
						function(result) {
							$(\"#usersarchloading\").html(result);
						}
					);
				});
				$(\"#UserBranchId\").change(function(){
					var UserGroupId = $(\"#UserGroupId\").val();
					var UserCompanyId = $(\"#UserCompanyId\").val();
					var UserBranchId = $(\"#UserBranchId\").val();
					var UserCategoryId = $(\"#UserCategoryId\").val();
					var UserDepartmentId = $(\"#UserDepartmentId\").val();
					var UserSubjectId = $(\"#UserSubjectId\").val();
					var UserDesignationId = $(\"#UserDesignationId\").val();
					var UserInstituteId = $(\"#UserInstituteId\").val();
					$(\"#usersarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usersearchbytext',
						{UserGroupId:UserGroupId,UserCompanyId:UserCompanyId,UserBranchId:UserBranchId, UserCategoryId:UserCategoryId, UserDepartmentId:UserDepartmentId, UserSubjectId:UserSubjectId, UserDesignationId:UserDesignationId, UserInstituteId:UserInstituteId},
						function(result) {
							$(\"#usersarchloading\").html(result);
						}
					);
				});
				$(\"#UserCategoryId\").change(function(){
					var UserGroupId = $(\"#UserGroupId\").val();
					var UserCompanyId = $(\"#UserCompanyId\").val();
					var UserBranchId = $(\"#UserBranchId\").val();
					var UserCategoryId = $(\"#UserCategoryId\").val();
					var UserDepartmentId = $(\"#UserDepartmentId\").val();
					var UserSubjectId = $(\"#UserSubjectId\").val();
					var UserDesignationId = $(\"#UserDesignationId\").val();
					var UserInstituteId = $(\"#UserInstituteId\").val();
					$(\"#usersarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usersearchbytext',
						{UserGroupId:UserGroupId,UserCompanyId:UserCompanyId,UserBranchId:UserBranchId, UserCategoryId:UserCategoryId, UserDepartmentId:UserDepartmentId, UserSubjectId:UserSubjectId, UserDesignationId:UserDesignationId, UserInstituteId:UserInstituteId},
						function(result) {
							$(\"#usersarchloading\").html(result);
						}
					);
				});
				$(\"#UserDepartmentId\").change(function(){
					var UserGroupId = $(\"#UserGroupId\").val();
					var UserCompanyId = $(\"#UserCompanyId\").val();
					var UserBranchId = $(\"#UserBranchId\").val();
					var UserCategoryId = $(\"#UserCategoryId\").val();
					var UserDepartmentId = $(\"#UserDepartmentId\").val();
					var UserSubjectId = $(\"#UserSubjectId\").val();
					var UserDesignationId = $(\"#UserDesignationId\").val();
					var UserInstituteId = $(\"#UserInstituteId\").val();
					$(\"#usersarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usersearchbytext',
						{UserGroupId:UserGroupId,UserCompanyId:UserCompanyId,UserBranchId:UserBranchId, UserCategoryId:UserCategoryId, UserDepartmentId:UserDepartmentId, UserSubjectId:UserSubjectId, UserDesignationId:UserDesignationId, UserInstituteId:UserInstituteId},
						function(result) {
							$(\"#usersarchloading\").html(result);
						}
					);
				});
				$(\"#UserSubjectId\").change(function(){
					var UserGroupId = $(\"#UserGroupId\").val();
					var UserCompanyId = $(\"#UserCompanyId\").val();
					var UserBranchId = $(\"#UserBranchId\").val();
					var UserCategoryId = $(\"#UserCategoryId\").val();
					var UserDepartmentId = $(\"#UserDepartmentId\").val();
					var UserSubjectId = $(\"#UserSubjectId\").val();
					var UserDesignationId = $(\"#UserDesignationId\").val();
					var UserInstituteId = $(\"#UserInstituteId\").val();
					$(\"#usersarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usersearchbytext',
						{UserGroupId:UserGroupId,UserCompanyId:UserCompanyId,UserBranchId:UserBranchId, UserCategoryId:UserCategoryId, UserDepartmentId:UserDepartmentId, UserSubjectId:UserSubjectId, UserDesignationId:UserDesignationId, UserInstituteId:UserInstituteId},
						function(result) {
							$(\"#usersarchloading\").html(result);
						}
					);
				});
				$(\"#UserDesignationId\").change(function(){
					var UserGroupId = $(\"#UserGroupId\").val();
					var UserCompanyId = $(\"#UserCompanyId\").val();
					var UserBranchId = $(\"#UserBranchId\").val();
					var UserCategoryId = $(\"#UserCategoryId\").val();
					var UserDepartmentId = $(\"#UserDepartmentId\").val();
					var UserSubjectId = $(\"#UserSubjectId\").val();
					var UserDesignationId = $(\"#UserDesignationId\").val();
					var UserInstituteId = $(\"#UserInstituteId\").val();
					$(\"#usersarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usersearchbytext',
						{UserGroupId:UserGroupId,UserCompanyId:UserCompanyId,UserBranchId:UserBranchId, UserCategoryId:UserCategoryId, UserDepartmentId:UserDepartmentId, UserSubjectId:UserSubjectId, UserDesignationId:UserDesignationId, UserInstituteId:UserInstituteId},
						function(result) {
							$(\"#usersarchloading\").html(result);
						}
					);
				});
				$(\"#UserInstituteId\").change(function(){
					var UserGroupId = $(\"#UserGroupId\").val();
					var UserCompanyId = $(\"#UserCompanyId\").val();
					var UserBranchId = $(\"#UserBranchId\").val();
					var UserCategoryId = $(\"#UserCategoryId\").val();
					var UserDepartmentId = $(\"#UserDepartmentId\").val();
					var UserSubjectId = $(\"#UserSubjectId\").val();
					var UserDesignationId = $(\"#UserDesignationId\").val();
					var UserInstituteId = $(\"#UserInstituteId\").val();
					$(\"#usersarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usersearchbytext',
						{UserGroupId:UserGroupId,UserCompanyId:UserCompanyId,UserBranchId:UserBranchId, UserCategoryId:UserCategoryId, UserDepartmentId:UserDepartmentId, UserSubjectId:UserSubjectId, UserDesignationId:UserDesignationId, UserInstituteId:UserInstituteId},
						function(result) {
							$(\"#usersarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>
