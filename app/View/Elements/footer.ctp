                    <?php if( isset( $_SESSION['User']['id'] ) ) {
                    //YES, the user is Logged in
                    ?>
                    <footer role="contentinfo" id="site-footer">
                        <a href="#" class="logo-footer pull-left" target="_blank">
                            <?php echo $this->html->image('/img/innotech-monogram.png', array('alt'=> 'Logo of innotech')); ?>
                        </a>
                        <p class="site-credit pull-right text-right">
                            &copy; innotech 2010-<?php echo date('Y'); ?><br>
                            Powered by: <a href="http://innotech.com.bd/" target="_blank" title="Developed by Innotech"><em>Innotech</em></a>
                        </p>
                    </footer>
                    <!-- /#site-footer -->
                    <?php } //endif( isset( $_SESSION['User']['id'] ) ) ?>
                    
                    </div> <!-- .col-sm-10 col-sm-offset-2 main -->
                </div>
                <!-- /.main -->
            </div>
        </div>
        <!-- /.container-fluid -->
        
        <?php
        /* JavaScripts */
        echo $this->fetch('script');
        echo $this->Html->script('languages/jquery.validationEngine-en');
        echo $this->Html->script('jquery.validationEngine');
        echo $this->Html->script('jquery.prettyPhoto');
        echo $this->Html->script('bootstrap.min');
        echo $this->Html->script('bootstrap-switch.min');
        echo $this->Html->script('moment-with-langs.min');
        echo $this->Html->script('bootstrap-datetimepicker.min');
        echo $this->Html->script('jquery.print');
        echo $this->Html->script('jspdf.min');
        echo $this->Html->script('tableExport');
        echo $this->Html->script('jquery.base64');
        echo $this->Html->script('html2canvas');
        
        //echo $this->Html->script('jspdf.plugin.cell');
        echo $this->Html->script('addimage');
        echo $this->Html->script('standard_fonts_metrics');
        echo $this->Html->script('split_text_to_size');
        echo $this->Html->script('from_html');
        echo $this->Html->script('libs/html2pdf');
        echo $this->Html->script('custom');
        /* JavaScripts */
        ?>
        <script>
        
        /*! LEFT SIDEBAR AND SIDBEAR MENU */
        !function ($) {
            $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
                $(this).find('em:first').toggleClass("glyphicon-chevron-up");      
            }); 
            $(".left-sidebar span.icon").find('em:first').addClass("glyphicon-chevron-down");
        }(window.jQuery);
        $(window).on('resize', function () {
            if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
        });
        $(window).on('resize', function () {
            if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
        });
        $(document).ready(function(){
            $("a[rel^='prettyPhoto']").prettyPhoto({social_tools:''});
            setTimeout(function() {
            $("a i,#flashMessage").css({"display":"none","opacity":"0"}); 
            }, 5000);

        });
        </script>
        <?php //echo $this->element('sql_dump'); ?>
        <!-- scripts_for_layout -->
        <?php echo $scripts_for_layout; ?>
        <!-- Js writeBuffer -->
        <?php
        if (class_exists('JsHelper') && method_exists($this->Js, 'writeBuffer')) echo $this->Js->writeBuffer();
        // Writes cached scripts
        ?>
        
    </body>
</html>
