<?php 
	echo"
		<table>
			<tr>
				<td>
					".__('Add Chart of Account information')."
				</td>
			</tr>
			<tr>
				<td>
	";
	echo $this->Form->create('Coas', array('action' => 'coainsertupdateaction'));
		echo $this->Form->inpute('id', array('type'=>'hidden', 'value'=>@$coa[0]['Coa']['id']));
		echo $this->Form->inpute('coauuid', array('type'=>'hidden', 'value'=>@$coa[0]['Coa']['coauuid']));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$coa[0]['Coa']['coaisactive'] ? $coa[0]['Coa']['coaisactive'] : 1), 
			'class' => 'radio inline'
		);
					echo $Utilitys->tablestart($class=null, $id=null);
	echo"
						<!--<tr>
							<td>".__("Company")."</td>
							<td>
								".$this->Form->input(
									"Coa.company_id",array(
										'type'=>'select',
										"options"=>array($company),
										'empty'=>__('Select Company'),
										'div'=>false,
										'tabindex'=>1,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coa[0]['Coa']['company_id'] ? $coa[0]['Coa']['company_id'] : 0)
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("Branch")."</td>
							<td>
								".$this->Form->input(
									"Coa.branch_id",array(
										'type'=>'select',
										"options"=>array($branch),
										'empty'=>__('Select Branch'),
										'div'=>false,
										'tabindex'=>2,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coa[0]['Coa']['branch_id'] ? $coa[0]['Coa']['branch_id'] : 0)
									)
								)."
								<span id=\"branchloading\"></span>
							</td>
						</tr>-->
						<tr>
							<td>".__("Type")."</td>
							<td>
								".$this->Form->input(
									"Coa.coatype__code_id",
									array(
										'type'=>'select',
										"options"=>array($coatype_code),
										'empty'=>__('Select Code'),
										'div'=>false,
										'tabindex'=>1,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coa[0]['Coa']['coatype_id'] ? $coa[0]['Coa']['coatype_id'] : 0)
									)
								)."
							</td>
							<td>
								".$this->Form->input(
									"Coa.coatype_id",
									array(
										'type'=>'select',
										"options"=>array($coatype),
										'empty'=>__('Select Chart of Account Type'),
										'div'=>false,
										'tabindex'=>3,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coa[0]['Coa']['coatype_id'] ? $coa[0]['Coa']['coatype_id'] : 0)
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("Group")."</td>
							<td>
								".$this->Form->input(
									"Coa.coagroup__code_id",array(
										'type'=>'select',
										"options"=>array($coagroup_code),
										'empty'=>__('Select Code'),
										'div'=>false,
										'tabindex'=>4,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coa[0]['Coa']['coagroup_id'] ? $coa[0]['Coa']['coagroup_id'] : 0)
									)
								)."								
							</td>
							<td>
								".$this->Form->input(
									"Coa.coagroup_id",array(
										'type'=>'select',
										"options"=>array($coagroup),
										'empty'=>__('Select Chart of Account Group'),
										'div'=>false,
										'tabindex'=>4,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coa[0]['Coa']['coagroup_id'] ? $coa[0]['Coa']['coagroup_id'] : 0)
									)
								)."								
							</td>
						</tr>
						<tr>
						<td>".__("Class")."</td>
							<td>
								".$this->Form->input(
									"Coa.coaclass_code_id",array(
										'type'=>'select',
										"options"=>array($coaclass_code),
										'empty'=>__('Select Code'),
										'div'=>false,
										'tabindex'=>5,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coa[0]['Coa']['coaclass_id'] ? $coa[0]['Coa']['coaclass_id'] : 0)
									)
								)."								
							</td>
							<td>
								".$this->Form->input(
									"Coa.coaclass_id",array(
										'type'=>'select',
										"options"=>array($coaclass),
										'empty'=>__('Select Class'),
										'div'=>false,
										'tabindex'=>5,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coa[0]['Coa']['coaclass_id'] ? $coa[0]['Coa']['coaclass_id'] : 0)
									)
								)."								
							</td>
						</tr>
						<tr>
							<td>".__("Sub Class")."</td>
							<td>
								".$this->Form->input(
									"Coa.coaclasssubclass_code_id",array(
										'type'=>'select',
										"options"=>array($coaclasssubclass_code),
										'empty'=>__('Select Code'),
										'div'=>false,
										'tabindex'=>5,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coa[0]['Coa']['coaclasssubclass_id'] ? $coa[0]['Coa']['coaclasssubclass_id'] : 0)
									)
								)."
								<span id=\"classloading\"></span>
							</td>
							<td>
								".$this->Form->input(
									"Coa.coaclasssubclass_id",array(
										'type'=>'select',
										"options"=>array($coaclasssubclass),
										'empty'=>__('Select Sub Class'),
										'div'=>false,
										'tabindex'=>5,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coa[0]['Coa']['coaclasssubclass_id'] ? $coa[0]['Coa']['coaclasssubclass_id'] : 0)
									)
								)."
								<span id=\"classloading\"></span>
							</td>
						</tr>
						<tr>
							<td>".__("Code")."</td>
							<td>
								".$this->Form->input(
									"Coa.coacode",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'tabindex'=>6,
										'data-validation-engine'=>'validate[required]',
										'value' =>@$coa[0]['Coa']['coacode']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("COA")."</td>
							<td>
								".$this->Form->input(
									"Coa.coaname",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'tabindex'=>7,
										'data-validation-engine'=>'validate[required]',
										'value' =>@$coa[0]['Coa']['coaname']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("COA BN")."</td>
							<td>
								".$this->Form->input(
									"Coa.coanamebn",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'tabindex'=>8,
										'data-validation-engine'=>'validate[required]',
										'required' => 'required',
										'value' => @$coa[0]['Coa']['coanamebn']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("isActive")."</td>
							<td>
								".$this->Form->radio(
									'Coa.coaisactive', 
									$options, 
									$attributes
								)."
							</td>
						</tr>
						<tr>
							<td colspan=\"2\">
								".$Utilitys->allformbutton('',$this->request["controller"],$page='coa')."
							</td>
						</tr>
					</table>
	";
	echo $this->Form->end();
	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				$(\"#CoaCoatypeCodeId\").select2();
				$(\"#CoaCoatypeId\").select2();
				$(\"#CoaCoagroupCodeId\").select2();
				$(\"#CoaCoagroupId\").select2();
				$(\"#CoaCoaclassCodeId\").select2();
				$(\"#CoaCoaclassId\").select2();
				$(\"#CoaCoaclasssubclassCodeId\").select2();
				$(\"#CoaCoaclasssubclassId\").select2();
				$(\"#s2id_CoaCoatypeId\").css(\"width\", \"+=43\" );
				$(\"#s2id_CoaCoagroupId\").css(\"width\", \"+=41.5\" );
				$(\"#s2id_CoaCoaclassId\").css(\"width\", \"+=147\" );
				$(\"#s2id_CoaCoaclasssubclassId\").css(\"width\", \"+=141\" );
				jQuery(\"#CoasCoainsertupdateactionForm\").validationEngine({
					'custom_error_messages': {
						'required': {
							'message': \"".__("This field is mandatory.")."\"
						}
					}
				});    
				$(\"#CoaCompanyId\").change(function(){
					var company_id = $(this).val();
					$(\"#branchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."companies/listbranchbycompany/'+company_id, {company_id:company_id},
					function(result) {
						$(\"#CoaBranchId\").html(result);
						$(\"#branchloading\").html('');
					});
				});
				$(\"#CoaCoatypeId\").change(function(){
					var coatype_id = $(this).val(),type;
					$(\"#classloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/listcoagroup/'+coatype_id, {coatype_id:coatype_id},
					function(result) {
						$(\"#CoaCoagroupId\").html(result);
						$('#CoaCoatypeCodeId option[value='+coatype_id+']').attr('selected', 'selected');
						$(\"#classloading\").html('');
						type = $('#CoaCoatypeCodeId option:selected').text();
                		$('#s2id_CoaCoatypeCodeId .select2-chosen' ).text( type);
					});
				});


				$(\"#CoaCoagroupId\").change(function(){
					var coatype_id = $(\"#CoaCoatypeId\").val(),group;
					var coagroup_id = $(\"#CoaCoagroupId\").val();
					if(coatype_id !='' && coatype_id == 0){
						coatype_id = coatype_id;
					}else{
						coatype_id = 0;
					}
					$(\"#classloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/listcoaclass/'+coatype_id+'/'+coagroup_id, {coatype_id:coatype_id, coagroup_id:coagroup_id},
					function(result) {
						$(\"#CoaCoaclassId\").html(result);
						$('#CoaCoagroupCodeId option[value='+coagroup_id+']').attr('selected', 'selected');
						$(\"#classloading\").html('');
						group = $('#CoaCoagroupCodeId option:selected').text();
						$('#s2id_CoaCoagroupCodeId .select2-chosen' ).text(group);
					});
				});

				$(\"#CoaCoaclassId\").change(function(){
					var coatype_id = $(\"#CoaCoatypeId\").val(),coa_class;
					var coagroup_id = $(\"#CoaCoagroupId\").val();
					var coaclass_id = $(\"#CoaCoaclassId\").val();
					if(coatype_id !='' && coatype_id == 0){
						coatype_id = coatype_id;
					}else{
						coatype_id = 0;
					}
					if(coagroup_id !='' && coagroup_id == 0){
						coagroup_id = coagroup_id;
					}else{
						coagroup_id = 0;
					}
					$(\"#classloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/listcoasubclass/'+coatype_id+'/'+coagroup_id+'/'+coaclass_id, {coatype_id:coatype_id, coagroup_id:coagroup_id, coaclass_id:coaclass_id},
					function(result) {
						$(\"#CoaCoaclasssubclassId\").html(result);
						$('#CoaCoaclassCodeId option[value='+coaclass_id+']').attr('selected', 'selected');
						$(\"#classloading\").html('');
						coa_class = $('#CoaCoaclassCodeId option:selected').text();
						$('#s2id_CoaCoaclassCodeId .select2-chosen' ).text(coa_class);
					});
				});

			$(\"#CoaCoatypeCodeId\").change(function(){
                var coa_type_code_id = $(this).val(),type_code;
                $.post(
                    '".$this->webroot."coas/getCode',
                    {coa_type_code_id:coa_type_code_id},
                    function(result) {
                    	$('#CoaCoatypeId option[value='+result+']').attr('selected', 'selected');
                    	type_code = $('#CoaCoatypeId option:selected').text();
						$('#s2id_CoaCoatypeId .select2-chosen' ).text(type_code);

                    }
                );
                
            });

		
			$(\"#CoaCoagroupCodeId\").change(function(){
				var coa_group_code_id = $(this).val(),group_code;
                $.post(
                    '".$this->webroot."coas/getCode',
                    {coa_group_code_id:coa_group_code_id},
                    function(result) {
                    	$('#CoaCoagroupId option[value='+result+']').attr('selected', 'selected');
                    	group_code = $('#CoaCoagroupId option:selected').text();
						$('#s2id_CoaCoagroupId .select2-chosen' ).text(group_code);

                    }
                );
                
            });
		
			$(\"#CoaCoaclassCodeId\").change(function(){
                var coa_class_code_id = $(this).val(),class_code;
                $.post(
                    '".$this->webroot."coas/getCode',
                    {coa_class_code_id:coa_class_code_id},
                    function(result) {
                    	$('#CoaCoaclassId option[value='+result+']').attr('selected', 'selected');
                    	class_code = $('#CoaCoaclassId option:selected').text();
						$('#s2id_CoaCoaclassId .select2-chosen' ).text(class_code);


                    }
                );
                
            });

			$(\"#CoaCoaclasssubclassCodeId\").change(function(){
				var coa_sub_class_code_id = $(this).val(),class_code;
                $.post(
                    '".$this->webroot."coas/getCode',
                    {coa_sub_class_code_id:coa_sub_class_code_id},
                    function(result) {
                    	$('#CoaCoaclasssubclassId option[value='+result+']').attr('selected', 'selected');
                    	class_code = $('#CoaCoaclasssubclassId option:selected').text();
						$('#s2id_CoaCoaclasssubclassId .select2-chosen' ).text(class_code);

                    }
                );
                
            });

			$(\"#CoaCoaclasssubclassId\").change(function(){
				var coa_sub_class_name_id = $(this).val(),sub_class_code;
                $('#CoaCoaclasssubclassCodeId option[value='+coa_sub_class_name_id+']').attr('selected', 'selected');
                sub_class_code = $('#CoaCoaclasssubclassCodeId option:selected').text();
				$('#s2id_CoaCoaclasssubclassCodeId .select2-chosen' ).text(sub_class_code);
                
            });

		});

		</script>
	";


?>