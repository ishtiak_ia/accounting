<?php
	print_r($userlifecycle);
	echo "<div class=\"col-md-12\">";
		echo $Utilitys->filtertagstart();
	$searchfield="";
echo"
        <div class=\"btn-group\">
            <button class=\"btn btn-warning btn-sm dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-bars\"></i> Export Table Data</button>
            <ul class=\"dropdown-menu \" role=\"menu\">
                 <li><a href=\"#\" onClick =\"$('#userlifecyclesearchloading').tableExport({type:'png',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> <img src='".$this->webroot."js/icons/png.png' width='24px'> PNG</a></li>
                <li><a href=\"#\" onClick =\"$('#userlifecyclesearchloading').tableExport({type:'pdf',pdfFontSize:'7',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> <img src='".$this->webroot."js/icons/pdf.png' width='24px'> PDF</a></li>
            </ul>
        </div>  
    ";
	echo $Utilitys->filtertagend();
	
	echo "
		<div id=\"userlifecyclesearchloading\">
	";
		echo "<div class=\"row\">";
			echo "<div class=\"col-md-12\">";
				echo "<div class=\"row\">";
					echo"<div class=\"col-md-12 text-center\"><h2>".__('Employee Lifecycle Report')."</h2></div>";
				echo"</div>";
				echo "<div class=\"row\">";
					echo "<div class=\"col-md-4\"><b>Emp. / Stuff ID:</b>&nbsp;".
					$userlifecycle[0]["User"]["userpunchmachineid"]."</div>";
					echo"<div class=\"col-md-4\"><b>Name:</b>&nbsp;".
					$userlifecycle[0]["User"]["user_fullname"]."</div>";
					echo"<div class=\"col-md-4\"><b>Wages:</b>&nbsp;".
					$userlifecycle[0]["User"]["salaryamount"]."</div>";
					echo "<div class=\"col-md-4\"><b>Section:</b>&nbsp;".
					$userlifecycle[0]["User"]["department_name"]."</div>";
					echo"<div class=\"col-md-4\"><b>Designation:</b>&nbsp;".
					$userlifecycle[0]["User"]["designation_name"]."</div>";
					echo"<div class=\"col-md-4\"><b>Grade:</b>&nbsp;</div>";
				echo "</div>";
			echo "</div>";
		echo "</div>";
		$dob = strtotime($userlifecycle[0]["User"]["userdob"]);// returns bool(false)
		$EmloyeeBirthDate = date('l, F d, Y', $dob);

		echo "<div class=\"row\">";
			echo "<div class=\"col-md-12\">";
				echo "<div class=\"row\">";
					echo"<div class=\"col-md-12 text-center\"><h4>".__('Employee Personal Information')."</h4></div>";
				echo"</div>";
				echo "<div class=\"row\">";
					echo"<div class=\"col-md-4 pull-right\">";
						echo "<div class=\"row\">";
							echo "<div class=\"col-md-4\">";
								echo $this->html->image(
									'/img/user/small/'.$userlifecycle[0]["User"]["userimage"], 
									array(
										'alt'=> 'Picture of '.$userlifecycle[0]["User"]["user_fullname"], 'width'=>100, 'height'=>100
									)
								);
							echo"</div>";
						echo"</div>";
					echo"</div>";
					echo"<div class=\"col-md-4\">";
						echo "<div class=\"row\">";
							echo "<div class=\"col-md-12\"><b>Father Name:</b>&nbsp;".
							$userlifecycle[0]["User"]["userfathername"]."</div>";
							echo "<div class=\"col-md-12\"><b>Mother Name:</b>&nbsp;".
							$userlifecycle[0]["User"]["usermothername"]."</div>";
							echo "<div class=\"col-md-12\"><b>Present address:</b>&nbsp;".
							$userlifecycle[0]["User"]["userpresentaddress"]."</div>";
							echo "<div class=\"col-md-12\"><b>Permenant Address:</b>&nbsp;".
							$userlifecycle[0]["User"]["userpermanentaddress"]."</div>";
							echo "<div class=\"col-md-12\"><b>Date of Birth:</b>&nbsp;".
							$EmloyeeBirthDate."</div>";
						echo"</div>";
					echo"</div>";
					echo"<div class=\"col-md-4\">";
						echo "<div class=\"row\">";
							echo "<div class=\"col-md-12\"><b>Gender:</b>&nbsp;".
							$userlifecycle[0]["User"]["usergender"]."</div>";
							echo "<div class=\"col-md-12\"><b>Marital Status:</b>&nbsp;".
							$userlifecycle[0]["User"]["usermeritposition"]."</div>";
							echo "<div class=\"col-md-12\"><b>Blood Group:</b>&nbsp;".
							$userlifecycle[0]["User"]["userbloodgroup"]."</div>";
							echo "<div class=\"col-md-12\"><b>Telephone:</b>&nbsp;".
							$userlifecycle[0]["User"]["usertelephone"]."</div>";
							echo "<div class=\"col-md-12\"><b>Email:</b>&nbsp;".
							$userlifecycle[0]["User"]["useremailaddress"]."</div>";
						echo"</div>";
					echo"</div>";
				echo"</div>";

				echo "<div class=\"row\">";
					echo "<div class=\"col-md-12\">";
						echo "<div class=\"row\">";
							echo"<div class=\"col-md-12 text-center\"><h4>".__('Employee Education')."</h4></div>";
						echo"</div>";
						echo $Utilitys->tablestart();
							echo $this->Html->tableHeaders(
								array(
									array(
										__('Degree')  => array('class' => 'highlight sortable')
									),
									array(
										__('Group')  => array('class' => 'highlight sortable')
									),
									array(
										__('Institute')  => array('class' => 'highlight sortable')
									),
									array(
										__('Passing Year')  => array('class' => 'highlight sortable')
									),
									array(
										__('Board')  => array('class' => 'highlight sortable')
									),
									array(
										__('CGP')  => array('class' => 'highlight sortable')
									),
									array(
										__('Durations')  => array('class' => 'highlight sortable')
									)

								)
							);
							echo "".$userlifecycle[0]["User"]["user_education"];
						echo $Utilitys->tableend();
					echo"</div>";
				echo"</div>";

				echo "<div class=\"row\">";
					echo "<div class=\"col-md-12\">";
						echo "<div class=\"row\">";
							echo"<div class=\"col-md-12 text-center\"><h4>".__('Employee Job Circle')."</h4></div>";
						echo"</div>";
						echo $Utilitys->tablestart();
							echo $this->Html->tableHeaders(
								array(
									array(
										__('Joining Date')  => array('class' => 'highlight sortable')
									),
									array(
										__('Salary')  => array('class' => 'highlight sortable')
									),
									array(
										__('Position')  => array('class' => 'highlight sortable')
									)
								)
							);

							$usersalaryrows = array();
							$countUsersalaryrows = 0-1+$this->Paginator->counter('%start%');
							echo "".$userlifecycle[0]["User"]["all_salaryamount"]."".$userlifecycle[0]["User"]["designation_name"];
						echo $Utilitys->tableend();
					echo"</div>";
				echo"</div>";

				echo "<div class=\"row\">";
					echo "<div class=\"col-md-12\">";
						echo "<div class=\"row\">";
							echo"<div class=\"col-md-12 text-center\"><h4>".__('Employee Punishment')."</h4></div>";
						echo"</div>";
						echo $Utilitys->tablestart();
							echo $this->Html->tableHeaders(
								array(
									array(
										__('Title')  => array('class' => 'highlight sortable')
									),
									array(
										__('Description')  => array('class' => 'highlight sortable')
									),
									array(
										__('Action')  => array('class' => 'highlight sortable')
									),
									array(
										__('Date')  => array('class' => 'highlight sortable')
									),
									array(
										__('By')  => array('class' => 'highlight sortable')
									)
								)
							);
							$usergrievancesrows = array();
							$countUsergrievancesrows = 0-1+$this->Paginator->counter('%start%');
							echo @$userlifecycle[0]["User"]["all_grievances"].''.@$userlifecycle[0]["User"]["grievances_by"];
						echo $Utilitys->tableend();
					echo "</div>";
				echo"</div>";

				echo "<div class=\"row\">";
					echo "<div class=\"col-md-12\">";
						echo "<div class=\"row\">";
							echo"<div class=\"text-center\"><h4>".__('Employee Rewards')."</h4></div>";
						echo"</div>";
						echo $Utilitys->tablestart();
							echo $this->Html->tableHeaders(
								array(
									array(
										__('Title')  => array('class' => 'highlight sortable')
									),
									array(
										__('Amount')  => array('class' => 'highlight sortable')
									),
									array(
										__('Description')  => array('class' => 'highlight sortable')
									),
									array(
										__('Date')  => array('class' => 'highlight sortable')
									)
								)
							);
							$rewardrows = array();
							$countRewardrows = 0-1+$this->Paginator->counter('%start%');
							echo "".$userlifecycle[0]["User"]["user_rewards"];
						echo $Utilitys->tableend();
				echo"</div>";
			echo "</div>";
		echo "</div>";
	echo "</div>";

?>
