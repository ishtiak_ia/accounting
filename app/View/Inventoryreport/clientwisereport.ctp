<?php echo $this->Form->create('journalreports', array('default' => false, 'class'=>' ')); ?>
<fieldset>
    <legend><h3>Client Wise Sales Report</h3></legend>
    <table  border="0">
        <table  border="0">
            <tr>
                <td>Client:</td>
                <td>
                    <?php
                    echo $this->Form->input(
                        "Inventory.client_id",
                        array(
                            'type'=>'select',
                            'options'=>array($customer_id),
                            'empty'=>__('Select Code'),
                            'div'=> array('class'=> 'form-group'),
                            'tabindex'=>7,
                            'label'=>'',
                            'class'=> '',
                            'style'=>'width:250px;',
                            'data-validation-engine'=>'validate[required]'
                        )
                    );
                    ?>
                </td>
                <td>&nbsp;</td>
                <td>
                    <?php
                    echo $this->Form->input(
                        "Inventory.client_name",
                        array(
                            'type'=>'select',
                            'options'=>array($customer_list),
                            'empty'=>__('Select Name'),
                            'div'=> array('class'=> 'form-group'),
                            'class'=> '',
                            'label'=>'',
                            'style'=>'width:250px;',
                            'data-validation-engine'=>'validate[required]'
                        )
                    );
                    ?>
                </td>
            </tr>
            <tr>
                <td>Date:</td>
                <td>
                    <?php
                    echo $this->Form->input(
                        "Inventory.fromdate",
                        array(
                            'type' => 'text',
                            'div' => array('class'=> 'form-group'),
                            'label' =>'',
                            'placeholder' => 'From',
                            'class'=> 'form-control',
                            'data-validation-engine'=>'validate[required]',
                            'style' => 'width:250px;'
                        )
                    );
                    ?>
                </td>
                <td>&nbsp;</td>
                <td>
                    <?php
                    echo $this->Form->input(
                        "Inventory.todate",
                        array(
                            'type' => 'text',
                            'div' => array('class'=> 'form-group'),
                            'label' =>'',
                            'placeholder' => 'To',
                            'class'=> 'form-control',
                            'data-validation-engine'=>'validate[required]',
                            'style' => ''
                        )
                    );
                    ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php
                    echo $this->Form->submit('View', array('div' => array('class'=> 'form-group'), 'align' => 'right', 'formnovalidate' => 'true', 'class'=> '', 'id' => 'submit_button', 'onClick' => 'get_inventory();'));
                    ?>
                </td>
            </tr>
        </table>
</fieldset>
<br>
<br>
<?php echo $this->form->end(); ?>

<?php
echo "<div id='print_page' class='right-aligned-texts'>";
echo $Utilitys->printbutton ($printableid="loadinventory", $searchfield=null, $pageformat="c3", $orientation="l", $unit=null, $companyname=$_SESSION["User"]["company_name"], $tabletitle="yes", $tablemonth="yes", $tabledepartment="yes", $printsectoin="printtwotable");
echo "</div>";

?>

<div id="loadinventory">

</div>


<script type="text/javascript">
    $(document).ready(function() {
        var host = "<?php echo $this->webroot; ?>";
        $("#InventoryClientId").select2();
        $("#InventoryClientName").select2();
        $("#print_page").hide();
        $("#InventoryFromdate").datetimepicker({language: "bn",format: "yyyy-mm-dd", use24hours: true, showMeridian: false, startView: 2, minView: 2, autoclose: 1});
        $("#InventoryTodate").datetimepicker({language: "bn",format: "yyyy-mm-dd", use24hours: true, showMeridian: false, startView: 2, minView: 2, autoclose: 1});

        $("#InventoryClientId").change(function(){
            var product_code = $(this).val(),code;
            $('#InventoryClientName option[value='+product_code+']').attr('selected', 'selected');
            code = $('#InventoryClientName option:selected').text();
            $('#s2id_InventoryClientName .select2-chosen').text(code);

        });

        $("#InventoryClientName").change(function(){
            var product_name = $(this).val(),coa;
            $('#InventoryClientId option[value='+product_name+']').attr('selected', 'selected');
            product_name = $('#InventoryClientId option:selected').text();
            $('#s2id_InventoryClientId .select2-chosen').text(product_name);

        });

        $("#submit_button").click(function(){
            var client_id = $("#InventoryClientId").val();
            var from_date = $("#InventoryFromdate").val();
            var to_date = $("#InventoryTodate").val();
            if(client_id !='' && from_date!='' && to_date!=''){
                $("#print_page").hide();
                $("#loadinventory").html("<img  src="+host+ "img/indicator.gif />");
                $.post(
                    host + 'inventoryreports/clientwisereportgenerate',
                    {client_id:client_id,from_date:from_date,to_date:to_date},
                    function(result) {
                        $("#print_page").show();
                        $("#loadinventory").html(result);
                    }
                );

            }else
                alert('Form date and To date can not be empty');
        });

    });
</script>
