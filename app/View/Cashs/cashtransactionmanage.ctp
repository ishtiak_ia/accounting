<?php
	echo "<h2>".__('Cash Transaction')." ". $Utilitys->addurl($title=__("Add Cash Transaction"), $this->request["controller"], $page="cashtransaction")."</h2>";
		echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id=null);
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('Transaction Date')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Particulars')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Withdraw')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Deposits')  => array('class' => 'highlight sortable')
						), 
						__('Action')
					)
				);
				$cashtransactionrows = array();
				$countcashtransaction = 0-1+$this->Paginator->counter('%start%');
				foreach($cashtransaction as $Thiscashtransaction):
					$countcashtransaction++;
					$cashtransactionrows[]= array($countcashtransaction,$Thiscashtransaction["Cashtransaction"]["cashtransaction_date"],$Thiscashtransaction["Cashtransaction"]["cashtransaction_particulars"],array($Thiscashtransaction["Cashtransaction"]["cashtransaction_withdraw"],'colspan="1" align="right"'),array($Thiscashtransaction["Cashtransaction"]["cashtransaction_deposit"],'colspan="1" align="right"'),$Utilitys->cusurl($this->request["controller"], 'cashtransaction', $Thiscashtransaction["Cashtransaction"]["id"], $Thiscashtransaction["Cashtransaction"]["cashtransactionuuid"], $Thiscashtransaction["Cashtransaction"]["transaction_id"]));
				endforeach;
				echo $this->Html->tableCells(
					$cashtransactionrows
				);
			echo $Utilitys->tableend();
		echo $Utilitys->paginationcommon();
?>