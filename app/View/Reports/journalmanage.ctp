<?php
	echo "<h2>".__('Journal')."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Journal.formdate",
			array(
				'type' => 'text',
				'div' => array('class'=> 'form-group'),
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __("Form Date"),
				'class'=> 'form-control',
				'style' => ''
			)
		);
		echo $this->Form->input(
			"Journal.todate",
			array(
				'type' => 'text',
				'div' => array('class'=> 'form-group'),
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __(" To Date"),
				'class'=> 'form-control',
				'style' => ''
			)
		);
		/*echo $this->Form->input(
			"Journal.searchtext",
			array(
				'type' => 'text',
				'div' => array('class'=> 'form-group'),
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
			)
		);*/
	echo $Utilitys->filtertagend();
	echo"
		<div id=\"journalsarchloading\">
	";
		echo $Utilitys->paginationcommon();
		echo $Utilitys->tablestart($class=null, $id=null);
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Date')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Particulars')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Debit')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Credit')  => array('class' => 'highlight sortable')
					)
				)
			);
			$journalrows = array();
			$countJournal = 0-1+$this->Paginator->counter('%start%');
			foreach($gltransaction as $Thisgltransaction):
				$countJournal++;
				$journalrows[]= array($countJournal,$Thisgltransaction["Gltransaction"]["gltransaction_date"],$Thisgltransaction["Gltransaction"]["gltransaction_particulars"],$Thisgltransaction["Gltransaction"]["gltransaction_deposit"],str_replace("-", "", $Thisgltransaction["Gltransaction"]["gltransaction_withdraw"]));
			endforeach;
			echo $this->Html->tableCells(
				$journalrows
			);
		echo $Utilitys->tableend();
		echo $Utilitys->paginationcommon();
	echo"
		</div>
		<script>
			$(document).ready(function() {
				var startDate = new Date('01/01/1980');
				var FromEndDate = new Date();
				var ToEndDate = new Date();
				ToEndDate.setDate(ToEndDate.getDate()+365);

				$('#JournalFormdate').datetimepicker({
					format: \"yyyy-mm-dd\",
					language: 'en',
					weekStart: 1,
					todayBtn: 1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					minView: 2,
					forceParse: 0,
					startDate: '01/01/2012',
					endDate: FromEndDate
				}).on('changeDate', function(selected){
					startDate = new Date(selected.date.valueOf());
					startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
					$('#JournalTodate').datetimepicker('setStartDate', startDate);

					var JournalFormdate = $(this).val();
					var JournalTodate = $('#JournalTodate').val();
					$(\"#journalsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."reports/journalsearchbydate',
						{JournalFormdate:JournalFormdate,JournalTodate:JournalTodate},
						function(result) {
							$(\"#journalsarchloading\").html(result);
						}
					);
				});

				$('#JournalTodate').datetimepicker({
					format: \"yyyy-mm-dd\",
					language: 'en',
					weekStart: 1,
					todayBtn: 1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					minView: 2,
					forceParse: 0,
					startDate: startDate,
					endDate: ToEndDate
				}).on('changeDate', function(selected){
					FromEndDate = new Date(selected.date.valueOf());
					FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
					$('#JournalFormdate').datetimepicker('setEndDate', FromEndDate);

					var JournalTodate = $(this).val();
					var JournalFormdate = $('#JournalFormdate').val();
					$(\"#journalsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."reports/journalsearchbydate',
						{JournalFormdate:JournalFormdate,JournalTodate:JournalTodate},
						function(result) {
							$(\"#journalsarchloading\").html(result);
						}
					);
				});
    				/*$('#JournalFormdate').datetimepicker({
					format: \"yyyy-mm-dd\",
					language: 'en',
					weekStart: 1,
					todayBtn: 1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					minView: 2,
					forceParse: 0
				});
				$('#JournalTodate').datetimepicker({
					format: \"yyyy-mm-dd\",
					language: 'en',
					weekStart: 1,
					startDate: startDate,
					todayBtn: 1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					minView: 2,
					forceParse: 0
				});*/

				$(\"#BankSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#journalsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/banksearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#journalsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>