<?php
App::import('Controller', 'Logins');
App::import('Controller', 'Transactions');
App::import('Controller', 'Users');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');

class JournalsController extends AppController { 
	public $name = 'Journals';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'JqueryValidation');
	public $components = array('RequestHandler','Session');
	public $uses = array('Banktype', 'Bank', 'Bankbranch', 'Bankaccounttype', 'Bankaccount', 'Bankchequebook', 'Banktransaction', 'Company', 'Group', 'User', 'Usergroup', 'Usertransaction', 'Cashtransaction', 'Gltransaction', 'Journaltransaction', 'Coa');

	var $Transactions;
	var $Users;

	var $Logins;
	/*public function index() {
		$this->redirect("login");
	}*/
	function beforeFilter(){
		$this->Logins =& new LoginsController;
		$this->Logins->constructClasses();
		$this->Logins->__validateLoginStatus();
		//$Logins = new LoginsController;
		//$this->set('Logins',$Logins);
		$this->Transactions =& new TransactionsController;
		$this->Transactions->constructClasses();
		$this->Users =& new UsersController;
		$this->Users->constructClasses();


		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	public  function beforeRender(){
		$this->response->disableCache();//prevent useless warnings for Ajax
		if($this->RequestHandler->isAjax()){
			Configure::write('debug', 0);
		}
	}

	//Start Journal Transaction//
	public function journaltransactionmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Journal Transaction').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$gltransaction_id = array();
		$journaltransaction_id = array();
		$bankaccount_id = array();
		if($group_id==1):
		elseif($group_id==2):
			$gltransaction_id[] = array('Gltransaction.company_id'=>$company_id);
			$journaltransaction_id[] = array('Journaltransaction.company_id'=>$company_id);
			$journaltransaction_id[] = array('Journaltransaction.journaltransactionisactive IN (0,1)');
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$gltransaction_id[] = array('Gltransaction.gltransactionisactive IN (0,1)');
		else:
			$gltransaction_id[] = array('Gltransaction.company_id'=>$company_id);
			$gltransaction_id[] = array('Gltransaction.branch_id'=>$branch_id);
			$gltransaction_id[] = array('Gltransaction.gltransactionisactive IN (1)');
			$journaltransaction_id[] = array('Journaltransaction.company_id'=>$company_id);
			$journaltransaction_id[] = array('Journaltransaction.branch_id'=>$branch_id);
			$journaltransaction_id[] = array('Journaltransaction.journaltransactionisactive IN (1)');
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.branch_id'=>$branch_id);
		endif;

		$bank=$this->Bank->find('list',array("fields" => array("id","bank_name")));
		$this->set('bank',$bank);

		$bankaccount=$this->Bankaccount->find(
			'list',
			array(
				"fields" => array("id","bankaccount_name"),
				"conditions" => $bankaccount_id
			)
		);
		$this->set('bankaccount',$bankaccount);

			$gltransaction_id[] = array('Gltransaction.gltransactionisactive'=>1);
			$journaltransaction_id[] = array('Journaltransaction.journaltransactionisactive'=>1);
		$this->paginate = array(
			'fields' => 'Journaltransaction.*',
			'conditions'=>array_merge($journaltransaction_id),
			'order'  =>'Journaltransaction.transactiondate DESC',
			'limit' => 20
		);
		$journaltransaction = $this->paginate('Journaltransaction');
		$this->set('journaltransaction',$journaltransaction);
	}
	public function journaltransactioninsertupdate($id=null,$uuid=null) {
		$this->layout='default';
		$this->set('title_for_layout', __('Journal Transaction').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$banktransaction_id = array();
		$bank_id = array();
		$bankaccount_id = array();
		$bankchequebook_id = array();
		$group_array = array();
		$user_array = array();
		$coa_array = array();
		if($group_id==1):
			$coa_array[] = array('NOT'=>array('Coa.id'=>array(1,2)));
		elseif($group_id==2):
			$banktransaction_id[] = array('Banktransaction.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankchequebook_id[] = array('Bankchequebook.company_id'=>$company_id);
			$bank_id[] = array('Bankaccount.company_id'=>$company_id);
			$group_array[] = array('NOT'=>array('Group.id'=>1));
			$user_array[] = array('NOT'=>array('User.group_id'=>1));
			$user_array[] = array('User.company_id'=>$company_id);
			$coa_array[] = array('NOT'=>array('Coa.coaisactive'=>array(2)));
			$coa_array[] = array('NOT'=>array('Coa.id'=>array(1,2)));
			$coa_array[] = array('Coa.Company_id'=>array(1,$company_id));
		else:
			$banktransaction_id[] = array('Banktransaction.company_id'=>$company_id);
			$banktransaction_id[] = array('Banktransaction.branch_id'=>$branch_id);
			$bank_id[] = array('Bankaccount.company_id'=>$company_id);
			$bank_id[] = array('Bankaccount.branch_id'=>$branch_id);
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.branch_id'=>$branch_id);
			$bankchequebook_id[] = array('Bankchequebook.company_id'=>$company_id);
			$bankchequebook_id[] = array('Bankchequebook.branch_id'=>$branch_id);
			$group_array[] = array('NOT'=>array('Group.id'=>array(1, 2)));
			$user_array[] = array('NOT'=>array('User.group_id'=>array(1, 2)));
			$user_array[] = array('User.company_id'=>$company_id);
			$user_array[] = array('User.branch_id'=>$branch_id);
			$coa_array[] = array('NOT'=>array('Coa.coaisactive'=>array(2)));

			$coa_array[] = array('Coa.Company_id'=>array(1,$company_id));

			//$coa_array[] = array('IN'=>array('Coa.branch_id'=>array(1,$branch_id)));
		endif;

		$bank=$this->Bankaccount->find('list',array("fields" => array("bank_id","bank_name"),"conditions" => $bank_id, 'recursive'=>1));
		$this->set('bank',$bank);

		$group=$this->Group->find(
			'list',
			array(
				"fields" => array("id","groupname"),
				"conditions" => $group_array
			)
		);
		$this->set('group',$group);

		$user=$this->User->find(
			'list',
			array(
				"fields" => array("id","user_fullname"),
				"conditions" => $user_array
			)
		);
		$this->set('user',$user);

		$bankaccount=$this->Bankaccount->find(
			'list',
			array(
				"fields" => array("id","bankaccount_name"),
				"conditions" => $bankaccount_id
			)
		);
		$this->set('bankaccount',$bankaccount);

		$bankchequebook=$this->Bankchequebook->find(
			'list',
			array(
				"fields" => array("id","bankchequebook_name"),
				"conditions" => $bankchequebook_id
			)
		);
		$this->set('bankchequebook',$bankchequebook);
		$coa=$this->Coa->find(
			'list',
			array(
				"fields" => array("id","coa_name"),
				"conditions" => $coa_array
			)
		);
		$this->set('coa',$coa);
		/*if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
			$banktransaction=$this->Banktransaction->find('all',array("fields" => "Banktransaction.*,User.*","conditions"=>array('Banktransaction.id'=>$id,'Banktransaction.banktransactionuuid'=>$uuid)));
			$this->set('banktransaction',$banktransaction);
			$this->set('id',$id);
		endif;*/
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
			$journaltransaction=$this->Journaltransaction->find('all',array("fields" => "Journaltransaction.*,User.*","conditions"=>array('Journaltransaction.id'=>$id,'Journaltransaction.journaltransactionuuid'=>$uuid)));
			$this->set('journaltransaction',$journaltransaction);
			$this->set('id',$id);
		}
	}
	public function journaltransactioninsertupdateaction() {
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_ID=$_SESSION["User"]["group_id"];
		$user_ID=$_SESSION["User"]["id"];

		$id = @$this->request->data['Journals']['id'];
		$uuid = @$this->request->data['Journals']['gltransactionuuid'];
		$transaction_id = @$this->request->data['Journals']['id']?$this->request->data['Journals']['id']:0;

		$bank_id = @$this->request->data['Gltransaction']['bank_id'] ? $this->request->data['Gltransaction']['bank_id'] : 0;
		$this->request->data['Journaltransaction']['bank_id']=$bank_id;
		$bankbranch_id = @$this->request->data['Gltransaction']['bankbranch_id'] ? $this->request->data['Gltransaction']['bankbranch_id'] : 0;
		$this->request->data['Journaltransaction']['bankbranch_id']=$bankbranch_id;
		$bankaccounttype_id = @$this->request->data['Gltransaction']['bankaccounttype_id'] ? $this->request->data['Gltransaction']['bankaccounttype_id'] : 0;
		$this->request->data['Journaltransaction']['bankaccounttype_id']=$bankaccounttype_id;
		$bankaccount_id = @$this->request->data['Gltransaction']['bankaccount_id'] ? $this->request->data['Gltransaction']['bankaccount_id'] : 0;
		$this->request->data['Journaltransaction']['bankaccount_id']=$bankaccount_id;
		$journaltransactiontype_id = $this->request->data['Gltransaction']['transactiontype_id'];
		$this->request->data['Journaltransaction']['journaltransactiontype_id']=$journaltransactiontype_id;
		$transactiontype_id = $_SESSION['ST_JOURNAL'];
		$this->request->data['Journaltransaction']['transactiontype_id']=$transactiontype_id;
		//$transaction_id = @$this->request->data['Gls']['id']?$this->request->data['Gls']['id']:0;
		$this->request->data['Journaltransaction']['transaction_id'] = 0;
		$coa_id = $this->request->data['Gltransaction']['coa_id'];
		$this->request->data['Journaltransaction']['coa_id']=$coa_id;
		$this->request->data['Journaltransaction']['company_id'] = $company_id;
		$this->request->data['Journaltransaction']['branch_id'] = $branch_id;
		$user_id = @$this->request->data['Gltransaction']['user_id'] ? $this->request->data['Gltransaction']['user_id'] : 0;
		$this->request->data['Journaltransaction']['user_id'] = $user_id;
		$group_id = @$this->request->data['Gltransaction']['group_id'] ? $this->request->data['Gltransaction']['group_id'] : 0;
		$this->request->data['Journaltransaction']['group_id'] = $group_id;
		$salesman_id = @$this->request->data['Gltransaction']['salesman_id'] ? $this->request->data['Gltransaction']['salesman_id'] : 0;
		$this->request->data['Journaltransaction']['salesman_id'] = $salesman_id;
		$bankchequebook_id = @$this->request->data['Gltransaction']['banktransactionchequebookseries'] ? $this->request->data['Gltransaction']['banktransactionchequebookseries'] : 0;
		$this->request->data['Journaltransaction']['bankchequebook_id'] = $bankchequebook_id;
		$banktransactionchequebookseries = @$this->request->data['Gltransaction']['banktransactionchequebookseries']?$this->request->data['Gltransaction']['banktransactionchequebookseries']:0;
		if(isset($this->request->data['Gltransaction']['banktransactionchequesequencynumber'])){
			$banktransactionchequesequencynumber = explode("-", $this->request->data['Gltransaction']['banktransactionchequesequencynumber']);
			$chequebookseries = $banktransactionchequesequencynumber[0];
			$chequesequencynumber =$banktransactionchequesequencynumber[1];
		} else {
			$chequebookseries = 0;
			$chequesequencynumber = 0;
		}
		$this->request->data['Journaltransaction']['journaltransactionchequebookseries'] = $chequebookseries;
		$this->request->data['Journaltransaction']['journaltransactionchequesequencynumber'] = $chequesequencynumber;
		$transactiondate = $this->request->data['Gltransaction']['transactiondate'];
		$this->request->data['Journaltransaction']['transactiondate'] = $transactiondate;
		if(($journaltransactiontype_id == $_SESSION["ST_CASHPAYMENT"]) || ($journaltransactiontype_id == $_SESSION["ST_BANKPAYMENT"])):
			$transactionamount = -$this->request->data['Gltransaction']['transactionamount'];
		else:
			$transactionamount = $this->request->data['Gltransaction']['transactionamount'];
		endif;
		$this->request->data['Journaltransaction']['transactionamount'] = $transactionamount;
		if(isset($this->request->data['Gltransaction']['gltransactionothernote'])){
			$journaltransactionothernote = 	$this->request->data['Gltransaction']['gltransactionothernote'];
		} else {
			$journaltransactionothernote = 0;
		}
		$this->request->data['Journaltransaction']['journaltransactionothernote'] = $journaltransactionothernote;
		$this->request->data['Journaltransaction']['journaltransactionisactive'] = 1;
		$group_id = $this->request->data['Gltransaction']['group_id'];
		
		$product_id = @$this->request->data['Gltransaction']['product_id']?$this->request->data['Gltransaction']['product_id']:0;
		
		//pr($_SESSION);
		//pr($_POST);
		//pr($this->request);
		//exit();
		if ($this->request->is('post')){
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
				$this->request->data['Journaltransaction']['journaltransactionupdateid'] = $user_ID;
				$this->request->data['Journaltransaction']['journaltransactionupdatedate'] = date('Y-m-d H:i:s');
				$message = __('Update Successfully.');
				$this->Journaltransaction->id = $id;
			} else {
				$this->request->data['Journaltransaction']['journaltransactioninsertid'] = $user_ID;
				$this->request->data['Journaltransaction']['journaltransactioninsertdate'] = date('Y-m-d H:i:s');
				$this->request->data['Journaltransaction']['journaltransactionuuid']=String::uuid();
				$message = __('Save Successfully.');
			}
			if($this->Journaltransaction->save($this->request->data, array('validate' => 'only'))){
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				$transaction_id = $this->Journaltransaction->getLastInsertId() ? $this->Journaltransaction->getLastInsertId() : $id;

				$clientsuppler_id = @$this->request->data['Gltransaction']['user_id'] ? $this->request->data['Gltransaction']['user_id'] : 0;
				$clientsupplergroup_id = $this->request->data['Gltransaction']['group_id'];
				
				
				$banktransactiondate = $this->request->data['Gltransaction']['transactiondate'];
				$this->Gltransaction->deleteAll(array('Gltransaction.transaction_id' => $transaction_id, 'Gltransaction.transactiontype_id' => $transactiontype_id));
				$this->Transactions->usertransaction($transaction_id, $transactiontype_id, $transactionamount, $coa_id, $clientsuppler_id, $clientsupplergroup_id, $transactiondate, $isActive=1, 1);
				$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $transactionamount, $coa_id, $clientsuppler_id, $group_id, $product_id=0, $transactiondate, $isActive=1, 1);
				
				if(($journaltransactiontype_id == $_SESSION["ST_BANKPAYMENT"]) || ($journaltransactiontype_id == $_SESSION["ST_BANKDEPOSIT"])){
					$bank_cash_tansaction_id = $this->request->data['Gltransaction']['transactiontype_id'];
					$this->Cashtransaction->deleteAll(array('Cashtransaction.transaction_id' => $transaction_id, 'Cashtransaction.transactiontype_id' => $transactiontype_id));
					$this->Transactions->banktransaction($bank_id, $bankbranch_id, $bankaccounttype_id, $bankaccount_id, $bankchequebook_id, $chequebookseries, $chequesequencynumber, $transactiondate, $banktransactionothernote=0, $transaction_id, $transactiontype_id, $transactionamount, 1, $clientsuppler_id, $group_id, $salesman_id, $product_id, $bank_cash_tansaction_id, $isActive=1);
					$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $transactionamount, 1, $clientsuppler_id, $group_id, $product_id=0, $transactiondate, $isActive=1, 1);
				} else {
					$this->Banktransaction->deleteAll(array('Banktransaction.transaction_id' => $transaction_id, 'Banktransaction.transactiontype_id' => $transactiontype_id));
					$this->Transactions->cashtransaction($transaction_id, $transactiontype_id, $transactionamount, 2, $clientsuppler_id, $group_id, $product_id, $transactiondate, $isActive=1);
					$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $transactionamount, 2, $clientsuppler_id, $group_id, $product_id=0, $transactiondate, $isActive=1, 1);
				}
				$this->redirect("/journals/journaltransactionmanage");
			} else {
				$errors = $this->Journaltransaction->invalidFields();
				$this->set('alert alert-danger', $this->Journaltransaction->invalidFields());
				$this->redirect("/journals/journaltransactioninsertupdate");
			}
			pr($this->request);
		} else {
			$errors = $this->Journaltransaction->invalidFields();
			$this->set('alert alert-danger', $this->Journaltransaction->invalidFields());
			$this->redirect("/journals/journaltransactioninsertupdate");
		}
	}

	//End Journal Transaction//
}