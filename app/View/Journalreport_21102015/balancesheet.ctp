


<?php echo $this->Form->create('journalreports', array('default' => false, 'class'=>' ')); ?>
<fieldset>  
    <legend><h3>Balance Sheet</h3></legend>
    <table  border="0">
        <table  border="0">
       	 <tr>
            <td>Date:</td>
            <td>
            	<?php
            		echo $this->Form->input(
						"Generalledger.fromdate",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' =>'',
							'placeholder' => 'From',
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style' => 'width:250px;'
						)
					);
				?>
			 </td>
			 <td>&nbsp;</td>
			 <td>
			 <?php
      			echo $this->Form->input(
						"Generalledger.todate",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' =>'',
							'placeholder' => 'To',
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style' => 'width:250px;'
						)
					);
			?>
		    </td>
        </tr>
        <tr>
        	<td>
        		 <?php 
                	echo $this->Form->submit('View', array('div' => array('class'=> 'form-group'), 'align' => 'right', 'formnovalidate' => 'true', 'class'=> '', 'onClick' => 'get_trial_balance();')); 
                ?>
            </td>
        </tr>
   </table>
</fieldset> 
<br>
<br>
<?php echo $this->form->end(); ?>

<?php
	echo "<div id='print_page' class='right-aligned-texts'>";
    echo $Utilitys->printbutton ($printableid="load_balance_sheet", $searchfield=null, $pageformat="c3", $orientation="l", $unit=null, $companyname=$_SESSION["User"]["company_name"], $tabletitle="yes", $tablemonth="yes", $tabledepartment="yes", $printsectoin="printtwotable");
    echo "</div>";

?>

<div id="load_balance_sheet">
  
</div>

<?php
echo"
		<script>
		 function get_trial_balance(){	
		 	var from_date = $(\"#GeneralledgerFromdate\").val();
			var to_date = $(\"#GeneralledgerTodate\").val();
			if(from_date!='' && to_date!=''){
				$(\"#print_page\").hide();
				$(\"#load_balance_sheet\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.post(
					'".$this->webroot."journalreports/balancesheetgenerate',
					{from_date:from_date,to_date:to_date},
					function(result) {
						$(\"#print_page\").show();
						$(\"#load_balance_sheet\").html(result);
					}
				);

			}else
				alert('Form date and To date can not be empty');
		}

		</script>
	";
	
?>

<script type="text/javascript">
$(document).ready(function() {
	$("#print_page").hide();
  	$("#GeneralledgerFromdate").datetimepicker({language: "bn",format: "yyyy-mm-dd", use24hours: true, showMeridian: false, startView: 2, minView: 2, autoclose: 1});	

  	$("#GeneralledgerTodate").datetimepicker({language: "bn",format: "yyyy-mm-dd", use24hours: true, showMeridian: false, startView: 2, minView: 2, autoclose: 1});

});
</script>
