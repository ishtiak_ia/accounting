
<?php
	if($GroupId==4 || $GroupId==7){
		echo $this->Form->input(
					"User.salespersonid",
					array(
						'type' => 'select', 
		            	"options" => $salesperson,
		            	'empty' => 'Select Salesperson', 
		            	'div' => false, 
		            	'label' => 'Select Salesperson',
		            	"optgroup label" => false, 
		            	'data-validation-engine'=>'validate[required]',
		            	'style' => '',
		            	'selected'=> @$salespersonid,
		            	'class'=> 'form-control'
					)
		);
	} else {
		echo $this->Form->input('User.salespersonid', array('type'=>'hidden', 'value'=>0, 'class'=>''));
	}
		
?>