<?php 
	echo"
		<table>
			<tr>
				<td>
					".__('Add Chart of Account Group information')."
				</td>
			</tr>
			<tr>
				<td>
	";
	echo $this->Form->create('Coas', array('action' => 'coagroupinsertupdateaction'));
		echo $this->Form->inpute('id', array('type'=>'hidden', 'value'=>@$coagroup[0]['Coagroup']['id']));
		echo $this->Form->inpute('coagroupuuid', array('type'=>'hidden', 'value'=>@$coagroup[0]['Coagroup']['coagroupuuid']));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$coagroup[0]['Coagroup']['coagroupisactive'] ? $coagroup[0]['Coagroup']['coagroupisactive'] : 0), 
			'class' => 'radio inline'
		);
	echo"
					<table>
						<tr>
							<td>".__("Type")."</td>
							<td>
								".$this->Form->input(
									"Coagroup.coatype_code",array(
										'type'=>'select',
										"options"=>array($coatype_code),
										'empty'=>__('Select Code'),
											'div'=>false,
											'tabindex'=>1,
											'label'=>false,
											'style'=>'',
											'data-validation-engine'=>'validate[required]',
											'selected'=> (@$coagroup[0]['Coagroup']['coatype_id'] ? $coagroup[0]['Coagroup']['coatype_id'] : 0)
										)
									)."
							</td>
							<td>
								".$this->Form->input(
									"Coagroup.coatype",array(
										'type'=>'select',
										"options"=>array($coatype),
										'empty'=>__('Select Chart of Account Type'),
											'div'=>false,
											'tabindex'=>1,
											'label'=>false,
											'style'=>'',
											'data-validation-engine'=>'validate[required]',
											'selected'=> (@$coagroup[0]['Coagroup']['coatype_id'] ? $coagroup[0]['Coagroup']['coatype_id'] : 0)
										)
									)."
							</td>
							
						</tr>
						<tr>
							<td>".__("CODE")."</td>
							<td>
								".$this->Form->input(
									"Coagroup.coagroupcode",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'data-validation-engine'=>'validate[required]',
										'value' =>@$coagroup[0]['Coagroup']['coagroupcode']
									)
								)."
							</td>
						
							<td>
								".$this->Form->input(
									"Coagroup.coagroupname",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'data-validation-engine'=>'validate[required]',
										'value' =>@$coagroup[0]['Coagroup']['coagroupname']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("Group BN")."</td>
							<td>
								".$this->Form->input(
									"Coagroup.coagroupnamebn",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'data-validation-engine'=>'validate[required]',
										'value' => @$coagroup[0]['Coagroup']['coagroupnamebn']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("isActive")."</td>
							<td>
								".$this->Form->radio(
									'Coagroup.coagroupisactive', 
									$options, 
									$attributes
								)."
							</td>
						</tr>
						<tr>
							<td colspan=\"2\">
								".$Utilitys->allformbutton('',$this->request["controller"],$page='coagroup')."
							</td>
						</tr>
					</table>
	";
	echo $this->Form->end();
	echo"
				</td>
			</tr>
		</table>
		<script>
		$(document).ready(function() {
			$(\"#CoagroupCoatypeCode\").select2();
			$(\"#CoagroupCoatype\").select2();
			$(\"#CoasCoagroupinsertupdateactionForm\").validationEngine();
			$(\"#CoagroupCoatypeCode\").change(function(){
                var coa_type_code_id = $(this).val(),coa;
                $.post(
                    '".$this->webroot."coas/getCode',
                    {coa_type_code_id:coa_type_code_id},
                    function(result) {
                    	$('#CoagroupCoatype option[value='+result+']').attr('selected', 'selected');
                    	coa = $('#CoagroupCoatype option:selected').text();
                    	$( '#s2id_CoagroupCoatype .select2-chosen' ).text( coa );

                    }
                );
                
            });

			$(\"#CoagroupCoatype\").change(function(){
				var coa_type_name_id = $(this).val();
				$.post(
                    '".$this->webroot."coas/getCode',
                    {coa_type_name_id:coa_type_name_id},
                    function(result) {
                    	$('#CoagroupCoatypeCode option[value='+result+']').attr('selected', 'selected');
                    	coa = $('#CoagroupCoatypeCode option:selected').text();
                    	$('#s2id_CoagroupCoatypeCode .select2-chosen').text(coa);

                    }
                );
                
            });

		});
		</script>
	";
?>
 

