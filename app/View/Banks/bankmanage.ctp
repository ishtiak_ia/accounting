<?php
	echo "<h2>".__('Bank'). $Utilitys->addurl($title=__("Add New Bank"), $this->request["controller"], $page="bank")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Bank.searchtext",
			array(
				'type' => 'text',
				'div' => array('class'=> 'form-group'),
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
			)
		);
		$Utilitys->printbutton ($printableid='banksarchloading', $searchfield=null);
	echo $Utilitys->filtertagend();
	echo"
		<div id=\"banksarchloading\">
	";
		echo $Utilitys->paginationcommon();
		echo $Utilitys->tablestart($class=null, $id='printtable');
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Bank Type')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Bank')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Bank BN')  => array('class' => 'highlight sortable')
					), 
					__('Active'), 
					__('Action')
				)
			);
			$bankrows = array();
			$countBank = 0-1+$this->Paginator->counter('%start%');
			foreach($bank as $Thisbank):
				$countBank++;
				$bankrows[]= array($countBank,$Thisbank["Bank"]["banktype_name"],$Thisbank["Bank"]["bankname"],$Thisbank["Bank"]["banknamebn"],$Utilitys->statusURL($this->request["controller"], 'bank', $Thisbank["Bank"]["id"], $Thisbank["Bank"]["bankuuid"], $Thisbank["Bank"]["bankisactive"]),$Utilitys->cusurl($this->request["controller"], 'bank', $Thisbank["Bank"]["id"], $Thisbank["Bank"]["bankuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$bankrows
			);
		echo $Utilitys->tableend();
		echo $Utilitys->paginationcommon();
	echo"
		</div>
		<script>
			$(document).ready(function() {
				$(\"#BankSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#banksarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/banksearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#banksarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>
