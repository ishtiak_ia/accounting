<?php
	echo "<h2>".__('Employee Relationship'). $Utilitys->addurl($title=__("Add New Employee Relationship"), $this->request['controller'], $page="userrelationship")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Userrelationship.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"userrelationshipsearchloading\">
	";
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Relationship Name')  => array('class' => 'highlight sortable')
					),
					__('Active'), 
					__('Action')
				)
			);
			$Userrelationshiptrows = array();
			$countUserrelationshiptrows = 0-1+$this->Paginator->counter('%start%');
			foreach($userrelationship as $Thisuserrelationship):
				$countUserrelationshiptrows++;
				$Userrelationshiptrows[]= array($countUserrelationshiptrows,$Thisuserrelationship["Userrelationship"]["userrelationshipname"],$Utilitys->statusURL($this->request["controller"], 'userrelationship', $Thisuserrelationship["Userrelationship"]["id"], $Thisuserrelationship["Userrelationship"]["userrelationshipuuid"], $Thisuserrelationship["Userrelationship"]["userrelationshipisactive"]),$Utilitys->cusurl($this->request["controller"], 'userrelationship', $Thisuserrelationship["Userrelationship"]["id"], $Thisuserrelationship["Userrelationship"]["userrelationshipuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$Userrelationshiptrows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();

	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				$(\"#UserrelationshipSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#userrelationshipsearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/userrelationshipsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#userrelationshipsearchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>