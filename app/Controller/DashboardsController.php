<?php
App::import('Controller', 'Logins');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');

$Logins = new LoginsController;
class DashboardsController extends AppController {
	public $name = 'Dashboards';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session');
	public $components = array('RequestHandler','Session');
	public $uses = array('User', 'Usernarration', 'Group', 'Groupnarration', 'Menu', 'Menunarration', 'Menuprivilege', 'Company', 'Branch', 'Usertransaction', 'Department', 'tblemployeeinouttime', 'Reward', 'Grievance', 'Userachievement');

	var $Logins;
	
	function beforeFilter(){
		$this->Logins =& new LoginsController;
		$this->Logins->constructClasses();
		$this->Logins->__validateLoginStatus();
		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}	
	}

	public  function beforeRender(){
		$this->response->disableCache();//prevent useless warnings for Ajax
		if($this->RequestHandler->isAjax()){
			Configure::write('debug', 0);
		}
	}

	public function index() {
		$this->layout='default';
		$this->set('title_for_layout', __('Dashboard').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$user_id = array();
		$groupId = array();
		$companyId = array();
		$branchId = array();

		if($group_id==1):
		elseif($group_id==2):
//			$companyId[] =array('User.company_id'=>$company_id);
			$companyId[] =array('Company.id'=>$company_id);
		else:
			$companyId[] = array('User.company_id'=>$company_id);
			$companyId[] = array('User.branch_id'=>$branch_id);
		endif;


		$employeeCountbycompany = $this->User->find('all',array(
			"fields" => array("COUNT(User.id) AS Totalemployee", "User.company_id", "User.department_id", "company_name"),
			//"conditions" => array($companyId)
			"group"	=>	"User.company_id"
			)
		);
		$this->set('employeeCountbycompany', $employeeCountbycompany);

		$employeeCountbybranch = $this->User->find('all',array(
			"fields" => array("COUNT(User.id) AS Totalemployee", "User.company_id", "User.department_id", "branch_name"),
			//"conditions" => array($companyId)
			"group"	=>	"User.branch_id"
			)
		);
		$this->set('employeeCountbybranch', $employeeCountbybranch);

		$employeeCountbydepartment = $this->User->find('all',array(
			"fields" => array("COUNT(User.id) AS Totalemployee", "User.company_id", "User.department_id", "department_name"),
			//"conditions" => array($companyId)
			"group"	=>	"User.department_id"
			)
		);
		$this->set('employeeCountbydepartment', $employeeCountbydepartment);

		// $otCountbycompany = $this->Tbljobcardsummery->find('all',array(
		// 	"fields" => array("SUM(Tbljobcardsummery.TotalOverTimeMinutes) AS Totalot","company_name"),
		// 	//"conditions" => array($companyId)
		// 	"group"	=>	"Tbljobcardsummery.company_id"
		// 	)
		// );
		// $this->set('otCountbycompany', $otCountbycompany);

		// $otCountbybranch = $this->Tbljobcardsummery->find('all',array(
		// 	"fields" => array("SUM(Tbljobcardsummery.TotalOverTimeMinutes) AS Totalot","branch_name"),
		// 	//"conditions" => array($companyId)
		// 	"group"	=>	"Tbljobcardsummery.branch_id"
		// 	)
		// );
		// $this->set('otCountbybranch', $otCountbybranch);

		// $otCountbydepartment = $this->Tbljobcardsummery->find('all',array(
		// 	"fields" => array("SUM(Tbljobcardsummery.TotalOverTimeMinutes) AS Totalot", "Department"),
		// 	//"conditions" => array($companyId)
		// 	"group"	=>	"Tbljobcardsummery.DepartmentID"
		// 	)
		// );
		// $this->set('otCountbydepartment', $otCountbydepartment);

	}
	public function dashboarddetailsbyid ($id=null,$uuid=null){
		$Today=date("Y-m-d");
		echo $ThisMonth=date("m");
		echo $ThisYear=date("Y");
		$DisplayLayout= @$this->request->data['DisplayLayout']?$this->request->data['DisplayLayout']:'iframe';
		$this->layout=$DisplayLayout;

		$department = $this->Department->find('first', array(
			"fields" => "Department.*",
			"conditions" => array('Department.id' => $id)
			)
		);
		$this->set('department', $department);
		
		$count_departmentuser = $this->User->find('count',array(
			"fields" => array("User.department_id"),
			'conditions' => array('User.department_id' => $id)
			));
		$this->set('count_departmentuser', $count_departmentuser);
		$this->set('id', $id);
		$this->set('DisplayLayout', $DisplayLayout);
		
		$count_departmentuserpresent = $this->tblemployeeinouttime->find('count',array(
			"fields" => array("tblemployeeinouttime.DepartmentID", "tblemployeeinouttime.INTIME"),
			'conditions' => array('tblemployeeinouttime.DepartmentID' => $id, 'DATE(tblemployeeinouttime.INTIME)' => $Today)
			));
		$this->set('count_departmentuserpresent', $count_departmentuserpresent);
		
		$count_rewards = $this->Reward->find('count',array(
			"fields" => array("Reward.department_id", "Reward.rewarddate"),
			'conditions' => array('Reward.department_id' => $id, 'MONTH(Reward.rewarddate)' => $ThisMonth)
			));
		$this->set('count_rewards', $count_rewards);
	
		$count_grievances = $this->Grievance->find('count',array(
			"fields" => array("Grievance.department_id", "Grievance.grievancedate"),
			'conditions' => array('Grievance.department_id' => $id, 'MONTH(Grievance.grievancedate)' => $ThisMonth)
			));
		$this->set('count_grievances', $count_grievances);
	
		$count_userachievements = $this->Userachievement->find('count',array(
			"fields" => array("Userachievement.department_id", "Userachievement.userachievementdate"),
			'conditions' => array('Userachievement.department_id' => $id, 'MONTH(Userachievement.userachievementdate)' => $ThisMonth)
			));
		$this->set('count_userachievements', $count_userachievements);

    }

}
