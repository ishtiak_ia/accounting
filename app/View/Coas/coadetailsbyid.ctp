<?php
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"2\">
				<h2>".__('Add Chart of Account information')."</h2>
			</td>
		</tr>
		<tr>
			<td>".__("Company")."</td>
			<td>
				".@$coa[0]['Coa']['company_name']."
			</td>
		</tr>
		<tr>
			<td>".__("Branch")."</td>
			<td>
				".@$coa[0]['Coa']['branch_name']."
			</td>
		</tr>
		<tr>
			<td>".__("Type")."</td>
			<td>
				".@$coa[0]['Coa']['coatype_name']."
			</td>
		</tr>
		<tr>
			<td>".__("Group")."</td>
			<td>
				".@$coa[0]['Coa']['coagroup_name']."
			</td>
		</tr>
		<tr>
			<td>".__("Class")."</td>
			<td>
				".@$coa[0]['Coa']['coaclass_name']."
			</td>
		</tr>
		<tr>
			<td>".__("Sub Class")."</td>
			<td>
				".@$coa[0]['Coa']['coaclasssubclass_name']."
			</td>
		</tr>
		<tr>
			<td>".__("COA")."</td>
			<td>
				".@$coa[0]['Coa']['coaname']."
			</td>
		</tr>
		<tr>
			<td>".__("COA BN")."</td>
			<td>
				".@$coa[0]['Coa']['coanamebn']."
			</td>
		</tr>
		<tr>
			<td>".__("isActive")."</td>
			<td>
				".@$coa[0]['Coa']['isActive']."
			</td>
		</tr>
	";
	echo $Utilitys->tableend();

?>