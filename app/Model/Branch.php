<?php
App::uses('AppModel', 'Model');
class Branch extends AppModel {
	public $name = 'Branch';
	public $usetables = 'branches';
	var $belongsTo = array(
		'Company' => array(
			'fields' =>array('companyname','companynamebn'),
			'className'    => 'Company',
			'foreignKey'    => 'company_id'
		),
		'Branchemail' => array(
			'fields' =>array('branchemail'),
			'className'    => 'Branchemail',
			'foreignKey'    => false,
			'conditions'=>array('Branchemail.branch_id'=>'Branch.id')
		),
		'Branchphone' => array(
			'fields' =>array('Branchphoneno'),
			'className'    => 'Branchphone',
			'foreignKey'    => false,
			'conditions'=>array('Branchphone.branch_id'=>'Branch.id')
		)
	);
	var $virtualFields = array(
		'branch_name' => 'CONCAT(Branch.branchname, " / ", Branch.branchnamebn)',
		'company_name' => 'CONCAT(Company.companyname, " / ", Company.companynamebn)',
		'branch_email' => '(SELECT GROUP_CONCAT(Branchemail.branchemailaddress) FROM branchemails as Branchemail WHERE Branch.id=Branchemail.branch_id	)',
		'branch_phone' => '(SELECT GROUP_CONCAT(Branchphone.branchphoneno) FROM  branchphones as Branchphone WHERE Branch.id=Branchphone.branch_id	)',
		'isActive' => 'IF(Branch.branchisactive = 0, "<span class=\"button btn btn-sm btn-warning\"><span class=\"glyphicon glyphicon-remove-circle\" title=\"Inactive\"></span></span>", IF(Branch.branchisactive = 1, "<span class=\"button btn btn-sm btn-success\"><span class=\"glyphicon glyphicon-ok-circle\" title=\"Active\"></span></span>", "<span class=\"button btn btn-sm btn-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span></span>"))'
	);
	// var $hasMany = array(
	// 'Branchemail' => array(
	// 		'fields' =>array('branchemail'),
	// 		'className'    => 'Branchemail',
	// 		'foreignKey'    => 'branch_id'
	// 	)
	// );
	
}

?>