<?php
App::uses('AppModel', 'Model');
class Bankaccounttype extends AppModel {
	public $name = 'Bankaccounttype';
	public $usetables = 'bankaccounttypes';
	var $belongsTo  = array(
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'bankaccounttypeinsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'bankaccounttypeupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'bankaccounttypedeleteid'
		)
	);
	var $virtualFields = array(
		'bankaccounttype_name' => 'CONCAT(Bankaccounttype.bankaccounttypename, " / ", Bankaccounttype.bankaccounttypenamebn)',
		'isActive' => 'IF(Bankaccounttype.bankaccounttypeisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Bankaccounttype.bankaccounttypeisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
	public $validate = array(
		'bankaccounttypename' => array(
			'rule' => 'notEmpty',
			'allowEmpty' => false,
			'required' => true,
			'message' => 'Enter a valid Bank Account Type Name'
		),
		'bankaccounttypenamebn' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'allowEmpty' => false,
			'message' => 'Enter a valid Bank Account Type Name in Bangla'
		),
	);
}

?>