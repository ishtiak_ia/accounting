<?php
	echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id=null);
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('COA Type')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA Group')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA Class')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA Sub Class')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA Sub Class BN')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA Sub Class Code')  => array('class' => 'highlight sortable')
						), 
						__('Active'), 
						__('Action')
					)
				);
				$coaclasssubclassrows = array();
				$countCoaclasssubclass = 0-1+$this->Paginator->counter('%start%');
				foreach($coaclasssubclass as $Thiscoaclasssubclass):
					$countCoaclasssubclass++;
					$coaclasssubclassrows[]= array($countCoaclasssubclass,
						$Thiscoaclasssubclass["Coatype"]["coatypename"]."/".$Thiscoaclasssubclass["Coatype"]["coatypenamebn"],
						$Thiscoaclasssubclass["Coagroup"]["coagroupname"]."/".$Thiscoaclasssubclass["Coagroup"]["coagroupnamebn"],
						$Thiscoaclasssubclass["Coaclass"]["coaclassname"]."/".$Thiscoaclasssubclass["Coaclass"]["coaclassnamebn"],
						$Thiscoaclasssubclass["Coaclasssubclass"]["coaclasssubclassname"],
						$Thiscoaclasssubclass["Coaclasssubclass"]["coaclasssubclassnamebn"],
						$Thiscoaclasssubclass["Coaclasssubclass"]["coaclasssubclasscode"],
						$Utilitys->statusURL($this->request["controller"], 'coaclasssubclass', $Thiscoaclasssubclass["Coaclasssubclass"]["id"], $Thiscoaclasssubclass["Coaclasssubclass"]["coaclasssubclassuuid"], $Thiscoaclasssubclass["Coaclasssubclass"]["coaclasssubclassisactive"]),
						$Utilitys->cusurl($this->request["controller"], 'coaclasssubclass', $Thiscoaclasssubclass["Coaclasssubclass"]["id"], $Thiscoaclasssubclass["Coaclasssubclass"]["coaclasssubclassuuid"]));
				endforeach;
				echo $this->Html->tableCells(
					$coaclasssubclassrows
				);
			echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var subclassname = $('#CoasubclassCoasubclassname').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{subclassname:subclassname, subclasscode:subclasscode},
						async: true,
						beforeSend: function(){
							$(\"#subclasssearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#subclasssearchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>