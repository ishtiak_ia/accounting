$(function() {
    $("#PushingexpensePushingexpensecaroil").keyup(function(){
        var caroil = this.value;
        if(caroil =='' || caroil == 'NaN'){
            caroil = 0;
        }else{
            caroil = parseFloat(caroil);
        }

        var transport = $("#PushingexpensePushingexpensetransport").val();
        if(transport =='' || transport == 'NaN'){
            transport = 0;
        }else{
            transport = parseFloat(transport);
        }
        var dailyfood = $("#PushingexpensePushingexpensedailyfood").val();
        if(dailyfood =='' || dailyfood == 'NaN'){
            dailyfood = 0;
        }else{
            dailyfood = parseFloat(dailyfood);
        }
        var snack = $("#PushingexpensePushingexpensesnack").val();
        if(snack =='' || snack == 'NaN'){
            snack = 0;
        }else{
            snack = parseFloat(snack);
        }
        var entertain = $("#PushingexpensePushingexpenseentertain").val();
        if(entertain =='' || entertain == 'NaN'){
            entertain = 0;
        }else{
            entertain = parseFloat(entertain);
        }
        var etcamount = $("#PushingexpensePushingexpenseetcamount").val();
        if(etcamount =='' || etcamount == 'NaN'){
            etcamount = 0;
        }else{
            etcamount = parseFloat(etcamount);
        }
        var courieramount = $("#PushingexpensePushingexpensecourier").val();
        if(courieramount =='' || courieramount == 'NaN'){
            courieramount = 0;
        }else{
            courieramount = parseFloat(courieramount);
        }
        var residenceamount = $("#PushingexpensePushingexpensehotelresidence").val();
        if(residenceamount =='' || residenceamount == 'NaN'){
            residenceamount = 0;
        }else{
            residenceamount = parseFloat(residenceamount);
        }

        var total_expense = (caroil + transport + dailyfood + snack + entertain + etcamount + residenceamount + courieramount);
        $("#PushingexpensePushingexpensetotalexpense").val(total_expense);
    });   
    $("#PushingexpensePushingexpensetransport").keyup(function(){
        var transport = this.value;
        if(transport =='' || transport == 'NaN'){
            transport = 0;
        }else{
            transport = parseFloat(transport);
        }

        var caroil = $("#PushingexpensePushingexpensecaroil").val();
        if(caroil =='' || caroil == 'NaN'){
            caroil = 0;
        }else{
            caroil = parseFloat(caroil);
        }
        var dailyfood = $("#PushingexpensePushingexpensedailyfood").val();
        if(dailyfood =='' || dailyfood == 'NaN'){
            dailyfood = 0;
        }else{
            dailyfood = parseFloat(dailyfood);
        }
        var snack = $("#PushingexpensePushingexpensesnack").val();
        if(snack =='' || snack == 'NaN'){
            snack = 0;
        }else{
            snack = parseFloat(snack);
        }
        var entertain = $("#PushingexpensePushingexpenseentertain").val();
        if(entertain =='' || entertain == 'NaN'){
            entertain = 0;
        }else{
            entertain = parseFloat(entertain);
        }
        var etcamount = $("#PushingexpensePushingexpenseetcamount").val();
        if(etcamount =='' || etcamount == 'NaN'){
            etcamount = 0;
        }else{
            etcamount = parseFloat(etcamount);
        }
        var courieramount = $("#PushingexpensePushingexpensecourier").val();
        if(courieramount =='' || courieramount == 'NaN'){
            courieramount = 0;
        }else{
            courieramount = parseFloat(courieramount);
        }
        var residenceamount = $("#PushingexpensePushingexpensehotelresidence").val();
        if(residenceamount =='' || residenceamount == 'NaN'){
            residenceamount = 0;
        }else{
            residenceamount = parseFloat(residenceamount);
        }

        var total_expense = (caroil + transport + dailyfood + snack + entertain + etcamount + courieramount + residenceamount);
        $("#PushingexpensePushingexpensetotalexpense").val(total_expense);
    }); 
    $("#PushingexpensePushingexpensedailyfood").keyup(function(){
        var dailyfood = this.value;
        if(dailyfood =='' || dailyfood == 'NaN'){
            dailyfood = 0;
        }else{
            dailyfood = parseFloat(dailyfood);
        }

        var transport = $("#PushingexpensePushingexpensetransport").val();
        if(transport =='' || transport == 'NaN'){
            transport = 0;
        }else{
            transport = parseFloat(transport);
        }
        var caroil = $("#PushingexpensePushingexpensecaroil").val();
        if(caroil =='' || caroil == 'NaN'){
            caroil = 0;
        }else{
            caroil = parseFloat(caroil);
        }
        var snack = $("#PushingexpensePushingexpensesnack").val();
        if(snack =='' || snack == 'NaN'){
            snack = 0;
        }else{
            snack = parseFloat(snack);
        }
        var entertain = $("#PushingexpensePushingexpenseentertain").val();
        if(entertain =='' || entertain == 'NaN'){
            entertain = 0;
        }else{
            entertain = parseFloat(entertain);
        }
        var etcamount = $("#PushingexpensePushingexpenseetcamount").val();
        if(etcamount =='' || etcamount == 'NaN'){
            etcamount = 0;
        }else{
            etcamount = parseFloat(etcamount);
        }
        var courieramount = $("#PushingexpensePushingexpensecourier").val();
        if(courieramount =='' || courieramount == 'NaN'){
            courieramount = 0;
        }else{
            courieramount = parseFloat(courieramount);
        }
        var residenceamount = $("#PushingexpensePushingexpensehotelresidence").val();
        if(residenceamount =='' || residenceamount == 'NaN'){
            residenceamount = 0;
        }else{
            residenceamount = parseFloat(residenceamount);
        }

        var total_expense = (caroil + transport + dailyfood + snack + entertain + etcamount + courieramount + residenceamount);
        $("#PushingexpensePushingexpensetotalexpense").val(total_expense);
    });
    $("#PushingexpensePushingexpensesnack").keyup(function(){
        var snack = this.value;
        if(snack =='' || snack == 'NaN'){
            snack = 0;
        }else{
            snack = parseFloat(snack);
        }

        var transport = $("#PushingexpensePushingexpensetransport").val();
        if(transport =='' || transport == 'NaN'){
            transport = 0;
        }else{
            transport = parseFloat(transport);
        }
        var dailyfood = $("#PushingexpensePushingexpensedailyfood").val();
        if(dailyfood =='' || dailyfood == 'NaN'){
            dailyfood = 0;
        }else{
            dailyfood = parseFloat(dailyfood);
        }
        var caroil = $("#PushingexpensePushingexpensecaroil").val();
        if(caroil =='' || caroil == 'NaN'){
            caroil = 0;
        }else{
            caroil = parseFloat(caroil);
        }
        var entertain = $("#PushingexpensePushingexpenseentertain").val();
        if(entertain =='' || entertain == 'NaN'){
            entertain = 0;
        }else{
            entertain = parseFloat(entertain);
        }
        var etcamount = $("#PushingexpensePushingexpenseetcamount").val();
        if(etcamount =='' || etcamount == 'NaN'){
            etcamount = 0;
        }else{
            etcamount = parseFloat(etcamount);
        }
        var courieramount = $("#PushingexpensePushingexpensecourier").val();
        if(courieramount =='' || courieramount == 'NaN'){
            courieramount = 0;
        }else{
            courieramount = parseFloat(courieramount);
        }
        var residenceamount = $("#PushingexpensePushingexpensehotelresidence").val();
        if(residenceamount =='' || residenceamount == 'NaN'){
            residenceamount = 0;
        }else{
            residenceamount = parseFloat(residenceamount);
        }

        var total_expense = (caroil + transport + dailyfood + snack + entertain + etcamount + courieramount + residenceamount);
        $("#PushingexpensePushingexpensetotalexpense").val(total_expense);
    });     
    $("#PushingexpensePushingexpenseentertain").keyup(function(){
        var entertain = this.value;
        if(entertain =='' || entertain == 'NaN'){
            entertain = 0;
        }else{
            entertain = parseFloat(entertain);
        }

        var transport = $("#PushingexpensePushingexpensetransport").val();
        if(transport =='' || transport == 'NaN'){
            transport = 0;
        }else{
            transport = parseFloat(transport);
        }
        var dailyfood = $("#PushingexpensePushingexpensedailyfood").val();
        if(dailyfood =='' || dailyfood == 'NaN'){
            dailyfood = 0;
        }else{
            dailyfood = parseFloat(dailyfood);
        }
        var snack = $("#PushingexpensePushingexpensesnack").val();
        if(snack =='' || snack == 'NaN'){
            snack = 0;
        }else{
            snack = parseFloat(snack);
        }
        var caroil = $("#PushingexpensePushingexpensecaroil").val();
        if(caroil =='' || caroil == 'NaN'){
            caroil = 0;
        }else{
            caroil = parseFloat(caroil);
        }
        var etcamount = $("#PushingexpensePushingexpenseetcamount").val();
        if(etcamount =='' || etcamount == 'NaN'){
            etcamount = 0;
        }else{
            etcamount = parseFloat(etcamount);
        }
        var courieramount = $("#PushingexpensePushingexpensecourier").val();
        if(courieramount =='' || courieramount == 'NaN'){
            courieramount = 0;
        }else{
            courieramount = parseFloat(courieramount);
        }
        var residenceamount = $("#PushingexpensePushingexpensehotelresidence").val();
        if(residenceamount =='' || residenceamount == 'NaN'){
            residenceamount = 0;
        }else{
            residenceamount = parseFloat(residenceamount);
        }

        var total_expense = (caroil + transport + dailyfood + snack + entertain + etcamount + courieramount + residenceamount);
        $("#PushingexpensePushingexpensetotalexpense").val(total_expense);
    }); 
    $("#PushingexpensePushingexpenseetcamount").keyup(function(){
        var etcamount = this.value;
        if(etcamount =='' || etcamount == 'NaN'){
            etcamount = 0;
        }else{
            etcamount = parseFloat(etcamount);
        }

        var transport = $("#PushingexpensePushingexpensetransport").val();
        if(transport =='' || transport == 'NaN'){
            transport = 0;
        }else{
            transport = parseFloat(transport);
        }
        var dailyfood = $("#PushingexpensePushingexpensedailyfood").val();
        if(dailyfood =='' || dailyfood == 'NaN'){
            dailyfood = 0;
        }else{
            dailyfood = parseFloat(dailyfood);
        }
        var snack = $("#PushingexpensePushingexpensesnack").val();
        if(snack =='' || snack == 'NaN'){
            snack = 0;
        }else{
            snack = parseFloat(snack);
        }
        var entertain = $("#PushingexpensePushingexpenseentertain").val();
        if(entertain =='' || entertain == 'NaN'){
            entertain = 0;
        }else{
            entertain = parseFloat(entertain);
        }
        var caroil = $("#PushingexpensePushingexpensecaroil").val();
        if(caroil =='' || caroil == 'NaN'){
            caroil = 0;
        }else{
            caroil = parseFloat(caroil);
        }

        var courieramount = $("#PushingexpensePushingexpensecourier").val();
        if(courieramount =='' || courieramount == 'NaN'){
            courieramount = 0;
        }else{
            courieramount = parseFloat(courieramount);
        }
        var residenceamount = $("#PushingexpensePushingexpensehotelresidence").val();
        if(residenceamount =='' || residenceamount == 'NaN'){
            residenceamount = 0;
        }else{
            residenceamount = parseFloat(residenceamount);
        }

        var total_expense = (caroil + transport + dailyfood + snack + entertain + etcamount + courieramount + residenceamount);
        $("#PushingexpensePushingexpensetotalexpense").val(total_expense);
    });

    $("#PushingexpensePushingexpensecourier").keyup(function(){
        var courieramount = this.value;
        if(courieramount =='' || courieramount == 'NaN'){
            courieramount = 0;
        }else{
            courieramount = parseFloat(courieramount);
        }

        var etcamount = $("#PushingexpensePushingexpenseetcamount").val();
        if(etcamount =='' || etcamount == 'NaN'){
            etcamount = 0;
        }else{
            etcamount = parseFloat(etcamount);
        }

        var transport = $("#PushingexpensePushingexpensetransport").val();
        if(transport =='' || transport == 'NaN'){
            transport = 0;
        }else{
            transport = parseFloat(transport);
        }
        var dailyfood = $("#PushingexpensePushingexpensedailyfood").val();
        if(dailyfood =='' || dailyfood == 'NaN'){
            dailyfood = 0;
        }else{
            dailyfood = parseFloat(dailyfood);
        }
        var snack = $("#PushingexpensePushingexpensesnack").val();
        if(snack =='' || snack == 'NaN'){
            snack = 0;
        }else{
            snack = parseFloat(snack);
        }
        var entertain = $("#PushingexpensePushingexpenseentertain").val();
        if(entertain =='' || entertain == 'NaN'){
            entertain = 0;
        }else{
            entertain = parseFloat(entertain);
        }
        var caroil = $("#PushingexpensePushingexpensecaroil").val();
        if(caroil =='' || caroil == 'NaN'){
            caroil = 0;
        }else{
            caroil = parseFloat(caroil);
        }

        var residenceamount = $("#PushingexpensePushingexpensehotelresidence").val();
        if(residenceamount =='' || residenceamount == 'NaN'){
            residenceamount = 0;
        }else{
            residenceamount = parseFloat(residenceamount);
        }

        var total_expense = (caroil + transport + dailyfood + snack + entertain + etcamount + courieramount + residenceamount);
        $("#PushingexpensePushingexpensetotalexpense").val(total_expense);
    });


    $("#PushingexpensePushingexpensehotelresidence").keyup(function(){
        var residenceamount = this.value;
        if(residenceamount =='' || residenceamount == 'NaN'){
            residenceamount = 0;
        }else{
            residenceamount = parseFloat(residenceamount);
        }

        var etcamount = $("#PushingexpensePushingexpenseetcamount").val();
        if(etcamount =='' || etcamount == 'NaN'){
            etcamount = 0;
        }else{
            etcamount = parseFloat(etcamount);
        }

        var transport = $("#PushingexpensePushingexpensetransport").val();
        if(transport =='' || transport == 'NaN'){
            transport = 0;
        }else{
            transport = parseFloat(transport);
        }
        var dailyfood = $("#PushingexpensePushingexpensedailyfood").val();
        if(dailyfood =='' || dailyfood == 'NaN'){
            dailyfood = 0;
        }else{
            dailyfood = parseFloat(dailyfood);
        }
        var snack = $("#PushingexpensePushingexpensesnack").val();
        if(snack =='' || snack == 'NaN'){
            snack = 0;
        }else{
            snack = parseFloat(snack);
        }
        var entertain = $("#PushingexpensePushingexpenseentertain").val();
        if(entertain =='' || entertain == 'NaN'){
            entertain = 0;
        }else{
            entertain = parseFloat(entertain);
        }
        var caroil = $("#PushingexpensePushingexpensecaroil").val();
        if(caroil =='' || caroil == 'NaN'){
            caroil = 0;
        }else{
            caroil = parseFloat(caroil);
        }

        var courieramount = $("#PushingexpensePushingexpensecourier").val();
        if(courieramount =='' || courieramount == 'NaN'){
            courieramount = 0;
        }else{
            courieramount = parseFloat(courieramount);
        }

        var total_expense = (caroil + transport + dailyfood + snack + entertain + etcamount + courieramount + residenceamount);
        $("#PushingexpensePushingexpensetotalexpense").val(total_expense);
    });

    $("#PushingexpensePushingexpensetotalusage").keyup(function(){

        var finishing = $("#PushingexpensePushingexpenseafterfinishing").val();
        if(finishing =='' || finishing == 'NaN'){
            finishing = 0;
        }else{
            finishing = parseFloat(finishing);
        }
        var startting = $("#PushingexpensePushingexpenseatstarting").val();
        if(startting =='' || startting == 'NaN'){
            startting = 0;
        }else{
            startting = parseFloat(startting);
        }

        var total_usage = (finishing - startting);
        $("#PushingexpensePushingexpensetotalusage").val(total_usage);
    });

});