<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Product Type Name')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Product Type Name BN')  => array('class' => 'highlight sortable')
					), 
					__('Active'), 
					__('Action')
				)
			);
			$producttyperows = array();
			$countProducttype = 0-1+$this->Paginator->counter('%start%');
			foreach($producttype as $Thisproducttype):
				$countProducttype++;
				$producttyperows[]= array($countProducttype,$Thisproducttype["Producttype"]["producttypename"],$Thisproducttype["Producttype"]["producttypenamebn"],$Utilitys->statusURL($this->request["controller"], 'producttype', $Thisproducttype["Producttype"]["id"], $Thisproducttype["Producttype"]["producttypeuuid"], $Thisproducttype["Producttype"]["producttypeisactive"]),$Utilitys->cusurl($this->request["controller"], 'producttype', $Thisproducttype["Producttype"]["id"], $Thisproducttype["Producttype"]["producttypeuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$producttyperows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#ProducttypeSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#producttypesarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#producttypesarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>