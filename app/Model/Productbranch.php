<?php
App::uses('AppModel', 'Model');
class Productbranch extends AppModel {
	public $name = 'Productbranch';
	public $usetables = 'productbranches';

	var $belongsTo = array(
		'Company' => array(
			'fields' =>array('companyname', 'companynamebn'),
			'className'    => 'Company',
			'foreignKey'    => 'company_id'
		),
		'Branch' => array(
			'fields' =>array('branchname', 'branchnamebn'),
			'className'    => 'Branch',
			'foreignKey'    => 'branch_id'
		),
		'Product' => array(
			'fields' =>array('id','productname','productnamebn', 'productcode'),
//			'fields' =>array('Product.*'),
			'className'    => 'Product',
			//'foreignKey'    => 'product_id'
			'foreignKey'    => false,
			'conditions'	=> 'Productbranch.product_id=Product.id'
		),
		'Unit' => array(
			'fields' =>array('unitname', 'unitnamebn'),
			'className'    => 'Unit',
			'foreignKey'    => false,
			'conditions'	=> 'Product.unit_id=Unit.id'
		)
	);
	var $virtualFields = array(
		'company_name' => 'CONCAT(Company.companyname, " / ", Company.companynamebn)',
		//'product_name' => 'Product.productname',
		'product_name' => 'CONCAT(Product.productname, " / ", Product.productnamebn)',
		//'product_type' => 'Product.producttype_id',
		'product_code' => 'Product.productcode',
		'branch_name' => 'CONCAT(Branch.branchname, " / ", Branch.branchnamebn)',
		'isActive' => 'IF(Productbranch.productbranchisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Productbranch.productbranchisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
		//'isActive' => 'IF(User.userisactive = 0, "<span class=\"orange\">Inactive</span>", IF(User.userisactive = 1, "<span class=\"green\">Active</span>", "<span class=\"red\">Deleted</span>"))'
	);
}