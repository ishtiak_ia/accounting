<?php
	echo "<h2>".__('Bank Account Type'). $Utilitys->addurl($title=__("Add New Bank Account Type"), $this->request["controller"], $page="bankaccounttype")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Bankaccounttype.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
			)
		);
		$Utilitys->printbutton ($printableid='bankaccounttypesarchloading', $searchfield=null);
	echo $Utilitys->filtertagend();
	echo"
		<div id=\"bankaccounttypesarchloading\">
	";
			echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id='printtable');
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('Account Type')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Account Type BN')  => array('class' => 'highlight sortable')
						), 
						__('Active'), 
						__('Action')
					)
				);
				$bankaccounttyperows = array();
				$countBankaccounttype = 0-1+$this->Paginator->counter('%start%');
				foreach($bankaccounttype as $Thisbankaccounttype):
					$countBankaccounttype++;
					$bankaccounttyperows[]= array($countBankaccounttype,$Thisbankaccounttype["Bankaccounttype"]["bankaccounttypename"],$Thisbankaccounttype["Bankaccounttype"]["bankaccounttypenamebn"],$Utilitys->statusURL($this->request["controller"], 'bankaccounttype', $Thisbankaccounttype["Bankaccounttype"]["id"], $Thisbankaccounttype["Bankaccounttype"]["bankaccounttypeuuid"], $Thisbankaccounttype["Bankaccounttype"]["bankaccounttypeisactive"]),$Utilitys->cusurl($this->request["controller"], 'bankaccounttype', $Thisbankaccounttype["Bankaccounttype"]["id"], $Thisbankaccounttype["Bankaccounttype"]["bankaccounttypeuuid"]));
				endforeach;
				echo $this->Html->tableCells(
					$bankaccounttyperows
				);
			echo $Utilitys->tableend();
			echo $Utilitys->paginationcommon();

	echo"
		</div>
		<script>
			$(document).ready(function() {
				$(\"#BankaccounttypeSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#bankaccounttypesarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/bankaccounttypesearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#bankaccounttypesarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>
