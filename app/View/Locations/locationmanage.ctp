<?php
	echo "<h2>".__('Location'). $Utilitys->addurl($title=__("Add New Location"), $this->request["controller"], $page="location")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Location.divisionid",
			array(
				'type'=>'select',
				"options"=>array($divisionlist),
				'empty'=>__('Division'),
				'div' => array('class'=> 'form-group'),
				'tabindex'=>1,
				'label' => false,
				'class'=> 'form-control',
				'style'=>''
			)
		);
		echo $this->Form->input(
			"Location.districid",
			array(
				'type'=>'select',
				"options"=>array($districlist),
				'empty'=>__('District'),
				'div' => array('class'=> 'form-group'),
				'tabindex'=>2,
				'label' => false,
				'class'=> 'form-control',
				'style'=>''
			)
		);
		echo $this->Form->input(
			"Location.upazilaid",
			array(
				'type'=>'select',
				"options"=>array($upazilalist),
				'empty'=>__('Upazila'),
				'div' => array('class'=> 'form-group'),
				'tabindex'=>3,
				'label' => false,
				'class'=> 'form-control',
				'style'=>''
			)
		);
		echo $this->Form->input(
			"Location.unionparishadid",
			array(
				'type'=>'select',
				"options"=>array($unionparishadlist),
				'empty'=>__('Union Parishad'),
				'div' => array('class'=> 'form-group'),
				'tabindex'=>4,
				'label' => false,
				'class'=> 'form-control',
				'style'=>''
			)
		);
		/*echo $this->Form->input(
			"Location.paraid",
			array(
				'type'=>'select',
				"options"=>array($paralist),
				'empty'=>__('Para / Area'),
				'div' => array('class'=> 'form-group'),
				'tabindex'=>5,
				'label' => false,
				'class'=> 'form-control',
				'style'=>''
			)
		);*/
		echo $this->Form->input(
			"Location.searchtext",
			array(
				'type' => 'text',
				'div' => array('class'=> 'form-group'),
				'label' => false,
				'tabindex'=>6,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => ''
			)
		);
	echo $Utilitys->filtertagend();
	echo"
		<div id=\"locationsarchloading\">
	";
	//echo $this->Paginator->counter('%start%');
		echo $Utilitys->paginationcommon();
		echo $Utilitys->tablestart($class=null, $id=null);
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Location')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Location BN')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Parent')  => array('class' => 'highlight sortable')
					), 
					__('Active'), 
					__('Action')
				)
			);
			$locationrows = array();
			$countLocation = 0-1+$this->Paginator->counter('%start%');
			foreach($location as $Thislocation):
				$countLocation++;
				$locationrows[]= array($countLocation,$Thislocation["Location"]["locationname"],$Thislocation["Location"]["locationnamebn"],$Thislocation["Location"]["locationparent_name"],$Utilitys->statusURL($this->request["controller"], 'location', $Thislocation["Location"]["id"], $Thislocation["Location"]["locationuuid"], $Thislocation["Location"]["locationisactive"]),$Utilitys->cusurl($this->request["controller"], 'location', $Thislocation["Location"]["id"], $Thislocation["Location"]["locationuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$locationrows
			);
		echo $Utilitys->tableend();
		echo $Utilitys->paginationcommon();
	echo"
		</div>
		<script>
			$(document).ready(function() {
				$(\"#LocationSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#locationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."locations/locationsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#locationsarchloading\").html(result);
						}
					);
				});
				$(\"#LocationDivisionid\").change(function(){
					var LocationDivisionid = $(this).val();
					$(\"#locationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."locations/districbydividion',
						{LocationDivisionid:LocationDivisionid},
						function(result) {
							$(\"#LocationDistricid\").html(result);
						}
					);
					$.post(
						'".$this->webroot."locations/locationsearchbytext',
						{LocationDivisionid:LocationDivisionid},
						function(result) {
							$(\"#locationsarchloading\").html(result);
						}
					);
				});
				$(\"#LocationDistricid\").change(function(){
					var LocationDistricid = $(this).val();
					$(\"#locationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."locations/upazilabydistric',
						{LocationDistricid:LocationDistricid},
						function(result) {
							$(\"#LocationUpazilaid\").html(result);
						}
					);
					$.post(
						'".$this->webroot."locations/locationsearchbytext',
						{LocationDistricid:LocationDistricid},
						function(result) {
							$(\"#locationsarchloading\").html(result);
						}
					);
				});
				$(\"#LocationUpazilaid\").change(function(){
					var LocationUpazilaid = $(this).val();
					$(\"#locationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."locations/unionparishadbyupazila',
						{LocationUpazilaid:LocationUpazilaid},
						function(result) {
							$(\"#LocationUnionparishadid\").html(result);
						}
					);
					$.post(
						'".$this->webroot."locations/locationsearchbytext',
						{LocationUpazilaid:LocationUpazilaid},
						function(result) {
							$(\"#locationsarchloading\").html(result);
						}
					);
				});
				$(\"#LocationUnionparishadid\").change(function(){
					var LocationUnionparishadid = $(this).val();
					$(\"#locationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					/*$.post(
						'".$this->webroot."locations/paraareabyunionparishad',
						{LocationUnionparishadid:LocationUnionparishadid},
						function(result) {
							$(\"#LocationParaid\").html(result);
						}
					);*/
					$.post(
						'".$this->webroot."locations/locationsearchbytext',
						{LocationUnionparishadid:LocationUnionparishadid},
						function(result) {
							$(\"#locationsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>