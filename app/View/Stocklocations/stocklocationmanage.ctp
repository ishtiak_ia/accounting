<?php
	echo "<h2>".__('Stocklocation'). $Utilitys->addurl($title=__("Add New Stocklocation"), $this->request['controller'], $page="stocklocation")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Stocklocation.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'tabindex'=>3,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"stocklocationsarchloading\">
	";
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('Stocklocation Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Stocklocation Name BN')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Stocklocation Address')  => array('class' => 'highlight sortable')
				),
				__('Active'), 
				__('Action')
			)
		);
		$stocklocationrows = array();
		$countStocklocation = 0-1+$this->Paginator->counter('%start%');
		foreach($stocklocations as $Thisstocklocation):
			$countStocklocation++;
			$stocklocationrows[]= array($countStocklocation,$Thisstocklocation["Stocklocation"]["stocklocationname"],$Thisstocklocation["Stocklocation"]["stocklocationnamebn"],$Thisstocklocation["Stocklocation"]["stocklocationaddress"],$Utilitys->statusURL($this->request["controller"], 'stocklocation', $Thisstocklocation["Stocklocation"]["id"], $Thisstocklocation["Stocklocation"]["stocklocationuuid"], $Thisstocklocation["Stocklocation"]["stocklocationisactive"]),$Utilitys->cusurl($this->request["controller"], 'stocklocation', $Thisstocklocation["Stocklocation"]["id"], $Thisstocklocation["Stocklocation"]["stocklocationuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$stocklocationrows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();

	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				$(\"#StocklocationSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#stocklocationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."stocklocations/stocklocationsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#stocklocationsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>