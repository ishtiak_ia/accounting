<?php
	echo $Utilitys->paginationcommon();
					echo $Utilitys->tablestart();
						echo $this->Html->tableHeaders(
							array(
								__('SL#'), 
								array(
									__('Menu Name')  => array('class' => 'highlight sortable')
								), 
								array(
									__('URL')  => array('class' => 'highlight sortable')
								),
								array(
									__('Is Parent')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Is Permanent')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Is Active')  => array('class' => 'highlight sortable')
								), 
								__('Action')
							)
						);
						$menurows = array();
				$countMenu  = 0-1+$this->Paginator->counter('%start%');
				foreach($menus as $Thismenu):
					$countMenu ++;
					$menurows[]= array($countMenu ,$Thismenu["Menu"]["menuname"],$Thismenu["Menu"]["menuurl"],$Thismenu["Menu"]["isParent"],$Thismenu["Menu"]["isPermanent"],$Utilitys->statusURL($this->request["controller"], 'menu', $Thismenu["Menu"]["id"], $Thismenu["Menu"]["menuuuid"], $Thismenu["Menu"]["menuisactive"]),$Utilitys->cusurl($this->request["controller"], 'menu', $Thismenu["Menu"]["id"], $Thismenu["Menu"]["menuuuid"]));
				endforeach;
				echo $this->Html->tableCells(
					$menurows
				);
			echo $Utilitys->tableend();
			echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#MenuSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#menusarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#menusarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>