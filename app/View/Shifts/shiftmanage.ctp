<?php
	echo "<h2>".__('Shift'). $Utilitys->addurl($title=__("Add New Shift"), $this->request['controller'], $page="shift")."</h2>";	
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Shift.searchtext",
			array(
				'type' => 'text', 
				'div' => false,
				'label' => false,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
				)
			);
	echo $Utilitys->filtertagend();	
	echo "
		<div id=\"shiftsarchloading\">
	";
	
	echo $Utilitys->paginationcommon();
					echo $Utilitys->tablestart();
						echo $this->Html->tableHeaders(
							array(
								__('SL#'), 
								array(
									__('Shift Name')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Is Parmanent')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Is Active')  => array('class' => 'highlight sortable')
								), 
								__('Action')
							)
						);
						$shiftrows = array();
						$countShift = 0-1+$this->Paginator->counter('%start%');
						foreach($shifts as $Thisshift):
							$countShift++;
							$shiftrows[]= array($countShift,$Thisshift["Shift"]["shiftname"],$Thisshift["Shift"]["isPermanent"],$Utilitys->statusURL($this->request["controller"], 'shift', $Thisshift["Shift"]["id"], $Thisshift["Shift"]["shiftuuid"], 
								$Thisshift["Shift"]["shiftisactive"]),$Utilitys->cusurl($this->request['controller'], 'shift', $Thisshift["Shift"]["id"], $Thisshift["Shift"]["shiftuuid"]));
						endforeach;
						echo $this->Html->tableCells(
							$shiftrows
						);
				echo $Utilitys->tableend();
				echo $Utilitys->paginationcommon();

	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				$(\"#ShiftSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#shiftsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."shifts/shiftsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#shiftsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>