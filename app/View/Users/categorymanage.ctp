<?php
	echo "<h2>".__('Category'). $Utilitys->addurl($title=__("Add New Category"), $this->request['controller'], $page="category")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Category.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"categorysearchloading\">
	";
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Category Name')  => array('class' => 'highlight sortable')
					),
					__('Active'), 
					__('Action')
				)
			);
			$categoryrows = array();
			$countCategory = 0-1+$this->Paginator->counter('%start%');
			foreach($category as $Thiscategory):
				$countCategory++;
				$categoryrows[]= array($countCategory,$Thiscategory["Category"]["categoryname"],$Utilitys->statusURL($this->request["controller"], 'category', $Thiscategory["Category"]["id"], $Thiscategory["Category"]["categoryuuid"], $Thiscategory["Category"]["categoryisactive"]),$Utilitys->cusurl($this->request["controller"], 'category', $Thiscategory["Category"]["id"], $Thiscategory["Category"]["categoryuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$categoryrows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();

	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				$(\"#CategorySearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#categorysearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/categorysearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#categorysearchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>