<?php
//pr($product);
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"3\">
				<h2>".__('Department Information')."</h2>
			</td>
		</tr>
		<tr>
			<th>
				".__('Department Name')."
			</th>
			<td>
				".$department[0]["Department"]["department_name"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Company Name')."
			</th>
			<td>
				".$department[0]["Department"]["company_fullname"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Group Name')."
			</th>
			<td>
				".$department[0]["Department"]["group_name"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Active")."
			</th>
			<td>
				".$department[0]["Department"]["isActive"]."
			</td>
		</tr>
	";
	echo $Utilitys->tableend($class=null, $id=null);
?>