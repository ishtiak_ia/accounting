<?php
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"2\">
				<h2>".__('Menu')."</h2>
			</td>
		</tr>
		<tr>
			<th>
				".__('Name')."
			</th>
			<td>
				".$menu[0]["Menu"]["menuname"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('URL')."
			</th>
			<td>
				".$menu[0]["Menu"]["menuurl"]."
			</td>
		</tr>
		
		<tr>
			<th>
				".__('Is Parent')."
			</th>
			<td>
				".$menu[0]["Menu"]["isParent"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Is Permanent')."
			</th>
			<td>
				".$menu[0]["Menu"]["isPermanent"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Is Active")."
			</th>
			<td>
				".$menu[0]["Menu"]["isActive"]."
			</td>
		</tr>
	";
	echo $Utilitys->tableend();
?>