<?php
echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Employee Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Emergency Contact Name')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Emergency Contact BN')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Relationship')  => array('class' => 'highlight sortable')
					),
					__('Active'), 
					__('Action')
				)
			);
			$useremergencycontactrows = array();
			$countUseremergencycontactrows = 0-1+$this->Paginator->counter('%start%');
			foreach($useremergencycontact as $Thisuseremergencycontact):
				$countUseremergencycontactrows++;
				$useremergencycontactrows[]= array($countUseremergencycontactrows,$Thisuseremergencycontact["Useremergencycontact"]["user_fullname"],$Thisuseremergencycontact["Useremergencycontact"]["useremergencycontactname"],$Thisuseremergencycontact["Useremergencycontact"]["useremergencycontactnamebn"],$Thisuseremergencycontact["Useremergencycontact"]["useremergencycontactrelationship"],$Utilitys->statusURL($this->request["controller"], 'useremergencycontact', $Thisuseremergencycontact["Useremergencycontact"]["id"], $Thisuseremergencycontact["Useremergencycontact"]["useremergencycontactuuid"], $Thisuseremergencycontact["Useremergencycontact"]["useremergencycontactisactive"]),$Utilitys->cusurl($this->request["controller"], 'useremergencycontact', $Thisuseremergencycontact["Useremergencycontact"]["id"], $Thisuseremergencycontact["Useremergencycontact"]["useremergencycontactuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$useremergencycontactrows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#UseremergencycontactSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#useremergencycontactsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#useremergencycontactsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>