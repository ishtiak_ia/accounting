<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart($class=null, $id=null);
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('Bank')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Bank BN')  => array('class' => 'highlight sortable')
				), 
				__('Active'), 
				__('Action')
			)
		);
		$bankrows = array();
		$countBank = 0-1+$this->Paginator->counter('%start%');
		foreach($bank as $Thisbank):
			$countBank++;
			$bankrows[]= array($countBank,$Thisbank["Bank"]["bankname"],$Thisbank["Bank"]["banknamebn"],$Utilitys->statusURL($this->request["controller"], 'bank', $Thisbank["Bank"]["id"], $Thisbank["Bank"]["bankuuid"], $Thisbank["Bank"]["bankisactive"]),$Utilitys->cusurl($this->request["controller"], 'bank', $Thisbank["Bank"]["id"], $Thisbank["Bank"]["bankuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$bankrows
		);
		echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#BankSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#banksarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#banksarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>