<?php
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"2\">
				<h2>".__('Bank Account')."</h2>
			</td>
		</tr>
		<tr>
			<th>
				".__('Bank')."
			</th>
			<td>
				".$bankaccount[0]["Bankaccount"]["bank_name"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Branch')."
			</th>
			<td>
				".$bankaccount[0]["Bankaccount"]["bankbranch_name"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Account Type')."
			</th>
			<td>
				".$bankaccount[0]["Bankaccount"]["bankaccounttype_name"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Account')."
			</th>
			<td>
				".$bankaccount[0]["Bankaccount"]["bankaccountname"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Account BN")."
			</th>
			<td>
				".$bankaccount[0]["Bankaccount"]["bankaccountnamebn"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Number")."
			</th>
			<td>
				".$bankaccount[0]["Bankaccount"]["bankaccountnumber"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Active")."
			</th>
			<td>
				".$bankaccount[0]["Bankaccount"]["isActive"]."
			</td>
		</tr>
	";
	echo $Utilitys->tableend();
?>