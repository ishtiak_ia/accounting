<?php
$array_order=0;
$serial_no=1;
$net_change_purchase_rate=0;
$closing_balance_price = $closing_balance_quantity = $closing_balance_rate = 0;
$opening_balance_purchase_price = $opening_balance_purchase_quantity = $opening_balance_purchase_rate =0;

echo "<div class='center-aligned-texts'><h1>".$company_info[0]["Company"]["companyname"]."</h1></div>";
echo "<div class='center-aligned-texts'><b>".$company_info[0]["Company"]["companypresentaddress"]."</b></div>";
echo "<br><br>";
echo "<div class='left-aligned-texts'><b>Ledger</b>:".$general_ledger_data[0]["Coa"]["coacode"]."  ".$general_ledger_data[0]["Coa"]["coaname"]."</div>";
echo "<div  class='left-aligned-texts'><b>Date From:</b>".$form_date."<b> To: </b>".$to_date."</div><br>";

echo $Utilitys->tablestart($class=null, $id=null);
echo $this->Html->tableHeaders(
    array(
        array(
            __('Sl#')  => array('class' => '')
        ),
        array(
            __('Date')  => array('class' => '')
        ),
        array(
            __('Product Code')  => array('class' => '')
        ),
        array(
            __('Product Name')  => array('class' => '')
        ),
        array(
            __('Product Type')  => array('class' => '')
        ),
        array(
            __('Product Category')  => array('class' => '')
        ),
        array(
            __('Opening Balance')  => array('class' => '','colspan'=>'3')
        ),
        array(
            __('Purchase During The Period')  => array('class' => '','colspan'=>'3')
        ),
        array(
            __('Sales During The Period')  => array('class' => '','colspan'=>'3')
        ),
        array(
            __('Closing Balance')  => array('class' => '','colspan'=>'3')
        ),
        array(
            __('Stock Location')  => array('class' => '','colspan'=>'3')
        )
    )
);
echo $this->Html->tableHeaders(
    array(
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('Quantity')  => array('class' => '')
        ),
        array(
            __('Rate')  => array('class' => '')
        ),
        array(
            __('Total')  => array('class' => '')
        ),
        array(
            __('Quantity')  => array('class' => '')
        ),
        array(
            __('Rate')  => array('class' => '')
        ),
        array(
            __('Total')  => array('class' => '')
        ),
        array(
            __('Quantity')  => array('class' => '')
        ),
        array(
            __('Rate')  => array('class' => '')
        ),
        array(
            __('Total')  => array('class' => '')
        ),
        array(
            __('Quantity')  => array('class' => '')
        ),
        array(
            __('Rate')  => array('class' => '')
        ),
        array(
            __('Total')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        )
    )
);
//print_r($net_change_sales);
foreach($product_info as $product):
    $net_change_purchase_price =$net_change_purchase_quantity = $net_change_purchase_rate = 0;
    $net_change_sales_price = $net_change_sales_quantity = $net_change_sales_rate = 0;
    if($product["Orderdetail"]["transactiontype_id"] == 8){
        $net_change_purchase_price =$product["Orderdetail"]["price"];
        $net_change_purchase_quantity =$product["Orderdetail"]["quantity"];
        $net_change_purchase_rate =$product["Orderdetail"]["unitprice"];

    }else if($product["Orderdetail"]["transactiontype_id"] == 7){
        $net_change_sales_price = $product["Orderdetail"]["price"];
        $net_change_sales_quantity = $product["Orderdetail"]["quantity"];
        $net_change_sales_rate = $product["Orderdetail"]["unitprice"];

    }

    //opening balance start
    $opening_balance_purchase_price = $opening_balance_purchase[$array_order][0][0]["total_opening_price"] - $opening_balance_sales[$array_order][0][0]["total_opening_price"]-$opening_balance_purchase_return[$array_order][0][0]["total_opening_price"]+$opening_balance_sales_return[$array_order][0][0]["total_opening_price"];
    $opening_balance_purchase_quantity = $opening_balance_purchase[$array_order][0][0]["total_opening_quantity"] - $opening_balance_sales[$array_order][0][0]["total_opening_quantity"]-$opening_balance_purchase_return[$array_order][0][0]["total_opening_quantity"]+$opening_balance_sales_return[$array_order][0][0]["total_opening_quantity"];
    $opening_balance_purchase_rate = $opening_balance_purchase_price/$opening_balance_purchase_quantity;
    //opening balance end

    //closing balance start
    $closing_balance_price = $opening_balance_purchase[$array_order][0][0]["total_opening_price"]+$net_change_purchase_price-$net_change_sales_price-$net_change_purchase_return[$array_order][0][0]["total_opening_price"]+$net_change_sales_return[$array_order][0][0]["total_opening_price"];
    $closing_balance_quantity = $opening_balance_purchase[$array_order][0][0]["total_opening_quantity"]+$net_change_purchase_quantity-$net_change_sales_quantity-$net_change_purchase_return[$array_order][0][0]["total_opening_quantity"]+$net_change_sales_return[$array_order][0][0]["total_opening_quantity"];
    $closing_balance_rate = $closing_balance_price/$closing_balance_quantity;
    //closing balance end


        $coatyperows[]= array(
        $serial_no,
        $product["Orderdetail"]["transactiondate"],
        $product["Product"]["productcode"],
        $product["Product"]["productname"],
        $product["Producttype"]["producttypename"],
        $product["Productcategory"]["productcategoryname"],
        $opening_balance_purchase_quantity,
        round($opening_balance_purchase_rate),
        $opening_balance_purchase_price,
        $net_change_purchase_quantity,
        $net_change_purchase_rate,
        $net_change_purchase_price,
        $net_change_sales_quantity,
        $net_change_sales_rate,
        $net_change_sales_price,
        $closing_balance_quantity,
        round($closing_balance_rate,2),
        round($closing_balance_price,2),
        $product["Branch"]["branchname"],
    );


$serial_no++;
$array_order++;
endforeach;
$rows[]= array(array("Total","colspan='3'"),
    $sum_of_debit ,
    $sum_of_credit,
    ''
);
echo $this->Html->tableCells(
    $coatyperows
);
//echo $this->Html->tableCells(
//    $rows
//);
echo $Utilitys->tableend();
?>
