<?php
	echo "<h2>".__('Chart of Account'). $Utilitys->addurl($title=__("Add New Chart of Account"), $this->request["controller"], $page="coa")."</h2>";
	echo $Utilitys->filtertagstart();
		if($_SESSION["User"]["group_id"] == 1):
		echo $this->Form->input(
			"Coa.company_id",
			array(
				'type'=>'select',
				"options"=>array($company),
				'empty'=>__('Select Company'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'style'=>'',
				'class'=> 'form-control'
			)
		);
		endif;
		if($_SESSION["User"]["group_id"] == 1 || $_SESSION["User"]["group_id"] == 2 ):
		echo $this->Form->input(
			"Coa.branch_id",
			array(
				'type'=>'select',
				"options"=>array($branch),
				'empty'=>__('Select Branch'),
				'div'=>false,
				'tabindex'=>2,
				'label'=>false,
				'style'=>'',
				'class'=> 'form-control'
			)
		)
		."<span id=\"branchloading\"></span>";
		endif;
		echo $this->Form->input(
			"Coa.coatype_id",
			array(
				'type'=>'select',
				"options"=>array($coatype),
				'empty'=>__('Select Chart of Account Type'),
				'div'=>false,
				'tabindex'=>3,
				'label'=>false,
				'style'=>'',
				'class'=> 'form-control'
			)
		);
		echo $this->Form->input(
			"Coa.coagroup_id",
			array(
				'type'=>'select',
				"options"=>array($coagrouplist),
				'empty'=>__('Select Chart of Account Group'),
				'div'=>false,
				'tabindex'=>4,
				'label'=>false,
				'style'=>'',
				'class'=> 'form-control'
			)
		)
		."<span id=\"grouploading\"></span>";
		echo $this->Form->input(
			"Coa.coagroupname",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'tabindex'=>5,
				'style'=>'',
				'class'=> 'form-control'
			)
		);
		echo"<a href='#' id='gridview'>G</a> | <a href='#' id='listview'>L</a>";
	echo $Utilitys->filtertagend();
	echo"

		<div id=\"groupsarchloading\">
	";
			echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id=null);
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('Code')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA BN')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Type')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Group')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Class')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Company')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Branch')  => array('class' => 'highlight sortable')
						), 
						__('Active'), 
						__('Action')
					)
				);
				
				$coarows = array();
				$countCoa = 0-1+$this->Paginator->counter('%start%');
				
				foreach($coa as $Thiscoa):
					$countCoa++;
					$coarows[]= array($countCoa,$Thiscoa["Coa"]["coacode"],$Utilitys->amountDetailsURL($Thiscoa["Coa"]["coaname"], $Thiscoa["Coa"]["id"], "Gltransaction","coa_id"),$Thiscoa["Coa"]["coanamebn"],$Thiscoa["Coa"]["coatype_name"],$Thiscoa["Coa"]["coagroup_name"],$Thiscoa["Coa"]["coaclass_name"],$Thiscoa["Coa"]["company_name"],$Thiscoa["Coa"]["branch_name"],$Thiscoa["Coa"]["isActive"],$Utilitys->cusurl($this->request["controller"], 'coa', $Thiscoa["Coa"]["id"], $Thiscoa["Coa"]["coauuid"]));
				endforeach;
				echo $this->Html->tableCells(
					$coarows
				);
			echo $Utilitys->tableend();
			echo $Utilitys->paginationcommon();
	echo"
		</div>
		<script>
			$(document).ready(function() {
				$(\"#CoaCompanyId\").change(function(){
					var company_id = $(this).val();
					var coatype_id = $(\"#CoaCoatypeId\").val();
					var coagroup_id = $(\"#CoaCoagroupId\").val();
					if(coatype_id !='' && coatype_id == 0){
						coatype_id = coatype_id;
					}else{
						coatype_id = 0;
					}
					if(coagroup_id !='' && coagroup_id == 0){
						coagroup_id = coagroup_id;
					}else{
						coagroup_id = 0;
					}
					$(\"#branchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$(\"#groupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coasearchbytgcb/'+coatype_id+'/'+coagroup_id+'/'+company_id, {coatype_id:coatype_id,coagroup_id:coagroup_id,company_id:company_id},
					function(result) {
						$(\"#groupsarchloading\").html(result);
					});
					$.post('".$this->webroot."companies/listbranchbycompany/'+company_id, {company_id:company_id},
					function(result) {
						$(\"#CoaBranchId\").html(result);
						$(\"#branchloading\").html('');
					});
				});
				$(\"#CoaBranchId\").change(function(){
					var company_id = $(\"#CoaCompanyId\").val();
					var branch_id = $(this).val();
					var coatype_id = $(\"#CoaCoatypeId\").val();
					var coagroup_id = $(\"#CoaCoagroupId\").val();
					if(company_id !='' && company_id == 0){
						company_id = company_id;
					}else{
						company_id = 0;
					}
					if(coatype_id !='' && coatype_id == 0){
						coatype_id = coatype_id;
					}else{
						coatype_id = 0;
					}
					if(coagroup_id !='' && coagroup_id == 0){
						coagroup_id = coagroup_id;
					}else{
						coagroup_id = 0;
					}
					$(\"#groupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coasearchbytgcb/'+coatype_id+'/'+coagroup_id+'/'+company_id+'/'+branch_id, {coatype_id:coatype_id,coagroup_id:coagroup_id,company_id:company_id,branch_id:branch_id},
					function(result) {
						$(\"#groupsarchloading\").html(result);
					});
				});
				$(\"#CoaCoatypeId\").change(function(){
					var company_id = $(\"#CoaCompanyId\").val();
					var branch_id = $(\"CoaBranchId\").val();
					var coatype_id = $(this).val();
					if(company_id !='' && company_id == 0){
						company_id = company_id;
					}else{
						company_id = 0;
					}
					if(branch_id !='' && branch_id == 0){
						branch_id = branch_id;
					}else{
						branch_id = 0;
					}
					$(\"#groupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coasearchbytgcb/'+coatype_id, {coatype_id:coatype_id,company_id:company_id,branch_id:branch_id},
					function(result) {
						$(\"#groupsarchloading\").html(result);
					});
					$(\"#grouploading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/listcoagroup/'+coatype_id+'/0/'+company_id+'/'+branch_id, {coatype_id:coatype_id},
					function(result) {
						$(\"#CoagroupCoagroupId\").html(result);
						$(\"#grouploading\").html('');
					});
				});
				$(\"#CoaCoagroupId\").change(function(){
					var company_id = $(\"#CoaCompanyId\").val();
					var branch_id = $(\"#CoaBranchId\").val();
					var coatype_id = $(\"#CoaCoatypeId\").val();
					var coagroup_id = $(this).val();
					if(company_id !='' && company_id == 0){
						company_id = company_id;
					}else{
						company_id = 0;
					}
					if(branch_id !='' && branch_id == 0){
						branch_id = branch_id;
					}else{
						branch_id = 0;
					}
					if(coatype_id !='' && coatype_id == 0){
						coatype_id = coatype_id;
					}else{
						coatype_id = 0;
					}
					$(\"#groupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coasearchbytgcb/'+coatype_id+'/'+coagroup_id+'/'+company_id+'/'+branch_id, {coatype_id:coatype_id,coagroup_id:coagroup_id,company_id:company_id,branch_id:branch_id},
					function(result) {
						$(\"#groupsarchloading\").html(result);
					});
				});
				$(\"#CoaCoagroupname\").keyup(function(){
					var coaname = $(this).val();
					$(\"#groupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coasearchbytgcb/0/0/0/0/'+coaname, {coaname:coaname},
					function(result) {
						$(\"#groupsarchloading\").html(result);
					});
				});
				$(\"#gridview\").click(function(){
					var company_id = $(\"#CoaCompanyId\").val();
					var branch_id = $(\"#CoaBranchId\").val();
					var coatype_id = $(\"#CoaCoatypeId\").val();
					var coagroup_id = $(\"#CoaCoagroupId\").val();
					if(company_id !='' && company_id != 0){
						company_id = company_id;
					}else{
						company_id = 0;
					}
					if(branch_id !='' && branch_id != 0){
						branch_id = branch_id;
					}else{
						branch_id = 0;
					}
					if(coatype_id !='' && coatype_id != 0){
						coatype_id = coatype_id;
					}else{
						coatype_id = 0;
					}
					if(coagroup_id !='' && coagroup_id != 0){
						coagroup_id = coagroup_id;
					}else{
						coagroup_id = 0;
					}
					$(\"#groupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coasearchbytgcb/'+coatype_id+'/'+coagroup_id+'/'+company_id+'/'+branch_id, {coatype_id:coatype_id,coagroup_id:coagroup_id,company_id:company_id,branch_id:branch_id},
					function(result) {
						$(\"#groupsarchloading\").html(result);
					});
				});
				$(\"#listview\").click(function(){
					var company_id = $(\"#CoaCompanyId\").val();
					var branch_id = $(\"#CoaBranchId\").val();
					var coatype_id = $(\"#CoaCoatypeId\").val();
					var coagroup_id = $(\"#CoaCoagroupId\").val();
					if(company_id !='' && company_id == 0){
						company_id = company_id;
					}else{
						company_id = 0;
					}
					if(branch_id !='' && branch_id == 0){
						branch_id = branch_id;
					}else{
						branch_id = 0;
					}
					if(coatype_id !='' && coatype_id == 0){
						coatype_id = coatype_id;
					}else{
						coatype_id = 0;
					}
					if(coagroup_id !='' && coagroup_id != 0){
						coagroup_id = coagroup_id;
					}else{
						coagroup_id = 0;
					}
					$(\"#groupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coasearchbytgcblistview/'+coatype_id+'/'+coagroup_id+'/'+company_id+'/'+branch_id, {coatype_id:coatype_id,coagroup_id:coagroup_id,company_id:company_id,branch_id:branch_id},
					function(result) {
						$(\"#groupsarchloading\").html(result);
					});
				});
			});
		</script>
	";
?>
