<?php
	echo "<h2>".__('Chart of Account Sub Class'). $Utilitys->addurl($title=__("Add New Chart of Account Sub Class"), $this->request["controller"], $page="coaclasssubclass")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Coaclasssubclass.coatype_id",
			array(
				'type'=>'select',
				"options"=>array($coatype),
				'empty'=>__('Select Chart of Account Type'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'style'=>'',
				'class'=> 'form-control'
			)
		)
		.$this->Form->input(
			"Coaclasssubclass.coagroup_id",
			array(
				'type'=>'select',
				"options"=>array($coagrouplist),
				'empty'=>__('Select Chart of Account Group'),
				'div'=>false,
				'tabindex'=>2,
				'label'=>false,
				'style'=>'',
				'class'=> 'form-control'
			)
		)
		.$this->Form->input(
			"Coaclasssubclass.coaclass_id",
			array(
				'type'=>'select',
				"options"=>array($coaclasslist),
				'empty'=>__('Select Chart of Account Class'),
				'div'=>false,
				'tabindex'=>3,
				'label'=>false,
				'style'=>'',
				'class'=> 'form-control'
			)
		)
		.$this->Form->input(
			"Coaclasssubclass.coaclasssubclass_id",
			array(
				'type'=>'select',
				"options"=>array($coaclasssubclasslist),
				'empty'=>__('Select Chart of Account Sub Class'),
				'div'=>false,
				'tabindex'=>4,
				'label'=>false,
				'style'=>'',
				'class'=> 'form-control'
			)
		)
		."<span id=\"subclassloading\"></span>"
		.$this->Form->input(
			"Coaclasssubclass.coasubclassname",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'tabindex'=>5,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control'
			)
		);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"subclasssearchloading\">
	";
			echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id=null);
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('COA Type')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA Group')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA Class')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA Sub Class')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA Sub Class BN')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA Sub Class Code')  => array('class' => 'highlight sortable')
						), 
						__('Active'), 
						__('Action')
					)
				);
				$coaclasssubclassrows = array();
				$countCoaclasssubclass = 0-1+$this->Paginator->counter('%start%');
				foreach($coaclasssubclass as $Thiscoaclasssubclass):
					$countCoaclasssubclass++;
					$coaclasssubclassrows[]= array($countCoaclasssubclass,
						$Thiscoaclasssubclass["Coatype"]["coatypename"]."/".$Thiscoaclasssubclass["Coatype"]["coatypenamebn"],
						$Thiscoaclasssubclass["Coagroup"]["coagroupname"]."/".$Thiscoaclasssubclass["Coagroup"]["coagroupnamebn"],
						$Thiscoaclasssubclass["Coaclass"]["coaclassname"]."/".$Thiscoaclasssubclass["Coaclass"]["coaclassnamebn"],
						$Thiscoaclasssubclass["Coaclasssubclass"]["coaclasssubclassname"],
						$Thiscoaclasssubclass["Coaclasssubclass"]["coaclasssubclassnamebn"],
						$Thiscoaclasssubclass["Coaclasssubclass"]["coaclasssubclasscode"],
						$Utilitys->statusURL($this->request["controller"], 'coaclasssubclass', $Thiscoaclasssubclass["Coaclasssubclass"]["id"], $Thiscoaclasssubclass["Coaclasssubclass"]["coaclasssubclassuuid"], $Thiscoaclasssubclass["Coaclasssubclass"]["coaclasssubclassisactive"]),
						$Utilitys->cusurl($this->request["controller"], 'coaclasssubclass', $Thiscoaclasssubclass["Coaclasssubclass"]["id"], $Thiscoaclasssubclass["Coaclasssubclass"]["coaclasssubclassuuid"]));
				endforeach;
				echo $this->Html->tableCells(
					$coaclasssubclassrows
				);
			echo $Utilitys->tableend();
			echo $Utilitys->paginationcommon();
	echo"
		</div>
		<script>
			$(document).ready(function() {


				$(\"#CoaclasssubclassCoatypeId\").change(function(){
					var coatype_id = $(this).val();
					$(\"#subclasssearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coaclasssubclasssearchbytext/'+coatype_id, {coatype_id:coatype_id},
					function(result) {
						$(\"#subclasssearchloading\").html(result);
					});
					$(\"#subclassloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/listcoagroup/'+coatype_id, {coatype_id:coatype_id},
					function(result) {
						$(\"#CoaclasssubclassCoagroupId\").html(result);
						$(\"#subclassloading\").html('');
					});
				});


				$(\"#CoaclasssubclassCoagroupId\").change(function(){
					var coatype_id = $(\"#CoaclasssubclassCoatypeId\").val();
					var coagroup_id = $(\"#CoaclasssubclassCoagroupId\").val();
					if(coatype_id !='' && coatype_id != 0){
						coatype_id = coatype_id;
					}else{
						coatype_id = 0;
					}
					$(\"#subclasssearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coaclasssubclasssearchbytext/'+coatype_id+'/'+coagroup_id, {coatype_id:coatype_id,coagroup_id:coagroup_id},
					function(result) {
						$(\"#subclasssearchloading\").html(result);
					});
					$(\"#subclassloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/listcoaclass/'+coatype_id+'/'+coagroup_id, {coatype_id:coatype_id, coagroup_id:coagroup_id},
					function(result) {
						$(\"#CoaclasssubclassCoaclassId\").html(result);
						$(\"#subclassloading\").html('');
					});
				});

				$(\"#CoaclasssubclassCoaclassId\").change(function(){
					var coatype_id = $(\"#CoaclasssubclassCoatypeId\").val();
					var coagroup_id = $(\"#CoaclasssubclassCoagroupId\").val();
					var coaclass_id = $(\"#CoaclasssubclassCoaclassId\").val();

					if(coatype_id !='' && coatype_id != 0){
						coatype_id = coatype_id;
					}else{
						coatype_id = 0;
					}
					if(coagroup_id !='' && coagroup_id != 0){
						coagroup_id = coagroup_id;
					}else{
						coagroup_id = 0;
					}
					if(coaclass_id !='' && coaclass_id != 0){
						coaclass_id = coaclass_id;
					}else{
						coaclass_id = 0;
					}
					$(\"#subclasssearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coaclasssubclasssearchbytext/'+coatype_id+'/'+coagroup_id+'/'+coaclass_id,
					 {coatype_id:coatype_id,coagroup_id:coagroup_id,coaclass_id:coaclass_id},
					function(result) {
						$(\"#subclasssearchloading\").html(result);
					});
					$(\"#subclassloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/listcoasubclass/'+coatype_id+'/'+coagroup_id+'/'+coaclass_id, {coatype_id:coatype_id, coagroup_id:coagroup_id, coaclass_id:coaclass_id},
					function(result) {
						$(\"#CoaclasssubclassCoaclasssubclassId\").html(result);
						$(\"#subclassloading\").html('');
					});
				});

				$(\"#CoaclasssubclassCoaclasssubclassId\").change(function(){
					var coatype_id = $(\"#CoaclasssubclassCoatypeId\").val();
					var coagroup_id = $(\"#CoaclasssubclassCoagroupId\").val();
					var coaclass_id = $(\"#CoaclasssubclassCoaclassId\").val();
					var coasubclass_id = $(\"#CoaclasssubclassCoaclasssubclassId\").val();
					if(coatype_id !='' && coatype_id != 0){
						coatype_id = coatype_id;
					}else{
						coatype_id = 0;
					}
					if(coagroup_id !='' && coagroup_id != 0){
						coagroup_id = coagroup_id;
					}else{
						coagroup_id = 0;
					}
					if(coaclass_id !='' && coaclass_id != 0){
						coaclass_id = coaclass_id;
					}else{
						coaclass_id = 0;
					}
					if(coaclasssubclass_id !='' && coaclasssubclass_id != 0){
						coaclasssubclass_id = coaclasssubclass_id;
					}else{
						coaclasssubclass_id = 0;
					}
					$(\"#subclasssearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coaclasssubclasssearchbytext/'+coatype_id+'/'+coagroup_id+'/'+coaclass_id+'/'+coasubclass_id,
					 {coatype_id:coatype_id,coagroup_id:coagroup_id,coaclass_id:coaclass_id,coasubclass_id:coasubclass_id},
					function(result) {
						$(\"#subclasssearchloading\").html(result);
					});
				});



				$(\"#CoaclasssubclassCoasubclassname\").keyup(function(){
					var subclassname = $(this).val();
					var subclasscode = $(this).val();
					$(\"#subclasssearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coaclasssubclasssearchbytext/0/0/0/0/'+subclassname+'/'+subclasscode, {subclassname:subclassname, subclasscode:subclasscode},
					function(result) {
						$(\"#subclasssearchloading\").html(result);
					});
				});
			});
		</script>
	";
?>
