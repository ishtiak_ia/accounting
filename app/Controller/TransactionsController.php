<?php
App::import('Controller', 'Logins');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');
class TransactionsController extends AppController {
	public $name = 'Transactions';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'Form' => array('className' => 'BootstrapForm'));
	public $components = array('RequestHandler','Session');
	public $uses = array('Transactiontype', 'User', 'Gltransaction', 'Banktransaction', 'Cashtransaction','Usertransaction', 'Producttransaction', 'Orderdetail','Journaltransaction', 'Institutetransaction');
	var $Logins;

	function beforeFilter(){
		$this->Logins =& new LoginsController;
		$this->Logins->constructClasses();
		$this->Logins->__validateLoginStatus();
		$Logins = new LoginsController;
		$this->set('Logins',$Logins);
		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	//Start Transaction Type//
	
	public function transactiontypemanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Transaction Type').' | '.__(Configure::read('site_name')));

		$this->paginate = array(
			'fields' => 'Transactiontype.*',
			'order'  =>'Transactiontype.id ASC',
			'limit' => 20
		);
		$transactiontype = $this->paginate('Transactiontype');
		$this->set('transactiontype',$transactiontype);
	}

	public function transactiontypesearchbytext($Searchtext=null){
		$this->layout='ajax';
		$Searchtext = @$this->request->data['Searchtext'];
		$transactiontype_id = array();
		if(!empty($Searchtext) && $Searchtext!=''):
			$transactiontype_id[] = array(
				'OR' => array(
					'Transactiontype.transactiontypename LIKE ' => '%'.$Searchtext.'%',
					'Transactiontype.transactiontypenamebn LIKE ' => '%'.$Searchtext.'%'
				)
			);
		endif;

		$this->paginate = array(
			'fields' => 'Transactiontype.*',
			'order'  =>'Transactiontype.transactiontypename ASC',
			"conditions"=>$transactiontype_id,
			'limit' => 20
		);
		$transactiontype = $this->paginate('Transactiontype');
		$this->set('transactiontype',$transactiontype);
	}

	public function transactiontypedetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$transactiontype=$this->Transactiontype->find('all',array("fields" => "Transactiontype.*","conditions"=>array('Transactiontype.id'=>$id,'Transactiontype.transactiontypeuuid'=>$uuid)));
		$this->set('transactiontype',$transactiontype);
	}

	public function transactiontypeinsertupdate($id=null,$uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Transaction Type').' | '.__(Configure::read('site_name')));
		$transactiontypelist=$this->Transactiontype->find('list',array("fields" => array("id","transactiontype_name")));
		$this->set('transactiontypelist',$transactiontypelist);
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
			$transactiontype=$this->Transactiontype->find('all',array("fields" => "Transactiontype.*","conditions"=>array('Transactiontype.id'=>$id,'Transactiontype.transactiontypeuuid'=>$uuid)));
			$this->set('transactiontype',$transactiontype);
		endif;
	}

	public function  transactiontypeinsertupdateaction($id=null,$uuid=null){
		$userID = $_SESSION["User"]["id"];
		$id = @$this->request->data['Transactions']['id'];
		$uuid = @$this->request->data['Transactions']['transactiontypeuuid'];
		$transactiontypename = $this->request->data['Transactiontype']['transactiontypename'];
		$transactiontypenamebn = $this->request->data['Transactiontype']['transactiontypenamebn'];
		$counttransactiontypename=$this->Transactiontype->find('all',array("fields" => "Transactiontype.*","conditions"=>array('Transactiontype.transactiontypename'=>$transactiontypename,'Transactiontype.transactiontypenamebn'=>$transactiontypenamebn)));
		//echo count($countbankaccount);
		//pr($_POST);exit();
		if(count($counttransactiontypename)>0):
			$this->Session->setFlash(__("This Transactiontype Type Already Exists"), 'default', array('class' => 'error'));
			$this->redirect("/transactions/transactiontypeinsertupdate");
		else:
			if ($this->request->is('post')) {
				if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
					$this->request->data['Transactiontype']['transactiontypeupdateid']=$userID;
					$this->request->data['Transactiontype']['transactiontypeupdatedate']=date('Y-m-d H:i:s');
					$message = __('Update Successfully.');
					$this->Transactiontype->id = $id;
				else:
					$this->request->data['Transactiontype']['transactiontypeinsertid']=$userID;
					$this->request->data['Transactiontype']['transactiontypeinsertdate']=date('Y-m-d H:i:s');
					$this->request->data['Transactiontype']['transactiontypeuuid']=String::uuid();
					$message = __('Save Successfully.');
				endif;
				if ($this->Transactiontype->save($this->request->data, array('validate' => 'only'))) {
					$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
					$this->redirect("/transactions/transactiontypemanage");
				}else{
					$errors = $this->Transactiontype->invalidFields();
					$this->set('errors', $this->Transactiontype->invalidFields());
					$this->redirect("/transactions/transactiontypeinsertupdate");
				}
			}
		endif;
	}

	public function transactiontypedelete($id=null,$uuid=null){
		$userID = $_SESSION["User"]["id"];
		if ($this->Transactiontype->updateAll(array('transactiontypedeleteid'=>$userID, 'transactiontypedeletedate'=>"'".date('Y-m-d H:i:s')."'", 'transactiontypeisactive'=>2), array('AND' => array('Transactiontype.id'=>$id,'Transactiontype.transactiontypeuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/transactions/transactiontypemanage");
	}

	//End Transaction Type//

	//Start General Ledger Insert Update Portation//

	public function gltransaction($transaction_id, $transactiontype_id, $transactionamount, $coa_id, $clientsuppler_id, $group_id, $product_id, $transactiondate, $voucher_no, $isActive, $step=0){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		//$group_id=$_SESSION["User"]["group_id"];
		$userID = $_SESSION["User"]["id"];

		$this->request->data['Gltransaction']['transaction_id'] = $transaction_id;
		$this->request->data['Gltransaction']['transactiontype_id'] = $transactiontype_id;
		$this->request->data['Gltransaction']['transactionamount'] = $transactionamount;
		$this->request->data['Gltransaction']['transactiondate'] = $transactiondate;
		$this->request->data['Gltransaction']['voucher_no'] = $voucher_no;
		$this->request->data['Gltransaction']['coa_id'] = $coa_id;
		$this->request->data['Gltransaction']['user_id'] = $clientsuppler_id;
        $this->request->data['Gltransaction']['group_id'] = $group_id;
		$this->request->data['Gltransaction']['product_id'] = $product_id;
		$this->request->data['Gltransaction']['gltransactionisactive'] = $isActive;

		$this->request->data['Gltransaction']['company_id'] = $company_id;
		$this->request->data['Gltransaction']['branch_id'] = $branch_id;
		$this->request->data['Gltransaction']['gltransactionstep'] = $step;
		$this->request->data['Gltransaction']['gltransactioninsertid']=$userID;
		$this->request->data['Gltransaction']['gltransactioninsertdate']=date('Y-m-d H:i:s');
		$this->request->data['Gltransaction']['gltransactionuuid']=String::uuid();

		$gltransaction=$this->Gltransaction->find('count',array("fields" => "Gltransaction.transaction_id","conditions"=>array('Gltransaction.transaction_id'=>$transaction_id,'Gltransaction.transactiontype_id'=>$transactiontype_id,'Gltransaction.product_id'=>$product_id, 'Gltransaction.gltransactionstep'=>$step)));
		$this->Gltransaction->id = 0;
		if($gltransaction>0):
			$this->Gltransaction->updateAll(
				array(
					'transactionamount'=>$transactionamount, 
					'transactiondate'=>"'".$transactiondate."'", 
					'coa_id'=>$coa_id,
					'user_id'=>$clientsuppler_id,
					'product_id'=>$product_id,
					'company_id'=>$company_id,
					'branch_id'=>$branch_id,
					'gltransactionstep'=>$step,
					'gltransactionupdateid'=>$userID,
					'gltransactionupdatedate'=>"'".date('Y-m-d H:i:s')."'",
				), 
				array(
					'AND' => array(
						'Gltransaction.transaction_id'=>$transaction_id,
						'Gltransaction.transactiontype_id'=>$transactiontype_id,
						'Gltransaction.product_id'=>$product_id,
						'Gltransaction.gltransactionstep'=>$step,
					)
				)
			);
		else:
			$this->Gltransaction->save($this->request->data);
		endif;
	}

	//Delete General Ledger Insert Update Portation//

	//Start Cash Insert Update Portation//
	public function cashtransaction($transaction_id, $transactiontype_id, $transactionamount, $coa_id, $clientsuppler_id, $group_id, $product_id, $transactiondate, $isActive){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$userID = $_SESSION["User"]["id"];

		$this->request->data['Cashtransaction']['transaction_id'] = $transaction_id;
		$this->request->data['Cashtransaction']['transactiontype_id'] = $transactiontype_id;
		$this->request->data['Cashtransaction']['transactionamount'] = $transactionamount;
		$this->request->data['Cashtransaction']['transactiondate'] = $transactiondate;
		$this->request->data['Cashtransaction']['coa_id'] = $coa_id;
		$this->request->data['Cashtransaction']['user_id'] = $clientsuppler_id;
                $this->request->data['Cashtransaction']['group_id'] = $group_id;
		$this->request->data['Cashtransaction']['product_id'] = $product_id;
		$this->request->data['Cashtransaction']['cashtransactionisactive'] = $isActive;

		$this->request->data['Cashtransaction']['company_id'] = $company_id;
		$this->request->data['Cashtransaction']['branch_id'] = $branch_id;

		$this->request->data['Cashtransaction']['cashtransactioninsertid']=$userID;
		$this->request->data['Cashtransaction']['cashtransactioninsertdate']=date('Y-m-d H:i:s');
		$this->request->data['Cashtransaction']['cashtransactionuuid']=String::uuid();

		$cashtransaction=$this->Cashtransaction->find('count',array("fields" => "Cashtransaction.transaction_id","conditions"=>array('Cashtransaction.transaction_id'=>$transaction_id,'Cashtransaction.transactiontype_id'=>$transactiontype_id,'Cashtransaction.coa_id'=>$coa_id, 'Cashtransaction.product_id'=>$product_id)));
		$this->Cashtransaction->id = 0;
		if($cashtransaction>0):
			$this->Cashtransaction->updateAll(
				array(
					'transactionamount'=>$transactionamount, 
					'transactiondate'=>"'".$transactiondate."'", 
					'coa_id'=>$coa_id,
					'user_id'=>$clientsuppler_id,
                                        'group_id'=>$group_id,
					'product_id'=>$product_id,
					'company_id'=>$company_id,
					'branch_id'=>$branch_id,
					'cashtransactionupdateid'=>$userID,
					'cashtransactionupdatedate'=>"'".date('Y-m-d H:i:s')."'",
				), 
				array(
					'AND' => array(
						'Cashtransaction.transaction_id'=>$transaction_id,
						'Cashtransaction.transactiontype_id'=>$transactiontype_id,
						'Cashtransaction.coa_id'=>$coa_id,
						'Cashtransaction.product_id'=>$product_id
					)
				)
			);
		else:
			$this->Cashtransaction->save($this->request->data);
		endif;
	}

	//End Cash Insert Update Portation//

	//Start User Insert Update Portation//

	public function usertransaction($transaction_id, $transactiontype_id, $transactionamount, $coa_id, $clientsuppler_id, $clientsupplergroup_id, $transactiondate,  $isActive, $step=0){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$userID = $_SESSION["User"]["id"];

		$this->request->data['Usertransaction']['transaction_id'] = $transaction_id;
		$this->request->data['Usertransaction']['transactiontype_id'] = $transactiontype_id;
		$this->request->data['Usertransaction']['transactionamount'] = $transactionamount;
		$this->request->data['Usertransaction']['transactiondate'] = $transactiondate;
		$this->request->data['Usertransaction']['coa_id'] = $coa_id;
		$this->request->data['Usertransaction']['user_id'] = $clientsuppler_id;
		$this->request->data['Usertransaction']['group_id'] = $clientsupplergroup_id;
		$this->request->data['Usertransaction']['usertransactionisactive'] = $isActive;
		$this->request->data['Usertransaction']['usertransactionstep'] = $step;

		$this->request->data['Usertransaction']['company_id'] = $company_id;
		$this->request->data['Usertransaction']['branch_id'] = $branch_id;

		$this->request->data['Usertransaction']['usertransactioninsertid']=$userID;
		$this->request->data['Usertransaction']['usertransactioninsertdate']=date('Y-m-d H:i:s');
		$this->request->data['Usertransaction']['usertransactionuuid']=String::uuid();


		$usertransaction=$this->Usertransaction->find('count',array("fields" => "Usertransaction.transaction_id","conditions"=>array('Usertransaction.transaction_id'=>$transaction_id,'Usertransaction.transactiontype_id'=>$transactiontype_id, 'Usertransaction.usertransactionstep'=>$step)));
		$this->Usertransaction->id = 0;
		if($usertransaction>0):
			$this->Usertransaction->updateAll(
				array(
					'transactionamount'=>$transactionamount, 
					'transactiondate'=>"'".$transactiondate."'", 
					'coa_id'=>$coa_id,
					'user_id'=>$clientsuppler_id,
					'group_id'=>$clientsupplergroup_id,
					'company_id'=>$company_id,
					'branch_id'=>$branch_id,
					'usertransactionstep'=>$step,
					'usertransactionupdateid'=>$userID,
					'usertransactionupdatedate'=>"'".date('Y-m-d H:i:s')."'",
				), 
				array(
					'AND' => array(
						'Usertransaction.transaction_id'=>$transaction_id,
						'Usertransaction.transactiontype_id'=>$transactiontype_id,
						'Usertransaction.usertransactionstep'=>$step,
					)
				)
			);
		else:
			$this->Usertransaction->save($this->request->data);
		endif;
	}

	//Delete User Insert Update Portation//

	//Start Product Insert Update Portation//

	public function producttransaction($transaction_id, $transactiontype_id, $transactionamount, $coa_id, $product_id, $isActive){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$userID = $_SESSION["User"]["id"];

		$this->request->data['Producttransaction']['transaction_id'] = $transaction_id;
		$this->request->data['Producttransaction']['transactiontype_id'] = $transactiontype_id;
		$this->request->data['Producttransaction']['transactionamount'] = $transactionamount;
		$this->request->data['Producttransaction']['coa_id'] = $coa_id;
		$this->request->data['Producttransaction']['product_id'] = $product_id;
		$this->request->data['Producttransaction']['producttransactionisactive'] = $isActive;

		$this->request->data['Producttransaction']['company_id'] = $company_id;
		$this->request->data['Producttransaction']['branch_id'] = $branch_id;

		$this->request->data['Usertransaction']['producttransactioninsertid']=$userID;
		$this->request->data['Usertransaction']['producttransactioninsertdate']=date('Y-m-d H:i:s');
		$this->request->data['Producttransaction']['producttransactionuuid']=String::uuid();

		$producttransaction=$this->Producttransaction->find('count',array("fields" => "Producttransaction.transaction_id","conditions"=>array('Producttransaction.transaction_id'=>$transaction_id,'Producttransaction.transactiontype_id'=>$transactiontype_id)));
		$this->Producttransaction->id = 0;
		if($producttransaction>0):
			$this->Producttransaction->updateAll(
				array(
					'transactionamount'=>$transactionamount, 
					'coa_id'=>$coa_id,
					'user_id'=>$clientsuppler_id,
					'product_id'=>$product_id,
					'company_id'=>$company_id,
					'branch_id'=>$branch_id,
					'producttransactionupdateid'=>$userID,
					'producttransactionupdatedate'=>"'".date('Y-m-d H:i:s')."'",
				), 
				array(
					'AND' => array(
						'Producttransaction.transaction_id'=>$transaction_id,
						'Producttransaction.transaction_id'=>$transactiontype_id
					)
				)
			);
		else:
			$this->Producttransaction->save($this->request->data);
		endif;
	}

	//Delete Product Insert Update Portation//

	//Start Bank Insert Update Portation//

	public function banktransaction($bank_id, $bankbranch_id, $bankaccounttype_id, $bankaccount_id, $bankchequebook_id, $chequebookseries, $chequesequencynumber, $banktransactiondate, $banktransactionothernote, $transaction_id, $transactiontype_id, $transactionamount, $coa_id, $clientsuppler_id, $group_id, $salesman_id, $product_id, $banktansaction_id, $isActive){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$userID = $_SESSION["User"]["id"];

		$this->request->data['Banktransaction']['bank_id'] = $bank_id;
		$this->request->data['Banktransaction']['bankbranch_id'] = $bankbranch_id;
		$this->request->data['Banktransaction']['bankaccounttype_id'] = $bankaccounttype_id;
		$this->request->data['Banktransaction']['bankaccount_id'] = $bankaccount_id;
		$this->request->data['Banktransaction']['bankchequebook_id'] = $bankchequebook_id;
		$this->request->data['Banktransaction']['banktransactionchequebookseries'] = $chequebookseries;
		$this->request->data['Banktransaction']['banktransactionchequesequencynumber'] = $chequesequencynumber;
		$this->request->data['Banktransaction']['transactiondate'] = $banktransactiondate;
		$this->request->data['Banktransaction']['banktransactionothernote'] = $banktransactionothernote;
		$this->request->data['Banktransaction']['transactionamount'] = $transactionamount;
		$this->request->data['Banktransaction']['salesman_id'] = $salesman_id;
		$this->request->data['Banktransaction']['transaction_id'] = $transaction_id;
		$this->request->data['Banktransaction']['transactiontype_id'] = $transactiontype_id;
		$this->request->data['Banktransaction']['coa_id'] = $coa_id;
		$this->request->data['Banktransaction']['user_id'] = $clientsuppler_id;
                $this->request->data['Banktransaction']['group_id'] = $group_id;
		$this->request->data['Banktransaction']['banktransactionisactive'] = $isActive;

		$this->request->data['Banktransaction']['company_id'] = $company_id;
		$this->request->data['Banktransaction']['branch_id'] = $branch_id;

		$this->request->data['Banktransaction']['banktransactioninsertid']=$userID;
		$this->request->data['Banktransaction']['banktransactioninsertdate']=date('Y-m-d H:i:s');
		$this->request->data['Banktransaction']['banktransactionuuid']=String::uuid();
		if($_SESSION["ST_PURCHASEORDER"]==$transactiontype_id):
			$conditionarray=array('Banktransaction.id'=>$banktansaction_id);
		else:
			$conditionarray=array('Banktransaction.transaction_id'=>$transaction_id,'Banktransaction.transactiontype_id'=>$transactiontype_id);
		endif;

		$banktransaction=$this->Banktransaction->find('count',array("fields" => "Banktransaction.transaction_id","conditions"=>array($conditionarray)));
		if($banktransaction>0):
			$this->Banktransaction->updateAll(
				array(
					'bank_id'=>$bank_id, 
					'bankbranch_id'=>$bankbranch_id, 
					'bankaccounttype_id'=>$bankaccounttype_id, 
					'bankaccount_id'=>$bankaccount_id, 
					'bankchequebook_id'=>$bankchequebook_id, 
					'banktransactionchequebookseries'=>"'".trim($chequebookseries)."'", 
					'banktransactionchequesequencynumber'=>trim($chequesequencynumber), 
					'transactiondate'=>"'".$banktransactiondate."'", 
					'transaction_id'=>$transaction_id, 
					'transactiontype_id'=>$transactiontype_id, 
					'banktransactionothernote'=>$banktransactionothernote, 
					'transactionamount'=>$transactionamount, 
					'salesman_id'=>$salesman_id,
					'coa_id'=>$coa_id,
					'user_id'=>$clientsuppler_id,
                                        'group_id'=>$group_id,
					'company_id'=>$company_id,
					'branch_id'=>$branch_id,
					'banktransactionupdateid'=>$userID,
					'banktransactionupdatedate'=>"'".date('Y-m-d H:i:s')."'",
					'banktransactionisactive'=>$isActive
				), 
				array(
					'AND' => array(
						$conditionarray
					)
				)
			);
		else:
			$this->Banktransaction->save($this->request->data);
		endif;
	}


	//Delete Bank Insert Update Portation//

	//Start Journal Portation//
	public function journaltransaction($bank_id, $bankbranch_id, $bankaccounttype_id, $bankaccount_id, $journaltransactiontype_id, $transactiontype_id, $transaction_id, $coa_id, $particulars, $cf_code, $description, $voucher_no, $user_id, $group_id, $salesman_id, $bankchequebook_id, $journaltransactionchequebookseries, $journaltransactionchequesequencynumber, $transactiondate, $transactionamount, $journaltransactionothernote, $isActive, $step=0){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		//$group_id=$_SESSION["User"]["group_id"];
		$userID = $_SESSION["User"]["id"];

		$this->request->data['Journaltransaction']['bank_id'] = $bank_id;
		$this->request->data['Journaltransaction']['bankbranch_id'] = $bankbranch_id;
		$this->request->data['Journaltransaction']['bankaccounttype_id'] = $bankaccounttype_id;
		$this->request->data['Journaltransaction']['bankaccount_id'] = $bankaccount_id;
		$this->request->data['Journaltransaction']['journaltransactiontype_id'] = $journaltransactiontype_id;
		$this->request->data['Journaltransaction']['transactiontype_id'] = $transactiontype_id;
		$this->request->data['Journaltransaction']['transaction_id'] = $transaction_id;
		$this->request->data['Journaltransaction']['coa_id'] = $coa_id;
		$this->request->data['Journaltransaction']['particulars'] = $particulars;
		$this->request->data['Journaltransaction']['cf_code'] = $cf_code;
		$this->request->data['Journaltransaction']['description'] = $description;
		$this->request->data['Journaltransaction']['voucher_no'] = $voucher_no;
		$this->request->data['Journaltransaction']['company_id'] = $company_id;
		$this->request->data['Journaltransaction']['branch_id'] = $branch_id;
		$this->request->data['Journaltransaction']['user_id'] = $user_id;
		$this->request->data['Journaltransaction']['group_id'] = $group_id;
		$this->request->data['Journaltransaction']['salesman_id'] = $salesman_id;
		$this->request->data['Journaltransaction']['bankchequebook_id'] = $bankchequebook_id;
		$this->request->data['Journaltransaction']['journaltransactionchequebookseries'] = $journaltransactionchequebookseries;
		$this->request->data['Journaltransaction']['journaltransactionchequesequencynumber'] = $journaltransactionchequesequencynumber;
		$this->request->data['Journaltransaction']['transactiondate'] = $transactiondate;
		$this->request->data['Journaltransaction']['transactionamount'] = $transactionamount;
		$this->request->data['Journaltransaction']['journaltransactionothernote'] = $journaltransactionothernote;

		$this->request->data['Journaltransaction']['journaltransactionstep']=$step;
		$this->request->data['Journaltransaction']['journaltransactionisactive']=$isActive;
		$this->request->data['Journaltransaction']['journaltransactioninsertid']=$userID;
		$this->request->data['Journaltransaction']['journaltransactioninsertdate']=date('Y-m-d H:i:s');
		$this->request->data['Journaltransaction']['journaltransactionuuid']=String::uuid();

		// if($_SESSION["ST_JOURNAL"]==$transactiontype_id){
		// 	$conditionarray=array('Journaltransaction.id'=>$banktansaction_id);
		// } else {
		// 	$conditionarray=array('Journaltransaction.transaction_id'=>$transaction_id,'Journaltransaction.transactiontype_id'=>$transactiontype_id);
		// }
		$journaltransaction=$this->Journaltransaction->find('count',array("fields" => "Journaltransaction.transaction_id","conditions"=>array('Journaltransaction.transaction_id'=>$transaction_id,'Journaltransaction.transactiontype_id'=>$transactiontype_id, 'Journaltransaction.journaltransactionstep'=>$step)));
        $this->Journaltransaction->id = 0;
		if($journaltransaction > 0){
			$this->Journaltransaction->updateAll(
				array(
					'bank_id'=>$bank_id, 
					'bankbranch_id'=>$bankbranch_id, 
					'bankaccounttype_id'=>$bankaccounttype_id, 
					'bankaccount_id'=>$bankaccount_id, 
					'bankchequebook_id'=>$bankchequebook_id, 
					//'journaltransactionchequebookseries'=>"'".trim($chequebookseries)."'", 
					//'journaltransactionchequesequencynumber'=>trim($chequesequencynumber), 
					'particulars'=>"'".$particulars."'", 
					'cf_code'=>"'".$cf_code."'", 
					'description'=>"'".$description."'", 
					'voucher_no'=>"'".$voucher_no."'",
					'transactiondate'=>"'".$transactiondate."'", 
					'transaction_id'=>$transaction_id, 
					'transactiontype_id'=>$transactiontype_id, 
					'journaltransactionothernote'=>$journaltransactionothernote, 
					'transactionamount'=>$transactionamount, 
					'salesman_id'=>$salesman_id,
					'coa_id'=>$coa_id,
					'user_id'=>$user_id,
					'company_id'=>$company_id,
					'branch_id'=>$branch_id,
					'journaltransactionstep'=>$step,
					'journaltransactionupdateid'=>$userID,
					'journaltransactionupdatedate'=>"'".date('Y-m-d H:i:s')."'",
					'journaltransactionisactive'=>$isActive
				), 
				array(
					'AND' => array(
						'Journaltransaction.transaction_id'=>$transaction_id,
						'Journaltransaction.transactiontype_id'=>$transactiontype_id,
						'Journaltransaction.journaltransactionstep'=>$step
					)
				)
			);
		} else{
			$this->Journaltransaction->save($this->request->data);
		}

	}
	//End Journal Portation//

	//Start Purchase Details Portation//

	public function purchasdetailstransaction($product_id, $company_id, $branch_id, $transaction_id, $transactiontype_id, $coa_id, $quantity, $unit_id, $discount, $orderdetailmon, $orderdetailkg, $orderdetailgram, $orderdetailmonunitprice, $orderdetailkgunitprice, $orderdetailgramunitprice, $transactionamount, $clientsuppler_id, $transactiondate, $paymentType, $user_id, $isActive, $orderdetailID=null){

		$group_id=$_SESSION["User"]["group_id"];
		$userID = $_SESSION["User"]["id"];

		$this->request->data['Orderdetail']['transaction_id']=$transaction_id;
		$this->request->data['Orderdetail']['company_id']=$company_id;
		$this->request->data['Orderdetail']['branch_id']=$branch_id;
		$this->request->data['Orderdetail']['orderdetailuuid']=String::uuid();
		$this->request->data['Orderdetail']['product_id'] = $product_id;
		$this->request->data['Orderdetail']['transactiontype_id'] = $transactiontype_id;
		$this->request->data['Orderdetail']['coa_id'] = $coa_id;
		$this->request->data['Orderdetail']['quantity'] = $quantity;
		$this->request->data['Orderdetail']['unit_id'] = $unit_id;
		$this->request->data['Orderdetail']['discount'] = $discount;
		$this->request->data['Orderdetail']['orderdetailmon'] = $orderdetailmon;
		$this->request->data['Orderdetail']['orderdetailkg'] = $orderdetailkg;
		$this->request->data['Orderdetail']['orderdetailgram'] = $orderdetailgram;
		$this->request->data['Orderdetail']['transactionamount'] = $transactionamount;
		$this->request->data['Orderdetail']['transactiondate'] = "{$transactiondate}";
		$this->request->data['Orderdetail']['orderdetailinsertdate'] = "{$transactiondate}";
		$this->request->data['Orderdetail']['orderdetailinsertid'] = $user_id;
		$this->request->data['Orderdetail']['orderdetailisactive']=$isActive;

		
		$this->request->data['Orderdetail']['orderdetailmonunitprice'] = $orderdetailmonunitprice;
		$this->request->data['Orderdetail']['orderdetailkgunitprice'] = $orderdetailkgunitprice;
		$this->request->data['Orderdetail']['orderdetailgramunitprice'] = $orderdetailgramunitprice;
		
		$this->Orderdetail->save($this->data);
		return $this->Orderdetail->id = $this->Orderdetail->getLastInsertId();
		//pr($this->data);exit();
	}

	//End Purchase Details Portation//

	//Start Institute Portation//
	public function institutetransaction($transaction_id, $transactiontype_id, $transactionamount, $coa_id, $user_id, $institute_id, $forma_id, $donationdetail_id, $transactiondate,  $isActive){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$userID = $_SESSION["User"]["id"];

		$this->request->data['Institutetransaction']['institutetransactionuuid']=String::uuid();
		$this->request->data['Institutetransaction']['coa_id'] = $coa_id;
		$this->request->data['Institutetransaction']['user_id'] = $user_id;
		$this->request->data['Institutetransaction']['institute_id'] = $institute_id;
		$this->request->data['Institutetransaction']['forma_id'] = $forma_id;
		$this->request->data['Institutetransaction']['donationdetail_id'] = $donationdetail_id;
		$this->request->data['Institutetransaction']['transaction_id'] = $transaction_id;
		$this->request->data['Institutetransaction']['transactiontype_id'] = $transactiontype_id;
		$this->request->data['Institutetransaction']['transactiondate'] = $transactiondate;
		$this->request->data['Institutetransaction']['transactionamount'] = $transactionamount;
		$this->request->data['Institutetransaction']['institutetransactionisactive'] = $isActive;
		$this->request->data['Institutetransaction']['institutetransactioninsertid']=$userID;
		$this->request->data['Institutetransaction']['institutetransactioninsertdate']=date('Y-m-d H:i:s');
		$institutetransaction=$this->Institutetransaction->find('count',array("fields" => "Institutetransaction.transaction_id","conditions"=>array('Institutetransaction.transaction_id'=>$transaction_id,'Institutetransaction.transactiontype_id'=>$transactiontype_id,'Institutetransaction.coa_id'=>$coa_id)));
        $this->Institutetransaction->id = 0;
		if($institutetransaction > 0){
			$this->Institutetransaction->updateAll(
				array(
					'coa_id'=>$coa_id, 
					'user_id'=>$user_id, 
					'institute_id'=>$institute_id, 
					'forma_id'=>$forma_id, 
					'donationdetail_id'=>$donationdetail_id,
					'transactiondate'=>"'".$transactiondate."'", 
					'transaction_id'=>$transaction_id, 
					'transactiontype_id'=>$transactiontype_id,
					'transactionamount'=>$transactionamount, 
					'institutetransactionupdateid'=>$userID,
					'institutetransactionupdatedate'=>"'".date('Y-m-d H:i:s')."'",
					'institutetransactionisactive'=>$isActive
				), 
				array(
					'AND' => array(
						'Institutetransaction.transaction_id'=>$transaction_id,
						'Institutetransaction.transactiontype_id'=>$transactiontype_id,
					)
				)
			);
		} else {
			$this->Institutetransaction->save($this->request->data);
		}
		
		
	}

	//End Institute Portation//
	
	//Start User Total Amount Portation//	
	public function usertransactionmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('User Transaction Manage').' | '.__(Configure::read('site_name')));
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];
		$condition = array();
		if($group_id==1):
		elseif($group_id==2):
			$condition[] = array('Usertransaction.company_id'=>$company_id);
		else:
			$condition[] = array('Usertransaction.company_id'=>$company_id);
			$condition[] = array('Usertransaction.branch_id'=>$branch_id);
		endif;
		$this->paginate = array(
			'fields' => 'Usertransaction.*',
			'conditions'=> $condition ,
			'limit' => 20
		);
		$usertransaction = $this->paginate('Usertransaction');
		$this->set('usertransaction',$usertransaction);
	}

}
?>