<?php
	echo $Utilitys->paginationcommon();
					echo $Utilitys->tablestart();
						echo $this->Html->tableHeaders(
							array(
								__('SL#'), 
								array(
									__('Name (EN)')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Name (BN)')  => array('class' => 'highlight sortable')
								),
								array(
									__('Is Active')  => array('class' => 'highlight sortable')
								),
								__('Action')
							)
						);
						$companyrows = array();
						$countCompany = 0-1+$this->Paginator->counter('%start%');
						foreach($company as $Thiscompany):
							$countCompany++;
							$companyrows[]= array($countCompany,$Thiscompany["Company"]["companyname"],$Thiscompany["Company"]["companynamebn"],$Utilitys->statusURL($this->request["controller"], 'company', $Thiscompany["Company"]["id"], $Thiscompany["Company"]["companyuuid"], $Thiscompany["Company"]["companyisactive"]),$Utilitys->cusurl($this->request['controller'], 'company', $Thiscompany["Company"]["id"], $Thiscompany["Company"]["companyuuid"]));
						endforeach;
						echo $this->Html->tableCells(
							$companyrows
						);
					echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#ProductSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#productsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#productsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>