<?php
//pr($amountdetailsbyid);
	if($displaylayout=="ajax"):
	else:
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Amount.formdate",
			array(
				'type' => 'text',
				'div' => array('class'=> 'form-group'),
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __("Form Date"),
				'class'=> 'form-control',
				'style' => ''
			)
		);
		echo $this->Form->input(
			"Amount.todate",
			array(
				'type' => 'text',
				'div' => array('class'=> 'form-group'),
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __(" To Date"),
				'class'=> 'form-control',
				'style' => ''
			)
		);
		/*echo $this->Form->input(
			"Amount.searchtext",
			array(
				'type' => 'text',
				'div' => array('class'=> 'form-group'),
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
			)
		);*/
echo"
        <div class=\"btn-group\">
            <button class=\"btn btn-warning btn-sm dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-bars\"></i> Export Table Data</button>
            <ul class=\"dropdown-menu \" role=\"menu\">
                <li><a href=\"#\" onClick =\"$('#printtable').tableExport({type:'csv',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> <img src='".$this->webroot."js/icons/csv.png' width='24px'> CSV</a></li>
                <li><a href=\"#\" onClick =\"$('#printtable').tableExport({type:'txt',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> <img src='".$this->webroot."js/icons/txt.png' width='24px'> TXT</a></li>
                <li class=\"divider\"></li>
                <li><a href=\"#\" onClick =\"$('#printtable').tableExport({type:'excel',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> <img src='".$this->webroot."js/icons/xls.png' width='24px'> XLS</a></li>
                <li><a href=\"#\" onClick =\"$('#printtable').tableExport({type:'doc',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> <img src='".$this->webroot."js/icons/word.png' width='24px'> Word</a></li>
                <li><a href=\"#\" onClick =\"$('#printtable').tableExport({type:'powerpoint',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> <img src='".$this->webroot."js/icons/ppt.png' width='24px'> PowerPoint</a></li>
                <li class=\"divider\"></li>
                <li><a href=\"#\" onClick =\"$('#printtable').tableExport({type:'png',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> <img src='".$this->webroot."js/icons/png.png' width='24px'> PNG</a></li>
                <li><a href=\"#\" onClick =\"$('#printtable').tableExport({type:'pdf',pdfFontSize:'7',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> <img src='".$this->webroot."js/icons/pdf.png' width='24px'> PDF</a></li>
            </ul>
        </div>  
    ";
	echo $Utilitys->filtertagend();
	endif;
	echo"
		<div id=\"amountdetailsbyidsarchloading\">
	";
		$rid=$id;
		echo $this->Form->inpute('id', array('type'=>'hidden', 'value'=>@$id));
		echo $this->Form->inpute('model', array('type'=>'hidden', 'value'=>@$model));
		echo $this->Form->inpute('searchfield', array('type'=>'hidden', 'value'=>@$searchfield));
		//echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id='printtable');
			echo "<thead>";
				echo $this->Html->tableHeaders(
					array(
						array(
							__('Date')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Particulars')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Chq.No')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Withdraw')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Deposits')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Balance')  => array('class' => 'highlight sortable')
						)
					)
				);
			echo "</thead>";
				$amountdetailsbyidrows = array();
				$countAmountDetails = 0-1+$this->Paginator->counter('%start%');
				$totalBalance = ($amountdetailsbyidforpreviousday[0][0]["Previousamount"]?$amountdetailsbyidforpreviousday[0][0]["Previousamount"]:0);
				$totalBalance = $totalBalance + @$totalBalancefield;
				echo $this->Html->tableCells(
					array(array(array("Opening Balance",'colspan="5" align="left"'),array($totalBalance,'colspan="1" align="right"')))
				);
				foreach($amountdetailsbyid as $Thisamountdetailsbyid):
					$countAmountDetails++;
					$totalBalance += (@$Thisamountdetailsbyid["{$model}"]["".strtolower($model)."_balance"] ? $Thisamountdetailsbyid["{$model}"]["".strtolower($model)."_balance"] : 0);
					$amountdetailsbyidrows[]= array($Thisamountdetailsbyid["{$model}"]["transactiondate"],$Thisamountdetailsbyid["{$model}"]["".strtolower($model)."_particulars"],$Thisamountdetailsbyid["{$model}"]["".strtolower($model)."_chequenumber"],array($Thisamountdetailsbyid["{$model}"]["".strtolower($model)."_withdraw"],'colspan="1" align="right"'),array($Thisamountdetailsbyid["{$model}"]["".strtolower($model)."_deposit"],'colspan="1" align="right"'),array($totalBalance,'colspan="1" align="right"'));
				endforeach;
				echo $this->Html->tableCells(
					$amountdetailsbyidrows
				);
				echo $this->Html->tableCells(
					array(array(array("Ending Balance",'colspan="5" align="left"'),array($totalBalance,'colspan="1" align="right"')))
				);
			echo $Utilitys->tableend();
		//echo $Utilitys->paginationcommon();
		echo $this->Form->inpute('totalBalancefield', array('type'=>'hidden', 'value'=>@$totalBalance));
	echo"
		</div>
	";
	if($displaylayout=="ajax"):
	else:	
	echo"	<script>
			$(document).ready(function() {
				var startDate = new Date('01/01/1980');
				var FromEndDate = new Date();
				var ToEndDate = new Date();
				ToEndDate.setDate(ToEndDate.getDate()+365);

				$('#AmountFormdate').datetimepicker({
					format: \"yyyy-mm-dd\",
					language: 'en',
					weekStart: 1,
					todayBtn: 1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					minView: 2,
					forceParse: 0,
					startDate: '01/01/2012',
					endDate: FromEndDate
				}).on('changeDate', function(selected){
					startDate = new Date(selected.date.valueOf());
					startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
					$('#AmountTodate').datetimepicker('setStartDate', startDate);

					var AmountFormdate = $(this).val();
					var AmountTodate = $('#AmountTodate').val();
					$(\"#amountdetailsbyidsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."Customs/amountdetailsbyid/".$rid."/{$model}/{$searchfield}',
						{AmountFormdate:AmountFormdate,AmountTodate:AmountTodate,DisplayLayout:'ajax'},
						function(result) {
							$(\"#amountdetailsbyidsarchloading\").html(result);
						}
					);
				});

				$('#AmountTodate').datetimepicker({
					format: \"yyyy-mm-dd\",
					language: 'en',
					weekStart: 1,
					todayBtn: 1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					minView: 2,
					forceParse: 0,
					startDate: startDate,
					endDate: ToEndDate
				}).on('changeDate', function(selected){
					FromEndDate = new Date(selected.date.valueOf());
					FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
					$('#AmountFormdate').datetimepicker('setEndDate', FromEndDate);

					var AmountTodate = $(this).val();
					var AmountFormdate = $('#AmountFormdate').val();
					$(\"#amountdetailsbyidsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."Customs/amountdetailsbyid/".$rid."/{$model}/{$searchfield}',
						{AmountFormdate:AmountFormdate,AmountTodate:AmountTodate,DisplayLayout:'ajax'},
						function(result) {
							$(\"#amountdetailsbyidsarchloading\").html(result);
						}
					);
				});
				$(\".paging a\").click(function(){
					var id = $('#id').val();
					var model = $('#model').val();
					var searchfield = $('#searchfield').val();
					var totalBalancefield = $('#totalBalancefield').val();
					alert(totalBalancefield);
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{id:id, model:model, searchfield:searchfield, totalBalancefield:totalBalancefield},
						async: true,
						beforeSend: function(){
							$(\"#amountdetailsbyidsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#amountdetailsbyidsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
	endif;
?>