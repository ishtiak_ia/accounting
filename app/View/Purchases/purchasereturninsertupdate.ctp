<?php //echo $this->Html->script('order/order', array('inline' => true)); 
//print_r($purchase);
?>	

<!--<form class="sales-order-manage" name="purchaseProduct" id="purchaseProduct" action="purchaseinsertupdateaction" method="post">-->
<?php
echo $this->Form->create('Purchases', array('action' => 'purchasereturninsertupdateaction', 'type' => 'file'));
echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$purchase[0]['Order']['id']));
echo $this->Form->input('orderuuid', array('type'=>'hidden', 'value'=>@$purchase[0]['Order']['orderuuid']));
//$options = array(1 => __("Yes"), 0 =>__("No"));
//$attributes = array(
// 	'legend' => false, 
// 	'label' => true, 
// 	'value' => (@$purchase[0]['Order']['orderisactive'] ? $purchase[0]['Order']['orderisactive'] : 0),
// 	'class' => 'form-inline'
// );
if(empty($voucher_no[0]['voucher_no']))
	$voucher_no = "PR-".date('Y-m')."-0001";
else{
	$voucher_no_array = explode("-", $voucher_no[0]['voucher_no']);
	$voucher_no_new_last_dgits = $voucher_no_array[3]+1;
	if(strlen($voucher_no_new_last_dgits)!=4){
		$add_digits = 4 - strlen($voucher_no_new_last_dgits);
		if($add_digits == 3)
			$voucher_no_new_last_dgits = '000'.$voucher_no_new_last_dgits;
		else if($add_digits == 2)
			$voucher_no_new_last_dgits = '00'.$voucher_no_new_last_dgits;
		else if($add_digits == 1)
			$voucher_no_new_last_dgits = '0'.$voucher_no_new_last_dgits;

	}
	$voucher_no = "PR-".date('Y-m')."-$voucher_no_new_last_dgits";

}
if((isset($purchase[0]["Order"]["id"]))){
	$orderreferenceno = @$purchase[0]['Order']['orderreferenceno'];
}else {
	$orderreferenceno = $voucher_no;
}

	echo"<h3>".__("Add New Purchase Return")."</h3>";
?>
<style>
.purchase_page label {
    display: block;
}
</style>
	<div class="row text-center" style='overflow-x:scroll;'>
		<table id="hajji-meta-table" class="table table-condensed table-hover">
			<tbody>
				<tr class="sales-item-row purchase_page">
					<td>
						<?php
						echo $this->Form->input(
							"Order.orderdate",
							array(
								'type' => 'text',
								'label' =>__('Date'),
								//'tabindex'=>1,
								'placeholder' => __("Date"),
								'class'=> 'form-control',
								'data-validation-engine'=>'validate[required]',
								'value' => @$purchase[0]['Order']['orderdate']
							)
						);
						?>
					</td>
					<td>
						<?php
						echo $this->Form->input(
							"Order.orderreferenceno",
							array(
								'type' => 'text',
								'label' =>__('Invoice'),
								//'tabindex'=>2,
								'readonly' => 'readonly',
								'placeholder' => __("Invoice"),
								'class'=> 'form-control',
								'data-validation-engine'=>'validate[required]',
								'value' => $orderreferenceno
							)
						);
						?>
					</td>
					<td>
						<?php
						echo $this->Form->input(
							"Order.clientcode",
							array(
								'type'=>'select',
								"options"=>array($supplierId),
								'empty'=>__('Select Supplier'),
								//'tabindex'=>3,
								'label'=>__('Supplier ID'),
								'data-validation-engine'=>'validate[required]',
								"selected" => (@$purchase[0]['Order']['clientcode'] ? $purchase[0]['Order']['clientcode'] : 0)
							)
						);
						?>
					</td>
					<td>
						<?php
						echo $this->Form->input(
							"Order.client_id",
							array(
								'type'=>'select',
								"options"=>array($supplierlist),
								'empty'=>__('Select Supplier'),
								//'tabindex'=>4,
								'label'=>__('Supplier Name'),
								//'data-validation-engine'=>'validate[required]',
								"selected" => (@$purchase[0]['Order']['client_id'] ? $purchase[0]['Order']['client_id'] : 0)
							)
						);
						?>
					</td>
					<td>
						<?php
						echo $this->Form->input(
							"Order.branch_id",
							array(
								'type'=>'select',
								"options"=>array($salescenter),
								'empty'=>__('Select Stock Center'),
								//'tabindex'=>5,
								'label'=>__('Stock Center'),
								'onchange'=>'getItem(1,false)',
								'data-validation-engine'=>'validate[required]',
								"selected" => (@$purchase[0]['Order']['branch_id'] ? $purchase[0]['Order']['branch_id'] : 0)
							)
						);
						?>
					</td>
				</tr>	
			</tbody>
		</table>	
			
	</div>
	<!-- .row -->

	<div class="row">
		<div class="table-responsive">
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>Item ID</th>
						<th>Item Name</th>
						<th>Quantity</th>
						<th>Unit Price</th>
						<th>Price</th>
						<th>Stock</th>
						<th>Unit</th>
						<th>Discount (%)</th>
						<th>Discount Value</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody id='content'>
						<?php
							$count_orderdetails_data = count(@$orderdetails_data);
							if(isset($orderdetails_data[0]["Orderdetail"]["id"])){
								$count=0;
								foreach ($orderdetails_data as $this_orderdetails_data) {
									$count++;
						?>
						<tr id='mydivcontent<?php echo $count; ?>'>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.productcode{$count}",
								array(
									'type'=>'select',
									"options"=>$product_code_edit,
									//"options"=>array($productcodelist),
									'empty'=>__('Select Product Code'),
									//'tabindex'=>6,
									'label'=>false,
									//'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'onchange'=>"getProductName($count); stockCountForPurchaseReturn($count);",
									'style'=>'',
									"selected" => (@$this_orderdetails_data['Orderdetail']['productcode'] ? $this_orderdetails_data['Orderdetail']['productcode'] : 0)
								)
							);
						?>	
						</td>
						<td>
							<div class="form-group"  id="productchangediv">
							<?php
							echo $this->Form->input(
								"Orderdetail.product_id{$count}",
								array(
									'type'=>'select',
									"options"=>$product_list_edit,
									//"options"=>array($productlist),
									'empty'=>__('Select Product Name'),
									//'tabindex'=>7,
									'label'=>false,
									//'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'onchange'=>"getProductCode($count); stockCountForPurchaseReturn($count);",
									'style'=>'',
									"selected" => (@$this_orderdetails_data['Orderdetail']['product_id'] ? $this_orderdetails_data['Orderdetail']['product_id'] : 0)
								)
							);
						echo $this->Form->input("Order.unitmon", array("type" => "hidden"));
						echo $this->Form->input("Order.unitkilogram", array("type" => "hidden"));
						echo $this->Form->input("Order.unitgram", array("type" => "hidden"));
						echo $this->Form->input("Order.stockmon", array("type" => "hidden"));
						echo $this->Form->input("Order.stockkilogram", array("type" => "hidden"));
						echo $this->Form->input("Order.stockgram", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailmon", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailkg", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailgram", array("type" => "hidden"));
						echo $this->Form->input("Order.productdafultprice", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailmonunitprice", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailkgunitprice", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailgramunitprice", array("type" => "hidden"));
						?>
							</div>
							<span id="OrderbankAccountIdspan"></span>
						</td>
						<td>
							<?php
							echo $this->Form->input(
								"Orderdetail.quantity{$count}",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									//'tabindex'=>8,
									'placeholder' => __("Quantity"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$this_orderdetails_data['Orderdetail']['quantity'],
									'onkeyup' => 'priceandamountcalculation('.$count.'); stockLimitAlert('.$count.');'
								)
							);
							?>
							<span id="quantityspan"></span>
							<span id="price_span"></span>
						</td>
						<td>
							<?php
							echo $this->Form->input(
								"Orderdetail.unitprice{$count}",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									//'tabindex'=>9,
									'placeholder' => __("Unit Price"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$this_orderdetails_data['Orderdetail']['unitprice'],
									'onkeyup' => 'priceandamountcalculation('.$count.');'
								)
							);
							?>
							<span id="stocktspan"></span>
						</td>
						<td>
							<span id="pricespan">
							<?php
							echo $this->Form->input(
								"Orderdetail.price{$count}",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									//'tabindex'=>10,
									//'placeholder' => __("Unit Price"),
									'readonly' => 'readonly',
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$this_orderdetails_data['Orderdetail']['price']
								)
							);
							?>	
							</span>
						</td>
						<td>
							<?php
							echo $this->Form->input(
								"Orderdetail.stock{$count}",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'readonly' => 'readonly',
									'class'=> 'form-control',
									'style' => ''
									//'value' => @$purchase[0]['Orderdetail']['price']
								)
							);
							?>
						</td>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.unit_id{$count}",
								array(
									'type'=>'select',
									"options"=>array($unitlist),
									'empty'=>__('Select Unit'),
									//'tabindex'=>11,
									'label'=>false,
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style'=>'',
									"selected" => (@$this_orderdetails_data['Orderdetail']['unit_id'] ? $this_orderdetails_data['Orderdetail']['unit_id'] : 0)
								)
							);
						?>		
						<span id="productidloadspan"></span>	
						</td>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.discount{$count}",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									//'tabindex'=>12,
									'placeholder' => __("Discount (%)"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$this_orderdetails_data['Orderdetail']['discount'],
									'onkeyup' => 'priceandamountcalculation('.$count.');'
								)
							);
						?>	
						</td>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.discountvalue{$count}",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'readonly' => 'readonly',
									//'tabindex'=>13,
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$this_orderdetails_data['Orderdetail']['discountvalue']
								)
							);
						?>	
						</td>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.transactionamount{$count}",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'readonly' => 'readonly',
									//'tabindex'=>14,
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$this_orderdetails_data['Orderdetail']['transactionamount']
								)
							);
						?>		
						</td>
					</tr>
						<?php 			
								}	
							}else{
						?>
					<tr id='mydivcontent1'>
						<td>
						<?php
						echo $this->Form->input(
							"Orderdetail.productcode1",
							array(
								'type'=>'select',
								/*"options"=>array($productcodelist),*/
								'empty'=>__('Select Product Code'),
								//'tabindex'=>6,
								'label'=>false,
								//'class'=> 'form-control',
								'data-validation-engine'=>'validate[required]',
								'style'=>'',
								'onchange'=>'getProductName(1); stockCountForPurchaseReturn(1);',
								"selected" => (@$purchase[0]['Orderdetail']['productcode'] ? $purchase[0]['Orderdetail']['productcode'] : 0)
							)
						);
						?>
						</td>
						<td>
							<div class="form-group"  id="productchangediv">
							<?php
							echo $this->Form->input(
								"Orderdetail.product_id1",
								array(
									'type'=>'select',
									/*"options"=>array($productlist),*/
									'empty'=>__('Select Product Name'),
									//'tabindex'=>7,
									'label'=>false,
									//'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style'=>'',
									'onchange'=>'getProductCode(1); stockCountForPurchaseReturn(1);',
									"selected" => (@$purchase[0]['Orderdetail']['product_id'] ? $purchase[0]['Orderdetail']['product_id'] : 0)
								)
							);
						echo $this->Form->input("Order.unitmon", array("type" => "hidden"));
						echo $this->Form->input("Order.unitkilogram", array("type" => "hidden"));
						echo $this->Form->input("Order.unitgram", array("type" => "hidden"));
						echo $this->Form->input("Order.stockmon", array("type" => "hidden"));
						echo $this->Form->input("Order.stockkilogram", array("type" => "hidden"));
						echo $this->Form->input("Order.stockgram", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailmon", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailkg", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailgram", array("type" => "hidden"));
						echo $this->Form->input("Order.productdafultprice", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailmonunitprice", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailkgunitprice", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailgramunitprice", array("type" => "hidden"));
						?>
							</div>
							<span id="OrderbankAccountIdspan"></span>
						</td>
						<td>
							<?php
							echo $this->Form->input(
								"Orderdetail.quantity1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									//'tabindex'=>8,
									'placeholder' => __("Quantity"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$purchase[0]['Orderdetail']['quantity'],
									'onkeyup' => 'priceandamountcalculation(1); stockLimitAlert(1);'
								)
							);
							?>
							<span id="quantityspan"></span>
							<span id="price_span"></span>
						</td>
						<td>
							<?php
							echo $this->Form->input(
								"Orderdetail.unitprice1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									//'tabindex'=>9,
									'placeholder' => __("Unit Price"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$purchase[0]['Orderdetail']['unitprice'],
									'onkeyup' => 'priceandamountcalculation(1);'
								)
							);
							?>
							<span id="stocktspan"></span>
						</td>
						<td>
							<span id="pricespan">
							<?php
							echo $this->Form->input(
								"Orderdetail.price1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									//'tabindex'=>10,
									//'placeholder' => __("Unit Price"),
									'readonly' => 'readonly',
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$purchase[0]['Orderdetail']['price']
								)
							);
							?>	
							</span>
						</td>
						<td>
							<?php
							echo $this->Form->input(
								"Orderdetail.stock1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									//'tabindex'=>10,
									//'placeholder' => __("Unit Price"),
									'readonly' => 'readonly',
									'class'=> 'form-control',
									//'data-validation-engine'=>'validate[required]',
									'style' => ''
									//'value' => @$purchase[0]['Orderdetail']['price']
								)
							);
							?>
						</td>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.unit_id1",
								array(
									'type'=>'select',
									"options"=>array($unitlist),
									'empty'=>__('Select Unit'),
									//'tabindex'=>11,
									'label'=>false,
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style'=>'',
									"selected" => (@$purchase[0]['Orderdetail']['unit_id'] ? $purchase[0]['Orderdetail']['unit_id'] : 0)
								)
							);
						?>		
						<span id="productidloadspan"></span>	
						</td>
						
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.discount1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									//'tabindex'=>12,
									'placeholder' => __("Discount (%)"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$purchase[0]['Orderdetail']['discount'],
									'onkeyup' => 'priceandamountcalculation(1);'
								)
							);
						?>	
						</td>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.discountvalue1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'readonly' => 'readonly',
									//'tabindex'=>13,
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$purchase[0]['Orderdetail']['discountvalue']
								)
							);
						?>	
						</td>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.transactionamount1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'readonly' => 'readonly',
									//'tabindex'=>14,
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$purchase[0]['Orderdetail']['transactionamount']
								)
							);
						?>		
						</td>
					</tr>		
						<?php		
							}	
						?>
					</tbody>
			</table>
			<table class='table table-condensed table-hover table-responsive'>

				<tr>
					<td colspan="8">
						<input type="button" id="more_fields"  value="Add a Row" />
						<!--<input type="button" id="remove_fields" onclick="remove_field();" value="Remove More" />-->
						<input type="button" id="anc_rem" value="Remove a Row" />
						<!--<a href="javascript:void(0);" id="anc_rem">Remove Row</a>-->
						<input type="hidden" style="width:48px;" name="totlarow" id="totlarow" value="<?php echo @$count_orderdetails_data?$count_orderdetails_data:1  ?>" />
					</td>
				</tr>


			</table>
			<!--table for total amount start-->
			<table class="table table-condensed table-hover table-responsive">		
					<tr>
						<th class="text-right" style="width:85%">Total Price</th>
						<td>
						<?php
							echo $this->Form->input(
								"Order.ordertotal",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'readonly' => 'readonly',
									//'tabindex'=>15,
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$purchase[0]['Order']['ordertotal']
								)
							);
						?>	
						</td>
					</tr>
					<tr>
						<th class="text-right" style="width:85%">Total Discount (-)</th>
						<td>
						<?php
							echo $this->Form->input(
								"Order.orderdiscount",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'readonly' => 'readonly',
									//'tabindex'=>16,
									'class'=> 'form-control',
									'style' => '',
									'value' => @$purchase[0]['Order']['orderdiscount']
								)
							);
						?>
						</td>
					</tr>
					<tr>
						<th class="text-right" style="width:85%">Grand Total</th>
						<td>
						<?php
							echo $this->Form->input(
								"Order.ordergrandtotal",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'readonly' => 'readonly',
									//'tabindex'=>17,
									'class'=> 'form-control',
									'style' => '',
									'value' => @$purchase[0]['Order']['transactionamount']
								)
							);
						?>	
						</td>
					</tr>
					<tr>
						<th class="text-right" style="width:85%">Payment</th>
						<td>
						<?php
							echo $this->Form->input(
								"Order.orderpayment",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									//'tabindex'=>18,
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$purchase[0]['Order']['orderpayment']
								)
							);
						?>	
						</td>
					</tr>
					<tr>
						<th class="text-right" style="width:85%">Due</th>
						<td>
						<?php
							echo $this->Form->input(
								"Order.orderdue",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'readonly' => 'readonly',
									//'tabindex'=>19,
									'class'=> 'form-control',
									'style' => '',
									'value' => @$purchase[0]['Order']['orderdue']
								)
							);
						?>	
						</td>
					</tr>
					<tr>
						<th class="text-right" style="width:85%">
						Due Amount<br>
						Payment Date
						</th>
						<?php //= 0){ ?>
						<td>
							<?php
							echo $this->Form->input(
								"Order.orderduepaymentdate",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									//'tabindex'=>20,
									'class'=> 'form-control',
									'style' => '',
									'value' => @$purchase[0]['Order']['orderduepaymentdate']
								)
							);
							?>
						</td>
						<?php //} ?>
					</tr>
			</table>
			<!--table for total amount end-->
			<!--table for coa start-->
			<table class="table table-condensed table-hover table-responsive">
				<tr>
					<th class="text-right" style="width:85%; text-align:center; ">COA Code</th>
					<th class="text-right" style="width:85%; text-align:center">COA Name</th>

				</tr>
				<tr>
					<td style="width:85%; text-align:center; ">
						<?php
						echo $this->Form->input(
							"Order.coacode",
							array(
								'type' => 'select',
								'empty'=>__('Select Coa Code'),
								'options'=>array($coa_code),
								'label'=>false,
								//'tabindex'=>21,
								//'class'=> 'form-control',

								'style'=>'width:120px',
								'style' => '',
								'value' => @$purchase[0]['Order']['coacode']
							)
						);
						?>
					</td>
					<td style="width:85%; text-align:center; ">
						<?php
						echo $this->Form->input(
							"Order.coa_id",
							array(
								'type' => 'select',
								'empty'=>__('Select Coa Name'),
								'options'=>array($coa),
								'label'=>false,
								//'tabindex'=>22,
								'style'=>'width:120px',
								//'class'=> 'form-control',
								'style' => '',
								'value' => @$purchase[0]['Order']['coa_id']
							)
						);
						?>
					</td>
				</tr>
			</table>
			<!--table for coa start-->
		</div>
	</div>
	<!-- /.row -->

	<fieldset>
	

	

	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
			<?php
				echo $this->Form->input(
					"Order.ordercomment",
					array(
						'type' => 'textarea',
						'div' => array('class'=> 'form-group'),
						'label' =>__('Comment'),
						//'tabindex'=>22,
						'placeholder' => __("Comment"),
						'class'=> 'form-control',
						'data-validation-engine'=>'validate[required]',
						'style' => '',
						'cols'=>30,
						'rows'=>4,
						'value' => @$purchase[0]['Order']['ordercomment']
					)
				);
			?>	
			</div>
			<?php
			echo $Utilitys->allformbutton('',$this->request["controller"],$page='purchasereturn');
			echo $this->Form->end();
			?>
		</div>
		<!-- /.col-sm-6 -->
	</div>
	<!-- /.row -->
	</fieldset>
<!--</form> -->

<script>
 $('.sales-order-manage .cash-check-choice input[type="radio"]').on( 'change', function() {
		var target_div = $('.check-payment-enabled');
		if (this.value == '1') {
			target_div.slideDown('fast');
		} else {
	 		target_div.slideUp('fast');
	 	}
	 });
</script>
<script  type="text/javascript">
	var room = 0;
	// Start variable for adding dynamic porduct list from the controller to the dynamically created Order Form input

		var productcodelist = "<?php
								if(isset($productcodelist)){
									foreach($productcodelist as $key => $value){?>"+
										'<option value="<?php echo $key ?>"><?php echo $value?></option>'+
						      "<?php
												}
											}
				 				?>";
		var productlist = "<?php
								if(isset($productlist)){
									foreach($productlist as $key => $value){?>"+
										'<option value="<?php echo $key ?>"><?php echo $value?></option>'+
							  "<?php
												}
											}
				 			  ?>";
		var unitlist = "<?php
								if(isset($unitlist)){
									foreach($unitlist as $key => $value){?>"+
										'<option value="<?php echo $key ?>"><?php echo $value?></option>'+
							  "<?php
												}
											}
				 			  ?>";		 			  
		// End variable for adding dynamic porduct list from the controller to the dynamically created Order Form input
	$('#more_fields').click(function(){
		room = parseInt(document.getElementById("totlarow").value);
		//alert(room);
        room = room+1;

        $("#content").append(
        	'<tr id="mydivcontent'+room+'">' +
        	'<td><div class="control-group"><select id="OrderdetailProductcode'+room+'" style="" data-validation-engine="validate[required]" name="data[Orderdetail][productcode'+room+']" onchange="getProductName('+room+'); stockCountForPurchaseReturn('+room+');"><option value="">Select Product Code</option></select></div></td>' +
        	'<td><div id="productchangediv" class="form-group"><div class="control-group"><select id="OrderdetailProductId'+room+'" style="" data-validation-engine="validate[required]" name="data[Orderdetail][product_id'+room+']"  onchange="getProductCode('+room+'); stockCountForPurchaseReturn('+room+');"><option value="">Select Product Name</option></select></div><input type="hidden" id="OrderUnitmon'+room+'" class="input-xxlarge" name="data[Order][unitmon]"><input type="hidden" id="OrderUnitkilogram'+room+'" class="input-xxlarge" name="data[Order][unitkilogram]"><input type="hidden" id="OrderUnitgram'+room+'" class="input-xxlarge" name="data[Order][unitgram]"><input type="hidden" id="OrderStockmon'+room+'" class="input-xxlarge" name="data[Order][stockmon]"><input type="hidden" id="OrderStockkilogram'+room+'" class="input-xxlarge" name="data[Order][stockkilogram]"><input type="hidden" id="OrderStockgram'+room+'" class="input-xxlarge" name="data[Order][stockgram]"><input type="hidden" id="OrderOrderdetailmon'+room+'" class="input-xxlarge" name="data[Order][orderdetailmon]"><input type="hidden" id="OrderOrderdetailkg'+room+'" class="input-xxlarge" name="data[Order][orderdetailkg]"><input type="hidden" id="OrderOrderdetailgram'+room+'" class="input-xxlarge" name="data[Order][orderdetailgram]"><input type="hidden" id="OrderProductdafultprice'+room+'" class="input-xxlarge" name="data[Order][productdafultprice]"><input type="hidden" id="OrderOrderdetailmonunitprice'+room+'" class="input-xxlarge" name="data[Order][orderdetailmonunitprice]"><input type="hidden" id="OrderOrderdetailkgunitprice'+room+'" class="input-xxlarge" name="data[Order][orderdetailkgunitprice]"><input type="hidden" id="OrderOrderdetailgramunitprice'+room+'" class="input-xxlarge" name="data[Order][orderdetailgramunitprice]"></div><span id="OrderbankAccountIdspan"></span>	</td>' +
        	'<td><div class="form-group"><input type="text" id="OrderdetailQuantity'+room+'" onkeyup="priceandamountcalculation('+room+'); stockLimitAlert('+room+');" style="" data-validation-engine="validate[required]" placeholder="Quantity" class="form-control" name="data[Orderdetail][quantity'+room+']"></div><span id="quantityspan"></span><span id="price_span"></span></td>' +
        	'<td><div class="form-group"><input type="text" id="OrderdetailUnitprice'+room+'" onkeyup="priceandamountcalculation('+room+');" style="" data-validation-engine="validate[required]" placeholder="Unit Price" class="form-control" name="data[Orderdetail][unitprice'+room+']"></div><span id="stocktspan"></span></td>' +
        	'<td><span id="pricespan"><div class="form-group"><input type="text" id="OrderdetailPrice'+room+'" style="" data-validation-engine="validate[required]" readonly="readonly" class="form-control" name="data[Orderdetail][price'+room+']"></div></span></td>' +
        	'<td><div class="form-group"><input type="text" id="OrderdetailStock'+room+'" style="" readonly="readonly" class="form-control" name="data[Orderdetail][stock'+room+']"></div></td>' +
        	'<td><div class="control-group"><select id="OrderdetailUnitId'+room+'" style="" data-validation-engine="validate[required]" class="form-control" name="data[Orderdetail][unit_id'+room+']"><option value="">Select Unit</option>'+unitlist+'</select></div><span id="productidloadspan"></span></td>' +
        	'<td><div class="form-group"><input type="text" id="OrderdetailDiscount'+room+'" onkeyup="priceandamountcalculation('+room+');" style="" data-validation-engine="validate[required]" placeholder="Discount (%)" class="form-control" name="data[Orderdetail][discount'+room+']"></div></td>' +
        	'<td><div class="form-group"><input type="text" id="OrderdetailDiscountvalue'+room+'" style="" readonly="readonly" class="form-control" name="data[Orderdetail][discountvalue'+room+']"></div></td>' +
        	'<td><div class="form-group"><input type="text" id="OrderdetailTransactionamount'+room+'" style="" data-validation-engine="validate[required]" readonly="readonly" class="form-control" name="data[Orderdetail][transactionamount'+room+']"></div></td>' +
        	'</tr>');
			document.getElementById("totlarow").value=room;
			$('#OrderdetailProductcode'+room).select2();
			$('#OrderdetailProductId'+room).select2();


		getItem(room,true);
		});	

	$("#anc_rem").click(function(){
		if($('#content tr').size()>1){
	 		$('#content tr:last-child').remove();
				room = room-1;
				//alert(room);
				document.getElementById("totlarow").value=room;
	 	}else{
	 		alert('One row should be present in table');
	 	}
	 });

</script>
<script type="text/javascript">
 $(document).ready(function(){
    $("#PurchasesPurchaseinsertupdateactionForm").validationEngine();
    $("#OrderOrderdate").datetimepicker({language: "bn",format: "yyyy-mm-dd HH:mm:ss", use24hours: true, showMeridian: false , startView: 2, minView: 2, autoclose: 1});
	$("#OrderOrderduepaymentdate").datetimepicker({language: "bn",format: "yyyy-mm-dd HH:mm:ss", use24hours: true, showMeridian: false , startView: 2, minView: 2, autoclose: 1});
    $('#OrderClientcode').select2();	
    $('#OrderClientId').select2();
    $('#OrderBranchId').select2();
    $('#OrderCoacode').select2();
    $('#OrderCoaId').select2();
    $('[id^="OrderdetailProductcode"]').select2();
    $('[id^="OrderdetailProductId"]').select2();
    $("#OrderOrderpayment").keyup(function(){
    	var grand_total = $("#OrderOrdergrandtotal").val();
    	var payment = $("#OrderOrderpayment").val();
    	var due = grand_total - payment;
    	$("#OrderOrderdue").val(due);
    });
    $('#OrderClientId').change(function () {
		var clientId = $(this).find('option:selected').val();
//			alert(clientId);
		$('#OrderClientcode option').each(function() {
			if($(this).val() == clientId) {
				if($(this).text()){
					$(this).prop("selected", true);
					$('#s2id_OrderClientcode .select2-chosen' ).text( $(this).text());
				}else
					$('#s2id_OrderClientcode .select2-chosen' ).text( 'Not Found');
//						$(this).prop("selected", true);
			}
		});
	});
    $('#OrderClientcode').change(function () {
		var clientId = $(this).find('option:selected').val();
//			alert(clientId);
		$('#OrderClientId option').each(function() {
			if($(this).val() == clientId) {
				if($(this).text()){
					$(this).prop("selected", true);
					$('#s2id_OrderClientId .select2-chosen' ).text( $(this).text());
				}else
					$('#s2id_OrderClientId .select2-chosen' ).text( 'Not Found');
//						$(this).prop("selected", true);
			}
		});
	});
	$('#OrderCoacode').change(function () {
		var clientId = $(this).find('option:selected').val();
//			alert(clientId);
		$('#OrderCoaId option').each(function() {
			if($(this).val() == clientId) {
					$(this).prop("selected", true);
					$('#s2id_OrderCoaId .select2-chosen' ).text( $(this).text());
			}
		});
	});
	$('#OrderCoaId').change(function () {
		var clientId = $(this).find('option:selected').val();
//			alert(clientId);
		$('#OrderCoacode option').each(function() {
			if($(this).val() == clientId) {
				$(this).prop("selected", true);
				$('#s2id_OrderCoacode .select2-chosen' ).text( $(this).text());
				//finished
			}
		});
	});
 });
</script>
<script type="text/javascript">
function priceandamountcalculation(fieldid){
	var quantity = $("#OrderdetailQuantity"+fieldid).val();
	if(quantity ==''){
        quantity = 0;
    }else{
        quantity = parseFloat(quantity);
    }
	var unit_price = $("#OrderdetailUnitprice"+fieldid).val();
	if(unit_price ==''){
        unit_price = 0;
    }else{
        unit_price = parseFloat(unit_price);
    }

    $("#OrderdetailPrice"+fieldid).val(quantity * unit_price);
    //var price = document.getElementById("OrderdetailPrice"+fieldid);
	//price.value = quantity * unit_price;

	//var price_value = document.getElementById("OrderdetailPrice"+fieldid).value;
	var price = $("#OrderdetailPrice"+fieldid).val();

	var discount_rate = $("#OrderdetailDiscount"+fieldid).val();
    if(discount_rate =='' || discount_rate == 'NaN'){
        discount_rate = 0;
    }else{
        discount_rate = parseFloat(discount_rate);
    }
    var discount_amount = ((price*discount_rate)/100);
    var amount_value = price - discount_amount;
    //var amount =  document.getElementById("OrderdetailTransactionamount"+fieldid);
    //amount.value = amount_value;
    $("#OrderdetailDiscountvalue"+fieldid).val(discount_amount);
    $("#OrderdetailTransactionamount"+fieldid).val(amount_value);

    var total_amount = 0;
    row = parseInt(document.getElementById("totlarow").value);
    for(i = 1; i<=row; i++){
    	total_amount += parseFloat(document.getElementById("OrderdetailPrice"+i).value);
    }
    $("#OrderOrdertotal").val(total_amount);

    var total_discount_amount = 0
    for(j =1; j <= row; j++){
    	total_discount_amount += parseFloat(document.getElementById("OrderdetailDiscountvalue"+j).value);
    }
    $("#OrderOrderdiscount").val(total_discount_amount);

    var bonus_discount = $("#OrderOrderdiscount").val();
    if(bonus_discount ==''){
        bonus_discount = 0;
    }else{
        bonus_discount = parseFloat(bonus_discount);
    }
    var grand_total = total_amount - bonus_discount;
    $("#OrderOrdergrandtotal").val(grand_total);

    var payment = $("#OrderOrderpayment").val();
    if(payment == ''){
    	payment = 0;
    }else{
    	payment = parseFloat(payment);
    }
    var due = grand_total - payment;
    //var due_show = due.toFixed(2);
    $("#OrderOrderdue").val(due.toFixed(2));
}

//------------------------------------------------------------------------------------
//Start-- onchange event between dynamically created OrderDetail fields

	function getProductName(rowNumber) {

		var productId = $('#OrderdetailProductcode'+rowNumber).find('option:selected').val();

		$('#OrderdetailProductId' + rowNumber + ' option').each(function () {
			if ($(this).val() == productId) {

				$(this).prop("selected", true);
				$('#s2id_OrderdetailProductId' + rowNumber + ' .select2-chosen').text($(this).text());

			}

		});

	}

	function getProductCode(rowNumber){

		var productId = $('#OrderdetailProductId'+rowNumber).find('option:selected').val();

		$('#OrderdetailProductcode'+rowNumber+' option').each(function() {
				if($(this).val() == productId) {

					$(this).prop("selected", true);
					$('#s2id_OrderdetailProductcode'+rowNumber+' .select2-chosen' ).text( $(this).text());
					//finished

				}

		});

	}

//End-- onchange event between dynamically created OrderDetail fields
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//Start onchange event between branch id and item

	function getItem(rowNumber,flag) {

		var host = "<?php echo $this->webroot; ?>";

			var brachId = $('#OrderBranchId').find('option:selected').val();


			$.ajax({


				'url': host + 'orders/getProduct/' + brachId,
				'dataType': 'json'

			}).success(function (data) {

				var itemCode = "<option value=''>Select </option>";
				var itemName = "<option value=''>Select </option>";

				$(data).each(function(index,item){

					itemCode += "<option value ="+ item.Product.id +">"+item.Productbranch.product_code+" </option>";
					itemName += "<option value ="+ item.Product.id +">"+item.Product.productname+" </option>";


				});
					var totalRow = $('#totlarow').val();

				if(flag == false){

					for(var row = 1; row<=totalRow; row++){
						$('#s2id_OrderdetailProductcode'+row+' .select2-chosen' ).text( 'Select');
						$('#s2id_OrderdetailProductId'+row+' .select2-chosen' ).text( 'Select');
						$('#OrderdetailProductcode'+row ).html(itemCode);
						$('#OrderdetailProductId'+row ).html(itemName);
					}
				}

				if(flag == true){

					$('#OrderdetailProductcode'+rowNumber ).html(itemCode);
					$('#OrderdetailProductId'+rowNumber ).html(itemName);
				}

			})

	}

//End onchange event between branch id and item
//Start Count Stock as per the quantity of items sold and puchased

	function stockCountForPurchaseReturn(rowNumber){

		var host = "<?php echo $this->webroot; ?>";
		var branchId 	= $('#OrderBranchId').find('option:selected').val();
		var productId 	= $('#OrderdetailProductId'+rowNumber).find('option:selected').val();

		$.ajax({

			'url': host + 'purchases/getStockForPurchaseReturn/' + branchId +'/'+productId ,
			'dataType': 'json'

		}).success(function (result) {

			$('#OrderdetailStock'+rowNumber).val(result);

		});
	}
// End 
//---------------------------------------------------------------------
// Start
function stockLimitAlert(rowNumber) {
	stock = parseInt($('#OrderdetailStock'+rowNumber).val());		
	inputQuantity = parseInt($('#OrderdetailQuantity'+rowNumber).val());

	if(stock != ''){
		if(inputQuantity>stock){
			alert('Quantity Exceeds Stock');
		}
		if(inputQuantity===stock){
			alert('Quantity Equals Stock');
		}
	}
}	
</script>