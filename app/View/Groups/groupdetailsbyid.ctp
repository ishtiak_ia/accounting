<?php
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"2\">
				<h2>".__('Group')."</h2>
			</td>
		</tr>
		<tr>
			<th>
				".__('Name')."
			</th>
			<td>
				".$groups[0]["Group"]["groupname"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Permanent")."
			</th>
			<td>
				".$groups[0]["Group"]["groupispermanent"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Active")."
			</th>
			<td>
				".$groups[0]["Group"]["isActive"]."
			</td>
		</tr>
	";
	echo $Utilitys->tableend();
?>