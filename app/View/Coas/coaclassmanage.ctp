<?php
	echo "<h2>".__('Chart of Account Class'). $Utilitys->addurl($title=__("Add New Chart of Account Class"), $this->request["controller"], $page="coaclass")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Coaclass.coatype_id",
			array(
				'type'=>'select',
				"options"=>array($coatype),
				'empty'=>__('Select Chart of Account Type'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'style'=>'',
				'class'=> 'form-control'
			)
		)
		.$this->Form->input(
			"Coaclass.coagroup_id",
			array(
				'type'=>'select',
				"options"=>array($coagrouplist),
				'empty'=>__('Select Chart of Account Group'),
				'div'=>false,
				'tabindex'=>2,
				'label'=>false,
				'style'=>'',
				'class'=> 'form-control'
			)
		)
		.$this->Form->input(
			"Coaclass.coaclass_id",
			array(
				'type'=>'select',
				"options"=>array($coaclasslist),
				'empty'=>__('Select Chart of Account Class'),
				'div'=>false,
				'tabindex'=>3,
				'label'=>false,
				'style'=>'',
				'class'=> 'form-control'
			)
		)
		."<span id=\"classloading\"></span>"
		.$this->Form->input(
			"Coaclass.coaclassname",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'tabindex'=>4,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control'
			)
		);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"classsearchloading\">
	";
			echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id=null);
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('COA Type')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA Group')  => array('class' => 'highlight sortable')
						), 
						array(
							__('CODE')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA Class')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA Class BN')  => array('class' => 'highlight sortable')
						), 
						__('Active'), 
						__('Action')
					)
				);
				$coaclassrows = array();
				$countCoaclass = 0-1+$this->Paginator->counter('%start%');
				foreach($coaclass as $Thiscoaclass):
					$countCoaclass++;
					$coaclassrows[]= array(
						$countCoaclass,
						$Thiscoaclass["Coatype"]["coatypename"]."/".$Thiscoaclass["Coatype"]["coatypenamebn"],
						$Thiscoaclass["Coagroup"]["coagroupname"]."/".$Thiscoaclass["Coagroup"]["coagroupnamebn"],
						$Thiscoaclass["Coaclass"]["coaclasscode"],
						$Thiscoaclass["Coaclass"]["coaclassname"],
						$Thiscoaclass["Coaclass"]["coaclassnamebn"],
						$Utilitys->statusURL($this->request["controller"], 'coaclass', $Thiscoaclass["Coaclass"]["id"], $Thiscoaclass["Coaclass"]["coaclassuuid"], $Thiscoaclass["Coaclass"]["coaclassisactive"]),$Utilitys->cusurl($this->request["controller"], 'coaclass', $Thiscoaclass["Coaclass"]["id"], $Thiscoaclass["Coaclass"]["coaclassuuid"]));
				endforeach;
				echo $this->Html->tableCells(
					$coaclassrows
				);
			echo $Utilitys->tableend();
			echo $Utilitys->paginationcommon();
	echo"
		</div>
		<script>
			$(document).ready(function() {


				$(\"#CoaclassCoatypeId\").change(function(){
					var coatype_id = $(this).val();
					$(\"#classsearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coaclasssearchbycoatype/'+coatype_id, {coatype_id:coatype_id},
					function(result) {
						$(\"#classsearchloading\").html(result);
					});
					$(\"#classloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/listcoagroup/'+coatype_id, {coatype_id:coatype_id},
					function(result) {
						$(\"#CoaclassCoagroupId\").html(result);
						$(\"#classloading\").html('');
					});
				});


				$(\"#CoaclassCoagroupId\").change(function(){
					var coatype_id = $(\"#CoaclassCoatypeId\").val();
					var coagroup_id = $(\"#CoaclassCoagroupId\").val();
					if(coatype_id !='' && coatype_id == 0){
						coatype_id = coatype_id;
					}else{
						coatype_id = 0;
					}
					$(\"#classsearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coaclasssearchbycoatype/'+coatype_id+'/'+coagroup_id, {coatype_id:coatype_id,coagroup_id:coagroup_id},
					function(result) {
						$(\"#classsearchloading\").html(result);
					});
					$(\"#classloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/listcoaclass/'+coatype_id+'/'+coagroup_id, {coatype_id:coatype_id, coagroup_id:coagroup_id},
					function(result) {
						$(\"#CoaclassCoaclassId\").html(result);
						$(\"#classloading\").html('');
					});
				});

				$(\"#CoaclassCoaclassId\").change(function(){
					var coatype_id = $(\"#CoaclassCoatypeId\").val();
					var coagroup_id = $(\"#CoaclassCoagroupId\").val();
					var coaclass_id = $(\"#CoaclassCoaclassId\").val();
					if(coatype_id !='' && coatype_id == 0){
						coatype_id = coatype_id;
					}else{
						coatype_id = 0;
					}
					if(coagroup_id !='' && coagroup_id == 0){
						coagroup_id = coagroup_id;
					}else{
						coagroup_id = 0;
					}
					$(\"#classsearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coaclasssearchbycoatype/'+coatype_id+'/'+coagroup_id+'/'+coaclass_id,
					 {coatype_id:coatype_id,coagroup_id:coagroup_id,coaclass_id:coaclass_id},
					function(result) {
						$(\"#classsearchloading\").html(result);
					});
				});

				$(\"#CoaclassCoaclassname\").keyup(function(){
					var coaclassname = $(this).val();
					$(\"#classsearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coaclasssearchbycoatype/0/0/'+coaclassname, {coaclassname:coaclassname},
					function(result) {
						$(\"#classsearchloading\").html(result);
					});
				});
			});
		</script>
	";
?>
