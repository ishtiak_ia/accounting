<?php
App::uses('AppModel', 'Model');
class Useremergencycontact extends AppModel {
	public $name = 'Useremergencycontact';
	public $usetables = 'useremergencycontacts';

	var $belongsTo = array(
		'User' => array(
			'fields' =>array('User.*'),
			'className'    => 'User',
			'foreignKey'    => 'user_id'
		)
	);
	var $virtualFields = array(
		'user_name' => 'CONCAT(User.username)',
		'user_fullname' => 'CONCAT(User.userfirstname, "   ", User.usermiddlename, "   ", User.userlastname)',
		'emergencycontact_name' => 'CONCAT(Useremergencycontact.useremergencycontactname, " / ",  Useremergencycontact.useremergencycontactnamebn)',
		'isActive' => 'IF(Useremergencycontact.useremergencycontactisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Useremergencycontact.useremergencycontactisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
}