<?php
App::uses('AppModel', 'Model');
class Bank extends AppModel {
	public $name = 'Bank';
	public $usetables = 'banks';
	var $belongsTo  = array(
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'bankinsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'bankupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'bankdeleteid'
		),
		'Banktype' => array(
			'fields' =>array('banktype_name'),
			'className'    => 'Banktype',
			'foreignKey'    => 'banktype_id'
		)
	);
	var $virtualFields = array(
		'bank_name' => 'CONCAT(Bank.bankname, " / ", Bank.banknamebn)',
		'banktype_name' => 'CONCAT(Banktype.banktypename, " / ", Banktype.banktypenamebn)',
		'isActive' => 'IF(Bank.bankisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Bank.bankisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
	public $validate = array(
		'bankname' => array(
			'rule' => 'notEmpty',
			'allowEmpty' => false,
			'required' => true,
			'message' => 'Enter a valid Bank Name'
		),
		'banknamebn' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'allowEmpty' => false,
			'message' => 'Enter a valid Bank Name in Bangla'
		)
	);
}

?>