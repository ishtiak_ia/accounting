<?php
	echo"
        <table>
            <tr>
                <td>
                    <h2>".__('Assign Product Branch')."</h2>
                </td>
            </tr>
            <tr>
                <td>
    ";
	echo $this->Form->create('Products', array('action' => 'productbranchassignaction'));
	$options = array(1 => __("Yes"), 0 =>__("No"));
	$attributes = array(
            'legend' => false, 
            'label' => true, 
            //'value' => (@$product[0]['Product']['productisactive'] ? $product[0]['Product']['productisactive'] : 0), 
            'class' => 'form-inline'
        );

	echo $Utilitys->tablestart();
		echo "
			<tr>
				<td>".__("Product Name")."</td>
				<td>
					".$this->Form->input(
						"Productbranch.product_id",
							array(
								'type' => 'select', 
								'options' => $product_list,
								'empty'=>__('Select Product Name'),
								'div' => false, 
                                'label' => false,
                                'optgroup label' => false,
								'data-validation-engine'=>'validate[required]',
								'class'=> 'form-control'
								//'selected'=> (@$branch[0]['Branch']['company_id'] ? $branch[0]['Branch']['company_id'] : 0)
							)
					)."
				</td>
			</tr>
			<tr>
				<td>".__("Branch Name")."</td>
				<td>
					".$this->Form->input(
						"Productbranch.branch_id",
							array(
								'type' => 'select', 
								'options' => $branch_list,
								'empty'=>__('Select Branch Name'),
								'div' => false, 
                                'label' => false,
                                'optgroup label' => false,
								'data-validation-engine'=>'validate[required]',
								'class'=> 'form-control'
								//'selected'=> (@$branch[0]['Branch']['company_id'] ? $branch[0]['Branch']['company_id'] : 0)
							)
					)."
				</td>
			</tr>
			<tr>
	            <td>".__("isActive")."</td>
	            <td>
	                ".$this->Form->radio(
	                'Productbranch.productbranchisactive',
	                $options, 
	                $attributes
	                )."
	            </td>
	        </tr>
	        <tr>
				<td colspan=\"2\">
					".$Utilitys->allformbutton('',$this->request["controller"],$page='productbranch')."
				</td>
			</tr>
		";
	echo $Utilitys->tableend();
	echo $this->Form->end();
	 echo"
                </td>
            </tr>
        </table>
    ";
 ?>
<script type="text/javascript">
 jQuery(document).ready(function(){
    jQuery("#ProductsProductbranchassignactionForm").validationEngine();
 });
</script>

