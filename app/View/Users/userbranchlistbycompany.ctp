<?php
	echo $this->Form->input(
				"User.branch_id",
				array(
					'type' => 'select', 
	            	"options" => $branches,
	            	'empty' => 'Select Branch', 
	            	'div' => false, 
	            	'label' => 'Select a Branch',
	            	"optgroup label" => false, 
	            	'style' => '',
	            	'class'=> 'form-control'
				)
	);
?>