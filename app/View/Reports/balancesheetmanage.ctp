<?php
//echo $daterange;
//echo count($balancesheet);
//pr($totalbalancesheet);
//echo "<pre>";print_r($balancesheet);echo"</pre>";
	echo "<h2>".__('Balance Sheet')."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Balancesheet.formdate",
			array(
				'type' => 'text',
				'div' => array('class'=> 'form-group'),
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __("Form Date"),
				'class'=> 'form-control',
				'style' => ''
			)
		);
		echo $this->Form->input(
			"Balancesheet.todate",
			array(
				'type' => 'text',
				'div' => array('class'=> 'form-group'),
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __(" To Date"),
				'class'=> 'form-control',
				'style' => ''
			)
		);
		/*echo $this->Form->input(
			"Bank.searchtext",
			array(
				'type' => 'text',
				'div' => array('class'=> 'form-group'),
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
			)
		);*/
	echo $Utilitys->filtertagend();
	echo"
		<div id=\"balancesheetsarchloading\">
	";
		//echo $Utilitys->paginationcommon();
		echo $Utilitys->tablestart($class=null, $id=null);
			echo $this->Html->tableHeaders(
				array(
					array(
						__('Particulars')  => array('class' => 'highlight sortable')
					),					
					array(
						__('Notes')  => array('class' => 'highlight sortable')
					), 
					array(
						__($daterange)  => array('class' => 'highlight sortable')
					)
				)
			);
			$balancesheetrows = array();
			//$countBalancesheet = 0-1+$this->Paginator->counter('%start%');
			$countBalancesheet = 0;
			$Coatype_id=0;
			$Coatype_idd=0;
			$Coagroup_id=0;
			$Coa_id=0;
			$Coatype_amount=0;
			$Coagroup_amount=0;
			foreach($balancesheet as $Thisbalancesheet):
				foreach($totalbalancesheet as $Thistotalbalancesheet):
					if($Thistotalbalancesheet["Coatype"]["id"]==$Thisbalancesheet["Coatype"]["id"]):
						$totalBalanceCOAType = $Thistotalbalancesheet[0]["gltransaction_totalbalance"]?$Thistotalbalancesheet[0]["gltransaction_totalbalance"]:0;
					endif;
				endforeach;
				$countBalancesheet++;
				if($Coatype_id!=$Thisbalancesheet["Coatype"]["id"]):
					$Coatype_amount=0;
					echo $this->Html->tableCells(
						array(
							array(
								array(
									$Thisbalancesheet["Coatype"]["coatypename"],
									'colspan="2" align="left"'
								),
								array(
									$totalBalanceCOAType,
									'colspan="1" align="right"'
								)
							)
						)
					);
					$Coatype_id=$Thisbalancesheet["Coatype"]["id"];
				endif;
				if($Coagroup_id!=$Thisbalancesheet["Coagroup"]["id"]):
					$Coagroup_amount=0;
					echo $this->Html->tableCells(
						array(
							array(
								array(
									$Thisbalancesheet["Coagroup"]["coagroupname"],
									'colspan="3" align="left"'
								)/*,
								array(
									$totalBalance,
									'colspan="1" align="right"'
								)*/
							)
						)
					);
					$Coagroup_id=$Thisbalancesheet["Coagroup"]["id"];
				endif;
					$Coatype_amount+=$Thisbalancesheet[0]["gltransaction_totalbalance"];
					$Coagroup_amount+=$Thisbalancesheet[0]["gltransaction_totalbalance"];
					echo $this->Html->tableCells(
						array(
							$Thisbalancesheet["Coa"]["coaname"],
							$Thisbalancesheet["Coa"]["coacode"],
							$Thisbalancesheet[0]["gltransaction_totalbalance"]
							
						)
					);
				/*if($Coatype_idd!=$Thisbalancesheet["Coatype"]["id"]):
					$Coatype_amount=0;
					echo $this->Html->tableCells(
						array(
							array(
								array(
									$Thisbalancesheet["Coatype"]["coatypename"],
									'colspan="2" align="left"'
								),
								array(
									$totalBalanceCOAType,
									'colspan="1" align="right"'
								),
								array(
									$totalBalanceCOAType,
									'colspan="1" align="right"'
								)
							)
						)
					);
					$Coatype_idd=$Thisbalancesheet["Coatype"]["id"];
				endif;*/
			endforeach;
			/*echo $this->Html->tableCells(
				array(array(array("Ending Balance",'colspan="5" align="left"'),array($totalBalance,'colspan="1" align="right"')))
			);*/
			/*echo $this->Html->tableCells(
				$balancesheetrows
			);*/
		echo $Utilitys->tableend();
		//echo $Utilitys->paginationcommon();
	echo"
		</div>
		<script>
			$(document).ready(function() {
				var startDate = new Date('01/01/1980');
				var FromEndDate = new Date();
				var ToEndDate = new Date();
				ToEndDate.setDate(ToEndDate.getDate()+365);

				$('#BalancesheetFormdate').datetimepicker({
					format: \"yyyy-mm-dd\",
					language: 'en',
					weekStart: 1,
					todayBtn: 1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					minView: 2,
					forceParse: 0,
					startDate: '01/01/2012',
					endDate: FromEndDate
				}).on('changeDate', function(selected){
					startDate = new Date(selected.date.valueOf());
					startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
					$('#BalancesheetTodate').datetimepicker('setStartDate', startDate);

					var BalancesheetFormdate = $(this).val();
					var BalancesheetTodate = $('#BalancesheetTodate').val();
					$(\"#balancesheetsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."reports/balancesheetsearchbydate',
						{BalancesheetFormdate:BalancesheetFormdate,BalancesheetTodate:BalancesheetTodate},
						function(result) {
							$(\"#balancesheetsarchloading\").html(result);
						}
					);
				});

				$('#BalancesheetTodate').datetimepicker({
					format: \"yyyy-mm-dd\",
					language: 'en',
					weekStart: 1,
					todayBtn: 1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					minView: 2,
					forceParse: 0,
					startDate: startDate,
					endDate: ToEndDate
				}).on('changeDate', function(selected){
					FromEndDate = new Date(selected.date.valueOf());
					FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
					$('#BalancesheetFormdate').datetimepicker('setEndDate', FromEndDate);

					var BalancesheetTodate = $(this).val();
					var BalancesheetFormdate = $('#BalancesheetFormdate').val();
					$(\"#balancesheetsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."reports/balancesheetsearchbydate',
						{BalancesheetFormdate:BalancesheetFormdate,BalancesheetTodate:BalancesheetTodate},
						function(result) {
							$(\"#balancesheetsarchloading\").html(result);
						}
					);
				});
    				/*$('#BalancesheetFormdate').datetimepicker({
					format: \"yyyy-mm-dd\",
					language: 'en',
					weekStart: 1,
					todayBtn: 1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					minView: 2,
					forceParse: 0
				});
				$('#BalancesheetTodate').datetimepicker({
					format: \"yyyy-mm-dd\",
					language: 'en',
					weekStart: 1,
					startDate: startDate,
					todayBtn: 1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					minView: 2,
					forceParse: 0
				});*/

				$(\"#BankSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#balancesheetsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/banksearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#balancesheetsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>