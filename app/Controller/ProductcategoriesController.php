<?php

App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');
class ProductcategoriesController extends AppController {
	public $name = 'Productcategories';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'Form' => array('className' => 'BootstrapForm'));
	public $components = array('RequestHandler','Session');
	public $uses = array('User', 'Usernarration', 'Group', 'Groupnarration', 'Menu', 'Menunarration', 'Privilege', 'Product', 'Productcategory', 'Unit');

}