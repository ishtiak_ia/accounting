<?php 
$userId = $this->session->read('User.id');
?>
<h2>Change Password</h2>
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('Users', array('action' => 'userchangepassword'));?>
<table class="table table-bordered table-hover">
	<tr>
		<td>Current Password</td>
		<td><?php echo $this->Form->input("User.currentpassword",array('type'=>'password','div'=>false,'label'=>false));?>
			<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$userId)); ?>
		</td>
	</tr>
	<tr>
		<td>New Password</td>
		<td><?php echo $this->Form->input("User.password",array('type'=>'password','div'=>false,'label'=>false));?></td>
	</tr>
	<tr>
		<td>Retype New Password</td>
		<td><?php echo $this->Form->input("User.repassword",array('type'=>'password','div'=>false,'label'=>false));?>
			<input type="hidden" name="passwordsubmit" />
		</td>
	</tr>
	<tr>
	    <td></td>
	    <td><?php echo $this->Form->submit('Change', array('div' => false,'align'=>'right','formnovalidate'=>true));?></td>
	</tr>
</table>
<?php echo $this->Form->end(); ?>