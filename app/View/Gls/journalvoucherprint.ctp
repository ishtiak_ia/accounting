<?php
$debit = $credit ='';
$sum_of_debit = $sum_of_credit = 0;

echo "<div id='print_page' class='right-aligned-texts'>";
    echo $Utilitys->printbutton ($printableid="loadvoucher", $searchfield=null, $pageformat="c3", $orientation="l", $unit=null, $companyname=$_SESSION["User"]["company_name"], $tabletitle="yes", $tablemonth="yes", $tabledepartment="yes", $printsectoin="printtwotable");
echo "</div>";

echo "<div id='loadvoucher'>";
	echo "<div class='center-aligned-texts'><h1>Voucher</h1></div>";
	echo "<div class='right-aligned-texts'><b>Voucher NO.</b>:".$voucher_data[0]["Journaltransaction"]["voucher_no"]."</div>";
	echo "<div  class='right-aligned-texts'><b>Date:</b>".$voucher_data[0]["Journaltransaction"]["transactiondate"]."</div>";
   	
		echo $Utilitys->tablestart($class=null, $id=null);
			echo $this->Html->tableHeaders(
				array(
					array(
						__('A/C CODE')  => array('class' => '')
					), 
					array(
						__('Chart of Account')  => array('class' => '')
					), 
					array(
						__('Debit')  => array('class' => '')
					), 
					array(
						__('Credit')  => array('class' => '')
					)
				)
			);
			$coatyperows = array();
			foreach($voucher_data as $voucher):
				if($voucher["Journaltransaction"]["transactionamount"]>=0){
					$debit = $voucher["Journaltransaction"]["transactionamount"];
					$credit = '';

				}else{
					$credit = str_replace("-", "", $voucher["Journaltransaction"]["transactionamount"]);
					$debit = '';
				}

				$sum_of_debit = $sum_of_debit + $debit;
				$sum_of_credit = $sum_of_credit + $credit;

				$coatyperows[]= array(
					$voucher["Coa"]["coacode"],
					$voucher["Coa"]["coaname"],
					$debit,
					$credit
				);


			endforeach;
			$rows[]= array(array("Total","colspan='2'"),
							$sum_of_debit ,		
							$sum_of_credit
				);
		    echo $this->Html->tableCells(
				$coatyperows
			);
			echo $this->Html->tableCells(
				$rows
			);
			echo $Utilitys->tableend();
		
		echo "<div>In word ".$Utilitys->convert_number_to_words($sum_of_debit)." Taka Only</div>";
		echo "<div>".$voucher['Journaltransaction']['description']."</div>";
	
echo "</div>";
?>