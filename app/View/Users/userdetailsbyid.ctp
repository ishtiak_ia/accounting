<?php
	$convertJoiningDate = strtotime($users[0]["User"]["userjoiningdate"]);
	$JoiningDate=date("dS M, Y", $convertJoiningDate);

	$convertBirthDate = strtotime($users[0]["User"]["userdob"]);
	$BirthDate=date("dS M, Y", $convertBirthDate);
//	print_r($useremergencycontact);
$searchfield="";
echo"
	<style>
	.DivDisplayStyle{		
		height: 30px;
		width: 33%;
		float: left;
		text-left;
		padding-left:10px;		
	}
	.DivDisplayStyle08Col{		
		height: 30px;
		width: 80%;
		float: left;
		text-left;
		padding-left:10px;		
	}
	.DivDisplayStyle04Col{		
		height: 30px;
		width: 20%;
		float: left;
		text-left;
		padding-left:10px;		
	}
	</style>
    ";
	$Utilitys->printbutton ($printableid='departmenttimesearchloading', $searchfield=null);
	echo "
		<div id=\"departmenttimesearchloading\">
	";
		echo "<div class=\"row\">";
			echo "<div class=\"col-md-12\">";
				echo "<div class=\"row\" style=\"height:10px !important;\">";
					echo"<div class=\"col-md-12 text-center\" ><h3>".__('HASSAN BOOK DEPOT')."</h3></div>";
				echo"</div>";
				echo "<div class=\"row\">";
					echo"<div class=\"col-md-12 text-center\"><h4>".__('Employee Database Information')."</h4></div>";
				echo"</div>";
				echo "<div class=\"row\">";
					echo"<div class=\"DivDisplayStyle\">
							<span >Fullname:</span>
							".$users[0]["User"]["user_fullname"]." 
					</div>";
					echo"<div class=\"DivDisplayStyle\">
							Father's Name:
							".$users[0]["User"]["userfathername"]." 
					</div>";
					echo"<div class=\"DivDisplayStyle\">
							Mother's Name:
							".$users[0]["User"]["usermothername"]."
					</div>";
					echo"<div class=\"col-md-4 text-left  DivDisplayStyle\">
							<span >Department:</span>
							".$users[0]["User"]["department_name"]." 
					</div>";
					echo"<div class=\"col-md-4 text-left  DivDisplayStyle\">
							<span >Joining Date:</span>
							".$JoiningDate." 
					</div>";
					echo"<div class=\"col-md-4 text-left  DivDisplayStyle\">
							<span >Joining Salary:</span>
							".$users[0]["User"]["userjoiningsalary"]." 
					</div>";
					echo"<div class=\"col-md-4 text-left DivDisplayStyle\">
							<span >Email:</span>
							".$users[0]["User"]["useremailaddress"] ."
					</div>";
					echo"<div class=\"col-md-4 text-left DivDisplayStyle\">
							<span >Contact No.:</span>
							".$users[0]["User"]["usertelephone"]."
					</div>";
					echo"<div class=\"col-md-4 text-left DivDisplayStyle\">
							<span >Res. Address:</span>
							".$users[0]["User"]["userpresentaddress"]."
					</div>";
					echo"<div class=\"col-md-4 text-left  DivDisplayStyle\">
							<span >Per. Address:</span>
							".$users[0]["User"]["userpermanentaddress"]."
					</div>";
					echo"<div class=\"col-md-4 text-left  DivDisplayStyle\">
							<span >Gender:</span>
							".$users[0]["User"]["usergender"]."
					</div>";
					echo"<div class=\"col-md-4 text-left  DivDisplayStyle\">
							<span >Date of birth:</span>
							".$BirthDate."
					</div>";
					echo"<div class=\"col-md-4 text-left  DivDisplayStyle\">
							<span >Blood group:</span>
							".$users[0]["User"]["userbloodgroup"]."
					</div>";
					echo"<div class=\"col-md-4 text-left  DivDisplayStyle\">
							
						</div>";
					echo"<div class=\"col-md-4 text-left  DivDisplayStyle\">
							
						</div>";
				echo "</div>";
				echo "<div class=\"row\">";
					echo"<div class=\"col-md-12 text-center\"><h4>".__('Emergency Contact information')."</h4></div>";
				echo"</div>";
				echo "<div class=\"row\">";
					echo"<div class=\"col-md-12 text-left \">";

			echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Relationship')  => array('class' => 'highlight sortable')
					),
					array(
						__('Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Date of Birth')  => array('class' => 'highlight sortable')
					)
				)
			);
			$useremergencycontactrows = array();
			$countUseremergencycontactrows = 0-1+$this->Paginator->counter('%start%');
			foreach($useremergencycontact as $Thisuseremergencycontact):
				$countUseremergencycontactrows++;
				$useremergencycontactrows[]= array($countUseremergencycontactrows,
				$Thisuseremergencycontact["Useremergencycontact"]["useremergencycontactname"],
				$Thisuseremergencycontact["Useremergencycontact"]["useremergencycontactrelationship"],
				$Thisuseremergencycontact["Useremergencycontact"]["useremergencycontacthomephone"],
				$Thisuseremergencycontact["Useremergencycontact"]["useremergencycontactmobilephone"]
				);
			endforeach;
			echo $this->Html->tableCells(
				$useremergencycontactrows
			);
			echo $Utilitys->tableend();
				echo "</div>";
				echo "</div>";
				echo "<div class=\"row\">";
					echo"<div class=\"col-md-12 text-center\"><h4>".__('Family Details')."</h4></div>";
				echo"</div>";
				echo "<div class=\"row\">";
					echo"<div class=\"col-md-6 text-left  DivDisplayStyle08Col\">";

			echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Relationship')  => array('class' => 'highlight sortable')
					),
					array(
						__('Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Date of Birth')  => array('class' => 'highlight sortable')
					)
				)
			);
			$userdependentrows = array();
			$countUserdependentrows = 0-1+$this->Paginator->counter('%start%');
			foreach($userdependent as $Thisuser):
				$countUserdependentrows++;
				$userdependentrows[]= array($countUserdependentrows,
				$Thisuser["Userdependent"]["userdependentname"],
				$Thisuser["Userdependent"]["userrelationship_name"],
				$Thisuser["Userdependent"]["userdependentdob"]
				);
			endforeach;
			echo $this->Html->tableCells(
				$userdependentrows
			);
			echo $Utilitys->tableend();
				echo "</div>";
				echo "<div class=\"col-md-6 text-right  DivDisplayStyle04Col\">
					".$this->html->image('/img/user/small/'.$users[0]["User"]["userimage"], array('alt'=> 'Picture of '.$users[0]["User"]["user_fullname"], 'width'=>100, 'height'=>100))."
					";
				echo "</div>";
			echo "</div>";
			echo "
	";
	echo $Utilitys->tableend($class=null, $id=null);
		echo "</div>";

?>
