<?php 
echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Employee Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Compamy Name')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Position')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Location')  => array('class' => 'highlight sortable')
					), 
					array(
						__('From')  => array('class' => 'highlight sortable')
					), 
					array(
						__('To')  => array('class' => 'highlight sortable')
					),
					__('Active'), 
					__('Action')
				)
			);
			$usercareerhistoryrows = array();
			$countUsercareerhistoryrows = 0-1+$this->Paginator->counter('%start%');
			foreach($usercareerhistory as $Thisusercareerhistory):
				$countUsercareerhistoryrows++;
				$usercareerhistoryrows[]= array($countUsercareerhistoryrows,$Thisusercareerhistory["Usercareerhistory"]["user_fullname"],$Thisusercareerhistory["Usercareerhistory"]["usercareerhistorycompamyname"],$Thisusercareerhistory["Usercareerhistory"]["usercareerhistorypositionheld"],$Thisusercareerhistory["Usercareerhistory"]["usercareerhistorylocation"],$Thisusercareerhistory["Usercareerhistory"]["usercareerhistoryfromdate"],$Thisusercareerhistory["Usercareerhistory"]["usercareerhistorytodate"],$Utilitys->statusURL($this->request["controller"], 'usercareerhistory', $Thisusercareerhistory["Usercareerhistory"]["id"], $Thisusercareerhistory["Usercareerhistory"]["usercareerhistoryuuid"], $Thisusercareerhistory["Usercareerhistory"]["usercareerhistoryisactive"]),$Utilitys->cusurl($this->request["controller"], 'usercareerhistory', $Thisusercareerhistory["Usercareerhistory"]["id"], $Thisusercareerhistory["Usercareerhistory"]["usercareerhistoryuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$usercareerhistoryrows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#UsercareerhistorySearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#usercareerhistorysearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#usercareerhistorysearchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>