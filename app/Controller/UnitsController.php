<?php
App::import('Controller', 'Logins');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');


class UnitsController extends AppController {
	public $name = 'Units';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'Form' => array('className' => 'BootstrapForm'));
	public $components = array('RequestHandler','Session', 'Image');
	public $uses = array('Product', 'Productcategory', 'Producttype', 'Unit', 'Unittype', 'User');

	var $Logins;
	function beforeFilter(){
		$this->Logins =& new LoginsController;
		$this->Logins->constructClasses();
		$this->Logins->__validateLoginStatus();
		//$this->recordActivity();
		// $this->Auth->allow('register');
		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	public  function beforeRender(){
		$this->response->disableCache();//prevent useless warnings for Ajax
		if($this->RequestHandler->isAjax()){
			Configure::write('debug', 0);
		}
	}

	//Start Unit Type//
	public function unittypemanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Unit Type').' | '.__(Configure::read('site_name')));

		$this->paginate = array(
			'fields' => 'Unittype.*',
			'order'  =>'Unittype.unittypename ASC',
			'limit' => 20
		);
		$unittype = $this->paginate('Unittype');
		$this->set('unittype',$unittype);
	}

	public function unittypeinsertupdate($id=null,$uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Unit Type').' | '.__(Configure::read('site_name')));
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
			$unittype=$this->Unittype->find('all',array("fields" => "Unittype.*","conditions"=>array('Unittype.id'=>$id,'Unittype.unittypeuuid'=>$uuid)));
			$this->set('unittype',$unittype);
		endif;
	}
	public function  unittypeinsertupdateaction($id=null, $uuid=null){
		$userID = $this->Session->read('User.id');
		$company_id = $_SESSION["User"]["company_id"];
		$branch_id = $_SESSION["User"]["branch_id"];
		$id = @$this->request->data['Units']['id'];
		$uuid = @$this->request->data['Units']['unittypeuuid'];
		if ($this->request->is('post')) {
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
				$this->request->data['Unittype']['unittypeupdateid']=$userID;
				$this->request->data['Unittype']['unittypeupdatedate']=date('Y-m-d H:i:s');
				$message = __('Update Successfully.');
				$this->Unittype->id = $id;
			else:
				$this->request->data['Unittype']['unittypeinsertid']=$userID;
				$this->request->data['Unittype']['unittypeinsertdate']=date('Y-m-d H:i:s');
				$this->request->data['Unittype']['unittypeuuid']=String::uuid();
				$message = __('Save Successfully.');
			endif;
			if ($this->Unittype->save($this->request->data, array('validate' => 'only'))) {
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				$this->redirect("/units/unittypemanage");
			}else{
				$errors = $this->Unittype->invalidFields();
				$this->set('errors', $this->Unittype->invalidFields());
				$this->redirect("/units/unittypeinsertupdate");
			}
		}
	}
	public function unittypedetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$unittypes=$this->Unittype->find('all',array("fields" => "Unittype.*","conditions"=>array('Unittype.id'=>$id,'Unittype.unittypeuuid'=>$uuid)));
		$this->set('unittypes',$unittypes);
	}
	public function unittypesearchbytext($Searchtext=null){
		$this->layout='ajax';
		$Searchtext = @$this->request->data['Searchtext'];
        $unittype_id = array();
        if(!empty($Searchtext) && $Searchtext!=''):
            $unittype_id[] = array(
                'OR' => array(
                    'Unittype.unittypename LIKE ' => '%'.$Searchtext.'%',
                    'Unittype.unittypenamebn LIKE ' => '%'.$Searchtext.'%'
                )
            );
        else:
            $unittype_id[] = array();
        endif;

        $this->paginate = array(
            'fields' => 'Unittype.*',
            'order'  =>'Unittype.unittypename ASC',
            "conditions"=>$unittype_id,
            'limit' => 20
        );
        $unittypes = $this->paginate('Unittype');
        $this->set('unittypes',$unittypes);
    }
	public function unittypedelete($id=null,$uuid=null){
		$userID = $_SESSION["User"]["id"];
		if ($this->Unittype->updateAll(array('unittypedeleteid'=>$userID, 'unittypedeletedate'=>"'".date('Y-m-d H:i:s')."'", 'unittypeisactive'=>2), array('AND' => array('Unittype.id'=>$id,'Unittype.unittypeuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/units/unittypemanage");
	}
	//End Unit Type//

	//Start Unit//
	public function unitmanage(){
		$this->layout='default';
		$userID = $this->Session->read('User.id');
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$this->set('title_for_layout', __('Unit').' | '.__(Configure::read('site_name')));

		$condition = array();
		if($group_id==1):
			$condition[] = array('');
		elseif($group_id==2):
			$condition[] = array('Unit.company_id'=>$company_id);
		else:
			$condition[] = array('Unit.company_id'=>$company_id);
			//$condition[] = array('Unit.branch_id'=>$branch_id);
		endif;

		$unittypes =$this->Unittype->find('list',array(
			"fields" => array("Unittype.id","unittype_name"),
			'conditions' => array('Unittype.unittypeisactive' => 1)
			));
		$this->set('unittypes', $unittypes);

		$this->paginate = array(
			'fields' => 'Unit.*, Unittype.*',
			'order'  =>'Unit.id ASC',
			'conditions' => $condition,
			'limit' => 20
		);
		$units = $this->paginate('Unit');
		$this->set('units',$units);
	}
	public function unitdetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$units=$this->Unit->find('all',array("fields" => "Unit.*, Unittype.*","conditions"=>array('Unit.id'=>$id,'Unit.unituuid'=>$uuid)));
		$this->set('units',$units);
	}
	public function unitinsertupdate($id=null,$uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Unit').' | '.__(Configure::read('site_name')));
		$unittypes =$this->Unittype->find('list',array(
			"fields" => array("Unittype.id","unittype_name"),
			'conditions' => array('Unittype.unittypeisactive' => 1)
			));
		$this->set('unittypes', $unittypes);
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
			$units=$this->Unit->find('all',array("fields" => "Unit.*, Unittype.*","conditions"=>array('Unit.id'=>$id,'Unit.unituuid'=>$uuid)));
			$this->set('units',$units);
		endif;
	}
	public function  unitinsertupdateaction($id=null, $uuid=null){
		$userID = $this->Session->read('User.id');
		$company_id = $_SESSION["User"]["company_id"];
		$branch_id = $_SESSION["User"]["branch_id"];
		$id = @$this->request->data['Units']['id'];
		$uuid = @$this->request->data['Units']['unituuid'];
		if ($this->request->is('post')) {
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
				$this->request->data['Unit']['unitupdateid']=$userID;
				$this->request->data['Unit']['unitupdatedate']=date('Y-m-d H:i:s');
				$message = __('Update Successfully.');
				$this->Unit->id = $id;
			else:
				$this->request->data['Unit']['company_id']=$company_id;
				$this->request->data['Unit']['branch_id']=$branch_id;
				$this->request->data['Unit']['unitinsertid']=$userID;
				$this->request->data['Unit']['unitinsertdate']=date('Y-m-d H:i:s');
				$this->request->data['Unit']['unituuid']=String::uuid();
				$message = __('Save Successfully.');
			endif;
			if ($this->Unit->save($this->request->data, array('validate' => 'only'))) {
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				$this->redirect("/units/unitmanage");
			}else{
				$errors = $this->Unit->invalidFields();
				$this->set('errors', $this->Unit->invalidFields());
				$this->redirect("/units/unittypeinsertupdate");
			}
		}
	}
	public function unitsearchbytext($Searchtext=null){
        $this->layout='ajax';
        $group_id=$_SESSION["User"]["group_id"];
        $company_id=$_SESSION["User"]["company_id"];
        $branch_id=$_SESSION["User"]["branch_id"];
        $Searchtext = @$this->request->data['Searchtext'];
        $UnitUnittypeId = @$this->request->data['UnitUnittypeId'];
        $unit_id = array();
        if($group_id==1):
			$unit_id[] = array('');
		elseif($group_id==2):
			$unit_id[] = array('Unit.company_id'=>$company_id);
		else:
			$unit_id[] = array('Unit.branch_id'=>$branch_id, 'Unit.company_id'=>$company_id);
		endif;
        if(!empty($UnitUnittypeId) && $UnitUnittypeId!=''):
            $unit_id[] = array('Unit.unittype_id'=>$UnitUnittypeId);
        endif;
        if(!empty($Searchtext) && $Searchtext!=''):
            $unit_id[] = array(
                'OR' => array(
                    'Unit.unitname LIKE ' => '%'.$Searchtext.'%',
                    'Unit.unitnamebn LIKE ' => '%'.$Searchtext.'%',
                    //'User.usermiddlename LIKE ' => '%'.$Searchtext.'%',
                    'Unittype.unittypename LIKE ' => '%'.$Searchtext.'%',
                    'Unittype.unittypenamebn LIKE ' => '%'.$Searchtext.'%',
                    'Unit.unitsymbol LIKE ' => '%'.$Searchtext.'%',
                    'Unit.unitsymbolbn LIKE ' => '%'.$Searchtext.'%'
                )
            );
        else:
            $unit_id[] = array();
        endif;

        $this->paginate = array(
            'fields' => 'Unit.*, Unittype.*',
            'order'  =>'Unit.unitname ASC',
            "conditions"=>$unit_id,
            'limit' => 20
        );
        $units = $this->paginate('Unit');
        $this->set('units',$units);
    }
	public function unitdelete($id=null,$uuid=null){
		$userID = $_SESSION["User"]["id"];
		if ($this->Unit->updateAll(array('unitdeletetid'=>$userID, 'unitdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'unitisactive'=>2), array('AND' => array('Unit.id'=>$id,'Unit.unituuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/units/unitmanage");
	}
	//End Unit//
}	