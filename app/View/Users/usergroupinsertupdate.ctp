<?php
	echo"
        <table>
            <tr>
                <td>
                    <h2>".__('Assign User Group')."</h2>
                </td>
            </tr>
            <tr>
                <td>
    ";
	echo $this->Form->create('Users', array('action' => 'usergroupinsertupdateaction'));
	echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$usergroup[0]['Usergroup']['id']));
	echo $this->Form->input('usergroupuuid', array('type'=>'hidden', 'value'=>@$usergroup[0]['Usergroup']['usergroupuuid']));
	echo $this->Form->input('company_id', array('type'=>'hidden'));
	echo $this->Form->input('branch_id', array('type'=>'hidden'));
	
	$options = array(1 => __("Yes"), 0 =>__("No"));
	$attributes = array(
            'legend' => false, 
            'label' => true, 
            'value' => (@$usergroup[0]['Usergroup']['usergroupisactive'] ? $usergroup[0]['Usergroup']['usergroupisactive'] : 0), 
            'class' => 'form-inline'
        );

	echo $Utilitys->tablestart();
		echo "
			<tr>
				<td>".__("User Name")."</td>
				<td>
					".$this->Form->input(
						"Usergroup.user_id",
							array(
								'type' => 'select', 
								'options' => $user_list,
								'empty'=>__('Select User Name'),
								'div' => false, 
                                'label' => false,
                                'optgroup label' => false,
								'data-validation-engine'=>'validate[required]',
								'class'=> 'form-control',
								'selected'=> (@$usergroup[0]['Usergroup']['user_id'] ? $usergroup[0]['Usergroup']['user_id'] : 0)
							)
					)."
					".$this->Html->tag('span', null, array('id' => 'useridloadspan'))."
				</td>
			</tr>
			<tr>
				<td>".__("Group Name")."</td>
				<td>
					".$this->Form->input(
						"Usergroup.group_id",
							array(
								'type' => 'select', 
								'options' => $group_list,
								'empty'=>__('Select Group Name'),
								'div' => false, 
                                'label' => false,
                                'optgroup label' => false,
								'data-validation-engine'=>'validate[required]',
								'class'=> 'form-control',
								'selected'=> (@$usergroup[0]['Usergroup']['group_id'] ? $usergroup[0]['Usergroup']['group_id'] : 0)
							)
					)."
				</td>
			</tr>
			<tr>
	            <td>".__("isActive")."</td>
	            <td>
	                ".$this->Form->radio(
	                'Usergroup.usergroupisactive',
	                $options, 
	                $attributes
	                )."
	            </td>
	        </tr>
	        <tr>
				<td colspan=\"2\">
					".$Utilitys->allformbutton('',$this->request["controller"],$page='usergroup')."
				</td>
			</tr>
		";
	echo $Utilitys->tableend();
	echo $this->Form->end();
	 echo"
                </td>
            </tr>
        </table>
    ";
 ?>
 <?php echo "
<script>
	$(\"#UsergroupUserId\").change(function(){	
				var UsergroupUserId = $(this).val();
				$(\"#useridloadspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
				       	cache: false,
				       	type: 'POST',
					url: '".$this->webroot."users/companybranchbyuser',
					data: {UsergroupUserId:UsergroupUserId},					
					dataType: \"JSON\",
					success: function(result) {
						$(\"#UsersCompanyId\").val(result.companyID);
						//var UsersCompanyId = result.companyID;
						$(\"#UsersBranchId\").val(result.branchID);
						//var UsersBranchId = result.branchID;
						
						//$(\"#price_span\").html('Price : '+quantity * productdafultprice+' TK / '+quantity * productdafultprice+' TK / '+quantity * productdafultprice+' TK');
						$(\"#useridloadspan\").html('');
					}
				});
			});
</script>
";?>
<script type="text/javascript">
 jQuery(document).ready(function(){
    jQuery("#UsersUsergroupinsertupdateactionForm").validationEngine();
 });
</script>

