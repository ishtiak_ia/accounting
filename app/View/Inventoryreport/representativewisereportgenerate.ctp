<?php
$array_order=0;
$serial_no=1;
$net_change_purchase_rate=0;
$opening_balance_purchase_price = $opening_balance_purchase_rate = $opening_balance_purchase_quantity =0;
$closing_balance_price = $closing_balance_rate = $closing_balance_quantity =0;
$total_quantity = $total_net_quantity = $total_return_quantity =0;
$total_price = $total_net_price = $total_return_price =0;

echo "<div class='center-aligned-texts'><h1>".$company_info[0]["Company"]["companyname"]."</h1></div>";
echo "<div class='center-aligned-texts'><b>".$company_info[0]["Company"]["companypresentaddress"]."</b></div>";
echo "<br><br>";
echo "<div  class='left-aligned-texts'><b>Date From:</b>".$form_date."<b> To: </b>".$to_date."</div><br>";

echo $Utilitys->tablestart($class=null, $id=null);
echo $this->Html->tableHeaders(
    array(
        array(
            __('Sl#')  => array('class' => 'center-aligned-texts')
        ),
        array(
            __('Client Code')  => array('class' => 'center-aligned-texts')
        ),
        array(
            __('Client Name')  => array('class' => 'center-aligned-texts')
        ),
        array(
            __('Product Code')  => array('class' => 'center-aligned-texts')
        ),
        array(
            __('Product Name')  => array('class' => 'center-aligned-texts')
        ),
        array(
            __('Product Type')  => array('class' => 'center-aligned-texts')
        ),
        array(
            __('Product Category')  => array('class' => 'center-aligned-texts')
        ),
        array(
            __('Quantity')  => array('class' => 'center-aligned-texts','colspan'=>'3')
        ),
        array(
            __('Price')  => array('class' => 'center-aligned-texts','colspan'=>'3')
        ),
    )
);
echo $this->Html->tableHeaders(
    array(
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('')  => array('class' => '')
        ),

        array(
            __('Sale')  => array('class' => 'center-aligned-texts')
        ),
        array(
            __('Return')  => array('class' => 'center-aligned-texts')
        ),
        array(
            __('Net Quantity')  => array('class' => 'center-aligned-texts')
        ), array(
        __('Sale')  => array('class' => 'center-aligned-texts')
    ),
        array(
            __('Back')  => array('class' => 'center-aligned-texts')
        ),
        array(
            __('Net Sale')  => array('class' => 'center-aligned-texts')
        )
    )
);
//print_r($net_change_sales);
foreach($product_info as $product):
    $client_name = $client_info[$array_order][0]["User"]["userfirstname"].' '.$client_info[$array_order][0]["User"]["usermiddlename"].' '.$client_info[$array_order][0]["User"]["userlastname"];
    //opening balance start
    $opening_balance_purchase_price = $opening_balance_purchase[$array_order][0][0]["total_opening_price"] - $opening_balance_sales[$array_order][0][0]["total_opening_price"]-$opening_balance_purchase_return[$array_order][0][0]["total_opening_price"]+$opening_balance_sales_return[$array_order][0][0]["total_opening_price"];
    $opening_balance_purchase_quantity = $opening_balance_purchase[$array_order][0][0]["total_opening_quantity"] - $opening_balance_sales[$array_order][0][0]["total_opening_quantity"]-$opening_balance_purchase_return[$array_order][0][0]["total_opening_quantity"]+$opening_balance_sales_return[$array_order][0][0]["total_opening_quantity"];
    $opening_balance_purchase_rate = $opening_balance_purchase_price/$opening_balance_purchase_quantity;
    //opening balance end

    //closing balance start
    $closing_balance_price = $opening_balance_purchase[$array_order][0][0]["total_opening_price"]+$net_change_purchase[$array_order][0][0]["total_net_change_price"]-$net_change_sales[$array_order][0][0]["total_net_change_price"]-$net_change_purchase_return[$array_order][0][0]["total_opening_price"]+$net_change_sales_return[$array_order][0][0]["total_opening_price"];
    $closing_balance_quantity = $opening_balance_purchase[$array_order][0][0]["total_opening_quantity"]+$net_change_purchase[$array_order][0][0]["total_net_change_quantity"]-$net_change_sales[$array_order][0][0]["total_net_change_quantity"]-$net_change_purchase_return[$array_order][0][0]["total_opening_quantity"]+$net_change_sales_return[$array_order][0][0]["total_opening_quantity"];
    $closing_balance_rate = $closing_balance_price/$closing_balance_quantity;
    //closing balance end

    $coatyperows[]= array(
        $serial_no,
        $product["Order"]["client_id"],
        $client_name,
        $product["Product"]["productcode"],
        $product["Product"]["productname"],
        $product["Producttype"]["producttypename"],
        $product["Productcategory"]["productcategoryname"],
        $net_change_sales[$array_order][0][0]["total_net_change_quantity"],
        $net_change_sales_return[$array_order][0][0]["total_opening_quantity"],
        $net_change_sales[$array_order][0][0]["total_net_change_quantity"]- $net_change_sales_return[$array_order][0][0]["total_opening_quantity"],
        array(round($net_change_sales[$array_order][0][0]["total_net_change_price"],2),array('class' => 'right-aligned-texts')),
        array(round($net_change_sales_return[$array_order][0][0]["total_opening_price"],2),array('class' => 'right-aligned-texts')),
        array(round(($net_change_sales[$array_order][0][0]["total_net_change_price"] - $net_change_sales_return[$array_order][0][0]["total_opening_price"]),2),array('class' => 'right-aligned-texts'))
    );


    $serial_no++;
    $array_order++;
endforeach;

echo $this->Html->tableCells(
    $coatyperows
);

echo $Utilitys->tableend();


echo "<legend><h3>Summary</h3></legend>";
echo $Utilitys->tablestart($class=null, $id=null);
echo $this->Html->tableHeaders(
    array(
        array(
            __('Category')  => array('class' => 'center-aligned-texts')
        ),
        array(
            __('Quantity')  => array('class' => 'center-aligned-texts','colspan'=>'3')
        ),
        array(
            __('Price')  => array('class' => 'center-aligned-texts','colspan'=>'3')
        )
    )
);
echo $this->Html->tableHeaders(
    array(
        array(
            __('')  => array('class' => '')
        ),
        array(
            __('Sale')  => array('class' => 'center-aligned-texts')
        ),
        array(
            __('Return')  => array('class' => 'center-aligned-texts')
        ),
        array(
            __('Net Quantity')  => array('class' => 'center-aligned-texts')
        ),
        array(
            __('Sale')  => array('class' => 'center-aligned-texts')
        ),
        array(
            __('Back')  => array('class' => 'center-aligned-texts')
        ),
        array(
            __('Net Sale')  => array('class' => 'center-aligned-texts')
        )
    )
);

foreach($category_sell_summary as $cat_sell_sumarry){
     $summary_rows[]= array(
        $cat_sell_sumarry["category"],
        $cat_sell_sumarry["quantity"],
        $cat_sell_sumarry["return_quantity"],
        $cat_sell_sumarry["net_quantity"],
        array($cat_sell_sumarry["price"],array('class' => 'right-aligned-texts')),
        array($cat_sell_sumarry["return_price"],array('class' => 'right-aligned-texts')),
        array($cat_sell_sumarry["net_price"],array('class' => 'right-aligned-texts'))
     );

    $total_quantity = $total_quantity + $cat_sell_sumarry["quantity"];
    $total_net_quantity = $total_net_quantity + $cat_sell_sumarry["net_quantity"];
    $total_return_quantity = $total_return_quantity +   $cat_sell_sumarry["return_quantity"];

    $total_price = $total_price + $cat_sell_sumarry["price"];
    $total_net_price = $total_net_price + $cat_sell_sumarry["net_price"];
    $total_return_price =$total_return_price +  $cat_sell_sumarry["return_price"];
}

$rows[]= array("Grand Total",
    $total_quantity ,
    $total_return_quantity,
    $total_net_quantity,
    array(round($total_price,2),array('class' => 'right-aligned-texts')),
    array(round($total_return_price,2),array('class' => 'right-aligned-texts')),
    array(round($total_net_price,2),array('class' => 'right-aligned-texts'))
);

echo $this->Html->tableCells(
    $summary_rows
);

echo $this->Html->tableCells(
    $rows
);

echo $Utilitys->tableend();
?>
