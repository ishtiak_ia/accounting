<?php 
echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Employee Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Achievement')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Type')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Amount')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Description')  => array('class' => 'highlight sortable')
					),
					array(
						__('Date')  => array('class' => 'highlight sortable')
					),
					__('Active'), 
					__('Action')
				)
			);
			$userachievementrows = array();
			$countUserachievementrows = 0-1+$this->Paginator->counter('%start%');
			foreach($userachievement as $Thisuserachievement):
				$countUserachievementrows++;
				$userachievementrows[]= array($countUserachievementrows,$Thisuserachievement["Userachievement"]["user_fullname"],$Thisuserachievement["Userachievement"]["userachievementname"],$Thisuserachievement["Userachievement"]["achievementStatus"],$Thisuserachievement["Userachievement"]["userachievementamount"],$Thisuserachievement["Userachievement"]["userachievementdescription"],$Thisuserachievement["Userachievement"]["userachievementdate"],$Utilitys->statusURL($this->request["controller"], 'userachievement', $Thisuserachievement["Userachievement"]["id"], $Thisuserachievement["Userachievement"]["userachievementuuid"], $Thisuserachievement["Userachievement"]["userachievementisactive"]),$Utilitys->cusurl($this->request["controller"], 'userachievement', $Thisuserachievement["Userachievement"]["id"], $Thisuserachievement["Userachievement"]["userachievementuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$userachievementrows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#UserachievementSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#userachievementsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#userachievementsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>