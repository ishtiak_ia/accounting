<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart($class=null, $id=null);
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('Account')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Account BN')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Number')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Bank')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Branch')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Account Type')  => array('class' => 'highlight sortable')
				), 
				__('Active'), 
				__('Action')
			)
		);
		$bankaccountrows = array();
		$countBankaccount = 0-1+$this->Paginator->counter('%start%');
		foreach($bankaccount as $Thisbankaccount):
			$countBankaccount++;
			$bankaccountrows[]= array($countBankaccount,$Thisbankaccount["Bankaccount"]["bankaccountname"],$Thisbankaccount["Bankaccount"]["bankaccountnamebn"],$Utilitys->amountDetailsURL($Thisbankaccount["Bankaccount"]["bankaccountnumber"], $Thisbankaccount["Bankaccount"]["id"], "Banktransaction","bankaccount_id"),$Utilitys->amountDetailsURL($Thisbankaccount["Bankaccount"]["bank_name"], $Thisbankaccount["Bankaccount"]["bank_id"], "Banktransaction","bank_id"),$Utilitys->amountDetailsURL($Thisbankaccount["Bankaccount"]["bankbranch_name"], $Thisbankaccount["Bankaccount"]["bankbranch_id"], "Banktransaction","bankbranch_id"),$Thisbankaccount["Bankaccount"]["bankaccounttype_name"],$Utilitys->statusURL($this->request["controller"], 'bankaccount', $Thisbankaccount["Bankaccount"]["id"], $Thisbankaccount["Bankaccount"]["bankaccountuuid"], $Thisbankaccount["Bankaccount"]["bankaccountisactive"]),$Utilitys->cusurl($this->request["controller"], 'bankaccount', $Thisbankaccount["Bankaccount"]["id"], $Thisbankaccount["Bankaccount"]["bankaccountuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$bankaccountrows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#BankaccountSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#bankaccountsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#bankaccountsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>