<?php
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"2\">
				<h2>".__('Stocklocation')."</h2>
			</td>
		</tr>
		<tr>
			<th>
				".__('Stocklocation')."
			</th>
			<td>
				".$stocklocation[0]["Stocklocation"]["stocklocationname"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Stocklocation BN")."
			</th>
			<td>
				".$stocklocation[0]["Stocklocation"]["stocklocationnamebn"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Address")."
			</th>
			<td>
				".$stocklocation[0]["Stocklocation"]["stocklocationaddress"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Active")."
			</th>
			<td>
				".$stocklocation[0]["Stocklocation"]["isActive"]."
			</td>
		</tr>
	";
	echo $Utilitys->tableend();
?>