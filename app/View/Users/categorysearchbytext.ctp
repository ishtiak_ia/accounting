<?php
echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Category Name')  => array('class' => 'highlight sortable')
					),
					__('Active'), 
					__('Action')
				)
			);
			$categoryrows = array();
			$countCategory = 0-1+$this->Paginator->counter('%start%');
			foreach($category as $Thiscategory):
				$countCategory++;
				$categoryrows[]= array($countCategory,$Thiscategory["Category"]["categoryname"],$Utilitys->statusURL($this->request["controller"], 'category', $Thiscategory["Category"]["id"], $Thiscategory["Category"]["categoryuuid"], $Thiscategory["Category"]["categoryisactive"]),$Utilitys->cusurl($this->request["controller"], 'category', $Thiscategory["Category"]["id"], $Thiscategory["Category"]["categoryuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$categoryrows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#CategorySearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#categorysearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#categorysearchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>