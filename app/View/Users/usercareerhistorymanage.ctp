<?php
	echo "<h2>".__('Previous Career History'). $Utilitys->addurl($title=__("Add New Career History"), $this->request['controller'], $page="usercareerhistory")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Usercareerhistory.user_id",
			array(
				'type'=>'select',
				"options"=>array($user_list),
				'empty'=>__('Select Employee'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
				)
			)
		.$this->Form->input(
			"Usercareerhistory.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"usercareerhistorysearchloading\">
	";
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Employee Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Compamy Name')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Position')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Location')  => array('class' => 'highlight sortable')
					), 
					array(
						__('From')  => array('class' => 'highlight sortable')
					), 
					array(
						__('To')  => array('class' => 'highlight sortable')
					),
					__('Active'), 
					__('Action')
				)
			);
			$usercareerhistoryrows = array();
			$countUsercareerhistoryrows = 0-1+$this->Paginator->counter('%start%');
			foreach($usercareerhistory as $Thisusercareerhistory):
				$countUsercareerhistoryrows++;
				$usercareerhistoryrows[]= array($countUsercareerhistoryrows,$Thisusercareerhistory["Usercareerhistory"]["user_fullname"],$Thisusercareerhistory["Usercareerhistory"]["usercareerhistorycompamyname"],$Thisusercareerhistory["Usercareerhistory"]["usercareerhistorypositionheld"],$Thisusercareerhistory["Usercareerhistory"]["usercareerhistorylocation"],$Thisusercareerhistory["Usercareerhistory"]["usercareerhistoryfromdate"],$Thisusercareerhistory["Usercareerhistory"]["usercareerhistorytodate"],$Utilitys->statusURL($this->request["controller"], 'usercareerhistory', $Thisusercareerhistory["Usercareerhistory"]["id"], $Thisusercareerhistory["Usercareerhistory"]["usercareerhistoryuuid"], $Thisusercareerhistory["Usercareerhistory"]["usercareerhistoryisactive"]),$Utilitys->cusurl($this->request["controller"], 'usercareerhistory', $Thisusercareerhistory["Usercareerhistory"]["id"], $Thisusercareerhistory["Usercareerhistory"]["usercareerhistoryuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$usercareerhistoryrows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();

	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				$(\"#UsercareerhistorySearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#usercareerhistorysearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usercareerhistorysearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#usercareerhistorysearchloading\").html(result);
						}
					);
				});
				$(\"#UsercareerhistoryUserId\").change(function(){
					var UsercareerhistoryUserId = $(\"#UsercareerhistoryUserId\").val();
					$(\"#usercareerhistorysearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usercareerhistorysearchbytext',
						{UsercareerhistoryUserId:UsercareerhistoryUserId},
						function(result) {
							$(\"#usercareerhistorysearchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>