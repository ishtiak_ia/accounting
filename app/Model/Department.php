<?php
App::uses('AppModel', 'Model');
class Department extends AppModel {
	public $name = 'Department';
	public $usetables = 'departments';

	var $belongsTo = array(
		'Group' => array(
			'fields' =>array('groupname'),
			'className'    => 'Group',
			'foreignKey'    => 'group_id'
		),
		'Company' => array(
			'fields' =>array('Company.*'),
			'className'    => 'Company',
			'foreignKey'    => 'company_id'
		)
	);
	var $virtualFields = array(
		'department_name' => 'CONCAT(Department.departmentname, " / ", Department.departmentnamebn)',
		'company_fullname' => 'CONCAT(Company.companyname, " / ", Company.companynamebn)',
		'group_name' => 'CONCAT(Group.groupname)',
		'isActive' => 'IF(Department.departmentisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Department.departmentisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
}