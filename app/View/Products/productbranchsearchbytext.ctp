<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
		echo $this->Html->tableHeaders(
			array(
				__('SL#'),
				array(
					__('Product Name')  => array('class' => 'highlight sortable')
				),
				array(
					__('Branch Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Company Name')  => array('class' => 'highlight sortable')
				),
				array(
					__('Is Active')  => array('class' => 'highlight sortable')
				),
				__('Action')
			)
		);
		$productbranchrows = array();
		$countProductBranch = 0-1+$this->Paginator->counter('%start%');
		
		foreach($productbranch as $Thisproductbranch):
			$countProductBranch++;
			$productbranchrows[]= array($countProductBranch,$Thisproductbranch["Productbranch"]["product_name"],$Thisproductbranch["Productbranch"]["branch_name"], $Thisproductbranch["Productbranch"]["company_name"], $Utilitys->statusURL($this->request["controller"], 'productbranch', $Thisproductbranch["Productbranch"]["id"], $Thisproductbranch["Productbranch"]["productbranchuuid"], $Thisproductbranch["Productbranch"]["productbranchisactive"]),$Utilitys->cusurl($this->request['controller'], 'productbranch', $Thisproductbranch["Productbranch"]["id"], $Thisproductbranch["Productbranch"]["productbranchuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$productbranchrows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var ProductbranchCompanyId = $(\"#ProductbranchCompanyId\").val();
					var ProductbranchBranchId = $(\"#ProductbranchBranchId\").val();
					var ProductbranchProductId = $(\"#ProductbranchProductId\").val();
					var Searchtext = $('#productbranchsarchloading').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext,ProductbranchCompanyId:ProductbranchCompanyId,ProductbranchBranchId:ProductbranchBranchId,ProductbranchProductId:ProductbranchProductId},
						async: true,
						beforeSend: function(){
							$(\"#stocklocationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#productbranchsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";

?>