<?php
App::import('Controller', 'Logins');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');
class BranchesController extends AppController {

    public $name = 'Branches';
    public $helpers = array('Html', 'Form', 'Paginator', 'Session');
    public $components = array('RequestHandler','Session', 'Image');
    public $uses = array('Company', 'Companyphone', 'Companyphonenaration', 'Companyynaration', 'Companyemail', 'Companyemailnaration','Branch','Branchemail','Branchphone');

    var $Logins;
    // public function index() {
    //     $this->redirect("login");
    // }
function beforeFilter(){
    $this->Logins =& new LoginsController;
    $this->Logins->constructClasses();
    $this->Logins->__validateLoginStatus();
    //$this->__validateLoginStatus();
    //$this->recordActivity();
    // $this->Auth->allow('register');
    $Utilitys = new UtilitysController;
    $this->set('Utilitys',$Utilitys);
    if(!isset($_SESSION["User"]["id"])){
      $this->redirect("/logins/login");
    }
  }
    public function branchinsertupdate($id=null){
      $this->layout='default';
      //pr($_SESSION);
      $userID = $this->Session->read('User.id');
      $company_id=$_SESSION["User"]["company_id"];
      $branch_id=$_SESSION["User"]["branch_id"];
      $group_id=$_SESSION["User"]["group_id"];
      $condition = array();
      if($group_id==1):
        $condition[] = array('');
      elseif($group_id==2):
        $condition[] = array('Company.id'=>$company_id);
      else:
        $condition[] = array('Company.id'=>$company_id);
        //$condition_productcategory[] = array('Product.branch_id'=>$branch_id);
      endif;
        $company=$this->Company->find('list',array(
          "fields" => array("Company.id","company_name"),
          'conditions' => array('Company.companyisactive' => 1, $condition)
          ));
      $this->set('company',$company);
       if(!empty($id) && $id!=0):
       /// echo "helo";exit();
            $branch=$this->Branch->find('all',array("fields" => "Branch.*, Branchemail.*, Branchphone.*","conditions"=>array('Branch.id'=>$id)));
            $this->set('branch',$branch);
            if($branch != null){
              $branchemail = explode(",", $branch[0]['Branch']['branch_email']);
              $this->set('branchemail',$branchemail);
              $branchphone = explode(",", $branch[0]['Branch']['branch_phone']);
              $this->set('branchphone',$branchphone);
            }
            //$branchemail=$this->Branchemail->find('all',array("fields" => "Branchemail.*","conditions"=>array('Branchemail.branch_id'=>$id)));
           // print_r($branch);
            
            //$branchphone=$this->Branchphone->find('all',array("fields" => "Branchphone.*","conditions"=>array('Branchphone.branch_id'=>$id)));
            
            
        endif;
    }
    public function  branchinsertupdateaction($id=null){
      $id = @$this->request->data['Branches']['id'];
      $uuid = @$this->request->data['Branches']['branchuuid'];
      $userID = $this->Session->read('User.id');
      $groupID=$_SESSION["User"]["group_id"];
      $company_id=$_SESSION["User"]["company_id"];
      $branch_id=$_SESSION["User"]["branch_id"];
      $branch_name = $this->request->data['Branch']['branchname'];
      $branch_namebn = $this->request->data['Branch']['branchnamebn'];
      $branch_code = $this->request->data['Branch']['branchcode'];

      if(!empty($id) && !empty($uuid)){
        $count_branch_name = 0; 
        $count_branch_code = 0;  
      } else{
        $count_branch_name = $this->Branch->find('all', array("fields" => "Branch.*", "conditions" => array("Branch.branchname" => $branch_name, "Branch.branchnamebn" => $branch_namebn, "Branch.company_id" => $company_id)));
        $count_branch_name = count( $count_branch_name);
        $count_branch_code = $this->Branch->find('all', array("fields" => "Branch.*", "conditions" => array("Branch.branchcode" => $branch_code, "Branch.company_id" => $company_id)));
        $count_branch_code = count( $count_branch_code);
      }
      if(($count_branch_name > 0) && ($count_branch_code > 0)){
          $this->Session->setFlash(__("This Branch Name And Branch Code Already Exists"), 'default', array('class' => 'alert alert-warning'));
          $this->redirect("/branches/branchinsertupdate");  
      }elseif($count_branch_name > 0){
          $this->Session->setFlash(__("This Branch Name Already Exists"), 'default', array('class' => 'alert alert-warning'));
          $this->redirect("/branches/branchinsertupdate"); 
      }elseif($count_branch_code > 0){
          $this->Session->setFlash(__("This Branch Code Already Exists"), 'default', array('class' => 'alert alert-warning'));
          $this->redirect("/branches/branchinsertupdate"); 
      } else{
            if ($this->request->is('post')){
              if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
                  $this->request->data['Branch']['branchupdateid']=$userID;
                  $this->request->data['Branch']['branchupdatedate']=date('Y-m-d H:i:s');
                  $message = __('Update Successfully.');
                  $this->Branch->id = $id;
                  $this->Branch->save($this->data);
                  $this->Session->setFlash('Update Successfully.', 'default', array('class' => 'alert alert-success'));
                  $this->Branchphone->deleteAll(array('Branchphone.branch_id' => $id), false);
                  $this->Branchemail->deleteAll(array('Branchemail.branch_id' => $id), false);
                  
                  if(!empty($this->request->data['mytext'])){
                       $count = count($this->request->data['mytext']);
                         for($i = 0 ;$i<$count; $i++){
                          $this->request->data['Branchemail']['branchemailaddress'] = $this->request->data['mytext'][$i];
                          $this->request->data['Branchemail']['company_id'] = $company_id;
                          $this->request->data['Branchemail']['branch_id'] = $id;
                            
                            $this->Branchemail->create();
                           
                           $this->Branchemail->save($this->data);
                        }
                  }
                  if(!empty($this->request->data['mytext1'])){
                       $count1 = count($this->request->data['mytext1']);
                         for($i = 0 ;$i<$count1; $i++){
                          $this->request->data['Branchphone']['branchphoneno'] = $this->request->data['mytext1'][$i];
                          $this->request->data['Branchphone']['company_id'] = $company_id;
                          $this->request->data['Branchphone']['branch_id'] = $id;
                          $this->Branchphone->create();
                          $this->Branchphone->save($this->data);
                        }
                  }

                
              }
              else{

              $now = date('Y-m-d H:i:s');
              $this->request->data['Branch']['branchinsertid']=$userID;
              $this->request->data['Branch']['branchinsertdate'] = $now;
              $this->request->data['Branch']['branchuuid']=String::uuid();
              $message = __('Save Successfully.');


              if ($this->Branch->save($this->data)) {

                  if(empty($id) && empty($uuid)){
                  $insertedid = $this->Branch->getLastInsertId();
                  $this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
              
              if(!empty($this->request->data['mytext'])){
               $count = count($this->request->data['mytext']);
                 for($i = 0 ;$i<$count; $i++){
                  $this->request->data['Branchemail']['branchemailaddress'] = $this->request->data['mytext'][$i];
                  $this->request->data['Branchemail']['company_id'] = $company_id;
                  $this->request->data['Branchemail']['branch_id'] = $insertedid;
                    
                    $this->Branchemail->create();
                    //echo "save";
                   $this->Branchemail->save($this->data);
                }
              }
               if(!empty($this->request->data['mytext1'])){
               $count1 = count($this->request->data['mytext1']);
                 for($i = 0 ;$i<$count1; $i++){
                  $this->request->data['Branchphone']['branchphoneno'] = $this->request->data['mytext1'][$i];
                  $this->request->data['Branchphone']['company_id'] = $company_id;
                  $this->request->data['Branchphone']['branch_id'] = $insertedid;
                    $this->Branchphone->create();
                    //echo "save";
                   $this->Branchphone->save($this->data);
                }
              }
          }
          }
        }
      } 
        $this->redirect("branchmanage"); 
      }    
    }
    public function branchmanage(){
      $company_id=$_SESSION["User"]["company_id"];
      $branch_id=$_SESSION["User"]["branch_id"];
      $group_id=$_SESSION["User"]["group_id"];
      $user_id=$_SESSION["User"]["id"];
      $condition = array();
      $condition2 = array();
      if($group_id==1):
        elseif($group_id==2):
       $condition[] = array('Branch.company_id'=>$company_id);
        $condition2[] = array('Company.id'=>$company_id);
        else:
        $condition[] = array('Branch.company_id'=>$company_id);
        $condition[] = array('Branch.id'=>$branch_id);
        $condition2[] = array('Company.id'=>$company_id);
      endif;
      $branchlist = $this->Branch->find('list',array("fields" => array("Branch.id","branch_name"),
                                                   "conditions" => $condition));
     
      $this->set('branchlist',$branchlist);
      $company=$this->Company->find('list',array("fields" => array("Company.id","company_name"),
                                                 "conditions" => $condition2));
      $this->set('company',$company);
        $this->paginate = array(
            'fields' => array("Branch.*","company_name"),
            'conditions' => array($condition,$condition2),
            'order'  =>'Branch.branchname ASC',
            'limit' => 20
        );
        $branches = $this->paginate('Branch');
        //print_r($branches);exit();
        $this->set('branches',$branches);

    }
    public function branchdelete($id=null){
        $userID = $this->Session->read('User.id');
        $this->request->data['Branch']['branchdeleteid']=$userID;
        $this->request->data['Branch']['branchdeletedate']=date('Y-m-d H:i:s');
        $this->request->data['Branch']['branchisactive']=2;
        $this->Branch->id = $id;
        if ($this->Branch->save($this->data)) {
            $this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'success'));
        }
        $this->redirect("/Branch/branchmanage");
    }

	public function branchdetailsbyid($id=null){
		$branch=$this->Branch->find('all',array("fields" => "Branch.*","conditions"=>array('Branch.id'=>$id)));
		$this->set('branch',$branch);
	}

	public function listbranchbycompany($id=null){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$company_id = array();
		if(!empty($id) && $id!=0):
			$company_id[] = array('Branch.company_id' => $id);
		else:
			$company_id[] = array();
		endif;
		$branch=$this->Branch->find(
			'list',array(
				"fields" => array("Branch.id","branch_name"),
				"conditions" => $company_id
			)
		);
			echo "<option value=\"0\">Select Branch</option>";
		foreach($branch as $key => $val) {
			echo "<option value=\"".$key."\">".$val."</option>";
		}
	}

  public function branchsearchbytext(){
    $this->layout = 'ajax';
    $group_id=$_SESSION["User"]["group_id"];
    $company_id=$_SESSION["User"]["company_id"];
    $branchID=$_SESSION["User"]["branch_id"];
    //$Searchtext = @$this->request->data['Searchtext'];
    $BranchBranchId = @$this->request->data['BranchBranchId'];
    $BranchCompanyId = @$this->request->data['BranchCompanyId'];
    
        //$ProductUnitId = @$this->request->data['ProductUnitId'];

        $branch_id = array();
        if($group_id==1):
        $branch_id[] = array('');
        elseif($group_id==2):
          $branch_id[] = array('Branch.company_id'=>$company_id);
        else:
          $branch_id[] = array('Branch.company_id'=>$company_id);
          $branch_id[] = array('Branch.id'=>$branchID);
        endif;
        if(!empty($BranchBranchId) && $BranchBranchId!=''):
            $branch_id = array('Branch.id'=>$BranchBranchId);

        endif;
        if(!empty($BranchCompanyId) && $BranchCompanyId!=''):
            $branch_id = array('Branch.company_id'=>$BranchCompanyId);

        endif;
       
        // if(!empty($Searchtext) && $Searchtext!=''):
        //     $product_id[] = array(
        //         'OR' => array(
        //             'Product.productname LIKE ' => '%'.$Searchtext.'%',
        //             'Product.productnamebn LIKE ' => '%'.$Searchtext.'%',
        //             'Productcategory.productcategoryname LIKE ' => '%'.$Searchtext.'%',
        //             'Productcategory.productcategorynamebn LIKE ' => '%'.$Searchtext.'%',
        //             'Unit.unitname LIKE ' => '%'.$Searchtext.'%',
        //             'Unit.unitnamebn LIKE ' => '%'.$Searchtext.'%'
        //         )
        //     );
        // else:
        //     $product_id[] = array();
        // endif;

        $this->paginate = array(
            'fields' => array("Branch.*","company_name"),
            //'order'  =>'Product.productname ASC',
            "conditions"=>$branch_id,
            'limit' => 20
        );
        $branch = $this->paginate('Branch');
        //print_r($branch);exit();
        $this->set('branch',$branch);
  }

}

?>
