<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('Product Code')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Product Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Product Name BN')  => array('class' => 'highlight sortable')
				),
				array(
					__('Product Category')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Product Type')  => array('class' => 'highlight sortable')
				),
				array(
					__('Product Price')  => array('class' => 'highlight sortable')
				),
				array(
					__('Format')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Is Active')  => array('class' => 'highlight sortable')
				),
				__('Action')
			)
		);
		$productrows = array();
		$countProduct = 0-1+$this->Paginator->counter('%start%');
		
		foreach($product as $Thisproduct):
			$countProduct++;
			$productrows[]= array($countProduct,$Thisproduct["Product"]["productcode"],$Thisproduct["Product"]["productname"],$Thisproduct["Product"]["productnamebn"],$Thisproduct["Productcategory"]["productcategoryname"]. " / " .$Thisproduct["Productcategory"]["productcategorynamebn"],$Thisproduct["Producttype"]["producttypename"]. " / " .$Thisproduct["Producttype"]["producttypenamebn"],$Thisproduct["Product"]["productprice"],$Thisproduct["Product"]["format"],$Utilitys->statusURL($this->request["controller"], 'product', $Thisproduct["Product"]["id"], $Thisproduct["Product"]["productuuid"], $Thisproduct["Product"]["productisactive"]),$Utilitys->cusurl($this->request['controller'], 'product', $Thisproduct["Product"]["id"], $Thisproduct["Product"]["productuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$productrows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#ProductSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#productsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#productsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>