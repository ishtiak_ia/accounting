<?php echo $this->Form->create('Units', array('action' => 'unittypeinsertupdateaction', 'type' => 'file'));
echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$unittype[0]['Unittype']['id']));
echo $this->Form->input('unittypeuuid', array('type'=>'hidden', 'value'=>@$unittype[0]['Unittype']['unittypeuuid']));
 ?>
<fieldset>
	<legend>Add Unit Type Information</legend>
	<table>
		<tr>
            <td>Unit Type Name:</td>
            <td><?php echo $this->Form->input("Unittype.unittypename", array('type' => 'text', 'div' => false, 'label' => false, 'value'=>@$unittype[0]['Unittype']['unittypename'])); ?>

            </td>
        </tr>
        <tr>
            <td>Unit Type Name BN:</td>
            <td><?php echo $this->Form->input("Unittype.unittypenamebn", array('type' => 'text', 'div' => false, 'label' => false, 'value'=>@$unittype[0]['Unittype']['unittypenamebn'])); ?>
            </td>
        </tr>
        <tr>
            <td>Is Active :</td>
            <td><?php
                $options = array( 1 => __("Yes"), 0 => __("No"));
                $attributes = array(
                    'legend' => false, 
                    'value' => (@$unittype[0]['Unittype']['unittypeisactive'] ? $unittype[0]['Unittype']['unittypeisactive'] : 0), 
                        'class' => 'form-inline'
                    );
                echo $this->Form->radio('Unittype.unittypeisactive', $options, $attributes);
                ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><?php echo $this->Form->submit('Save', array('div' => false, 'align' => 'right', 'formnovalidate' => true)); ?>
               <?php echo $this->Form->input('unittypesubmit', array('type' => 'hidden','value'=>'unittypesubmit'));?>
            </td>
        </tr>
	</table>
</fieldset>
<?php echo $this->Form->end(); ?> 