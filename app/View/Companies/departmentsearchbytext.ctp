<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
		echo $this->Html->tableHeaders(
			array(
				__('SL#'),
				array(
					__('Department Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Department Name BN')  => array('class' => 'highlight sortable')
				),
				array(
					__('Company')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Group')  => array('class' => 'highlight sortable')
				),
				array(
					__('Is Active')  => array('class' => 'highlight sortable')
				),
				__('Action')
			)
		);
		$departmentrows = array();
		$countDepartment = 0-1+$this->Paginator->counter('%start%');
		
		foreach($departments as $Thisdepartment):
			$countDepartment++;
			$departmentrows[]= array($countDepartment,$Thisdepartment["Department"]["departmentname"],$Thisdepartment["Department"]["departmentnamebn"],$Thisdepartment["Department"]["company_fullname"],$Thisdepartment["Group"]["groupname"],$Utilitys->statusURL($this->request["controller"], 'department', $Thisdepartment["Department"]["id"], $Thisdepartment["Department"]["departmentuuid"], $Thisdepartment["Department"]["departmentisactive"]),$Utilitys->cusurl($this->request['controller'], 'department', $Thisdepartment["Department"]["id"], $Thisdepartment["Department"]["departmentuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$departmentrows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#DepartmentSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#departmentsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#departmentsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>