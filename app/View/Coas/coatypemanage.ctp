<?php
	echo "<h2>".__('Chart of Account Type'). $Utilitys->addurl($title=__("Add New Chart of Account Type"), $this->request["controller"], $page="coatype")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Coatype.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
			)
		);
	echo $Utilitys->filtertagend();
	echo "<div id=\"coatypesearchhloading\">";
		echo $Utilitys->paginationcommon();
		echo $Utilitys->tablestart($class=null, $id=null);
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('CODE')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Type')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Type BN')  => array('class' => 'highlight sortable')
					), 
					__('Active'), 
					__('Action')
				)
			);
			$coatyperows = array();
			$countCoatype = 0-1+$this->Paginator->counter('%start%');
			foreach($coatype as $Thiscoatype):
				$countCoatype++;
				$coatyperows[]= array(
					$Utilitys->numberLangConverter($lan=null, $countCoatype
					),
					$Thiscoatype["Coatype"]["coatypecode"],
					$Thiscoatype["Coatype"]["coatypename"],
					$Thiscoatype["Coatype"]["coatypenamebn"],$Utilitys->statusURL($this->request["controller"], 'coatype', $Thiscoatype["Coatype"]["id"], $Thiscoatype["Coatype"]["coatypeuuid"], $Thiscoatype["Coatype"]["coatypeisactive"]),
					$Utilitys->cusurl($this->request["controller"], 'coatype', $Thiscoatype["Coatype"]["id"], $Thiscoatype["Coatype"]["coatypeuuid"]
					 )
				);
			endforeach;
			echo $this->Html->tableCells(
				$coatyperows
			);
		echo $Utilitys->tableend();
		echo $Utilitys->paginationcommon();
	echo "</div>";
	echo "
		<script>
			$(document).ready(function() {
				$(\"#CoatypeSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#coatypesearchhloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."coas/coatypesearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#coatypesearchhloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>