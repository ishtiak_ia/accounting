<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('Unit Type Name')  => array('class' => 'highlight sortable')
				),
				array(
					__('Is Active')  => array('class' => 'highlight sortable')
				),
				__('Action')
			)
		);
		$unittyperows = array();
		$countUnittype = 0-1+$this->Paginator->counter('%start%');
		foreach($unittypes as $Thisunittype):
			$countUnittype++;
			$unittyperows[]= array($countUnittype,$Thisunittype["Unittype"]["unittype_name"],$Utilitys->statusURL($this->request["controller"], 'unittype', $Thisunittype["Unittype"]["id"], $Thisunittype["Unittype"]["unittypeuuid"], $Thisunittype["Unittype"]["unittypeisactive"]),$Utilitys->cusurl($this->request['controller'], 'unittype', $Thisunittype["Unittype"]["id"], $Thisunittype["Unittype"]["unittypeuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$unittyperows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#UnitSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#unitsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#unitsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>