<?php
App::uses('AppModel', 'Model');
class Category extends AppModel {
	public $name = 'Category';
	public $usetables = 'categories';

	var $virtualFields = array(
		'category_name' => 'CONCAT(Category.categoryname)',
		'isActive' => 'IF(Category.categoryisactive = 0, "<span class=\"button btn btn-sm btn-warning\"><span class=\"glyphicon glyphicon-remove-circle\" title=\"Inactive\"></span></span>", IF(Category.categoryisactive = 1, "<span class=\"button btn btn-sm btn-success\"><span class=\"glyphicon glyphicon-ok-circle\" title=\"Active\"></span></span>", "<span class=\"button btn btn-sm btn-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span></span>"))'
	);
}