<?php
//pr($productbranch);
echo "<h2>".__('Product Branch Assign List'). $Utilitys->addurl($title=__("Assign New Product Branch"), $this->request['controller'], $page="productbranch")."</h2>";
echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Productbranch.company_id",
			array(
				'type'=>'select',
				"options"=>array($company),
				'empty'=>__('Select Company'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
				)
			)
		.$this->Form->input(
			"Productbranch.branch_id",
			array(
				'type'=>'select',
				"options"=>array($branch),
				'class'=> 'form-control',
				'empty'=>__('Select Branch'),
				'div'=>false,
				'tabindex'=>2,
				'label'=>false,
				'style'=>''
				)
			)
		.$this->Form->input(
			"Productbranch.product_id",
			array(
				'type'=>'select',
				"options"=>array($product),
				'class'=> 'form-control',
				'empty'=>__('Select Product'),
				'div'=>false,
				'tabindex'=>3,
				'label'=>false,
				'style'=>''
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"productbranchsarchloading\">
	";
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
		echo $this->Html->tableHeaders(
			array(
				__('SL#'),
				array(
					__('Product Name')  => array('class' => 'highlight sortable')
				),
				array(
					__('Branch Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Company Name')  => array('class' => 'highlight sortable')
				),
				array(
					__('Is Active')  => array('class' => 'highlight sortable')
				),
				__('Action')
			)
		);
		$productbranchrows = array();
		$countProductBranch = 0-1+$this->Paginator->counter('%start%');
		
		foreach($productbranch as $Thisproductbranch):
			$countProductBranch++;
			$productbranchrows[]= array($countProductBranch,$Thisproductbranch["Productbranch"]["product_name"],$Thisproductbranch["Productbranch"]["branch_name"], $Thisproductbranch["Productbranch"]["company_name"], $Utilitys->statusURL($this->request["controller"], 'productbranch', $Thisproductbranch["Productbranch"]["id"], $Thisproductbranch["Productbranch"]["productbranchuuid"], $Thisproductbranch["Productbranch"]["productbranchisactive"]),$Utilitys->cusurl($this->request['controller'], 'productbranch', $Thisproductbranch["Productbranch"]["id"], $Thisproductbranch["Productbranch"]["productbranchuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$productbranchrows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
				</div>
		<script>
			$(document).ready(function() {
				$(\"#ProductbranchCompanyId\").change(function(){
					var ProductbranchCompanyId = $(\"#ProductbranchCompanyId\").val();
					var ProductbranchBranchId = $(\"#ProductbranchBranchId\").val();
					var ProductbranchProductId = $(\"#ProductbranchProductId\").val();
					$(\"#productbranchsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."products/productbranchsearchbytext',
						{ProductbranchCompanyId:ProductbranchCompanyId, ProductbranchBranchId:ProductbranchBranchId,ProductbranchProductId:ProductbranchProductId},
						function(result) {
							$(\"#productbranchsarchloading\").html(result);
						}
					);
				});
				$(\"#ProductbranchBranchId\").change(function(){
					var ProductbranchCompanyId = $(\"#ProductbranchCompanyId\").val();
					var ProductbranchBranchId = $(\"#ProductbranchBranchId\").val();
					var ProductbranchProductId = $(\"#ProductbranchProductId\").val();
					$(\"#productbranchsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."products/productbranchsearchbytext',
						{ProductbranchCompanyId:ProductbranchCompanyId,ProductbranchBranchId:ProductbranchBranchId,ProductbranchProductId:ProductbranchProductId},
						function(result) {
							$(\"#productbranchsarchloading\").html(result);
						}
					);
				});
				$(\"#ProductbranchProductId\").change(function(){
					var ProductbranchCompanyId = $(\"#ProductbranchCompanyId\").val();
					var ProductbranchBranchId = $(\"#ProductbranchBranchId\").val();
					var ProductbranchProductId = $(\"#ProductbranchProductId\").val();
					$(\"#productbranchsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."products/productbranchsearchbytext',
						{ProductbranchCompanyId:ProductbranchCompanyId,ProductbranchBranchId:ProductbranchBranchId,ProductbranchProductId:ProductbranchProductId},
						function(result) {
							$(\"#productbranchsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>