<?php
echo $this->Form->create('Locations', array('action' => 'locationinsertupdateaction', 'type' => 'file'));
	echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$location[0]['Location']['id']));
	echo $this->Form->input('locationuuid', array('type'=>'hidden', 'value'=>@$location[0]['Location']['locationuuid']));
	$options = array(1 => __("Yes"), 0 =>__("No"));
	echo"<h3>".__("Add New Location")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-12\">";
			echo"<div class=\"row\">";
				echo"<div class=\"col-md-6\">";
					echo $this->Form->input(
						"Location.locationname",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Location'),
							'tabindex'=>1,
							'placeholder' => __("Location"),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style' => '',
							'value' =>@$location[0]['Location']['locationname']
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-6\">";
					echo $this->Form->input(
						"Location.locationnamebn",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Location BN'),
							'tabindex'=>2,
							'placeholder' => __("Location BN"),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style' => '',
							'value' =>@$location[0]['Location']['locationnamebn']
						)
					);
				echo"</div>";
			echo"</div>";
			echo"<div class=\"row\">";
				echo"<div class=\"col-md-2\">";
					echo "<label class=\"control-label\" for=\"Locationlocationisparent\">is Parent</label>";
					echo $this->Form->input(
						"Location.locationisparent",
						array(
							'type' => 'radio',
							'div' => array('class'=> 'form-group'),
							'label' => true,
							'tabindex'=>3,
							'class'=> 'form-inline',
							'options' => $options,
							'legend' => false,
							'value' => (@$location[0]['Location']['locationisparent'] ? $location[0]['Location']['locationisparent'] : 0),

							'style' => ''
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-8\">";
					echo $this->Form->input(
						"Location.locationparentid",
						array(
							'type'=>'select',
							"options"=>array($locationparentlist),
							'empty'=>__('Parent'),
							'div' => array('class'=> 'form-group'),
							'tabindex'=>4,
							'label' => __('Parent'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							'selected'=> (@$location[0]['Location']['locationparentid'] ? $location[0]['Location']['locationparentid'] : 0)
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-2\">";
					echo "<label class=\"control-label\" for=\"Locationlocationisactive\">is Active</label>";
					echo $this->Form->input(
						"Location.locationisactive",
						array(
							'type' => 'radio',
							'div' => array('class'=> 'form-group'),
							'label' => true,
							'tabindex'=>5,
							'class'=> 'form-inline',
							'options' => $options,
							'legend' => false,
							'value' => (@$location[0]['Location']['locationisactive'] ? $location[0]['Location']['locationisactive'] : 0),

							'style' => ''
						)
					);
				echo"</div>";
			echo"</div>";
			echo $Utilitys->allformbutton('',$this->request["controller"],$page='location');
		echo"</div>";
	echo"</div>";
echo $this->Form->end();
echo"
<script>
	$(document).ready(function() {
		$(\"#LocationsLocationinsertupdateactionForm\").validationEngine();
	});	
</script>
";
?>