<?php
$debit = $credit ='';
$sum_of_debit = $sum_of_credit = $total_balance = $opening_balance = 0;
if($coatype[0]['Coa']['coatype_id']==1 || $coatype[0]['Coa']['coatype_id']==2)
    $opening_balance = 0+$total_debit[0][0]['total_debit']+$total_credit[0][0]['total_credit'];

echo "<div class='center-aligned-texts'><h1>".$company_info[0]["Company"]["companyname"]."</h1></div>";
echo "<div class='center-aligned-texts'><b>".$company_info[0]["Company"]["companypresentaddress"]."</b></div>";
echo "<br><br>";
echo "<div class='left-aligned-texts'><b>Id:</b>".$party_ledger_data[0]["User"]["id"]." "." <b>Name</b>:".$party_ledger_data[0]["User"]["userfirstname"].
    $party_ledger_data[0]["User"]["usermiddlename"].$party_ledger_data[0]["User"]["userlastname"]."</div>";
echo "<div  class='left-aligned-texts'><b>Date From:</b>".$form_date."<b> To: </b>".$to_date."</div><br>";

echo $Utilitys->tablestart($class=null, $id=null);
echo $this->Html->tableHeaders(
    array(
        array(
            __('Date')  => array('class' => '')
        ),
        array(
            __('Voucher NO.')  => array('class' => '')
        ),
        array(
            __('Particulars')  => array('class' => '')
        ),
        array(
            __('Debit')  => array('class' => '')
        ),
        array(
            __('Credit')  => array('class' => '')
        ),
        array(
            __('Balance')  => array('class' => '')
        )
    )
);
$coatyperows = array();
$coatyperows[]= array(
    $form_date,
    '',
    'Opening Balance',
    '',
    '',
    $opening_balance
);

$total_balance= $opening_balance;
foreach($party_ledger_data as $party_ledger):
    if($party_ledger["Journaltransaction"]["transactionamount"]>=0){
        $debit = $party_ledger["Journaltransaction"]["transactionamount"];
        $credit = 0;

    }else{
        $credit = str_replace("-", "", $party_ledger["Journaltransaction"]["transactionamount"]);
        $debit = 0;

    }

    $total_balance = $total_balance + $debit - $credit;
    $sum_of_debit = $sum_of_debit + $debit;
    $sum_of_credit = $sum_of_credit + $credit;

    $coatyperows[]= array(
        $party_ledger["Journaltransaction"]["transactiondate"],
        $party_ledger["Journaltransaction"]["voucher_no"],
        $party_ledger["Journaltransaction"]["particulars"],
        $debit,
        $credit,
        $total_balance
    );


endforeach;
$rows[]= array(array("Total","colspan='3'"),
    $sum_of_debit ,
    $sum_of_credit,
    ''
);
echo $this->Html->tableCells(
    $coatyperows
);
echo $this->Html->tableCells(
    $rows
);
echo $Utilitys->tableend();




//$(\"#banksarchloading\").html(result);
?>
