<?php
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"2\">
				<h2>".__('Chart of Account Type')."</h2>
			</td>
		</tr>
		<tr>
			<th>
				".__('CODE')."
			</th>
			<td>
				".$coatype[0]["Coatype"]["coatypecode"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Type')."
			</th>
			<td>
				".$coatype[0]["Coatype"]["coatypename"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Type BN")."
			</th>
			<td>
				".$coatype[0]["Coatype"]["coatypenamebn"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Active")."
			</th>
			<td>
				".$coatype[0]["Coatype"]["isActive"]."
			</td>
		</tr>
	";
	echo $Utilitys->tableend();
?>