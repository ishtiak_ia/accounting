<?php
App::import('Controller', 'Logins');
App::import('Controller', 'Transactions');
App::import('Controller', 'Users');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');
App::uses('File', 'Utility');

class FilesController extends AppController{
	public $name = 'Files';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'JqueryValidation');
	public $components = array('RequestHandler','Session');
	public $uses = array( 'File', 'Employeeinouttime', 'Punchingmachin', 'Institute');

	var $Logins;
	function beforeFilter(){
		$this->Logins =& new LoginsController;
		$this->Logins->constructClasses();
		$this->Logins->__validateLoginStatus();
		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	public  function beforeRender(){
		$this->response->disableCache();//prevent useless warnings for Ajax
		if($this->RequestHandler->isAjax()){
			Configure::write('debug', 0);
		}
	}

	function add(){
		$this->layout='default';
		$this->set('title_for_layout', __('Log File Upload').' | '.__(Configure::read('site_name')));
	}

	function fileinsertupdate(){
		ini_set('max_execution_time', 9999999999);
		set_time_limit(0);
		ini_set('memory_limit', '-1');
		$this->layout='default';
		echo "Please wait!!! The Log File are now processing......";
		if (@$_FILES["data"]["error"]["Files"]["File"] > 0){
			echo "Error: " . $_FILES["data"]["error"]["Files"]["File"]  . "<br />";
		}elseif (@$_FILES["data"]["type"]["Files"]["File"] !== "text/plain"){
			echo "File must be a .txt";
		}else{
			//$this->Punchingmachin->query('CALL dayplans()');
			$file_handle = fopen($_FILES["data"]["tmp_name"]["Files"]["File"], "rb");
			ini_set('auto_detect_line_endings', TRUE);
			$data_array=file($_FILES["data"]["tmp_name"]["Files"]["File"], FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
			foreach($data_array as $data_string){
				$words = preg_split('/[\s]+/', $data_string);
				$employeeID=(string)$words[0];
				$punchingDate=@$words[1]." ".@$words[2];
				if (strlen($employeeID) >1) {
					if (strlen($employeeID) > 9){
						$employeeID = substr($employeeID, 2, 7);
					}
					$punchingmachin=$this->Punchingmachin->find(
						'first',array(
							"fields" => "Punchingmachin.*",
							"conditions"=>array(
								'Punchingmachin.employee_id'=>$employeeID,
								'Punchingmachin.punchingdate'=>$punchingDate
							)
						)
					);
					$this->set('punchingmachin', $punchingmachin);


					if(@$punchingmachin["Punchingmachin"]["employee_id"]>0){
					}else{
						$punch_exists=$this->Punchingmachin->find(
							'count',array(
								"fields" => "Punchingmachin.employee_id",
								"conditions"=>array(
									'Punchingmachin.employee_id'=>$employeeID,
									'Punchingmachin.punchingdate'=>$words[1].' 00:00:00',
									'Punchingmachin.punchingtime'=>$words[1].' 00:00:00'
								)
							)
						);
						$this->set('punch_exists', $punch_exists);
						//echo $employeeID."--".$punch_exists."--".$punchingDate."--";
						if($punch_exists>0){
							//echo"update<br>";
							$this->Punchingmachin->query("Update punchingmachins SET punchingdate='{$punchingDate}', punchingtime='{$punchingDate}', PDATE='{$words[1]}', PTIME='{$words[2]}' WHERE employee_id = '".$employeeID."' AND punchingdate='".$words[1]." 00:00:00' AND punchingtime='".$words[1]." 00:00:00' ");
						}else{
							$punchingmachin=$this->Punchingmachin->find(
								'first',array(
									"fields" => "Punchingmachin.*",
									"conditions"=>array(
										'Punchingmachin.employee_id'=>$employeeID
									)
								)
							);
							$this->set('punchingmachin', $punchingmachin);
							
							if(@$punchingmachin["Punchingmachin"]["employee_id"]>0):
								//echo"insert<br>";
								$this->request->data['Punchingmachin']['employee_id']=$employeeID;
								$this->request->data['Punchingmachin']['punchingdate']=$punchingDate;
								$this->request->data['Punchingmachin']['punchingtime']=$punchingDate;
								$this->request->data['Punchingmachin']['PDATE']=$words[1];
								$this->request->data['Punchingmachin']['PTIME']=$words[2];
								$this->Punchingmachin->create();
								$this->Punchingmachin->save($this->data);
							else:
							endif;

						}
					}
				}
			}
			//$this->Punchingmachin->query('CALL fnpunchingmachinintimes()');
			//$this->Punchingmachin->query('CALL fnpunchingmachinouttimes()');
			//$this->Punchingmachin->query('CALL fnpunchingmachinlunchtimes()');
			//$this->Punchingmachin->query('CALL fnjobcardsummeries()');
			//$this->Punchingmachin->query('CALL fnsalarystatements()');
			$message = __('Log File Upload Successfully.');
			$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
//			$this->redirect("/files/add");


    		/*$this->redirect(array("controller" => "files", 
                          "action" => "add",
                          "param1" => "",
                          "param2" => "",
                          ""));*/


			echo"<script>window.location = '".$this->webroot."files/add';</script>";
		} 
		//exit();
	}
	function addinstitute(){
		$this->layout='default';
		$this->set('title_for_layout', __('Institute File Upload').' | '.__(Configure::read('site_name')));
	}
	function institutefileinsertupdate() {
		$userID = $this->Session->read('User.id');
		ini_set('max_execution_time', 9999999999);
		set_time_limit(0);
		ini_set('memory_limit', '-1');
		$this->layout='default';
		echo "Please wait!!! The Institute File are now processing......";
		/*$csv_mimetypes = array(
		    'text/csv',
		    'application/csv',
		    'text/comma-separated-values',
		    'application/vnd.ms-excel',
		    'application/vnd.msexcel',
		    'application/octet-stream',
		);*/
		if(@$_FILES["data"]["error"]["Files"]["File"] > 0) {
			echo "Error: " . $_FILES["data"]["error"]["Files"]["File"]  . "<br />";
		}elseif(@$_FILES["data"]["type"]["Files"]["File"] !== "text/csv") {
			echo "File must be a .csv";
		}else{
			$file_handle = fopen($_FILES["data"]["tmp_name"]["Files"]["File"], "rb");
			ini_set('auto_detect_line_endings', TRUE);
			$data_array=file($_FILES["data"]["tmp_name"]["Files"]["File"], FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
			foreach ($data_array as $data_string) {
				$words = preg_split('/[,]+/', $data_string);
				$this->request->data['Institute']['instituteuuid'] = String::uuid();
				$this->request->data['Institute']['divisionid'] = $words[0];
				$this->request->data['Institute']['districtid'] = $words[1];
				$this->request->data['Institute']['upazilaid'] = $words[2];
				$this->request->data['Institute']['institutetype_id'] = $words[3];
				$this->request->data['Institute']['instituteeiin'] = $words[4];
				$this->request->data['Institute']['institutename'] = $words[5];
				$this->request->data['Institute']['institutenamebn'] = $words[6];
				$this->request->data['Institute']['institutemobile'] = $words[7];
				$this->request->data['Institute']['instituteagentid'] = $words[8];
				$this->request->data['Institute']['instituteisactive'] = 1;
				$this->request->data['Institute']['instituteinsertid'] = $userID;
				$this->request->data['Institute']['instituteinsertdate'] = date('Y-m-d H:i:s');
				$this->Institute->create();
				$this->Institute->save($this->data);
			}
			echo "<script>window.location = '".$this->webroot."files/addinstitute';</script>";
			echo "<script>alert('Institute File Uploaded Successfully.');</script>";
			//$message = __('Institute File Upload Successfully.');
			//$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
		}
	}
}
