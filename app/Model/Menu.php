<?php

App::uses('AppModel', 'Model');

class Menu extends AppModel {

    public $name = 'Menu';
    public $usetables = 'menus';

    var $virtualFields = array(
    	'isParent' => 'IF(Menu.menuisparent = 1, "<span class=\"green\">Yes</span>", "<span class=\"red\">No</span>")',
    	'isPermanent' => 'IF(Menu.menuispermanent = 1, "<span class=\"green\">Yes</span>", "<span class=\"red\">No</span>")',
		'isActive' => 'IF(Menu.menuisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Menu.menuisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
    
}

?>