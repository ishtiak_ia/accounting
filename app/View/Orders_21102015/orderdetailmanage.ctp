<?php
	//pr($orderdetail);
	echo "<h2>".__('Orderdetail')."</h2>";
		echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id=null);
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('Order Date')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Product')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Particulars')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Quantity')  => array('class' => 'highlight sortable')
						), 
						array(
							__('MON')  => array('class' => 'highlight sortable')
						), 
						array(
							__('KG')  => array('class' => 'highlight sortable')
						), 
						array(
							__('GRAM')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Discount')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Total Amount')  => array('class' => 'highlight sortable')
						), 
						__('Action')
					)
				);
				$orderdetailrows = array();
				$countorderdetail = 0-1+$this->Paginator->counter('%start%');
				foreach($orderdetail as $Thisorderdetail):
					$countorderdetail++;
					$orderdetailrows[]= array($countorderdetail,$Thisorderdetail["Orderdetail"]["orderdetail_date"],$Thisorderdetail["Orderdetail"]["orderdetail_productname"],$Thisorderdetail["Orderdetail"]["orderdetail_particulars"],array($Thisorderdetail["Orderdetail"]["orderdetail_productquantity"],'colspan="1" align="right"'),array($Thisorderdetail["Orderdetail"]["orderdetail_productmon"],'colspan="1" align="right"'),array($Thisorderdetail["Orderdetail"]["orderdetail_productkg"],'colspan="1" align="right"'),array($Thisorderdetail["Orderdetail"]["orderdetail_productgram"],'colspan="1" align="right"'),array($Thisorderdetail["Orderdetail"]["order_discountamount"],'colspan="1" align="right"'),array($Thisorderdetail["Orderdetail"]["order_amount"],'colspan="1" align="right"'),$Utilitys->cusurl($this->request["controller"], 'orderdetail', $Thisorderdetail["Orderdetail"]["id"], $Thisorderdetail["Orderdetail"]["orderdetailuuid"]));
				endforeach;
				echo $this->Html->tableCells(
					$orderdetailrows
				);
			echo $Utilitys->tableend();
		echo $Utilitys->paginationcommon();
?>