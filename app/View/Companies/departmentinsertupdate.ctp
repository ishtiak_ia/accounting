<?php 
    echo"
        <table>
            <tr>
                <td>
                    <h2>".__('Add Department Information')."</h2>
                </td>
            </tr>
            <tr>
                <td>
    ";
    echo $this->Form->create('Companies', array('action' => 'departmentinsertupdateaction', 'type' => 'file'));
    echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$departments[0]['Department']['id']));
    echo $this->Form->input('departmentuuid', array('type'=>'hidden', 'value'=>@$departments[0]['Department']['departmentuuid']));
    $options = array(1 => __("Yes"), 0 =>__("No"));
            $attributes = array(
                'legend' => false, 
                'label' => true, 
                'value' => (@$departments[0]['Department']['departmentisactive'] ? $departments[0]['Department']['departmentisactive'] : 0), 
                'class' => 'form-inline'
            );    
echo $Utilitys->tablestart();
echo"
        <tr>
            <td>".__("Company")."</td>
            <td>
                ".$this->Form->input(
                                    "Department.company_id",
                                    array(
                                        'type' => 'select',
                                        "options" => $company, 
                                        'empty' => 'Select Company',
                                        'div' => false, 
                                        'label' => false,
                                        "optgroup label" => false,
                                        'data-validation-engine'=>'validate[required]',
                                        'selected'=> (@$departments[0]['Department']['company_id'] ? $departments[0]['Department']['company_id'] : 0)
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("Group")."</td>
            <td>
                ".$this->Form->input(
                                    "Department.group_id",
                                    array(
                                        'type' => 'select',
                                        "options" => $group, 
                                        'empty' => 'Select Group',
                                        'div' => false, 
                                        'label' => false,
                                        "optgroup label" => false,
                                        'data-validation-engine'=>'validate[required]',
                                        'selected'=> (@$departments[0]['Department']['group_id'] ? $departments[0]['Department']['group_id'] : 0)
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("Department Name")."</td>
            <td>
                ".$this->Form->input(
                                    "Department.departmentname",
                                    array(
                                        'type' => 'text',
                                        'div' => false, 
                                        'label' => false,
                                        'data-validation-engine'=>'validate[required]',
                                        'value'=> @$departments[0]['Department']['departmentname']
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("Department Name BN")."</td>
            <td>
                ".$this->Form->input(
                                    "Department.departmentnamebn",
                                    array(
                                        'type' => 'text',
                                        'div' => false, 
                                        'label' => false,
                                        'value'=> @$departments[0]['Department']['departmentnamebn']
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("isActive")."</td>
            <td>
                ".$this->Form->radio(
                'Department.departmentisactive',
                $options, 
                $attributes
                )."
            </td>
        </tr>
        <tr>
            <td colspan=\"2\">
                ".$Utilitys->allformbutton('',$this->request["controller"],$page='department')."
            </td>
        </tr>
    </table>
    ";
    echo $this->Form->end();
    echo"
                </td>
            </tr>
        </table>
    ";
?>   

<?php echo $this->Form->end(); ?> 
<script type="text/javascript">
 jQuery(document).ready(function(){
    jQuery("#CompaniesDepartmentinsertupdateactionForm").validationEngine();
 });
</script>