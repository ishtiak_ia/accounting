<?php
App::import('Controller', 'Logins');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');

class MenusController extends AppController {
	public $name = 'Menus';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'Form' => array('className' => 'BootstrapForm'));
	public $components = array('RequestHandler','Session');
	public $uses = array('User', 'Usernarration', 'Group', 'Groupnarration', 'Menu', 'Menunarration', 'Menuprivilege', 'Company', 'Branch');

	var $Logins;
	function beforeFilter(){
		$this->Logins =& new LoginsController;
		$this->Logins->constructClasses();
		$this->Logins->__validateLoginStatus();
		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	public  function beforeRender(){
		$this->response->disableCache();//prevent useless warnings for Ajax
		if($this->RequestHandler->isAjax()){
			Configure::write('debug', 0);
		}
	}

	public function menuinsertupdate($id = null){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Insert/Edit Menu').' | '.__(Configure::read('site_name')));

		$parentmenu = $this->Menu->find(
			'list', array(
				"fields" => array('id', 'menuname'),
				"conditions" => array('menuisparent' => 1),
			)
		);
		$this->set('parentmenu',$parentmenu);

		if(!empty($id) && $id!=0) {
			$menu = $this->Menu->find('all', array("fields" => "Menu.*", "conditions" => array('Menu.id' => $id)));
			$this->set('menu', $menu);
		}
	}

	public function menuaddeditaction($id=null){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Insert/Edit Menu').' | '.__(Configure::read('site_name')));
		$userID = $this->Session->read('User.id');
		$this->request->data['Menu']['parentmenu']=($this->request->data['Menu']['parentmenu'] ? $this->request->data['Menu']['parentmenu'] : 0);
		if ($this->request->is('post')) {
			//print_r($_POST); exit();
			if(!empty($id) && $id!=0):
				$this->request->data['Menu']['menuupdateid']=$userID;
				$this->request->data['Menu']['menuupdatedate']=date('Y-m-d H:i:s');
				$message = __('Update Successfully.');
				$this->Menu->id = $id;
				// $this->Session->setFlash('Update Successfully.', 'default', array('class' => 'alert alert-success'));
			else:
				$now = date('Y-m-d H:i:s');
				$this->request->data['Menu']['menuinsertid']=$userID;
				$this->request->data['Menu']['menuinsertdate'] = $now;
				$this->request->data['Menu']['menuuuid']=String::uuid();
				$message = __('Save Successfully.');
			endif;
			if ($this->Menu->save($this->data)) {
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
			}
		}
		$this->redirect("/menus/menumanage");
	}

	public function menumanage() {
		$this->layout = 'default';
		$this->set('title_for_layout', __('Menu Manage').' | '.__(Configure::read('site_name')));

		$parentmenu = $this->Menu->find(
			'list', array(
				"fields" => array('id', 'menuname'),
				"conditions" => array('menuisparent' => 1),
			)
		);
		$this->set('parentmenu',$parentmenu);

		$this->paginate = array(
			'fields' => array('Menu.*'),
			'order'  =>'Menu.id ASC',
			//'condition' =
			'limit' => 20
		);
		$menus = $this->paginate('Menu');
		$this->set('menus',$menus);
	}

	public function menudetailsbyid($id=null, $uuid=null){
		$this->layout='ajax';
		$menu=$this->Menu->find('all',array("fields" => "Menu.*","conditions"=>array('Menu.id'=>$id,'Menu.menuuuid'=>$uuid)));
		$this->set('menu',$menu);
	}

	public function menusearchbytext($Searchtext=null){
		$this->layout='ajax';
		$Searchtext = @$this->request->data['Searchtext'];
		$MenuParentmenu = @$this->request->data['MenuParentmenu'];
		$menu_id = array();
		if(!empty($MenuParentmenu) && $MenuParentmenu!=''):
			$menu_id = array('Menu.parentmenu'=>$MenuParentmenu);
		endif;
		if(!empty($Searchtext) && $Searchtext!=''):
			$menu_id[] = array(
				'OR' => array(
					'Menu.menuname LIKE ' => '%'.$Searchtext.'%'
				)
			);
		endif;

		$this->paginate = array(
			'fields' => 'Menu.*',
			'order'  =>'Menu.menuname ASC',
			"conditions"=>$menu_id,
			'limit' => 20
		);
		$menus = $this->paginate('Menu');
		$this->set('menus',$menus);
	}

	public function menudelete($id = null) {
		$this->layout='ajax';
		$userID = $this->Session->read('User.id');
		$this->request->data['Menu']['menudeleteid']=$userID;
		$this->request->data['Menu']['menudeletedate'] = date('Y-m-d H:i:s');
		$this->request->data['Menu']['menuisactive']=2;
		$this->Menu->id = $id;

		if ($this->Menu->save($this->data)) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/menus/menumanage");
	}

	public function menu($group_id = null) {
		$menulist = $this->Menuprivilege->find(
			'all', array(
				"conditions" => array('Menuprivilege.group_id' => $group_id),
			)
		);
		if($menulist != null):
			foreach($menulist as $menulist_val):
				$menuitems[] = $this->Menu->find(
					'all', array(
						"conditions" => array('Menu.id' => $menulist_val['Menuprivilege']['menu_id']),
					)
				);
			endforeach;
			return $menuitems;
		endif;
		return $menuitems;
	}

	public function submenu($group_id = null, $parentMenu = 0) {
		App::import('Helper', 'Html');
		$htmlobj = new HtmlHelper(new View(null));
		if($group_id != null):
			$submenu = $this->Menu->query("
				SELECT 
					Menu.*, 
					(
						SELECT 
							count(*)
						FROM
							menus
						WHERE
							Menu.id=menus.parentmenu
						GROUP BY
							menus.parentmenu
					) as menuCount
				FROM
					menus AS Menu 
				RIGHT JOIN
					menuprivileges AS Menuprivilege ON Menu.id = Menuprivilege.menu_id
				WHERE 
					`Menu`.`parentmenu` = {$parentMenu} AND
					`Menuprivilege`.`group_id` = {$group_id} AND 
					Menu.menuishidden=0
			");
			foreach ($submenu as $submenu_val):
				$url = explode('/', $submenu_val['Menu']["menuurl"]);
				$controller = $url[0];
				if (isset($url[1])):
					$action = str_replace($url[0],'', $submenu_val['Menu']["menuurl"]);
				endif;
				if ($submenu_val[0]["menuCount"] > 0):
					if($submenu_val['Menu']["menuurl"]=='#'):
					echo "<li class=\"parent\">";
						echo "<a class=\"dropdown-toggle\" data-toggle=\"collapse\" href=\"#sub-item{$submenu_val['Menu']["id"]}\">".$submenu_val['Menu']["menuname"];
						$parentMenu = $submenu_val['Menu']["id"];
						echo "<span class=\"icon pull-right\"><em class=\"glyphicon glyphicon-chevron-down\"></em></span>";
						echo "</a>";
						echo "<ul class=\"children collapse\" role=\"menu\" id=\"sub-item{$submenu_val['Menu']["id"]}\">";
							$this->submenu($group_id, $parentMenu);
						echo "</ul>";
					echo "</li>";
					else:
						echo "<li>";
							echo $htmlobj->link($submenu_val['Menu']["menuname"], array('controller' => $controller, 'action' => $action));
						echo "</li>";
					endif;
				else:
					echo "<li>";
						echo $htmlobj->link($submenu_val['Menu']["menuname"], array('controller' => $controller, 'action' => $action));
					echo "</li>";
				endif;
			endforeach;
		endif;
	}

	public function menuprivilege() {
		$this->set('title_for_layout', "Menu privilege ");
		$grouptypelist = $this->Group->find(
			'list', array(
				"fields" => array('id', 'groupname')
			)
		);
		$menulist = $this->Menu->find(
			'list', array(
				"fields" => array('id', 'menuname')
			)
		);
		$allow_menu_id = $this->Menuprivilege->find(
			'all', array(
				"order"=> array('Menuprivilege.group_id'),
			)
		);
		$this->set('allow_menu_id', $allow_menu_id);
		$this->set('grouptypelist', $grouptypelist);
		$this->set('menulist', $menulist);
		if (isset($_POST['menuprivilegesubmit'])):
			if (!empty($this->data)):
				$this->Menuprivilege->create($this->data);
				if ($this->Menuprivilege->save($this->data)):
					$saved = true;
				else:
					$this->Session->setFlash('Menu privilege Not saved.');
					$this->redirect("menuprivilege");
				endif;
			endif;
		endif;
	}

	public function assignmenuprivilege() {
		if (isset($_POST['owemenuprivilegesubmit'])) {
			if (isset($this->data['Menuprivilege']['menu'])) {
				$now = date('Y-m-d H:i:s');
				$group_id = $this->data['Menuprivilege']['group_id'];
				$userID = $this->Session->read('User.id');
				$companyID = $this->Session->read('User.company_id');
				$branchID = $this->Session->read('User.branch_id');
				$menuprivilege_data = $this->Menuprivilege->find(
					'all', array(
						'conditions' => array('Menuprivilege.group_id' => $group_id),
					)
				);
				if(!empty($menuprivilege_data)){
					$this->Menuprivilege->deleteAll(array('Menuprivilege.group_id' => $group_id));
				}
				if (!empty($this->data)) {
					foreach ($this->data['Menuprivilege']['menu']['menu_id'] as $menuprivilege_key => $menuprivilege_value) {
						if(!empty($menuprivilege_value) && $menuprivilege_value!=0):
						$data['Menuprivilege']['privilegeuuid'] = String::uuid();
						$data['Menuprivilege']['privilegeinsertid'] = $userID;
						$data['Menuprivilege']['priviligeinsertdate'] = date('Y-m-d H:i:s');
						$data['Menuprivilege']['menu_id'] = $menuprivilege_value;
						$data['Menuprivilege']['group_id'] = $this->data['Menuprivilege']['group_id'];
						$data['Menuprivilege']['user_id'] = $userID;
						$data['Menuprivilege']['company_id'] = $companyID;
						$data['Menuprivilege']['branch_id'] = $branchID;
						$this->Menuprivilege->create($data);
						$this->Menuprivilege->save($data);
						$save = true;
						endif;
					}
					if ($save) {
						$this->Session->setFlash("Assign menuprivilage Successfully", 'default', array('class' => 'alert alert-success'));
						$this->redirect("menuprivilege");
					} else {
						$this->Session->setFlash('menuprivilage Not Assigned.', 'default', array('class' => 'alert alert-danger'));
						$this->redirect("menuprivilege");
					}
				}
			} else {
				$this->Session->setFlash("Assign menuprivilage Successfully", 'default', array('class' => 'alert alert-success'));
				$this->redirect("menuprivilege");
			}
		}
	}

	public function searchmenuprivilege() {
		ini_set('max_execution_time', 9999999999);
		set_time_limit(0);
		ini_set('memory_limit', '-1');
		$this->layout = 'ajax';
		$menuprivilegeGroupId = $this->request->data['menuprivilegeGroupId'];
		$allow_menu_id = $this->Menuprivilege->find(
			'list', array(
				"fields" => array("id", "menu_id"),
				"conditions" => array('Menuprivilege.group_id' => $menuprivilegeGroupId),
			)
		);
		$this->set('allow_menu_id', $allow_menu_id);
		$menucount = $this->Menu->find(
			'count', array(
				"fields" => array('id', 'menuname', 'parentmenu'),
				"order" => array('Menu.parentmenu ASC, Menu.id ASC')
			)
		);
		$this->set('menucount', $menucount);
		$menulist = $this->Menu->query("
			SELECT
				Parent.id AS ParentID,
				Parent.menuname AS ParentName,
				GROUP_CONCAT(
					CONCAT(
						Child.id,
						'-',
						Child.menuname
					)
					ORDER BY
						Child.id ASC
				) AS ChildName
			FROM
				menus AS Parent
			INNER JOIN menus AS Child ON Parent.id = Child.parentmenu
			GROUP BY
				ParentName		
		");
		$this->set('menulist', $menulist);
		$this->set('menuprivilegeGroupId', $menuprivilegeGroupId);
	}
}

