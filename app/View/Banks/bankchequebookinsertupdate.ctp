<?php 
	echo"
		<table>
			<tr>
				<td>
					<h2>".__('Add Bank Cheque Book information')."</h2>
				</td>
			</tr>
			<tr>
				<td>
	";
	echo $this->Form->create('Banks', array('action' => 'bankchequebookinsertupdateaction'));
		echo $this->Form->inpute('id', array('type'=>'hidden', 'value'=>@$bankchequebook[0]['Bankchequebook']['id']));
		echo $this->Form->inpute('bankchequebookuuid', array('type'=>'hidden', 'value'=>@$bankchequebook[0]['Bankchequebook']['bankchequebookuuid']));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$bankchequebook[0]['Bankchequebook']['bankchequebookisactive'] ? $bankchequebook[0]['Bankchequebook']['bankchequebookisactive'] : 0), 
			'class' => 'form-inline radio inline'
		);
	echo"
					<table class=\"table table-bordered table-hover table-striped\">
						<tr>
							<td>".__("Bank")."</td>
							<td>
								".$this->Form->input(
									"Bankchequebook.bank_id",array(
										'type'=>'select',
										"options"=>array($bank),
										'empty'=>__('Select Bank'),
										'div'=>false,
										'tabindex'=>1,
										'label'=>false,
										'style'=>'',
										'class'=> 'form-control',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$bankchequebook[0]['Bankchequebook']['bank_id'] ? $bankchequebook[0]['Bankchequebook']['bank_id'] : 0)
										)
									)."
							</td>
						</tr>
						<tr>
							<td>".__("Branch")."</td>
							<td>
								".$this->Form->input(
									"Bankchequebook.bankbranch_id",array(
										'type'=>'select',
										"options"=>array($bankbranch),
										'empty'=>__('Select Branch'),
										'div'=>false,
										'tabindex'=>2,
										'label'=>false,
										'style'=>'',
										'class'=> 'form-control',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$bankchequebook[0]['Bankchequebook']['bankbranch_id'] ? $bankchequebook[0]['Bankchequebook']['bankbranch_id'] : 0)
										)
									)."
								<span id=\"loadbranch\"></span>
							</td>
						</tr>
						<tr>
							<td>".__("Account Type")."</td>
							<td>
								".$this->Form->input(
									"Bankchequebook.bankaccounttype_id",array(
										'type'=>'select',
										"options"=>array($bankaccounttype),
										'empty'=>__('Select Account Type'),
										'div'=>false,
										'tabindex'=>3,
										'label'=>false,
										'style'=>'',
										'class'=> 'form-control',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$bankchequebook[0]['Bankchequebook']['bankaccounttype_id'] ? $bankchequebook[0]['Bankchequebook']['bankaccounttype_id'] : 0)
										)
									)."
								<span id=\"loadaccounttype\"></span>
							</td>
						</tr>
						<tr>
							<td>
								".__("Account")."
							</td>
							<td>
								".$this->Form->input(
									"Bankchequebook.bankaccount_id",array(
										'type'=>'select',
										"options"=>array($bankaccount),
										'empty'=>__('Select Account'),
										'div'=>false,
										'tabindex'=>4,
										'label'=>false,
										'style'=>'',
										'class'=> 'form-control',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$bankchequebook[0]['Bankchequebook']['bankaccount_id'] ? $bankchequebook[0]['Bankchequebook']['bankaccount_id'] : 0)
										)
									)."
								<span id=\"loadaccount\"></span>
							</td>
						</tr>
						<tr>
							<td>".__("Series")."</td>
							<td>
								".$this->Form->input(
									"Bankchequebook.bankchequebookseries",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'class'=> 'form-control',
										'data-validation-engine'=>'validate[required]',
										'value' =>@$bankchequebook[0]['Bankchequebook']['bankchequebookseries']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("Sequency Form")."</td>
							<td>
								".$this->Form->input(
									"Bankchequebook.bankchequebooksequencyfrom",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'class'=> 'form-control',
										'data-validation-engine'=>'validate[required]',
										'value' => @$bankchequebook[0]['Bankchequebook']['bankchequebooksequencyfrom']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("Sequency To")."</td>
							<td>
								".$this->Form->input(
									"Bankchequebook.bankchequebooksequencyto",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'class'=> 'form-control',
										'data-validation-engine'=>'validate[required]',
										'value' => @$bankchequebook[0]['Bankchequebook']['bankchequebooksequencyto']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("isActive")."</td>
							<td>
								".$this->Form->radio(
									'Bankchequebook.bankchequebookisactive', 
									$options, 
									$attributes
								)."
							</td>
						</tr>
						<tr>
							<td colspan=\"2\">
								".$Utilitys->allformbutton('',$this->request["controller"],$page='bankchequebook')."
							</td>
						</tr>
					</table>
	";
	echo $this->Form->end();
	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				$(\"#BanksBankchequebookinsertupdateactionForm\").validationEngine();
				$(\"#BankchequebookBankId\").change(function(){
					var BankchequebookBankId = $(\"#BankchequebookBankId\").val();
					$(\"#loadbranch\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/listbankbranchbybankid',
						{BankchequebookBankId:BankchequebookBankId},
						function(result) {
							$(\"#BankchequebookBankbranchId\").html(result);
							$(\"#loadbranch\").html('');
						}
					);
				});
				$(\"#BankchequebookBankbranchId\").change(function(){
					var BankchequebookBankId = $(\"#BankchequebookBankId\").val();
					var BankchequebookBankbranchId = $(\"#BankchequebookBankbranchId\").val();
					$(\"#loadaccounttype\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/listbankaccounttypebybankbranchid',
						{BankchequebookBankId:BankchequebookBankId,BankchequebookBankbranchId:BankchequebookBankbranchId},
						function(result) {
							$(\"#BankchequebookBankaccounttypeId\").html(result);
							$(\"#loadaccounttype\").html('');
						}
					);
				});
				$(\"#BankchequebookBankaccounttypeId\").change(function(){
					var BankchequebookBankId = $(\"#BankchequebookBankId\").val();
					var BankchequebookBankbranchId = $(\"#BankchequebookBankbranchId\").val();
					var BankchequebookBankaccounttypeId = $(\"#BankchequebookBankaccounttypeId\").val();
					$(\"#loadaccount\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/listbankaccountbybankaccounttypeid',
						{BankchequebookBankId:BankchequebookBankId,BankchequebookBankbranchId:BankchequebookBankbranchId,BankchequebookBankaccounttypeId:BankchequebookBankaccounttypeId},
						function(result) {
							$(\"#BankchequebookBankaccountId\").html(result);
							$(\"#loadaccount\").html('');
						}
					);
				});
			});
		</script>
	";
?>