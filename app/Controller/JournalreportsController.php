<?php
App::import('Controller', 'Logins');
App::import('Controller', 'Users');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');

class JournalreportsController extends AppController {
	public $name = 'Journalreport';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'JqueryValidation');
	public $components = array('RequestHandler','Session');
	public $uses = array('Banktype', 'Bank', 'Bankbranch', 'Bankaccounttype', 'Bankaccount', 'Bankchequebook', 'Banktransaction', 'Company', 'Group', 'User', 'Usergroup', 'Usertransaction', 'Cashtransaction', 'Gltransaction', 'Journaltransaction', 'Coa',
					 'Coatype', 'Coagroup', 'Coaclass', 'Coaclasssubclass', 'Coas');

	var $Transactions;
	var $Users;

	var $Logins;

	function beforeFilter(){
		$this->Logins =& new LoginsController;
		$this->Logins->constructClasses();
		$this->Logins->__validateLoginStatus();
		//$Logins = new LoginsController;
		//$this->set('Logins',$Logins);
		$this->Users =& new UsersController;
		$this->Users->constructClasses();


		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	public  function beforeRender(){
		$this->response->disableCache();//prevent useless warnings for Ajax
		if($this->RequestHandler->isAjax()){
			Configure::write('debug', 0);
		}
	}

	public function voucher(){
		$voucher_no=$this->Journaltransaction->find(
			'list',
			array(
				"fields" => array("journaltransaction_id","voucher_no")
			)
		);
		$this->set('voucher_no',$voucher_no);
	}

	public function vouchergenerate(){
		$this->layout = 'ajax';
		$voucher_data = $this->Journaltransaction->find('all',array("fields" => "Journaltransaction.transactionamount,
								 Journaltransaction.voucher_no,Journaltransaction.transactiondate,Journaltransaction.description,Coa.coaname,Coa.coacode","conditions"=>array('Journaltransaction.journaltransaction_id'=>$this->request->data['Searchtext'],
								 'Journaltransaction.coa_id=Coa.id')));
		$this->set('voucher_data',$voucher_data);

		$company_info = $this->Company->find('all',array("fields" =>"Company.companyname,Company.companypresentaddress",
						"conditions"=>array('Company.id'=>$_SESSION['User']['company_id'])));
		$this->set('company_info',$company_info);
	}

	public function generalledger(){
		$coa_code=$this->Coa->find(
			'list',
			array(
				"fields" => array("id","coacode")
			)
		);
		$this->set('coa_code',$coa_code);

		$coa_name=$this->Coa->find(
			'list',
			array(
				"fields" => array("id","coa_name")
			)
		);
		$this->set('coa_name',$coa_name);


   }

   function generalledgergenerate(){
   		$this->layout = 'ajax';
   		$general_ledger_data = $this->Journaltransaction->find('all',array("fields" => "Journaltransaction.transactionamount,
								 Journaltransaction.voucher_no,Journaltransaction.transactiondate,Journaltransaction.particulars,Coa.coaname,Coa.coacode",
								 "conditions"=>array('Journaltransaction.coa_id'=>$this->request->data['coa_id'],
								 'Journaltransaction.coa_id=Coa.id','Journaltransaction.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']))));
		$this->set('general_ledger_data',$general_ledger_data);

		 $coatype=$this->Coa->find(
		   'all',array(
			   "fields" => array(
				   'Coa.coatype_id'
			   ),
			   "conditions"=>array('Coa.id'=>$this->request->data['coa_id']),

		   )
	   );
	   $this->set('coatype',$coatype);

	   $company_info = $this->Company->find('all',array("fields" =>"Company.companyname,Company.companypresentaddress",
						"conditions"=>array('Company.id'=>$_SESSION['User']['company_id'])));
		$this->set('company_info',$company_info);

		$total_debit = $this->Journaltransaction->find('all',array("fields" =>"sum(Journaltransaction.transactionamount) as total_debit",
						"conditions"=>array('Journaltransaction.coa_id'=>$this->request->data['coa_id'],'Journaltransaction.transactionamount >'=>0,
						'Journaltransaction.transactiondate <'=>$this->request->data['from_date'])));
		$this->set('total_debit',$total_debit);
		
		$total_credit = $this->Journaltransaction->find('all',array("fields" =>"sum(Journaltransaction.transactionamount) as total_credit",
						"conditions"=>array('Journaltransaction.coa_id'=>$this->request->data['coa_id'],'Journaltransaction.transactionamount <'=>0,
						'Journaltransaction.transactiondate <'=>$this->request->data['from_date'])));
   		$this->set('total_credit',$total_credit);
		$this->set('form_date',$this->request->data['from_date']);
		$this->set('to_date',$this->request->data['to_date']);

   }

   public function trialbalance(){
	$this->layout='default';
	$this->set('title_for_layout', __('Trial Balance').' | '.__(Configure::read('site_name')));

   }

   function trialbalancegenerate(){
	$this->layout = 'ajax';
	$coatype_id = array();
	$Coagroup_id=array('');
	$Coaclass_id=array();
	$Coaclasssubclass_id=array();
	$company_id=$_SESSION["User"]["company_id"];
	$branch_id=$_SESSION["User"]["branch_id"];
	$group_id=$_SESSION["User"]["group_id"];
	$user_id=$_SESSION["User"]["id"];
	$total_income_opening_balance_debit = 0;
    $total_income_net_change_debit = 0;
    $total_income_net_change_credit = 0;
    $total_direct_expense_net_change_debit = 0;
    $total_direct_expense_net_change_credit = 0;
    $total_expense_net_change_debit = 0;
    $total_expense_net_change_credit = 0;
    $total_other_income_net_change_debit = 0;
    $total_other_income_net_change_credit = 0;
    $total_other_expense_net_change_debit = 0;
    $total_other_expense_net_change_credit = 0;
	$total_income_closing_credit = $total_income_closing_debit = $total_other_income_closing_credit = $total_other_expense_closing_debit = 0;
    $td_debit_id = $td_credit_id =  $retained_closing_td_debit_id = $retained_closing_td_credit_id= $retained_opening_td_credit_id = $retained_opening_td_debit_id = '';
    $gross_profit='';
    $gross_loss='';
    $displaytable='';
	$Coatype=$this->Coatype->find(
		'all',array(
			"fields" => array(
				'Coatype.id',
				'Coatype.coatypename',
				'Coatype.coatypecode'
			),
			"order"=>array('Coatype.coatypecode ASC')
		)
	);
	foreach($Coatype as $ThisCoatype):
		$type_opening_debit_balance =0; 
		$type_opening_credit_balance =0; 
		$type_net_change_debit_balance =0;
		$type_net_change_credit_balance =0;  
		$type_debit_closing =0;
		$type_credit_closing =0;
		$type_retained_earning_opening_debit = $type_retained_earning_opening_credit = 0;
		$count_type= 0;
		if($ThisCoatype["Coatype"]["id"]==1)
			$type_code = '2099-99-999';
		else if($ThisCoatype["Coatype"]["id"]==2)
			$type_code = '2800-99-999';
		else if($ThisCoatype["Coatype"]["id"]==4)
			$type_code = '3099-99-999';
		else if($ThisCoatype["Coatype"]["id"]==6)
			$type_code = '4999-99-999';
		else if($ThisCoatype["Coatype"]["id"]==7)
			$type_code = '7999-99-999';
		else
			$type_code = '8999-99-999';

		$displaytable.= "<tr><td class='bold-text'>".$ThisCoatype["Coatype"]["coatypecode"]."</td><td class='bold-text'>".$ThisCoatype["Coatype"]["coatypename"]."</td>
		                 <td class='bold-text' colspan='6'></td></tr>";
		$Coagroup_id = array('Coagroup.coatype_id'=>$ThisCoatype["Coatype"]["id"]);
		$Coagroup=$this->Coagroup->find(
			'all',array(
				"fields" => array(
					'Coagroup.*'
				),
				"conditions"=>array('Coagroup.coatype_id'=>$ThisCoatype["Coatype"]["id"]),
				"order"=>array('Coagroup.coagroupcode ASC')
			)
		);
		$total_type=$this->Coagroup->find(
			'all',array(
				"fields" => array(
					'count(Coagroup.id) as total_type'
				),
				"conditions"=>array('Coagroup.coatype_id'=>$ThisCoatype["Coatype"]["id"]),
			)
		);
		
		foreach($Coagroup as $ThisCoagroup):
			$group_opening_debit_balance =0; 
			$group_opening_credit_balance =0; 
			$group_net_change_debit_balance =0;
			$group_net_change_credit_balance =0;  
			$group_debit_closing =0;
			$group_credit_closing =0;
			$count_group=0;
			$displaytable.= "<tr><td class='bold-text'>".$ThisCoagroup["Coagroup"]["coagroupcode"]."</td><td class='bold-text'>".$ThisCoagroup["Coagroup"]["coagroupname"]."</td>
			 <td class='bold-text' colspan='6'></td></tr>";
			$Coaclass_id[] = array('Coaclass.coagroup_id'=>$ThisCoagroup["Coagroup"]["id"]);
			$Coaclass=$this->Coaclass->find(
				'all',array(
					"fields" => array(       
						'Coaclass.id',
						'Coaclass.coaclasscode',
						'Coaclass.coaclassname'
					),
					"conditions"=> array('Coaclass.coagroup_id'=>$ThisCoagroup["Coagroup"]["id"]),
					"order"=>array('Coaclass.coaclasscode ASC')
				)
			); 
			$total_group=$this->Coaclass->find(
			'all',array(
				"fields" => array(
					'count(Coaclass.id) as total_group'
				),
				"conditions"=>array('Coaclass.coagroup_id'=>$ThisCoagroup["Coagroup"]["id"]),
			)
			);
			$count_type++;
			foreach($Coaclass as $ThisCoaclass):
				$class_opening_debit_balance =0;
				$class_opening_credit_balance =0;
				$class_net_change_debit_balance =0;
				$class_net_change_credit_balance =0;
				$class_debit_closing =0;
				$class_credit_closing =0;
				$count_sub_class =0;
				$displaytable.= "<tr><td class='bold-text'>".$ThisCoaclass["Coaclass"]["coaclasscode"]."</td><td class='bold-text'>".$ThisCoaclass["Coaclass"]["coaclassname"]."</td>
				                 <td class='bold-text' colspan='6'></td></tr>";
				$Coaclasssubclass_id[] = array('Coaclasssubclass.coaclass_id'=>$ThisCoaclass["Coaclass"]["id"]);
				$Coaclasssubclass=$this->Coaclasssubclass->find(
					'all',array(
						"fields" => array(
							'Coaclasssubclass.*'
						),
						"conditions"=> array('Coaclasssubclass.coaclass_id'=>$ThisCoaclass["Coaclass"]["id"]),
						"order"=>array('Coaclasssubclass.coaclasssubclasscode ASC')
					)
				);
				$total_sub_class = $this->Coaclasssubclass->find(
												'all',array(
													"fields" => array(
														'count(Coaclasssubclass.id) as total_sub_class'
													),
													"conditions"=> array('Coaclasssubclass.coaclass_id'=>$ThisCoaclass["Coaclass"]["id"])
													
												)
											);
				$count_group++;
				foreach($Coaclasssubclass as $ThisCoaclasssubclass):
					$count_coa =0;
					$subtotal_opening_debit_balance =0; 
					$subtotal_opening_credit_balance =0; 
					$subtotal_net_change_debit_balance =0;
					$subtotal_net_change_credit_balance =0;  
					$subtotal_debit_closing =0;
					$subtotal_credit_closing =0;
					$retained_earning_opening_debit_balance = $retained_earning_opening_credit_balance =0;
					$displaytable.= "<tr><td class='bold-text'>".$ThisCoaclasssubclass["Coaclasssubclass"]["coaclasssubclasscode"]."</td><td class='bold-text'>".$ThisCoaclasssubclass["Coaclasssubclass"]["coaclasssubclassname"]."</td>
					                <td class='bold-text' colspan='6'></td></tr>";
					$Coa=$this->Coa->find(
						'all',array(
							"fields" => array(
								'Coa.id',
								'Coa.coacode',
								'Coa.coaname'
							),
							"conditions"=>array('Coa.coaclasssubclass_id'=>$ThisCoaclasssubclass["Coaclasssubclass"]["id"]),
							"order"=>array('Coa.coacode ASC')
						)
					);
					$count_sub_class++;
					foreach($Coa as $ThisCoa):
					    $opening_debit_balance=0;
						$opening_balance_credit =0;
						$net_change_debit_arr[0][0]["net_change_debit"] =0;
						$net_change_credit = 0;
						$closing_balance = 0;
						$debit_closing_balance = 0;
                        $credit_closing_balance = 0;

                        $total_coa = $this->Coa->find(
										'all',array(
											"fields" => array(
												'count(Coa.id) as total_coa'
												
											),
											"conditions"=>array('Coa.coaclasssubclass_id'=>$ThisCoaclasssubclass["Coaclasssubclass"]["id"])
											
										)
									);
                       
                        $net_change_debit_arr = $this->Journaltransaction->find('all',array("fields" => "sum(Journaltransaction.transactionamount) as net_change_debit",
												 "conditions"=>array('Journaltransaction.coa_id'=>$ThisCoa['Coa']['id'],
												 'Journaltransaction.coa_id=Coa.id','Journaltransaction.transactionamount >'=>0,'Journaltransaction.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']))));
						$net_change_credit_arr = $this->Journaltransaction->find('all',array("fields" => "sum(Journaltransaction.transactionamount) as net_change_credit",
												 "conditions"=>array('Journaltransaction.coa_id'=>$ThisCoa['Coa']['id'],
												 'Journaltransaction.coa_id=Coa.id','Journaltransaction.transactionamount <'=>0,'Journaltransaction.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']))));
						$net_change_credit = str_replace("-", "", $net_change_credit_arr[0][0]["net_change_credit"]);

						$opening_debit_balance_arr = $this->Journaltransaction->find('all', array("fields" => "sum(Journaltransaction.transactionamount) as opening_debit_balance",
							"conditions" => array('Journaltransaction.coa_id' => $ThisCoa['Coa']['id'], 'Journaltransaction.transactionamount >' => 0,
								'Journaltransaction.transactiondate <' => $this->request->data['from_date'])));
						$opening_credit_balance_arr = $this->Journaltransaction->find('all', array("fields" => "sum(Journaltransaction.transactionamount) as opening_credit_balance",
							"conditions" => array('Journaltransaction.coa_id' => $ThisCoa['Coa']['id'], 'Journaltransaction.transactionamount <' => 0,
								'Journaltransaction.transactiondate <' => $this->request->data['from_date'])));
						if($ThisCoatype["Coatype"]["id"]==1 || $ThisCoatype["Coatype"]["id"]==2) {
							if(!empty($opening_credit_balance_arr))
								$opening_balance_credit = str_replace("-", "", $opening_credit_balance_arr[0][0]["opening_credit_balance"]);

                            if($ThisCoatype["Coatype"]["id"]==1){
								$opening_debit_balance = $opening_debit_balance_arr[0][0]["opening_debit_balance"]-str_replace("-", "", $opening_credit_balance_arr[0][0]["opening_credit_balance"]);
								$opening_balance_credit = 0;

							}else if($ThisCoatype["Coatype"]["id"]==2){
								$opening_balance_credit = str_replace("-", "", $opening_credit_balance_arr[0][0]["opening_credit_balance"]) - $opening_debit_balance_arr[0][0]["opening_debit_balance"];
								$opening_debit_balance = 0;

							}

						}else{
							$retained_earning_opening_debit_balance = $opening_debit_balance_arr[0][0]["opening_debit_balance"]-str_replace("-", "", $opening_credit_balance_arr[0][0]["opening_credit_balance"]);
							$retained_earning_opening_credit_balance = str_replace("-", "", $opening_credit_balance_arr[0][0]["opening_credit_balance"]) - $opening_debit_balance_arr[0][0]["opening_debit_balance"];

						}
						$closing_balance = ($opening_debit_balance_arr[0][0]['opening_debit_balance'] - str_replace("-", "", $opening_credit_balance_arr[0][0]["opening_credit_balance"]))+($net_change_debit_arr[0][0]["net_change_debit"] - $net_change_credit);
						if($ThisCoatype["Coatype"]["id"]==1 || $ThisCoatype["Coatype"]["id"]==6 || $ThisCoatype["Coatype"]["id"]==8){
							$debit_closing_balance =  $closing_balance;
							$credit_closing_balance=0;

						}else if($ThisCoatype["Coatype"]["id"]==2 || $ThisCoatype["Coatype"]["id"]==4 || $ThisCoatype["Coatype"]["id"]==7){
							$credit_closing_balance =  $closing_balance;
							$debit_closing_balance =0;

						}

                        //group subtotal start
						if($ThisCoatype["Coatype"]["id"]==1 || $ThisCoatype["Coatype"]["id"]==2) {
							if($ThisCoatype["Coatype"]["id"]==1){
								$group_opening_debit_balance = $group_opening_debit_balance + $opening_debit_balance;
								$group_opening_credit_balance =0;

							}
							if($ThisCoatype["Coatype"]["id"]==2) {
								$group_opening_credit_balance = $group_opening_credit_balance + $opening_balance_credit;
								$group_opening_debit_balance=0;

							}

						}
						$group_net_change_debit_balance =$group_net_change_debit_balance+$net_change_debit_arr[0][0]["net_change_debit"];
						$group_net_change_credit_balance =$group_net_change_credit_balance+ $net_change_credit_arr[0][0]["net_change_credit"];  
						$group_debit_closing =$group_debit_closing+$debit_closing_balance;
						$group_credit_closing =$group_credit_closing+$credit_closing_balance;
						//group subtotal end

                        //subclass subtotal start
						if($ThisCoatype["Coatype"]["id"]==1 || $ThisCoatype["Coatype"]["id"]==2) {
							if($ThisCoatype["Coatype"]["id"]==1){
								$class_opening_debit_balance = $class_opening_debit_balance + $opening_debit_balance;
								$class_opening_credit_balance = 0;

							}
							if($ThisCoatype["Coatype"]["id"]==2){
								$class_opening_credit_balance = $class_opening_credit_balance + $opening_balance_credit;
								$class_opening_debit_balance =0;

							}

                        }
						$class_net_change_debit_balance = $class_net_change_debit_balance + $net_change_debit_arr[0][0]["net_change_debit"];
                        $class_net_change_credit_balance = $class_net_change_credit_balance + $net_change_credit_arr[0][0]["net_change_credit"];
                        $class_debit_closing = $class_debit_closing + $debit_closing_balance;
                        $class_credit_closing = $class_credit_closing + $credit_closing_balance;
						//direct expense total calculation start
						if($ThisCoaclass["Coaclass"]["id"]==14){
							$total_direct_expense_net_change_debit = $class_net_change_debit_balance;
							$total_direct_expense_net_change_credit = $class_net_change_credit_balance;

						}
						//direct expense total calculation end
                        //subclass subtotal end

                        //coa subtotal start
						if($ThisCoatype["Coatype"]["id"]==1 || $ThisCoatype["Coatype"]["id"]==2) {
							if($ThisCoatype["Coatype"]["id"]==1){
								$subtotal_opening_debit_balance = $subtotal_opening_debit_balance + $opening_debit_balance;
								$subtotal_opening_credit_balance =0;

							}

							if($ThisCoatype["Coatype"]["id"]==2){
								$subtotal_opening_credit_balance = $subtotal_opening_credit_balance + $opening_balance_credit;
								$subtotal_opening_debit_balance=0;

							}

						}

						$subtotal_net_change_debit_balance = $subtotal_net_change_debit_balance + $net_change_debit_arr[0][0]["net_change_debit"];
                        $subtotal_net_change_credit_balance = $subtotal_net_change_credit_balance + $net_change_credit_arr[0][0]["net_change_credit"];
                        $subtotal_debit_closing = $subtotal_debit_closing + $debit_closing_balance;
                        $subtotal_credit_closing = $subtotal_credit_closing + $credit_closing_balance;
                        //coa subtotal end

                        //coa type subtotal start
						if($ThisCoatype["Coatype"]["id"]==1 || $ThisCoatype["Coatype"]["id"]==2) {
							if($ThisCoatype["Coatype"]["id"]==1){
								$type_opening_debit_balance = $type_opening_debit_balance + $opening_debit_balance;
								$type_opening_credit_balance=0;

							}
							if($ThisCoatype["Coatype"]["id"]==2){
								$type_opening_credit_balance = $type_opening_credit_balance + $opening_balance_credit;
								$type_opening_debit_balance =0;

							}


						}else{
							$type_retained_earning_opening_debit = $type_retained_earning_opening_debit+$opening_debit_balance_arr[0][0]["opening_debit_balance"]-str_replace("-", "", $opening_credit_balance_arr[0][0]["opening_credit_balance"]) ;
							$type_retained_earning_opening_credit = $type_retained_earning_opening_credit+str_replace("-", "", $opening_credit_balance_arr[0][0]["opening_credit_balance"]) - $opening_debit_balance_arr[0][0]["opening_debit_balance"];

						}
						$type_net_change_debit_balance = $type_net_change_debit_balance+$net_change_debit_arr[0][0]["net_change_debit"];
						$type_net_change_credit_balance = $type_net_change_credit_balance+$net_change_credit_arr[0][0]["net_change_credit"];  
						$type_debit_closing =$type_debit_closing+$debit_closing_balance;
						$type_credit_closing =$type_credit_closing+$credit_closing_balance;
						//total income calculation start

						//total income calculation end
						//coa type subtotal end
						if($ThisCoa["Coa"]["id"]==52){
							$td_debit_id = 'retained_earning_debit';
							$td_credit_id = 'retained_earning_credit';
							$retained_closing_td_debit_id = 'retained_earning_closing_debit';
							$retained_closing_td_credit_id = 'retained_earning_closing_credit';
							$retained_opening_td_credit_id = 'retained_earning_opening_credit';
							$retained_opening_td_debit_id = 'retained_earning_opening_debit';

						}

						$displaytable.= "<tr><td>".$ThisCoa["Coa"]["coacode"]."</td><td>".$ThisCoa["Coa"]["coaname"]."</td><td class='right-aligned-texts' id='$retained_opening_td_debit_id'>".number_format($opening_debit_balance,2)."</td>
										<td class='right-aligned-texts' id='$retained_opening_td_credit_id'>".number_format($opening_balance_credit,2)."</td><td class='right-aligned-texts' id=".$td_debit_id.">".number_format($net_change_debit_arr[0][0]["net_change_debit"],2)."</td><td class='right-aligned-texts' id=".$td_credit_id.">".str_replace("-", "",number_format($net_change_credit_arr[0][0]["net_change_credit"],2))."</td>
										<td class='right-aligned-texts' id=".$retained_closing_td_debit_id.">".number_format($debit_closing_balance,2)."</td><td class='right-aligned-texts' id=".$retained_closing_td_credit_id.">".number_format(str_replace("-", "",$credit_closing_balance),2)."</td></tr>";//number_format(str_replace("-", "",$net_change_credit),2)
						//print_r($total_coa);
						$count_coa ++;

						//subclass subtotal display start
						if($count_coa==$total_coa[0][0]["total_coa"]){
							$split_coa_code = explode("-", $ThisCoa["Coa"]["coacode"]);
							$sub_total_coa_code = $split_coa_code[0].'-'.$split_coa_code[1].'-'.'999';
							$displaytable.= "<tr><td class='bold-text'>".$sub_total_coa_code."</td><td class='bold-text'>Total ".$ThisCoaclasssubclass["Coaclasssubclass"]["coaclasssubclassname"]."</td><td class='bold-right-aligned-text'>".number_format($subtotal_opening_debit_balance,2)."</td>
											<td class='bold-right-aligned-text'>".number_format($subtotal_opening_credit_balance,2)."</td><td class='bold-right-aligned-text'>".number_format($subtotal_net_change_debit_balance,2)."</td><td class='bold-right-aligned-text'>".number_format(str_replace("-", "",$subtotal_net_change_credit_balance),2)."</td>
											<td class='bold-right-aligned-text'>".number_format($subtotal_debit_closing,2)."</td><td class='bold-right-aligned-text'>".number_format(str_replace("-", "",$subtotal_credit_closing),2)."</td></tr>";
							
						}
						//subclass subtotal display end

						//class subtotal display start
						if($count_coa==$total_coa[0][0]["total_coa"] && $count_sub_class == $total_sub_class[0][0]["total_sub_class"]){
							$split_coa_code = explode("-", $ThisCoa["Coa"]["coacode"]);
							$sub_total_coa_code = $split_coa_code[0].'-09-'.'999';
							$displaytable.= "<tr><td class='bold-text'>".$sub_total_coa_code."</td><td class='bold-text'>Total ".$ThisCoaclass["Coaclass"]["coaclassname"]."</td><td class='bold-right-aligned-text'>".number_format($class_opening_debit_balance,2)."</td>
										<td class='bold-right-aligned-text'>".number_format($class_opening_credit_balance,2)."</td><td class='bold-right-aligned-text'>".number_format($class_net_change_debit_balance,2)."</td><td class='bold-right-aligned-text'>".str_replace("-", "",number_format($class_net_change_credit_balance,2))."</td>
										<td class='bold-right-aligned-text'>".number_format($class_debit_closing,2)."</td><td class='bold-right-aligned-text'>".str_replace("-", "",number_format($class_credit_closing,2))."</td></tr>";

							if($sub_total_coa_code =='4101-09-999') {
								$gross_profit_or_loass = str_replace("-", "",$total_income_net_change_credit) - $total_direct_expense_net_change_debit;
								if($gross_profit_or_loass>0)
									$gross_profit = $gross_profit_or_loass;
								else
									$gross_loss = str_replace("-", "",number_format($gross_profit_or_loass,2));

								$displaytable .= "<tr><td class='bold-text'>4101-99-999</td><td class='bold-text'>Gross Profit</td><td class='bold-right-aligned-text'></td>
										<td class='bold-right-aligned-text'></td><td class='bold-right-aligned-text'>" . $gross_loss . "</td><td class='bold-right-aligned-text'>".str_replace("-", "",number_format($gross_profit,2))."</td>
										<td class='bold-right-aligned-text'></td><td class='bold-right-aligned-text'></td></tr>";

							}

						}
						//class subtotal display end

						//group subtotal display start
						if($count_coa==$total_coa[0][0]["total_coa"] && $count_sub_class == $total_sub_class[0][0]["total_sub_class"] && $count_group == $total_group[0][0]["total_group"]){
							$split_group_code = substr($ThisCoa["Coa"]["coacode"], 0, 2);;
							$sub_total_group_code = $split_group_code.'99-99-'.'999';
							$displaytable.= "<tr><td class='bold-text'>".$sub_total_group_code."</td><td class='bold-text'>Total ".$ThisCoagroup["Coagroup"]["coagroupname"]."</td><td class='bold-right-aligned-text'>".number_format($group_opening_debit_balance,2)."</td>
										<td class='bold-right-aligned-text'>".number_format($group_opening_credit_balance,2)."</td><td class='bold-right-aligned-text'>".number_format($group_net_change_debit_balance,2)."</td><td class='bold-right-aligned-text'>".str_replace("-", "",number_format($group_net_change_credit_balance,2))."</td>
										<td class='bold-right-aligned-text'>".number_format($group_debit_closing,2)."</td><td class='bold-right-aligned-text'>".str_replace("-", "",number_format($group_credit_closing,2))."</td></tr>";
						
						}
						//group subtotal display end

						//type subtotal display start
						if($count_coa==$total_coa[0][0]["total_coa"] && $count_sub_class == $total_sub_class[0][0]["total_sub_class"] &&
						 	$count_group == $total_group[0][0]["total_group"] && $count_type == $total_type[0][0]["total_type"]){
							//total income calculation start
							if($ThisCoatype["Coatype"]["id"]==4){
								$total_income_net_change_debit = $type_net_change_debit_balance;
								$total_income_net_change_credit = $type_net_change_credit_balance;
								$total_income_closing_credit = $type_credit_closing;
								$retained_total_income_opening_credit = $type_retained_earning_opening_credit;

							}
							//total income calculation end
							//total expense calculation start
							if($ThisCoatype["Coatype"]["id"]==6){
								$total_expense_net_change_debit = $type_net_change_debit_balance;
								$total_expense_net_change_credit = $type_net_change_credit_balance;
								$total_income_closing_debit = $type_debit_closing;
								$retained_total_income_opening_debit = $type_retained_earning_opening_debit;

							}
							//total expense calculation end
							//total other income calculation start
							if($ThisCoatype["Coatype"]["id"]==7){
								$total_other_income_net_change_debit = $type_net_change_debit_balance;
								$total_other_income_net_change_credit = $type_net_change_credit_balance;
								$total_other_income_closing_credit = $type_credit_closing;
								$retained_total_other_income_opening_credit = $type_retained_earning_opening_credit;

							}
							//total other income calculation end
							//total other expense calculation start
							if($ThisCoatype["Coatype"]["id"]==8){
								$total_other_expense_net_change_debit = $type_net_change_debit_balance;
								$total_other_expense_net_change_credit = $type_net_change_credit_balance;
								$total_other_expense_closing_debit = $type_debit_closing;
								$retained_total_other_expense_opening_debit = $type_retained_earning_opening_debit;

							}
							//total other expense calculation end

							$displaytable.= "<tr><td class='bold-text'>".$type_code ."</td><td class='bold-text'>Total ".$ThisCoatype["Coatype"]["coatypename"]."</td><td class='bold-right-aligned-text'>".number_format($type_opening_debit_balance,2)."</td>
										<td class='bold-right-aligned-text'>".number_format($type_opening_credit_balance,2)."</td><td class='bold-right-aligned-text'>".number_format($type_net_change_debit_balance,2)."</td><td class='bold-right-aligned-text'>".str_replace("-", "",number_format($type_net_change_credit_balance,2))."</td>
										<td class='bold-right-aligned-text'>".number_format($type_debit_closing,2)."</td><td class='bold-right-aligned-text'>".str_replace("-", "",number_format($type_credit_closing,2))."</td></tr>";


						}
						//type subtotal display end


					endforeach;
				endforeach;
			endforeach;
		endforeach;
	  endforeach;
	  $net_profit_or_loss = str_replace("-", "",$total_income_net_change_credit)-$total_expense_net_change_debit+$total_other_income_net_change_credit-$total_other_expense_net_change_debit;
	  $retained_earning_closing = str_replace("-", "",$total_income_net_change_credit)-$total_expense_net_change_debit+$total_other_income_net_change_credit-$total_other_expense_net_change_debit;
      $retained_earning_opening= str_replace("-", "",$retained_total_income_opening_credit)-$retained_total_income_opening_debit+$retained_total_other_income_opening_credit-$retained_total_other_expense_opening_debit;
     if($net_profit_or_loss>0)
	  		 $displaytable .= "<tr><td class='bold-text'>8999-99-998</td><td class='bold-text'>Net Profit/Loss</td><td class='bold-right-aligned-text'></td>
										<td class='bold-right-aligned-text'></td><td class='bold-right-aligned-text'></td><td class='bold-right-aligned-text'>$net_profit_or_loss</td>
										<td class='bold-right-aligned-text'></td><td class='bold-right-aligned-text'></td></tr>";
	  else
		  $displaytable .= "<tr><td class='bold-text'>8999-99-998</td><td class='bold-text'>Net Profit/Loss</td><td class='bold-right-aligned-text'></td>
										<td class='bold-right-aligned-text'></td><td class='bold-right-aligned-text'>".str_replace("-", "",$net_profit_or_loss)."</td><td class='bold-right-aligned-text'></td>
										<td class='bold-right-aligned-text'></td><td class='bold-right-aligned-text'></td></tr>";

	  $this->set('displaytable',$displaytable);
      $this->set('profit_or_loss',$net_profit_or_loss);
	  $this->set('retained_earning_closing',$retained_earning_closing);
	  $this->set('retained_earning_opening',$retained_earning_opening);

	   $company_info = $this->Company->find('all',array("fields" =>"Company.companyname,Company.companypresentaddress",
		   "conditions"=>array('Company.id'=>$_SESSION['User']['company_id'])));
	   $this->set('company_info',$company_info);
	   $this->set('form_date',$this->request->data['from_date']);
	   $this->set('to_date',$this->request->data['to_date']);

  }
	
 public function incomestatement(){
  	$this->layout='default';
	$this->set('title_for_layout', __('Income Statement').' | '.__(Configure::read('site_name')));
  }

  function incomestatementgenerate(){
  	$this->layout = 'ajax';
	$coatype_id = array();
	$Coagroup_id=array('');
	$Coaclass_id=array();
	$Coaclasssubclass_id=array();
	$company_id=$_SESSION["User"]["company_id"];
	$branch_id=$_SESSION["User"]["branch_id"];
	$group_id=$_SESSION["User"]["group_id"];
	$user_id=$_SESSION["User"]["id"];
	  $total_income_net_change_debit = 0;
	  $total_income_net_change_credit = 0;
	  $total_direct_expense_net_change_debit = 0;
	  $total_direct_expense_net_change_credit = 0;
	  $total_expense_net_change_debit = 0;
	  $total_expense_net_change_credit = 0;
	  $total_other_income_net_change_debit = 0;
	  $total_other_income_net_change_credit = 0;
	  $total_other_expense_net_change_debit = 0;
	  $total_other_expense_net_change_credit = 0;
	  $td_debit_id='';
	  $td_credit_id='';
	  $gross_profit = '';
	  $gross_loss = '';
	$displaytable='';
	$Coatype=$this->Coatype->find(
		'all',array(
			"fields" => array(
				'Coatype.id',
				'Coatype.coatypename',
				'Coatype.coatypecode'
			),
			"conditions"=>array('Coatype.id' => array(4,6,7,8)),
			"order"=>array('Coatype.coatypecode ASC')
		)
	);
	foreach($Coatype as $ThisCoatype):
		$type_opening_debit_balance =0; 
		$type_opening_credit_balance =0; 
		$type_net_change_debit_balance =0;
		$type_net_change_credit_balance =0;  
		$type_debit_closing =0;
		$type_credit_closing =0;
		$count_type= 0;
		if($ThisCoatype["Coatype"]["id"]==1)
			$type_code = '2099-99-999';
		else if($ThisCoatype["Coatype"]["id"]==2)
			$type_code = '2800-99-999';
		else if($ThisCoatype["Coatype"]["id"]==4)
			$type_code = '3099-99-999';
		else if($ThisCoatype["Coatype"]["id"]==6)
			$type_code = '4999-99-999';
		else if($ThisCoatype["Coatype"]["id"]==7)
			$type_code = '7999-99-999';
		else
			$type_code = '8999-99-999';

		$displaytable.= "<tr><td class='bold-text'>".$ThisCoatype["Coatype"]["coatypecode"]."</td><td class='bold-text'>".$ThisCoatype["Coatype"]["coatypename"]."</td>
		                 <td class='bold-text' colspan='6'></td></tr>";
		$Coagroup_id = array('Coagroup.coatype_id'=>$ThisCoatype["Coatype"]["id"]);
		$Coagroup=$this->Coagroup->find(
			'all',array(
				"fields" => array(
					'Coagroup.*'
				),
				"conditions"=>array('Coagroup.coatype_id'=>$ThisCoatype["Coatype"]["id"]),
				"order"=>array('Coagroup.coagroupcode ASC')
			)
		);
		$total_type=$this->Coagroup->find(
			'all',array(
				"fields" => array(
					'count(Coagroup.id) as total_type'
				),
				"conditions"=>array('Coagroup.coatype_id'=>$ThisCoatype["Coatype"]["id"]),
			)
		);
		
		foreach($Coagroup as $ThisCoagroup):
			$group_opening_debit_balance =0; 
			$group_opening_credit_balance =0; 
			$group_net_change_debit_balance =0;
			$group_net_change_credit_balance =0;  
			$group_debit_closing =0;
			$group_credit_closing =0;
			$count_group=0;
			$displaytable.= "<tr><td class='bold-text'>".$ThisCoagroup["Coagroup"]["coagroupcode"]."</td><td class='bold-text'>".$ThisCoagroup["Coagroup"]["coagroupname"]."</td>
			 <td class='bold-text' colspan='6'></td></tr>";
			$Coaclass_id[] = array('Coaclass.coagroup_id'=>$ThisCoagroup["Coagroup"]["id"]);
			$Coaclass=$this->Coaclass->find(
				'all',array(
					"fields" => array(       
						'Coaclass.id',
						'Coaclass.coaclasscode',
						'Coaclass.coaclassname'
					),
					"conditions"=> array('Coaclass.coagroup_id'=>$ThisCoagroup["Coagroup"]["id"]),
					"order"=>array('Coaclass.coaclasscode ASC')
				)
			); 
			$total_group=$this->Coaclass->find(
			'all',array(
				"fields" => array(
					'count(Coaclass.id) as total_group'
				),
				"conditions"=>array('Coaclass.coagroup_id'=>$ThisCoagroup["Coagroup"]["id"]),
			)
			);
			$count_type++;   
			foreach($Coaclass as $ThisCoaclass):
				$class_opening_debit_balance =0; 
				$class_opening_credit_balance =0; 
				$class_net_change_debit_balance =0;
				$class_net_change_credit_balance =0;  
				$class_debit_closing =0;
				$class_credit_closing =0;
				$count_sub_class =0;
				$displaytable.= "<tr><td class='bold-text'>".$ThisCoaclass["Coaclass"]["coaclasscode"]."</td><td class='bold-text'>".$ThisCoaclass["Coaclass"]["coaclassname"]."</td>
				                 <td class='bold-text' colspan='6'></td></tr>";
				$Coaclasssubclass_id[] = array('Coaclasssubclass.coaclass_id'=>$ThisCoaclass["Coaclass"]["id"]);
				$Coaclasssubclass=$this->Coaclasssubclass->find(
					'all',array(
						"fields" => array(
							'Coaclasssubclass.*'
						),
						"conditions"=> array('Coaclasssubclass.coaclass_id'=>$ThisCoaclass["Coaclass"]["id"]),
						"order"=>array('Coaclasssubclass.coaclasssubclasscode ASC')
					)
				);
				$total_sub_class = $this->Coaclasssubclass->find(
												'all',array(
													"fields" => array(
														'count(Coaclasssubclass.id) as total_sub_class'
													),
													"conditions"=> array('Coaclasssubclass.coaclass_id'=>$ThisCoaclass["Coaclass"]["id"])
													
												)
											);
				$count_group++;
				foreach($Coaclasssubclass as $ThisCoaclasssubclass):
					$count_coa =0;
					$subtotal_opening_debit_balance =0; 
					$subtotal_opening_credit_balance =0; 
					$subtotal_net_change_debit_balance =0;
					$subtotal_net_change_credit_balance =0;  
					$subtotal_debit_closing =0;
					$subtotal_credit_closing =0;
					$displaytable.= "<tr><td class='bold-text'>".$ThisCoaclasssubclass["Coaclasssubclass"]["coaclasssubclasscode"]."</td><td class='bold-text'>".$ThisCoaclasssubclass["Coaclasssubclass"]["coaclasssubclassname"]."</td>
					                <td class='bold-text' colspan='6'></td></tr>";
					$Coa=$this->Coa->find(
						'all',array(
							"fields" => array(
								'Coa.id',
								'Coa.coacode',
								'Coa.coaname'
							),
							"conditions"=>array('Coa.coaclasssubclass_id'=>$ThisCoaclasssubclass["Coaclasssubclass"]["id"]),
							"order"=>array('Coa.coacode ASC')
						)
					);
					$count_sub_class++;
					foreach($Coa as $ThisCoa):
					    $opening_debit_balance[0][0]['opening_debit_balance']=0;
						$opening_balance_credit =0;
						$net_change_debit_arr[0][0]["net_change_debit"] =0;
						$net_change_credit = 0;
						$closing_balance = 0;
						$debit_closing_balance = 0;
                        $credit_closing_balance = 0;

                        $total_coa = $this->Coa->find(
										'all',array(
											"fields" => array(
												'count(Coa.id) as total_coa'
												
											),
											"conditions"=>array('Coa.coaclasssubclass_id'=>$ThisCoaclasssubclass["Coaclasssubclass"]["id"])
											
										)
									);
                       
                        $net_change_debit_arr = $this->Journaltransaction->find('all',array("fields" => "sum(Journaltransaction.transactionamount) as net_change_debit",
												 "conditions"=>array('Journaltransaction.coa_id'=>$ThisCoa['Coa']['id'],
												 'Journaltransaction.coa_id=Coa.id','Journaltransaction.transactionamount >'=>0,'Journaltransaction.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']))));
						$net_change_credit_arr = $this->Journaltransaction->find('all',array("fields" => "sum(Journaltransaction.transactionamount) as net_change_credit",
												 "conditions"=>array('Journaltransaction.coa_id'=>$ThisCoa['Coa']['id'],
												 'Journaltransaction.coa_id=Coa.id','Journaltransaction.transactionamount <'=>0,'Journaltransaction.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']))));
						$net_change_credit = str_replace("-", "", number_format($net_change_credit_arr[0][0]["net_change_credit"],2));
						$opening_debit_balance = $this->Journaltransaction->find('all',array("fields" =>"sum(Journaltransaction.transactionamount) as opening_debit_balance",
						"conditions"=>array('Journaltransaction.coa_id'=>$ThisCoa['Coa']['id'],'Journaltransaction.transactionamount >'=>0,
						'Journaltransaction.transactiondate <'=>$this->request->data['from_date'])));
						$opening_credit_balance_arr = $this->Journaltransaction->find('all',array("fields" =>"sum(Journaltransaction.transactionamount) as opening_credit_balance",
						"conditions"=>array('Journaltransaction.coa_id'=>$ThisCoa['Coa']['id'],'Journaltransaction.transactionamount <'=>0,
						'Journaltransaction.transactiondate <'=>$this->request->data['from_date'])));
						if(!empty($opening_credit_balance_arr))
							$opening_balance_credit = str_replace("-", "", $opening_credit_balance_arr[0][0]["opening_credit_balance"]);

                        $closing_balance = ($opening_debit_balance[0][0]['opening_debit_balance'] - $opening_balance_credit)+($net_change_debit_arr[0][0]["net_change_debit"] - $net_change_credit);
						if($closing_balance>0)
                            $debit_closing_balance =  $closing_balance;
                        else
                            $credit_closing_balance =  $closing_balance;

                        //group subtotal start
                        $group_net_change_debit_balance = $group_net_change_debit_balance+$net_change_debit_arr[0][0]["net_change_debit"];
						$group_net_change_credit_balance = $group_net_change_credit_balance+ $net_change_credit_arr[0][0]["net_change_credit"];
						//group subtotal end

                        //subclass subtotal start
                     	$class_net_change_debit_balance = $class_net_change_debit_balance + $net_change_debit_arr[0][0]["net_change_debit"];
                        $class_net_change_credit_balance = $class_net_change_credit_balance + $net_change_credit_arr[0][0]["net_change_credit"];
                        //subclass subtotal end

                        //coa subtotal start
                        $subtotal_net_change_debit_balance = $subtotal_net_change_debit_balance + $net_change_debit_arr[0][0]["net_change_debit"];
                        $subtotal_net_change_credit_balance = $subtotal_net_change_credit_balance + $net_change_credit_arr[0][0]["net_change_credit"];
                        //coa subtotal end

                        //coa type subtotal start
                     	$type_net_change_debit_balance =$type_net_change_debit_balance+$net_change_debit_arr[0][0]["net_change_debit"];
						$type_net_change_credit_balance = $type_net_change_credit_balance+$net_change_credit_arr[0][0]["net_change_credit"];  
						//coa type subtotal end

                       $displaytable.= "<tr><td>".$ThisCoa["Coa"]["coacode"]."</td><td>".$ThisCoa["Coa"]["coaname"]."</td><td class='right-aligned-texts'>".number_format($net_change_debit_arr[0][0]["net_change_debit"],2)."</td><td class='right-aligned-texts'>".$net_change_credit."</td>
										</tr>";
						//print_r($total_coa);
						$count_coa ++;

						//subclass subtotal display start
						if($count_coa==$total_coa[0][0]["total_coa"]){
							$split_coa_code = explode("-", $ThisCoa["Coa"]["coacode"]);
							$sub_total_coa_code = $split_coa_code[0].'-'.$split_coa_code[1].'-'.'999';
							$displaytable.= "<tr><td class='bold-text'>".$sub_total_coa_code."</td><td class='bold-text'>Total ".$ThisCoaclasssubclass["Coaclasssubclass"]["coaclasssubclassname"]."</td>
											<td class='bold-right-aligned-text'>".number_format($subtotal_net_change_debit_balance,2)."</td><td class='bold-right-aligned-text'>".str_replace("-", "",number_format($subtotal_net_change_credit_balance,2))."</td>
											</tr>";
							
						}
						//subclass subtotal display end

						//class subtotal display start
						if($count_coa==$total_coa[0][0]["total_coa"] && $count_sub_class == $total_sub_class[0][0]["total_sub_class"]){
							$split_coa_code = explode("-", $ThisCoa["Coa"]["coacode"]);
							$sub_total_coa_code = $split_coa_code[0].'-09-'.'999';
							$displaytable.= "<tr><td class='bold-text'>".$sub_total_coa_code."</td><td class='bold-text'>Total ".$ThisCoaclass["Coaclass"]["coaclassname"]."</td>
											<td class='bold-right-aligned-text'>".number_format($subtotal_net_change_debit_balance,2)."</td><td class='bold-right-aligned-text'>".str_replace("-", "",number_format($subtotal_net_change_credit_balance,2))."</td>
											</tr>";
							if($sub_total_coa_code =='4101-09-999') {
								$gross_profit_or_loass = str_replace("-","", $total_income_net_change_credit) - $total_direct_expense_net_change_debit;
								if($gross_profit_or_loass>0)
									$gross_profit = $gross_profit_or_loass;
								else
									$gross_loss = str_replace("-", "",number_format($gross_profit_or_loass,2));
								$displaytable .= "<tr><td class='bold-text'>4101-99-999</td><td class='bold-text'>Gross Profit</td>
										<td class='bold-right-aligned-text'>" . $gross_loss. "</td><td class='bold-right-aligned-text'>".str_replace("-", "",number_format($gross_profit,2))."</td>
										</tr>";

							}
						
						}
						//class subtotal display end

						//group subtotal display start
						if($count_coa==$total_coa[0][0]["total_coa"] && $count_sub_class == $total_sub_class[0][0]["total_sub_class"] && $count_group == $total_group[0][0]["total_group"]){
							$split_group_code = substr($ThisCoa["Coa"]["coacode"], 0, 2);;
							$sub_total_group_code = $split_group_code.'99-99-'.'999';
							$displaytable.= "<tr><td class='bold-text'>".$sub_total_group_code."</td><td class='bold-text'>Total ".$ThisCoagroup["Coagroup"]["coagroupname"]."</td>
										<td class='bold-right-aligned-text'>".number_format($group_net_change_debit_balance,2)."</td><td class='bold-right-aligned-text'>".str_replace("-", "",number_format($group_net_change_credit_balance,2))."</td>
										</tr>";
						
						}
						//group subtotal display end

						//type subtotal display start
						if($count_coa==$total_coa[0][0]["total_coa"] && $count_sub_class == $total_sub_class[0][0]["total_sub_class"] &&
						 	$count_group == $total_group[0][0]["total_group"] && $count_type == $total_type[0][0]["total_type"]){
							if($ThisCoatype["Coatype"]["id"]==4){
								$total_income_net_change_debit = $type_net_change_debit_balance;
								$total_income_net_change_credit = $type_net_change_credit_balance;

							}
							//total income calculation end
							//total expense calculation start
							if($ThisCoatype["Coatype"]["id"]==6){
								$total_expense_net_change_debit = $type_net_change_debit_balance;
								$total_expense_net_change_credit = $type_net_change_credit_balance;

							}
							//total expense calculation end
							//total other income calculation start
							if($ThisCoatype["Coatype"]["id"]==7){
								$total_other_income_net_change_debit = $type_net_change_debit_balance;
								$total_other_income_net_change_credit = $type_net_change_credit_balance;

							}
							//total other income calculation end
							//total other expense calculation start
							if($ThisCoatype["Coatype"]["id"]==8){
								$total_other_expense_net_change_debit = $type_net_change_debit_balance;
								$total_other_expense_net_change_credit = $type_net_change_credit_balance;

							}
							//total other expense calculation end
							$displaytable.= "<tr><td class='bold-text'>".$type_code ."</td><td class='bold-text'>Total ".$ThisCoatype["Coatype"]["coatypename"]."</td>
											<td class='bold-right-aligned-text'>".number_format($type_net_change_debit_balance,2)."</td><td class='bold-right-aligned-text'>".str_replace("-", "",number_format($type_net_change_credit_balance,2))."</td>
											</tr>";
						
						}
						//type subtotal display end


					endforeach;
				endforeach;
			endforeach;
		endforeach;
	  endforeach;
	  $net_profit_or_loss = str_replace("-", "",$total_income_net_change_credit)-$total_expense_net_change_debit+$total_other_income_net_change_credit-$total_other_expense_net_change_debit;
	  if($net_profit_or_loss>0)
		  $displaytable .= "<tr><td class='bold-text'>8999-99-998</td><td class='bold-text'>Net Profit/Loss</td>
										<td class='bold-right-aligned-text'></td><td class='bold-right-aligned-text'>".number_format($net_profit_or_loss,2)."</td>
										</tr>";
	  else
		  $displaytable .= "<tr><td class='bold-text'>8999-99-998</td><td class='bold-text'>Net Profit/Loss</td>
										<td class='bold-right-aligned-text'>".str_replace("-", "",$net_profit_or_loss)."</td><td class='bold-right-aligned-text'></td>
										</tr>";
	  $this->set('displaytable',$displaytable);
	  $this->set('profit_or_loss',$net_profit_or_loss);

	  $company_info = $this->Company->find('all',array("fields" =>"Company.companyname,Company.companypresentaddress",
		  "conditions"=>array('Company.id'=>$_SESSION['User']['company_id'])));
	  $this->set('company_info',$company_info);
	  $this->set('form_date',$this->request->data['from_date']);
	  $this->set('to_date',$this->request->data['to_date']);
  }

  public function balancesheet(){
  	$this->layout='default';
	$this->set('title_for_layout', __('Balance Sheet').' | '.__(Configure::read('site_name')));
  }

  function balancesheetgenerate(){
  	$this->layout = 'ajax';
	$coatype_id = array();
	$Coagroup_id=array('');
	$Coaclass_id=array();
	$Coaclasssubclass_id=array();
	$company_id=$_SESSION["User"]["company_id"];
	$branch_id=$_SESSION["User"]["branch_id"];
	$group_id=$_SESSION["User"]["group_id"];
	$user_id=$_SESSION["User"]["id"];
	$displaytable='';
	$Coatype=$this->Coatype->find(
		'all',array(
			"fields" => array(
				'Coatype.id',
				'Coatype.coatypename',
				'Coatype.coatypecode'
			),
			"conditions"=>array('Coatype.id' => array(1,2)),
			"order"=>array('Coatype.coatypecode ASC')
		)
	);
	foreach($Coatype as $ThisCoatype):
		$type_opening_debit_balance =0; 
		$type_opening_credit_balance =0; 
		$type_net_change_debit_balance =0;
		$type_net_change_credit_balance =0;  
		$type_debit_closing =0;
		$type_credit_closing =0;
		$count_type= 0;
		if($ThisCoatype["Coatype"]["id"]==1)
			$type_code = '2099-99-999';
		else if($ThisCoatype["Coatype"]["id"]==2)
			$type_code = '2800-99-999';
		else if($ThisCoatype["Coatype"]["id"]==4)
			$type_code = '3099-99-999';
		else
			$type_code = '5099-99-999
';

		$displaytable.= "<tr><td class='bold-text'>".$ThisCoatype["Coatype"]["coatypecode"]."</td><td class='bold-text'>".$ThisCoatype["Coatype"]["coatypename"]."</td>
		                 <td class='bold-text' colspan='6'></td></tr>";
		$Coagroup_id = array('Coagroup.coatype_id'=>$ThisCoatype["Coatype"]["id"]);
		$Coagroup=$this->Coagroup->find(
			'all',array(
				"fields" => array(
					'Coagroup.*'
				),
				"conditions"=>array('Coagroup.coatype_id'=>$ThisCoatype["Coatype"]["id"]),
				"order"=>array('Coagroup.coagroupcode ASC')
			)
		);
		$total_type=$this->Coagroup->find(
			'all',array(
				"fields" => array(
					'count(Coagroup.id) as total_type'
				),
				"conditions"=>array('Coagroup.coatype_id'=>$ThisCoatype["Coatype"]["id"]),
			)
		);
		
		foreach($Coagroup as $ThisCoagroup):
			$group_opening_debit_balance =0; 
			$group_opening_credit_balance =0; 
			$group_net_change_debit_balance =0;
			$group_net_change_credit_balance =0;  
			$group_debit_closing =0;
			$group_credit_closing =0;
			$count_group=0;
			$displaytable.= "<tr><td class='bold-text'>".$ThisCoagroup["Coagroup"]["coagroupcode"]."</td><td class='bold-text'>".$ThisCoagroup["Coagroup"]["coagroupname"]."</td>
			 <td class='bold-text' colspan='6'></td></tr>";
			$Coaclass_id[] = array('Coaclass.coagroup_id'=>$ThisCoagroup["Coagroup"]["id"]);
			$Coaclass=$this->Coaclass->find(
				'all',array(
					"fields" => array(       
						'Coaclass.id',
						'Coaclass.coaclasscode',
						'Coaclass.coaclassname'
					),
					"conditions"=> array('Coaclass.coagroup_id'=>$ThisCoagroup["Coagroup"]["id"]),
					"order"=>array('Coaclass.coaclasscode ASC')
				)
			); 
			$total_group=$this->Coaclass->find(
			'all',array(
				"fields" => array(
					'count(Coaclass.id) as total_group'
				),
				"conditions"=>array('Coaclass.coagroup_id'=>$ThisCoagroup["Coagroup"]["id"]),
			)
			);
			$count_type++;   
			foreach($Coaclass as $ThisCoaclass):
				$class_opening_debit_balance =0; 
				$class_opening_credit_balance =0; 
				$class_net_change_debit_balance =0;
				$class_net_change_credit_balance =0;  
				$class_debit_closing =0;
				$class_credit_closing =0;
				$count_sub_class =0;
				$displaytable.= "<tr><td class='bold-text'>".$ThisCoaclass["Coaclass"]["coaclasscode"]."</td><td class='bold-text'>".$ThisCoaclass["Coaclass"]["coaclassname"]."</td>
				                 <td colspan='6' class='bold-text'></td></tr>";
				$Coaclasssubclass_id[] = array('Coaclasssubclass.coaclass_id'=>$ThisCoaclass["Coaclass"]["id"]);
				$Coaclasssubclass=$this->Coaclasssubclass->find(
					'all',array(
						"fields" => array(
							'Coaclasssubclass.*'
						),
						"conditions"=> array('Coaclasssubclass.coaclass_id'=>$ThisCoaclass["Coaclass"]["id"]),
						"order"=>array('Coaclasssubclass.coaclasssubclasscode ASC')
					)
				);
				$total_sub_class = $this->Coaclasssubclass->find(
												'all',array(
													"fields" => array(
														'count(Coaclasssubclass.id) as total_sub_class'
													),
													"conditions"=> array('Coaclasssubclass.coaclass_id'=>$ThisCoaclass["Coaclass"]["id"])
													
												)
											);
				$count_group++;
				foreach($Coaclasssubclass as $ThisCoaclasssubclass):
					$count_coa =0;
					$subtotal_opening_debit_balance =0; 
					$subtotal_opening_credit_balance =0; 
					$subtotal_net_change_debit_balance =0;
					$subtotal_net_change_credit_balance =0;  
					$subtotal_debit_closing =0;
					$subtotal_credit_closing =0;
					$displaytable.= "<tr><td class='bold-text'>".$ThisCoaclasssubclass["Coaclasssubclass"]["coaclasssubclasscode"]."</td><td class='bold-text'>".$ThisCoaclasssubclass["Coaclasssubclass"]["coaclasssubclassname"]."</td>
					                <td class='bold-text' colspan='6'></td></tr>";
					$Coa=$this->Coa->find(
						'all',array(
							"fields" => array(
								'Coa.id',
								'Coa.coacode',
								'Coa.coaname'
							),
							"conditions"=>array('Coa.coaclasssubclass_id'=>$ThisCoaclasssubclass["Coaclasssubclass"]["id"]),
							"order"=>array('Coa.coacode ASC')
						)
					);
					$count_sub_class++;
					foreach($Coa as $ThisCoa):
					    $opening_debit_balance[0][0]['opening_debit_balance']=0;
						$opening_balance_credit =0;
						$net_change_debit_arr[0][0]["net_change_debit"] =0;
						$net_change_credit = 0;
						$closing_balance = 0;
						$debit_closing_balance = 0;
                        $credit_closing_balance = 0;

                        $total_coa = $this->Coa->find(
										'all',array(
											"fields" => array(
												'count(Coa.id) as total_coa'
												
											),
											"conditions"=>array('Coa.coaclasssubclass_id'=>$ThisCoaclasssubclass["Coaclasssubclass"]["id"])
											
										)
									);
                       
                        $net_change_debit_arr = $this->Journaltransaction->find('all',array("fields" => "sum(Journaltransaction.transactionamount) as net_change_debit",
												 "conditions"=>array('Journaltransaction.coa_id'=>$ThisCoa['Coa']['id'],
												 'Journaltransaction.coa_id=Coa.id','Journaltransaction.transactionamount >'=>0,'Journaltransaction.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']))));
						$net_change_credit_arr = $this->Journaltransaction->find('all',array("fields" => "sum(Journaltransaction.transactionamount) as net_change_credit",
												 "conditions"=>array('Journaltransaction.coa_id'=>$ThisCoa['Coa']['id'],
												 'Journaltransaction.coa_id=Coa.id','Journaltransaction.transactionamount <'=>0,'Journaltransaction.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']))));
						$net_change_credit = str_replace("-", "", $net_change_credit_arr[0][0]["net_change_credit"]);
						$opening_debit_balance = $this->Journaltransaction->find('all',array("fields" =>"sum(Journaltransaction.transactionamount) as opening_debit_balance",
						"conditions"=>array('Journaltransaction.coa_id'=>$ThisCoa['Coa']['id'],'Journaltransaction.transactionamount >'=>0,
						'Journaltransaction.transactiondate <'=>$this->request->data['from_date'])));
						$opening_credit_balance_arr = $this->Journaltransaction->find('all',array("fields" =>"sum(Journaltransaction.transactionamount) as opening_credit_balance",
						"conditions"=>array('Journaltransaction.coa_id'=>$ThisCoa['Coa']['id'],'Journaltransaction.transactionamount <'=>0,
						'Journaltransaction.transactiondate <'=>$this->request->data['from_date'])));
						if(!empty($opening_credit_balance_arr))
							$opening_balance_credit = str_replace("-", "", $opening_credit_balance_arr[0][0]["opening_credit_balance"]);

                        $closing_balance = ($opening_debit_balance[0][0]['opening_debit_balance'] - $opening_balance_credit)+($net_change_debit_arr[0][0]["net_change_debit"] - $net_change_credit);
						if($closing_balance>0)
                            $debit_closing_balance =  $closing_balance;
                        else
                            $credit_closing_balance =  $closing_balance;

                        //group subtotal start
                       	$group_debit_closing =$group_debit_closing+$debit_closing_balance;
						$group_credit_closing =$group_credit_closing+$credit_closing_balance;
                        //group subtotal end

                        //subclass subtotal start
                       	$class_debit_closing = $class_debit_closing + $debit_closing_balance;
                        $class_credit_closing = $class_credit_closing + $credit_closing_balance;
                        //subclass subtotal end

                        //coa subtotal start
                        $subtotal_debit_closing = $subtotal_debit_closing + $debit_closing_balance;
                        $subtotal_credit_closing = $subtotal_credit_closing + $credit_closing_balance;
                        //coa subtotal end

                        //coa type subtotal start
                        $type_debit_closing =$type_debit_closing+$debit_closing_balance;
						$type_credit_closing =$type_credit_closing+$credit_closing_balance;
						//coa type subtotal end

                       $displaytable.= "<tr><td>".$ThisCoa["Coa"]["coacode"]."</td><td>".$ThisCoa["Coa"]["coaname"]."</td>
										<td class='right-aligned-texts'>".number_format($debit_closing_balance,2)."</td><td class='right-aligned-texts'>".str_replace("-", "",number_format($credit_closing_balance,2))."</td></tr>";
						//print_r($total_coa);
						$count_coa ++;

						//subclass subtotal display start
						if($count_coa==$total_coa[0][0]["total_coa"]){
							$split_coa_code = explode("-", $ThisCoa["Coa"]["coacode"]);
							$sub_total_coa_code = $split_coa_code[0].'-'.$split_coa_code[1].'-'.'999';
							$displaytable.= "<tr><td class='bold-text'>".$sub_total_coa_code."</td><td class='bold-text'>Total ".$ThisCoaclasssubclass["Coaclasssubclass"]["coaclasssubclassname"]."</td>
											<td class='bold-right-aligned-text'>".number_format($subtotal_debit_closing,2)."</td><td class='bold-right-aligned-text'>".str_replace("-", "",number_format($subtotal_credit_closing,2))."</td></tr>";
							
						}
						//subclass subtotal display end

						//class subtotal display start
						if($count_coa==$total_coa[0][0]["total_coa"] && $count_sub_class == $total_sub_class[0][0]["total_sub_class"]){
							$split_coa_code = explode("-", $ThisCoa["Coa"]["coacode"]);
							$sub_total_coa_code = $split_coa_code[0].'-99-'.'999';
							$displaytable.= "<tr><td class='bold-text'>".$sub_total_coa_code."</td><td class='bold-text'>Total ".$ThisCoaclass["Coaclass"]["coaclassname"]."</td>
												<td class='bold-right-aligned-text'>".number_format($subtotal_debit_closing,2)."</td><td class='bold-right-aligned-text'>".str_replace("-", "",number_format($subtotal_credit_closing,2))."</td></tr>";
						
						}
						//class subtotal display end

						//group subtotal display start
						if($count_coa==$total_coa[0][0]["total_coa"] && $count_sub_class == $total_sub_class[0][0]["total_sub_class"] && $count_group == $total_group[0][0]["total_group"]){
							$split_group_code = substr($ThisCoa["Coa"]["coacode"], 0, 2);;
							$sub_total_group_code = $split_group_code.'99-99-'.'999';
							$displaytable.= "<tr><td class='bold-text'>".$sub_total_group_code."</td><td class='bold-text'>Total ".$ThisCoagroup["Coagroup"]["coagroupname"]."</td>
										<td class='bold-right-aligned-text'>".number_format($group_debit_closing,2)."</td><td class='bold-right-aligned-text'>".str_replace("-", "",number_format($group_credit_closing,2))."</td></tr>";
						
						}
						//group subtotal display end

						//type subtotal display start
						if($count_coa==$total_coa[0][0]["total_coa"] && $count_sub_class == $total_sub_class[0][0]["total_sub_class"] &&
						 	$count_group == $total_group[0][0]["total_group"] && $count_type == $total_type[0][0]["total_type"]){
							$split_group_code = substr($ThisCoa["Coa"]["coacode"], 0, 2);;
							$sub_total_group_code = $split_group_code.'99-99-'.'999';
							$displaytable.= "<tr><td class='bold-text'>".$type_code ."</td><td class='bold-text'>Total ".$ThisCoatype["Coatype"]["coatypename"]."</td>
										<td class='bold-right-aligned-text'>".number_format($type_debit_closing,2)."</td><td class='bold-right-aligned-text'>".str_replace("-", "",number_format($type_credit_closing,2))."</td></tr>";
						
						}
						//type subtotal display end


					endforeach;
				endforeach;
			endforeach;
		endforeach;
	  endforeach;
	  $this->set('displaytable',$displaytable);

	  $company_info = $this->Company->find('all',array("fields" =>"Company.companyname,Company.companypresentaddress",
		  "conditions"=>array('Company.id'=>$_SESSION['User']['company_id'])));
	  $this->set('company_info',$company_info);
	  $this->set('form_date',$this->request->data['from_date']);
	  $this->set('to_date',$this->request->data['to_date']);
  }

	public function userlistbygroup(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$GroupId = @$this->request->data['GroupId'];
		//$UserId = @$this->request->data['GltransactionUserId'];
		$transactionothernote = @$this->request->data['GltransactionGltransactionothernote'];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$json_array = array();
		$user_array = array();
		if($group_id==1):
		elseif($group_id==2):
			$user_array[] = array('NOT'=>array('User.group_id'=>1));
			$user_array[] = array('User.company_id'=>$company_id);
			$user_array[] = array('User.company_id'=>$company_id);
			$user_array[] = array('User.userisactive'=>1);
		else:
			$user_array[] = array('NOT'=>array('User.group_id'=>array(1, 2)));
			$user_array[] = array('User.company_id'=>$company_id);
			$user_array[] = array('User.userisactive'=>1);
		endif;
		$this->set('GroupId',$GroupId);
		$user=$this->User->find(
			'all',
			array(
				"fields" => array("id", "userfirstname", "usermiddlename", "userlastname"),
				"conditions"=>array('User.group_id'=>$GroupId))
		);
		//$this->set('user',$user);
		$user_option = "<option value=\"0\">".__("Select Member")."</option>";
		foreach ($user as $thisuser) {
			$user_name = $thisuser["User"]["userfirstname"].' '.$thisuser["User"]["usermiddlename"].' '.$thisuser["User"]["userlastname"];
			$user_option .= "<option value=\"{$thisuser["User"]["id"]}\" >{$user_name}</option>";
		}
		echo $user_option;
	}

   public function partieledger(){
	   $this->layout='default';
	   $this->set('title_for_layout', __('Party Ledger').' | '.__(Configure::read('site_name')));


	   $group_id=$this->Group->find(
		   'list',
		   array(
			   "fields" => array("id","id")
		   )
	   );
	   $this->set('group_id',$group_id);

	   $group=$this->Group->find(
		   'list',
		   array(
			   "fields" => array("id","groupname")
		   )
	   );
	   $this->set('group',$group);

	   	 $user_id=$this->Coa->find(
			   'list',
			   array(
				   "fields" => array("id","coacode")
			   )
		   );
		   $this->set('user_id',$user_id);

		   $coa_name=$this->Coa->find(
			   'list',
			   array(
				   "fields" => array("id","coa_name")
			   )
		   );
		   $this->set('coa_name',$coa_name);

   }

	function partyledgergenerate(){
		$this->layout = 'ajax';
		$party_ledger_data = $this->Journaltransaction->find('all',array("fields" => "Journaltransaction.transactionamount,
								 Journaltransaction.voucher_no,Journaltransaction.transactiondate,Coa.id,Journaltransaction.particulars,User.id,User.userfirstname,User.usermiddlename,User.userlastname",
			    "conditions"=>array('Journaltransaction.user_id'=>$this->request->data['user_id'],
				'Journaltransaction.coa_id=Coa.id','Journaltransaction.user_id=User.id','Journaltransaction.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']))));
		$this->set('party_ledger_data',$party_ledger_data);

		/*$coatype=$this->Coa->find(
			'all',array(
				"fields" => array(
					'Coa.coatype_id'
				),
				"conditions"=>array('Coa.id'=>$this->request->data['coa_id']),

			)
		);
		$this->set('coatype',$coatype);*/

		$company_info = $this->Company->find('all',array("fields" =>"Company.companyname,Company.companypresentaddress",
			"conditions"=>array('Company.id'=>$_SESSION['User']['company_id'])));
		$this->set('company_info',$company_info);

		$total_debit = $this->Journaltransaction->find('all',array("fields" =>"sum(Journaltransaction.transactionamount) as total_debit",
			"conditions"=>array('Journaltransaction.user_id'=>$this->request->data['user_id'],'Journaltransaction.transactionamount >'=>0,
				'Journaltransaction.transactiondate <'=>$this->request->data['from_date'])));
		$this->set('total_debit',$total_debit);

		$total_credit = $this->Journaltransaction->find('all',array("fields" =>"sum(Journaltransaction.transactionamount) as total_credit",
			"conditions"=>array('Journaltransaction.user_id'=>$this->request->data['user_id'],'Journaltransaction.transactionamount <'=>0,
				'Journaltransaction.transactiondate <'=>$this->request->data['from_date'])));
		$this->set('total_credit',$total_credit);
		$this->set('form_date',$this->request->data['from_date']);
		$this->set('to_date',$this->request->data['to_date']);

	}

  //End Journalreport
 
}