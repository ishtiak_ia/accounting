<?php
		echo $Utilitys->paginationcommon();
		echo $Utilitys->tablestart($class=null, $id=null);
		echo $Utilitys->tablestart($class=null, $id=null);
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('Account')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Cheque Series')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Cheque Sequency Form')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Cheque Sequency To')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Page')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Amount')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Bank')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Branch')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Account Type')  => array('class' => 'highlight sortable')
						), 
						__('Active'), 
						__('Action')
					)
				);
				$bankchequebookrows = array();
				$countBankchequebook = 0-1+$this->Paginator->counter('%start%');
				foreach($bankchequebook as $Thisbankchequebook):
					$countBankchequebook++;
					$bankchequebookrows[]= array(
						$countBankchequebook,
						$Utilitys->amountDetailsURL(
							$Thisbankchequebook["Bankchequebook"]["bankaccount_name"],
							$Thisbankchequebook["Bankchequebook"]["bankaccount_id"],
							"Banktransaction",
							"bankaccount_id"
							),
						$Thisbankchequebook["Bankchequebook"]["bankchequebookseries"],
						$Thisbankchequebook["Bankchequebook"]["bankchequebooksequencyfrom"],
						$Thisbankchequebook["Bankchequebook"]["bankchequebooksequencyto"],
						$Thisbankchequebook["Bankchequebook"]["bankchequebook_totalpage"],
						$Utilitys->amountDetailsURL(
							$Thisbankchequebook["Bankchequebook"]["bankchequebook_totalamount"],
							$Thisbankchequebook["Bankchequebook"]["id"],
							"Banktransaction",
							"bankchequebook_id"
							),
						$Thisbankchequebook["Bankchequebook"]["bank_name"],
						$Thisbankchequebook["Bankchequebook"]["bankbranch_name"],
						$Thisbankchequebook["Bankchequebook"]["bankaccounttype_name"],
						$Utilitys->statusURL(
							$this->request["controller"],
							'bankchequebook',
							$Thisbankchequebook["Bankchequebook"]["id"],
							$Thisbankchequebook["Bankchequebook"]["bankchequebookuuid"],
							$Thisbankchequebook["Bankchequebook"]["bankchequebookisactive"]
							),
						$Utilitys->cusurl(
							$this->request["controller"],
							'bankchequebook',
							$Thisbankchequebook["Bankchequebook"]["id"],
							$Thisbankchequebook["Bankchequebook"]["bankchequebookuuid"]
							)
						);
				endforeach;
				echo $this->Html->tableCells(
					$bankchequebookrows
				);
		echo $Utilitys->tableend();
		echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#BankSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#banksarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#banksarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>