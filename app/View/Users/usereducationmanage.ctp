<?php
	echo "<h2>".__('Academic Qualification'). $Utilitys->addurl($title=__("Add New Academic Qualification"), $this->request['controller'], $page="usereducation")."</h2>";
	echo $Utilitys->filtertagstart();
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-12\">";
			echo $this->Form->input(
				"Usereducation.department_id",
				array(
					'type'=>'select',
					"options"=>array($department_list),
					'empty'=>__('Select Department'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>1,
					'label'=>false,
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style'=>'',
					"selected" => (@$usereducation[0]['Usereducation']['department_id'] ? $usereducation[0]['Usereducation']['department_id'] : 0)
				)
			);
			echo "<div class=\"form-group\" id=\"usereducationuserchangediv\">";
			echo $this->Form->input(
				"Usereducation.user_id",
				array(
					'type'=>'select',
					"options"=>array($user_list),
					'empty'=>__('Select Employee'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>2,
					'label'=>false,
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style'=>'',
					"selected" => (@$salary[0]['Salary']['user_id'] ? $salary[0]['Salary']['user_id'] : 0)
				)
			);
            echo "</div>";
		echo $this->Form->input(
			"Usereducation.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"usereducationsarchloading\">
	";
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Employee Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Exam/Degree Title')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Institute Name')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Result')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Passing Year')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Duration')  => array('class' => 'highlight sortable')
					),
					__('Active'), 
					__('Action')
				)
			);
			$usereducationrows = array();
			$countUsereducationrows = 0-1+$this->Paginator->counter('%start%');
			foreach($usereducation as $Thisusereducation):
				$countUsereducationrows++;
				$usereducationrows[]= array($countUsereducationrows,$Thisusereducation["Usereducation"]["user_fullname"],$Thisusereducation["Usereducation"]["usereducationdegreename"],$Thisusereducation["Usereducation"]["usereducationinstitute"],$Thisusereducation["Usereducation"]["usereducationresult"],$Thisusereducation["Usereducation"]["usereducationpassing"],$Thisusereducation["Usereducation"]["usereducationduration"],$Utilitys->statusURL($this->request["controller"], 'usereducation', $Thisusereducation["Usereducation"]["id"], $Thisusereducation["Usereducation"]["usereducationuuid"], $Thisusereducation["Usereducation"]["usereducationisactive"]),$Utilitys->cusurl($this->request["controller"], 'usereducation', $Thisusereducation["Usereducation"]["id"], $Thisusereducation["Usereducation"]["usereducationuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$usereducationrows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();

	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				$(\"#UsereducationSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#usereducationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usereducationsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#usereducationsarchloading\").html(result);
						}
					);
				});
				$(\"#UsereducationDepartmentId\").change(function(){
					var UsereducationDepartmentId = $(this).val();
					//$(\"#usereducationuserchangediv\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/userlistbydepartmentid',
						{UsereducationDepartmentId:UsereducationDepartmentId},
						function(result) {
							//alert(result);
							$(\"#UsereducationUserId\").html(result);
							//$(\"#usereducationuserchangediv\").html('');
						}
					);
				});
				$(\"#UsereducationUserId\").change(function(){
					var UsereducationUserId = $(\"#UsereducationUserId\").val();
					$(\"#usereducationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usereducationsearchbytext',
						{UsereducationUserId:UsereducationUserId},
						function(result) {
							$(\"#usereducationsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>