<?php
App::uses('AppModel', 'Model');
class Userdepartment extends AppModel {
	public $name = 'Userdepartment';
	public $usetables = 'userdepartments';

	var $belongsTo = array(
		'User' => array(
			'fields' =>array('User.*'),
			'className'    => 'User',
			'foreignKey'    => 'user_id'
		),
		'Department' => array(
			'fields' =>array('Department.*'),
			'className'    => 'Department',
			'foreignKey'    => 'department_id'
		)
	);
	var $virtualFields = array(
		'user_name' => 'CONCAT(User.username)',
		'user_fullname' => 'CONCAT(User.userfirstname, "   ", User.usermiddlename, "   ", User.userlastname)',
		'department_name' => 'CONCAT(Department.departmentname, " / ",  Department.departmentnamebn)',
		'isActive' => 'IF(Userdepartment.userdepartmentisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Userdepartment.userdepartmentisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
}