<?php
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"2\">
				<h2>".__('Bank Cheque Book')."</h2>
			</td>
		</tr>
		<tr>
			<th>
				".__('Bank')."
			</th>
			<td>
				".$bankchequebook[0]["Bankchequebook"]["bank_name"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Branch')."
			</th>
			<td>
				".$bankchequebook[0]["Bankchequebook"]["bankbranch_name"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Account Type')."
			</th>
			<td>
				".$bankchequebook[0]["Bankchequebook"]["bankaccounttype_name"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Account')."
			</th>
			<td>
				".$bankchequebook[0]["Bankchequebook"]["bankaccount_name"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Cheque Series')."
			</th>
			<td>
				".$bankchequebook[0]["Bankchequebook"]["bankchequebookseries"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Cheque Sequency Form")."
			</th>
			<td>
				".$bankchequebook[0]["Bankchequebook"]["bankchequebooksequencyfrom"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Cheque Sequency To")."
			</th>
			<td>
				".$bankchequebook[0]["Bankchequebook"]["bankchequebooksequencyto"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Total Leaf")."
			</th>
			<td>
				".$bankchequebook[0]["Bankchequebook"]["bankchequebook_totalpage"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Total Amount")."
			</th>
			<td>
				".$bankchequebook[0]["Bankchequebook"]["bankchequebook_totalamount"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Active")."
			</th>
			<td>
				".$bankchequebook[0]["Bankchequebook"]["isActive"]."
			</td>
		</tr>
	";
	echo $Utilitys->tableend();
?>