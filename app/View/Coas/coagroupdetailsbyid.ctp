<?php
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"2\">
				<h2>".__('Chart of Account Group')."</h2>
			</td>
		</tr>
		<tr>
			<th>
				".__('Type')."
			</th>
			<td>
				".$coagroup[0]["Coatype"]["coatypename"]."/".$coagroup[0]["Coatype"]["coatypenamebn"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('CODE')."
			</th>
			<td>
				".$coagroup[0]["Coagroup"]["coagroupcode"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Group')."
			</th>
			<td>
				".$coagroup[0]["Coagroup"]["coagroupname"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Group BN")."
			</th>
			<td>
				".$coagroup[0]["Coagroup"]["coagroupnamebn"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Active")."
			</th>
			<td>
				".$coagroup[0]["Coagroup"]["isActive"]."
			</td>
		</tr>
	";
	echo $Utilitys->tableend($class=null, $id=null);
?>