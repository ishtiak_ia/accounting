<?php
App::uses('AppModel', 'Model');
class Company extends AppModel {
	public $name = 'Company';
	public $usetables = 'companies';
	var $belongsTo = array(
	/*'Branch' => array(
			'fields' =>array('branchname', 'branchnamebn'),
			'className'    => 'Branch',
			'foreignKey'    => 'company_id'
		),	*/
	/*'Companyemail' => array(
			'fields' =>array('companyemail'),
			'className'    => 'Companyemail',
			'foreignKey'    => false,
			'conditions'=>array('Company.id=Companyemail.company_id')
		),
	'Companyphone' => array(
			'fields' =>array('companyphoneno'),
			'className'    => 'Companyphone',
			'foreignKey'    => false,
			'conditions'=>array('Company.id=Companyphone.company_id')
		)*/
	);
	var $virtualFields = array(
		'company_name' => 'CONCAT(Company.companyname, " / ", Company.companynamebn)',
		'company_email' => '(SELECT GROUP_CONCAT(Companyemail.companyemailaddress) FROM companyemails as Companyemail WHERE Company.id=Companyemail.company_id	)',
		'company_phone' => '(SELECT GROUP_CONCAT(Companyphone.companyphoneno) FROM companyphones as Companyphone WHERE Company.id=Companyphone.company_id	)',
	    'isActive' => 'IF(Company.companyisactive = 0, "<span class=\"button btn btn-sm btn-warning\"><span class=\"glyphicon glyphicon-remove-circle\" title=\"Inactive\"></span></span>", IF(Company.companyisactive = 1, "<span class=\"button btn btn-sm btn-success\"><span class=\"glyphicon glyphicon-ok-circle\" title=\"Active\"></span></span>", "<span class=\"button btn btn-sm btn-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span></span>"))'
	);
}

?>