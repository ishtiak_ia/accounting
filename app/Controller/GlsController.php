<?php
App::import('Controller', 'Logins');
App::import('Controller', 'Transactions');
App::import('Controller', 'Users');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');

class GlsController extends AppController {
	public $name = 'Gls';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'JqueryValidation');
	public $components = array('RequestHandler','Session');
	public $uses = array('Banktype', 'Bank', 'Bankbranch', 'Bankaccounttype', 'Bankaccount', 'Bankchequebook', 'Banktransaction', 'Company', 'Group', 'User', 'Usergroup', 'Usertransaction', 'Cashtransaction', 'Gltransaction', 'Journaltransaction', 'Coa');

	var $Transactions;
	var $Users;

	var $Logins;
	/*public function index() {
		$this->redirect("login");
	}*/
	function beforeFilter(){
		$this->Logins =& new LoginsController;
		$this->Logins->constructClasses();
		$this->Logins->__validateLoginStatus();
		//$Logins = new LoginsController;
		//$this->set('Logins',$Logins);
		$this->Transactions =& new TransactionsController;
		$this->Transactions->constructClasses();
		$this->Users =& new UsersController;
		$this->Users->constructClasses();


		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	public  function beforeRender(){
		$this->response->disableCache();//prevent useless warnings for Ajax
		if($this->RequestHandler->isAjax()){
			Configure::write('debug', 0);
		}
	}

	//Start Cash//
	public function gltransactionmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Journal Transaction').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$gltransaction_id = array();
		$journaltransaction_id = array();
		$bankaccount_id = array();
		if($group_id==1):
		elseif($group_id==2):
			$gltransaction_id[] = array('Gltransaction.company_id'=>$company_id);
			$journaltransaction_id[] = array('Journaltransaction.company_id'=>$company_id);
			$journaltransaction_id[] = array('Journaltransaction.journaltransactionisactive IN (0,1)');
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$gltransaction_id[] = array('Gltransaction.gltransactionisactive IN (0,1)');
		else:
			$gltransaction_id[] = array('Gltransaction.company_id'=>$company_id);
			$gltransaction_id[] = array('Gltransaction.branch_id'=>$branch_id);
			$gltransaction_id[] = array('Gltransaction.gltransactionisactive IN (1)');
			$journaltransaction_id[] = array('Journaltransaction.company_id'=>$company_id);
			$journaltransaction_id[] = array('Journaltransaction.branch_id'=>$branch_id);
			$journaltransaction_id[] = array('Journaltransaction.journaltransactionisactive IN (1)');
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.branch_id'=>$branch_id);
		endif;

		$bank=$this->Bank->find('list',array("fields" => array("id","bank_name")));
		$this->set('bank',$bank);

		$bankaccount=$this->Bankaccount->find(
			'list',
			array(
				"fields" => array("id","bankaccount_name"),
				"conditions" => $bankaccount_id
			)
		);
		$this->set('bankaccount',$bankaccount);

			$gltransaction_id[] = array('Gltransaction.gltransactionisactive'=>1);
			$journaltransaction_id[] = array('Journaltransaction.journaltransactionisactive'=>1);
		$this->paginate = array(
			'fields' => 'Journaltransaction.*',
			'conditions'=>array_merge($journaltransaction_id),
			'order'  =>'Journaltransaction.transactiondate DESC',
			'limit' => 20
		);
		$journaltransaction = $this->paginate('Journaltransaction');
		$this->set('journaltransaction',$journaltransaction);
	}

	public function gltransactioninsertupdate($id=null,$uuid=null) {
		$this->layout='default';
		$this->set('title_for_layout', __('Journal Transaction').' | '.__(Configure::read('site_name')));
         
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$banktransaction_id = array();
		$bank_id = array();
		$bankaccount_id = array();
		$bankchequebook_id = array();
		$group_array = array();
		$user_array = array();
		$coa_array = array();
		if($group_id==1):
			//$coa_array[] = array('NOT'=>array('Coa.id'=>array(1,2)));
		elseif($group_id==2):
			$banktransaction_id[] = array('Banktransaction.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankchequebook_id[] = array('Bankchequebook.company_id'=>$company_id);
			$bank_id[] = array('Bankaccount.company_id'=>$company_id);
			$group_array[] = array('NOT'=>array('Group.id'=>1));
			$user_array[] = array('NOT'=>array('User.group_id'=>1));
			$user_array[] = array('User.company_id'=>$company_id);
			$coa_array[] = array('NOT'=>array('Coa.coaisactive'=>array(2)));
			$coa_array[] = array('NOT'=>array('Coa.id'=>array(1,2)));
			$coa_array[] = array('Coa.Company_id'=>array(1,$company_id));
		else:
			$banktransaction_id[] = array('Banktransaction.company_id'=>$company_id);
			$banktransaction_id[] = array('Banktransaction.branch_id'=>$branch_id);
			$bank_id[] = array('Bankaccount.company_id'=>$company_id);
			$bank_id[] = array('Bankaccount.branch_id'=>$branch_id);
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.branch_id'=>$branch_id);
			$bankchequebook_id[] = array('Bankchequebook.company_id'=>$company_id);
			$bankchequebook_id[] = array('Bankchequebook.branch_id'=>$branch_id);
			$group_array[] = array('NOT'=>array('Group.id'=>array(1, 2)));
			$user_array[] = array('NOT'=>array('User.group_id'=>array(1, 2)));
			$user_array[] = array('User.company_id'=>$company_id);
			$user_array[] = array('User.branch_id'=>$branch_id);
			$coa_array[] = array('NOT'=>array('Coa.coaisactive'=>array(2)));

			$coa_array[] = array('Coa.Company_id'=>array(1,$company_id));

			//$coa_array[] = array('IN'=>array('Coa.branch_id'=>array(1,$branch_id)));
		endif;

		$bank=$this->Bankaccount->find('list',array("fields" => array("bank_id","bank_name"),"conditions" => $bank_id, 'recursive'=>1));
		$this->set('bank',$bank);

		//get voucher no code start
		$current_month = date('Y-m');
		$voucher_no = $this->Journaltransaction->find('first', array('fields' => array("max(voucher_no) as voucher_no"),'conditions' => array('Journaltransaction.voucher_no LIKE' => '%'.$current_month.'%')
)); 
		$this->set('voucher_no',$voucher_no);
		//get voucher no code end
		
		$group=$this->Group->find(
			'list',
			array(
				"fields" => array("id","groupname"),
				"conditions" => $group_array
			)
		);
		$this->set('group',$group);

		$user=$this->User->find(
			'list',
			array(
				"fields" => array("id","user_fullname"),
				"conditions" => $user_array
			)
		);
		$this->set('user',$user);

		$bankaccount=$this->Bankaccount->find(
			'list',
			array(
				"fields" => array("id","bankaccount_name"),
				"conditions" => $bankaccount_id
			)
		);
		$this->set('bankaccount',$bankaccount);

		$bankchequebook=$this->Bankchequebook->find(
			'list',
			array(
				"fields" => array("id","bankchequebook_name"),
				"conditions" => $bankchequebook_id
			)
		);
		$this->set('bankchequebook',$bankchequebook);
		//get coa code
		$coa_code=$this->Coa->find(
			'list',
			array(
				"fields" => array("id","coacode"),
				"conditions" => $coa_array
			)
		);
		$this->set('coa_code',$coa_code);
		$coa=$this->Coa->find(
			'list',
			array(
				"fields" => array("id","coa_name"),
				"conditions" => $coa_array
			)
		);
		$this->set('coa',$coa);
		/*if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
			$banktransaction=$this->Banktransaction->find('all',array("fields" => "Banktransaction.*,User.*","conditions"=>array('Banktransaction.id'=>$id,'Banktransaction.banktransactionuuid'=>$uuid)));
			$this->set('banktransaction',$banktransaction);
			$this->set('id',$id);
		endif;*/
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
			$journaltransaction=$this->Journaltransaction->find('all',array("fields" => "Journaltransaction.*,User.*","conditions"=>array('Journaltransaction.journaltransaction_id'=>$id)));
			$total_transactions_by_this_id = $this->Journaltransaction->find('count', array('conditions' => array('Journaltransaction.journaltransaction_id' => $id))); 
			$this->set('journaltransaction',$journaltransaction);
			$this->set('id',$id);
			$this->set('total_transactions',$total_transactions_by_this_id);
		}
		//pr($journaltransaction);
	}

	public function gltransctionuserlistbygroup(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$GroupId = @$this->request->data['GroupId'];
		//$UserId = @$this->request->data['GltransactionUserId'];
		$transactionothernote = @$this->request->data['GltransactionGltransactionothernote'];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$json_array = array();
		$user_array = array();
		if($group_id==1):
		elseif($group_id==2):
			$user_array[] = array('NOT'=>array('User.group_id'=>1));
			$user_array[] = array('User.company_id'=>$company_id);
			$user_array[] = array('User.company_id'=>$company_id);
			$user_array[] = array('User.userisactive'=>1);
		else:
			$user_array[] = array('NOT'=>array('User.group_id'=>array(1, 2)));
			$user_array[] = array('User.company_id'=>$company_id);
			$user_array[] = array('User.userisactive'=>1);
		endif;
		$this->set('GroupId',$GroupId);
		$user=$this->Usergroup->find(
			'all',
			array(
				"fields" => array("user_id", "user_fullname"),
				"conditions"=>array('Usergroup.group_id'=>$GroupId))
		);
		//$this->set('user',$user);
		$user_option = "<option value=\"0\">".__("Select Member")."</option>";
		foreach ($user as $thisuser) {
			$user_option .= "<option value=\"{$thisuser["Usergroup"]["user_id"]}\" >{$thisuser["Usergroup"]["user_fullname"]}</option>";
		}
		$json_array["user_option"] = $user_option;
		$json_array["transactionothernote"] = $transactionothernote;
		return json_encode($json_array);
	}

	public function totalcashtransaction(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$json_array = array();
		$cash_array = array();
		if($group_id==1):
		elseif($group_id==2):
			$cash_array[] = array('Cashtransaction.company_id'=>$company_id);
		else:
			$cash_array[] = array('Cashtransaction.company_id'=>$company_id);
			$cash_array[] = array('Cashtransaction.branch_id'=>$branch_id);
		endif;
		$totalcashtransaction=$this->Cashtransaction->find(
			'first',
			array(
				"fields" => array("SUM(Cashtransaction.transactionamount) AS totalcashtransaction"),
				"conditions" => $cash_array
			)
		);
		$json_array["totalcashtransaction"] = $totalcashtransaction[0]["totalcashtransaction"];
		return json_encode($json_array);
	}
	public function gltransactioninsertupdateaction() {
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_ID=$_SESSION["User"]["id"];
		$journaltransactiontype_id = $_SESSION["ST_JOURNAL"];
		
		$total_transaction = $_POST["totalrow"];
		$id = @$this->request->data['Gls']['id'];
		$uuid = @$this->request->data['Gls']['gltransactionuuid'];
		$transaction_id = @$this->request->data['Gls']['id']?$this->request->data['Gls']['id']:0;
		$journaltransaction_id  = '';
		$transaction_insert_date= date('Y-m-d H:i:s');

		$transactiontype_id = $_SESSION['ST_JOURNAL'];
		$this->request->data['Journaltransaction']['journaltransactiontype_id']=$journaltransactiontype_id;
		$this->request->data['Journaltransaction']['transactiontype_id']=$transactiontype_id;
		$this->request->data['Journaltransaction']['transaction_id'] = 0;
		$this->request->data['Journaltransaction']['company_id'] = $company_id;
		$this->request->data['Journaltransaction']['branch_id'] = $branch_id;
		$salesman_id = @$this->request->data['Gltransaction']['salesman_id'] ? $this->request->data['Gltransaction']['salesman_id'] : 0;
		$this->request->data['Journaltransaction']['salesman_id'] = $salesman_id;
		$transactiondate = $this->request->data['Gltransaction']['transactiondate'];
		$this->request->data['Journaltransaction']['transactiondate'] = $transactiondate;
		if(isset($this->request->data['Gltransaction']['gltransactionothernote]'])){
			$journaltransactionothernote = 	$this->request->data['Gltransaction']['gltransactionothernote]'];
		} else {
			$journaltransactionothernote = 0;
		}
		$this->request->data['Journaltransaction']['journaltransactionothernote'] = $journaltransactionothernote;
		$this->request->data['Journaltransaction']['journaltransactionisactive'] = 1;
		$this->request->data['Journaltransaction']['description'] = $this->request->data['Gltransaction']['description'];
		$this->request->data['Journaltransaction']['voucher_no'] = $this->request->data['Gltransaction']['voucher_no'];
		if ($this->request->is('post')){
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
				$this->request->data['Journaltransaction']['journaltransactionupdateid'] = $user_ID;
				$this->request->data['Journaltransaction']['journaltransactionupdatedate'] = $transaction_insert_date;
				$message = __('Update Successfully.');
				$this->Journaltransaction->id = $id;
			} else {
				$this->request->data['Journaltransaction']['journaltransactioninsertid'] = $user_ID;
				$this->request->data['Journaltransaction']['journaltransactioninsertdate'] = $transaction_insert_date;
				$this->request->data['Journaltransaction']['journaltransactionuuid']=String::uuid();
				$message = __('Save Successfully.');
			}
			for($i=1;$i<=$total_transaction;$i++){
				if(@$this->request->data["Gltransaction"]["debit{$i}"]>0){
					$this->request->data["Journaltransaction"]["transactionamount"] = $this->request->data["Gltransaction"]["debit{$i}"];
					$transactionamount = $this->request->data["Gltransaction"]["debit{$i}"]; 				
				}elseif(@$this->request->data["Gltransaction"]["credit{$i}"]>0){
					$this->request->data["Journaltransaction"]["transactionamount"] = -$this->request->data["Gltransaction"]["credit{$i}"];
					$transactionamount = -$this->request->data["Gltransaction"]["credit{$i}"];				
				}

				$this->request->data["Journaltransaction"]["coa_id"] = $this->request->data["Gltransaction"]["coa_id{$i}"];
				$coa_id = $this->request->data["Journaltransaction"]["coa_id"];
				$this->request->data["Journaltransaction"]["group_id"] = $this->request->data["Gltransaction"]["group_id{$i}"];
				$this->request->data["Journaltransaction"]["user_id"]  = $this->request->data["Gltransaction"]["user_id{$i}"] ? $this->request->data["Gltransaction"]["user_id{$i}"] : 0;
				$this->request->data["Journaltransaction"]["particulars"] = $this->request->data["Gltransaction"]["particulars{$i}"];
				$this->request->data["Journaltransaction"]["cf_code"] = $this->request->data["Gltransaction"]["cf_code{$i}"];
				
				$this->Journaltransaction->create($this->request->data);
				if($this->Journaltransaction->save($this->request->data, array('validate' => 'only'))){
					$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
					$transaction_id = $this->Journaltransaction->getLastInsertId() ? $this->Journaltransaction->getLastInsertId() : $id;
					$clientsuppler_id = @$this->request->data["Gltransaction"]["user_id"]?$this->request->data["Gltransaction"]["user_id{$i}"]:0;
					if(empty($journaltransaction_id))
						$journaltransaction_id = $transaction_id;

					$this->Journaltransaction->query("update  journaltransactions
													  set     journaltransactions.journaltransaction_id = $journaltransaction_id
													  where   journaltransactions.journaltransactioninsertdate = '".$transaction_insert_date."'");
					$this->Gltransaction->deleteAll(array('Gltransaction.transaction_id' => $transaction_id, 'Gltransaction.transactiontype_id' => $transactiontype_id));
					$this->Transactions->usertransaction($transaction_id, $transactiontype_id, $transactionamount, $coa_id, $clientsuppler_id, $this->request->data["Journaltransaction"]["group_id"], $transactiondate, $isActive=1, 1);
					$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $transactionamount, $coa_id, $clientsuppler_id,$this->request->data["Journaltransaction"]["group_id"], $product_id=0, $transactiondate,$this->request->data['Journaltransaction']['voucher_no']='', $isActive=1, 1);

					
				} else {
					$errors = $this->Journaltransaction->invalidFields();
					$this->set('alert alert-danger', $this->Journaltransaction->invalidFields());
					$this->redirect("/gls/gltransactioninsertupdate");
				}
				//pr($this->request);
			}//for($i=0;$i<$total_transaction;$i++)
		//journal save and print code start
		  if($this->request->data["gltransactionsave_and_print"]==1){
		  	$this->redirect("/gls/journalvoucherprint/$journaltransaction_id");

		  }
		  //journal save and print code end
		  	
		  $this->redirect("/gls/gltransactionmanage");

		} else {
			$errors = $this->Journaltransaction->invalidFields();
			$this->set('alert alert-danger', $this->Journaltransaction->invalidFields());
			$this->redirect("/gls/gltransactioninsertupdate");
		}
	}

	//function for journal update
	function gltransactionupdateaction(){
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_ID=$_SESSION["User"]["id"];
		$journaltransactiontype_id = $_SESSION["ST_JOURNAL"];
		
		$total_transaction = $_POST["totalrow"];
		$id = @$this->request->data['Journaltransaction']['id'];
		$uuid = @$this->request->data['Gls']['gltransactionuuid'];
		$transaction_id = @$this->request->data['Gls']['id']?$this->request->data['Gls']['id']:0;
		$journaltransaction_id  = '';
		$transaction_update_date= date('Y-m-d H:i:s');
		$transactiontype_id = $_SESSION['ST_JOURNAL'];
		
		$this->request->data['Journaltransaction']['journaltransactiontype_id']=$journaltransactiontype_id;
		$this->request->data['Journaltransaction']['transactiontype_id']=$transactiontype_id;
		$this->request->data['Journaltransaction']['transaction_id'] = 0;
		$this->request->data['Journaltransaction']['company_id'] = $company_id;
		$this->request->data['Journaltransaction']['branch_id'] = $branch_id;
		$user_id = @$this->request->data['Gltransaction']['user_id'] ? $this->request->data['Gltransaction']['user_id'] : 0;
		$this->request->data['Journaltransaction']['user_id'] = $user_id;
		$salesman_id = @$this->request->data['Gltransaction']['salesman_id'] ? $this->request->data['Gltransaction']['salesman_id'] : 0;
		$this->request->data['Journaltransaction']['salesman_id'] = $salesman_id;
		$transactiondate = $this->request->data['Gltransaction']['transactiondate'];
		$this->request->data['Journaltransaction']['transactiondate'] = $transactiondate;

		$count =1;

		if(isset($this->request->data['Gltransaction']['gltransactionothernote]'])){
			$journaltransactionothernote = 	$this->request->data['Gltransaction']['gltransactionothernote]'];
		} else {
			$journaltransactionothernote = 0;
		}
		$this->request->data['Journaltransaction']['journaltransactionothernote'] = $journaltransactionothernote;
		$this->request->data['Journaltransaction']['journaltransactionisactive'] = 1;
		$this->request->data['Journaltransaction']['group_id'] = $this->request->data['Gltransaction']['group_id'];
		
		$product_id = @$this->request->data['Gltransaction']['product_id']?$this->request->data['Gltransaction']['product_id']:0;
		if ($this->request->is('post')){
			$this->request->data['Journaltransaction']['journaltransactionupdateid'] = $user_ID;
			$this->request->data['Journaltransaction']['journaltransactionupdatedate'] = $transaction_update_date;
			$message = __('Update Successfully.');

			$total_transactions_for_update = $this->Journaltransaction->find('count', array('conditions' => array('Journaltransaction.journaltransaction_id' => $id))); 
			$total_inserted_transactions = $total_transaction - $total_transactions_for_update;

			$journaltransaction_ids=$this->Journaltransaction->find('all',array("fields" => "Journaltransaction.id","conditions"=>array('Journaltransaction.journaltransaction_id'=>$id)));
			$this->Journaltransaction->create($this->request->data);
			
			foreach ($journaltransaction_ids as $key => $journaltransaction_id) {
				$this->request->data["Journaltransaction"]["coa_id"] = $this->request->data["Gltransaction"]["coa_id{$count}"];
				$coa_id = $this->request->data["Journaltransaction"]["coa_id"];
				
				if(@$this->request->data["Gltransaction"]["debit{$count}"]>0){
					$this->request->data["Journaltransaction"]["transactionamount"] = $this->request->data["Gltransaction"]["debit{$count}"];
					$transactionamount = $this->request->data["Gltransaction"]["debit{$count}"]; 				
				}elseif(@$this->request->data["Gltransaction"]["credit{$count}"]>0){
					$this->request->data["Journaltransaction"]["transactionamount"] = -$this->request->data["Gltransaction"]["credit{$count}"];
					$transactionamount = -$this->request->data["Gltransaction"]["credit{$count}"];				
				}

				$journaltransaction_id = $journaltransaction_id['Journaltransaction']['id'];
				$data = array('journaltransactiontype_id' => $journaltransactiontype_id,
							  'transactiontype_id' => $transactiontype_id,
							  'transaction_id' => $transaction_id,
							  'transactiondate' => $transactiondate,
							  'coa_id' => $coa_id,
							  'transactionamount' => $transactionamount,
							  'journaltransactionisactive' => 1
							  );
				//$this->Journaltransaction->create($this->request->data);
				//if($this->Journaltransaction->save($this->request->data, array('validate' => 'only'))){
				//if($this->Journaltransaction->updateAll($data,array('Journaltransaction.id' => $this->Journaltransaction->id))){
					$this->Journaltransaction->query("update journaltransactions set journaltransactiontype_id='".$journaltransactiontype_id."',
												      transactiontype_id = '".$transactiontype_id."',transaction_id='".$transaction_id."',
												      transactiondate='".$transactiondate."',coa_id='".$coa_id."',
												      transactionamount = '".$transactionamount."', journaltransactionisactive =1,
												      company_id = '".$company_id."', user_id  = '".$this->request->data['Gltransaction']['user_id'] ."',
												      branch_id = '".$branch_id."', group_id = '".$this->request->data['Gltransaction']['group_id']."'
													  where journaltransactions.id = '".$journaltransaction_id."'");
					
					
					$clientsuppler_id = @$this->request->data['Gltransaction']['user_id']?$this->request->data['Gltransaction']['user_id']:0;
					$clientsupplergroup_id = $this->request->data['Gltransaction']['group_id'];
					//$this->Gltransaction->deleteAll(array('Gltransaction.transaction_id' => $transaction_id, 'Gltransaction.transactiontype_id' => $transactiontype_id));
					//$this->Transactions->usertransaction($transaction_id, $transactiontype_id, $transactionamount, $coa_id, $clientsuppler_id, $clientsupplergroup_id, $transactiondate, $isActive=1, 1);
					//$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $transactionamount, $coa_id, $clientsuppler_id, $product_id=0, $transactiondate, $isActive=1, 1);

				/*}else{
					$errors = $this->Journaltransaction->invalidFields();
					$this->set('alert alert-danger', $this->Journaltransaction->invalidFields());
					$this->redirect("/gls/gltransactioninsertupdate");

			    }*/
			    $count++;

			}//foreach ($journaltransaction_ids as $key => $journaltransaction_id)
			
			if(!empty($total_inserted_transactions)){
				$transaction_insert_date = date('Y-m-d H:i:s');
				$this->request->data['Journaltransaction']['journaltransactioninsertid'] = $user_ID;
				$this->request->data['Journaltransaction']['journaltransactioninsertdate'] = date('Y-m-d H:i:s');
				$this->request->data['Journaltransaction']['journaltransactionuuid']=String::uuid();
				
				for($i=$total_inserted_transactions+1; $i<=$total_transaction;$i++){
					if(@$this->request->data["Gltransaction"]["debit{$i}"]>0){
						$this->request->data["Journaltransaction"]["transactionamount"] = $this->request->data["Gltransaction"]["debit{$i}"];
						$transactionamount = $this->request->data["Gltransaction"]["debit{$i}"]; 				
					}elseif(@$this->request->data["Gltransaction"]["credit{$i}"]>0){
						$this->request->data["Journaltransaction"]["transactionamount"] = -$this->request->data["Gltransaction"]["credit{$i}"];
						$transactionamount = -$this->request->data["Gltransaction"]["credit{$i}"];				
					}
					
					$this->request->data["Journaltransaction"]["coa_id"] = $this->request->data["Gltransaction"]["coa_id{$i}"];
					$coa_id = $this->request->data["Journaltransaction"]["coa_id"];

					$this->Journaltransaction->create($this->request->data);
					if($this->Journaltransaction->save($this->request->data, array('validate' => 'only'))){
						$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
						$transaction_id = $this->Journaltransaction->getLastInsertId() ? $this->Journaltransaction->getLastInsertId() : $id;

						$clientsuppler_id = @$this->request->data['Gltransaction']['user_id']?$this->request->data['Gltransaction']['user_id']:0;
						$clientsupplergroup_id = $this->request->data['Gltransaction']['group_id'];
						
						if(empty($journaltransaction_id))
							$journaltransaction_id = $transaction_id;

						//$this->Journaltransaction->id = $id;
						//$this->Journaltransaction->save($this->request->data, array('validate' => 'only'));
						$this->Journaltransaction->query("update  journaltransactions 
														  set     journaltransactions.journaltransaction_id = $journaltransaction_id
														  where   journaltransactions.journaltransactioninsertdate = '".$transaction_insert_date."'");
						//$banktransactiondate = $this->request->data['Gltransaction']['transactiondate'];
						$this->Gltransaction->deleteAll(array('Gltransaction.transaction_id' => $transaction_id, 'Gltransaction.transactiontype_id' => $transactiontype_id));
						$this->Transactions->usertransaction($transaction_id, $transactiontype_id, $transactionamount, $coa_id, $clientsuppler_id, $clientsupplergroup_id, $transactiondate, $isActive=1, 1);
						$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $transactionamount, $coa_id, $clientsuppler_id, $product_id=0, $transactiondate, $isActive=1, 1);
				
				}else {
					$errors = $this->Journaltransaction->invalidFields();
					$this->set('alert alert-danger', $this->Journaltransaction->invalidFields());
					$this->redirect("/gls/gltransactioninsertupdate");

				}
			  }
		    }
		    $this->redirect("/gls/gltransactionmanage");
		}

	}

    //function for journal voucher print
	function journalvoucherprint($id){
		$voucher_data = $this->Journaltransaction->find('all',array("fields" => "Journaltransaction.transactionamount,
								 Journaltransaction.voucher_no,Journaltransaction.transactiondate,Journaltransaction.description,Coa.coaname,Coa.coacode","conditions"=>array('Journaltransaction.journaltransaction_id'=>$id,
								 'Journaltransaction.coa_id=Coa.id')));
		$this->set('voucher_data',$voucher_data);
		

	}
	//End Gl//
}
?>
