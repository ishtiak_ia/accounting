<?php
App::uses('AppModel', 'Model');
class Cashtransaction extends AppModel {
	public $name = 'Cashtransaction';
	public $usetables = 'cashtransactions';
	var $belongsTo  = array(
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'cashtransactioninsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'cashtransactionupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'cashtransactiondeleteid'
		),
		'Company' => array(
			'fields' =>array('companyname', 'companynamebn'),
			'className'    => 'Company',
			'foreignKey'    => 'company_id'
		),
		'Branch' => array(
			'fields' =>array('branchname', 'branchnamebn'),
			'className'    => 'Branch',
			'foreignKey'    => 'branch_id'
		),
		'User' => array(
			'fields' =>array('User.*'),
			'className'    => 'User',
			'foreignKey'    => 'user_id'
		),
		'Coa' => array(
			'fields' =>array('Coa.*'),
			'className'    => 'Coa',
			'foreignKey'    => 'coa_id'
		)
	);
	var $virtualFields = array(
		'cashtransaction_date' => 'DATE(Cashtransaction.transactiondate)',
		'cashtransaction_particulars' => 'CONCAT(
			IF(Cashtransaction.transactiontype_id=0, "", IF(Cashtransaction.transactiontype_id=4,"Withdraw","Deposits")), 
			IF(Cashtransaction.user_id=0, CONCAT("/", Cashtransaction.cashtransactionothernote), CONCAT("/", User.userfirstname, " ", User.usermiddlename, " ", User.userlastname)), 
			IF(Cashtransaction.coa_id=0, " ", CONCAT("/", Coa.coaname))
		)',
		'cashtransaction_balance' => 'IF(Cashtransaction.transactionamount=0, "0.00", ROUND(Cashtransaction.transactionamount, 2))',
		'cashtransaction_withdraw' => 'IF(Cashtransaction.transactionamount<0,ROUND(Cashtransaction.transactionamount, 2), "0.00")',
		'cashtransaction_deposit' => 'IF(Cashtransaction.transactionamount>0,ROUND(Cashtransaction.transactionamount, 2), "0.00")',
		'isActive' => 'IF(Cashtransaction.cashtransactionisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Cashtransaction.cashtransactionisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
}

?>	