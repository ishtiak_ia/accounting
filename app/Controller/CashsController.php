<?php
App::import('Controller', 'Logins');
App::import('Controller', 'Transactions');
App::import('Controller', 'Users');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');

class CashsController extends AppController {
	public $name = 'Cashs';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'JqueryValidation');
	public $components = array('RequestHandler','Session');
	public $uses = array('Cashtransaction', 'Company', 'Group', 'User','Usertransaction');

	var $Transactions;
	var $Users;

	var $Logins;
	/*public function index() {
		$this->redirect("login");
	}*/
	function beforeFilter(){
		$this->Logins =& new LoginsController;
		$this->Logins->constructClasses();
		$this->Logins->__validateLoginStatus();
		//$Logins = new LoginsController;
		//$this->set('Logins',$Logins);
		$this->Transactions =& new TransactionsController;
		$this->Transactions->constructClasses();
		$this->Users =& new UsersController;
		$this->Users->constructClasses();


		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	public  function beforeRender(){
		$this->response->disableCache();//prevent useless warnings for Ajax
		if($this->RequestHandler->isAjax()){
			Configure::write('debug', 0);
		}
	}

	//Start Cash//
	public function cashtransactionmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Cash Transaction Manage').' | '.__(Configure::read('site_name')));
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];
		$condition = array();
		
		if($group_id==1):
		elseif($group_id==2):
			$condition[] = array('Cashtransaction.company_id'=>$company_id);
		else:
			$condition[] = array('Cashtransaction.company_id'=>$company_id);
			$condition[] = array('Cashtransaction.branch_id'=>$branch_id);
		endif;
		$this->paginate = array(
			'fields' => 'Cashtransaction.*',
			'conditions'=> $condition ,
			'limit' => 20
		);
		$cashtransaction = $this->paginate('Cashtransaction');
		$this->set('cashtransaction',$cashtransaction);
	}

	public function cashtransactioninsertupdate($id=null,$uuid=null) {
		$this->layout='default';
		$this->set('title_for_layout', __('Cash Transaction').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$group_array = array();
		$user_array = array();
		$cash_array = array();
		if($group_id==1):
		elseif($group_id==2):
			$group_array[] = array('NOT'=>array('Group.id'=>1));
			$user_array[] = array('NOT'=>array('User.group_id'=>1));
			$user_array[] = array('User.company_id'=>$company_id);
			$cash_array[] = array('Cashtransaction.company_id'=>$company_id);
		else:
			$group_array[] = array('NOT'=>array('Group.id'=>array(1, 2)));
			$user_array[] = array('NOT'=>array('User.group_id'=>array(1, 2)));
			$user_array[] = array('User.company_id'=>$company_id);
			$user_array[] = array('User.branch_id'=>$branch_id);
			$cash_array[] = array('Cashtransaction.company_id'=>$company_id);
			$cash_array[] = array('Cashtransaction.branch_id'=>$branch_id);
		endif;
		$group=$this->Group->find(
			'list',
			array(
				"fields" => array("id","groupname"),
				"conditions" => $group_array
			)
		);
		$this->set('group',$group);

		$user=$this->User->find(
			'list',
			array(
				"fields" => array("id","user_fullname"),
				"conditions" => $user_array
			)
		);
		$this->set('user',$user);

		$totalcashtransaction=$this->Cashtransaction->find(
			'first',
			array(
				"fields" => array("SUM(Cashtransaction.transactionamount) AS totalcashtransaction"),
				"conditions" => $cash_array
			)
		);
		$this->set('totalcashtransaction',$totalcashtransaction);
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
			$cashtransaction=$this->Cashtransaction->find('all',array("fields" => "Cashtransaction.*,User.*","conditions"=>array('Cashtransaction.id'=>$id,'Cashtransaction.cashtransactionuuid'=>$uuid)));
			$this->set('cashtransaction',$cashtransaction);
			$this->set('id',$id);
		endif;
	}
	public function cashtransactioninsertupdateaction() {
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$id = @$this->request->data['Cashs']['id'];
		$uuid = @$this->request->data['Cashs']['cashtransactionuuid'];
		$transaction_id = @$this->request->data['Cashs']['id']?$this->request->data['Cashs']['id']:0;
		$cashtransactiontype_id = $this->request->data['Cashtransaction']['cashtransactiontype_id'];
		$transactiontype_id = $this->request->data['Cashtransaction']['transactiontype_id'] = $this->request->data['Cashtransaction']['cashtransactiontype_id'];
		if($cashtransactiontype_id == $_SESSION["ST_CASHPAYMENT"]):
			$transactionamount = -$this->request->data['Cashtransaction']['transactionamount'];
		else:
			$transactionamount = $this->request->data['Cashtransaction']['transactionamount'];
		endif;
		$transactiondate = $this->request->data['Cashtransaction']['transactiondate'];
		$cashtransactionothernote = $this->request->data['Cashtransaction']['cashtransactionothernote'] = @$this->request->data['Cashtransaction']['cashtransactionothernote']?$this->request->data['Cashtransaction']['cashtransactionothernote']:'';

		$this->request->data['Cashtransaction']['company_id'] = $company_id;
		$this->request->data['Cashtransaction']['branch_id'] = $branch_id;
		$coa_id =0;
		$user_id =  $this->request->data['Cashtransaction']['user_id'] = @$this->request->data['Banktransaction']['user_id']?$this->request->data['Banktransaction']['user_id']:0;
		$salesman_id = @$this->request->data['Cashtransaction']['salesman_id']?$this->request->data['Cashtransaction']['salesman_id']:0;
		$product_id = $this->request->data['Cashtransaction']['product_id'] = @$this->request->data['Cashtransaction']['product_id']?$this->request->data['Cashtransaction']['product_id']:0;
		$this->request->data['Cashtransaction']['cashtransactionisactive'] = 1;
		if ($this->request->is('post')):
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
				$this->request->data['Cashtransaction']['cashtransactionupdateid']=$user_id;
				$this->request->data['Banktransaction']['cashtransactionupdatedate']=date('Y-m-d H:i:s');
				$message = __('Update Successfully.');
				$this->Cashtransaction->id = $id;
			else:
				$this->request->data['Cashtransaction']['cashtransactioninsertid']=$user_id;
				$this->request->data['Cashtransaction']['cashtransactioninsertdate']=date('Y-m-d H:i:s');
				$this->request->data['Cashtransaction']['cashtransactionuuid']=String::uuid();
				$message = __('Save Successfully.');
			endif;
			if ($this->Cashtransaction->save($this->request->data, array('validate' => 'only'))):
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				$transaction_id = $this->Cashtransaction->getLastInsertId()?$this->Cashtransaction->getLastInsertId():$id;
				$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $transactionamount, 2, $user_id, $product_id=0, $transactiondate, $isActive=1, 0);

				$clientsupplergroup_id = $this->request->data['Cashtransaction']['group_id'];
				if($clientsupplergroup_id==4):
					$coa_id=3;
				else:
					$coa_id=5;
				endif;

				$this->Transactions->usertransaction($transaction_id, $transactiontype_id, (0+$transactionamount), $coa_id, $user_id, $clientsupplergroup_id, $transactiondate, $isActive=1, $step=1);
				$this->Transactions->gltransaction($transaction_id, $transactiontype_id, (0+$transactionamount), $coa_id, $user_id, $product_id=0, $transactiondate, $isActive=1, 1);
				$this->redirect("/cashs/cashtransactionmanage");
			else:
				$errors = $this->Cashtransaction->invalidFields();
				$this->set('alert alert-danger', $this->Cashtransaction->invalidFields());
				$this->redirect("/cashs/cashtransactioninsertupdate");
			endif; 
			//pr($this->request);
		else:
			$errors = $this->Cashtransaction->invalidFields();
			$this->set('alert alert-danger', $this->Banktransaction->invalidFields());
			$this->redirect("/cashs/cashtransactioninsertupdate");

		endif;
	}

	//End Cash//
}
?>