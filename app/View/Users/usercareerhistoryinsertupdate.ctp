<?php
echo $this->Form->create('Users', array('action' => 'usercareerhistoryinsertupdateaction', 'type' => 'file'));
	echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$usercareerhistory[0]["Usercareerhistory"]['id']));
	echo $this->Form->input('usercareerhistoryuuid', array('type'=>'hidden', 'value'=>@$usercareerhistory[0]["Usercareerhistory"]['usercareerhistoryuuid']?$usercareerhistory[0]["Usercareerhistory"]['usercareerhistoryuuid']:0, 'class'=>''));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'tabindex'=>10,
			'value' => (@$usercareerhistory[0]['Usercareerhistory']['usercareerhistoryisactive'] ? $usercareerhistory[0]['Usercareerhistory']['usercareerhistoryisactive'] : 0),
			'class' => 'form-inline'
		);
	echo"<h3>".__("Add Career History")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-6\">";
			echo $this->Form->input(
				"Usercareerhistory.department_id",
				array(
					'type'=>'select',
					"options"=>array($department_list),
					'empty'=>__('Select Department'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>1,
					'label'=>__('Select Department&nbsp;<span style=\'color:#cc0000\'>*</span>'),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style'=>'',
					"selected" => (@$usercareerhistory[0]['Usercareerhistory']['department_id'] ? $usercareerhistory[0]['Usercareerhistory']['department_id'] : 0)
				)
			);
			echo "<div class=\"form-group\" id=\"usercareerhistorychangediv\">";
			echo $this->Form->input(
				"Usercareerhistory.user_id",
				array(
					'type'=>'select',
					"options"=>array($user_list),
					'empty'=>__('Select Employee'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>1,
					'label'=>__('Select Employee&nbsp;<span style=\'color:#cc0000\'>*</span>'),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style'=>'',
					"selected" => (@$usercareerhistory[0]['Usercareerhistory']['user_id'] ? $usercareerhistory[0]['Usercareerhistory']['user_id'] : 0)
				)
			);
			echo "</div>";
			echo"<div class=\"row\" id=\"content\">";
				echo"<div class=\"col-md-11\" id=\"mydivcontent1\">";
					echo"<div class=\"row\">";
						echo"<div class=\"col-md-11\">";
							echo $this->Form->input(
								"usercareerhistorycompamyname1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label' => __('Compamy Name&nbsp;<span style=\'color:#cc0000\'>*</span>'),
									'tabindex'=>2,
									'placeholder' => __("Compamy Name"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$usercareerhistory[0]['Usercareerhistory']['usercareerhistorycompamyname']
								)
							);
							echo $this->Form->input(
								"usercareerhistorycompanytype1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label' => __('Company Type&nbsp;<span style=\'color:#cc0000\'>*</span>'),
									'tabindex'=>3,
									'placeholder' => __("Company Type"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$usercareerhistory[0]['Usercareerhistory']['usercareerhistorycompanytype']
								)
							);
				            //echo "<div class=\"form-group\" id=\"relationchangediv\">";

				            //echo "</div>";
							echo $this->Form->input("usercareerhistorylocation1", array(
					            	'type' => 'text', 
					            	'div' => array('class'=>'form-group'), 
					            	'label'=> __('Company Location&nbsp;<span style=\'color:#cc0000\'>*</span>'), 
					            	'tabindex'=>4,
					            	'style' => '',
					            	'placeholder' => __("Company Location"),
					            	'data-validation-engine'=>'validate[required]',
					            	'value' =>(@$usercareerhistory[0]['Usercareerhistory']['usercareerhistorylocation']),
									'class'=> 'form-control'
					        	)
					        );
							echo $this->Form->input(
								"usercareerhistorypositionheld1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label' => __('Position Held&nbsp;<span style=\'color:#cc0000\'>*</span>'),
									'tabindex'=>5,
									'placeholder' => __("Position Held"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$usercareerhistory[0]['Usercareerhistory']['usercareerhistorypositionheld']
								)
							);
							echo $this->Form->input(
								"usercareerhistorydepartment1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label' => __('Department'),
									'tabindex'=>6,
									'placeholder' => __("Department"),
									'class'=> 'form-control',
									'style' => '',
									'value' => @$usercareerhistory[0]['Usercareerhistory']['usercareerhistorydepartment']
								)
							);
							echo $this->Form->input(
								"usercareerhistoryresponsibilities1",
								array(
									'type' => 'textarea',
									'div' => array('class'=> 'form-group'),
									'label' => __('Responsibilities&nbsp;<span style=\'color:#cc0000\'>*</span>'),
									'tabindex'=>7,
									'placeholder' => __("Responsibilities"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$usercareerhistory[0]['Usercareerhistory']['usercareerhistoryresponsibilities']
								)
							);
							echo $this->Form->input(
								"usercareerhistoryfromdate1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label' => __('From&nbsp;<span style=\'color:#cc0000\'>*</span>'),
									'tabindex'=>8,
									'placeholder' => __("From Date, Format(yyyy-mm-dd)"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$usercareerhistory[0]['Usercareerhistory']['usercareerhistoryfromdate'],
									'onclick'=>'usercareerhistorydatetimepicker(1, "UsersUsercareerhistoryfromdate");'
								)
							);
							echo $this->Form->input(
								"usercareerhistorytodate1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label' => __('To'),
									'tabindex'=>9,
									'placeholder' => __("To Date, Format(yyyy-mm-dd)"),
									'class'=> 'form-control',
									'style' => '',
									'value' => @$usercareerhistory[0]['Usercareerhistory']['usercareerhistorytodate'],
									'onclick'=>'usercareerhistorydatetimepicker(1, "UsersUsercareerhistorytodate");'
								)
							);
							echo"<div class=\"form-group\">";
								echo"<label>isActive?</label><br>";
								echo $this->Form->radio(
									'usercareerhistoryisactive1', 
									$options,
									$attributes
								);
							echo"</div>";
							echo"</div>";	
						echo"</div>";								
					echo"</div>";		
				echo"</div>";
				echo"<div class=\"row\">";
					echo"<div class=\"col-md-11\">";
						echo"<input type=\"button\" id=\"more_fields\" onclick=\"add_fields();\" value=\"Add More\" />";
						echo"<input type=\"button\" id=\"remove_fields\" onclick=\"remove_field();\" value=\"Remove More\" />";
						echo"<input type=\"hidden\" style=\"width:48px;\" name=\"totlarow\" id=\"totlarow\" value=\"1\" /> ";
						echo"
							<div id=\"locationsarchloading\">

							</div>
						";
						echo "<br />";
			echo $Utilitys->allformbutton('',$this->request["controller"],$page='usercareerhistory');
				echo"</div>";
			echo"</div>";
		echo"</div>";
	echo"</div>";
echo $this->Form->end();
echo"
<script>
		var startDate = new Date('01/01/1980');
		var FromEndDate = new Date();
		var ToEndDate = new Date();
		ToEndDate.setDate(ToEndDate.getDate()+365);
		function usercareerhistorydatetimepicker(fieldID, fieldName){
			$('#'+fieldName+fieldID).datetimepicker({
				format: \"yyyy-mm-dd\",
				language: 'en',
				weekStart: 1,
				todayBtn: 1,
				autoclose: 1,
				todayHighlight: 1,
				startView: 2,
				minView: 2,
				forceParse: 0,
				startDate: '01/01/2012',
				endDate: FromEndDate,
				use24hours: false
			}).on('changeDate', function(selected){
				startDate = new Date(selected.date.valueOf());
				startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
				$('#'+fieldName+fieldID).datetimepicker('setStartDate', startDate);
				TimeDiff(fieldID);
			});
		}
			var room=0;
			function add_fields() {
			room = parseInt(document.getElementById(\"totlarow\").value);		
			room = room+1;
					
			$(\"#content\").append('<div id=\"mydivcontent'+room+'\" class=\"col-md-11\"><div class=\"row\"><div class=\"col-md-11\"><div class=\"form-group\"><label for=\"UsersUsercareerhistorycompamyname'+room+'\">Compamy Name</label><input type=\"text\" id=\"UsersUsercareerhistorycompamyname'+room+'\" style=\"\" data-validation-engine=\"validate[required]\" class=\"form-control\" placeholder=\"Compamy Name\" tabindex=\"2\" name=\"data[Users][usercareerhistorycompamyname'+room+']\"></div><div class=\"form-group\"><label for=\"UsersUsercareerhistorycompanytype'+room+'\">Company Type</label><input type=\"text\" id=\"UsersUsercareerhistorycompanytype'+room+'\" style=\"\" data-validation-engine=\"validate[required]\" class=\"form-control\" placeholder=\"Company Type\" tabindex=\"3\" name=\"data[Users][usercareerhistorycompanytype'+room+']\"></div><div class=\"form-group\"><label for=\"UsersUsercareerhistorylocation'+room+'\">Company Location</label><input type=\"text\" id=\"UsersUsercareerhistorylocation'+room+'\" class=\"form-control\" data-validation-engine=\"validate[required]\" placeholder=\"Company Location\" style=\"\" tabindex=\"4\" name=\"data[Users][usercareerhistorylocation'+room+']\"></div><div class=\"form-group\"><label for=\"UsersUsercareerhistorypositionheld'+room+'\">Position Held</label><input type=\"text\" id=\"UsersUsercareerhistorypositionheld'+room+'\" style=\"\" data-validation-engine=\"validate[required]\" class=\"form-control\" placeholder=\"Position Held\" tabindex=\"5\" name=\"data[Users][usercareerhistorypositionheld'+room+']\"></div><div class=\"form-group\"><label for=\"UsersUsercareerhistorydepartment'+room+'\">Department</label><input type=\"text\" id=\"UsersUsercareerhistorydepartment'+room+'\" style=\"\" class=\"form-control\" placeholder=\"Department\" tabindex=\"6\" name=\"data[Users][usercareerhistorydepartment'+room+']\"></div><div class=\"form-group\"><label for=\"UsersUsercareerhistoryresponsibilities'+room+'\">Responsibilities</label><textarea id=\"UsersUsercareerhistoryresponsibilities'+room+'\" rows=\"6\" cols=\"30\" style=\"\" data-validation-engine=\"validate[required]\" class=\"form-control\" placeholder=\"Responsibilities\" tabindex=\"7\" name=\"data[Users][usercareerhistoryresponsibilities'+room+']\"></textarea></div><div class=\"form-group\"><label for=\"UsersUsercareerhistoryfromdate'+room+'\">From</label><input type=\"text\" id=\"UsersUsercareerhistoryfromdate'+room+'\" style=\"\" data-validation-engine=\"validate[required]\" class=\"form-control\" placeholder=\"From Date, Format(yyyy-mm-dd)\" tabindex=\"8\" name=\"data[Users][usercareerhistoryfromdate'+room+']\"></div><div class=\"form-group\"><label for=\"UsersUsercareerhistorytodate'+room+'\">To</label><input type=\"text\" id=\"UsersUsercareerhistorytodate'+room+'\" style=\"\" class=\"form-control\" placeholder=\"To Date, Format(yyyy-mm-dd)\" tabindex=\"9\" name=\"data[Users][usercareerhistorytodate'+room+']\"></div><div class=\"form-group\"><label>isActive?</label><br><input type=\"radio\" class=\"form-inline\" value=\"1\" tabindex=\"10\" id=\"UsersUsercareerhistoryisactive'+room+'\" name=\"data[Users][usercareerhistoryisactive'+room+']\"><label for=\"UsersUsercareerhistoryisactive'+room+'\">Yes</label><input type=\"radio\" checked=\"checked\" class=\"form-inline\" value=\"0\" tabindex=\"10\" id=\"UsersUsercareerhistoryisactive'+room+'\" name=\"data[Users][usercareerhistoryisactive'+room+']\"><label for=\"UsersUsercareerhistoryisactive'+room+'\">No</label></div></div></div></div>');
			document.getElementById(\"totlarow\").value=room;
			usercareerhistorydatetimepicker(room, 'UsersUsercareerhistoryfromdate');
			usercareerhistorydatetimepicker(room, 'UsersUsercareerhistorytodate');

		}
		function remove_field(){  
			room = document.getElementById(\"totlarow\").value;
			var child = document.getElementById('mydivcontent'+room);
			var parent = document.getElementById('content');
			parent.removeChild(child);
			room = room-1;
			document.getElementById(\"totlarow\").value=room;
		}
			$(document).ready(function() {
			$(\"#UsersUsercareerhistoryinsertupdateactionForm\").validationEngine();
			usercareerhistorydatetimepicker(1, 'UsersUsercareerhistoryfromdate');
			usercareerhistorydatetimepicker(1, 'UsersUsercareerhistorytodate');
				$(\"#UsersUsercareerhistoryfromdate+room+\").datetimepicker({format: \"yyyy-mm-dd\", showMeridian: false , startView: 2, minView: 2, autoclose: 1
				});
				$(\"#UsersUsercareerhistorytodate+room+\").datetimepicker({format: \"yyyy-mm-dd\", showMeridian: false , startView: 2, minView: 2, autoclose: 1
				});
			});
				$(\"#UsercareerhistoryDepartmentId\").change(function(){
					var UsercareerhistoryDepartmentId = $(this).val();
					$(\"#usercareerhistorychangediv\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usercareerlistbydepartmentid',
						{UsercareerhistoryDepartmentId:UsercareerhistoryDepartmentId},
						function(result) {
							$(\"#usercareerhistorychangediv\").html(result);
						}
					);
				});
		</script>
	";
?>