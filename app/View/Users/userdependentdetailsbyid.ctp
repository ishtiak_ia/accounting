<?php
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"2\">
				<h2>".__('Dependent Information')."</h2>
			</td>
		</tr>
			<tr>
				<th>
					".__('Employe Name')."
				</th>
				<td>
					".$userdependent[0]["Userdependent"]["user_fullname"]."
				</td>
			</tr>
			<tr>
				<th>
					".__("Dependent Name")."
				</th>
				<td>
					".$userdependent[0]["Userdependent"]["userdependentname"]."
				</td>
			</tr>
			<tr>
				<th>
					".__("Dependent Name BN")."
				</th>
				<td>
					".$userdependent[0]["Userdependent"]["userdependentnamebn"]."
				</td>
			</tr>
			<tr>
				<th>
					".__("Dependent Relationship")."
				</th>
				<td>
					".$userdependent[0]["Userdependent"]["userrelationship_name"]."
				</td>
			</tr>
			<tr>
				<th>
					".__("Dependent Relationship (Other)")."
				</th>
				<td>
					".$userdependent[0]["Userdependent"]["userdependentrelationship"]."
				</td>
			</tr>
			<tr>
				<th>
					".__("Dependent Date of Birth")."
				</th>
				<td>
					".$userdependent[0]["Userdependent"]["userdependentdob"]."
				</td>
			</tr>
			<tr>
				<th>
					".__("Active")."
				</th>
				<td>
					".$userdependent[0]["Userdependent"]["isActive"]."
				</td>
			</tr>
	";
	echo $Utilitys->tableend();
?>