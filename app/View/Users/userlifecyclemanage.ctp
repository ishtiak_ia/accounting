<?php
	//echo "<h2>".__('Payroll Time'). $Utilitys->addurl($title=__("Add New Department Time"), $this->request['controller'], $page="departmenttime")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"User.department_id",
			array(
				'type'=>'select',
				"options"=>array($departmentname),
				'empty'=>__('Select Department'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
			)
		)		
		.$this->Form->input(
			"User.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => ''
			)
		);
$searchfield="";
echo"
        <div class=\"btn-group\">
            <button class=\"btn btn-warning btn-sm dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-bars\"></i> Export Table Data</button>
            <ul class=\"dropdown-menu \" role=\"menu\">
                <li><a href=\"#\" onClick =\"$('#userlifecyclesearchloading').tableExport({type:'png',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> <img src='".$this->webroot."js/icons/png.png' width='24px'> PNG</a></li>
                <li><a href=\"#\" onClick =\"$('#userlifecyclesearchloading').tableExport({type:'pdf',pdfFontSize:'7',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> <img src='".$this->webroot."js/icons/pdf.png' width='24px'> PDF</a></li>
            </ul>
        </div>  
    ";
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"userlifecyclesearchloading\">
	";
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('EMP. ID')  => array('class' => 'highlight sortable')
					),
					array(
						__('User')  => array('class' => 'highlight sortable')
					),
					array(
						__('Department Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Employee Lifecycle')  => array('class' => 'highlight sortable')
					)
				)
			);
			$userlifecyclerows = array();
			$countUserlifecyclerows = 0-1+$this->Paginator->counter('%start%');
			foreach($userlifecycle as $Thisuser):
				$countUserlifecyclerows++;
				$userlifecyclerows[]= array($countUserlifecyclerows,
				$Thisuser["User"]["userpunchmachineid"],
				$Thisuser["User"]["user_fullname"],
				$Thisuser["User"]["department_name"],
				$Utilitys->userlifecycleView(
				           'View', 
				           $Thisuser["User"]["id"],
				           $Thisuser["User"]["useruuid"],
				          'User')
				/*
				$Utilitys->paysleepView(
				           'View', 
				           $Thisuser["User"]["id"],
				           $Thisuser["User"]["useruuid"],
				           'User')
				*/
				);
			endforeach;
			echo $this->Html->tableCells(
				$userlifecyclerows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();

	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				$(\"#UserSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#userlifecyclesearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/userlifecyclesearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#userlifecyclesearchloading\").html(result);
						}
					);
				});
				$(\"#UserDepartmentId\").change(function(){
					var UserDepartmentId = $(\"#UserDepartmentId\").val();
					$(\"#userlifecyclesearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/userlifecyclesearchbytext',
						{UserDepartmentId:UserDepartmentId},
						function(result) {
							$(\"#userlifecyclesearchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>
