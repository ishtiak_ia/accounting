<?php
App::uses('AppModel', 'Model');
class Gltransaction extends AppModel {
	public $name = 'Gltransaction';
	public $usetables = 'gltransactions';
	var $belongsTo  = array(
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'gltransactioninsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'gltransactionupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'gltransactiondeleteid'
		),
		'Transactiontype' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'Transactiontype',
			'foreignKey'    => 'transactiontype_id'
		),
		'User' => array(
			'fields' =>array('User.*'),
			'className'    => 'User',
			'foreignKey'    => 'user_id'
		),
		'Coa' => array(
			'fields' =>array('Coa.*'),
			'className'    => 'Coa',
			'foreignKey'    => 'coa_id'
		),
		/*'Coagroup' => array(
			'fields'		=>array('Coagroup.*'),
			'className'	=> 'Coagroup',
			'foreignKey'	=> false,
			'conditions'	=> 'Coa.coagroup_id = Coagroup.id'
		),
		'Coatype' => array(
			'fields'		=>array('Coatype.*'),
			'className'	=> 'Coatype',
			'foreignKey'	=> false,
			'conditions'	=> 'Coagroup.coatype_id = Coatype.id'
		),*/
		'Banktransaction'	=> array(
			'fields'		=>array('Banktransaction.*'),
			'className'	=> 'Banktransaction',
			'foreignKey'	=> false,
			'conditions'	=> 'Banktransaction.transaction_id = Gltransaction.transaction_id AND Banktransaction.transactiontype_id = Gltransaction.transactiontype_id'
		)
	);
	var $virtualFields = array(
		"gltransaction_particulars" => "CONCAT(
			Transactiontype.transactiontypename,
			IF(Gltransaction.user_id=0, '', CONCAT('/', User.userfirstname, ' ', User.usermiddlename, ' ', User.userlastname)), 
			IF(Gltransaction.coa_id=0, ' ', CONCAT('/', Coa.coaname))
		)",
		/*"gltransaction_particulars" => "CONCAT(
			Transactiontype.transactiontypename,
			IF(Gltransaction.user_id=0, '', CONCAT('/', User.userfirstname, ' ', User.usermiddlename, ' ', User.userlastname)), 
			IF(Gltransaction.coa_id=0, ' ', CONCAT('/', Coa.coaname)),
			IF(Banktransaction.bankaccount_id=0, ' ', CONCAT('/',  '-', Banktransaction.bankaccount_id))
		)",*/
		"gltransaction_date" => "DATE(Gltransaction.transactiondate)",
		'gltransaction_balance' => 'IF(Gltransaction.transactionamount=0, "0.00", ROUND(Gltransaction.transactionamount, 2))',
		'gltransaction_withdraw' => 'IF(Gltransaction.transactionamount<0,ROUND(Gltransaction.transactionamount, 2), "0.00")',
		'gltransaction_deposit' => 'IF(Gltransaction.transactionamount>0,ROUND(Gltransaction.transactionamount, 2), "0.00")',
		'gltransaction_chequenumber' => 'CONCAT( Banktransaction.banktransactionchequebookseries, " - ", Banktransaction.banktransactionchequesequencynumber )'
	);

}

?>