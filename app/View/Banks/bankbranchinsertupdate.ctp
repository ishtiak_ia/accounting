<?php 
	echo $this->Form->create('Banks', array('action' => 'bankbranchinsertupdateaction'));
		echo $this->Form->inpute('id', array('type'=>'hidden', 'value'=>@$bankbranch[0]['Bankbranch']['id']));
		echo $this->Form->inpute('bankbranchuuid', array('type'=>'hidden', 'value'=>@$bankbranch[0]['Bankbranch']['bankbranchuuid']));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$bank[0]['Bankbranch']['bankbranchisactive'] ? $bankbranch[0]['Bankbranch']['bankbranchisactive'] : 0), 
			'class' => 'form-inline'
		);

	echo"<h3>".__("Add Bank Branch Information")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-8\">";
			echo $this->Form->input(
				"Bankbranch.bankbranchname",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Bank Branch'),
					'tabindex'=>2,
					'placeholder' => __("Bank Branch"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style' => '',
					'value' => @$bankbranch[0]['Bankbranch']['bankbranchname']
				)
			);
			echo $this->Form->input(
				"Bankbranch.bankbranchnamebn",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Bank Branch BN'),
					'tabindex'=>3,
					'placeholder' => __("Bank Branch BN"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style' => '',
					'value' => @$bankbranch[0]['Bankbranch']['bankbranchnamebn']
				)
			);
			echo"<div class=\"form-group\">";
				echo"<label>isActive?</label><br>";
				echo $this->Form->radio(
					'Bankbranch.bankbranchisactive', 
					$options,
					$attributes
				);
			echo"</div>";
			echo $Utilitys->allformbutton('',$this->request["controller"],$page='bankbranch');
		echo"</div>";
	echo"</div>";
	echo $this->Form->end();
	echo"
	<script>
		$(document).ready(function() {
			$(\"#BanksBankbranchinsertupdateactionForm\").validationEngine();
		});
	</script>
	";	
?>