<style>
    #print {
        display:none;

    }
</style>

<div class="row text-center" >
    <?php

        echo"<h3>".__(" Get Sales Report by Date")."</h3></br>";
        echo $this->Form->create(
            'salesReport',
            array(

                'action' => 'orderinsertupdatection',
                'type' => 'file', 'class'>'form-group'
            )
        );

    ?>
</div>
    <div class="row">
        <div class="col-sm-1 ">
            <?php echo __('Between Dates'); ?>
        </div>
        <div class="col-sm-1 ">
            <?php

            echo $this->Form->checkbox(
                'include_to',
                array(
                    'hiddenField' => false,
                    'checked'=>true,



                )
            );

            ?>
        </div>
    </div>
    </br>

    <div class="row" >
        <div class="col-sm-1">
            <?php echo __('Date'); ?>
        </div>
        <div class="col-sm-4 " id="from">
            <?php

            echo $this->Form->input(
                "date_from",
                array(
                    'type'=>'text',
                    'placeholder'=>"from",
                    'label'=> false,
                    'class'=> 'form-control',


                )
            );
            ?>
        </div>
        <div class="col-sm-4 " id="to">
            <?php

            echo $this->Form->input(
                "date_to",
                array(
                    'type'=>'text',
                    'placeholder'=>"to",
                    'label'=> false,
                    'class'=> 'form-control',


                )
            );
            ?>
    </div>
    </br>

    </br>
    <div class="row">

        <div class="col-sm-2 ">

            <button type="button" id="get-sales-report" class="form-control btn-primary" >Search Report</button>
        </div>

    </div>


    <?php
        echo $this->Form->end();
    ?>

</div>
<br>

<div id="print" >
    <?php echo $Utilitys->printbutton ($printableid="testSalesReport", $searchfield=null, $pageformat="c3", $orientation="l", $unit=null, $companyname=$_SESSION["User"]["company_name"], $tabletitle="yes", $tablemonth="yes", $tabledepartment="yes", $printsectoin="printtwotable"); ?>
</div>
<br>
<div id="testSalesReport" style='overflow-x:scroll;'>

Here Shows the Sales Report....
</div>





<script>

    $(document).ready(function() {

         host = "<?php echo $this->webroot; ?>";


//        $("#OrdersOrderinsertupdatectionForm").validationEngine();
        $('[id^="salesReportDate"]').datetimepicker({
            language: "bn",
            format: "yyyy-mm-dd",
            use24hours: true,
            showMeridian: false,
            startView: 2,
            minView: 2,
            autoclose: 1
        });

    });

//start ...asking for the second date input
    $("#salesReportIncludeTo").click(function(){
        if($(this).is(':checked')){
            if(confirm('If you want Sales Report between two dates click OK'+'\n'+'If you want Sales Report for a single date click CANCEL ')){

                $("#to").show('slow');
            }else{
                $(this).attr('checked',false);
            }
        }else{
            $("#to").hide('slow');
        }

    });
// end ...asking for the second date input
//---------------------------------------------------------------------------------------------------------------------------------
//start ...script for date-wise sales report

    $('#get-sales-report').click(function () {


        var dateFrom = $('#salesReportDateFrom').val();
        var dateTo   = $('#salesReportDateTo').val();
        if(dateFrom){

            url = host + 'orders/searchsalesreportbydate/' + dateFrom
        }
        if(dateFrom && dateTo ){

            url = host + 'orders/searchsalesreportbydate/' + dateFrom +'/'+ dateTo;
        }




        $.ajax({

            'url': url,
            type: "POST",
           /* beforeSend: function(){
                $("#testSalesReport").html('<img  src='+host+'img/indicator.gif' />');
            },*/

        }).success(function (data) {

            console.log(data);
            $('#print').show();

            $('#testSalesReport').html(data);

        });

    });








</script>
