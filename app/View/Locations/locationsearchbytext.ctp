<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart($class=null, $id=null);
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('Location')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Location BN')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Parent')  => array('class' => 'highlight sortable')
				), 
				__('Active'), 
				__('Action')
			)
		);
		$locationrows = array();
		$countLocation = 0;
		foreach($location as $Thislocation):
			$countLocation++;
			$locationrows[]= array($countLocation,$Thislocation["Location"]["locationname"],$Thislocation["Location"]["locationnamebn"],$Thislocation["Location"]["locationparent_name"],$Utilitys->statusURL($this->request["controller"], 'location', $Thislocation["Location"]["id"], $Thislocation["Location"]["locationuuid"], $Thislocation["Location"]["locationisactive"]),$Utilitys->cusurl($this->request["controller"], 'location', $Thislocation["Location"]["id"], $Thislocation["Location"]["locationuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$locationrows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#LocationSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#locationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#locationsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>