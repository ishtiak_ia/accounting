<?php
echo $this->Form->create('Users', array('action' => 'subjectinsertupdateaction', 'type' => 'file'));
	echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$subject[0]["Subject"]['id']));
	echo $this->Form->input('subjectuuid', array('type'=>'hidden', 'value'=>@$subject[0]["Subject"]['subjectuuid']?$subject[0]["Subject"]['subjectuuid']:0, 'class'=>''));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$subject[0]['Subject']['subjectisactive'] ? $subject[0]['Subject']['subjectisactive'] : 0),
			'class' => 'form-inline'
		);
	echo"<h3>".__("Add New Subject")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-4\">";
			echo $this->Form->input(
				"Subject.subjectname",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Subject Name'),
					'tabindex'=>1,
					'placeholder' => __("Subject Name"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style' => '',
					'value' => @$subject[0]['Subject']['subjectname']
				)
			);
			echo $this->Form->input(
				"Subject.subjectnamebn",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Subject Name BNG'),
					'tabindex'=>1,
					'placeholder' => __("Subject Name BNG"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style' => '',
					'value' => @$subject[0]['Subject']['subjectnamebn']
				)
			);
			echo"<div class=\"form-group\">";
				echo"<label>isActive?</label><br>";
				echo $this->Form->radio(
					'Subject.subjectisactive', 
					$options,
					$attributes
				);
			echo"</div>";
			echo $Utilitys->allformbutton('',$this->request["controller"],$page='subject');
		echo"</div>";
	echo"</div>";
echo $this->Form->end();
?>
<script type="text/javascript">
 jQuery(document).ready(function(){
    jQuery("#UsersSubjectinsertupdateactionForm").validationEngine();
 });
</script>