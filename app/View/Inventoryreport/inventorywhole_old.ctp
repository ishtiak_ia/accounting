


<?php echo $this->Form->create('journalreports', array('default' => false, 'class'=>' ')); ?>
<fieldset>
    <legend><h3>Inventory Whole</h3></legend>
    <table  border="0">
        <table  border="0">
            <tr>
                <td>Product:</td>
                <td>
                    <?php
                    echo $this->Form->input(
                        "Inventory.product_id",
                        array(
                            'type'=>'select',
                            'options'=>array($product_code),
                            'empty'=>__('Select Code'),
                            'div'=> array('class'=> 'form-group'),
                            'tabindex'=>7,
                            'label'=>'',
                            'class'=> '',
                            'style'=>'width:250px;',
                            'data-validation-engine'=>'validate[required]'
                        )
                    );
                    ?>
                </td>
                <td>&nbsp;</td>
                <td>
                    <?php
                    echo $this->Form->input(
                        "Inventory.product_name",
                        array(
                            'type'=>'select',
                            'options'=>array($product_name),
                            'empty'=>__('Select Name'),
                            'div'=> array('class'=> 'form-group'),
                            'class'=> '',
                            'label'=>'',
                            'style'=>'width:250px;',
                            'data-validation-engine'=>'validate[required]'
                        )
                    );
                    ?>
                </td>
            </tr>
            <tr width="10px">
                <td>Product:</td>
                <td>
                    <?php
                    echo $this->Form->input(
                        "Inventory.type_id",
                        array(
                            'type'=>'select',
                            'options'=>array($product_type),
                            'empty'=>__('Select Type'),
                            'div'=> array('class'=> 'form-group'),
                            'tabindex'=>7,
                            'label'=>'',
                            'class'=> '',
                            'style'=>'width:250px;',
                            'data-validation-engine'=>'validate[required]'
                        )
                    );
                    ?>
                </td>
                <td>&nbsp;</td>
                <td>
                    <?php
                    echo $this->Form->input(
                        "Inventory.category_id",
                        array(
                            'type'=>'select',
                            'options'=>array($product_category),
                            'empty'=>__('Select Category'),
                            'div'=> array('class'=> 'form-group'),
                            'class'=> '',
                            'label'=>'',
                            'style'=>'width:250px;',
                            'data-validation-engine'=>'validate[required]'
                        )
                    );
                    ?>
                </td>
            </tr>
            <tr>
                <td>Stock:</td>
                <td>
                    <?php
                    echo $this->Form->input(
                        "Inventory.branch_id",
                        array(
                            'type'=>'select',
                            'options'=>array($location_code),
                            'empty'=>__('Select Code'),
                            'div'=> array('class'=> 'form-group'),
                            'tabindex'=>7,
                            'label'=>'',
                            'class'=> '',
                            'style'=>'width:250px;',
                            'data-validation-engine'=>'validate[required]'
                        )
                    );
                    ?>
                </td>
                <td>&nbsp;</td>
                <td>
                    <?php
                    echo $this->Form->input(
                        "Inventory.branch_name",
                        array(
                            'type'=>'select',
                            'options'=>array($location_name),
                            'empty'=>__('Select Name'),
                            'div'=> array('class'=> 'form-group'),
                            'class'=> '',
                            'label'=>'',
                            'style'=>'width:250px;',
                            'data-validation-engine'=>'validate[required]'
                        )
                    );
                    ?>
                </td>
            </tr>
            <tr>
                <td>Date:</td>
                <td>
                    <?php
                    echo $this->Form->input(
                        "Inventory.fromdate",
                        array(
                            'type' => 'text',
                            'div' => array('class'=> 'form-group'),
                            'label' =>'',
                            'placeholder' => 'From',
                            'class'=> 'form-control',
                            'data-validation-engine'=>'validate[required]',
                            'style' => 'width:250px;'
                        )
                    );
                    ?>
                </td>
                <td>&nbsp;</td>
                <td>
                    <?php
                    echo $this->Form->input(
                        "Inventory.todate",
                        array(
                            'type' => 'text',
                            'div' => array('class'=> 'form-group'),
                            'label' =>'',
                            'placeholder' => 'To',
                            'class'=> 'form-control',
                            'data-validation-engine'=>'validate[required]',
                            'style' => ''
                        )
                    );
                    ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php
                    echo $this->Form->submit('View', array('div' => array('class'=> 'form-group'), 'align' => 'right', 'formnovalidate' => 'true', 'class'=> '', 'onClick' => 'get_inventory();'));
                    ?>
                </td>
            </tr>
        </table>
</fieldset>
<br>
<br>
<?php echo $this->form->end(); ?>

<?php
echo "<div id='print_page' class='right-aligned-texts'>";
echo $Utilitys->printbutton ($printableid="loadinventory", $searchfield=null, $pageformat="c3", $orientation="l", $unit=null, $companyname=$_SESSION["User"]["company_name"], $tabletitle="yes", $tablemonth="yes", $tabledepartment="yes", $printsectoin="printtwotable");
echo "</div>";

?>

<div id="loadinventory">

</div>

<?php
echo"
		<script>
			function get_inventory(){
			        var product_id = $(\"#InventoryProductId\").val();
			        var branch_id = $(\"#InventoryBranchId\").val();
			        var type_id = $(\"#InventoryTypeId\").val();
			        var category_id = $(\"#InventoryCategoryId\").val();
			        var from_date = $(\"#InventoryFromdate\").val();
					var to_date = $(\"#InventoryTodate\").val();
					if(from_date!='' && to_date!=''){
						$(\"#print_page\").hide();
						$(\"#loadinventory\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						$.post(
							'".$this->webroot."inventoryreports/inventorywholegenerate',
							{product_id:product_id,from_date:from_date,to_date:to_date,branch_id:branch_id,type_id:type_id,category_id:category_id},
							function(result) {
								$(\"#print_page\").show();
								$(\"#loadinventory\").html(result);
							}
						);

					}else
						alert('Chart of Account, Form date and To date can not be empty');
			}

		</script>
	";
//$(\"#banksarchloading\").html(result);
?>

<script type="text/javascript">
    $(document).ready(function() {
        $("#InventoryProductId").select2();
        $("#InventoryProductName").select2();
        $("#InventoryTypeId").select2();
        $("#InventoryCategoryId").select2();
        $("#InventoryBranchId").select2();
        $("#InventoryBranchName").select2();
        $("#print_page").hide();
        $("#InventoryFromdate").datetimepicker({language: "bn",format: "yyyy-mm-dd", use24hours: true, showMeridian: false, startView: 2, minView: 2, autoclose: 1});

        $("#InventoryTodate").datetimepicker({language: "bn",format: "yyyy-mm-dd", use24hours: true, showMeridian: false, startView: 2, minView: 2, autoclose: 1});

        $("#InventoryProductId").change(function(){
            var product_code = $(this).val(),code;
            $('#InventoryProductName option[value='+product_code+']').attr('selected', 'selected');
            code = $('#InventoryProductName option:selected').text();
            $('#s2id_InventoryProductName .select2-chosen').text(code);

        });

        $("#InventoryProductName").change(function(){
            var product_name = $(this).val(),coa;
            $('#InventoryProductId option[value='+product_name+']').attr('selected', 'selected');
            product_name = $('#InventoryProductId option:selected').text();
            $('#s2id_InventoryProductId .select2-chosen').text(product_name);

        });

        $("#InventoryBranchId").change(function(){
            var inventory_branch_id = $(this).val(),branch_id;
            $('#InventoryBranchName option[value='+inventory_branch_id+']').attr('selected', 'selected');
            branch_id = $('#InventoryBranchName option:selected').text();
            $('#s2id_InventoryBranchName .select2-chosen').text(branch_id);

        });

        $("#InventoryBranchName").change(function(){
            var inventory_branch = $(this).val(),branch_name;
            $('#InventoryBranchId option[value='+inventory_branch+']').attr('selected', 'selected');
            branch_name = $('#InventoryBranchId option:selected').text();
            $('#s2id_InventoryBranchId .select2-chosen').text(branch_name);

        });

    });
</script>
