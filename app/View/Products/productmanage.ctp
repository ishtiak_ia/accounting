<?php
//pr($product);
	echo "<h2>".__('Product'). $Utilitys->addurl($title=__("Add New Product"), $this->request['controller'], $page="product")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
		"Product.productcategory_id",
		array(
			'type'=>'select',
			"options"=>array($productcategory),
			'empty'=>__('Select Product Category'),
			'div'=>false,
			'tabindex'=>1,
			'label'=>false,
			'class'=> 'form-control',
			'style'=>''
			)
		)
		.$this->Form->input(
			"Product.producttype_id",
			array(
				'type'=>'select',
				"options"=>array($producttype),
				'class'=> 'form-control',
				'empty'=>__('Select Product Type'),
				'div'=>false,
				'tabindex'=>2,
				'label'=>false,
				'style'=>''
				)
			)
		.$this->Form->input(
			"Product.unit_id",
			array(
				'type'=>'select',
				"options"=>array($productunit),
				'class'=> 'form-control',
				'empty'=>__('Select Unit'),
				'div'=>false,
				'tabindex'=>3,
				'label'=>false,
				'style'=>''
				)
			)
		.$this->Form->input(
			"Product.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'placeholder' => __("Search Word"),
				'label' => false,
				'tabindex'=>4,
				'class'=> 'form-control'
				)
			);
		$searchfield="";
		echo"
			<div class=\"btn-group\">
				<button class=\"btn btn-warning btn-sm dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-bars\"></i> Export Table Data</button>
				<ul class=\"dropdown-menu \" role=\"menu\">
					<li><a href=\"#\" onClick =\"$('#productsarchloading').tableExport({type:'png',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> <img src='".$this->webroot."js/icons/png.png' width='24px'> PNG</a></li>
					<li><a href=\"#\" onClick =\"$('#productsarchloading').tableExport({type:'pdf',pdfFontSize:'7',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> <img src='".$this->webroot."js/icons/pdf.png' width='24px'> PDF</a></li>
				</ul>
			</div>
		";
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"productsarchloading\">
	";
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('Product Code')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Product Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Product Name BN')  => array('class' => 'highlight sortable')
				),
				array(
					__('Product Category')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Product Type')  => array('class' => 'highlight sortable')
				),
				array(
					__('Product Price')  => array('class' => 'highlight sortable')
				),
				array(
					__('Format')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Is Active')  => array('class' => 'highlight sortable')
				),
				__('Action')
			)
		);
		$productrows = array();
		$countProduct = 0-1+$this->Paginator->counter('%start%');
		
		foreach($product as $Thisproduct):
			$countProduct++;
			$productrows[]= array($countProduct,$Thisproduct["Product"]["productcode"],$Thisproduct["Product"]["productname"],$Thisproduct["Product"]["productnamebn"],$Thisproduct["Productcategory"]["productcategoryname"]. " / " .$Thisproduct["Productcategory"]["productcategorynamebn"],$Thisproduct["Producttype"]["producttypename"]. " / " .$Thisproduct["Producttype"]["producttypenamebn"],$Thisproduct["Product"]["productprice"],$Thisproduct["Product"]["format"],$Utilitys->statusURL($this->request["controller"], 'product', $Thisproduct["Product"]["id"], $Thisproduct["Product"]["productuuid"], $Thisproduct["Product"]["productisactive"]),$Utilitys->cusurl($this->request['controller'], 'product', $Thisproduct["Product"]["id"], $Thisproduct["Product"]["productuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$productrows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();

	echo"
				</div>
		<script>
			$(document).ready(function() {
				$(\"#ProductSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#productsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."products/productsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#productsarchloading\").html(result);
						}
					);
				});
				$(\"#ProductProductcategoryId\").change(function(){
					var ProductProductcategoryId = $(\"#ProductProductcategoryId\").val();
					var ProductProducttypeId = $(\"#ProductProducttypeId\").val();
					var ProductUnitId = $(\"#ProductUnitId\").val();
					$(\"#productsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."products/productsearchbytext',
						{ProductProductcategoryId:ProductProductcategoryId,ProductProducttypeId:ProductProducttypeId,ProductUnitId:ProductUnitId},
						function(result) {
							$(\"#productsarchloading\").html(result);
						}
					);
				});
				$(\"#ProductProducttypeId\").change(function(){
					var ProductProductcategoryId = $(\"#ProductProductcategoryId\").val();
					var ProductProducttypeId = $(\"#ProductProducttypeId\").val();
					var ProductUnitId = $(\"#ProductUnitId\").val();
					$(\"#productsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."products/productsearchbytext',
						{ProductProductcategoryId:ProductProductcategoryId, ProductProducttypeId:ProductProducttypeId,ProductUnitId:ProductUnitId},
						function(result) {
							$(\"#productsarchloading\").html(result);
						}
					);
				});
				$(\"#ProductUnitId\").change(function(){
					var ProductProductcategoryId = $(\"#ProductProductcategoryId\").val();
					var ProductProducttypeId = $(\"#ProductProducttypeId\").val();
					var ProductUnitId = $(\"#ProductUnitId\").val();
					$(\"#productsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."products/productsearchbytext',
						{ProductProductcategoryId:ProductProductcategoryId,ProductProducttypeId:ProductProducttypeId,ProductUnitId:ProductUnitId},
						function(result) {
							$(\"#productsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>
