<?PHP
echo $this->Form->create('Banks', array('action' => 'banktransactioninsertupdateaction', 'type' => 'file'));
	echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
	echo $this->Form->input('banktransactionuuid', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['banktransactionuuid']?$banktransaction[0]['Banktransaction']['banktransactionuuid']:0, 'class'=>''));
	echo $this->Form->input('Banktransaction.salesman_id', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['salesman_id']?$banktransaction[0]['Banktransaction']['salesman_id']:0, 'class'=>''));
	/*echo $this->Form->input('Banktransaction.bank_id', array('type'=>'hidden', 'value'=>"", 'class'=>''));*/
	echo $this->Form->input('Banktransaction.bankbranch_id', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['bankbranch_id']?$banktransaction[0]['Banktransaction']['bankbranch_id']:0, 'class'=>''));
	echo $this->Form->input('Banktransaction.bankaccounttype_id', array('type'=>'hidden', 'value'=>"", 'class'=>''));
	echo $this->Form->input('Banktransaction.coa_id', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['coa_id']?$banktransaction[0]['Banktransaction']['coa_id']:1, 'class'=>''));
	
	$options = array($group, 999 =>__("Others"));
	$attributes = array(
		'legend' => false, 
		'label' => true, 
		'value' => '', 
		'class' => 'form-inline'
	);
	if(@$banktransaction[0]['User']['id'] == 0 && @$id !=0):
		$banktransaction[0]['User']['group_id']=999;
	endif;
	echo"<h3>".__("Add New Bank Transaction")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-11\">";
			echo"<div class=\"row\">";
				echo"<div class=\"col-md-4\">";
					echo $this->Form->input(
						"Banktransaction.banktransactiondate",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Date'),
							'tabindex'=>1,
							'placeholder' => __("Date"),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style' => '',
							'value' => @$banktransaction[0]["Banktransaction"]["transactiondate"],
							'onkeydown'	=>	'test();'
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-4\">";
					echo $this->Form->input(
						"Banktransaction.group_id",
						array(
							'type'=>'select',
							"options"=>$options,
							'empty'=>__('Select Group'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>2,
							'label'=>__('Select Group'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$banktransaction[0]['User']['group_id'] ? $banktransaction[0]['User']['group_id'] : 0)
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-4\" id=\"groupuserchangediv\">";
					echo $this->Form->input(
						"Banktransaction.user_id",
						array(
							'type'=>'select',
							"options"=>array($user),
							'empty'=>__('Select Member'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>3,
							'label'=>__('Select Member'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$banktransaction[0]['User']['id'] ? $banktransaction[0]['User']['id'] : 0)
						)
					);
				echo"</div>";
			echo"</div>";
			echo"<div class=\"row\">";
				echo"<div class=\"col-md-4\">";
					echo $this->Form->input(
						"Banktransaction.banktransactiontype_id",
						array(
							'type'=>'select',
							"options"=>array($_SESSION["ST_BANKDEPOSIT"]=>"Deposit",$_SESSION["ST_BANKPAYMENT"]=>"Withdraw"),
							'empty'=>__('Select Transaction Type'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>7,
							'label'=>__('Select Type'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$banktransaction[0]['Banktransaction']['transactiontype_id'] ? $banktransaction[0]['Banktransaction']['transactiontype_id'] : 0)
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-4\">";
					echo"<div class=\"row\">";
						echo"<div class=\"col-md-6\">";
					echo $this->Form->input(
						"Banktransaction.bank_id",
						array(
							'type'=>'select',
							"options"=>array($bank),
							'empty'=>__('Select Bank'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>4,
							'label'=>__('Select Bank'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$banktransaction[0]['Banktransaction']['bank_id'] ? $banktransaction[0]['Banktransaction']['bank_id'] : 0)
						)
					);
						echo"</div>";
						echo"<div class=\"col-md-6\">";
					echo $this->Form->input(
						"Banktransaction.bankaccount_id",
						array(
							'type'=>'select',
							"options"=>array($bankaccount),
							'empty'=>__('Select Account'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>4,
							'label'=>__('Select Account'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$banktransaction[0]['Banktransaction']['bankaccount_id'] ? $banktransaction[0]['Banktransaction']['bankaccount_id'] : 0)
						)
					);
						echo"</div>";
					echo"</div>";
				echo"</div>";
				echo"<div class=\"col-md-4\">";
					echo $this->Form->input(
						"Banktransaction.banktransactionchequebookseries",
						array(
							'type'=>'select',
							"options"=>array($bankchequebook),
							'empty'=>__('Select Series'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>5,
							'label'=>__('Select Series'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$banktransaction[0]['Banktransaction']['bankchequebook_id'] ? $banktransaction[0]['Banktransaction']['bankchequebook_id'] : 0)
						)
					);
					echo"<span id=\"BanktransactionBankaccountIdspan\"></span>";
				echo"</div>";
			echo"</div>";
			echo"<div class=\"row\">";
				echo"<div class=\"col-md-4\">";
					echo $this->Form->input(
						"Banktransaction.banktransactionchequesequencynumber",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Cheque Number'),
							'tabindex'=>6,
							'placeholder' => __("Cheque Number"),
							'readonly' => 'readonly',
							'class'=> 'form-control',
							'style' => '',
							"value" => (@$banktransaction[0]['Banktransaction']['banktransaction_chequenumber'] ? $banktransaction[0]['Banktransaction']['banktransaction_chequenumber'] : '')
						)
					);
					echo"<span id=\"BanktransactionBanktransactionchequebookseriesspan\"></span>";
				echo"</div>";
				echo"<div class=\"col-md-4\">";
				echo"<div class=\"col-md-6\">";
					echo $this->Form->input(
						"paymentrecive",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Payment/Recived'),
							'tabindex'=>8,
							'placeholder' => __("Payment/Recived"),
							'readonly' => 'readonly',
							'class'=> 'form-control',
							'style' => '',
							'value'=>0.00
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-6\">";
					echo $this->Form->input(
						"usertotalamount",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Client/Suppler/User'),
							'tabindex'=>8,
							'placeholder' => __("Client/Suppler/User"),
							'readonly' => 'readonly',
							'class'=> 'form-control',
							'style' => '',
							'value'=>0.00
						)
					);
				echo"</div>";
				echo"</div>";
				echo"<div class=\"col-md-4\">";
				echo"<div class=\"col-md-6\">";
					echo $this->Form->input(
						"remainigbalance",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Remainig'),
							'tabindex'=>9,
							'placeholder' => __("Remainig"),
							'readonly' => 'readonly',
							'class'=> 'form-control',
							'style' => ''
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-6\">";
					echo $this->Form->input(
						"userremainigbalance",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Client/Suppler/User Remainig'),
							'tabindex'=>9,
							'placeholder' => __("Client/Suppler/User Remainig"),
							'readonly' => 'readonly',
							'class'=> 'form-control',
							'style' => ''
						)
					);
				echo"</div>";
				echo"</div>";
			echo"</div>";
			echo $this->Form->input(
				"Banktransaction.banktransactionamount",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Amount'),
					'tabindex'=>10,
					'placeholder' => __("Amount"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style' => '',
					"value" => (@$banktransaction[0]['Banktransaction']['transactionamount'] ? str_replace("-", "", $banktransaction[0]['Banktransaction']['transactionamount']) : 0)
				)
			);
			echo $Utilitys->allformbutton('',$this->request["controller"],$page='banktransaction');
		echo"</div>";
	echo"</div>";
	echo $this->Form->end();
	echo"
	<script>
	function test(){
		alert('test');
	}
		$(document).ready(function() {
			BanktransactionGroupId();
			$(\"#BanksBanktransactioninsertupdateactionForm\").validationEngine();
			var totalRemainig = 0.00;
			var banktransactiontotalamount = 0.00;
			var bankaccount_id = ".(@$banktransaction[0]['Banktransaction']['bankaccount_id'] ? $banktransaction[0]['Banktransaction']['bankaccount_id'] : 0).";
			var ST_BANKDEPOSIT = ".$_SESSION['ST_BANKDEPOSIT'].";
			var ST_BANKPAYMENT = ".$_SESSION['ST_BANKPAYMENT'].";
			banktransactiontotalamount = ".(@$banktransaction[0]['Banktransaction']['transactionamount'] ? $banktransaction[0]['Banktransaction']['transactionamount'] : 0).";

			if(ST_BANKDEPOSIT == ".(@$banktransaction[0]['Banktransaction']['transactiontype_id'] ? $banktransaction[0]['Banktransaction']['transactiontype_id'] : 0)."){
				$(\"#BanktransactionBanktransactionchequesequencynumber\").removeAttr(\"readonly\");
				$(\"#BanktransactionBanktransactionchequebookseries\").attr(\"readonly\",\"readonly\");
				$(\"#BanktransactionBanktransactionchequebookseries\").attr(\"disabled\",\"disabled\");
				$(\"#BanktransactionBanktransactionchequebookseries\").removeAttr(\"data-validation-engine\");
			}else{
				$(\"#BanktransactionBanktransactionchequesequencynumber\").attr(\"readonly\",\"readonly\");
				$(\"#BanktransactionBanktransactionchequebookseries\").removeAttr(\"readonly\");
				$(\"#BanktransactionBanktransactionchequebookseries\").removeAttr(\"disabled\");

				$(\"#BanktransactionBanktransactionchequebookseries\").attr(\"data-validation-engine\",\"validate[required]\");
			}
			if(bankaccount_id!=0){
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."banks/bankchequebookseriesbyaccount',
					data:{BankaccountId:bankaccount_id},
					dataType: \"JSON\",
					success: function(result) {
						//$(\"#BanktransactionBanktransactionchequebookseries\").html(result.bankchequebook_options);
						$(\"#BanksPaymentrecive\").val(result.banktransactiontotalamount-banktransactiontotalamount);
						$(\"#BanksRemainigbalance\").val(result.banktransactiontotalamount);
						//$(\"#BanktransactionBankId\").val(result.bank_id);
						$(\"#BanktransactionBankbranchId\").val(result.bankbranch_id);
						$(\"#BanktransactionBankaccounttypeId\").val(result.bankaccounttype_id);
						$(\"#BanktransactionBankaccountIdspan\").html('');
					}
				});
			}			

			$(\"#BanktransactionBanktransactiondate\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd HH:mm:ss\", use24hours: true, showMeridian: false, startView: 2, minView: 2, autoclose: 1});

			$(\"#BanktransactionGroupId\").change(function(){
				BanktransactionGroupId();
			});

			$(\"#BanktransactionUserId\").change(function(){
				var ClientId = $(this).val();
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."banks/salespersonbyuser',
					data:{ClientId:ClientId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#BanktransactionSalesmanId\").val(result.salesperson_id);
						$(\"#BanksUsertotalamount\").val(result.usertotalamount);
					}
				});
			});

			$(\"#BanktransactionBankId\").change(function(){
				var BankaccountId = $(this).val();
				$(\"#BanktransactionBankaccountIdspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."banks/bankchequebookseriesbybank',
					data:{BankaccountId:BankaccountId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#BanktransactionBankaccountId\").html(result.bankchequebook_options);
						$(\"#BanktransactionBankaccountIdspan\").html('');
					}
				});
			});

			$(\"#BanktransactionBankaccountId\").change(function(){
				var BankId = $(\"#BanktransactionBankId\").val();
				var BankaccountId = $(this).val();
				$(\"#BanktransactionBankaccountIdspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."banks/bankchequebookseriesbyaccount',
					data:{BankId:BankId,BankaccountId:BankaccountId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#BanktransactionBanktransactionchequebookseries\").html(result.bankchequebook_options);
						$(\"#BanksPaymentrecive\").val(result.banktransactiontotalamount);
						//$(\"#BanktransactionBankId\").val(result.bank_id);
						$(\"#BanktransactionBankbranchId\").val(result.bankbranch_id);
						$(\"#BanktransactionBankaccounttypeId\").val(result.bankaccounttype_id);
						$(\"#BanktransactionBankaccountIdspan\").html('');
					}
				});
			});

			$(\"#BanktransactionBanktransactionchequebookseries\").change(function(){
				var BanktransactionBanktransactionchequesequencynumber = $(this).val();
				$(\"#BanktransactionBanktransactionchequebookseriesspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
				       	cache: false,
				       	type: 'POST',
					url: '".$this->webroot."banks/bankchequebooksequencybyseries',
					data: {BanktransactionBanktransactionchequesequencynumber:BanktransactionBanktransactionchequesequencynumber},					
					dataType: \"JSON\",
					success: function(result) {
						$(\"#BanktransactionBanktransactionchequesequencynumber\").val(result.banktransaction_options);
						$(\"#BanksId\").val(result.banktransaction_id);
						$(\"#BanksBanktransactionuuid\").val(result.banktransaction_uuid);
						$(\"#BanktransactionBanktransactionchequebookseriesspan\").html('');
					}
				});
			});
			
			$(\"#BanktransactionBanktransactionamount\").keyup(function(){
				var BanktransactionBanktransactionamount = $(this).val();
				var BanksPaymentrecive = $(\"#BanksPaymentrecive\").val();
				if(BanksPaymentrecive == \"\"){
					BanksPaymentrecive = 0.00;
				}
				var BanktransactionBanktransactiontypeId = $(\"#BanktransactionBanktransactiontypeId\").val();
				if(BanktransactionBanktransactiontypeId==ST_BANKDEPOSIT){
					totalRemainig = parseFloat(BanksPaymentrecive) + parseFloat(BanktransactionBanktransactionamount);
					$(\"#BanktransactionBanktransactionchequesequencynumber\").removeAttr(\"readonly\");
				}else if(BanktransactionBanktransactiontypeId==ST_BANKPAYMENT){
					totalRemainig = parseFloat(BanksPaymentrecive) - parseFloat(BanktransactionBanktransactionamount);
				}else{
					alert('".__("Please Select Transaction Type")."');
					totalRemainig = 0.00;
				}
				$(\"#BanksRemainigbalance\").val(totalRemainig);
			});
			$(\"#BanktransactionBanktransactiontypeId\").change(function(){
				var totalRemainig = 0.00;
				var BanktransactionBanktransactionamount = $(\"#BanktransactionBanktransactionamount\").val();
				var BanksPaymentrecive = $(\"#BanksPaymentrecive\").val();
				var BanktransactionBanktransactiontypeId = $(this).val();
				if(BanktransactionBanktransactiontypeId==ST_BANKDEPOSIT){
					totalRemainig = parseFloat(BanksPaymentrecive) + parseFloat(BanktransactionBanktransactionamount);
					$(\"#BanktransactionBanktransactionchequesequencynumber\").removeAttr(\"readonly\");
					$(\"#BanktransactionBanktransactionchequebookseries\").attr(\"readonly\",\"readonly\");
					$(\"#BanktransactionBanktransactionchequebookseries\").attr(\"disabled\",\"disabled\");
					$(\"#BanktransactionBanktransactionchequebookseries\").removeAttr(\"data-validation-engine\");
				}else if(BanktransactionBanktransactiontypeId==ST_BANKPAYMENT){
					totalRemainig = parseFloat(BanksPaymentrecive) - parseFloat(BanktransactionBanktransactionamount);
					$(\"#BanktransactionBanktransactionchequesequencynumber\").attr(\"readonly\",\"readonly\");
					$(\"#BanktransactionBanktransactionchequebookseries\").removeAttr(\"readonly\");
					$(\"#BanktransactionBanktransactionchequebookseries\").removeAttr(\"disabled\");
					$(\"#BanktransactionBanktransactionchequebookseries\").attr(\"data-validation-engine\",\"validate[required]\");
				}else{
					alert('".__("Please Select Transaction Type")."');
					totalRemainig = 0.00;
					$(\"#BanktransactionBanktransactionchequesequencynumber\").attr(\"readonly\",\"readonly\");
					$(\"#BanktransactionBanktransactionchequebookseries\").removeAttr(\"readonly\");
					$(\"#BanktransactionBanktransactionchequebookseries\").removeAttr(\"disabled\");
				}
				$(\"#BanksRemainigbalance\").val(totalRemainig);
			});
		});
		function BanktransactionGroupId(){
			var GroupId = $(\"#BanktransactionGroupId\").val();
			var BanktransactionBanktransactionothernote = '".(@$banktransaction[0]['Banktransaction']['banktransactionothernote'] ? $banktransaction[0]['Banktransaction']['banktransactionothernote'] : '')."';
			var userid=  '".(@$banktransaction[0]['Banktransaction']['user_id'] ? $banktransaction[0]['Banktransaction']['user_id'] : 0)."';
			if(userid!=0){
				userothernote = userid;
			}else{
				userothernote = BanktransactionBanktransactionothernote;
			}
			if(GroupId==5){
				$(\"#BanktransactionSalesmanId\").attr(\"disabled\", \"disabled\");
			}else{
				$(\"#BanktransactionSalesmanId\").removeAttr(\"disabled\");
			}
			$(\"#groupuserchangediv\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
			$.post(
				'".$this->webroot."banks/banktransctionuserlistbygroup',
				{GroupId:GroupId,BanktransactionBanktransactionothernote:BanktransactionBanktransactionothernote},
				function(result) {
					$(\"#groupuserchangediv\").html(result);
				}
			);
		}
	</script>
	";
?>