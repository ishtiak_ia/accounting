<?php
App::uses('AppModel', 'Model');
class Unit extends AppModel {
	public $name = 'Unit';
	public $usetables = 'units';

	var $belongsTo = array(
		'Unittype' => array(
			'fields' =>array('unittypename', 'unittypenamebn'),
			'className'    => 'Unittype',
			'foreignKey'    => 'unittype_id'
		)
	);

	var $virtualFields = array(
		'unit_name' => 'CONCAT(Unit.unitname, " / ", Unit.unitnamebn)',
		'unit_symbol' => 'CONCAT(Unit.unitsymbol, " / ", Unit.unitsymbolbn)',
		'isActive' => 'IF(Unittype.unittypeisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Unittype.unittypeisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
		//'isActive' => 'IF(User.userisactive = 0, "<span class=\"orange\">Inactive</span>", IF(User.userisactive = 1, "<span class=\"green\">Active</span>", "<span class=\"red\">Deleted</span>"))'
	);
}