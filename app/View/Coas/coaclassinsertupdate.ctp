<?php 
//print_r($coaclass);
	echo"
		<table>
			<tr>
				<td>
					".__('Add Chart of Account Class')."
				</td>
			</tr>
			<tr>
				<td>
	";
	echo $this->Form->create('Coas', array('action' => 'coaclassinsertupdateaction'));
		echo $this->Form->inpute('id', array('type'=>'hidden', 'value'=>@$coaclass[0]['Coaclass']['id']));
		echo $this->Form->inpute('coaclassuuid', array('type'=>'hidden', 'value'=>@$coaclass[0]['Coaclass']['coaclassuuid']));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$coaclass[0]['Coaclass']['coaclassisactive'] ? $coaclass[0]['Coaclass']['coaclassisactive'] : 0), 
			'class' => 'radio inline'
		);
		echo $Utilitys->tablestart($class=null, $id=null);
		echo "
						<tr>
							<td>".__("Type")."</td>
							<td>
								".$this->Form->input(
									"Coaclass.coatype_code_id",
									array(
										'type'=>'select',
										"options"=>array($coatype_code),
										'empty'=>__('Select Code'),
										'div'=>false,
										'tabindex'=>1,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coaclass[0]['Coaclass']['coatype_id'] ? $coaclass[0]['Coaclass']['coatype_id'] : 0)
									)
								)."
							</td>
							<td>
								".$this->Form->input(
									"Coaclass.coatype_id",
									array(
										'type'=>'select',
										"options"=>array($coatype),
										'empty'=>__('Select Type'),
										'div'=>false,
										'tabindex'=>2,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coaclass[0]['Coaclass']['coatype_id'] ? $coaclass[0]['Coaclass']['coatype_id'] : 0)
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("Group")."</td>
							<td>
								".$this->Form->input(
									"Coaclass.coagroup_code_id",array(
										'type'=>'select',
										"options"=>array($coagroup_code),
										'empty'=>__('Select Code'),
										'div'=>false,
										'tabindex'=>3,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coaclass[0]['Coaclass']['coagroup_id'] ? $coaclass[0]['Coaclass']['coagroup_id'] : 0)
									)
								)."
								<span id=\"grouploading\"></span>
							</td>
							<td>
								".$this->Form->input(
									"Coaclass.coagroup_id",array(
										'type'=>'select',
										"options"=>array($coagroup),
										'empty'=>__('Select Group'),
										'div'=>false,
										'tabindex'=>4,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coaclass[0]['Coaclass']['coagroup_id'] ? $coaclass[0]['Coaclass']['coagroup_id'] : 0)
									)
								)."
								<span id=\"grouploading\"></span>
							</td>
						</tr>
						<tr>
							<td>".__("CODE")."</td>
							<td>
								".$this->Form->input(
									"Coaclass.coaclasscode",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'tabindex'=>3,
										'data-validation-engine'=>'validate[required]',
										'value' =>@$coaclass[0]['Coaclass']['coaclasscode']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("Class")."</td>
							<td>
								".$this->Form->input(
									"Coaclass.coaclassname",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'tabindex'=>3,
										'data-validation-engine'=>'validate[required]',
										'value' =>@$coaclass[0]['Coaclass']['coaclassname']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("Class BN")."</td>
							<td>
								".$this->Form->input(
									"Coaclass.coaclassnamebn",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'tabindex'=>4,
										'data-validation-engine'=>'validate[required]',
										'required' => 'required',
										'value' => @$coaclass[0]['Coaclass']['coaclassnamebn']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("isActive")."</td>
							<td>
								".$this->Form->radio(
									'Coaclass.coaclassisactive', 
									$options, 
									$attributes
								)."
							</td>
						</tr>
						<tr>
							<td colspan=\"2\">
								".$Utilitys->allformbutton('',$this->request["controller"],$page='coaclass')."
							</td>
						</tr>
					</table>
	";
	echo $this->Form->end();
	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				$(\"#CoaclassCoatypeCodeId\").select2();
				$(\"#CoaclassCoatypeId\").select2();
				$(\"#CoaclassCoagroupCodeId\").select2();
				$(\"#CoaclassCoagroupId\").select2();
				jQuery(\"#CoasCoaclassinsertupdateactionForm\").validationEngine({
					'custom_error_messages': {
						'required': {
							'message': \"".__("This field is mandatory.")."\"
						}
					}
				}); 

				$(\"#CoaclassCoagroupCodeId\").click(function(){
					var group_id = $(\"#CoaclassCoatypeCodeId\").val();
					var group_name = $(\"#CoaclassCoatypeId\").val();
					if(group_id == '' && group_name == '')
						alert(\"Please select type first\");
				});

				$(\"#CoaclassCoagroupId\").click(function(){
					var group_id = $(\"#CoaclassCoatypeCodeId\").val();
					var group_name = $(\"#CoaclassCoatypeId\").val();
					if(group_id == '' && group_name == '')
						alert(\"Please select type first\");
				});

				$(\"#CoaclassCoatypeId\").change(function(){
					var coatype_id = $(this).val(),show,type;
					$(\"#grouploading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/listcoagroup/'+coatype_id, {coatype_id:coatype_id},
					function(result) {
						$(\"#CoaclassCoagroupId\").html(result);
						$(\"#grouploading\").html('');
						$('#CoaclassCoatypeCodeId option[value='+coatype_id+']').attr('selected', 'selected');
						type = $('#CoaclassCoatypeCodeId option:selected').text();
                    	$( '#s2id_CoaclassCoatypeCodeId .select2-chosen' ).text(type);

					});
				});

				$(\"#CoaclassCoatypeCodeId\").change(function(){
                var coa_type_code_id = $(this).val(),type_code;
                $.post(
                    '".$this->webroot."coas/getCode',
                    {coa_type_code_id:coa_type_code_id},
                    function(result) {
                    	$('#CoaclassCoatypeId option[value='+result+']').attr('selected', 'selected');
                    	type_code = $('#CoaclassCoatypeId option:selected').text();
                    	$( '#s2id_CoaclassCoatypeId .select2-chosen' ).text(type_code);

                    }
                );
                
            });

			 $(\"#CoaclassCoagroupCodeId\").change(function(){
                var coa_group_code_id = $(this).val(),group_code;
                $.post(
                    '".$this->webroot."coas/getCode',
                    {coa_group_code_id:coa_group_code_id},
                    function(result) {
                    	$('#CoaclassCoagroupId option[value='+result+']').attr('selected', 'selected');
                    	group_code = $('#CoaclassCoagroupId option:selected').text();
                    	$( '#s2id_CoaclassCoagroupId .select2-chosen' ).text(group_code);

                    }
                ); 
             }); 

			 $(\"#CoaclassCoagroupId\").change(function(){
                var coa_group_name_id = $(this).val(),group;
                $.post(
                    '".$this->webroot."coas/getCode',
                    {coa_group_name_id:coa_group_name_id},
                    function(result) {
                    	$('#CoaclassCoagroupCodeId option[value='+result+']').attr('selected', 'selected');
                    	group = $('#CoaclassCoagroupCodeId option:selected').text();
                    	$( '#s2id_CoaclassCoagroupCodeId .select2-chosen' ).text(group);

                    }
                ); 
             }); 

		});
		</script>
	";


?>
