<?php
	echo "<h2>".__('Unit Type'). $Utilitys->addurl($title=__("Add New Unit Type"), $this->request['controller'], $page="unittype")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Unittype.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'placeholder' => __("Search Word"),
				'label' => false,
				'tabindex'=>5,
				'class'=> 'form-control'
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"unittypesarchloading\">
	";
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('Unit Type Name')  => array('class' => 'highlight sortable')
				),
				array(
					__('Is Active')  => array('class' => 'highlight sortable')
				),
				__('Action')
			)
		);
		$unittyperows = array();
		$countUnittype = 0-1+$this->Paginator->counter('%start%');
		foreach($unittype as $Thisunittype):
			$countUnittype++;
			$unittyperows[]= array($countUnittype,$Thisunittype["Unittype"]["unittype_name"],$Utilitys->statusURL($this->request["controller"], 'unittype', $Thisunittype["Unittype"]["id"], $Thisunittype["Unittype"]["unittypeuuid"], $Thisunittype["Unittype"]["unittypeisactive"]),$Utilitys->cusurl($this->request['controller'], 'unittype', $Thisunittype["Unittype"]["id"], $Thisunittype["Unittype"]["unittypeuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$unittyperows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();

	echo"
				</div>
		<script>
			$(document).ready(function() {
				$(\"#UnittypeSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#unittypesarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."units/unittypesearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#unittypesarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>