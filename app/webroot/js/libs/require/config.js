/**
 * config.js
 * Copyright (c) 2014 Steven Spungin (TwelveTone LLC)  steven@twelvetone.tv
 * Copyright (c) 2015 James Hall (Parallax Agency Ltd) james@parall.ax
 *
 * Licensed under the MIT License.
 * http://opensource.org/licenses/mit-license
 */

/**
 * This file declaratively defines jsPDF plugin dependencies.
 *
 * This allows a host page to simply include require.js and bootstrap the page with a single require statement.
 */

// Skip if Require.JS not installed
if (typeof require === 'object') {

if (typeof require_baseUrl_override === 'undefined'){
	require_baseUrl_override = '/hasan_hrm/';
}

require.config({
    baseUrl: require_baseUrl_override,
    shim:{
        'js/plugins/standard_fonts_metrics':{
            deps:[
	            'js/jspdf'
            ]
        },

        'js/plugins/split_text_to_size':{
            deps:[
	            'js/jspdf'
            ]
        },

        'js/plugins/annotations' : {
        	deps:[
            'js/jspdf',
            'js/plugins/standard_fonts_metrics',
            'js/plugins/split_text_to_size'
            ]
        },

        'js/plugins/outline':{
            deps:[
	            'js/jspdf'
            ]
        },

        'js/plugins/addimage':{
            deps:[
	            'js/jspdf'
            ]
        },

        'js/plugins/png_support':{
            deps:[
	            'js/jspdf',
	            'js/libs/png_support/png',
	            'js/libs/png_support/zlib'
            ]
        },

        'js/plugins/from_html':{
            deps:[
	            'js/jspdf'
            ]
        },

        'js/plugins/context2d':{
            deps:[
	            'js/jspdf',
	            'js/plugins/png_support',
	            'js/plugins/addimage',
	            'js/libs/css_colors'
            ]
        },

        'js/libs/html2canvas/dist/html2canvas':{
            deps:[
	            'js/jspdf'
            ]
        },

        'js/plugins/canvas' : {
            deps:[
	            'js/jspdf'
            ]
        },

        'html2pdf' : {
        	deps:[
            'js/jspdf',
            'js/plugins/standard_fonts_metrics',
            'js/plugins/split_text_to_size',
            'js/plugins/png_support',
            'js/plugins/context2d',
            'js/plugins/canvas',
            'js/plugins/annotations',

            'js/libs/html2canvas/dist/html2canvas'
            ]
        },

        'test_harness':{
            deps:[
	            'js/jspdf',
	            'js/standard_fonts_metrics',
	            'js/split_text_to_size'
            ]
        }
     },
     paths:{
    	 'js/libs/html2pdf': 'js/libs/html2pdf'
     }
});
} // Require.JS
