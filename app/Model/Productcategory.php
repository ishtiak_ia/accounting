<?php
App::uses('AppModel', 'Model');
class Productcategory extends AppModel {
	public $name = 'Productcategory';
	public $usetables = 'productcategories';

	var $virtualFields = array(
		'productcategory_name' => 'CONCAT(Productcategory.productcategoryname, " / ", Productcategory.productcategorynamebn)',
		'isActive' => 'IF(Productcategory.productcategoryisactive = 0, "<span class=\"button btn btn-sm btn-warning\"><span class=\"glyphicon glyphicon-remove-circle\" title=\"Inactive\"></span></span>", IF(Productcategory.productcategoryisactive = 1, "<span class=\"button btn btn-sm btn-success\"><span class=\"glyphicon glyphicon-ok-circle\" title=\"Active\"></span></span>", "<span class=\"button btn btn-sm btn-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span></span>"))'
	);
}