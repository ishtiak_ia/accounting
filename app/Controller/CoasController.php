<?php
App::import('Controller', 'Logins');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');
class CoasController extends AppController {
	public $name = 'Coas';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'Form' => array('className' => 'BootstrapForm'));
	public $components = array('RequestHandler','Session');
	public $uses = array('Coatype','Coagroup','Coaclass','Coaclasssubclass', 'Coa','Company','Branch','User');

	var $Logins;
	/*public function index() {
		$this->redirect("login");
	}*/
	function beforeFilter(){
		$this->Logins =& new LoginsController;
		$this->Logins->constructClasses();
		$this->Logins->__validateLoginStatus();
		//$this->recordActivity();
		//$this->Auth->allow('register');
		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	public  function beforeRender(){
		$this->response->disableCache();//prevent useless warnings for Ajax
		if($this->RequestHandler->isAjax()){
			Configure::write('debug', 0);
		}
	}

	/*public function __validateLoginStatus() {
		if ($this->action != 'login' && $this->action != 'logout') {
			if ($this->Session->check('User') == false) {
				$this->redirect("login");
				$this->Session->setFlash('The URL you\'ve followed requires you login.');
			}
		}
	}*/
    	//Start Chart of Account Type//

	public function coatypemanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Chart of Account Type').' | '.__(Configure::read('site_name')));

		$coa_id = array('');
		$coa_id[] = array('Coatype.coatypeisactive IN (0,1)');
		$this->paginate = array(
			'fields' => 'Coatype.*',
			'conditions' => $coa_id,
			'order'  =>'Coatype.coatypename ASC',
			'limit' => 20
		);
		$coatype = $this->paginate('Coatype');
		$this->set('coatype',$coatype);
	}

	public function coatypedetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$coatype=$this->Coatype->find('all',array("fields" => "Coatype.*","conditions"=>array('Coatype.id'=>$id,'Coatype.coatypeuuid'=>$uuid)));
		$this->set('coatype',$coatype);
	}

	public function coatypesearchbytext(){
		$this->layout='ajax';
		$coatype_id = array();
		$Searchtext = @$this->request->data['Searchtext'];
		if(!empty($Searchtext) && $Searchtext!=''):
			$coatype_id[] = array(
				'OR' => array(
					'Coatype.coatypename LIKE ' => '%'.$Searchtext.'%',
					'Coatype.coatypenamebn LIKE ' => '%'.$Searchtext.'%',
					'Coatype.coatypecode LIKE ' => '%'.$Searchtext.'%'
				)
			);
		else:
			$coatype_id[] = array();
		endif;
		$coatype_id[] = array('Coatype.coatypeisactive IN (0,1)');
		$this->paginate = array(
			'fields' => 'Coatype.*',
			"conditions"=>$coatype_id,
			'order'  =>'Coatype.coatypename ASC',
			'limit' => 20
		);
		$coatype = $this->paginate('Coatype');
		$this->set('coatype',$coatype);
	}

	public function coatypeinsertupdate($id=null,$uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Chart of Account Type').' | '.__(Configure::read('site_name')));
		if(!empty($id) && $id!=0):
			$coatype=$this->Coatype->find('all',array("fields" => "Coatype.*","conditions"=>array('Coatype.id'=>$id,'Coatype.coatypeuuid'=>$uuid)));
			$this->set('coatype',$coatype);
		endif;
	}

	public function  coatypeinsertupdateaction($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		$id = @$this->request->data['Coas']['id'];
		$uuid = @$this->request->data['Coas']['coatypeuuid'];
		$coatypecode = $this->request->data['Coatype']['coatypecode'];
		$coatypename = $this->request->data['Coatype']['coatypename'];
		$coatypenamebn = $this->request->data['Coatype']['coatypenamebn'];
		$coatypecode = $this->request->data['Coatype']['coatypecode'];
		$countcoatypename=$this->Coatype->find('all',array("fields" => "Coatype.*","conditions"=>array('Coatype.coatypename'=>$coatypename,'Coatype.coatypenamebn'=>$coatypenamebn, 'Coatype.coatypecode'=>$coatypecode)));
		//echo count($countbankaccount);
		//pr($_POST);exit();
		if(count($countcoatypename)>0):
			$this->Session->setFlash(__("This COA Type Already Exists"), 'default', array('class' => 'alert alert-warning'));
			$this->redirect("/coas/coatypeinsertupdate");
		else:
			if ($this->request->is('post')) {
				if(!empty($id) && $id!=0):
					$this->request->data['Coatype']['coatypeupdateid']=$userID;
					$this->request->data['Coatype']['coatypeupdatedate']=date('Y-m-d H:i:s');
					$message = __('Update Successfully.');
					$this->Coatype->id = $id;
				else:
					$this->request->data['Coatype']['coatypeinsertid']=$userID;
					$this->request->data['Coatype']['coatypeinsertdate']=date('Y-m-d H:i:s');
					$this->request->data['Coatype']['coatypeuuid']=String::uuid();
					$message = __('Save Successfully.');
				endif;
				if ($this->Coatype->save($this->request->data, array('validate' => 'only'))) {
					$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
					$this->redirect("/coas/coatypemanage");
				}else{
					$errors = $this->Coatype->invalidFields();
					$this->set('alert alert-danger', $this->Coatype->invalidFields());
					$this->redirect("/coas/coatypeinsertupdate");
				}
			}
		endif;
	}

	public function coatypedelete($id=null,$uuid=null){
		$this->layout='ajax';
		$userID = $this->Session->read('User.id');
		if ($this->Coatype->updateAll(array('coatypedeleteid'=>$userID, 'coatypedeletedate'=>"'".date('Y-m-d H:i:s')."'", 'coatypeisactive'=>2), array('AND' => array('Coatype.id'=>$id,'Coatype.coatypeuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/coas/coatypemanage");
	}

	//End Chart of Account Type//

	//Start Chart of Account Group//
	public function coagroupmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Chart of Account Group').' | '.__(Configure::read('site_name')));

		$coatype=$this->Coatype->find('list',array("fields" => array("Coatype.id","coatype_name")));
		$this->set('coatype',$coatype);

		$coagrouplist=$this->Coagroup->find('list',array("fields" => array("Coagroup.id","coagroup_name")));
		$this->set('coagrouplist',$coagrouplist);

		$coatype_id = array('');
		$coatype_id[] = array('Coagroup.coagroupisactive IN (0,1,2)');
		$this->paginate = array(
			'fields' => 'Coagroup.*,Coatype.*',
			'conditions'  =>$coatype_id,
			'order'  =>'Coagroup.coatype_id ASC',
			'limit' => 20
		);
		$coagroup = $this->paginate('Coagroup');
		$this->set('coagroup',$coagroup);
	}

	public function coagroupsearchbycoatype($typeid=null, $groupid=null, $groupname=null, $groupcode=null){
		$this->layout='ajax';
		$coatype_id = array();
		if(!empty($typeid) && $typeid!=0):
			$coatype_id[] = array('Coagroup.coatype_id' => $typeid);
		else:
			$coatype_id[] = array();
		endif;
		if(!empty($groupid) && $groupid!=0):
			$coatype_id[] = array('Coagroup.id' => $groupid);
		else:
			$coatype_id[] = array();
		endif;
		if(!empty($groupname) && $groupname!=''):
			$coatype_id[] = array(
				'OR' => array(
					'Coagroup.coagroupname LIKE ' => '%'.$groupname.'%',
					'Coagroup.coagroupnamebn LIKE ' => '%'.$groupname.'%',
					'Coagroup.coagroupcode LIKE ' => '%'.$groupcode.'%',
					'Coatype.coatypename LIKE ' => '%'.$groupname.'%',
					'Coatype.coatypenamebn LIKE ' => '%'.$groupname.'%'
				)
			);
		endif;
		$coatype_id[] = array('Coagroup.coagroupisactive IN (0,1)');
		$this->paginate = array(
			'fields' => 'Coagroup.*,Coatype.*',
			"conditions"=>$coatype_id,
			'order'  =>'Coagroup.coagroupname ASC',
			'limit' => 20
		);
		$coagroup = $this->paginate('Coagroup');
		$this->set('coagroup',$coagroup);
	}

	public function coagroupdetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$coagroup=$this->Coagroup->find('all',array("fields" => "Coagroup.*,Coatype.*","conditions"=>array('Coagroup.id'=>$id,'Coagroup.coagroupuuid'=>$uuid)));
		$this->set('coagroup',$coagroup);
	}

	public function coagroupinsertupdate($id=null,$uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Chart of Account Group').' | '.__(Configure::read('site_name')));
		$coatype=$this->Coatype->find('list',array("fields" => array("Coatype.id","coatype_name")));
		$this->set('coatype',$coatype);
		$coa_type_code = $this->Coatype->find('list',array("fields" => array("Coatype.id","Coatype.coatypecode")));
		$this->set('coatype_code',$coa_type_code);
		if(!empty($id) && $id!=0):
			$coagroup=$this->Coagroup->find('all',array("fields" => "Coagroup.*","conditions"=>array('Coagroup.id'=>$id,'Coagroup.coagroupuuid'=>$uuid)));
			$this->set('coagroup',$coagroup);
		endif;
	}

	public function  coagroupinsertupdateaction($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		$id = @$this->request->data['Coas']['id'];
		$uuid = @$this->request->data['Coas']['coagroupuuid'];
		$this->request->data['Coagroup']['coatype_id'] = $this->request->data['Coagroup']['coatype'];
		$coagroupname = $this->request->data['Coagroup']['coagroupname'];
		$coagroupnamebn = $this->request->data['Coagroup']['coagroupnamebn'];
		$coagroupcode = $this->request->data['Coagroup']['coagroupcode'];
		$countcoagroupname=$this->Coagroup->find('all',array("fields" => "Coagroup.*","conditions"=>array('Coagroup.coatype_id'=>$this->request->data['Coagroup']['coatype_id'] ,'Coagroup.coagroupname'=>$coagroupname,'Coagroup.coagroupnamebn'=>$coagroupnamebn, 'Coagroup.coagroupcode'=>$coagroupcode)));
		//echo count($countbankaccount);
		//pr($_POST);exit();
		if(count($countcoagroupname)>0):
			$this->Session->setFlash(__("This COA Group Already Exists"), 'default', array('class' => 'alert alert-warning'));
			$this->redirect("/coas/coagroupinsertupdate");
		else:
			if ($this->request->is('post')) {
				if(!empty($id) && $id!=0):
					$this->request->data['Coagroup']['coagroupupdateid']=$userID;
					$this->request->data['Coagroup']['coagroupupdatedate']=date('Y-m-d H:i:s');
					$message = __('Update Successfully.');
					$this->Coagroup->id = $id;
				else:
					$this->request->data['Coagroup']['coagroupinsertid']=$userID;
					$this->request->data['Coagroup']['coagroupinsertdate']=date('Y-m-d H:i:s');
					$this->request->data['Coagroup']['coagroupuuid']=String::uuid();
					$message = __('Save Successfully.');
				endif;
				if ($this->Coagroup->save($this->data)) {
					$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				}
			}
			$this->redirect("/coas/coagroupmanage");
		endif;
	}

	public function coagroupdelete($id=null,$uuid=null){
		$this->layout='ajax';
		$userID = $this->Session->read('User.id');
		if ($this->Coagroup->updateAll(array('coagroupdeleteid'=>$userID, 'coagroupdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'coagroupisactive'=>2), array('AND' => array('Coagroup.id'=>$id,'Coagroup.coagroupuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/coas/coagroupmanage");
	}

	//End Chart of Account Group//


	//Start Chart of Account Class//
	public function coaclassmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Chart of Account Class').' | '.__(Configure::read('site_name')));

		$coatype=$this->Coatype->find('list',array("fields" => array("Coatype.id","coatype_name")));
		$this->set('coatype',$coatype);

		$coagrouplist=$this->Coagroup->find('list',array("fields" => array("Coagroup.id","coagroup_name")));
		$this->set('coagrouplist',$coagrouplist);

		$coaclasslist=$this->Coaclass->find('list',array("fields" => array("Coaclass.id","coaclass_name")));
		$this->set('coaclasslist',$coaclasslist);

		$coagroup_id = array('');
		$coagroup_id[] = array('Coaclass.coaclassisactive IN (0,1)');
		$this->paginate = array(
			'fields' => 'Coaclass.*,Coagroup.*,Coatype.*',
			'conditions'  =>$coagroup_id,
			'order'  =>'Coaclass.coaclasscode ASC',
			'limit' => 20
		);
		$coaclass = $this->paginate('Coaclass');
		$this->set('coaclass',$coaclass);
	}

	public function coaclasssearchbycoatype($typeid=null, $groupid=null, $classid=null, $classname=null){
		$this->layout='ajax';
		$coatype_id = array();
		$coagroup_id = array();
		$coaclass_id = array();
		if(!empty($typeid) && $typeid!=0):
			$coatype_id[] = array('Coaclass.coatype_id' => $typeid);
		endif;
		if(!empty($groupid) && $groupid!=0):
			$coatype_id[] = array('Coaclass.coagroup_id' => $groupid);
		endif;
		if(!empty($classid) && $classid!=0):
			$coatype_id[] = array('Coaclass.id' => $classid);
		endif;
		if(!empty($classname) && $classname!=''):
			$coatype_id[] = array(
				'OR' => array(
					'Coaclass.coaclassname LIKE ' => '%'.$classname.'%',
					'Coaclass.coaclassnamebn LIKE ' => '%'.$classname.'%',
					'Coagroup.coagroupname LIKE ' => '%'.$classname.'%',
					'Coagroup.coagroupnamebn LIKE ' => '%'.$classname.'%',
					'Coatype.coatypename LIKE ' => '%'.$classname.'%',
					'Coatype.coatypenamebn LIKE ' => '%'.$classname.'%'
				)
			);
		endif;
		$coatype_id[] = array('Coaclass.coaclassisactive IN (0,1)');
		$this->paginate = array(
			'fields' => 'Coaclass.*,Coagroup.*,Coatype.*',
			"conditions"=>$coatype_id,
			'order'  =>'Coaclass.coaclassname ASC',
			'limit' => 20
		);
		$coaclass = $this->paginate('Coaclass');
		$this->set('coaclass',$coaclass);
	}

	public function coaclassinsertupdate($id=null,$uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Chart of Account Class').' | '.__(Configure::read('site_name')));

		$coatype=$this->Coatype->find('list',array("fields" => array("Coatype.id","coatype_name"),"conditions"=>array('Coatype.coatypeisactive'=>1)));
		$this->set('coatype',$coatype);

		$coa_type_code = $this->Coatype->find('list',array("fields" => array("Coatype.id","Coatype.coatypecode")));
		$this->set('coatype_code',$coa_type_code);

		$coagroup=$this->Coagroup->find('list',array("fields" => array("Coagroup.id","coagroup_name"),"conditions"=>array('Coagroup.coagroupisactive'=>1)));
		$this->set('coagroup',$coagroup);

		$coagroup_code=$this->Coagroup->find('list',array("fields" => array("Coagroup.id","coagroupcode"),"conditions"=>array('Coagroup.coagroupisactive'=>1)));
		$this->set('coagroup_code',$coagroup_code);

		if(!empty($id) && $id!=0):
			$coaclass=$this->Coaclass->find('all',array("fields" => "Coaclass.*","conditions"=>array('Coaclass.id'=>$id,'Coaclass.coaclassuuid'=>$uuid)));
			$this->set('coaclass',$coaclass);
		endif;
	}

	public function  coaclassinsertupdateaction($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		$id = @$this->request->data['Coas']['id'];
		$uuid = @$this->request->data['Coaclass']['coagroupuuid'];
		$coatype_id = $this->request->data['Coaclass']['coatype_id'];
		$coagroup_id = $this->request->data['Coaclass']['coagroup_id'];
		$coaclassname = $this->request->data['Coaclass']['coaclassname'];
		$coaclassnamebn = $this->request->data['Coaclass']['coaclassnamebn'];
		$coaclasscode = $this->request->data['Coaclass']['coaclasscode'];
		$countcoaclassname=$this->Coaclass->find('all',array("fields" => "Coaclass.*","conditions"=>array('Coaclass.coatype_id'=>$coatype_id,'Coaclass.coagroup_id'=>$coagroup_id,'Coaclass.coaclassname'=>$coaclassname,'Coaclass.coaclassnamebn'=>$coaclassnamebn, 'Coaclass.coaclassnamebn'=>$coaclasscode)));
		//echo count($countcoaclassname);
		//pr($_POST);exit();
		if(count($countcoaclassname)>0):
			$this->Session->setFlash(__("This COA Class Already Exists"), 'default', array('class' => 'alert alert-warning'));
			$this->redirect("/coas/coaclassinsertupdate");
		else:
			if ($this->request->is('post')) {
				if(!empty($id) && $id!=0):
					$this->request->data['Coaclass']['coaclassupdateid']=$userID;
					$this->request->data['Coaclass']['coaclassupdatedate']=date('Y-m-d H:i:s');
					$message = __('Update Successfully.');
					$this->Coaclass->id = $id;
				else:
					$this->request->data['Coaclass']['coaclassinsertid']=$userID;
					$this->request->data['Coaclass']['coaclassinsertdate']=date('Y-m-d H:i:s');
					$this->request->data['Coaclass']['coaclassuuid']=String::uuid();
					$message = __('Save Successfully.');
				endif;
				if ($this->Coaclass->save($this->data)) {
					$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				}
			}
			$this->redirect("/coas/coaclassmanage");
		endif;
	}

	public function coaclassdelete($id=null,$uuid=null){
		$this->layout='ajax';
		$userID = $this->Session->read('User.id');
		if ($this->Coaclass->updateAll(array('coaclassdeleteid'=>$userID, 'coaclassdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'coaclassisactive'=>2), array('AND' => array('Coaclass.id'=>$id,'Coaclass.coaclassuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/coas/coaclassmanage");
	}

	public function coaclassdetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$coaclass=$this->Coaclass->find('all',array("fields" => "Coaclass.*,Coagroup.*, Coatype.*","conditions"=>array('Coaclass.id'=>$id,'Coaclass.coaclassuuid'=>$uuid)));
		$this->set('coaclass',$coaclass);
	}

	//End Chart of Account Class//


	//Start Chart of Account SubClass//
	public function coaclasssubclassmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Chart of Account Sub Class').' | '.__(Configure::read('site_name')));

		$coatype=$this->Coatype->find('list',array("fields" => array("Coatype.id","coatype_name")));
		$this->set('coatype',$coatype);

		$coagrouplist=$this->Coagroup->find('list',array("fields" => array("Coagroup.id","coagroup_name")));
		$this->set('coagrouplist',$coagrouplist);

		$coaclasslist=$this->Coaclass->find('list',array("fields" => array("Coaclass.id","coaclass_name")));
		$this->set('coaclasslist',$coaclasslist);

		$coaclasssubclasslist=$this->Coaclasssubclass->find('list',array("fields" => array("Coaclasssubclass.id","coaclasssubclass_name")));
		$this->set('coaclasssubclasslist',$coaclasssubclasslist);

		$coaclass_id = array('');
		if($_SESSION["User"]["group_id"] == 1)
			$coaclass_id[] = array('Coaclasssubclass.coaclasssubclassisactive IN (0,1,2)');
		else if($_SESSION["User"]["group_id"] == 2)
			$coaclass_id[] = array('Coaclasssubclass.coaclasssubclassisactive IN (0,1)');
		else 
			$coaclass_id[] = array('Coaclasssubclass.coaclasssubclassisactive IN (1)');

		$this->paginate = array(
			'fields' => 'Coaclasssubclass.*, Coaclass.*,Coagroup.*,Coatype.*',
			'conditions'  =>$coaclass_id,
			'order'  =>'Coaclasssubclass.coaclass_id ASC',
			'limit' => 20
		);
		$coaclasssubclass = $this->paginate('Coaclasssubclass');
		$this->set('coaclasssubclass',$coaclasssubclass);
	}

	public function coaclasssubclasssearchbytext($typeid=null, $groupid=null, $classid=null, $subclassid=null, $subclassname=null, $subclasscode=null){
		$this->layout='ajax';
		$coatype_id = array();
		$coagroup_id = array();
		$coaclass_id = array();
		$coaclasssubclass_id = array();
		if(!empty($typeid) && $typeid!=0):
			$coatype_id[] = array('Coaclasssubclass.coatype_id' => $typeid);
		endif;
		if(!empty($groupid) && $groupid!=0):
			$coatype_id[] = array('Coaclasssubclass.coagroup_id' => $groupid);
		endif;
		if(!empty($classid) && $classid!=0):
			$coatype_id[] = array('Coaclasssubclass.coaclass_id' => $classid);
		endif;
		if(!empty($subclassid) && $subclassid!=0):
			$coatype_id[] = array('Coaclasssubclass.id' => $subclassid);
		endif;
		if(!empty($subclasscode) && $subclasscode!=''):
			$coatype_id[] = array(
				'OR' => array(
					'Coaclasssubclass.coaclasssubclassname LIKE ' => '%'.$subclassname.'%',
					'Coaclasssubclass.coaclasssubclasscode LIKE ' => '%'.$subclasscode.'%'
				)
			);
		endif;
		$coatype_id[] = array('Coaclasssubclass.coaclasssubclassisactive IN (0,1)');
		$this->paginate = array(
			'fields' => 'Coaclasssubclass.*,Coaclass.*,Coagroup.*,Coatype.*',
			"conditions"=>$coatype_id,
			'order'  =>'Coaclasssubclass.coaclasssubclassname ASC',
			'limit' => 20
		);
		$coaclasssubclass = $this->paginate('Coaclasssubclass');
		$this->set('coaclasssubclass',$coaclasssubclass);
	}

	public function coaclasssubclassinsertupdate($id=null,$uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Chart of Account Sub Class').' | '.__(Configure::read('site_name')));

        //coa type start
		$coatype=$this->Coatype->find('list',array("fields" => array("Coatype.id","coatype_name"),"conditions"=>array('Coatype.coatypeisactive'=>1)));
		$this->set('coatype',$coatype);
		$coatype_code=$this->Coatype->find('list',array("fields" => array("Coatype.id","coatypecode"),"conditions"=>array('Coatype.coatypeisactive'=>1)));
		$this->set('coatype_code',$coatype_code);
		//coa type end

		//coa group start
		$coagroup=$this->Coagroup->find('list',array("fields" => array("Coagroup.id","coagroup_name"),"conditions"=>array('Coagroup.coagroupisactive'=>1)));
		$this->set('coagroup',$coagroup);
		$coagroup_code=$this->Coagroup->find('list',array("fields" => array("Coagroup.id","coagroupcode"),"conditions"=>array('Coagroup.coagroupisactive'=>1)));
		$this->set('coagroup_code',$coagroup_code);
		//coa group end

		//coa class start
		$coaclass=$this->Coaclass->find('list',array("fields" => array("Coaclass.id","coaclass_name"),"conditions"=>array('Coaclass.coaclassisactive'=>1)));
		$this->set('coaclass',$coaclass);
		$coaclass_code=$this->Coaclass->find('list',array("fields" => array("Coaclass.id","coaclasscode"),"conditions"=>array('Coaclass.coaclassisactive'=>1)));
		$this->set('coaclass_code',$coaclass_code);
		//coa class end

		if(!empty($id) && $id!=0):
			$coaclasssubclass=$this->Coaclasssubclass->find('all',array("fields" => "Coaclasssubclass.*","conditions"=>array('Coaclasssubclass.id'=>$id,'Coaclasssubclass.coaclasssubclassuuid'=>$uuid)));
			$this->set('coaclasssubclass',$coaclasssubclass);
		endif;
	}
	public function  coaclasssubclassinsertupdateaction($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		$id = @$this->request->data['Coas']['id'];
		$uuid = @$this->request->data['Coaclasssubclass']['coaclasssubclassuuid'];
		$coatype_id = $this->request->data['Coaclasssubclass']['coatype_id'];
		$coagroup_id = $this->request->data['Coaclasssubclass']['coagroup_id'];
		$coaclass_id = $this->request->data['Coaclasssubclass']['coaclass_id'];
		$coaclasssubclassname = $this->request->data['Coaclasssubclass']['coaclasssubclassname'];
		$coaclasssubclassnamebn = $this->request->data['Coaclasssubclass']['coaclasssubclassnamebn'];
		$coaclasssubclasscode = $this->request->data['Coaclasssubclass']['coaclasssubclasscode'];
		$countcoaclasssubclassname=$this->Coaclasssubclass->find(
			'all',
			array(
				"fields" => "Coaclasssubclass.*",
				"conditions"=>array(
					'Coaclasssubclass.coatype_id'=>$coatype_id,
					'Coaclasssubclass.coagroup_id'=>$coagroup_id,
					'Coaclasssubclass.coaclass_id'=>$coaclass_id,
					'Coaclasssubclass.coaclasssubclassname'=>$coaclasssubclassname,
					'Coaclasssubclass.coaclasssubclassnamebn'=>$coaclasssubclassnamebn,
					'Coaclasssubclass.coaclasssubclasscode'=>$coaclasssubclasscode
				)
			)
		);
		//echo count($countcoaclassname);
		//pr($_POST);exit();
		if(count($countcoaclasssubclassname)>0):
			$this->Session->setFlash(__("This COA Sub Class Already Exists"), 'default', array('class' => 'alert alert-warning'));
			$this->redirect("/coas/coaclasssubclassinsertupdate");
		else:
			if ($this->request->is('post')) {
				if(!empty($id) && $id!=0):
					$this->request->data['Coaclasssubclass']['coaclasssubclassupdateid']=$userID;
					$this->request->data['Coaclasssubclass']['coaclasssubclassupdatedate']=date('Y-m-d H:i:s');
					$message = __('Update Successfully.');
					$this->Coaclasssubclass->id = $id;
				else:
					$this->request->data['Coaclasssubclass']['coaclasssubclassinsertid']=$userID;
					$this->request->data['Coaclasssubclass']['class']=date('Y-m-d H:i:s');
					$this->request->data['Coaclasssubclass']['coaclasssubclassuuid']=String::uuid();
					$message = __('Save Successfully.');
				endif;
				if ($this->Coaclasssubclass->save($this->data)) {
					$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				}
			}
			$this->redirect("/coas/coaclasssubclassmanage");
		endif;
	}


	public function coaclasssubclassdelete($id=null,$uuid=null){
		$this->layout='ajax';
		$userID = $this->Session->read('User.id');
		if ($this->Coaclasssubclass->updateAll(array('coaclasssubclassdeleteid'=>$userID, 'coaclasssubclassdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'coaclasssubclassisactive'=>2), array('AND' => array('Coaclasssubclass.id'=>$id,'Coaclasssubclass.coaclasssubclassuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/coas/coaclasssubclassmanage");
	}

	public function coaclasssubclassdetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$coaclasssubclass=$this->Coaclasssubclass->find('all',array("fields" => "Coaclasssubclass.*,Coaclass.*,Coagroup.*, Coatype.*","conditions"=>array('Coaclasssubclass.id'=>$id,'Coaclasssubclass.coaclasssubclassuuid'=>$uuid)));
		$this->set('coaclasssubclass',$coaclasssubclass);
	}

	//End Chart of Account Subclass//

	//Start Chart of Account//
	public function coamanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Chart of Account').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$coa_id = array();
		$companyid = array();
		$branchid = array();
		if($group_id==1):
		elseif($group_id==2):
			$companyid[] = array('Company.id'=>$company_id);
			$coa_id[] = array('Coa.coaisactive IN (0,1)');
		else:
			$companyid[] = array('Company.id'=>$company_id);
			$branchid[] = array('Branch.id'=>$branch_id);
			$coa_id[] = array('Coa.coaisactive IN (1)');
		endif;
		$company=$this->Company->find('list',array("fields" => array("Company.id","company_name"),"conditions"=>array('Company.companyisactive'=>1)));
		$this->set('company',$company);

		$branch=$this->Branch->find(
			'list',
			array(
				"fields" => array("Branch.id","branch_name"),
				"recursive" =>1,
				"conditions" => array($branchid,$companyid,array('Branch.branchisactive'=>1))
			)
		);
		$this->set('branch',$branch);

		$coatype=$this->Coatype->find('list',array("fields" => array("Coatype.id","coatype_name"),"conditions"=>array('Coatype.coatypeisactive'=>1)));
		$this->set('coatype',$coatype);

		$coagrouplist=$this->Coagroup->find('list',array("fields" => array("Coagroup.id","coagroup_name"),"conditions"=>array('Coagroup.coagroupisactive'=>1)));
		$this->set('coagrouplist',$coagrouplist);

		$this->paginate = array(
			'fields' => array('Coa.*','Coagroup.*','Coatype.*','Company.*','Branch.*'),
			'conditions' => array($coa_id,$branchid,$companyid),
			'order'  =>'Coatype.coatypecode ASC,Coagroup.coagroupcode ASC,Coaclass.coaclasscode ASC,Coaclasssubclass.coaclasssubclasscode ASC,Coa.coacode ASC',
			'limit' => 20
		);
		$coa = $this->paginate('Coa');
		$this->set('coa',$coa);
	}

	public function coadetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$coa=$this->Coa->find('all',array("fields" => 'Coa.*','Coagroup.*','Coatype.*','Company.*','Branch.*',"conditions"=>array('Coa.id'=>$id,'Coa.coauuid'=>$uuid)));
		$this->set('coa',$coa);
	}

	public function coasearchbytgcb($typeid=null, $groupid=null, $companyid=null, $branchid=null, $coaname=null){
		$this->layout='ajax';
		$coatype_id = array();
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		if($group_id==1):
			$coatype_id[] = array('');
		elseif($group_id==2):
			$coatype_id[] = array('Coa.company_id'=>$company_id);
		else:
			$coatype_id[] = array('Coa.company_id'=>$company_id);
			$coatype_id[] = array('Coa.branch_id'=>$branch_id);
		endif;
		if(!empty($companyid) && $companyid!=0):
			$coatype_id[] = array('Coa.company_id' => $companyid);
		else:
			$coatype_id[] = array();
		endif;
		if(!empty($branchid) && $branchid!=0):
			$coatype_id[] = array('Coa.branch_id' => $branchid);
		else:
			$coatype_id[] = array();
		endif;
		if(!empty($typeid) && $typeid!=0):
			$coatype_id[] = array('Coa.coatype_id' => $typeid);
		else:
			$coatype_id[] = array();
		endif;
		if(!empty($groupid) && $groupid!=0):
			$coatype_id[] = array('Coa.coagroup_id' => $groupid);
		else:
			$coatype_id[] = array();
		endif;
		$coatype_id[] = array('Coa.coaisactive IN (0,1)');
		if(!empty($coaname) && $coaname!=''):
			$coatype_id[] = array(
				'OR' => array(
					'Coa.coacode LIKE ' => '%'.$coaname.'%',
					'Coa.coaname LIKE ' => '%'.$coaname.'%',
					'Coa.coanamebn LIKE ' => '%'.$coaname.'%',
					'Coagroup.coagroupname LIKE ' => '%'.$coaname.'%',
					'Coagroup.coagroupnamebn LIKE ' => '%'.$coaname.'%',
					'Coatype.coatypename LIKE ' => '%'.$coaname.'%',
					'Coatype.coatypenamebn LIKE ' => '%'.$coaname.'%',
					'Company.companyname LIKE ' => '%'.$coaname.'%',
					'Company.companynamebn LIKE ' => '%'.$coaname.'%',
					'Branch.branchname LIKE ' => '%'.$coaname.'%',
					'Branch.branchnamebn LIKE ' => '%'.$coaname.'%'
				)
			);
		else:
			$coatype_id[] = array();
		endif;
		$this->paginate = array(
			'fields' => array('Coa.*','Coagroup.*','Coatype.*','Company.*','Branch.*'),
			"conditions"=>$coatype_id,
			'order'  =>'Coatype.coatypecode ASC,Coagroup.coagroupcode ASC,Coaclass.coaclasscode ASC,Coaclasssubclass.coaclasssubclasscode ASC,Coa.coacode ASC',
			'limit' => 20
		);
		$coa = $this->paginate('Coa');
		$this->set('coa',$coa);
	}

	public function coasearchbytgcblistview($typeid=null, $groupid=null, $companyid=null, $branchid=null, $coaname=null){
		$this->layout = 'ajax';
		//$this->beforeRender();
		//$this->autoRender = false;
		$coatype_id = array();
		$Coagroup_id=array('');
		$Coaclass_id=array();
		$Coaclasssubclass_id=array();
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];
		
		if($group_id==1):
			$coatype_id[] = array('');
		elseif($group_id==2):
			$coatype_id[] = array('Coa.company_id'=>$company_id);
		else:
			$coatype_id[] = array('Coa.company_id'=>$company_id);
			$coatype_id[] = array('Coa.branch_id'=>$branch_id);
		endif;
		if(!empty($companyid) && $companyid!=0):
			$coatype_id[] = array('Coa.company_id' => $companyid);
		else:
			$coatype_id[] = array();
		endif;
		if(!empty($branchid) && $branchid!=0):
			$coatype_id[] = array('Coa.branch_id' => $branchid);
		else:
			$coatype_id[] = array();
		endif;
		if(!empty($typeid) && $typeid!=0):
			$coatype_id[] = array('Coa.coatype_id' => $typeid);
		else:
			$coatype_id[] = array();
		endif;
		if(!empty($groupid) && $groupid!=0):
			$coatype_id[] = array('Coa.coagroup_id' => $groupid);
		else:
			$coatype_id[] = array();
		endif;
		//$coatype_id[] = array('Coa.coaisactive IN (0,1)');
		if(!empty($coaname) && $coaname!=''):
			$coatype_id[] = array(
				'OR' => array(
					'Coa.coacode LIKE ' => '%'.$coaname.'%',
					'Coa.coaname LIKE ' => '%'.$coaname.'%',
					'Coa.coanamebn LIKE ' => '%'.$coaname.'%',
					'Coagroup.coagroupname LIKE ' => '%'.$coaname.'%',
					'Coagroup.coagroupnamebn LIKE ' => '%'.$coaname.'%',
					'Coatype.coatypename LIKE ' => '%'.$coaname.'%',
					'Coatype.coatypenamebn LIKE ' => '%'.$coaname.'%',
					'Company.companyname LIKE ' => '%'.$coaname.'%',
					'Company.companynamebn LIKE ' => '%'.$coaname.'%',
					'Branch.branchname LIKE ' => '%'.$coaname.'%',
					'Branch.branchnamebn LIKE ' => '%'.$coaname.'%'
				)
			);
		else:
			$coatype_id[] = array();
		endif;
		$displaytable='';
		$Coatype=$this->Coatype->find(
			'all',array(
				"fields" => array(
					'Coatype.*'
				),
				//"conditions"=>$coatype_id,
				"order"=>array('Coatype.coatypecode ASC')
			)
		);
		$this->set('Coatype',$Coatype);
		foreach($Coatype as $ThisCoatype):
			$displaytable.= "<tr><td>".$ThisCoatype["Coatype"]["coatypecode"]."</td><td>".$ThisCoatype["Coatype"]["coatypename"]."</td></tr>";
			$Coagroup_id = array('Coagroup.coatype_id'=>$ThisCoatype["Coatype"]["id"]);
			$Coagroup=$this->Coagroup->find(
				'all',array(
					"fields" => array(
						'Coagroup.*'
					),
					"conditions"=>array('Coagroup.coatype_id'=>$ThisCoatype["Coatype"]["id"]),
					"order"=>array('Coagroup.coagroupcode ASC')
				)
			);
			$this->set('Coagroup',$Coagroup);
			foreach($Coagroup as $ThisCoagroup):
				$displaytable.= "<tr><td>".$ThisCoagroup["Coagroup"]["coagroupcode"]."</td><td>".$ThisCoagroup["Coagroup"]["coagroupname"]."</td></tr>";
				$Coaclass_id[] = array('Coaclass.coagroup_id'=>$ThisCoagroup["Coagroup"]["id"]);
				$Coaclass=$this->Coaclass->find(
					'all',array(
						"fields" => array(       
							'Coaclass.id',
							'Coaclass.coaclasscode',
							'Coaclass.coaclassname'
						),
						"conditions"=> array('Coaclass.coagroup_id'=>$ThisCoagroup["Coagroup"]["id"]),
						"order"=>array('Coaclass.coaclasscode ASC')
					)
				);         
				$this->set('Coaclass',$Coaclass);
				foreach($Coaclass as $ThisCoaclass):
					$displaytable.= "<tr><td>".$ThisCoaclass["Coaclass"]["coaclasscode"]."</td><td>".$ThisCoaclass["Coaclass"]["coaclassname"]."</td></tr>";
					$Coaclasssubclass_id[] = array('Coaclasssubclass.coaclass_id'=>$ThisCoaclass["Coaclass"]["id"]);
					$Coaclasssubclass=$this->Coaclasssubclass->find(
						'all',array(
							"fields" => array(
								'Coaclasssubclass.*'
							),
							"conditions"=> array('Coaclasssubclass.coaclass_id'=>$ThisCoaclass["Coaclass"]["id"]),
							"order"=>array('Coaclasssubclass.coaclasssubclasscode ASC')
						)
					);
					
					
					$this->set('Coaclasssubclass',$Coaclasssubclass);
					foreach($Coaclasssubclass as $ThisCoaclasssubclass):
						$displaytable.= "<tr><td>".$ThisCoaclasssubclass["Coaclasssubclass"]["coaclasssubclasscode"]."</td><td>".$ThisCoaclasssubclass["Coaclasssubclass"]["coaclasssubclassname"]."</td></tr>";
						$Coa=$this->Coa->find(
							'all',array(
								"fields" => array(
									'Coa.id',
									'Coa.coacode',
									'Coa.coaname'
								),
								"conditions"=>array('Coa.coaclasssubclass_id'=>$ThisCoaclasssubclass["Coaclasssubclass"]["id"]),
								"order"=>array('Coa.coacode ASC')
							)
						);
						//$this->set('Coa',$Coa);
						foreach($Coa as $ThisCoa):
							$displaytable.= "<tr><td>".$ThisCoa["Coa"]["coacode"]."</td><td>".$ThisCoa["Coa"]["coaname"]."</td></tr>";
						endforeach;
					endforeach;
				endforeach;
			endforeach;
		endforeach;
		
		$this->set('displaytable',$displaytable);
		/*$coa=$this->Coa->find(
			'all',array(
				"fields" => array(
					'Company.*',
					'Branch.*',
					'Coatype.*',
					'Coagroup.*',
					'Coaclass.*',
					'Coaclasssubclass.*',
					'Coa.*'
				),
				"conditions"=>$coatype_id,
				"order"=>array('Coatype.coatypecode ASC,Coagroup.coagroupcode ASC,Coaclass.coaclasscode ASC,Coaclasssubclass.coaclasssubclasscode ASC,Coa.coacode ASC')
			)
		);*/		
		/*$this->paginate = array(
			'fields' => array('Coa.*','Coagroup.*','Coatype.*','Coaclass.*','Coaclasssubclass.*','Company.*','Branch.*'),
			"conditions"=>$coatype_id,
			'order'  =>'Coa.coaname ASC',
			'limit' => 20
		);
		$coa = $this->paginate('Coa');*/
		//$this->set('coa',$coa);
	}


	public function coainsertupdate($id=null,$uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Chart of Account').' | '.__(Configure::read('site_name')));

		$coatype_id = array();
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		if($group_id==1):
			$coatype_id[] = array('');
		elseif($group_id==2):
			$coatype_id[] = array('Coa.company_id'=>$company_id);
		else:
			$coatype_id[] = array('Coa.company_id'=>$company_id);
			$coatype_id[] = array('Coa.branch_id'=>$branch_id);
		endif;
		$coa_id = array();
		$companyid = array();
		$branchid = array();
		if($group_id==1):
			$coa_id[] = array('');
		elseif($group_id==2):
			$companyid[] = array('Company.id'=>$company_id);
		else:
			$companyid[] = array('Company.id'=>$company_id);
			$branchid[] = array('Branch.id'=>$branch_id);
		endif;
		$company=$this->Company->find('list',array("fields" => array("Company.id","company_name"),"conditions"=>array('Company.companyisactive'=>1)));
		$this->set('company',$company);

		$branch=$this->Branch->find(
			'list',
			array(
				"fields" => array("Branch.id","branch_name"),
				"conditions" => array($branchid,$companyid,array('Branch.branchisactive'=>1))
			)
		);
		$this->set('branch',$branch);

		//coa type start
		$coatype=$this->Coatype->find('list',array("fields" => array("Coatype.id","coatype_name"),"conditions"=>array('Coatype.coatypeisactive'=>1)));
		$this->set('coatype',$coatype);
		$coatype_code=$this->Coatype->find('list',array("fields" => array("Coatype.id","coatypecode"),"conditions"=>array('Coatype.coatypeisactive'=>1)));
		$this->set('coatype_code',$coatype_code);
		//coa type end

		//coa group start
		$coagroup=$this->Coagroup->find('list',array("fields" => array("Coagroup.id","coagroup_name"),"conditions"=>array('Coagroup.coagroupisactive'=>1)));
		$this->set('coagroup',$coagroup);
		$coagroup_code=$this->Coagroup->find('list',array("fields" => array("Coagroup.id","coagroupcode"),"conditions"=>array('Coagroup.coagroupisactive'=>1)));
		$this->set('coagroup_code',$coagroup_code);
		//coa group end

		//coa class sart
		$coaclass=$this->Coaclass->find('list',array("fields" => array("Coaclass.id","coaclass_name"),"conditions"=>array('Coaclass.coaclassisactive'=>1)));
		$this->set('coaclass',$coaclass);
		$coaclass_code=$this->Coaclass->find('list',array("fields" => array("Coaclass.id","coaclasscode"),"conditions"=>array('Coaclass.coaclassisactive'=>1)));
		$this->set('coaclass_code',$coaclass_code);
		//coa class end

		//coa subclass start
		$coaclasssubclass=$this->Coaclasssubclass->find('list',array("fields" => array("Coaclasssubclass.id","coaclasssubclass_name"),"conditions"=>array('Coaclasssubclass.coaclasssubclassisactive'=>1)));
		$this->set('coaclasssubclass',$coaclasssubclass);
		$coaclasssubclass_code=$this->Coaclasssubclass->find('list',array("fields" => array("Coaclasssubclass.id","coaclasssubclasscode"),"conditions"=>array('Coaclasssubclass.coaclasssubclassisactive'=>1)));
		$this->set('coaclasssubclass_code',$coaclasssubclass_code);
		//coa subclass end

		if(!empty($id) && $id!=0):
			$coa=$this->Coa->find('all',array("fields" => "Coa.*","conditions"=>array('Coa.id'=>$id)));
			$this->set('coa',$coa);
		endif;
	}

	public function listcoagroup($id=null){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$this->getCode();
		$data = '';
		$coatype_id = array();
		if(!empty($id) && $id!=0):
			$coatype_id[] = array('Coagroup.coatype_id' => $id);
		else:
			$coatype_id[] = array();
		endif;
		$coagroup=$this->Coagroup->find(
			'list',array(
				"fields" => array("Coagroup.id","coagroup_name"),
				"conditions" => array("Coagroup.coatype_id"=>$id)
			)
		);
			$data .= "<option value=\"0\">Select Chart of Account Group</option>";
		foreach($coagroup as $key => $val) {
			$data .= "<option value=\"".$key."\">".$val."</option>";
		}

		$coa_type_id = $this->Coatype->find('all',array("fields" => "Coatype.id","conditions"=>array('Coatype.id'=>$id)));
		//$data .= -$coa_type_id;
		echo $data;
		

	}

	public function listcoaclass($coatypeID=null,$coagroupID=null){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$coaclass = array();
		$this->getCode();
		$data = '';
		if(!empty($coatypeID) && $coatypeID!=0):
			$coaclass[] = array('Coaclass.coatype_id' => $coatypeID);
		endif;
		if(!empty($coagroupID) && $coagroupID!=0):
			$coaclass[] = array('Coaclass.coagroup_id' => $coagroupID);
		endif;
		$coalist=$this->Coaclass->find(
			'list',array(
				"fields" => array("Coaclass.id","coaclass_name"),
				"conditions" => $coaclass
			)
		);
			$data .= "<option value=\"0\">Select Chart of Account Class</option>";
		foreach($coalist as $key => $val) {
			$data .= "<option value=\"".$key."\">".$val."</option>";
		}

		//get coa group code
		$coa_group_code = $this->Coagroup->find('all',array("fields" => "Coagroup.id","conditions"=>array('Coagroup.id'=>$coagroupID)));
		echo $coa_group_code[0]['Coagroup']['id'];

		echo $data;
	}


	public function listcoasubclass($coatypeID=null,$coagroupID=null, $coaclassID=null){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;

		$coaclasssubclass = array();
		if(!empty($coatypeID) && $coatypeID!=0):
			$coaclasssubclass[] = array('Coaclasssubclass.coatype_id' => $coatypeID);
		endif;
		if(!empty($coagroupID) && $coagroupID!=0):
			$coaclasssubclass[] = array('Coaclasssubclass.coagroup_id' => $coagroupID);
		endif;
		if(!empty($coaclassID) && $coaclassID!=0):
			$coaclasssubclass[] = array('Coaclasssubclass.coaclass_id' => $coaclassID);
		endif;

		$coaclasssublist=$this->Coaclasssubclass->find(
			'list',array(
				"fields" => array("Coaclasssubclass.id","coaclasssubclass_name"),
				"conditions" => $coaclasssubclass
			)
		);		
			echo "<option value=\"0\">Select Chart of Account Sub Class</option>";
		foreach($coaclasssublist as $key => $val) {
			echo "<option value=\"".$key."\">".$val."</option>";
		}
		
	}


	public function coatype(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$CoaclassCoatypeId = @$this->request->data['CoaclassCoatypeId'];

		$coatype_list=$this->Coatype->find(
			'all',
			array(
				"fields" => array("Coatype.id", "coatype_name"),
				"conditions"=>array(
					'Coatype.coatypeisactive'=>1,
				)
			)
		);
			echo "<option value=\"0\">Select Chart of Account Type yyy</option>";
		foreach($coatype_list as $thiscoatype_list) {
			echo "<option value=\"".$thiscoatype_list['Coatype']['id']."\">".$thiscoatype_list['Coatype']['coatype_name']."</option>";
		}
	}

	public function  coainsertupdateaction($id=null){
		$this->beforeRender();
		$this->autoRender = false;

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$userID = $this->Session->read('User.id');
		$id = @$this->request->data['Coas']['id'];
		$uuid = @$this->request->data['Coas']['coauuid'];
		$coatype_id = $this->request->data['Coa']['coatype_id'];
		$coagroup_id = $this->request->data['Coa']['coagroup_id'];
		$coaclass_id = $this->request->data['Coa']['coaclass_id'];
		$coaclasssubclass_id = $this->request->data['Coa']['coaclasssubclass_id'];
		$coacode = $this->request->data['Coa']['coacode'];
		$coaname = $this->request->data['Coa']['coaname'];
		$coanamebn = $this->request->data['Coa']['coanamebn'];
		$countcoagroupname=$this->Coa->find('all',array("fields" => "Coa.*","conditions"=>array('Coa.coatype_id'=>$coatype_id,'Coa.coagroup_id'=>$coagroup_id,'Coa.coaclass_id'=>$coaclass_id,'Coa.coaclasssubclass_id'=>$coaclasssubclass_id,'Coa.coacode'=>$coacode,'Coa.coaname'=>$coaname,'Coa.coanamebn'=>$coanamebn)));
		if(empty($id) && count($countcoagroupname)>0):
			$this->Session->setFlash(__("This COA Already Exists"), 'default', array('class' => 'alert alert-warning'));
			$this->redirect("/coas/coainsertupdate");
		else:
			if ($this->request->is('post')) {
				if(!empty($id) && $id!=0):
					$this->request->data['Coa']['coaupdateid']=$userID;
					$this->request->data['Coa']['coaupdatedate']=date('Y-m-d H:i:s');
					$message = __('Update Successfully.');
					$this->Coa->id = $id;
				else:
					$this->request->data['Coa']['company_id']=$company_id;
					//$this->request->data['Coa']['branch_id']=$branch_id;
					$this->request->data['Coa']['coainsertid']=$userID;
					$this->request->data['Coa']['coainsertdate']=date('Y-m-d H:i:s');
					$this->request->data['Coa']['coauuid']=String::uuid();
					$message = __('Save Successfully.');
				endif;
				if ($this->Coa->save($this->request->data, array('validate' => 'only'))) {
					$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
					$this->redirect("/coas/coamanage");
				}else{
					$this->Session->setFlash('error', 'default', array('class' => 'alert alert-danger'));				
					$this->redirect("/coas/coainsertupdate");
				}
			}
		endif;
	}

	public function coadelete($id=null,$uuid=null){
		$this->layout='ajax';
		$userID = $this->Session->read('User.id');
		if ($this->Coa->updateAll(array('coadeleteid'=>$userID, 'coadeletedate'=>"'".date('Y-m-d H:i:s')."'", 'coaisactive'=>2), array('AND' => array('Coa.id'=>$id,'Coa.coauuid' => $uuid)))) {
			$this->Session->setFlash(__('Delete Successfully.'), 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/coas/coamanage");
	}

	public function getCode(){
		$this->layout = 'ajax';
        $this->beforeRender();
        $this->autoRender = false;
        //get coa type name
        if(isset($this->request->data['coa_type_code_id'])){
		 	$coa_type_name = $this->Coatype->find('all',array("fields" => "Coatype.id","conditions"=>array('Coatype.id'=>$this->request->data['coa_type_code_id'])));
			echo $coa_type_name[0]['Coatype']['id'];
			
		}

		//get coa type code
		if(isset($this->request->data['coa_type_name_id'])){
		 	$coa_type_id = $this->Coatype->find('all',array("fields" => "Coatype.id","conditions"=>array('Coatype.id'=>$this->request->data['coa_type_name_id'])));
			echo $coa_type_id[0]['Coatype']['id'];
			
		}

		//get coa group name
		if(isset($this->request->data['coa_group_code_id'])){
			$coa_group_name = $this->Coagroup->find('all',array("fields" => "Coagroup.id","conditions"=>array('Coagroup.id'=>$this->request->data['coa_group_code_id'])));
			echo $coa_group_name[0]['Coagroup']['id'];

		}

		//get coa group code
		if(isset($this->request->data['coa_group_name_id'])){
			$coa_group_code = $this->Coagroup->find('all',array("fields" => "Coagroup.id","conditions"=>array('Coagroup.id'=>$this->request->data['coa_group_name_id'])));
			echo $coa_group_code[0]['Coagroup']['id'];

		}

		//get coa class name
		if(isset($this->request->data['coa_class_code_id'])){
			$coa_class_name = $this->Coaclass->find('all',array("fields" => "Coaclass.id","conditions"=>array('Coaclass.id'=>$this->request->data['coa_class_code_id'])));
			echo $coa_class_name[0]['Coaclass']['id'];

		}

		//get coa sub class name
		if(isset($this->request->data['coa_sub_class_code_id'])){
			$coa_sub_class_name = $this->Coaclasssubclass->find('all',array("fields" => "Coaclasssubclass.id","conditions"=>array('Coaclasssubclass.id'=>$this->request->data['coa_sub_class_code_id'])));
			echo $coa_sub_class_name[0]['Coaclasssubclass']['id'];

		}



	}

	//End Chart of Account //
}

?>
