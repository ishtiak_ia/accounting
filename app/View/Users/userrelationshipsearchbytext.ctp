<?php
echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Relationship Name')  => array('class' => 'highlight sortable')
					),
					__('Active'), 
					__('Action')
				)
			);
			$Userrelationshiptrows = array();
			$countUserrelationshiptrows = 0-1+$this->Paginator->counter('%start%');
			foreach($userrelationship as $Thisuserrelationship):
				$countUserrelationshiptrows++;
				$Userrelationshiptrows[]= array($countUserrelationshiptrows,$Thisuserrelationship["Userrelationship"]["userrelationshipname"],$Utilitys->statusURL($this->request["controller"], 'userrelationship', $Thisuserrelationship["Userrelationship"]["id"], $Thisuserrelationship["Userrelationship"]["userrelationshipuuid"], $Thisuserrelationship["Userrelationship"]["userrelationshipisactive"]),$Utilitys->cusurl($this->request["controller"], 'userrelationship', $Thisuserrelationship["Userrelationship"]["id"], $Thisuserrelationship["Userrelationship"]["userrelationshipuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$Userrelationshiptrows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#UserrelationshipSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#userrelationshipsearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#userrelationshipsearchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>