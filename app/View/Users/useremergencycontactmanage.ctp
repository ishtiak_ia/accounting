<?php
	echo "<h2>".__('Emergency Contact'). $Utilitys->addurl($title=__("Add New Emergency Contact"), $this->request['controller'], $page="useremergencycontact")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"User.department_id",
			array(
				'type'=>'select',
				"options"=>array($department),
				'empty'=>__('Select Department'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
				)
			)
		.$this->Form->input(
			"Useremergencycontact.user_id",
			array(
				'type'=>'select',
				"options"=>array($user_list),
				'empty'=>__('Select Employee'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
				)
			)
		.$this->Form->input(
			"Useremergencycontact.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"useremergencycontactsarchloading\">
	";
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Employe Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Emergency Contact Name')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Emergency Contact BN')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Relationship')  => array('class' => 'highlight sortable')
					),
					__('Active'), 
					__('Action')
				)
			);
			$useremergencycontactrows = array();
			$countUseremergencycontactrows = 0-1+$this->Paginator->counter('%start%');
			foreach($useremergencycontact as $Thisuseremergencycontact):
				$countUseremergencycontactrows++;
				$useremergencycontactrows[]= array($countUseremergencycontactrows,$Thisuseremergencycontact["Useremergencycontact"]["user_fullname"],$Thisuseremergencycontact["Useremergencycontact"]["useremergencycontactname"],$Thisuseremergencycontact["Useremergencycontact"]["useremergencycontactnamebn"],$Thisuseremergencycontact["Useremergencycontact"]["useremergencycontactrelationship"],$Utilitys->statusURL($this->request["controller"], 'useremergencycontact', $Thisuseremergencycontact["Useremergencycontact"]["id"], $Thisuseremergencycontact["Useremergencycontact"]["useremergencycontactuuid"], $Thisuseremergencycontact["Useremergencycontact"]["useremergencycontactisactive"]),$Utilitys->cusurl($this->request["controller"], 'useremergencycontact', $Thisuseremergencycontact["Useremergencycontact"]["id"], $Thisuseremergencycontact["Useremergencycontact"]["useremergencycontactuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$useremergencycontactrows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();

	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				$(\"#UseremergencycontactSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#useremergencycontactsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/useremergencycontactsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#useremergencycontactsarchloading\").html(result);
						}
					);
				});
				$(\"#UserDepartmentId\").change(function(){
					var UserDepartmentId = $(\"#UserDepartmentId\").val();
					$(\"#useremergencycontactsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/useremergencycontactsearchbytext',
						{UserDepartmentId:UserDepartmentId},
						function(result) {
							$(\"#useremergencycontactsarchloading\").html(result);
						}
					);
				});
				$(\"#UseremergencycontactUserId\").change(function(){
					var UseremergencycontactUserId = $(\"#UseremergencycontactUserId\").val();
					$(\"#useremergencycontactsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/useremergencycontactsearchbytext',
						{UseremergencycontactUserId:UseremergencycontactUserId},
						function(result) {
							$(\"#useremergencycontactsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>