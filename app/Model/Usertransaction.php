<?php
App::uses('AppModel', 'Model');
class Usertransaction extends AppModel {
	public $name = 'Usertransaction';
	public $usetables = 'usertransactions';
	var $belongsTo  = array(
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'usertransactioninsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'usertransactionupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'usertransactiondeleteid'
		),
		'User' => array(
			'fields' =>array('User.*'),
			'className'    => 'User',
			'foreignKey'    => 'user_id'
		),
		'Group' => array(
			'fields' =>array('Group.*'),
			'className'    => 'Group',
			'foreignKey'    => 'group_id'
		),
		'Coa' => array(
			'fields' =>array('Coa.*'),
			'className'    => 'Coa',
			'foreignKey'    => 'coa_id'
		),
		'Banktransaction' => array(
			'fields' =>array('Banktransaction.*'),
			'className'	=> 'Banktransaction',
			'foreignKey'	=> false,
			'conditions'	=> 'Banktransaction.transaction_id = Usertransaction.transaction_id AND Banktransaction.transactiontype_id = Usertransaction.transactiontype_id'
		)		
	);
	var $virtualFields = array(
		'usertransaction_particulars' => 'CONCAT(
			IF(Usertransaction.transactiontype_id=0, "", IF(Usertransaction.transactiontype_id=3,"Withdraw","Deposits")), 
			IF(Usertransaction.user_id=0, "", CONCAT("/", User.userfirstname, " ", User.usermiddlename, " ", User.userlastname)), 
			IF(Usertransaction.coa_id=0, " ", CONCAT("/", Coa.coaname)) 
		)',
		'usertransaction_balance' => 'IF(Usertransaction.transactionamount=0, "0.00", ROUND(Usertransaction.transactionamount, 2))',
		'usertransaction_withdraw' => 'IF(Usertransaction.transactionamount<0,ROUND(Usertransaction.transactionamount, 2), "0.00")',
		'usertransaction_deposit' => 'IF(Usertransaction.transactionamount>0,ROUND(Usertransaction.transactionamount, 2), "0.00")',
		'usertransaction_chequenumber' => 'CONCAT( Banktransaction.banktransactionchequebookseries, " - ", Banktransaction.banktransactionchequesequencynumber )'

	);
	/*public $validate = array(
		'banktypename' => array(
			'rule' => 'notEmpty',
			'allowEmpty' => false,
			'required' => true,
			'message' => 'Enter a valid Banktype Name'
		),
		'banktypenamebn' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'allowEmpty' => false,
			'message' => 'Enter a valid Banktype Name in Bangla'
		)
	);*/
}

?>