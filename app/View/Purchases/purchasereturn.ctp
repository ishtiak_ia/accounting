<?php echo $this->Form->create('journalreports', array('default' => false, 'class'=>' ')); ?>
<fieldset>
	<legend><h3>Purchase Return</h3></legend>
	<table>
		<tr>
			<td>Invoice No.</td>
			<td>
				<?php
			       echo $this->Form->input(
						"Order.orderreferenceno",
						array(
							'type' => 'select',
							'div' => array('class'=> 'form-group'),
							'empty'=>__('Select Invoice No.'),
							"options"=>array($invoice_no),
							'label' => false,
							'class'=> '',
							'style' => '',
							'onchange' =>'get_orderreferenceno();' 
						)
					);
                ?>
			</td>
		</tr>
	</table>
</fieldset>
<?php echo $this->form->end(); ?> 
<div id="loadInvoiceNo">
  
</div>
<script type="text/javascript">
$(document).ready(function() {
   $("#OrderOrderreferenceno").select2();
});

</script>
<?php
echo"
		<script type='text/javascript'>
			function get_orderreferenceno(){	
					var Searchtext = $(\"#OrderOrderreferenceno\").val();
					//alert(Searchtext);
					$(\"#loadInvoiceNo\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."purchases/purchaseddatabyinvoiceno',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#loadInvoiceNo\").html(result);
						}
					);
			}	
			
		</script>
	";
?>