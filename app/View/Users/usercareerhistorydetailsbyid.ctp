<?php
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"2\">
				<h2>".__('Career History')."</h2>
			</td>
		</tr>
			<tr>
				<th>
					".__('Employee Name')."
				</th>
				<td>
					".$usercareerhistory[0]["Usercareerhistory"]["user_fullname"]."
				</td>
			</tr>
			<tr>
				<th>
					".__('Compamy Name')."
				</th>
				<td>
					".$usercareerhistory[0]["Usercareerhistory"]["usercareerhistorycompamyname"]."
				</td>
			</tr>
			<tr>
				<th>
					".__('Company Type')."
				</th>
				<td>
					".$usercareerhistory[0]["Usercareerhistory"]["usercareerhistorycompanytype"]."
				</td>
			</tr>
			<tr>
				<th>
					".__('Company Location')."
				</th>
				<td>
					".$usercareerhistory[0]["Usercareerhistory"]["usercareerhistorylocation"]."
				</td>
			</tr>
			<tr>
				<th>
					".__('Position Held')."
				</th>
				<td>
					".$usercareerhistory[0]["Usercareerhistory"]["usercareerhistorypositionheld"]."
				</td>
			</tr>
			<tr>
				<th>
					".__('Department')."
				</th>
				<td>
					".$usercareerhistory[0]["Usercareerhistory"]["usercareerhistorydepartment"]."
				</td>
			</tr>
			<tr>
				<th>
					".__('Responsibilities')."
				</th>
				<td>
					".$usercareerhistory[0]["Usercareerhistory"]["usercareerhistoryresponsibilities"]."
				</td>
			</tr>
			<tr>
				<th>
					".__('From')."
				</th>
				<td>
					".$usercareerhistory[0]["Usercareerhistory"]["usercareerhistoryfromdate"]."
				</td>
			</tr>
			<tr>
				<th>
					".__('To')."
				</th>
				<td>
					".$usercareerhistory[0]["Usercareerhistory"]["usercareerhistorytodate"]."
				</td>
			</tr>
			<tr>
				<th>
					".__("Active")."
				</th>
				<td>
					".$usercareerhistory[0]["Usercareerhistory"]["isActive"]."
				</td>
			</tr>
	";
	echo $Utilitys->tableend();
?>