<?php
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"2\">
				<h2>".__('Product Branch')."</h2>
			</td>
		</tr>
		<tr>
			<th>
				".__('Product Name')."
			</th>
			<td>
				".$productbranch[0]["Product"]["productname"]."/".$productbranch[0]["Product"]["productnamebn"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Branch Name')."
			</th>
			<td>
				".$productbranch[0]["Branch"]["branchname"]."/".$productbranch[0]["Branch"]["branchnamebn"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Company Name')."
			</th>
			<td>
				".$productbranch[0]["Company"]["companyname"]."/".$productbranch[0]["Company"]["companynamebn"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Active")."
			</th>
			<td>
				".$productbranch[0]["Productbranch"]["isActive"]."
			</td>
		</tr>
	";
	echo $Utilitys->tableend($class=null, $id=null);
?>