<?php
	echo "<h2>".__('Family Details'). $Utilitys->addurl($title=__("Add New Family Details"), $this->request['controller'], $page="userdependent")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Userdependent.user_id",
			array(
				'type'=>'select',
				"options"=>array($user_list),
				'empty'=>__('Select Employee'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
				)
			)
		.$this->Form->input(
			"Userdependent.userrelationship_id",
			array(
				'type'=>'select',
				"options"=>array($user_relationship),
				'empty'=>__('Select Relationship'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
				)
			)
		.$this->Form->input(
			"Userdependent.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"userdependentsarchloading\">
	";
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Employee Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Dependent Name')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Dependent Name BN')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Relationship')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Other')  => array('class' => 'highlight sortable')
					),
					array(
						__('Date of Birth')  => array('class' => 'highlight sortable')
					),
					__('Active'), 
					__('Action')
				)
			);
			$userdependentrows = array();
			$countUserdependentrows = 0-1+$this->Paginator->counter('%start%');
			foreach($userdependent as $Thisuserdependent):
				$countUserdependentrows++;
				$userdependentrows[]= array($countUserdependentrows,$Thisuserdependent["Userdependent"]["user_fullname"],$Thisuserdependent["Userdependent"]["userdependentname"],$Thisuserdependent["Userdependent"]["userdependentnamebn"],$Thisuserdependent["Userdependent"]["userrelationship_name"],$Thisuserdependent["Userdependent"]["userdependentrelationship"],$Thisuserdependent["Userdependent"]["userdependentdob"],$Utilitys->statusURL($this->request["controller"], 'userdependent', $Thisuserdependent["Userdependent"]["id"], $Thisuserdependent["Userdependent"]["userdependentuuid"], $Thisuserdependent["Userdependent"]["userdependentisactive"]),$Utilitys->cusurl($this->request["controller"], 'userdependent', $Thisuserdependent["Userdependent"]["id"], $Thisuserdependent["Userdependent"]["userdependentuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$userdependentrows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();

	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				$(\"#UserdependentSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#userdependentsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/userdependentsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#userdependentsarchloading\").html(result);
						}
					);
				});
				$(\"#UserdependentUserId\").change(function(){
					var UserdependentUserId = $(\"#UserdependentUserId\").val();
					var UserdependentUserrelationshipId = $(\"#UserdependentUserrelationshipId\").val();
					$(\"#userdependentsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/userdependentsearchbytext',
						{UserdependentUserId:UserdependentUserId, UserdependentUserrelationshipId:UserdependentUserrelationshipId},
						function(result) {
							$(\"#userdependentsarchloading\").html(result);
						}
					);
				});
				$(\"#UserdependentUserrelationshipId\").change(function(){
					var UserdependentUserId = $(\"#UserdependentUserId\").val();
					var UserdependentUserrelationshipId = $(\"#UserdependentUserrelationshipId\").val();
					$(\"#userdependentsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/userdependentsearchbytext',
						{UserdependentUserId:UserdependentUserId, UserdependentUserrelationshipId:UserdependentUserrelationshipId},
						function(result) {
							$(\"#userdependentsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>