<?php
App::uses('AppModel', 'Model');
class Coagroup extends AppModel {
	public $name = 'Coagroup';
	public $usetables = 'coagroups';
	var $belongsTo = array(
		'Coatype' => array(
			'fields' =>array('coatypename','coatypenamebn'),
			'className'    => 'Coatype',
			'foreignKey'    => 'coatype_id'
		),
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'coagroupinsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'coagroupupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'coagroupdeleteid'
		)
	);
	var $virtualFields = array(
		'coagroup_name' => 'CONCAT(Coagroup.coagroupname, " / ", Coagroup.coagroupnamebn)',
		'coatype_name' => 'CONCAT(Coatype.coatypename, " / ", Coatype.coatypenamebn)',
		'isActive' => 'IF(Coagroup.coagroupisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Coagroup.coagroupisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
	public $validate = array(
		'coatype_id' => array(
			'coatype_id_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Type field is required',
				'last' => true
			)
		),
		'coagroupname' => array(
			'coagroupname_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Group Name field is required',
				'last' => true
			)
		),
		'coagroupnamebn' => array(
			'coagroupnamebn_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Group Name BN field is required',
				'last' => true
			)
		)
	);
}

?>