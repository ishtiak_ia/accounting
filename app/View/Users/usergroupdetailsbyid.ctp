<?php
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"2\">
				<h2>".__('User Group')."</h2>
			</td>
		</tr>
		<tr>
			<th>
				".__('User Name')."
			</th>
			<td>
				".$usergroup[0]["User"]["userfirstname"]." ".$usergroup[0]["User"]["usermiddlename"]." ".$usergroup[0]["User"]["userlastname"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Group Name')."
			</th>
			<td>
				".$usergroup[0]["Group"]["groupname"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Company Name')."
			</th>
			<td>
				".$usergroup[0]["Company"]["companyname"]." / ".$usergroup[0]["Company"]["companynamebn"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Branch Name')."
			</th>
			<td>
				".$usergroup[0]["Branch"]["branchname"]." / ".$usergroup[0]["Branch"]["branchnamebn"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Active")."
			</th>
			<td>
				".$usergroup[0]["Usergroup"]["isActive"]."
			</td>
		</tr>
	";
	echo $Utilitys->tableend($class=null, $id=null);
?>