<?php
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"2\">
				<h2>".__('Shift')."</h2>
			</td>
		</tr>
		<tr>
			<th>
				".__('Name')."
			</th>
			<td>
				".$shifts[0]["Shift"]["shiftname"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Permanent")."
			</th>
			<td>
				".$shifts[0]["Shift"]["shiftispermanent"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Active")."
			</th>
			<td>
				".$shifts[0]["Shift"]["isActive"]."
			</td>
		</tr>
	";
	echo $Utilitys->tableend();
?>