<?php
App::uses('AppModel', 'Model');
class Bankchequebook extends AppModel {
	public $name = 'Bankchequebook';
	public $usetables = 'bankchequebooks';
	var $belongsTo  = array(
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'bankchequebookinsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'bankchequebookupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'bankchequebookdeleteid'
		),
		'Bank' => array(
			'fields' =>array('Bank.*'),
			'className'    => 'Bank',
			'foreignKey'    => 'bank_id'
		),
		'Bankbranch' => array(
			'fields' =>array('Bankbranch.*'),
			'className'    => 'Bankbranch',
			'foreignKey'    => 'bankbranch_id'
		),
		'Bankaccounttype' => array(
			'fields' =>array('Bankaccounttype.*'),
			'className'    => 'Bankaccounttype',
			'foreignKey'    => 'bankaccounttype_id'
		),
		'Bankaccount' => array(
			'fields' =>array('Bankaccount.*'),
			'className'    => 'Bankaccount',
			'foreignKey'    => 'bankaccount_id'
		),
		'Company' => array(
			'fields' =>array('Company.*'),
			'className'    => 'Company',
			'foreignKey'    => 'company_id'
		),
		'Branch' => array(
			'fields' =>array('Branch.*'),
			'className'    => 'Branch',
			'foreignKey'    => 'branch_id'
		)
	);
	var $virtualFields = array(
		'company_name' => 'CONCAT(Company.companyname, " / ", Company.companynamebn)',
		'bank_name' => 'CONCAT(Bank.bankname, " / ", Bank.banknamebn)',
		'bankbranch_name' => 'CONCAT(Bankbranch.bankbranchname, " / ", Bankbranch.bankbranchnamebn)',
		'bankaccounttype_name' => 'CONCAT(Bankaccounttype.bankaccounttypename, " / ", Bankaccounttype.bankaccounttypenamebn)',
		'bankaccount_name' => 'CONCAT(Bankaccount.bankaccountname, " / ", Bankaccount.bankaccountnamebn, " (", Bankaccount.bankaccountnumber,") ")',
		'bankchequebook_name' => 'CONCAT(Bankchequebook.bankchequebookseries, "  ", Bankchequebook.bankchequebooksequencyfrom, " - ", Bankchequebook.bankchequebooksequencyto)',
		'bankchequebook_totalamount' => 'round((SELECT SUM(transactionamount) FROM banktransactions WHERE bankchequebook_id = Bankchequebook.id),2)',
		'bankchequebook_totalpage' => "CONCAT((SELECT COUNT(*) FROM banktransactions WHERE bankchequebook_id = Bankchequebook.id AND banktransactionisactive = 0),' of ',(SELECT COUNT(*) FROM banktransactions WHERE bankchequebook_id = Bankchequebook.id))",
		'isActive' => 'IF(Bankchequebook.bankchequebookisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Bankchequebook.bankchequebookisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
	public $validate = array(
		'bankchequebookseries' => array(
			'rule' => 'notEmpty',
			'message' => 'This Bank Cheque Book Series field is required',
			'last' => true
		),
		'bankchequebooksequencyfrom' => array(
			'bankchequebooksequencyfrom_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This Bank Cheque Book Sequency Start Number field is required',
				'last' => true
			)
		),
		'bankchequebooksequencyto' => array(
			'bankchequebooksequencyto_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This Bank Cheque Book Sequency End Number field is required',
				'last' => true
			)
		),
		'bank_id' => array(
			'rule' => 'notEmpty',
			'message' => 'This Bank Name field is required',
			'last' => true
		),
		'bankbranch_id' => array(
			'rule' => 'notEmpty',
			'message' => 'This Bank Branch Name field is required',
			'last' => true
		),
		'bankaccounttype_id' => array(
			'rule' => 'notEmpty',
			'message' => 'This Bank Account Type field is required',
			'last' => true
		),
		'bankaccount_id' => array(
			'rule' => 'notEmpty',
			'message' => 'This Bank Account field is required',
			'last' => true
		)
	);
}

?>