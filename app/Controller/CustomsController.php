<?php
App::import('Controller', 'Logins');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');
class CustomsController extends AppController {
	public $helpers = array('PaginatorExt');
	public $uses = array('Menu','Bankaccount','Banktransaction','Gltransaction','User','Usertransaction','Orderdetail', 'Orderdetailreport');
	var $Logins;

	function beforeFilter(){
		$this->Logins =& new LoginsController;
		$this->Logins->constructClasses();
		$this->Logins->__validateLoginStatus();
		$Logins = new LoginsController;
		$this->set('Logins',$Logins);
		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	public function statuschangnaration($controller, $page, $id, $uuid, $activeStatus){
		$this->layout='ajax';
		$this->set('controller',$controller);
		$this->set('page',$page);
		$this->set('id',$id);
		$this->set('uuid',$uuid);
		$this->set('activeStatus',$activeStatus);

	}

	public function changestatus(){
		$this->layout='ajax';
		$controller = @$this->request->data['Customs']['controller'];
		$page = @$this->request->data['Customs']['page'];
		$id = @$this->request->data['Customs']['id'];
		$uuid = @$this->request->data['Customs']['uuid'];
		$activeStatus = @$this->request->data['Customs']['activeStatus'];
		$details = @$this->request->data['Customs']['details'];
		//echo $controller.'/'.$page.'/'.$id.'/'.$uuid.'/'.$activeStatus;
		if($activeStatus==1):
			$activeStatusChange=0;
		elseif($activeStatus==2):
			$activeStatusChange=0;
		else:
			$activeStatusChange=1;
		endif;
		$userID = $_SESSION['User']['id'];
		$this->Menu->query("
			Update ".strtolower($this->pluralize($page))." SET ".strtolower($page)."updateid='{$userID}', ".strtolower($page)."updatedate='".date('Y-m-d H:i:s')."',  ".strtolower($page)."isactive={$activeStatusChange}
			WHERE id = '{$id}' AND ".strtolower($page)."uuid = '$uuid'
		");
		/*if ($this->$page->updateAll(array($page."deleteid"=>$userID, $page.'deletedate'=>"'".date('Y-m-d H:i:s')."'", $page.'isactive'=>2), array('AND' => array(ucfirst($page).'.id'=>$id,ucfirst($page).'.'.$page.'uuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}*/
		//$this->redirect('/'.$controller.'/'.$page.'manage');
		$this->Session->setFlash(__("Status is update Successfully.."), 'default', array('class' => 'alert alert-success'));
		$this->redirect("/".$controller."/".$page."manage");
	}

	public function amountdetailsbyid($id=null, $model=null, $searchfield=null, $datefield = true){
		$Layout= @$this->request->data['DisplayLayout']?$this->request->data['DisplayLayout']:'iframe';
		$this->layout=$Layout;
		$this->set('model',$model);
		$this->set('displaylayout',$Layout);

		if(!empty($id) || $id != 0):
			$id = $id;
		else:
			$id= $this->request->data['id'];
		endif;
		if(!empty($model) || $model != 0):
			$model = $model;
		else:
			$model= $this->request->data['model'];
		endif;
		if(!empty($searchfield) || $searchfield != 0):
			$searchfield = $searchfield;
		else:
			$searchfield= $this->request->data['searchfield'];
		endif;

		$AmountFormdate= @$this->request->data['AmountFormdate']?$this->request->data['AmountFormdate']:0;
		$AmountTodate= @$this->request->data['AmountTodate']?$this->request->data['AmountTodate']:0;

		$this->set('id',$id);
		$this->set('model',$model);
		$this->set('searchfield',$searchfield);

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$amountdetailsbyidarray = array('');
		$amountdetailsbyidprearray = array('');
		$amountdetailsbyidthisarray = array('');
		if($group_id==1):
			$amountdetailsbyidarray[] = array('');
		elseif($group_id==2):
			$amountdetailsbyidarray[] = array("{$model}.company_id"=>$company_id);
		else:
			$amountdetailsbyidarray[] = array("{$model}.company_id"=>$company_id);
			$amountdetailsbyidarray[] = array("{$model}.branch_id"=>$branch_id);
			if($model=='Orderdetail'):
			else:
				$amountdetailsbyidarray[] = array("{$model}.user_id"=>$user_id);
			endif;
		endif;
		$startDate=$AmountFormdate;
		$endDate=$AmountTodate;
		if($datefield):
			if(!empty($startDate) && empty($endDate)):
				$amountdetailsbyidthisarray[] = array("DATE({$model}.transactiondate) BETWEEN '{$startDate}' AND '{$startDate}' ");
				$amountdetailsbyidprearray = array("DATE({$model}.transactiondate) < DATE_SUB(CONCAT('".$startDate."'), INTERVAL 1 DAY)  ");
			elseif(empty($startDate) && !empty($endDate)):
				$amountdetailsbyidthisarray[] = array("DATE({$model}.transactiondate) BETWEEN '{$endDate}' AND '{$endDate}' ");
				$amountdetailsbyidprearray = array("DATE({$model}.transactiondate) < DATE_SUB(CONCAT('".$endDate."'), INTERVAL 1 DAY)  ");
			elseif(!empty($startDate) && !empty($endDate)):
				$amountdetailsbyidthisarray[] = array("DATE({$model}.transactiondate) BETWEEN '{$startDate}' AND '{$endDate}' ");
				$amountdetailsbyidprearray = array("DATE({$model}.transactiondate) < DATE_SUB(CONCAT('".$startDate."'), INTERVAL 1 DAY)  ");
			else:
				$amountdetailsbyidthisarray = array("DATE({$model}.transactiondate) BETWEEN '".date('Y-m-01')."' AND '".date('Y-m-t')."' ");
				//$amountdetailsbyidprearray = array("{$model}.transactiondate <= DATE_SUB(CONCAT(CURDATE(),' 23:59:59'), INTERVAL 1 DAY) ");
				$amountdetailsbyidprearray = array("{$model}.transactiondate < DATE_SUB(CONCAT('".date('Y-m-01')."'), INTERVAL 1 DAY) ");

			endif;
		endif;
		$amountdetailsbyidarray[] = array("{$model}.".strtolower($model)."isactive"=>1);
		$amountdetailsbyidarray[] = array("{$model}.{$searchfield}"=>$id);

		$this->paginate = array(
			'fields' => "SUM({$model}.transactionamount) AS Previousamount",
			'conditions' => array($amountdetailsbyidarray,$amountdetailsbyidprearray),
			'order'  => "{$model}.transactiondate DESC",
			'limit'	=>	5000
		);
		$amountdetailsbyidforpreviousday = $this->paginate("{$model}");
		$this->set('amountdetailsbyidforpreviousday',$amountdetailsbyidforpreviousday);

		$this->paginate = array(
			'fields' => "{$model}.*",
			'conditions' => array($amountdetailsbyidarray,$amountdetailsbyidthisarray),
			'order'  => "{$model}.transactiondate DESC",
			'limit'	=>	5000
		);
		$amountdetailsbyid = $this->paginate("{$model}");
		$this->set('amountdetailsbyid',$amountdetailsbyid);
	}

	function pluralize($word){
		$plural = array(
			'/(quiz)$/i' => 'zes',
			'/^(ox)$/i' => 'en',
			'/([m|l])ouse$/i' => 'ice',
			'/(matr|vert|ind)ix|ex$/i' => 'ices',
			/*'/(x|ch|ss|sh)$/i' => 'es',*/
			'/(x|ss|sh)$/i' => 'es',
			'/(ch)$/i' => 'ches',
			'/([^aeiouy]|qu)ies$/i' => 'y',
			/*'/([^aeiouy]|qu)y$/i' => 'ies',*/
			'/y$/i' => 'ies',
			'/(hive)$/i' => 's',
			'/(?:([^f])fe|([lr])f)$/i' => 'ves',
			'/sis$/i' => 'ses',
			'/([ti])um$/i' => 'a',
			'/(buffal|tomat)o$/i' => 'oes',
			'/(bu)s$/i' => 'ses',
			'/(alias|status)/i'=> 'es',
			'/(octop|vir)us$/i'=> 'i',
			'/(ax|test)is$/i'=> 'es',
			'/s$/i'=> 's',
			'/$/'=> 's');

		$uncountable = array('equipment', 'information', 'rice', 'money', 'species', 'series', 'fish', 'sheep');

		$irregular = array(
			'person' => 'people',
			'man' => 'men',
			'child' => 'children',
			'sex' => 'sexes',
			'move' => 'moves'
		);

		$lowercased_word = strtolower($word);

		foreach ($uncountable as $_uncountable){
			if(substr($lowercased_word,(-1*strlen($_uncountable))) == $_uncountable){
				return $word;
			}
		}

		foreach ($irregular as $_plural=> $_singular){
			if (preg_match('/('.$_plural.')$/i', $word, $arr)) {
				return preg_replace('/('.$_plural.')$/i', substr($arr[0],0,1).substr($_singular,1), $word);
			}
		}

		foreach ($plural as $rule => $replacement) {
			if (preg_match($rule, $word)) {
				return preg_replace($rule, $replacement, $word);
			}
		}
		return false;
	}
}
?>