<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart($class=null, $id=null);
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('Transactiontype')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Transactiontype BN')  => array('class' => 'highlight sortable')
				), 
				__('Active'), 
				__('Action')
			)
		);
		$transactiontyperows = array();
		$countTransactiontype = 0-1+$this->Paginator->counter('%start%');
		foreach($transactiontype as $Thistransactiontype):
			$countTransactiontype++;
			$transactiontyperows[]= array($countTransactiontype,$Thistransactiontype["Transactiontype"]["transactiontypename"],$Thistransactiontype["Transactiontype"]["transactiontypenamebn"],$Utilitys->statusURL($this->request["controller"], 'transactiontype', $Thistransactiontype["Transactiontype"]["id"], $Thistransactiontype["Transactiontype"]["transactiontypeuuid"], $Thistransactiontype["Transactiontype"]["transactiontypeisactive"]),$Utilitys->cusurl($this->request["controller"], 'transactiontype', $Thistransactiontype["Transactiontype"]["id"], $Thistransactiontype["Transactiontype"]["transactiontypeuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$transactiontyperows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#TransactiontypeSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#transactiontypesarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#transactiontypesarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>