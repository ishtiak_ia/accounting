<?php
App::uses('AppModel', 'Model');
class Producttransaction extends AppModel {
	public $name = 'Producttransaction';
	public $usetables = 'producttransactions';
	var $belongsTo  = array(
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'producttransactioninsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'producttransactionupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'producttransactiondeleteid'
		)
	);
	/*var $virtualFields = array(
		'banktype_name' => 'CONCAT(Banktype.banktypename, " / ", Banktype.banktypenamebn)',
		'isActive' => 'IF(Banktype.banktypeisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Banktype.banktypeisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
	public $validate = array(
		'banktypename' => array(
			'rule' => 'notEmpty',
			'allowEmpty' => false,
			'required' => true,
			'message' => 'Enter a valid Banktype Name'
		),
		'banktypenamebn' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'allowEmpty' => false,
			'message' => 'Enter a valid Banktype Name in Bangla'
		)
	);*/
}

?>