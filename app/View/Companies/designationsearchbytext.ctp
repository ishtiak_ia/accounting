<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
		echo $this->Html->tableHeaders(
			array(
				__('SL#'),
				array(
					__('Designation Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Designation Name BN')  => array('class' => 'highlight sortable')
				),
				array(
					__('Company')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Group')  => array('class' => 'highlight sortable')
				),
				array(
					__('Is Parent')  => array('class' => 'highlight sortable')
				),
				array(
					__('Is Active')  => array('class' => 'highlight sortable')
				),
				__('Action')
			)
		);
		$designationrows = array();
		$countDesignation = 0-1+$this->Paginator->counter('%start%');
		
		foreach($designations as $Thisdesignation):
			$countDesignation++;
			$designationrows[]= array($countDesignation,$Thisdesignation["Designation"]["designationname"],$Thisdesignation["Designation"]["designationnamebn"],$Thisdesignation["Designation"]["company_fullname"],$Thisdesignation["Designation"]["group_name"],$Thisdesignation["Designation"]["isParent"],$Utilitys->statusURL($this->request["controller"], 'designation', $Thisdesignation["Designation"]["id"], $Thisdesignation["Designation"]["designationuuid"], $Thisdesignation["Designation"]["designationisactive"]),$Utilitys->cusurl($this->request['controller'], 'designation', $Thisdesignation["Designation"]["id"], $Thisdesignation["Designation"]["designationuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$designationrows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#DesignationSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#designationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#designationsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>