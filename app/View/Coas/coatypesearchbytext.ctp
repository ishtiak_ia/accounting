<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart($class=null, $id=null);
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('CODE')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Type')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Type BN')  => array('class' => 'highlight sortable')
				), 
				__('Active'), 
				__('Action')
			)
		);
		$coatyperows = array();
		$countCoatype = 0-1+$this->Paginator->counter('%start%');
		foreach($coatype as $Thiscoatype):
			$countCoatype++;
			$coatyperows[]= array(
				$Utilitys->numberLangConverter($lan=null, $countCoatype
				),
				$Thiscoatype["Coatype"]["coatypecode"],
				$Thiscoatype["Coatype"]["coatypename"],
				$Thiscoatype["Coatype"]["coatypenamebn"],$Utilitys->statusURL($this->request["controller"], 'coatype', $Thiscoatype["Coatype"]["id"], $Thiscoatype["Coatype"]["coatypeuuid"], $Thiscoatype["Coatype"]["coatypeisactive"]),
				$Utilitys->cusurl($this->request["controller"], 'coatype', $Thiscoatype["Coatype"]["id"], $Thiscoatype["Coatype"]["coatypeuuid"]
				 )
			);
		endforeach;
		echo $this->Html->tableCells(
			$coatyperows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#CoatypeSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#coatypesearchhloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#coatypesearchhloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>