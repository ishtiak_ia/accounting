<?php
	echo "<h2>".__('Journal Transaction'). $Utilitys->addurl($title=__("Add New Journal Transaction"), $this->request["controller"], $page="gltransaction")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Banktransaction.bankaccount_id",
			array(
				'type'=>'select',
				"options"=>array($bankaccount),
				'empty'=>__('Select Account'),
				'div'=>array('class'=>'form-group'),
				'tabindex'=>4,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
			)
		)
		.$this->Form->input(
			"Banktransaction.searchtext",
			array(
				'type' => 'text',
				'div'=>array('class'=>'form-group'),
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => ''
			)
		);
	echo $Utilitys->filtertagend();
	echo"
		<div id=\"gltransactionsarchloading\">
	";
		echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id='printtable');
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('Date')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Particulars')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Chq.No')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Withdraw')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Deposits')  => array('class' => 'highlight sortable')
						), 
						__('Active'), 
						__('Action')
					)
				);
				$gltransactionrows = array();
				$same_journal_id = '';
				$countGltransaction = 0-1+$this->Paginator->counter('%start%');
				foreach($journaltransaction as $Thisgltransaction):
					$countGltransaction++;
					$edit_button ='';
				  
					if($Thisgltransaction["Journaltransaction"]["id"] == $Thisgltransaction["Journaltransaction"]["journaltransaction_id"])
				    		$same_journal_id = $Thisgltransaction["Journaltransaction"]["journaltransaction_id"];
 								

 					 if($Thisgltransaction["Journaltransaction"]["id"] == $same_journal_id)
							$edit_button = $Utilitys->cusurl($this->request["controller"], 'gltransaction', $Thisgltransaction["Journaltransaction"]["id"], $Thisgltransaction["Journaltransaction"]["journaltransactionuuid"],$Thisgltransaction["Journaltransaction"]["transaction_id"]);
		
				    if($Thisgltransaction["Journaltransaction"]["journaltransaction_withdraw"]<0)
				    	$Thisgltransaction["Journaltransaction"]["journaltransaction_withdraw"] = str_replace('-', '', $Thisgltransaction["Journaltransaction"]["journaltransaction_withdraw"]);
				    
				    $gltransactionrows[]= array($countGltransaction,$Thisgltransaction["Journaltransaction"]["transactiondate"],$Thisgltransaction["Journaltransaction"]["journaltransaction_particulars"],$Thisgltransaction["Journaltransaction"]["journaltransaction_chequenumber"],array($Thisgltransaction["Journaltransaction"]["journaltransaction_withdraw"],'colspan="1" align="right"'),array($Thisgltransaction["Journaltransaction"]["journaltransaction_deposit"],'colspan="1" align="right"'),$Utilitys->statusURL($this->request["controller"], 'gltransaction', $Thisgltransaction["Journaltransaction"]["id"], $Thisgltransaction["Journaltransaction"]["journaltransactionuuid"], $Thisgltransaction["Journaltransaction"]["journaltransactionisactive"], $Thisgltransaction["Journaltransaction"]["transaction_id"]),$edit_button);
				endforeach;
				echo $this->Html->tableCells(
					$gltransactionrows
				);
			echo $Utilitys->tableend();
		echo $Utilitys->paginationcommon();
	echo"
		</div>
		<script>
			$(document).ready(function() {
				$(\"#BanktransactionSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#gltransactionsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/banktransactionsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#gltransactionsarchloading\").html(result);
						}
					);
				});
				$(\"#BanktransactionBankaccountId\").change(function(){
					var BanktransactionBankaccountId = $(\"#BanktransactionBankaccountId\").val();
					$(\"#gltransactionsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/banktransactionsearchbytext',
						{BanktransactionBankaccountId:BanktransactionBankaccountId},
						function(result) {
							$(\"#gltransactionsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>