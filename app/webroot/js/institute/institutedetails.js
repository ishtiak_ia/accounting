$(function() {
	$("#InstitutedetailAdvancegrant").keyup(function(){
        var advance_donation = this.value;
        if(advance_donation =='' || advance_donation == 'NaN'){
            advance_donation = 0;
        }else{
            advance_donation = parseFloat(advance_donation);
        }

        var donation = $("#InstitutedetailGrantamount").val();
        if(donation =='' || donation == 'NaN'){
            donation = 0;
        }else{
            donation = parseFloat(donation);
        }
        var ten_percent_donation = $("#Institutedetail10percentbankinterest").val();
        if(ten_percent_donation =='' || ten_percent_donation == 'NaN'){
            ten_percent_donation = 0;
        }else{
            ten_percent_donation = parseFloat(ten_percent_donation);
        }

        var total_donation = (advance_donation + donation + ten_percent_donation);

       
        $("#InstitutedetailTotalgrant").val(total_donation);
    });   
	$("#InstitutedetailGrantamount").keyup(function(){
		var donation = this.value;
        if(donation =='' || donation == 'NaN'){
            donation = 0;
        }else{
            donation = parseFloat(donation);
        }
        var advance_donation = $("#InstitutedetailAdvancegrant").val();
        if(advance_donation =='' || advance_donation == 'NaN'){
            advance_donation = 0;
        }else{
            advance_donation = parseFloat(advance_donation);
        }

        var calculate_ten_percent_donation = parseFloat(donation * ( 1 + (10/100)));

        $("#Institutedetail10percentbankinterest").val(calculate_ten_percent_donation);

        var total_donation = (advance_donation + donation + calculate_ten_percent_donation);
        $("#InstitutedetailTotalgrant").val(total_donation);
	}); 
	$("#Institutedetail22percentlesskgsale").keyup(function(){
        var kg_sale = this.value;
        if(kg_sale =='' || kg_sale == 'NaN'){
            kg_sale = 0;
        }else{
            kg_sale = parseFloat(kg_sale);
        }
        var high_school_sale = $("#Institutedetail26percentlesshighsale").val();
        if(high_school_sale =='' || high_school_sale == 'NaN'){
            high_school_sale = 0;
        }else{
            high_school_sale = parseFloat(high_school_sale);
        }
        var guide_sale = $("#Institutedetail25percentlessguidesale").val();
        if(guide_sale =='' || guide_sale == 'NaN'){
            guide_sale = 0;
        }else{
            guide_sale = parseFloat(guide_sale);
        }
        var total_sale = (kg_sale + high_school_sale + guide_sale);
        $("#InstitutedetailTotalsale").val(total_sale);
    });
    $("#Institutedetail26percentlesshighsale").keyup(function(){
        var high_school_sale = this.value;
        if(high_school_sale =='' || high_school_sale == 'NaN'){
            high_school_sale = 0;
        }else{
            high_school_sale = parseFloat(high_school_sale);
        }
        var kg_sale = $("#Institutedetail22percentlesskgsale").val();
        if(kg_sale =='' || kg_sale == 'NaN'){
            kg_sale = 0;
        }else{
            kg_sale = parseFloat(kg_sale);
        }
        var guide_sale = $("#Institutedetail25percentlessguidesale").val();
        if(guide_sale =='' || guide_sale == 'NaN'){
            guide_sale = 0;
        }else{
            guide_sale = parseFloat(guide_sale);
        }
        var total_sale = (kg_sale + high_school_sale + guide_sale);
        $("#InstitutedetailTotalsale").val(total_sale);
    });   	
    $("#Institutedetail25percentlessguidesale").keyup(function(){
        var guide_sale = this.value;
        if(guide_sale =='' || guide_sale == 'NaN'){
            guide_sale = 0;
        }else{
            guide_sale = parseFloat(guide_sale);
        }
        var kg_sale = $("#Institutedetail22percentlesskgsale").val();
        if(kg_sale =='' || kg_sale == 'NaN'){
            kg_sale = 0;
        }else{
            kg_sale = parseFloat(kg_sale);
        }
        var high_school_sale = $("#Institutedetail26percentlesshighsale").val();
        if(high_school_sale =='' || high_school_sale == 'NaN'){
            high_school_sale = 0;
        }else{
            high_school_sale = parseFloat(high_school_sale);
        }
        var total_sale = (kg_sale + high_school_sale + guide_sale);
        $("#InstitutedetailTotalsale").val(total_sale);
    }); 
});