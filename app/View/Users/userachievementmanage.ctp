<?php
	echo "<h2>".__('Special Achievement'). $Utilitys->addurl($title=__("Add New Special Achievement"), $this->request['controller'], $page="userachievement")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Userachievement.user_id",
			array(
				'type'=>'select',
				"options"=>array($user_list),
				'empty'=>__('Select Employee'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
				)
			)
		.$this->Form->input(
			"Userachievement.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"userachievementsarchloading\">
	";
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Employee Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Achievement')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Type')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Amount')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Description')  => array('class' => 'highlight sortable')
					),
					array(
						__('Date')  => array('class' => 'highlight sortable')
					),
					__('Active'), 
					__('Action')
				)
			);
			$userachievementrows = array();
			$countUserachievementrows = 0-1+$this->Paginator->counter('%start%');
			foreach($userachievement as $Thisuserachievement):
				$countUserachievementrows++;
				$userachievementrows[]= array($countUserachievementrows,$Thisuserachievement["Userachievement"]["user_fullname"],$Thisuserachievement["Userachievement"]["userachievementname"],$Thisuserachievement["Userachievement"]["achievementStatus"],$Thisuserachievement["Userachievement"]["userachievementamount"],$Thisuserachievement["Userachievement"]["userachievementdescription"],$Thisuserachievement["Userachievement"]["userachievementdate"],$Utilitys->statusURL($this->request["controller"], 'userachievement', $Thisuserachievement["Userachievement"]["id"], $Thisuserachievement["Userachievement"]["userachievementuuid"], $Thisuserachievement["Userachievement"]["userachievementisactive"]),$Utilitys->cusurl($this->request["controller"], 'userachievement', $Thisuserachievement["Userachievement"]["id"], $Thisuserachievement["Userachievement"]["userachievementuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$userachievementrows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();

	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				$(\"#UserachievementSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#userachievementsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/userachievementsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#userachievementsarchloading\").html(result);
						}
					);
				});
				$(\"#UserachievementUserId\").change(function(){
					var UserachievementUserId = $(\"#UserachievementUserId\").val();
					$(\"#userachievementsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/userachievementsearchbytext',
						{UserachievementUserId:UserachievementUserId},
						function(result) {
							$(\"#userachievementsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>