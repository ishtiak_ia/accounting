<?php 
    echo"
        <table>
            <tr>
                <td>
                    <h2>".__('Add Workingarea Information')."</h2>
                </td>
            </tr>
            <tr>
                <td>
    ";
    echo $this->Form->create('Companies', array('action' => 'workingareainsertupdateaction', 'type' => 'file'));
    echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$workingareas[0]['Workingarea']['id']));
    echo $this->Form->input('workingareauuid', array('type'=>'hidden', 'value'=>@$workingareas[0]['Workingarea']['workingareauuid']));
    $options = array(1 => __("Yes"), 0 =>__("No"));
            $attributes = array(
                'legend' => false, 
                'label' => true, 
                'value' => (@$workingareas[0]['Workingarea']['workingareaisactive'] ? $workingareas[0]['Workingarea']['workingareaisactive'] : 0), 
                'class' => 'form-inline'
            );    
echo $Utilitys->tablestart();
echo"
        <tr>
            <td>".__("Workingarea Name")."</td>
            <td>
                ".$this->Form->input(
                                    "Workingarea.workingareaname",
                                    array(
                                        'type' => 'text',
                                        'div' => false, 
                                        'label' => false,
                                        'data-validation-engine'=>'validate[required]',
                                        'value'=> @$workingareas[0]['Workingarea']['workingareaname']
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("Workingarea Name BN")."</td>
            <td>
                ".$this->Form->input(
                                    "Workingarea.workingareanamebn",
                                    array(
                                        'type' => 'text',
                                        'div' => false, 
                                        'label' => false,
                                        'value'=> @$workingareas[0]['Workingarea']['workingareanamebn']
                                    )
                                )."
            </td>
        </tr>
        <tr>
            <td>".__("isActive")."</td>
            <td>
                ".$this->Form->radio(
                'Workingarea.workingareaisactive',
                $options, 
                $attributes
                )."
            </td>
        </tr>
        <tr>
            <td colspan=\"2\">
                ".$Utilitys->allformbutton('',$this->request["controller"],$page='workingarea')."
            </td>
        </tr>
    </table>
    ";
    echo $this->Form->end();
    echo"
                </td>
            </tr>
        </table>
    ";
?>   

<?php echo $this->Form->end(); ?> 
<script type="text/javascript">
 jQuery(document).ready(function(){
    jQuery("#CompaniesWorkingareainsertupdateactionForm").validationEngine();
 });
</script>