<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('EMP. ID')  => array('class' => 'highlight sortable')
					),
					array(
						__('User')  => array('class' => 'highlight sortable')
					),
					array(
						__('Department Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Employee Lifecycle')  => array('class' => 'highlight sortable')
					)
				)
			);
			$userlifecyclerows = array();
			$countUserlifecyclerows = 0-1+$this->Paginator->counter('%start%');
			foreach($userlifecycle as $Thisuser):
				$countUserlifecyclerows++;
				$userlifecyclerows[]= array($countUserlifecyclerows,
				$Thisuser["User"]["userpunchmachineid"],
				$Thisuser["User"]["user_fullname"],
				$Thisuser["User"]["department_name"],
				$Utilitys->userlifecycleView(
				           'View', 
				           $Thisuser["User"]["id"],
				           $Thisuser["User"]["useruuid"],
				          'User')
				/*
				$Utilitys->paysleepView(
				           'View', 
				           $Thisuser["User"]["id"],
				           $Thisuser["User"]["useruuid"],
				           'User')
				*/
				);
			endforeach;
			echo $this->Html->tableCells(
				$userlifecyclerows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#userlifecyclesearchbytext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#userlifecyclesearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#userlifecyclesearchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
	
?>