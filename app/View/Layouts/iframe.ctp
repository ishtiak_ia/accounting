<?php //pr($this->request); ?>
<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="bn-BD"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="bn-BD"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="bn-BD"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="bn-BD"> <!--<![endif]-->
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $title_for_layout; ?>
        <?php //echo $cakeDescription ?>:
        <?php //echo $this->fetch('title'); ?>
    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Holding Tax Management of Chittagong City Corporation">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <!--[if lt IE 9]>
        <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php
    echo $this->fetch('meta');

    //echo $this->Html->css('cake.generic');
    echo $this->Html->css('bootstrap.min');
    //echo $this->Html->css('bootstrap-switch.min');
    echo $this->Html->css('style');
    echo $this->Html->css('bootstrap-datetimepicker.min');
    //echo $this->Html->css('prettyPhoto');
    echo $this->Html->meta('icon');
    //echo $this->Html->css('validationEngine.jquery');
    echo $this->fetch('css');
    echo $this->Html->script('jquery-1.11.1.min');
    echo $this->Html->script('libs/require/require');


    //conditionally loading CSS scripts
    /*if( $this->CCC->is_page( 'Taxpayees', 'payeeassessment', $current_contoller, $current_action ) ) {
        echo $this->Html->css('datepicker3.min');
    }*/
    ?>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
        <!-- END OF HEADER.ctp -->
	<!-- BODY AREA -->
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>
	
	        		
                    <?php if( isset( $_SESSION['User']['id'] ) ) {
                    //YES, the user is Logged in
                    ?>
                    <footer role="contentinfo" id="site-footer">
                        <a href="#" class="logo-footer pull-left" target="_blank">
                            <?php echo $this->html->image('/img/innotech-monogram.png', array('alt'=> 'Logo of innotech')); ?>
                        </a>
                        <p class="site-credit pull-right text-right">
                            &copy; innotech 2010-<?php echo date('Y'); ?><br>
                            Powered by: <a href="http://innotech.com.bd/" target="_blank" title="Developed by Ariful Haque, Abdul Aziz, Kaniz Shikowa; Designed by Mayeenul Islam from innotech"><em>innotech</em></a>
                        </p>
                    </footer>
                    <!-- /#site-footer -->
                    <?php } //endif( isset( $_SESSION['User']['id'] ) ) ?>
                    
                    </div> <!-- .col-sm-10 col-sm-offset-2 main -->
                </div>
                <!-- /.main -->
            </div>
        </div>
        <!-- /.container-fluid -->
<?php
        /* JavaScripts */
        echo $this->fetch('script');
        echo $this->Html->script('languages/jquery.validationEngine-en');
        echo $this->Html->script('jquery.validationEngine');
        echo $this->Html->script('jquery.prettyPhoto');
        echo $this->Html->script('bootstrap.min');
        echo $this->Html->script('bootstrap-switch.min');
        echo $this->Html->script('moment-with-langs.min');
        echo $this->Html->script('bootstrap-datetimepicker.min');
        echo $this->Html->script('jquery.print');
        echo $this->Html->script('jspdf.min');
        echo $this->Html->script('tableExport');
        echo $this->Html->script('jquery.base64');
        echo $this->Html->script('html2canvas');
        
        //echo $this->Html->script('jspdf.plugin.cell');
        echo $this->Html->script('addimage');
        echo $this->Html->script('standard_fonts_metrics');
        echo $this->Html->script('split_text_to_size');
        echo $this->Html->script('from_html');
        echo $this->Html->script('libs/html2pdf');
        echo $this->Html->script('custom');
        /* JavaScripts */
?>
<script>
        $(document).ready(function(){
            $("a[rel^='prettyPhoto']").prettyPhoto({social_tools:''});
            setTimeout(function() {
            $("a i,#flashMessage").css({"display":"none","opacity":"0"}); 
            }, 5000);

        });
        </script>
        <?php //echo $this->element('sql_dump'); ?>
        <!-- scripts_for_layout -->
        <?php echo $scripts_for_layout; ?>
        <!-- Js writeBuffer -->
        <?php
        if (class_exists('JsHelper') && method_exists($this->Js, 'writeBuffer')) echo $this->Js->writeBuffer();
        // Writes cached scripts
        ?>
        
    </body>
</html>
