<?php
	//echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart($class=null, $id=null);
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('Date')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Particulars')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Debit')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Credit')  => array('class' => 'highlight sortable')
				)
			)
		);
		$journalrows = array();
		$countJournal = 0-1+$this->Paginator->counter('%start%');
		foreach($gltransaction as $Thisgltransaction):
			$countJournal++;
			$journalrows[]= array($countJournal,$Thisgltransaction["Gltransaction"]["gltransaction_date"],$Thisgltransaction["Gltransaction"]["gltransaction_particulars"],$Thisgltransaction["Gltransaction"]["gltransaction_deposit"],str_replace("-", "", $Thisgltransaction["Gltransaction"]["gltransaction_withdraw"]));
		endforeach;
		echo $this->Html->tableCells(
			$journalrows
		);
	echo $Utilitys->tableend();
	//echo $Utilitys->paginationcommon();
?>