<?php //echo $this->Html->script('order/order', array('inline' => true)); 
?>	

<!--<form class="sales-order-manage" name="purchaseProduct" id="purchaseProduct" action="purchaseinsertupdateaction" method="post">-->
<?php
echo $this->Form->create('Purchases', array('action' => 'purchaseinsertupdateaction', 'type' => 'file'));
echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$purchase[0]['Order']['id']));
echo $this->Form->input('orderuuid', array('type'=>'hidden', 'value'=>@$purchase[0]['Order']['orderuuid']));
	//echo $this->Form->input('Bank.banktransactionId', array('type'=>'hidden', 'value'=>"", 'class'=>''));
	//echo $this->Form->input('Banktransaction.bank_id', array('type'=>'hidden', 'value'=>"", 'class'=>''));
	//echo $this->Form->input('Banktransaction.bankbranch_id', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['bankbranch_id']?$banktransaction[0]['Banktransaction']['bankbranch_id']:0, 'class'=>''));
	//echo $this->Form->input('Banktransaction.bankaccounttype_id', array('type'=>'hidden', 'value'=>"", 'class'=>''));
	//echo $this->Form->input('Banktransaction.coa_id', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['coa_id']?$banktransaction[0]['Banktransaction']['coa_id']:1, 'class'=>''));
	echo"<h3>".__("Add New Purchase Products")."</h3>";
?>
	<div class="row text-center" style='overflow-x:scroll;'>
			<div class="col-sm-2">
				<?php
				echo $this->Form->input(
					"Order.orderdate",
					array(
						'type' => 'text',
						'div' => array('class'=> 'form-group'),
						'label' =>__('Date'),
						'tabindex'=>1,
						'placeholder' => __("Date"),
						'class'=> 'form-control',
						'data-validation-engine'=>'validate[required]',
						'style' => '',
						'value' => @$purchase[0]['Order']['orderdate']
					)
				);
				?>
			</div>
			<!-- /.col-sm-2 -->
			<div class="col-sm-2">
				<?php
				echo $this->Form->input(
					"Order.client_id",
					array(
						'type'=>'select',
						'div' => array('class'=> 'form-group'),
						"options"=>array($supplierlist),
						'empty'=>__('Select Supplier'),
						'tabindex'=>2,
						'label'=>__('Supplier'),
						//'class'=> 'form-control',
						'data-validation-engine'=>'validate[required]',
						'style'=>'',
						"selected" => (@$purchase[0]['Order']['client_id'] ? $purchase[0]['Order']['client_id'] : 0)
					)
				);
				?>
			</div>
			<!-- /.col-sm-2 -->
			<div class="col-sm-2">
				<?php
				echo $this->Form->input(
					"Order.orderreferenceno",
					array(
						'type' => 'text',
						'div' => array('class'=> 'form-group'),
						'label' =>__('Invoice'),
						'tabindex'=>3,
						'placeholder' => __("Invoice"),
						'class'=> 'form-control',
						'data-validation-engine'=>'validate[required]',
						'style' => '',
						'value' => @$purchase[0]['Order']['orderreferenceno']
					)
				);
				?>
			</div>
			<!-- /.col-sm-2 -->
			<div class="col-sm-2">
				<?php
				echo $this->Form->input(
					"Order.branch_id",
					array(
						'type'=>'select',
						'div' => array('class'=> 'form-group'),
						"options"=>array($salescenter),
						'empty'=>__('Select Stock Center'),
						'tabindex'=>4,
						'label'=>__('Stock Center'),
						//'class'=> 'form-control',
						'data-validation-engine'=>'validate[required]',
						'style'=>'',
						"selected" => (@$purchase[0]['Order']['branch_id'] ? $purchase[0]['Order']['branch_id'] : 0)
					)
				);
				?>
			</div>
			<!-- /.col-sm-2 -->
			<div class="col-sm-2">
				<?php
				echo $this->Form->input(
					"Order.salesman_id",
					array(
						'type'=>'select',
						'div' => array('class'=> 'form-group'),
						"options"=>array($salesmanlist),
						'empty'=>__('Select Salesman'),
						'tabindex'=>5,
						'label'=>__('Salesman'),
						'class'=> 'form-control',
						'data-validation-engine'=>'validate[required]',
						'style'=>'',
						"selected" => (@$purchase[0]['Order']['salesman_id'] ? $purchase[0]['Order']['salesman_id'] : 0)
					)
				);
				?>
				<span id="OrderClientSpan"></span>
			</div>
			<!-- /.col-sm-2 -->
	</div>
	<!-- .row -->

	<div class="row">
		<div class="table-responsive">
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>Item ID</th>
						<th>Item Name</th>
						<th>Quantity</th>
						<th>Unit Price</th>
						<th>Price</th>
						<th>Stock</th>
						<th>Unit</th>
						<th>Discount (%)</th>
						<th>Discount Value</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody id='content'>
					<tr id='mydivcontent'>
						<td>
						<?php
						echo $this->Form->input(
							"Orderdetail.productcode1",
							array(
								'type'=>'select',
								"options"=>array($productcodelist),
								'empty'=>__('Select Product Code'),
								'tabindex'=>6,
								'label'=>false,
								'class'=> 'form-control',
								'data-validation-engine'=>'validate[required]',
								'style'=>'',
								"selected" => (@$purchase[0]['Order']['productcode'] ? $purchase[0]['Order']['productcode'] : 0)
							)
						);
						?>
						</td>
						<td>
							<div class="form-group"  id="productchangediv">
							<?php
							echo $this->Form->input(
								"Orderdetail.product_id1",
								array(
									'type'=>'select',
									"options"=>array($productlist),
									'empty'=>__('Select Product Name'),
									'tabindex'=>7,
									'label'=>false,
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style'=>'',
									"selected" => (@$purchase[0]['Order']['product_id'] ? $purchase[0]['Order']['product_id'] : 0)
								)
							);
						echo $this->Form->input("Order.unitmon", array("type" => "hidden"));
						echo $this->Form->input("Order.unitkilogram", array("type" => "hidden"));
						echo $this->Form->input("Order.unitgram", array("type" => "hidden"));
						echo $this->Form->input("Order.stockmon", array("type" => "hidden"));
						echo $this->Form->input("Order.stockkilogram", array("type" => "hidden"));
						echo $this->Form->input("Order.stockgram", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailmon", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailkg", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailgram", array("type" => "hidden"));
						echo $this->Form->input("Order.productdafultprice", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailmonunitprice", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailkgunitprice", array("type" => "hidden"));
						echo $this->Form->input("Order.orderdetailgramunitprice", array("type" => "hidden"));
						?>
							</div>
							<span id="OrderbankAccountIdspan"></span>
						</td>
						<td>
							<?php
				echo $this->Form->input(
					"Orderdetail.quantity1",
					array(
						'type' => 'text',
						'div' => array('class'=> 'form-group'),
						'label'=>false,
						'tabindex'=>8,
						'placeholder' => __("Quantity"),
						'class'=> 'form-control',
						'data-validation-engine'=>'validate[required]',
						'style' => '',
						'value' => @$purchase[0]['Order']['orderdate'],
						'onkeyup' => 'priceandamountcalculation(1);'
					)
				);
				?>
							<span id="quantityspan"></span>
							<span id="price_span"></span>
						</td>
						<td>
							<?php
							echo $this->Form->input(
								"Orderdetail.unitprice1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'tabindex'=>9,
									'placeholder' => __("Unit Price"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$purchase[0]['Order']['orderdate'],
									'onkeyup' => 'priceandamountcalculation(1);'
								)
							);
							?>
							<span id="stocktspan"></span>
						</td>
						<td>
							<span id="pricespan">
							<?php
							echo $this->Form->input(
								"Orderdetail.price1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'tabindex'=>10,
									//'placeholder' => __("Unit Price"),
									'readonly' => 'readonly',
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$purchase[0]['Order']['orderdate']
								)
							);
							?>	
							</span>
						</td>
						<td><input type="text" class="form-control" id="stockunit" name="stockunit" placeholder="" readonly></td>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.unit_id1",
								array(
									'type'=>'select',
									"options"=>array($unitlist),
									'empty'=>__('Select Unit'),
									'tabindex'=>11,
									'label'=>false,
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style'=>'',
									"selected" => (@$purchase[0]['Order']['unit_id'] ? $purchase[0]['Order']['unit_id'] : 0)
								)
							);
						?>		
						<span id="productidloadspan"></span>	
						</td>
						
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.discount1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'tabindex'=>12,
									'placeholder' => __("Discount (%)"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$purchase[0]['Order']['discount'],
									'onkeyup' => 'priceandamountcalculation(1);'
								)
							);
						?>	
						</td>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.discountvalue1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'readonly' => 'readonly',
									'tabindex'=>13,
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$purchase[0]['Order']['discount']
								)
							);
						?>	
						</td>
						<td>
						<?php
							echo $this->Form->input(
								"Orderdetail.transactionamount1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'readonly' => 'readonly',
									'tabindex'=>14,
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$purchase[0]['Order']['transactionamount']
								)
							);
						?>		
						</td>
					</tr>
					</tbody>
			</table>
			<table class='table table-condensed table-hover table-responsive'>

				<tr>
					<td colspan="8">
						<input type="button" id="more_fields"  value="Add a Row" />
						<!--<input type="button" id="remove_fields" onclick="remove_field();" value="Remove More" />-->
						<input type="button" id="anc_rem" value="Remove a Row" />
						<!--<a href="javascript:void(0);" id="anc_rem">Remove Row</a>-->
						<input type="hidden" style="width:48px;" name="totlarow" id="totlarow" value="1" />
					</td>
				</tr>


			</table>
			<!--table for total amount start-->
			<table class="table table-condensed table-hover table-responsive">		
					<tr>
						<th class="text-right" style="width:85%">Total Price</th>
						<td>
						<?php
							echo $this->Form->input(
								"Order.ordertotal",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'readonly' => 'readonly',
									'tabindex'=>15,
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$purchase[0]['Order']['ordertotal']
								)
							);
						?>	
						</td>
					</tr>
					<tr>
						<th class="text-right" style="width:85%">Total Discount (-)</th>
						<td>
						<?php
							echo $this->Form->input(
								"Order.orderdiscount",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'readonly' => 'readonly',
									'tabindex'=>16,
									'class'=> 'form-control',
									'style' => '',
									'value' => @$purchase[0]['Order']['orderdiscount']
								)
							);
						?>
						</td>
					</tr>
					<tr>
						<th class="text-right" style="width:85%">Grand Total</th>
						<td>
						<?php
							echo $this->Form->input(
								"Order.ordergrandtotal",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'readonly' => 'readonly',
									'tabindex'=>17,
									'class'=> 'form-control',
									'style' => '',
									'value' => @$purchase[0]['Order']['ordergrandtotal']
								)
							);
						?>	
						</td>
					</tr>
					<tr>
						<th class="text-right" style="width:85%">Payment</th>
						<td>
						<?php
							echo $this->Form->input(
								"Order.orderpayment",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'tabindex'=>18,
									'class'=> 'form-control',
									//'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$purchase[0]['Order']['orderpayment']
								)
							);
						?>	
						</td>
					</tr>
					<tr>
						<th class="text-right" style="width:85%">Due</th>
						<td>
						<?php
							echo $this->Form->input(
								"Order.orderdue",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'readonly' => 'readonly',
									'tabindex'=>19,
									'class'=> 'form-control',
									'style' => '',
									'value' => @$purchase[0]['Order']['orderdue']
								)
							);
						?>	
						</td>
					</tr>
					<tr>
						<th class="text-right" style="width:85%">
						Due Amount<br>
						Payment Date
						</th>
						<?php //= 0){ ?>
						<td>
							<?php
							echo $this->Form->input(
								"Order.orderduepaymentdate",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label'=>false,
									'tabindex'=>20,
									'class'=> 'form-control',
									'style' => '',
									'value' => @$purchase[0]['Order']['orderduepaymentdate']
								)
							);
							?>
						</td>
						<?php //} ?>
					</tr>
			</table>
			<!--table for total amount end-->
		</div>
	</div>
	<!-- /.row -->

	<fieldset>
	

	

	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
			<?php
				echo $this->Form->input(
					"Order.ordercomment",
					array(
						'type' => 'textarea',
						'div' => array('class'=> 'form-group'),
						'label' =>__('Comment'),
						'tabindex'=>21,
						'placeholder' => __("Comment"),
						'class'=> 'form-control',
						'data-validation-engine'=>'validate[required]',
						'style' => '',
						'cols'=>30,
						'rows'=>10,
						'value' => @$purchase[0]['Order']['ordercomment']
					)
				);
			?>	
			</div>
			<?php
			echo $Utilitys->allformbutton('',$this->request["controller"],$page='purchase');
			echo $this->Form->end();
			?>
		</div>
		<!-- /.col-sm-6 -->
	</div>
	<!-- /.row -->
	</fieldset>
<!--</form> -->

<script>
 $('.sales-order-manage .cash-check-choice input[type="radio"]').on( 'change', function() {
		var target_div = $('.check-payment-enabled');
		if (this.value == '1') {
			target_div.slideDown('fast');
		} else {
	 		target_div.slideUp('fast');
	 	}
	 });
</script>
<?php echo "
<script>
	$(document).ready(function() {
		$(\"#OrderOrderdate\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd HH:mm:ss\", use24hours: true, showMeridian: false , startView: 2, minView: 2, autoclose: 1
		});
		$(\"#OrderOrderduepaymentdate\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd HH:mm:ss\", use24hours: true, showMeridian: false , startView: 2, minView: 2, autoclose: 1
		});
		$(\"#checkdate\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd HH:mm:ss\", use24hours: true, showMeridian: false , startView: 2, minView: 2, autoclose: 1
		});
		$(\"#OrderdetailProduct\").change(function(){
			var ProductId = $(this).val();
			$(\"#pricespan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.post(
					'".$this->webroot."purchases/purchasepricebyproductname',
					{ProductId:ProductId},
					function(result) {
						$(\"#pricespan\").html(result);
					}
				);
		});
		$(\"#OrderdetailProductId\").change(function(){	
				var OrderdetailProductId = $(this).val();
				var OrderBranchId = $(\"#OrderBranchId\").val();
				$(\"#productidloadspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
				       	cache: false,
				       	type: 'POST',
					url: '".$this->webroot."orders/qtyunitpricebyproduct',
					data: {OrderdetailProductId:OrderdetailProductId,OrderdetailBranchId:OrderBranchId},					
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderStock\").val(result.productquantity);
						var order_stock = result.productquantity;
						$(\"#stockunit\").val(result.productunitprice);
						var stock_unit = result.productunitprice;
						$(\"#OrderPrice\").val(result.producttotalprice);
						$(\"#OrderdetailProductCode\").val(result.productid);
						$(\"#OrderdetailUnit\").val(result.productunit);
						$(\"#unitmon\").val(result.unitmon);
						var unit_mon = result.unitmon;
						$(\"#unitkilogram\").val(result.unitkilogram);
						var unit_kilogram = result.unitkilogram;
						$(\"#unitgram\").val(result.unitgram);
						var unit_gram = result.unitgram;
						$(\"#stocktspan\").html('Remaining Stock : '+order_stock * unit_mon+' MON / '+order_stock * unit_kilogram+' KG / '+order_stock * unit_gram+' GRAM');
						var quantity = $(\"#OrderdetailQuantity\").val();
						$(\"#quantityspan\").html('Input Stock : '+quantity * unit_mon+' MON / '+quantity * unit_kilogram+' KG / '+quantity * unit_gram+' GRAM');
						$(\"#stockmon\").val(order_stock * unit_mon);
						$(\"#stockkilogram\").val(order_stock * unit_kilogram);
						$(\"#stockgram\").val(order_stock * unit_gram);
						$(\"#orderdetailmon\").val(result.orderdetailmon);
						$(\"#orderdetailkg\").val(result.orderdetailkg);
						$(\"#orderdetailgram\").val(result.orderdetailgram);
						$(\"#productdafultprice\").val(result.productdafultprice);
						
						
						//$(\"#price_span\").html('Price : '+quantity * productdafultprice+' TK / '+quantity * productdafultprice+' TK / '+quantity * productdafultprice+' TK');
						$(\"#productidloadspan\").html('');
					}
				});
			});
		$(\"#OrderdetailProductCode\").change(function(){
				var OrderdetailProductId = $(this).val();
				var OrderBranchId = $(\"#OrderBranchId\").val();
				$(\"#productidloadspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
				       	cache: false,
				       	type: 'POST',
					url: '".$this->webroot."orders/qtyunitpricebyproduct',
					data: {OrderdetailProductId:OrderdetailProductId,OrderdetailBranchId:OrderBranchId},					
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderStock\").val(result.productquantity);
						var order_stock = result.productquantity;
						$(\"#stockunit\").val(result.productunitprice);
						var stock_unit = result.productunitprice;
						$(\"#OrderPrice\").val(result.producttotalprice);
						$(\"#OrderdetailProductId\").val(result.productid);
						$(\"#OrderdetailUnit\").val(result.productunit);
						$(\"#OrdersPurchaseproductmon\").val(result.orderdetailmon);
						$(\"#OrdersPurchaseproductkg\").val(result.orderdetailkg);
						$(\"#OrdersPurchaseproductgram\").val(result.orderdetailgram);
						$(\"#unitmon\").val(result.unitmon);
						var unit_mon = result.unitmon;
						$(\"#unitkilogram\").val(result.unitkilogram);
						var unit_kilogram = result.unitkilogram;
						$(\"#unitgram\").val(result.unitgram);
						var unit_gram = result.unitgram;
						$(\"#stocktspan\").html('Remaining Stock : '+order_stock * unit_mon+' MON / '+order_stock * unit_kilogram+' KG / '+order_stock * unit_gram+' GRAM');
						var quantity = $(\"#OrderdetailQuantity\").val();
						$(\"#quantityspan\").html('Input Stock : '+quantity * unit_mon+' MON / '+quantity * unit_kilogram+' KG / '+quantity * unit_gram+' GRAM');
						$(\"#stockmon\").val(order_stock * unit_mon);
						$(\"#stockkilogram\").val(order_stock * unit_kilogram);
						$(\"#stockgram\").val(order_stock * unit_gram);
						$(\"#orderdetailmon\").val(result.orderdetailmon);
						$(\"#orderdetailkg\").val(result.orderdetailkg);
						$(\"#orderdetailgram\").val(result.orderdetailgram);
						$(\"#productdafultprice\").val(result.productdafultprice);
						var quantity = $(\"#OrderdetailQuantity\").val();
						//$(\"#price_span\").html('Price : '+quantity * productdafultprice+' TK / '+quantity * productdafultprice+' TK / '+quantity * productdafultprice+' TK');
						$(\"#productidloadspan\").html('');
					}
				});
			});
		$(\"#bankId\").change(function(){
			var bankId = $(this).val();
			$(\"#bankaccountchangediv\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.post(
					'".$this->webroot."purchases/bankaccountlistbybankid',
					{bankId:bankId},
					function(result) {
						$(\"#bankaccountchangediv\").html(result);
					}
				);
		});
		
		$(\"#OrderbankAccountId\").change(function(){
				var BankaccountId = $(this).val();
				$(\"#OrderbankAccountIdspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."banks/bankchequebookseriesbyaccount',
					data:{BankaccountId:BankaccountId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderBanktransactionchequebookseries\").html(result.bankchequebook_options);
						//$(\"#BanksPaymentrecive\").val(result.banktransactiontotalamount);
						$(\"#BanktransactionBankId\").val(result.bank_id);
						$(\"#BanktransactionBankbranchId\").val(result.bankbranch_id);
						$(\"#BanktransactionBankaccounttypeId\").val(result.bankaccounttype_id);
						$(\"#OrderbankAccountIdspan\").html('');
					}
				});
		});
		$(\"#OrderBanktransactionchequebookseries\").change(function(){
				var BanktransactionBanktransactionchequesequencynumber = $(this).val();
				$(\"#OrderBanktransactionchequebookseriesspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
				       	cache: false,
				       	type: 'POST',
					url: '".$this->webroot."banks/bankchequebooksequencybyseries',
					data: {BanktransactionBanktransactionchequesequencynumber:BanktransactionBanktransactionchequesequencynumber},					
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderCheckNo\").val(result.banktransaction_options);
						$(\"#BankBanktransactionId\").val(result.banktransaction_id);
						$(\"#BanksBanktransactionuuid\").val(result.banktransaction_uuid);
						$(\"#OrderBanktransactionchequebookseriesspan\").html('');
					}
				});
			});
		
		$(\"#OrderBranchId\").change(function(){
				var OrderBranchId = $(this).val();
				$(\"#OrderBranchIdspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."purchases/productlistbybranchid',
					data:{OrderBranchId:OrderBranchId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderdetailProductId\").html(result.product_options);
						$(\"#OrderdetailProductCode\").html(result.product_code_options);
						//$(\"#OrderBranchIdspan\").html('');
					}
				});
		});
		$(\"#OrderClientId\").change(function(){
				var OrderClientId = $(this).val();
				$(\"#OrderClientSpan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."purchases/salesmanbysupplierid',
					data:{OrderClientId:OrderClientId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderSalesmanId\").html(result.salesman_options);
						//$(\"#OrderClientSpan\").html('');
					}
				});
		});

	});	

</script>
";?>
<script  type="text/javascript">
	var room = 0;
	$('#more_fields').click(function(){
		room = parseInt(document.getElementById("totlarow").value);
		//alert(room);
        room = room+1;

        $("#content").append('<tr id="mydivcontent'+room+'"><td><div class="control-group"><select id="OrderdetailProductcode'+room+'" style="" data-validation-engine="validate[required]" tabindex="6" class="form-control" name="data[Orderdetail][productcode'+room+']"><option value="">Select Product Code</option><option value="1">112255</option></select></div></td><td><div id="productchangediv" class="form-group"><div class="control-group"><select id="OrderdetailProductId'+room+'" style="" data-validation-engine="validate[required]" tabindex="7" class="form-control" name="data[Orderdetail][product_id'+room+']"><option value="">Select Product Name</option><option value="1">Bangla / বাংলা</option></select></div><input type="hidden" id="OrderUnitmon'+room+'" class="input-xxlarge" name="data[Order][unitmon]"><input type="hidden" id="OrderUnitkilogram'+room+'" class="input-xxlarge" name="data[Order][unitkilogram]"><input type="hidden" id="OrderUnitgram'+room+'" class="input-xxlarge" name="data[Order][unitgram]"><input type="hidden" id="OrderStockmon'+room+'" class="input-xxlarge" name="data[Order][stockmon]"><input type="hidden" id="OrderStockkilogram'+room+'" class="input-xxlarge" name="data[Order][stockkilogram]"><input type="hidden" id="OrderStockgram'+room+'" class="input-xxlarge" name="data[Order][stockgram]"><input type="hidden" id="OrderOrderdetailmon'+room+'" class="input-xxlarge" name="data[Order][orderdetailmon]"><input type="hidden" id="OrderOrderdetailkg'+room+'" class="input-xxlarge" name="data[Order][orderdetailkg]"><input type="hidden" id="OrderOrderdetailgram'+room+'" class="input-xxlarge" name="data[Order][orderdetailgram]"><input type="hidden" id="OrderProductdafultprice'+room+'" class="input-xxlarge" name="data[Order][productdafultprice]"><input type="hidden" id="OrderOrderdetailmonunitprice'+room+'" class="input-xxlarge" name="data[Order][orderdetailmonunitprice]"><input type="hidden" id="OrderOrderdetailkgunitprice'+room+'" class="input-xxlarge" name="data[Order][orderdetailkgunitprice]"><input type="hidden" id="OrderOrderdetailgramunitprice'+room+'" class="input-xxlarge" name="data[Order][orderdetailgramunitprice]"></div><span id="OrderbankAccountIdspan"></span>	</td><td><div class="form-group"><input type="text" id="OrderdetailQuantity'+room+'" onkeyup="priceandamountcalculation('+room+');" style="" data-validation-engine="validate[required]" placeholder="Quantity" tabindex="8" class="form-control" name="data[Orderdetail][quantity'+room+']"></div><span id="quantityspan"></span><span id="price_span"></span></td><td><div class="form-group"><input type="text" id="OrderdetailUnitprice'+room+'" onkeyup="priceandamountcalculation('+room+');" style="" data-validation-engine="validate[required]" placeholder="Unit Price" tabindex="9" class="form-control" name="data[Orderdetail][unitprice'+room+']"></div><span id="stocktspan"></span></td><td><span id="pricespan"><div class="form-group"><input type="text" id="OrderdetailPrice'+room+'" style="" data-validation-engine="validate[required]" readonly="readonly" tabindex="10" class="form-control" name="data[Orderdetail][price'+room+']"></div></span></td><td><input type="text" readonly="" placeholder="" name="stockunit" id="stockunit" class="form-control"></td><td><div class="control-group"><select id="OrderdetailUnitId'+room+'" style="" data-validation-engine="validate[required]" tabindex="11" class="form-control" name="data[Orderdetail][unit_id'+room+']"><option value="">Select Unit</option><option value="1">Each / প্রতি</option></select></div><span id="productidloadspan"></span></td><td><div class="form-group"><input type="text" id="OrderdetailDiscount'+room+'" onkeyup="priceandamountcalculation('+room+');" style="" data-validation-engine="validate[required]" placeholder="Discount (%)" tabindex="12" class="form-control" name="data[Orderdetail][discount'+room+']"></div></td><td><div class="form-group"><input type="text" id="OrderdetailDiscountvalue'+room+'" style="" tabindex="13" readonly="readonly" class="form-control" name="data[Orderdetail][discountvalue'+room+']"></div></td><td><div class="form-group"><input type="text" id="OrderdetailTransactionamount'+room+'" style="" data-validation-engine="validate[required]" tabindex="14" readonly="readonly" class="form-control" name="data[Orderdetail][transactionamount'+room+']"></div></td></tr>');
			document.getElementById("totlarow").value=room;
		});	

	$("#anc_rem").click(function(){
		if($('#content tr').size()>1){
	 		$('#content tr:last-child').remove();
				room = room-1;
				//alert(room);
				document.getElementById("totlarow").value=room;
	 	}else{
	 		alert('One row should be present in table');
	 	}
	 });

</script>
<script type="text/javascript">
 $(document).ready(function(){
    $("#PurchasesPurchaseinsertupdateactionForm").validationEngine();	
    $('#OrderClientId').select2();
    $('#OrderBranchId').select2();
    $("#OrderOrderpayment").keyup(function(){
    	var grand_total = $("#OrderOrdergrandtotal").val();
    	var payment = $("#OrderOrderpayment").val();
    	var due = grand_total - payment;
    	$("#OrderOrderdue").val(due);
    });
 });
</script>
<script type="text/javascript">
function priceandamountcalculation(fieldid){
	var quantity = $("#OrderdetailQuantity"+fieldid).val();
	if(quantity ==''){
        quantity = 0;
    }else{
        quantity = parseFloat(quantity);
    }
	var unit_price = $("#OrderdetailUnitprice"+fieldid).val();
	if(unit_price ==''){
        unit_price = 0;
    }else{
        unit_price = parseFloat(unit_price);
    }

    $("#OrderdetailPrice"+fieldid).val(quantity * unit_price);
    //var price = document.getElementById("OrderdetailPrice"+fieldid);
	//price.value = quantity * unit_price;

	//var price_value = document.getElementById("OrderdetailPrice"+fieldid).value;
	var price = $("#OrderdetailPrice"+fieldid).val();

	var discount_rate = $("#OrderdetailDiscount"+fieldid).val();
    if(discount_rate =='' || discount_rate == 'NaN'){
        discount_rate = 0;
    }else{
        discount_rate = parseFloat(discount_rate);
    }
    var discount_amount = ((price*discount_rate)/100);
    var amount_value = price - discount_amount;
    //var amount =  document.getElementById("OrderdetailTransactionamount"+fieldid);
    //amount.value = amount_value;
    $("#OrderdetailDiscountvalue"+fieldid).val(discount_amount);
    $("#OrderdetailTransactionamount"+fieldid).val(amount_value);

    var total_amount = 0;
    row = parseInt(document.getElementById("totlarow").value);
    for(i = 1; i<=row; i++){
    	total_amount += parseFloat(document.getElementById("OrderdetailPrice"+i).value);
    }
    $("#OrderOrdertotal").val(total_amount);

    var total_discount_amount = 0
    for(j =1; j <= row; j++){
    	total_discount_amount += parseFloat(document.getElementById("OrderdetailDiscountvalue"+j).value);
    }
    $("#OrderOrderdiscount").val(total_discount_amount);

    var bonus_discount = $("#OrderOrderdiscount").val();
    if(bonus_discount ==''){
        bonus_discount = 0;
    }else{
        bonus_discount = parseFloat(bonus_discount);
    }
    var grand_total = total_amount - bonus_discount;
    $("#OrderOrdergrandtotal").val(grand_total);

    var payment = $("#OrderOrderpayment").val();
    if(payment == ''){
    	payment = 0;
    }else{
    	payment = parseFloat(payment);
    }
    var due = grand_total - payment;
    $("#OrderOrderdue").val(due);
}
</script>