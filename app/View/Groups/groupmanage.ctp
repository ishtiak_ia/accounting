<?php
	echo "<h2>".__('Group'). $Utilitys->addurl($title=__("Add New Group"), $this->request['controller'], $page="group")."</h2>";	
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Group.searchtext",
			array(
				'type' => 'text', 
				'div' => false,
				'label' => false,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
				)
			);
	echo $Utilitys->filtertagend();	
	echo "
		<div id=\"groupsarchloading\">
	";
	
	echo $Utilitys->paginationcommon();
					echo $Utilitys->tablestart();
						echo $this->Html->tableHeaders(
							array(
								__('SL#'), 
								array(
									__('Group Name')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Is Parmanent')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Is Active')  => array('class' => 'highlight sortable')
								), 
								__('Action')
							)
						);
						$grouprows = array();
						$countGroup = 0-1+$this->Paginator->counter('%start%');
						foreach($groups as $Thisgroup):
							$countGroup++;
							$grouprows[]= array($countGroup,$Thisgroup["Group"]["groupname"],$Thisgroup["Group"]["isPermanent"],$Utilitys->statusURL($this->request["controller"], 'group', $Thisgroup["Group"]["id"], $Thisgroup["Group"]["groupuuid"], $Thisgroup["Group"]["groupisactive"]),$Utilitys->cusurl($this->request['controller'], 'group', $Thisgroup["Group"]["id"], $Thisgroup["Group"]["groupuuid"]));
						endforeach;
						echo $this->Html->tableCells(
							$grouprows
						);
				echo $Utilitys->tableend();
				echo $Utilitys->paginationcommon();

	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				$(\"#GroupSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#groupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."groups/groupsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#groupsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>