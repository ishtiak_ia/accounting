<?php 
	echo"
		<table>
			<tr>
				<td>
					".__('Add Chart of Account Type information')."
				</td>
			</tr>
			<tr>
				<td>
	";
	echo $this->Form->create('Coas', array('action' => 'coatypeinsertupdateaction'));
		echo $this->Form->inpute('id', array('type'=>'hidden', 'value'=>@$coatype[0]['Coatype']['id']));
		echo $this->Form->inpute('coatypeuuid', array('type'=>'hidden', 'value'=>@$coatype[0]['Coatype']['coatypeuuid']));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$coatype[0]['Coatype']['coatypeisactive'] ? $coatype[0]['Coatype']['coatypeisactive'] : 0), 
			'class' => 'radio inline'
		);
	echo"
					<table>
						<tr>
							<td>".__("CODE")."</td>
							<td>
								".$this->Form->input(
									"Coatype.coatypecode",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'data-validation-engine'=>'validate[required]',
										'value' =>@$coatype[0]['Coatype']['coatypecode']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("Type")."</td>
							<td>
								".$this->Form->input(
									"Coatype.coatypename",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'data-validation-engine'=>'validate[required]',
										'value' =>@$coatype[0]['Coatype']['coatypename']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("Type BN")."</td>
							<td>
								".$this->Form->input(
									"Coatype.coatypenamebn",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'data-validation-engine'=>'validate[required]',
										'value' => @$coatype[0]['Coatype']['coatypenamebn']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("isActive")."</td>
							<td>
								".$this->Form->radio(
									'Coatype.coatypeisactive', 
									$options, 
									$attributes
								)."
							</td>
						</tr>
						<tr>
							<td colspan=\"2\">
								".$Utilitys->allformbutton('',$this->request["controller"],$page='coatype')."
							</td>
						</tr>
					</table>
	";
	echo $this->Form->end();
	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				jQuery(\"#CoasCoatypeinsertupdateactionForm\").validationEngine({
					'custom_error_messages': {
						'required': {
							'message': \"".__("This field is mandatory.")."\"
						}
					}
				});    
			});
		</script>
	";
?>