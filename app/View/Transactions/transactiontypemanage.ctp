<?php
	echo "<h2>".__('Transaction Type'). $Utilitys->addurl($title=__("Add New Transaction Type"), $this->request["controller"], $page="transactiontype")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Transactiontype.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
			)
		);
	echo $Utilitys->filtertagend();
	echo"
		<div id=\"transactiontypesarchloading\">
	";
		echo $Utilitys->paginationcommon();
		echo $Utilitys->tablestart($class=null, $id=null);
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Transaction Type')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Transaction Type BN')  => array('class' => 'highlight sortable')
					), 
					__('Active'), 
					__('Action')
				)
			);
			$transactiontyperows = array();
			$countTransactiontype = 0-1+$this->Paginator->counter('%start%');
			foreach($transactiontype as $Thistransactiontype):
				$countTransactiontype++;
				$transactiontyperows[]= array($countTransactiontype,$Thistransactiontype["Transactiontype"]["transactiontypename"],$Thistransactiontype["Transactiontype"]["transactiontypenamebn"],$Utilitys->statusURL($this->request["controller"], 'transactiontype', $Thistransactiontype["Transactiontype"]["id"], $Thistransactiontype["Transactiontype"]["transactiontypeuuid"], $Thistransactiontype["Transactiontype"]["transactiontypeisactive"]),$Utilitys->cusurl($this->request["controller"], 'transactiontype', $Thistransactiontype["Transactiontype"]["id"], $Thistransactiontype["Transactiontype"]["transactiontypeuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$transactiontyperows
			);
		echo $Utilitys->tableend();
		echo $Utilitys->paginationcommon();
	echo"
		</div>
		<script>
			$(document).ready(function() {
				$(\"#TransactiontypeSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#transactiontypesarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."transactions/transactiontypesearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#transactiontypesarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>