<?php 
echo"
		<table>
			<tr>
				<td>
					".__('Add Chart of Account Sub Class information')."
				</td>
			</tr>
			<tr>
				<td>
	";
	echo $this->Form->create('Coas', array('action' => 'coaclasssubclassinsertupdateaction'));
		echo $this->Form->inpute('id', array('type'=>'hidden', 'value'=>@$coaclasssubclass[0]['Coaclasssubclass']['id']));
		echo $this->Form->inpute('Coaclasssubclassuuid', array('type'=>'hidden', 'value'=>@$coaclasssubclass[0]['Coaclasssubclass']['coaclasssubclassuuid']));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$coaclasssubclass[0]['Coaclasssubclass']['coaclasssubclassisactive'] ? $coaclasssubclass[0]['Coaclasssubclass']['coaclasssubclassisactive'] : 0), 
			'class' => 'radio inline'
		);
					echo $Utilitys->tablestart($class=null, $id=null);
	echo"
						<tr>
							<td>".__("Type")."</td>
							<td>
								".$this->Form->input(
									"Coaclasssubclass.coatype_code_id",
									array(
										'type'=>'select',
										"options"=>array($coatype_code),
										'empty'=>__('Select Code'),
										'div'=>false,
										'tabindex'=>1,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coaclasssubclass[0]['Coaclasssubclass']['coatype_id'] ? $coaclasssubclass[0]['Coaclasssubclass']['coatype_id'] : 0)
									)
								)."
							</td>
							<td>
								".$this->Form->input(
									"Coaclasssubclass.coatype_id",
									array(
										'type'=>'select',
										"options"=>array($coatype),
										'empty'=>__('Select Type'),
										'div'=>false,
										'tabindex'=>2,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coaclasssubclass[0]['Coaclasssubclass']['coatype_id'] ? $coaclasssubclass[0]['Coaclasssubclass']['coatype_id'] : 0)
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("Group")."</td>
							<td>
								".$this->Form->input(
									"Coaclasssubclass.coagroup_code_id",array(
										'type'=>'select',
										"options"=>array($coagroup_code),
										'empty'=>__('Select Code'),
										'div'=>false,
										'tabindex'=>3,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coaclasssubclass[0]['Coaclasssubclass']['coagroup_id'] ? $coaclasssubclass[0]['Coaclasssubclass']['coagroup_id'] : 0)
									)
								)."								
							</td>
							<td>
								".$this->Form->input(
									"Coaclasssubclass.coagroup_id",array(
										'type'=>'select',
										"options"=>array($coagroup),
										'empty'=>__('Select Group'),
										'div'=>false,
										'tabindex'=>4,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coaclasssubclass[0]['Coaclasssubclass']['coagroup_id'] ? $coaclasssubclass[0]['Coaclasssubclass']['coagroup_id'] : 0)
									)
								)."								
							</td>
						</tr>
						<tr>
							<td>".__("Class")."</td>
							<td>
								".$this->Form->input(
									"Coaclasssubclass.coaclass_code_id",array(
										'type'=>'select',
										"options"=>array($coaclass_code),
										'empty'=>__('Select Code'),
										'div'=>false,
										'tabindex'=>5,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coaclasssubclass[0]['Coaclasssubclass']['coaclass_id'] ? $coaclasssubclass[0]['Coaclasssubclass']['coaclass_id'] : 0)
									)
								)."
								<span id=\"subclassloading\"></span>
							</td>
							<td>
								".$this->Form->input(
									"Coaclasssubclass.coaclass_id",array(
										'type'=>'select',
										"options"=>array($coaclass),
										'empty'=>__('Select Class'),
										'div'=>false,
										'tabindex'=>6,
										'label'=>false,
										'style'=>'',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$coaclasssubclass[0]['Coaclasssubclass']['coaclass_id'] ? $coaclasssubclass[0]['Coaclasssubclass']['coaclass_id'] : 0)
									)
								)."
								<span id=\"subclassloading\"></span>
							</td>
						</tr>
						<tr>
							<td>".__("Sub Class Code")."</td>
							<td>
								".$this->Form->input(
									"Coaclasssubclass.coaclasssubclasscode",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'tabindex'=>6,
										'data-validation-engine'=>'validate[required]',
										'value' =>@$coaclasssubclass[0]['Coaclasssubclass']['coaclasssubclasscode']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("Sub Class Name")."</td>
							<td>
								".$this->Form->input(
									"Coaclasssubclass.coaclasssubclassname",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'tabindex'=>7,
										'data-validation-engine'=>'validate[required]',
										'value' =>@$coaclasssubclass[0]['Coaclasssubclass']['coaclasssubclassname']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("sub Classs Name BN")."</td>
							<td>
								".$this->Form->input(
									"Coaclasssubclass.coaclasssubclassnamebn",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'tabindex'=>8,
										'data-validation-engine'=>'validate[required]',
										'required' => 'required',
										'value' => @$coaclasssubclass[0]['Coaclasssubclass']['coaclasssubclassnamebn']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("isActive")."</td>
							<td>
								".$this->Form->radio(
									'Coaclasssubclass.coaclasssubclassisactive', 
									$options, 
									$attributes
								)."
							</td>
						</tr>
						<tr>
							<td colspan=\"2\">
								".$Utilitys->allformbutton('',$this->request["controller"],$page='coa')."
							</td>
						</tr>
					</table>
	";
	echo $this->Form->end();
	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				$(\"#CoaclasssubclassCoatypeCodeId\").select2();
				$(\"#CoaclasssubclassCoatypeId\").select2();
				$(\"#CoaclasssubclassCoagroupCodeId\").select2();
				$(\"#CoaclasssubclassCoagroupId\").select2();
				$(\"#CoaclasssubclassCoaclassCodeId\").select2();
				$(\"#CoaclasssubclassCoaclassId\").select2();
				jQuery(\"#CoasCoasubclassinsertupdateactionForm\").validationEngine({
					'custom_error_messages': {
						'required': {
							'message': \"".__("This field is mandatory.")."\"
						}
					}
				}); 

				$(\"#CoaclasssubclassCoatypeId\").change(function(){
					var coatype_id = $(this).val(),type;
					$(\"#subclassloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/listcoagroup/'+coatype_id, {coatype_id:coatype_id},
					function(result) {
						$(\"#CoaclasssubclassCoagroupId\").html(result);
						$(\"#subclassloading\").html('');
						$('#CoaclasssubclassCoatypeCodeId option[value='+coatype_id+']').attr('selected', 'selected');
						type = $('#CoaclasssubclassCoatypeCodeId option:selected').text();
                    	$('#s2id_CoaclasssubclassCoatypeCodeId .select2-chosen' ).text( type );
					});
				});


				$(\"#CoaclasssubclassCoagroupId\").change(function(){
					var coatype_id = $(\"#CoaclasssubclassCoatypeId\").val(),group;
					var coagroup_id = $(\"#CoaclasssubclassCoagroupId\").val();
					if(coatype_id !='' && coatype_id == 0){
						coatype_id = coatype_id;
					}else{
						coatype_id = 0;
					}
					$(\"#subclassloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/listcoaclass/'+coatype_id+'/'+coagroup_id, {coatype_id:coatype_id, coagroup_id:coagroup_id},
					function(result) {
						show = result.split(\"-\");
						$(\"#CoaclasssubclassCoaclassId\").html(show[0]);
						$(\"#subclassloading\").html('');
						$('#CoaclasssubclassCoagroupCodeId option[value='+coagroup_id+']').attr('selected', 'selected');
						group = $('#CoaclasssubclassCoagroupCodeId option:selected').text();
                    	$('#s2id_CoaclasssubclassCoagroupCodeId .select2-chosen' ).text( group );

					});
				});

				$(\"#CoaclasssubclassCoatypeCodeId\").change(function(){
                var coa_type_code_id = $(this).val(),type_code;
                $.post(
                    '".$this->webroot."coas/getCode',
                    {coa_type_code_id:coa_type_code_id},
                    function(result) {
                    	$('#CoaclasssubclassCoatypeId option[value='+result+']').attr('selected', 'selected');
                    	type_code = $('#CoaclasssubclassCoatypeId option:selected').text();
                    	$('#s2id_CoaclasssubclassCoatypeId .select2-chosen' ).text( type_code );

                    }
                );
                
            });

		    $(\"#CoaclasssubclassCoagroupCodeId\").change(function(){
                var coa_group_code_id = $(this).val(),group_code;
                $.post(
                    '".$this->webroot."coas/getCode',
                    {coa_group_code_id:coa_group_code_id},
                    function(result) {
                    	$('#CoaclasssubclassCoagroupId option[value='+result+']').attr('selected', 'selected');
                    	group_code = $('#CoaclasssubclassCoagroupId option:selected').text();
                    	$('#s2id_CoaclasssubclassCoagroupId .select2-chosen' ).text( group_code );

                    }
                );
                
            });

			$(\"#CoaclasssubclassCoaclassCodeId\").change(function(){
                var coa_class_code_id = $(this).val(),class_code;
                $.post(
                    '".$this->webroot."coas/getCode',
                    {coa_class_code_id:coa_class_code_id},
                    function(result) {
                    	$('#CoaclasssubclassCoaclassId option[value='+result+']').attr('selected', 'selected');
                    	class_code = $('#CoaclasssubclassCoaclassId option:selected').text();
                    	$('#s2id_CoaclasssubclassCoaclassId .select2-chosen' ).text( class_code );


                    }
                );
                
            });

			$(\"#CoaclasssubclassCoaclassId\").change(function(){
                var coa_class_name_id = $(this).val(),coa_class;
                $('#CoaclasssubclassCoaclassCodeId option[value='+coa_class_name_id+']').attr('selected', 'selected');
                coa_class = $('#CoaclasssubclassCoaclassCodeId option:selected').text();
                $('#s2id_CoaclasssubclassCoaclassCodeId .select2-chosen' ).text( coa_class);
            });


		});
		</script>
	";


?>
