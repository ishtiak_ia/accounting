<?php
echo $this->Form->create('Users', array('action' => 'userrelationshipinsertupdateaction', 'type' => 'file'));
	echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$userrelationship[0]["Userrelationship"]['id']));
	echo $this->Form->input('userrelationshipuuid', array('type'=>'hidden', 'value'=>@$userrelationship[0]["Userrelationship"]['userrelationshipuuid']?$userrelationship[0]["Userrelationship"]['userrelationshipuuid']:0, 'class'=>''));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$userrelationship[0]['Userrelationship']['userrelationshipisactive'] ? $userrelationship[0]['Userrelationship']['userrelationshipisactive'] : 0),
			'class' => 'form-inline'
		);
	echo"<h3>".__("Add Relationship")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-11\">";
			echo $this->Form->input(
				"Userrelationship.userrelationshipname",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Relationship Name&nbsp;<span style=\'color:#cc0000\'>*</span>'),
					'tabindex'=>2,
					'placeholder' => __("Relationship Name"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style' => '',
					'value' => @$userrelationship[0]['Userrelationship']['userrelationshipname']
				)
			);
			echo"<div class=\"form-group\">";
				echo"<label>isActive?</label><br>";
				echo $this->Form->radio(
					'Userrelationship.userrelationshipisactive', 
					$options,
					$attributes
				);
			echo"</div>";
			echo $Utilitys->allformbutton('',$this->request["controller"],$page='userrelationship');
		echo"</div>";
	echo"</div>";
echo $this->Form->end();
echo"
<script>
			$(document).ready(function() {
				$(\"#UsersUserrelationshipinsertupdateactionForm\").validationEngine();
			});
		</script>
	";
?>