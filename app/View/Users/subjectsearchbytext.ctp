<?php 
echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Subject Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Subject Name BNG')  => array('class' => 'highlight sortable')
					),
					__('Active'), 
					__('Action')
				)
			);
			$subjectrows = array();
			$countSubject = 0-1+$this->Paginator->counter('%start%');
			foreach($subject as $Thissubject):
				$countSubject++;
				$subjectrows[]= array($countSubject,$Thissubject["Subject"]["subjectname"],$Thissubject["Subject"]["subjectnamebn"],$Utilitys->statusURL($this->request["controller"], 'subject', $Thissubject["Subject"]["id"], $Thissubject["Subject"]["subjectuuid"], $Thissubject["Subject"]["subjectisactive"]),$Utilitys->cusurl($this->request["controller"], 'subject', $Thissubject["Subject"]["id"], $Thissubject["Subject"]["subjectuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$subjectrows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#SubjectSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#subjectsearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#subjectsearchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>	