<script>
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><input type="text" class="form-control" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })

    var max_fields1      = 10; //maximum input boxes allowed
    var wrapper1         = $(".input_fields_wrap1"); //Fields wrapper
    var add_button1      = $(".add_field_button1"); //Add button ID
   
    var y = 1; //initlal text box count
    $(add_button1).click(function(e){ //on add input button click
        e.preventDefault();
        if(y < max_fields1){ //max input box allowed
            y++; //text box increment
            $(wrapper1).append('<div><input type="text" class="form-control" name="mytext1[]"/><a href="#" class="remove_field1">Remove</a></div>'); //add input box
        }
    });
   
    $(wrapper1).on("click",".remove_field1", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>
<?php 
	echo $this->Form->create('Branches', array('action' => 'branchinsertupdateaction', 'type' => 'file'));
	echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$branch[0]['Branch']['id']));
	echo $this->Form->input('branchuuid', array('type'=>'hidden', 'value'=>@$branch[0]['Branch']['branchuuid']));
	$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$branch[0]['Branch']['branchisactive'] ? $branch[0]['Branch']['branchisactive'] : 0), 
			'class' => 'form-inline'
		);
	echo"<h3>".__("Add Branch Information")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-8\">";
		
echo $Utilitys->tablestart($class=null, $id=null);
	echo $this->Form->input(
		"Branch.company_id",array(
			'type'=>'select',
			"options"=>array($company),
			'empty'=>__('Select Company'),
				'div'=>array('class'=>'form-group'),
				'tabindex'=>1,
				'label'=>__("Company"),
				'style'=>'',
				'data-validation-engine'=>'validate[required]',
				'class'=> 'form-control',
				'selected'=> (@$branch[0]['Branch']['company_id'] ? $branch[0]['Branch']['company_id'] : 0)
			)
		);
		echo $this->Form->input(
			"Branch.branchname",
			array(
				'type' => 'text', 
				'div' => array('class'=>'form-group'), 
				'label' => __("Name(English)"),
				'class'=> 'form-control',
				'data-validation-engine'=>'validate[required]',
				'value' =>@$branch[0]['Branch']['branchname']
			)
		);
		echo $this->Form->input(
			"Branch.branchnamebn",
			array(
				'type' => 'text', 
				'div' => array('class'=>'form-group'), 
				'label' => __("Name(Bangla)"),
				'class'=> 'form-control',
				'data-validation-engine'=>'validate[required]',
				'value' => @$branch[0]['Branch']['branchnamebn']
			)
		);
		echo $this->Form->input(
			"Branch.branchcode",
			array(
				'type' => 'text', 
				'div' => array('class'=>'form-group'), 
				'label' => __("Code"),
				'class'=> 'form-control',
				'data-validation-engine'=>'validate[required]',
				'value' => @$branch[0]['Branch']['branchcode']
			)
		);
		echo $this->Form->input(
			"Branch.branchpresentaddress",
			array(
				'type' => 'text', 
				'div' => array('class'=>'form-group'), 
				'label' => __("Address(Present)"),
				'class'=> 'form-control',
				'data-validation-engine'=>'validate[required]',
				'value' => @$branch[0]['Branch']['branchpresentaddress']
			)
		);
		echo $this->Form->input(
			"Branch.branchpermanentaddress",
			array(
				'type' => 'text', 
				'div' => array('class'=>'form-group'), 
				'label' => __("Address(Permanent)"),
				'class'=> 'form-control',
				'data-validation-engine'=>'validate[required]',
				'value' => @$branch[0]['Branch']['branchpermanentaddress']
			)
		);
		echo $Utilitys->tablestart($class=null, $id=null);
		echo"
			<tr>
				<td>".__("Email Address")."</td>
				<td>
					<div class=input_fields_wrap>
		";
?>
<?php 
						if(isset($branchemail)){
							foreach ($branchemail as $thisbranchemail) {
								echo"
								<div>
								<input type=\"text\" class=\"form-control\" name=\"mytext[]\" value=\"".$thisbranchemail."\">
								<a href=\"#\" class=\"remove_field\">Remove</a>
								</div>
								";
							}?>
              <?php  if($branchemail == NULL){?>
                    <div><input type="text" class="form-control" name="mytext[]"></div>
                    <?php
                     }
            }
             else{ ?>
                    <div><input type="text" class="form-control" name="mytext[]"></div>
              <?php } ?>      
                </div>
                    <button class="add_field_button">Add More Fields</button>
            </td>
        </tr>

        <tr>
     <?php echo " <td>".__("Phone")."</td> ";?>
            <td>
                <div class="input_fields_wrap1">
            <?php if (isset($branchphone)){
                foreach ($branchphone as $thisbranchphone) {
                    echo"
                        <div><input type=\"text\" class=\"form-control\" name=\"mytext1[]\" value=\"".$thisbranchphone."\"><a href=\"#\" class=\"remove_field1\">Remove</a></div>

                    ";
                }
                if($branchphone == NULL){ 
                	?>
                	<div><input type="text" class="form-control" name="mytext1[]"></div>
                	<?php 
                }
            }
            else{
            	?>
            	<div><input type="text" class="form-control" name="mytext1[]"></div>
            	<?php
            	 }
            ?>
                </div>
                    <button class="add_field_button1">Add More Fields</button>
            </td>
            <?php echo "
        </tr>
					</table>
	";
		echo"<div class=\"form-group\">";
			echo"<label>isActive?</label><br>";
			echo $this->Form->radio(
				'Branch.branchisactive', 
				$options,
				$attributes
			);
		echo"</div>";

		echo $Utilitys->allformbutton('',$this->request["controller"],$page='branch');
		echo"</div>";
	echo"</div>";
	echo $this->Form->end();
?>
<script type="text/javascript">
 jQuery(document).ready(function(){
    jQuery("#BranchesBranchinsertupdateactionForm").validationEngine();
 });
</script> 