<?php
 echo $this->Html->script('order/order', array('inline' => true)); 
?>
<form class="sales-order-manage" name="saleProduct" id="saleProduct" action="orderaddeditaction" method="post">
<style type="text/css">
	
	.tableWidth{width:100px;}
	.tableWidthBig{width:170px;}
	.tableWidthSmall{width:60px;}
</style>

<?php
	//echo $this->Form->input('OrderUnitId', array('type'=>'hidden', 'value'=>0));
	echo"<h3>".__("Add New Sales Products")."</h3>";
?>
	<div class="row">
	<!-- /.col-sm-2 -->
			<div class="col-sm-2">
				<div class="form-group">
					<label for="date">Date</label>
					<!-- <input type="date" class="form-control" id="date" placeholder=""> -->
					<input type="date" name = "OrderOrderdate" class="form-control validate[required]" id="OrderOrderdate" placeholder="">
				</div>
			</div>

			<!-- /.col-sm-2 -->
			<div class="col-sm-2">
				<div class="form-group">
					<label for="invoice">Invoice</label>
					<input type="text" class="form-control validate[required]" name = "OrderReferenceno" id="OrderReferenceno" placeholder="NP-045043643">
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<label for="customer-id">Customer ID</label>
					<!-- <input type="text" class="form-control" id="customer-id" placeholder=""> -->
					<select name="OrderClientId" id="OrderClientId" class="form-control validate[required]">
						<option value=" ">Select Customer</option>
						<?php if(isset($customerlist)){
							foreach($customerlist as $key => $value){?>
								<option value="<?php echo $key ?>"><?php echo $key?></option>
						<?php	}}?>
					</select>
				</div>
			</div>

			<div class="col-sm-2">
				<div class="form-group">
					<label for="customer-id">Customer Name</label>
					<!-- <input type="text" class="form-control" id="customer-id" placeholder=""> -->
					<select name="OrderClientId" id="OrderClientId" class="form-control validate[required]">
						<option value=" ">Select Customer</option>
						<?php if(isset($customerlist)){
							foreach($customerlist as $key => $value){?>
								<option value="<?php echo $value ?>"><?php echo $value?></option>
						<?php	}}?>
					</select>
				</div>
			</div>


			<!-- /.col-sm-2 -->
			<div class="col-sm-2">
				<div class="form-group">
					<label for="sales-center">Sales Center</label>
					<!-- <input type="text" class="form-control" id="sales-center" placeholder=""> -->
					<select name="OrderBranchId" id="OrderBranchId" class="form-control validate[required]">
						<option value=" ">Select Sales Center</option>
						<?php if(isset($salescenter)){
							foreach($salescenter as $key => $value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
					</select>
				</div>
			</div>
			<!-- /.col-sm-2 -->
			<div class="col-sm-2">
				<div class="form-group">
					<label for="salesman">Salesman ID</label>
					<select name="OrderSalesmanId" id="OrderSalesmanId" class="form-control validate[required]">
						<option value=" ">Select Salesman</option>
						<?php if(isset($salesmanlist)){
							foreach($salesmanlist as $key => $value){?>
								<option value="<?php echo $key ?>"><?php echo $key?></option>
						<?php	}}?>
					</select>
				</div>
				<span id="OrderClientSpan"></span>
			</div>

			<!-- /.col-sm-2 -->
			<div class="col-sm-2">
				<div class="form-group">
					<label for="salesman">Salesman Name</label>
					<select name="OrderSalesmanId" id="OrderSalesmanId" class="form-control validate[required]">
						<option value=" ">Select Salesman</option>
						<?php if(isset($salesmanlist)){
							foreach($salesmanlist as $key => $value){?>
								<option value="<?php echo $value ?>"><?php echo $value?></option>
						<?php	}}?>
					</select>
				</div>
				<span id="OrderClientSpan"></span>
			</div>
			<!-- /.col-sm-2 -->
	</div>
	<!-- .row -->

	<div class="row">
		<div class="table-responsive">
			<!--<table>
    <tr><td>Item Type</td><td>Item</td><td>Add/Remove Item</td></tr>
    <tr><td><input type='text' value='Fruit >'></td>
        <td><input type='text' value='Apple'></td>
        <td><input type='button' class='AddNew' value='Add new item'></td></tr>
</table>

 <input type='button' id="add_moallem_field" class='button' value='Add new item'> 
<button id="add_moallem_field" class="button" >+ ADD</button>-->

			<table id="hajji-meta-table" class="table table-condensed table-hover">
				<thead>
					<tr>
						<th class="tableWidth">Item ID</th>
						<th class="tableWidthBig">Item Name</th>
						<th class="tableWidth">Stock</th>
						<th class="tableWidth">Stock Unit Price</th>
						<th class="tableWidth">Stock Price</th>
						<th class="tableWidth">Quantity</th>
						<th class="tableWidth">Quantity Unit Price</th>
						<th class="tableWidth">Quantity Price</th>
						<th class="tableWidth">Discount</th>
						<th class="tableWidth">Amount</th>
					</tr>
				</thead>
				<tbody>
					<tr class="sales-item-row">
						<td colspan="10">
							<table id="content" >
								<tr  id="mydivcontent">
									<td>
										<select name="OrderdetailProductCode" id="OrderdetailProductCode" class="form-control" >
											<option value="0">Select Product Code</option>
										<?php  
												if(isset($productcodelist)){
													foreach($productcodelist as $key=>$productcode){
										?>
											<option value="<?php echo $key; ?>"><?php echo $productcode;?></option>
										<?php		}	
												}
										?>
										</select>
									</td>
									<td>
										<select name="OrderdetailProductId" id="OrderdetailProductId" class="form-control" >
											<option value="0">Select Product</option>
												<?php 
													if(isset($productlist)){
														foreach($productlist as $key => $value){?>
														<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php
														}
													}
						?>
										</select>
										<span id="productidloadspan"></span>
									<td>
										<input type="hidden" class="form-control" id="OrderStock" name="OrderStock" readonly = "readonly">
									 	<input type="hidden"  name="UnitMon" id="UnitMon" >
									 	<input type="hidden"  name="UnitKg" id="UnitKg" >
									 	<input type="hidden"  name="UnitGram" id="UnitGram" >

									 	<input type="hidden"  name="OrderdetailUnitMon" id="OrderdetailUnitMon" >
									 	<input type="hidden"  name="OrderdetailUnitKg" id="OrderdetailUnitKg" >
									 	<input type="hidden"  name="OrderdetailUnitGram" id="OrderdetailUnitGram" >
						 				<input type="hidden"  name="OrderdetailMonUnitPrice" id="OrderdetailMonUnitPrice" >
									 	<input type="hidden"  name="OrderdetailKgUnitPrice" id="OrderdetailKgUnitPrice" >
									 	<input type="hidden"  name="OrderdetailGramUnitPrice" id="OrderdetailGramUnitPrice" >
									 	<input type="hidden"  name="OrderUnitId" id="OrderUnitId" >
									</td>
									<td><input type="text" class="form-control" id="stockunit" name="stockunit" readonly = "readonly"></td>
									<td><input type="text" class="form-control" id="OrderPrice" name="OrderdetailPrice" readonly = "readonly"></td>
									<td><input type="text" class="form-control" name="OrderdetailQuantity" id="OrderdetailQuantity"></td>
									<td><input type="text" class="form-control" name="OrderdetailUnitprice" id="OrderdetailUnitprice"></td>
									<td><input type="text" class="form-control" name="Orderdetailprice2" id="Orderdetailprice"></td>
								  	<td><input type="text" class="form-control" id="OrderDiscount" name="OrderdetailDiscount"></td>
									<td><input type="text" class="form-control" id="OrderAmount" name="OrderdetailAmount"></td>
									<td><input type="text" class="form-control" id="OrderAmount" name="OrderdetailAmount"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr >
						<td colspan="10">
							<input type="button" id="more_fields" onclick="add_fields();" value="Add More" />
							<input type="button" id="remove_fields" onclick="remove_field();" value="Remove More" />
							<a href="javascript:void(0);" id="anc_rem">Remove Row</a>
							<input type="hidden" style="width:48px;" name="totlarow" id="totlarow" value="1" />
						</td>
					</tr>
					<!-- /.sales-item-row -->
					<tr>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
					</tr>
					<tr>
						<td colspan="8"></td>
						<th>Total Amount</th>
						<td>
							<input type="text" class="form-control" name = "OrderTotalAmount" id="OrderTotalAmount">
						</td>
					</tr>
					<tr>
						<td colspan="8"></td>
						<th>Bonus/Discount</th>
						<td><input type="text" class="form-control" name = "OrderBonusDiscount" id="OrderBonusDiscount"></td>
					</tr>
					<tr>
						<td colspan="8"></td>
						<th>Grand Total</th>
						<td><input type="text" class="form-control" name = "OrderGrandTotal" id="OrderGrandTotal" ></td>
					</tr>
					<tr>
						<td colspan="8"></td>
						<th>Payment</th>
						<td><input type="text" class="form-control" name = "OrderPayment" id="OrderPayment" ></td>
					</tr>
					<tr>
						<td colspan="8"></td>
						<th>Due</th>
						<td><input type="text" class="form-control" name = "OrderDue" id="OrderDue"></td>
					</tr>
					<tr>
						<td colspan="8"></td>
						<th>
						Due Amount<br>
						Payment Date
						</td>
						<td>&nbsp;<input type="date" class="form-control" name = "OrderDuePaymentdate" id="OrderDuePaymentdate"></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- /.row -->

	<fieldset>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group cash-check-choice">
				<label>Choose the Payment method:</label><br>
				<label><input type="radio" name="paymenttermId" id="check" value="1"> Cheque</label>
				&nbsp;
				<label><input type="radio" name="paymenttermId" id="cash" value="2" checked="checked"> Cash</label>
			</div>
		</div>
	</div>
	<!-- /.row -->

	<div class="row check-payment-enabled hide-by-default">
		<div class="col-sm-2">
			<div class="form-group">
				<label for="checkdate">Date</label>
				<input type="date" class="form-control" name= "checkdate" id="checkdate" placeholder="">
			</div>
		</div>
		<!-- /.col-sm-2 -->
		<div class="col-sm-2">
			<div class="form-group">
				<label for="bank">Bank Name</label>
				<select name="bankId" id="bankId" class="form-control">
					<option value=" ">Select Bank</option>
					<?php if(isset($banklist)){
							foreach($banklist as $key=>$value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
				</select>
			</div>
		</div>
		<!-- /.col-sm-2 -->
			<div class="col-sm-2">
			<div class="form-group" id="bankaccountchangediv1">
				<label for="bank">Bank Account</label>
				<select name="OrderbankAccountId" id="OrderbankAccountId" class="form-control">
					<option value=" ">Select Account</option>
					<?php if(isset($bankaccount)){
							foreach($bankaccount as $thisbankaccount){?>
								<option value="<?php echo $thisbankaccount['Bankaccount']['id'] ?>"><?php echo $thisbankaccount['Bankaccount']['bankaccount_name']?></option>
						<?php	}}?>
				</select><span id="orderbankaccountidspan"></span>
			</div>
		</div>
		<!-- /.col-sm-2 -->
		<div class="col-sm-2">
			<div class="form-group">
				<label id="accountbalancespan"></label>
			</div>
		</div>
		<!-- /.col-sm-2 -->
		<div class="col-sm-2">
			<div class="form-group">
				<label for="check-number">Cheque No.</label>
				<input type="text" class="form-control" name="OrderCheckNo" id="check-number" placeholder="">
			</div>
		</div>
		<!-- /.col-sm-2 -->
		<div class="col-sm-2">
			<div class="form-group">
				<label for="customer-phone">Customer Phone</label>
				<input type="text" class="form-control" name="OrderCustomerPhone" id="customer-phone" placeholder="">
				<input type="hidden"  name="OrderCoaId" id="OrderCoaId" value="6" >
				<input type="hidden"  name="OrderdetailCoaId" id="OrderdetailCoaId" value="8" >
                <input type="hidden"  name="BanktransactionBankbranchId" id="BanktransactionBankbranchId" value="8" >
                <input type="hidden"  name="BanktransactionBankaccounttypeId" id="BanktransactionBankaccounttypeId" value="8" >
			</div>
		</div>
		<!-- /.col-sm-2 -->
	</div>
	<!-- /.row.check-enabled -->

	<div class="row">
		<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label for="OrderShippingId">Shipping Company</label>
						<select name="OrderShippingId" id="OrderShippingId" class="form-control">
							<option value="">Select one</option>
							<option value="2">Company Name</option>
							<option value="3">Company Name</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label for="bank">Location</label>
						<select name="OrderLocationId" id="OrderLocationId" class="form-control">
							<?php if(isset($locationlist)){
							foreach($locationlist as $key=>$value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
						</select>
					</div>
				</div>
			</div>
		</div>
		<!-- /.col-sm-6 -->
		<div class="col-sm-6">
			<div class="form-group">
				<label for="customer-id">Comment</label>
				<textarea name="OrderComment" class="form-control" id="OrderComment" cols="30" rows="10"></textarea>
			</div>
			<input type="submit" id="submit" class="btn btn-primary pull-right" name="submit" value="Submit">
		</div>
		<!-- /.col-sm-6 -->
	</div>
	<!-- /.row -->
	</fieldset>
</form>
<style>
	#hajji-meta-table tbody tr.specific{
		background-color: red;
	}
</style>

<script>
 $('.sales-order-manage .cash-check-choice input[type="radio"]').on( 'change', function() {
		var target_div = $('.check-payment-enabled');
		if (this.value == '1') {
			target_div.slideDown('fast');
		} else {
	 		target_div.slideUp('fast');
	 	}
	 });

// $('.AddNew').click(function(){
//    var row = $(this).closest('tr').clone();
//    row.find('input').val('');
//    $(this).closest('tr').after(row);
//    $('input[type="button"]', row).removeClass('AddNew')
//                                  .addClass('RemoveRow').val('Remove item');
// });

// $('table').on('click', '.RemoveRow', function(){
//   $(this).closest('tr').remove();
// });


/*var targetScope = $('#hajji-meta-table');
var i = parseInt($('#row-count').val()) + 1;
var sel='';
$( '#add_moallem_field' ).on('click', function() {
$('<tr id="m-row'+ i +'"><td><input type="text" placeholder="Hajj Year" name="hajji_moallem_year'+ i +'" id="hajji_moallem_year'+ i +'" value="" required></td><td><select name="hajji_moallem_id'+ i +'" id="hajji_moallem_id'+ i +'" required>'+sel+'</select></td></tr>').appendTo(targetScope);
row_count = i;
$( '#row-count' ).val(row_count);
$( "#hajji_moallem_id" + i ).val(""); //to reset the selection on newly added field
i++;
return false;
});

$( '#remove_moallem_field' ).on('click', function() {
if( i > 2 ) {
$( '#m-row' + $( '#hajji-meta-table tr' ).size()).remove();
row_count = $('#hajji-meta-table tr' ).size();
$( '#row-count' ).val(row_count);
i--;
}
return false;
});*/
var targetScope = $('#hajji-meta-table tbody tr.sales-item-row');
var row_count = '';
//var i = parseInt($('#row-count').val()) + 1;
var i = $("#hajji-meta-table tbody .sales-item-row").size();
var last_row = targetScope.last();
$( '#add_moallem_field' ).on('click', function() {
        $("<tr class=\"sales-item-row\"><td>adv</td><td><a href=\"#\" id=\"remove_moallem_field\" class=\"btn btn-danger\">- Delete</a></td></tr>").insertAfter(targetScope);
        //row_count = i;
        //$( "#hajji_moallem_id" + i ).val(""); //to reset the selection on newly added field
        i++;
        return false;
});

$( '#hajji-meta-table tbody tr.sales-item-row #remove_moallem_field' ).on('click', function() {
	console.log('HHH');
	this.parents('tr.sales-item-row').remove();
    //return false;
});

</script>
<?php echo "

	<script>	
		$(document).ready(function() {
			$(\"cash\").attr('checked','checked');
			$(\"#OrderDuePaymentdate\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd HH:mm:ss\", use24hours: true, showMeridian: false, startView: 2, minView: 2, autoclose: 1});
			$(\"#OrderOrderdate\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd HH:mm:ss\", use24hours: true, showMeridian: false, startView: 2, minView: 2, autoclose: 1});
			$(\"#checkdate\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd HH:mm:ss\", use24hours: true, showMeridian: false, startView: 2, minView: 2, autoclose: 1});
			$(\"#OrderdetailUnitprice\").keyup(function(){
				var OrderdetailUnitprice = $(this).val();
				var OrderdetailQuantity = $(\"#OrderdetailQuantity\").val();
				$(\"#Orderdetailprice\").val(OrderdetailUnitprice*OrderdetailQuantity);
			});
			$(\"#OrderAmount\").keyup(function(){
				var OrderAmount = $(this).val();
				var OrderDiscount = $(\"#OrderDiscount\").val();
				if(OrderDiscount==''){
					OrderDiscount = 0;
					$(\"#OrderDiscount\").val(0);
				}
				$(\"#Orderdetailprice\").val(parseFloat(OrderAmount) + parseFloat(OrderDiscount));
				var Orderdetailprice = $(\"#Orderdetailprice\").val();
				var OrderdetailQuantity = $(\"#OrderdetailQuantity\").val();
				var OrderdetailUnitprice = Orderdetailprice/OrderdetailQuantity;
				$(\"#OrderdetailUnitprice\").val(OrderdetailUnitprice);
				$(\"#OrderTotalAmount\").val($(this).val());
			});
			$(\"#OrderdetailProductId\").change(function(){
				var OrderdetailProductId = $(this).val();
				var OrderdetailBranchId = $(\"#OrderBranchId\").val();
				$(\"#productidloadspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
				       	cache: false,
				       	type: 'POST',
					url: '".$this->webroot."orders/qtyunitpricebyproduct',
					data: {OrderdetailProductId:OrderdetailProductId,OrderdetailBranchId:OrderdetailBranchId},					
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderStock\").val(result.productquantity);
						$(\"#stockunit\").val(result.productunitprice);
						$(\"#OrderPrice\").val(result.producttotalprice);
						$(\"#OrderdetailProductCode\").val(result.productid);
						$(\"#UnitMon\").val(result.unitmon);
						$(\"#UnitKg\").val(result.unitkilogram);
						$(\"#UnitGram\").val(result.unitgram);
						$(\"#OrderUnitId\").val(result.productunit);
						$(\"#productidloadspan\").html('');
					}
				});
			});
            $(\"#OrderDiscount\").keyup(function(){
	               var Orderdetaildicount = $(\"#OrderDiscount\").val();
	               var Orderdetailprice = $(\"#Orderdetailprice\").val();
	               var Orderdetailamount = Orderdetailprice - Orderdetaildicount;
	               $(\"#OrderAmount\").val(Orderdetailamount);

	               var OrderdetailUnitMon = $(\"#OrderdetailUnitMon\").val();
	               var OrderdetailUnitKg = $(\"#OrderdetailUnitKg\").val();
	               var OrderdetailUnitGram = $(\"#OrderdetailUnitGram\").val();
            
	               var orderdetailmonunitprice = Orderdetailamount/OrderdetailUnitMon;

	               var orderdetailkgunitprice = Orderdetailamount/OrderdetailUnitKg;

	               var orderdetailgramunitprice = Orderdetailamount/OrderdetailUnitGram;

	                $(\"#OrderdetailMonUnitPrice\").val(orderdetailmonunitprice);
					$(\"#OrderdetailKgUnitPrice\").val(orderdetailkgunitprice);
					$(\"#OrderdetailGramUnitPrice\").val(orderdetailgramunitprice);
            });

            	$(\"#OrderdetailQuantity\").keyup(function(){
             	var OrderdetailQuantity = $(\"#OrderdetailQuantity\").val();
             	var Orderdetailstockunitprice = $(\"#stockunit\").val();

             	var OrderUnitMon =$(\"#UnitMon\").val();
             	var OrderUnitKg =$(\"#UnitKg\").val();
             	var OrderUnitGram =$(\"#UnitGram\").val();
             	
             	var totalmon = OrderUnitMon * OrderdetailQuantity;
			    var totalkg = OrderUnitKg * OrderdetailQuantity;
			    var totalgram = OrderUnitGram * OrderdetailQuantity;

			    $(\"#OrderdetailUnitMon\").val(totalmon);
				$(\"#OrderdetailUnitKg\").val(totalkg);
				$(\"#OrderdetailUnitGram\").val(totalgram);

				var quantityprice = OrderdetailQuantity * Orderdetailstockunitprice;
				var quantityunitprice = quantityprice/OrderdetailQuantity;

				$(\"#Orderdetailprice\").val(OrderdetailQuantity * Orderdetailstockunitprice);

				$(\"#OrderdetailUnitprice\").val(quantityunitprice);
					               $(\"#OrderAmount\").val(quantityprice);


             });

			$(\"#OrderdetailProductCode\").change(function(){
				var OrderdetailProductId = $(this).val();
				var OrderdetailBranchId = $(\"#OrderBranchId\").val();
				$(\"#productidloadspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
				       	cache: false,
				       	type: 'POST',
					url: '".$this->webroot."orders/qtyunitpricebyproduct',
					data: {OrderdetailProductId:OrderdetailProductId,OrderdetailBranchId:OrderdetailBranchId},					
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderStock\").val(result.productquantity);
						$(\"#stockunit\").val(result.productunitprice);
						$(\"#OrderPrice\").val(result.producttotalprice);
						$(\"#OrderdetailProductId\").val(result.productid);
						$(\"#UnitMon\").val(result.unitmon);
						$(\"#UnitKg\").val(result.unitkilogram);
						$(\"#UnitGram\").val(result.unitgram);
						$(\"#OrderUnitId\").val(result.productunit);
						$(\"#productidloadspan\").html('');
					}
				});
			});
			$(\"#OrderbankAccountId\").change(function(){
				var BankaccountId = $(this).val();
				$(\"#orderbankaccountidspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."banks/bankchequebookseriesbyaccount',
					data:{BankaccountId:BankaccountId},
					dataType: \"JSON\",
					success: function(result) {
						if(result.banktransactiontotalamount>0){
							$(\"#accountbalancespan\").attr('class','label label-success');
						}else if(result.banktransactiontotalamount<0){
							$(\"#accountbalancespan\").attr('class','label label-danger');
						}else{
							$(\"#accountbalancespan\").attr('class','label label-info');
						}
						$(\"#accountbalancespan\").html(result.banktransactiontotalamount);
						$(\"#BanktransactionBankbranchId\").val(result.bankbranch_id);
						$(\"#BanktransactionBankaccounttypeId\").val(result.bankaccounttype_id);
						$(\"#orderbankaccountidspan\").html('');
						$(\"#purchaseproductspan\").html('Stock : '+result.orderdetailmon+' MON / '+result.orderdetailkg+' KG / '+result.orderdetailgram+' Gram');
					}
				});
			});
			$(\"#bankId\").change(function(){
				var bankId = $(this).val();
				$(\"#bankaccountchangediv1\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."purchases/bankaccountlistbybankid',
						{bankId:bankId},
						function(result) {
							//alert(result);
							$(\"#bankaccountchangediv1\").html(result);
						}
					);
			});
			$(\"#OrderClientId\").change(function(){
				var OrderClientId = $(this).val();
				$(\"#OrderClientSpan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."purchases/salesmanbysupplierid',
					data:{OrderClientId:OrderClientId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderSalesmanId\").html(result.salesman_options);
						$(\"#OrderClientSpan\").html('');
					}
				});
			});
			$(\"#OrderBranchId\").change(function(){
				var OrderBranchId = $(this).val();
				$(\"#OrderBranchIdspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."purchases/productlistbybranchid',
					data:{OrderBranchId:OrderBranchId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderdetailProductId\").html(result.product_options);
						$(\"#OrderdetailProductCode\").html(result.product_code_options);
						$(\"#OrderBranchIdspan\").html('');
					}
				});
			});
		});
	</script>
	<script  type=\"text/javascript\">
		var room=0;
		function add_fields() {
			room = parseInt(document.getElementById(\"totlarow\").value);	
			room = room+1;
			$(\"#content\").append('<tr  id=\"mydivcontent'+room+'\"><td><select name=\"OrderdetailProductCode'+room+'\" id=\"OrderdetailProductCode'+room+'\" class=\"form-control\" ><option value=\"0\">Select Product Code</option><option value=\"1\">124</option><option value=\"2\">456</option></select></td><td><select name=\"OrderdetailProductId'+room+'\" id=\"OrderdetailProductId'+room+'\" class=\"form-control\" ><option value=\"0\">Select Product</option><option value=\"1\">Primary section Bangla / ???????? ????? ?????</option><option value=\"2\">Srijonsil Guide book / ??????? ???? ???</option></select><span id=\"productidloadspan\"></span></td><td><input type=\"text\" class=\"form-control\" id=\"OrderStock'+room+'\" name=\"OrderStock'+room+'\" readonly = \"readonly\",><input type=\"hidden\"  name=\"UnitMon'+room+'\" id=\"UnitMon'+room+'\" ><input type=\"hidden\"  name=\"UnitKg'+room+'\" id=\"UnitKg'+room+'\" ><input type=\"hidden\"  name=\"UnitGram'+room+'\" id=\"UnitGram'+room+'\" ><input type=\"hidden\"  name=\"OrderdetailUnitMon'+room+'\" id=\"OrderdetailUnitMon'+room+'\" ><input type=\"hidden\"  name=\"OrderdetailUnitKg'+room+'\" id=\"OrderdetailUnitKg'+room+'\" ><input type=\"hidden\"  name=\"OrderdetailUnitGram'+room+'\" id=\"OrderdetailUnitGram'+room+'\" ><input type=\"hidden\"	name=\"OrderdetailMonUnitPrice'+room+'\" id=\"OrderdetailMonUnitPrice'+room+'\" ><input type=\"hidden\"  name=\"OrderdetailKgUnitPrice'+room+'\" id=\"OrderdetailKgUnitPrice'+room+'\" ><input type=\"hidden\" name=\"OrderdetailGramUnitPrice'+room+'\" id=\"OrderdetailGramUnitPrice'+room+'\" ><input type=\"hidden\"  name=\"OrderUnitId'+room+'\" id=\"OrderUnitId'+room+'\" ></td><td><input type=\"text\" class=\"form-control\" id=\"stockunit'+room+'\" name=\"stockunit'+room+'\" readonly = \"readonly\"></td><td><input type=\"text\" class=\"form-control\" id=\"OrderPrice'+room+'\" name=\"OrderdetailPrice'+room+'\" readonly = \"readonly\"></td><td><input type=\"text\" class=\"form-control\" name=\"OrderdetailQuantity'+room+'\" id=\"OrderdetailQuantity'+room+'\"></td><td><input type=\"text\" class=\"form-control\" name=\"OrderdetailUnitprice'+room+'\" id=\"OrderdetailUnitprice'+room+'\"></td><td><input type=\"text\" class=\"form-control\" name=\"Orderdetailprice2'+room+'\"id=\"Orderdetailprice2'+room+'\"></td><td><input type=\"text\" class=\"form-control\" id=\"OrderDiscount'+room+'\" name=\"OrderdetailDiscount'+room+'\"></td><td><input type=\"text\" class=\"form-control\" id=\"OrderAmount'+room+'\" name=\"OrderdetailAmount'+room+'\"></td></tr>');
			document.getElementById(\"totlarow\").value=room;
		}
		function remove_field(){
			room = document.getElementById(\"totlarow\").value;
			var child = document.getElementById('mydivcontent'+room);
			var parent = document.getElementById('content');
			parent.deleteRow(child);
			room = room-1;
			document.getElementById(\"totlarow\").value=room;
		}
$(\"#anc_rem\").click(function(){
	if($('#content tr').size()>1){
 		$('#content tr:last-child').remove();
			room = room-1;
			document.getElementById(\"totlarow\").value=room;
 	}else{
 		alert('One row should be present in table');
 	}
 });
	</script>
";
?>
<script type="text/javascript">
 jQuery(document).ready(function(){
    jQuery("#saleProduct").validationEngine();
 });
</script>
