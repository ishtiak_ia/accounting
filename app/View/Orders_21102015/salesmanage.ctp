<form class="sales-order-manage" action="orderinsertupdateaction" method="post">
	<div class="row">
			<div class="col-sm-2">
				<div class="form-group">
					<label for="customer-id">Customer ID</label>
					<!-- <input type="text" class="form-control" id="customer-id" placeholder=""> -->
					<select name="OrderClientId" id="OrderClientId" class="form-control">
						<?php if(isset($customerlist)){
							foreach($customerlist as $key => $value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
					</select>
				</div>
			</div>
			<!-- /.col-sm-2 -->
			<div class="col-sm-2">
				<div class="form-group">
					<label for="invoice">Invoice</label>
					<input type="text" class="form-control" name = "OrderReferenceno" id="OrderReferenceno" placeholder="NP-045043643">
				</div>
			</div>
			<!-- /.col-sm-2 -->
			<div class="col-sm-2">
				<div class="form-group">
					<label for="date">Date</label>
					<!-- <input type="date" class="form-control" id="date" placeholder=""> -->
					<input type="date" name = "OrderOrderdate" class="form-control" id="OrderOrderdate" placeholder="">
				</div>
			</div>
			<!-- /.col-sm-2 -->
			<div class="col-sm-2">
				<div class="form-group">
					<label for="sales-center">Sales Center</label>
					<!-- <input type="text" class="form-control" id="sales-center" placeholder=""> -->
					<select name="OrderBranchId" id="OrderBranchId" class="form-control">
						<?php if(isset($salescenter)){
							foreach($salescenter as $key => $value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
					</select>
				</div>
			</div>
			<!-- /.col-sm-2 -->
			<div class="col-sm-2">
				<div class="form-group">
					<label for="salesman">Salesman</label>
					<select name="OrderSalesmanId" id="OrderSalesmanId" class="form-control">
						<?php if(isset($salesmanlist)){
							foreach($salesmanlist as $key => $value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
					</select>
				</div>
			</div>
			<!-- /.col-sm-2 -->
	</div>
	<!-- .row -->

	<div class="row">
		<div class="table-responsive">
			<table>
    <tr><td>Item Type</td><td>Item</td><td>Add/Remove Item</td></tr>
    <tr><td><input type='text' value='Fruit >'></td>
        <td><input type='text' value='Apple'></td>
        <td><input type='button' class='AddNew' value='Add new item'></td></tr>
</table>

<!-- <input type='button' id="add_moallem_field" class='button' value='Add new item'> -->
<button id="add_moallem_field" class="button" >+ ADD</button>

			<table id="hajji-meta-table" class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>Item ID</th>
						<th>Item Name</th>
						<th>Quantity</th>
						<th>Unit</th>
						<th>Stock</th>
						<th>Unit</th>
						<th>Price</th>
						<th>Discount</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<tr class="sales-item-row">
						<td><select name="OrderdetailProductCode" id="OrderdetailProductCode" >
						<?php if(isset($productcodelist)){
							foreach($productcodelist as $productcode){?>
								<option value="<?php echo $productcode ?>"><?php echo $productcode?></option>
						<?php	}}?>
					</select></td>
						<td><select name="OrderdetailProductId" id="OrderdetailProductId" >
						<?php if(isset($productlist)){
							foreach($productlist as $key => $value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
					</select></td>
						<td><input type="text" class="form-control" name="OrderdetailQuantity" id="OrderdetailQuantity"></td>
						<td><input type="text" class="form-control" name="OrderUnit" id="OrderUnit"></td>
						<td><input type="text" class="form-control" id="OrderStock">
						</td>
						<td><input type="text" class="form-control" id="OrderPrice">
						</td>
						<td><input type="text" class="form-control" id="OrderPrice">
						</td>
					  <td><input type="text" class="form-control" id="OrderDiscount"></td>
						<td><input type="text" class="form-control" id="OrderAmount"></td>

					</tr>
					<!-- /.sales-item-row -->
					<tr>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
					</tr>
					<tr>
						<td colspan="7"></td>
						<th>Total Amount</th>
						<td>
							<input type="text" class="form-control" name = "OrderTotalAmount" id="OrderTotalAmount">
						</td>
					</tr>
					<tr>
						<td colspan="7"></td>
						<th>Bonus/Discount</th>
						<td><input type="text" class="form-control" name = "OrderBonusDiscount" id="OrderBonusDiscount"></td>
					</tr>
					<tr>
						<td colspan="7"></td>
						<th>Grand Total</th>
						<td><input type="text" class="form-control" name = "OrderGrandTotal" id="OrderGrandTotal" ></td>
					</tr>
					<tr>
						<td colspan="7"></td>
						<th>Payment</th>
						<td><input type="text" class="form-control" name = "OrderPayment" id="OrderPayment" ></td>
					</tr>
					<tr>
						<td colspan="7"></td>
						<th>Due</th>
						<td><input type="text" class="form-control" name = "OrderDue" id="OrderDue"></td>
					</tr>
					<tr>
						<td colspan="7"></td>
						<th>
						Due Amount<br>
						Payment Date
						</td>
						<td><input type="text" class="form-control" name = "OrderDuePaymentdate" id="OrderDuePaymentdate"></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- /.row -->

	<fieldset>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group cash-check-choice">
				<label>Choose the Payment method:</label><br>
				<label><input type="radio" name="paymenttermId" id="check" value="1"> Check</label>
				&nbsp;
				<label><input type="radio" name="paymenttermId" id="cash" value="2"> Cash</label>
			</div>
		</div>
	</div>
	<!-- /.row -->

	<div class="row check-payment-enabled hide-by-default">
		<div class="col-sm-2">
			<div class="form-group">
				<label for="checkdate">Date</label>
				<input type="date" class="form-control" name= "checkdate" id="checkdate" placeholder="">
			</div>
		</div>
		<!-- /.col-sm-2 -->
		<div class="col-sm-2">
			<div class="form-group">
				<label for="bank">Bank Name</label>
				<select name="bankId" id="bank" class="form-control">
					<?php if(isset($banklist)){
							foreach($banklist as $key=>$value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
				</select>
			</div>
		</div>
		<!-- /.col-sm-2 -->
			<div class="col-sm-2">
			<div class="form-group">
				<label for="bank">Bank Account</label>
				<select name="OrderbankAccountId" id="OrderbankAccountId" class="form-control">
					<?php if(isset($bankaccount)){
							foreach($bankaccount as $key=>$value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
				</select>
			</div>
		</div>
		<!-- /.col-sm-2 -->
		<div class="col-sm-2">
			<div class="form-group">
				<label for="check-number">Check No.</label>
				<input type="text" class="form-control" name="OrderCheckNo" id="check-number" placeholder="">
			</div>
		</div>
		<!-- /.col-sm-2 -->
		<div class="col-sm-2">
			<div class="form-group">
				<label for="customer-phone">Customer Phone</label>
				<input type="text" class="form-control" name="OrderCustomerPhone" id="customer-phone" placeholder="">
				<input type="hidden"  name="OrderCoaId" id="OrderCoaId" value="6" >
			</div>
		</div>
		<!-- /.col-sm-2 -->
	</div>
	<!-- /.row.check-enabled -->

	<div class="row">
		<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label for="bank">Shipping Company</label>
						<select name="bank" id="bank" class="form-control">
							<option value="">Select one</option>
							<option value="2">Company Name</option>
							<option value="3">Company Name</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label for="bank">Location</label>
						<select name="bank" id="bank" class="form-control">
							<?php if(isset($locationlist)){
							foreach($locationlist as $key=>$value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
						</select>
					</div>
				</div>
			</div>
		</div>
		<!-- /.col-sm-6 -->
		<div class="col-sm-6">
			<div class="form-group">
				<label for="customer-id">Comment</label>
				<textarea name="OrderComment" class="form-control" id="OrderComment" cols="30" rows="10"></textarea>
			</div>
			<input type="submit" id="submit" class="btn btn-primary pull-right" name="submit" value="Submit">
		</div>
		<!-- /.col-sm-6 -->
	</div>
	<!-- /.row -->
	</fieldset>
</form>
<ul class="kauwaa">
	<li>a</li>
	<li>v</li>
	<li>d</li>
</ul>
<style>
	#hajji-meta-table tbody tr.specific{
		background-color: red;
	}
</style>

<script>
 $('.sales-order-manage .cash-check-choice input[type="radio"]').on( 'change', function() {
		var target_div = $('.check-payment-enabled');
		if (this.value == '1') {
			target_div.slideDown('fast');
		} else {
	 		target_div.slideUp('fast');
	 	}
	 });

// $('.AddNew').click(function(){
//    var row = $(this).closest('tr').clone();
//    row.find('input').val('');
//    $(this).closest('tr').after(row);
//    $('input[type="button"]', row).removeClass('AddNew')
//                                  .addClass('RemoveRow').val('Remove item');
// });

// $('table').on('click', '.RemoveRow', function(){
//   $(this).closest('tr').remove();
// });


/*var targetScope = $('#hajji-meta-table');
var i = parseInt($('#row-count').val()) + 1;
var sel='';
$( '#add_moallem_field' ).on('click', function() {
$('<tr id="m-row'+ i +'"><td><input type="text" placeholder="Hajj Year" name="hajji_moallem_year'+ i +'" id="hajji_moallem_year'+ i +'" value="" required></td><td><select name="hajji_moallem_id'+ i +'" id="hajji_moallem_id'+ i +'" required>'+sel+'</select></td></tr>').appendTo(targetScope);
row_count = i;
$( '#row-count' ).val(row_count);
$( "#hajji_moallem_id" + i ).val(""); //to reset the selection on newly added field
i++;
return false;
});

$( '#remove_moallem_field' ).on('click', function() {
if( i > 2 ) {
$( '#m-row' + $( '#hajji-meta-table tr' ).size()).remove();
row_count = $('#hajji-meta-table tr' ).size();
$( '#row-count' ).val(row_count);
i--;
}
return false;
});*/
var targetScope = $('#hajji-meta-table tbody tr.sales-item-row');
var row_count = '';
//var i = parseInt($('#row-count').val()) + 1;
var i = $("#hajji-meta-table tbody .sales-item-row").size();
var last_row = targetScope.last();
$( '#add_moallem_field' ).on('click', function() {
        $("<tr class=\"sales-item-row\"><td>adv</td><td><a href=\"#\" id=\"remove_moallem_field\" class=\"btn btn-danger\">- Delete</a></td></tr>").insertAfter(targetScope);
        //row_count = i;
        //$( "#hajji_moallem_id" + i ).val(""); //to reset the selection on newly added field
        i++;
        return false;
});

$( '#hajji-meta-table tbody tr.sales-item-row #remove_moallem_field' ).on('click', function() {
	console.log('HHH');
	this.parents('tr.sales-item-row').remove();
    //return false;
});

</script>
<?php echo "
<script>
	

$(\"#OrderOrderdate\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd HH:mm:ss\", use24hours: true, showMeridian: false});
$(\"#checkdate\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd HH:mm:ss\", use24hours: true, showMeridian: false});
</script>
";?>
