<?php echo $this->Html->script('order/order', array('inline' => true)); 
?>	

<form class="sales-order-manage" name="purchaseProduct" id="purchaseProduct" action="purchaseinsertupdateaction" method="post">
<?php
	echo $this->Form->input('Bank.banktransactionId', array('type'=>'hidden', 'value'=>"", 'class'=>''));
	//echo $this->Form->input('Banktransaction.bank_id', array('type'=>'hidden', 'value'=>"", 'class'=>''));
	//echo $this->Form->input('Banktransaction.bankbranch_id', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['bankbranch_id']?$banktransaction[0]['Banktransaction']['bankbranch_id']:0, 'class'=>''));
	//echo $this->Form->input('Banktransaction.bankaccounttype_id', array('type'=>'hidden', 'value'=>"", 'class'=>''));
	//echo $this->Form->input('Banktransaction.coa_id', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['coa_id']?$banktransaction[0]['Banktransaction']['coa_id']:1, 'class'=>''));
	echo"<h3>".__("Add New Purchase Products")."</h3>";
?>
	<div class="row">
			<div class="col-sm-2">
				<div class="form-group">
					<label for="customer-id">Supplier ID</label>
					<!-- <input type="text" class="form-control" id="customer-id" placeholder=""> -->
					<select name="OrderClientId" id="OrderClientId" class="form-control validate[required]" >
						<option value=" ">Select Supplier</option>
						<?php if(isset($supplierlist)){
							foreach($supplierlist as $key => $value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
					</select>
				</div>
			</div>
			<!-- /.col-sm-2 -->
			<div class="col-sm-2">
				<div class="form-group">
					<label for="invoice">Invoice</label>
					<input type="text" class="form-control validate[required]" name = "OrderReferenceno" id="OrderReferenceno" placeholder="Invoice">
				</div>
			</div>
			<!-- /.col-sm-2 -->
			<div class="col-sm-2">
				<div class="form-group">
					<label for="date">Date</label>
					<!-- <input type="date" class="form-control" id="date" placeholder=""> -->
					<input type="text" name = "OrderOrderdate" class="form-control validate[required]" id="OrderOrderdate" placeholder="Date" value="">
				</div>
			</div>
			<!-- /.col-sm-2 -->
			<div class="col-sm-2">
				<div class="form-group">
					<label for="sales-center">Stock Center</label>
					<!-- <input type="text" class="form-control" id="sales-center" placeholder=""> -->
					<select name="OrderBranchId" id="OrderBranchId" class="form-control validate[required]">
						<option value=" ">Select Stock Center</option>
						<?php if(isset($salescenter)){
							foreach($salescenter as $key => $value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
					</select>
				</div>
			</div>
			<!-- /.col-sm-2 -->
			<div class="col-sm-2">
				<div class="form-group">
					<label for="salesman">Salesman</label>
					<select name="OrderSalesmanId" id="OrderSalesmanId" class="form-control validate[required]">
						<option value=" ">Select Salesman</option>
						<?php if(isset($salesmanlist)){
							foreach($salesmanlist as $key => $value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
					</select>
				</div>
				<span id="OrderClientSpan"></span>
			</div>
			<!-- /.col-sm-2 -->
	</div>
	<!-- .row -->

	<div class="row">
		<div class="table-responsive">
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>Item ID</th>
						<th>Item Name</th>
						<th>Stock</th>
						<th>Unit Price</th>
						<th>Quantity</th>
						<th>Unit</th>
						<th>Price</th>
						<th>Discount (%)</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							
							<select name="OrderdetailProductCode" id="OrderdetailProductCode" class="form-control validate[required]">
							<option value="0">Select Product Code</option>
						<?php if(isset($productcodelist)){
							foreach($productcodelist as $key=>$productcode){?>
								<option value="<?php echo $key ?>"><?php echo $productcode?></option>
						<?php	}}?>
					</select>
				
				</td>
						<td>
							<div class="form-group"  id="productchangediv">
							<select name="OrderdetailProductId" id="OrderdetailProductId" class="form-control validate[required]">
							<option value="0">Select Product</option>
						<?php if(isset($productlist)){
							foreach($productlist as $key => $value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
					</select>
					<input type="hidden"  name="unitmon" id="unitmon">
					<input type="hidden"  name="unitkilogram" id="unitkilogram">
					<input type="hidden"  name="unitgram" id="unitgram">
					<input type="hidden"  name="stockmon" id="stockmon">
					<input type="hidden"  name="stockkilogram" id="stockkilogram">
					<input type="hidden"  name="stockgram" id="stockgram">
					<input type="hidden"  name="orderdetailmon" id="orderdetailmon">
					<input type="hidden"  name="orderdetailkg" id="orderdetailkg">
					<input type="hidden"  name="orderdetailgram" id="orderdetailgram">
					<input type="hidden"  name="productdafultprice" id="productdafultprice">
					<input type="hidden"  name="orderdetailmonunitprice" id="orderdetailmonunitprice">
					<input type="hidden"  name="orderdetailkgunitprice" id="orderdetailkgunitprice">
					<input type="hidden"  name="orderdetailgramunitprice" id="orderdetailgramunitprice">
					</div>
					<span id="OrderbankAccountIdspan"></span>
					</td>
					<td>
							<input type="text" class="form-control" id="OrderStock" placeholder="" readonly>
							<span id="stocktspan"></span>
						</td>
						<td><input type="text" class="form-control" id="stockunit" name="stockunit" placeholder="" readonly></td>
						<td>
							<input type="text" class="form-control" name="OrderdetailQuantity" id="OrderdetailQuantity" placeholder="Quantity">
							<span id="quantityspan"></span>
							<span id="price_span"></span>
						</td>
						<td>
							<select name="OrderdetailUnit" id="OrderdetailUnit" class="form-control validate[required]">
								<?php if(isset($unitlist)){
							foreach($unitlist as $key => $value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
							</select>
						<span id="productidloadspan"></span>	
						</td>
						<td><span id="pricespan"><input type="text" class="form-control" name="OrderdetailPrice" id="OrderdetailPrice" placeholder="Price"></span></td>
						<td><input type="text" class="form-control" name="OrderdetailDiscount" id="OrderdetailDiscount" placeholder="Discount (%)"></td>
						<td><input type="text" class="form-control" name="OrderdetailAmount"  id="OrderdetailAmount" placeholder="Amount"></td>
					</tr>
					<tr>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
						<td>BODY</td>
					</tr>
					<tr>
						<td colspan="7"></td>
						<th>Total Amount</th>
						<td>
							<input type="text" class="form-control" name = "OrderTotalamount" id="OrderTotalAmount" placeholder="">
						</td>
					</tr>
					<tr>
						<td colspan="7"></td>
						<th>Bonus/Discount (%)</th>
						<td><input type="text" class="form-control" name ="OrderBonusDiscount" id="OrderBonusDiscount" placeholder=""></td>
					</tr>
					<tr>
						<td colspan="7"></td>
						<th>Grand Total</th>
						<td><input type="text" class="form-control" name ="OrderGrandTotal" id="OrderGrandTotal" placeholder=""></td>
					</tr>
					<tr>
						<td colspan="7"></td>
						<th>Payment</th>
						<td><input type="text" class="form-control" name ="OrderPayment" id="OrderPayment" placeholder=""></td>
					</tr>
					<tr>
						<td colspan="7"></td>
						<th>Due</th>
						<td><input type="text" class="form-control" name ="OrderDue" id="OrderDue" placeholder=""></td>
					</tr>
					<tr>
						<td colspan="7"></td>
						<th>
						Due Amount<br>
						Payment Date
						</td>
						<?php //= 0){ ?>
						<td><input type="text" class="form-control" name ="OrderDuePaymentdate" id="OrderDuePaymentdate" placeholder=""></td>
						<?php //} ?>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- /.row -->

	<fieldset>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group cash-check-choice">
				<label>Choose the Payment method:</label><br>
				<label><input type="radio" name="paymenttermId" id="check" value="1"> Cheque</label>
				&nbsp;
				<label><input type="radio" name="paymenttermId" id="cash" value="2" checked> Cash</label>
			</div>
		</div>
	</div>
	<!-- /.row -->

	<div class="row check-payment-enabled hide-by-default">
		<div class="col-sm-2">
			<div class="form-group">
				<label for="checkdate">Date</label>
				<input type="text" class="form-control" name= "checkdate" id="checkdate" placeholder="Date">
			</div>
		</div>
		<!-- /.col-sm-2 -->
		<div class="col-sm-2">
			<div class="form-group">
				<label for="bank">Bank Name</label>
				<select name="bankId" id="bankId" class="form-control">
					<option value="0">Select Bank</option>
					<?php if(isset($banklist)){
							foreach($banklist as $key=>$value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
				</select>
			</div>
		</div>
		<!-- /.col-sm-2 -->
			<div class="col-sm-2">
			<div class="form-group" id="bankaccountchangediv">
				<label for="OrderbankAccountId">Bank Account</label>
				<select name="OrderbankAccountId" id="OrderbankAccountId" class="form-control">
					<option value="0">Select Bank Account</option>
					<?php if(isset($bankaccount)){
							foreach($bankaccount as $thisbankaccount){?>
								<option value="<?php echo $thisbankaccount['Bankaccount']['id'] ?>"><?php echo $thisbankaccount['Bankaccount']['bankaccount_name']?></option>
								<!-- <input type="hidden"  name="OrderdetailCoaId" id="OrderdetailCoaId" value="<?php echo $thisbankaccount['Bankaccount']['bankaccounttype_id']?>" >
				<input type="hidden"  name="OrderdetailCoaId" id="OrderdetailCoaId" value="<?php echo $thisbankaccount['Bankaccount']['bankbranch_id']?>" > -->

						<?php	}}?>
				</select>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="form-group">
			<label for="OrderBanktransactionchequebookseries">Select Series</label>
			<select name="OrderBanktransactionchequebookseries" id="OrderBanktransactionchequebookseries" class="form-control">	
				<?php if(isset($bankchequebook)){
							foreach($bankchequebook as $key=>$thisbankchequebook){?>
								<option value="<?php echo $key; ?>"><?php echo $thisbankchequebook; ?></option>
								
						<?php	}}?>
			</select>
			<span id="OrderbankAccountIdspan"></span>	
			</div>	
		</div>	
		<!-- /.col-sm-2 -->
		<div class="col-sm-2">
			<div class="form-group">
				<label for="check-number">Cheque No.</label>
				<input type="text" class="form-control" name="OrderCheckNo" id="OrderCheckNo" placeholder="">
				<span id=\"OrderBanktransactionchequebookseriesspan\"></span>
			</div>
		</div>
		<!-- /.col-sm-2 -->
		<div class="col-sm-2">
			<div class="form-group">
				<label for="OrderCustomerPhone">Customer Phone</label>
				<input type="text" class="form-control" name="OrderCustomerPhone" id="OrderCustomerPhone" placeholder="">
				<input type="hidden"  name="OrderCoaId" id="OrderCoaId" value="9" >
				<input type="hidden"  name="BanktransactionBankbranchId" id="BanktransactionBankbranchId" >
				<input type="hidden"  name="BanktransactionBankaccounttypeId" id="BanktransactionBankaccounttypeId">
			</div>
		</div>
		<!-- /.col-sm-2 -->
	</div>
	<!-- /.row.check-enabled -->

	<div class="row">
		<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label for="OrderShippingId">Shipping Company</label>
						<select name="OrderShippingId" id="OrderShippingId" class="form-control">
							<option value="">Select one</option>
							<option value="2">Company Name</option>
							<option value="3">Company Name</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label for="OrderLocationId">Location</label>
						<select name="OrderLocationId" id="OrderLocationId" class="form-control">
							<option value="0">Select Location</option>
							<?php if(isset($locationlist)){
							foreach($locationlist as $key=>$value){?>
								<option value="<?php echo $key ?>"><?php echo $value?></option>
						<?php	}}?>
						</select>
					</div>
				</div>
			</div>
		</div>
		<!-- /.col-sm-6 -->
		<div class="col-sm-6">
			<div class="form-group">
				<label for="OrderComment">Comment</label>
				<textarea name="OrderComment" class="form-control" id="OrderComment" cols="30" rows="10"></textarea>
			</div>
			<input type="submit" id="submit" class="btn btn-primary pull-right" name="submit" value="Submit">
		</div>
		<!-- /.col-sm-6 -->
	</div>
	<!-- /.row -->
	</fieldset>
</form>

<script>
 $('.sales-order-manage .cash-check-choice input[type="radio"]').on( 'change', function() {
		var target_div = $('.check-payment-enabled');
		if (this.value == '1') {
			target_div.slideDown('fast');
		} else {
	 		target_div.slideUp('fast');
	 	}
	 });
</script>
<?php echo "
<script>
	$(document).ready(function() {
		$(\"#OrderOrderdate\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd HH:mm:ss\", use24hours: true, showMeridian: false , startView: 2, minView: 2, autoclose: 1
		});
		$(\"#OrderDuePaymentdate\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd HH:mm:ss\", use24hours: true, showMeridian: false , startView: 2, minView: 2, autoclose: 1
		});
		$(\"#checkdate\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd HH:mm:ss\", use24hours: true, showMeridian: false , startView: 2, minView: 2, autoclose: 1
		});
		$(\"#OrderdetailProduct\").change(function(){
			var ProductId = $(this).val();
			$(\"#pricespan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.post(
					'".$this->webroot."purchases/purchasepricebyproductname',
					{ProductId:ProductId},
					function(result) {
						$(\"#pricespan\").html(result);
					}
				);
		});
		$(\"#OrderdetailProductId\").change(function(){	
				var OrderdetailProductId = $(this).val();
				var OrderBranchId = $(\"#OrderBranchId\").val();
				$(\"#productidloadspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
				       	cache: false,
				       	type: 'POST',
					url: '".$this->webroot."orders/qtyunitpricebyproduct',
					data: {OrderdetailProductId:OrderdetailProductId,OrderdetailBranchId:OrderBranchId},					
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderStock\").val(result.productquantity);
						var order_stock = result.productquantity;
						$(\"#stockunit\").val(result.productunitprice);
						var stock_unit = result.productunitprice;
						$(\"#OrderPrice\").val(result.producttotalprice);
						$(\"#OrderdetailProductCode\").val(result.productid);
						$(\"#OrderdetailUnit\").val(result.productunit);
						$(\"#unitmon\").val(result.unitmon);
						var unit_mon = result.unitmon;
						$(\"#unitkilogram\").val(result.unitkilogram);
						var unit_kilogram = result.unitkilogram;
						$(\"#unitgram\").val(result.unitgram);
						var unit_gram = result.unitgram;
						$(\"#stocktspan\").html('Remaining Stock : '+order_stock * unit_mon+' MON / '+order_stock * unit_kilogram+' KG / '+order_stock * unit_gram+' GRAM');
						var quantity = $(\"#OrderdetailQuantity\").val();
						$(\"#quantityspan\").html('Input Stock : '+quantity * unit_mon+' MON / '+quantity * unit_kilogram+' KG / '+quantity * unit_gram+' GRAM');
						$(\"#stockmon\").val(order_stock * unit_mon);
						$(\"#stockkilogram\").val(order_stock * unit_kilogram);
						$(\"#stockgram\").val(order_stock * unit_gram);
						$(\"#orderdetailmon\").val(result.orderdetailmon);
						$(\"#orderdetailkg\").val(result.orderdetailkg);
						$(\"#orderdetailgram\").val(result.orderdetailgram);
						$(\"#productdafultprice\").val(result.productdafultprice);
						
						
						//$(\"#price_span\").html('Price : '+quantity * productdafultprice+' TK / '+quantity * productdafultprice+' TK / '+quantity * productdafultprice+' TK');
						$(\"#productidloadspan\").html('');
					}
				});
			});
		$(\"#OrderdetailProductCode\").change(function(){
				var OrderdetailProductId = $(this).val();
				var OrderBranchId = $(\"#OrderBranchId\").val();
				$(\"#productidloadspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
				       	cache: false,
				       	type: 'POST',
					url: '".$this->webroot."orders/qtyunitpricebyproduct',
					data: {OrderdetailProductId:OrderdetailProductId,OrderdetailBranchId:OrderBranchId},					
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderStock\").val(result.productquantity);
						var order_stock = result.productquantity;
						$(\"#stockunit\").val(result.productunitprice);
						var stock_unit = result.productunitprice;
						$(\"#OrderPrice\").val(result.producttotalprice);
						$(\"#OrderdetailProductId\").val(result.productid);
						$(\"#OrderdetailUnit\").val(result.productunit);
						$(\"#OrdersPurchaseproductmon\").val(result.orderdetailmon);
						$(\"#OrdersPurchaseproductkg\").val(result.orderdetailkg);
						$(\"#OrdersPurchaseproductgram\").val(result.orderdetailgram);
						$(\"#unitmon\").val(result.unitmon);
						var unit_mon = result.unitmon;
						$(\"#unitkilogram\").val(result.unitkilogram);
						var unit_kilogram = result.unitkilogram;
						$(\"#unitgram\").val(result.unitgram);
						var unit_gram = result.unitgram;
						$(\"#stocktspan\").html('Remaining Stock : '+order_stock * unit_mon+' MON / '+order_stock * unit_kilogram+' KG / '+order_stock * unit_gram+' GRAM');
						var quantity = $(\"#OrderdetailQuantity\").val();
						$(\"#quantityspan\").html('Input Stock : '+quantity * unit_mon+' MON / '+quantity * unit_kilogram+' KG / '+quantity * unit_gram+' GRAM');
						$(\"#stockmon\").val(order_stock * unit_mon);
						$(\"#stockkilogram\").val(order_stock * unit_kilogram);
						$(\"#stockgram\").val(order_stock * unit_gram);
						$(\"#orderdetailmon\").val(result.orderdetailmon);
						$(\"#orderdetailkg\").val(result.orderdetailkg);
						$(\"#orderdetailgram\").val(result.orderdetailgram);
						$(\"#productdafultprice\").val(result.productdafultprice);
						var quantity = $(\"#OrderdetailQuantity\").val();
						//$(\"#price_span\").html('Price : '+quantity * productdafultprice+' TK / '+quantity * productdafultprice+' TK / '+quantity * productdafultprice+' TK');
						$(\"#productidloadspan\").html('');
					}
				});
			});
		$(\"#bankId\").change(function(){
			var bankId = $(this).val();
			$(\"#bankaccountchangediv\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.post(
					'".$this->webroot."purchases/bankaccountlistbybankid',
					{bankId:bankId},
					function(result) {
						$(\"#bankaccountchangediv\").html(result);
					}
				);
		});
		
		$(\"#OrderbankAccountId\").change(function(){
				var BankaccountId = $(this).val();
				$(\"#OrderbankAccountIdspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."banks/bankchequebookseriesbyaccount',
					data:{BankaccountId:BankaccountId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderBanktransactionchequebookseries\").html(result.bankchequebook_options);
						//$(\"#BanksPaymentrecive\").val(result.banktransactiontotalamount);
						$(\"#BanktransactionBankId\").val(result.bank_id);
						$(\"#BanktransactionBankbranchId\").val(result.bankbranch_id);
						$(\"#BanktransactionBankaccounttypeId\").val(result.bankaccounttype_id);
						$(\"#OrderbankAccountIdspan\").html('');
					}
				});
		});
		$(\"#OrderBanktransactionchequebookseries\").change(function(){
				var BanktransactionBanktransactionchequesequencynumber = $(this).val();
				$(\"#OrderBanktransactionchequebookseriesspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
				       	cache: false,
				       	type: 'POST',
					url: '".$this->webroot."banks/bankchequebooksequencybyseries',
					data: {BanktransactionBanktransactionchequesequencynumber:BanktransactionBanktransactionchequesequencynumber},					
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderCheckNo\").val(result.banktransaction_options);
						$(\"#BankBanktransactionId\").val(result.banktransaction_id);
						$(\"#BanksBanktransactionuuid\").val(result.banktransaction_uuid);
						$(\"#OrderBanktransactionchequebookseriesspan\").html('');
					}
				});
			});
		
		$(\"#OrderBranchId\").change(function(){
				var OrderBranchId = $(this).val();
				$(\"#OrderBranchIdspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."purchases/productlistbybranchid',
					data:{OrderBranchId:OrderBranchId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderdetailProductId\").html(result.product_options);
						$(\"#OrderdetailProductCode\").html(result.product_code_options);
						//$(\"#OrderBranchIdspan\").html('');
					}
				});
		});
		$(\"#OrderClientId\").change(function(){
				var OrderClientId = $(this).val();
				$(\"#OrderClientSpan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."purchases/salesmanbysupplierid',
					data:{OrderClientId:OrderClientId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderSalesmanId\").html(result.salesman_options);
						//$(\"#OrderClientSpan\").html('');
					}
				});
		});

	});	

</script>
";?>
<script type="text/javascript">
 jQuery(document).ready(function(){
    jQuery("#purchaseProduct").validationEngine();
 });
</script>