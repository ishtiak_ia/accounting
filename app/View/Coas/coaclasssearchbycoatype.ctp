<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart($class=null, $id=null);
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('COA Type')  => array('class' => 'highlight sortable')
				), 
				array(
					__('COA Group')  => array('class' => 'highlight sortable')
				), 
				array(
					__('CODE')  => array('class' => 'highlight sortable')
				), 
				array(
					__('COA Class')  => array('class' => 'highlight sortable')
				), 
				array(
					__('COA Class BN')  => array('class' => 'highlight sortable')
				), 
				__('Active'), 
				__('Action')
			)
		);
	if(count($coaclass)>0):
		$coaclassrows = array();
		$countCoaclass = 0-1+$this->Paginator->counter('%start%');
				foreach($coaclass as $Thiscoaclass):
					$countCoaclass++;
					$coaclassrows[]= array(
						$countCoaclass,
						$Thiscoaclass["Coatype"]["coatypename"]."/".$Thiscoaclass["Coatype"]["coatypenamebn"],
						$Thiscoaclass["Coagroup"]["coagroupname"]."/".$Thiscoaclass["Coagroup"]["coagroupnamebn"],
						$Thiscoaclass["Coaclass"]["coaclasscode"],
						$Thiscoaclass["Coaclass"]["coaclassname"],
						$Thiscoaclass["Coaclass"]["coaclassnamebn"],
						$Utilitys->statusURL($this->request["controller"], 'coaclass', $Thiscoaclass["Coaclass"]["id"], $Thiscoaclass["Coaclass"]["coaclassuuid"], $Thiscoaclass["Coaclass"]["coaclassisactive"]),$Utilitys->cusurl($this->request["controller"], 'coaclass', $Thiscoaclass["Coaclass"]["id"], $Thiscoaclass["Coaclass"]["coaclassuuid"]));
				endforeach;
		echo $this->Html->tableCells(
			$coaclassrows
		);
	else:
	echo"
		<tr><td colspan=\"6\">".__("Data Not Found!!!")."</td></tr>
	";
	endif;
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var coaclassname = $('#CoaclassCoaclassname').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{coaclassname:coaclassname},
						async: true,
						beforeSend: function(){
							$(\"#classsearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#classsearchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>