<?php
App::import('Controller', 'Logins');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');

$Logins = new LoginsController;

class UsersController extends AppController {
	public $name = 'Users';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session');
	public $components = array('RequestHandler','Session', 'Image');
	public $uses = array('User', 'Usergroup', 'Category', 'Subject', 'Userinstitute', 'Userdepartment', 'Userdesignation', 'Usereducation', 'Usercareerhistory', 'Useremergencycontact', 'Userdependent','Userrelationship','Userachievement','Usernarration', 'Group', 'Groupnarration', 'Menu', 'Menunarration', 'Menuprivilege', 'Company', 'Branch', 'Usertransaction', 'Department', 'Designation', 'Institute', 'Institutetype', 'Location', 'LocationParent', 'Department', 'Salary', 'Grievance', 'Reward', 'Jobcardsummerie', 'Shift');


    // Instantiation 
   // $this->Logins = new LoginsController;
    //$this->set('Logins',$Logins);
    //$Logins->__validateLoginStatus();

    // Call a method from
    //$Logins->__validateLoginStatus();
    var $Logins;


	function beforeFilter(){
        $this->Logins =& new LoginsController;
        $this->Logins->constructClasses();
        $this->Logins->__validateLoginStatus();
		//$this->__validateLoginStatus();
		//$this->recordActivity();
		// $this->Auth->allow('register');
		//parent::beforeFilter();
        //$this->Auth->allow("logins/login"); 
		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	/*public function login(){
		$this->redirect("/logins/login");
	}*/
    public  function beforeRender(){
        $this->response->disableCache();//prevent useless warnings for Ajax
        if($this->RequestHandler->isAjax()){
            Configure::write('debug', 0);
        }
    }
    public function districbydividion(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$divisionid = @$this->request->data['UserDivisionid'];

		$districlist=$this->Location->find(
			'all',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions"=>array(
					'Location.locationparentid' => $divisionid,
					'Location.locationstep'=>1,
					'Location.locationisactive'=>1,
				)
			)
		);
			echo "<option value=\"\">District</option>";
		foreach($districlist as $thisdistriclist) {
			echo "<option value=\"".$thisdistriclist['Location']['id']."\">".$thisdistriclist['Location']['location_name']."</option>";
		}
	}
	public function upazilabydistric(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$districid = @$this->request->data['UserDistrictid'];

		$upazilalist=$this->Location->find(
			'all',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions"=>array(
					'Location.locationparentid' => $districid,
					'Location.locationstep'=>2,
					'Location.locationisactive'=>1,
				)
			)
		);
			echo "<option value=\"\">Upazila</option>";
		foreach($upazilalist as $thisupazilalist) {
			echo "<option value=\"".$thisupazilalist['Location']['id']."\">".$thisupazilalist['Location']['location_name']."</option>";
		}
	}
	public function designationlistbygroupid(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$group_id=$_SESSION["User"]["group_id"];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$GroupId = @$this->request->data['GroupId'];
		$CompanyId = @$this->request->data['CompanyId'];

		$designation_list=$this->Designation->find(
			'all',
			array(
				"fields" => array("Designation.id", "Designation.designation_name"),
				"conditions"=>array(
					'Designation.company_id' => $CompanyId,
					'Designation.group_id' => $GroupId,
					'Designation.designationisactive'=>1,
				)
			)
		);
		echo "<option value=\"\">Select Designation</option>";
		foreach($designation_list as $thisdesignation_list) {
			echo "<option value=\"".$thisdesignation_list['Designation']['id']."\">".$thisdesignation_list['Designation']['designation_name']."</option>";
		}	
	}
	public function departmentlistbygroupid(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$group_id=$_SESSION["User"]["group_id"];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$GroupId = @$this->request->data['GroupId'];
		$CompanyId = @$this->request->data['CompanyId'];

		$department_list=$this->Department->find(
			'all',
			array(
				"fields" => array("Department.id", "Department.department_name"),
				"conditions"=>array(
					'Department.company_id' => $CompanyId,
					'Department.group_id' => $GroupId,
					'Department.departmentisactive'=>1,
				)
			)
		);
		echo "<option value=\"\">Select Department</option>";
		foreach($department_list as $thisdepartment_list) {
			echo "<option value=\"".$thisdepartment_list['Department']['id']."\">".$thisdepartment_list['Department']['department_name']."</option>";
		}	
	}
	public function designationlistbygroupidsearch(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$group_id=$_SESSION["User"]["group_id"];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$UserGroupId = @$this->request->data['UserGroupId'];

		$designation_list=$this->Designation->find(
			'all',
			array(
				"fields" => array("Designation.id", "Designation.designation_name"),
				"conditions"=>array(
					'Designation.group_id' => $UserGroupId,
					'Designation.designationisactive'=>1,
				)
			)
		);
		echo "<option value=\"\">Select Designation</option>";
		foreach($designation_list as $thisdesignation_list) {
			echo "<option value=\"".$thisdesignation_list['Designation']['id']."\">".$thisdesignation_list['Designation']['designation_name']."</option>";
		}	
	}
	public function userinsertupdate($id = null, $uuid=null) {
		$this->layout='default';
		$userID = $this->Session->read('User.id');
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$this->set('group_id', $group_id);

		$this->set('title_for_layout', __('Insert/Edit User').' | '.__(Configure::read('site_name')));

		$companyname = $this->Company->find('list',array(
			"fields" => array("Company.id","company_name"),
			'conditions' => array('Company.companyisactive' => 1)
			));
		$this->set('companyname', $companyname);

		$shiftname = $this->Shift->find('list',array(
			"fields" => array("Shift.id","shift_name"),
			'conditions' => array('Shift.shiftisactive' => 1)
			));
		$this->set('shiftname', $shiftname);

		$condition_branch = array();
		if($group_id==1):
			$condition_branch[] = array('');
		elseif($group_id==2):
			$condition_branch[] = array('Branch.company_id'=>$company_id);
		else:
			$condition_branch[] = array('Branch.company_id'=>$company_id);
			$condition_branch[] = array('Branch.id'=>$branch_id);
		endif;

		$branchname = $this->Branch->find('list',array(
			"fields" => array("Branch.id","branch_name"),
			'conditions' => array('Branch.branchisactive' => 1, $condition_branch)
			));
		$this->set('branchname', $branchname);


		$condition_group = array();
		if($group_id==1):
			$condition_group[] = array('');
		elseif($group_id==2):
			$condition_group[] = array('NOT'=>array('Group.id'=>1));
		else:
			$condition_group[] = array('NOT'=>array('Group.id'=>array(1, 2)));
			//$condition_group[] = array('Group.id'=>$group_id);
		endif;

		$groupname = $this->Group->find('list', array(
			"fields" => array('id','groupname'),
			'conditions' => array('Group.groupisactive' => 1, $condition_group)
		));
		$this->set('groupname', $groupname);

		$salesperson = $this->User->find('list', array(
			"fields" => array("id", "user_fullname"),
			"conditions"=>array('User.group_id'=>5)
		));
		$this->set('salesperson', $salesperson);

		$department = $this->Department->find('list', array(
			"fields" => array("id", "department_name"),
			"conditions"=>array('Department.departmentisactive'=>1)
		));
		$this->set('department', $department);

		$designation = $this->Designation->find('list', array(
			"fields" => array("id", "designation_name"),
			"conditions"=>array('Designation.designationisactive'=>1)
		));
		$this->set('designation', $designation);

		$institute = $this->Institute->find('list', array(
			"fields" => array("id", "institute_name"),
			"conditions"=>array('Institute.instituteisactive'=>1)
		));
		$this->set('institute', $institute);
		$category_list = $this->Category->find('list', array(
			"fields" => array("id", "categoryname"),
			"conditions"=>array('Category.categoryisactive'=>1)
		));
		$this->set('category_list', $category_list);

		$subject_list = $this->Subject->find('list', array(
			"fields" => array("id", "subject_fullname"),
			"conditions"=>array('Subject.subjectisactive'=>1)
		));
		$this->set('subject_list', $subject_list);

		$condition_superior = array();
		$condition_superior[] = array('NOT'=>array('User.group_id'=>array(8, 9)));
		$condition_superior[] = array('User.company_id'=>$company_id);
		$authorized_superior = $this->User->find('list', array(
			"fields" => array("id", "user_fullname"),
			"conditions" => $condition_superior
		));
		$this->set('authorized_superior', $authorized_superior);
		$divisionlist=$this->Location->find(
			'list',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions"=>array(
					'Location.locationstep'=>0,
					'Location.locationisactive'=>1,
				)
			)
		);
		$this->set('divisionlist',$divisionlist);
		$districlist=$this->Location->find(
			'list',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions"=>array(
					'Location.locationstep'=>1,
					'Location.locationisactive'=>1,
				)
			)
		);
		$this->set('districlist',$districlist);
		$upazilalist=$this->Location->find(
			'list',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions"=>array(
					'Location.locationstep'=>2,
					'Location.locationisactive'=>1,
				)
			)
		);
		$this->set('upazilalist',$upazilalist);
		$unionparishadlist=$this->Location->find(
			'list',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions"=>array(
					'Location.locationstep'=>3,
					'Location.locationisactive'=>1,
				)
			)
		);
		$this->set('unionparishadlist',$unionparishadlist);
		$paralist=$this->Location->find(
			'list',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions"=>array(
					'Location.locationstep'=>4,
					'Location.locationisactive'=>1,
				)
			)
		);
		$this->set('paralist',$paralist);

		if(!empty($id) && $id!=0) {
			$user = $this->User->find('all', array("fields" => "User.*", "conditions" => array('User.id' => $id, 'User.useruuid'=>$uuid)));
			$this->set('user', $user);
		}
	}
	public function userlistbygroupid(){
		$this->layout = 'ajax';
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$GroupId = @$this->request->data['GroupId'];
		$salespersonid = @$this->request->data['salespersonid'];
		
		$condition = array();
		if($group_id==1):
			$condition[] = array('');
		elseif($group_id==2):
			$condition[] = array('User.company_id'=>$company_id);
		else:
			$condition[] = array('User.company_id'=>$company_id);
			$condition[] = array('User.branch_id'=>$branch_id);
		endif;
		$salesperson = $this->User->find('list', array(
			"fields" => array("id", "user_fullname"),
			"conditions"=>array('User.group_id'=>5, $condition)
		));
		$this->set('salesperson', $salesperson);
		$this->set('GroupId', $GroupId);
		$this->set('salespersonid', $salespersonid);
	}
	public function userbranchlistbycompany($companyID=null){
		$this->layout = 'ajax';
		$CompanyId = @$this->request->data['CompanyId'];
		$this->set('CompanyId',$CompanyId);
		$branches=$this->Branch->find(
			'list',
			array(
				"fields" => array("id", "branch_name"),
				"conditions"=>array('Branch.company_id'=>$CompanyId, 'Branch.branchisactive'=>1))
		);
		$this->set('branches',$branches);
	}
	public function usergroupinsert($userid = null, $companyID = null, $branchID = null, $groupID = null){
		$user_id = $this->Session->read('User.id'); 
	  	//$companyID = $this->Session->read('User.company_id');

	  	$count_user_id = $this->Usergroup->find('all', array("fields" => "Usergroup.*","conditions"=>array('Usergroup.user_id'=>$userid)));
	  	if($count_user_id > 0){
	  		$this->Usergroup->updateAll(array("Usergroup.usergroupisactive" => 0),array("Usergroup.user_id" => $userid));
	  		$this->request->data['Usergroup']['company_id']=$companyID;
	      	$this->request->data['Usergroup']['branch_id']=$branchID;
	      	$this->request->data['Usergroup']['group_id']=$groupID;
	      	$this->request->data['Usergroup']['user_id']=$userid;
	      	$this->request->data['Usergroup']['usergroupisactive'] = 1;
	      	$this->request->data['Usergroup']['usergroupinsertid']= $user_id;
	      	$this->request->data['Usergroup']['usergroupinsertdate']= date('Y-m-d H:i:s');
	      	$this->request->data['Usergroup']['usergroupuuid']=String::uuid();
	      	$this->Usergroup->create();
	      	$this->Usergroup->save($this->data);
	  	} else {
	  		$this->request->data['Usergroup']['company_id']=$companyID;
	      	$this->request->data['Usergroup']['branch_id']=$branchID;
	      	$this->request->data['Usergroup']['group_id']=$groupID;
	      	$this->request->data['Usergroup']['user_id']=$userid;
	      	$this->request->data['Usergroup']['usergroupisactive'] = 1;
	      	$this->request->data['Usergroup']['usergroupinsertid']= $user_id;
	      	$this->request->data['Usergroup']['usergroupinsertdate']= date('Y-m-d H:i:s');
	      	$this->request->data['Usergroup']['usergroupuuid']=String::uuid();
	      	$this->Usergroup->create();
	      	$this->Usergroup->save($this->data);
	  	}
	}
	public function userinstituteinsert($userid = null, $groupID = null, $instituteID = null){
		$user_id = $this->Session->read('User.id'); 
	  	//$companyID = $this->Session->read('User.company_id');

	  	$count_user_id = $this->Userinstitute->find('all', array("fields" => "Userinstitute.*","conditions"=>array('Userinstitute.user_id'=>$userid)));
	  	if($count_user_id > 0){
	  		$this->Userinstitute->updateAll(array("Userinstitute.userinstituteisactive" => 0),array("Userinstitute.user_id" => $userid));
	  		$this->request->data['Userinstitute']['institute_id']=$instituteID;
	      	$this->request->data['Userinstitute']['user_id']=$userid;
			$this->request->data['Userinstitute']['group_id']=$groupID;
	      	$this->request->data['Userinstitute']['userinstituteisactive'] = 1;
	      	$this->request->data['Userinstitute']['userinstituteinsertid']= $user_id;
	      	$this->request->data['Userinstitute']['userinstituteinsertdate']= date('Y-m-d H:i:s');
	      	$this->request->data['Userinstitute']['userinstituteuuid']=String::uuid();
	      	$this->Userinstitute->create();
	      	$this->Userinstitute->save($this->data);
	  	} else {
	      	$this->request->data['Userinstitute']['institute_id']=$instituteID;
	      	$this->request->data['Userinstitute']['user_id']=$userid;
			$this->request->data['Userinstitute']['group_id']=$groupID;
	      	$this->request->data['Userinstitute']['userinstituteisactive'] = 1;
	      	$this->request->data['Userinstitute']['userinstituteinsertid']= $user_id;
	      	$this->request->data['Userinstitute']['userinstituteinsertdate']= date('Y-m-d H:i:s');
	      	$this->request->data['Userinstitute']['userinstituteuuid']=String::uuid();
	      	$this->Userinstitute->create();
	      	$this->Userinstitute->save($this->data);
	  	}
	}
	public function userdepartmentinsert($userid = null, $departmentID = null){
		$user_id = $this->Session->read('User.id'); 
	  	//$companyID = $this->Session->read('User.company_id');

	  	$count_user_id = $this->Userdepartment->find('all', array("fields" => "Userdepartment.*","conditions"=>array('Userdepartment.user_id'=>$userid)));
	  	if($count_user_id > 0){
	  		$this->Userdepartment->updateAll(array("Userdepartment.userdepartmentisactive" => 0),array("Userdepartment.user_id" => $userid));
	  		$this->request->data['Userdepartment']['department_id']=$departmentID;
	      	$this->request->data['Userdepartment']['user_id']=$userid;
	      	$this->request->data['Userdepartment']['userdepartmentisactive'] = 1;
	      	$this->request->data['Userdepartment']['userdepartmentinsertid']= $user_id;
	      	$this->request->data['Userdepartment']['userdepartmentinsertdate']= date('Y-m-d H:i:s');
	      	$this->request->data['Userdepartment']['userdepartmentuuid']=String::uuid();
	      	$this->Userdepartment->create();
	      	$this->Userdepartment->save($this->data);
	  	} else {
	      	$this->request->data['Userdepartment']['department_id']=$departmentID;
	      	$this->request->data['Userdepartment']['user_id']=$userid;
	      	$this->request->data['Userdepartment']['userdepartmentisactive'] = 1;
	      	$this->request->data['Userdepartment']['userdepartmentinsertid']= $user_id;
	      	$this->request->data['Userdepartment']['userdepartmentinsertdate']= date('Y-m-d H:i:s');
	      	$this->request->data['Userdepartment']['userdepartmentuuid']=String::uuid();
	      	$this->Userdepartment->create();
	      	$this->Userdepartment->save($this->data);
	  	}
	}
	public function supervisorlistbygroupid(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$group_id=$_SESSION["User"]["group_id"];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$GroupId = @$this->request->data['GroupId'];

		$supervisor_list=$this->User->find(
			'all',
			array(
				"fields" => array("User.id", "User.user_fullname"),
				"conditions"=>array(
					'User.group_id' => $GroupId,
					'User.userisactive'=>1,
				)
			)
		);
		echo "<option value=\"\">Select Authorized Superior</option>";
		foreach($supervisor_list as $thissupervisor_list) {
			echo "<option value=\"".$thissupervisor_list['User']['id']."\">".$thissupervisor_list['User']['user_fullname']."</option>";
		}	
	}
	public function supervisorlistbygroupidanddepartmentid(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$group_id=$_SESSION["User"]["group_id"];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$GroupId = @$this->request->data['GroupId'];
		$UserDepartmentId = @$this->request->data['UserDepartmentId'];

		$supervisor_list=$this->User->find(
			'all',
			array(
				"fields" => array("User.id", "User.user_fullname"),
				"conditions"=>array(
					'User.group_id' => $GroupId,
					'User.department_id' => $UserDepartmentId,
					'User.userisactive'=>1,
				)
			)
		);
		echo "<option value=\"\">Select Authorized Superior</option>";
		foreach($supervisor_list as $thissupervisor_list) {
			echo "<option value=\"".$thissupervisor_list['User']['id']."\">".$thissupervisor_list['User']['user_fullname']."</option>";
		}	
	}
	public function userdesignationinsert($userid = null, $designationID = null){
		$user_id = $this->Session->read('User.id'); 
	  	//$companyID = $this->Session->read('User.company_id');

	  	$count_user_id = $this->Userdesignation->find('all', array("fields" => "Userdesignation.*","conditions"=>array('Userdesignation.user_id'=>$userid)));
	  	if($count_user_id > 0){
	  		$this->Userdesignation->updateAll(array("Userdesignation.userdesignationisactive" => 0),array("Userdesignation.user_id" => $userid));
	  		$this->request->data['Userdesignation']['designation_id']=$designationID;
	      	$this->request->data['Userdesignation']['user_id']=$userid;
	      	$this->request->data['Userdesignation']['userdesignationisactive'] = 1;
	      	$this->request->data['Userdesignation']['userdesignationinsertid']= $user_id;
	      	$this->request->data['Userdesignation']['userdesignationinsertdate']= date('Y-m-d H:i:s');
	      	$this->request->data['Userdesignation']['userdesignationuuid']=String::uuid();
	      	$this->Userdesignation->create();
	      	$this->Userdesignation->save($this->data);
	  	} else {
	      	$this->request->data['Userdesignation']['designation_id']=$designationID;
	      	$this->request->data['Userdesignation']['user_id']=$userid;
	      	$this->request->data['Userdesignation']['userdesignationisactive'] = 1;
	      	$this->request->data['Userdesignation']['userdesignationinsertid']= $user_id;
	      	$this->request->data['Userdesignation']['userdesignationinsertdate']= date('Y-m-d H:i:s');
	      	$this->request->data['Userdesignation']['userdesignationuuid']=String::uuid();
	      	$this->Userdesignation->create();
	      	$this->Userdesignation->save($this->data);
	  	}
	}
	public function usersalaryinsert($departmentID = null, $userid = null, $salaryStartdate = null, $initialSalary = null){
		$user_id = $this->Session->read('User.id'); 
	  	//$companyID = $this->Session->read('User.company_id');

	  	$count_user_id = $this->Salary->find('all', array("fields" => "Salary.*","conditions"=>array('Salary.user_id'=>$userid)));
	  	if($count_user_id > 0){
	  		$this->Salary->updateAll(array("Salary.salaryisactive" => 0),array("Salary.user_id" => $userid));
	  		$this->request->data['Salary']['department_id']=$departmentID;
	      	$this->request->data['Salary']['user_id']=$userid;
	      	$this->request->data['Salary']['salaryamount']=$initialSalary;
	      	$this->request->data['Salary']['salarystartdate']=$salaryStartdate;
	      	$this->request->data['Salary']['salaryisactive'] = 1;
	      	$this->request->data['Salary']['salaryinsertid']= $user_id;
	      	$this->request->data['Salary']['salaryinsertdate']= date('Y-m-d H:i:s');
	      	$this->request->data['Salary']['salaryuuid']=String::uuid();
	      	$this->Salary->create();
	      	$this->Salary->save($this->data);
	  	} else {
	      	$this->request->data['Salary']['department_id']=$departmentID;
	      	$this->request->data['Salary']['user_id']=$userid;
	      	$this->request->data['Salary']['salaryamount']=$initialSalary;
	      	$this->request->data['Salary']['salarystartdate']=$salaryStartdate;
	      	$this->request->data['Salary']['salaryisactive'] = 1;
	      	$this->request->data['Salary']['salaryinsertid']= $user_id;
	      	$this->request->data['Salary']['salaryinsertdate']= date('Y-m-d H:i:s');
	      	$this->request->data['Salary']['salaryuuid']=String::uuid();
	      	$this->Salary->create();
	      	$this->Salary->save($this->data);
	  	}
	}
	public function userinsertupdateaction($id=null, $uuid = null){
		$companyname = $this->Company->find('list', array(
			"fields" => array('id', 'companyname'),
		));
		$this->set('companyname', $companyname);

		$id = @$this->request->data['Users']['id'];
		$uuid = @$this->request->data['Users']['useruuid'];
		$userID = $this->Session->read('User.id');
		$group_id=$_SESSION["User"]["group_id"];
		$user_name = @$this->request->data['User']['username'];
		$email_address = @$this->request->data['User']['useremailaddress'];
		if(!empty($id) && !empty($uuid)){
        	$count_user_name = 0;
        } else {
        	$count_user_name = $this->User->find('all', array("fields" => "User.*","conditions"=>array('User.username'=>$user_name)));
            $count_user_name = count( $count_user_name );
        }
        if($count_user_name > 0){
        	$this->Session->setFlash(__("This User Name Already Exists"), 'default', array('class' => 'alert alert-warning'));
        	$this->redirect("/users/userinsertupdate");  
        } else {
        	if ($this->request->is('post')) {
			//print_r($_POST); exit();
				if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
					$this->request->data['User']['userupdateid']=$userID;
					$this->request->data['User']['userupdatedate']=date('Y-m-d H:i:s');
					$message = __('Update Successfully.');
					$this->User->id = $id;
					if($this->User->save($this->data)){
	                	$image = $this->request->data['User']['photo1']['name'];
	                	if (!empty($this->request->data['User']['photo1']['name'])) {
		                    $image_path = $this->Image->upload_image_and_thumbnail($image, $this->data['User']['photo1'], 600, 450, 150, 180, "user");
		                     if ($this->data['User']['photo2'] != '') {
		                        unlink(WWW_ROOT . 'img/user/big/' . $this->data['User']['photo2']);
		                        unlink(WWW_ROOT . 'img/user/small/' . $this->data['User']['photo2']);
		                        unlink(WWW_ROOT . 'img/user/home/' . $this->data['User']['photo2']);
		                    }
		                     if (isset($image_path)) {
		                        $this->User->saveField('userimage', $image_path);
		                        $uploaded = true;
		                    }
		                }
	                }
					// $this->Session->setFlash('Update Successfully.', 'default', array('class' => 'success'));
				}	
				else {
					$now = date('Y-m-d H:i:s');
					$this->request->data['User']['userpassword'] = ($this->request->data["User"]["userpassword"] ? $this->request->data["User"]["userpassword"] : 123456);
					$this->request->data['User']['userinsertid']=$userID;
					$this->request->data['User']['userinsertdate'] = $now;
					$this->request->data['User']['useruuid']=String::uuid();
					$message = __('Save Successfully.');
					if ($this->User->save($this->data)){
			            	$image = $this->request->data['User']['photo1']['name'];
			            	if (!empty($this->request->data['User']['photo1']['name'])) {
			            		$image_path = $this->Image->upload_image_and_thumbnail($image, $this->data['User']['photo1'], 600, 450, 150, 180, "user");
			            		if (isset($image_path)) {
			                        $this->User->saveField('userimage', $image_path);
			                        $uploaded = true;
			                    }
			            	}
			            }
				}
				if ($this->User->save($this->data)) {
					if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
						$userid = @$this->request->data['Users']['id'];
					} else {
						$userid = $this->User->getLastInsertId();
						$departmentID = $this->request->data['User']['department_id'];
						$salaryStartdate = $this->request->data['User']['userjoiningdate'];
						$initialSalary = ($this->request->data['User']['userjoiningsalary'] ? $this->request->data['User']['userjoiningsalary'] : 0);
						$this->usersalaryinsert($departmentID,$userid, $salaryStartdate, $initialSalary);
					}
					$companyID = $this->request->data['User']['company_id'];
					$branchID = $this->request->data['User']['branch_id'];
					$groupID = ($this->request->data['User']['group_id'] ? $this->request->data['User']['group_id'] : 0);
					$instituteID = ($this->request->data['User']['institute_id'] ? $this->request->data['User']['institute_id'] : 0);
					$departmentID = $this->request->data['User']['department_id'];
					$designationID = $this->request->data['User']['designation_id'];
					if($groupID == 9){
						$this->usergroupinsert($userid, $companyID, $branchID, $groupID);
						$this->userinstituteinsert($userid, $groupID, $instituteID);
					} else {
						$this->usergroupinsert($userid, $companyID, $branchID, $groupID);
						$this->userinstituteinsert($userid, $groupID, $instituteID);
						$this->userdepartmentinsert($userid, $departmentID);
						$this->userdesignationinsert($userid, $designationID);
					}
					$this->Session->setFlash($message, 'default', array('class' => 'success'));
				} else{
					$message = __('User Not Saved.');	
					$this->Session->setFlash($message, 'default', array('class' => 'red'));
				}
			}
			$this->redirect("/users/usermanage");	
        }
		
	}
	public function userprofileviewbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$users=$this->User->find('all',array("fields" => "User.*","conditions"=>array('User.id'=>$id,'User.useruuid'=>$uuid)));
		$this->set('users',$users);
	}
	public function usermanage($sgroup_id=null) {
		$this->layout='default';
		$this->set('title_for_layout', __('User Manage').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$userId=$_SESSION["User"]["id"];

		$user_id = array();
		$groupId = array();
		$companyId = array();
		$branchId = array();
		$departmentId = array();
		if($group_id==1):
		elseif($group_id==2):
			$user_id[] = array('User.company_id'=>$company_id);
			$groupId[] = array('NOT'=>array('Group.id'=>1));
			$companyId[] = array('Company.id'=>$company_id);
			$branchId[] = array('Branch.id'=>$branch_id);
			$departmentId[] = array('Department.company_id'=>$company_id);
		else:
			$user_id[] = array('User.company_id'=>$company_id,'User.branch_id'=>$branch_id, 'NOT'=>array('User.group_id'=>array(1, 2)));
			$groupId[] = array('NOT'=>array('Group.id'=>array(1, 2)));
			$companyId[] = array('Company.id'=>$company_id);
			$branchId[] = array('Branch.id'=>$branch_id);
			$departmentId[] = array('Department.company_id'=>$company_id);
		endif;
		if($sgroup_id!=''):
			$groupId[] = array('Group.id'=>$sgroup_id);
			$user_id[] = array('User.group_id'=>$sgroup_id);
		else:
			$sgroup_id = 0;
		endif;
		$this->set('sgroup_id',$sgroup_id);
        $group=$this->Group->find(
        	'list',
        	array(
        		"fields" => array("id","groupname"),
        		"conditions" => $groupId
        		)
        );
        $this->set('group',$group);

        $department=$this->Department->find(
			'list',
			array(
				"fields" => array("id","department_name"),
				"conditions" => $departmentId
			)
		);
		$this->set('department',$department);

        $company=$this->Company->find(
        	'list',
        	array(
        		"fields" => array("id","company_name"),
        		"conditions" => $companyId
        		)
        );
        $this->set('company',$company);
        $branch=$this->Branch->find(
        	'list',
        	array(
        		"fields" => array("id","branch_name"),
        		"conditions" => $branchId
        		)
        );
        $this->set('branch',$branch);
		$category = $this->Category->find('list', array(
			"fields" => array("id", "categoryname"),
			"conditions"=>array('Category.categoryisactive'=>1)
		));
        $this->set('category',$category);
 		$department = $this->Department->find('list', array(
			"fields" => array("id", "departmentname"),
			"conditions"=>array('Department.departmentisactive'=>1)
		));
        $this->set('department',$department);
 		$subject = $this->Subject->find('list', array(
			"fields" => array("id", "subjectname"),
			"conditions"=>array('Subject.subjectisactive'=>1)
		));
        $this->set('subject',$subject);
 		$designation = $this->Designation->find('list', array(
			"fields" => array("id", "designationname"),
			"conditions"=>array('Designation.designationisactive'=>1)
		));
        $this->set('designation',$designation);
        
 		$institute = $this->Institute->find('list', array(
			"fields" => array("id", "institutename"),
			'order'  =>'Institute.institutename ASC',    
			"conditions"=>array('Institute.instituteisactive'=>1)
		));
        $this->set('institute',$institute);
		$this->paginate = array(
			'fields' => array('User.*', 'Branch.*', 'Group.*', 'Company.*'), 
			'conditions' => $user_id,
			'order'  =>'User.userpunchmachineid ASC',
			'limit' => 20
		);
		$users = $this->paginate('User');
		$this->set('users',$users);
	}

	public function userdetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$this->paginate = array(
			'fields' => array('User.*', 'Useremergencycontact.*' ), 
			'conditions' => array('Useremergencycontact.user_id' =>$id),
			'order'  =>'Useremergencycontact.id ASC',
			'limit' => 20
		);
		$useremergencycontact = $this->paginate('Useremergencycontact');
		$this->set('useremergencycontact',$useremergencycontact);

		$this->paginate = array(
			'fields' => array('User.*', 'Userrelationship.*', 'Userdependent.*' ), 
			'conditions' => array('Userdependent.user_id' =>$id),
			'order'  =>'User.id ASC',
			//'condition' =
			'limit' => 20
		);
		$userdependent = $this->paginate('Userdependent');
		$this->set('userdependent',$userdependent);

		$users=$this->User->find('all',array("fields" => "User.*, Company.*, Group.*, Branch.*","conditions"=>array('User.id'=>$id,'User.useruuid'=>$uuid)));
		$this->set('users',$users);
	}

	public function usersearchbytext($Searchtext=null){
		$this->layout='ajax';
		$Searchtext = @$this->request->data['Searchtext'];
		$UserGroupId = @$this->request->data['UserGroupId'];
		$UserCompanyId = @$this->request->data['UserCompanyId'];
		$UserBranchId = @$this->request->data['UserBranchId'];
		$UserCategoryId = @$this->request->data['UserCategoryId'];
		$UserDepartmentId = @$this->request->data['UserDepartmentId'];
		$UserSubjectId = @$this->request->data['UserSubjectId'];
		$UserDesignationId = @$this->request->data['UserDesignationId'];
		$UserInstituteId = @$this->request->data['UserInstituteId'];

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$userId=$_SESSION["User"]["id"];

	
		$user_id = array();
		if($group_id==1):
			$user_id[] = array('');
		elseif($group_id==2):
			$user_id[] = array('User.company_id'=>$company_id);
		else:
			$user_id[] = array('User.branch_id'=>$branch_id, 'User.company_id'=>$company_id, 'NOT'=>array('User.group_id'=>array(1, 2)));
		endif;

		if(!empty($UserGroupId) && $UserGroupId!=''):
			$user_id[] = array('User.group_id'=>$UserGroupId);
		endif;
        if(!empty($UserCompanyId) && $UserCompanyId!=''):
            $user_id[] = array('User.company_id'=>$UserCompanyId);
        endif;
        if(!empty($UserBranchId) && $UserBranchId!=''):
            $user_id[] = array('User.branch_id'=>$UserBranchId);
        endif;
        if(!empty($UserCategoryId) && $UserCategoryId!=''):
            $user_id[] = array('User.category_id'=>$UserCategoryId);
        endif;
        if(!empty($UserDepartmentId) && $UserDepartmentId!=''):
            $user_id[] = array('User.department_id'=>$UserDepartmentId);
        endif;
        if(!empty($UserSubjectId) && $UserSubjectId!=''):
            $user_id[] = array('User.subject_id'=>$UserSubjectId);
        endif;
        if(!empty($UserDesignationId) && $UserDesignationId!=''):
            $user_id[] = array('User.designation_id'=>$UserDesignationId);
        endif;
        if(!empty($UserInstituteId) && $UserInstituteId!=''):
            $user_id[] = array('User.institute_id'=>$UserInstituteId);
        endif;
        if(!empty($Searchtext) && $Searchtext!=''):
            $user_id[] = array(
                'OR' => array(
                    'User.username LIKE ' => '%'.$Searchtext.'%',
                    'User.userfirstname LIKE ' => '%'.$Searchtext.'%',
                    'User.usermiddlename LIKE ' => '%'.$Searchtext.'%',
                    'User.userlastname LIKE ' => '%'.$Searchtext.'%',
                    'User.useremailaddress LIKE ' => '%'.$Searchtext.'%',
                    'User.usertelephone LIKE ' => '%'.$Searchtext.'%',
                    'User.userpunchmachineid' => ''.$Searchtext.''
                    //'Group.groupname LIKE ' => '%'.$Searchtext.'%',
                    //'Department.departmentname LIKE ' => '%'.$Searchtext.'%',
                    //'Company.companyname LIKE ' => '%'.$Searchtext.'%',
                    //'Company.companynamebn LIKE ' => '%'.$Searchtext.'%',
                    // 'Branch.branchname LIKE ' => '%'.$Searchtext.'%',
                    // 'Branch.branchnamebn LIKE ' => '%'.$Searchtext.'%'
                )
            );
        endif;

		$this->paginate = array(
			'fields' => array('User.*', 'Branch.*', 'Group.*', 'Company.*', 'Department.*'), 
			'conditions' => $user_id,
			'order'  =>'User.userpunchmachineid ASC',
			'limit' => 20
		);
		$users = $this->paginate('User');
		$this->set('users',$users);
    }

	public function userdelete($id = null,$uuid=null) {
		$this->layout='ajax';
		$userID = $this->Session->read('User.id');
		$this->request->data['User']['userdeleteid']=$userID;
		$this->request->data['User']['userdeletedate'] = date('Y-m-d H:i:s');
		$this->request->data['User']['userisactive']=2;
		$this->User->id = $id;
		$this->User->useruuid = $uuid;
		if ($this->User->save($this->data)) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'success'));
		}
		$this->redirect("/users/usermanage");
	}
	public function userprofile(){
		$this->layout = 'default';
		$this->set('title_for_layout', __('User Profile').' | '.__(Configure::read('site_name')));
		$userID = $this->Session->read('User.id');
		$this->User->id = $userID;
		//$this->data = $this->User->read();
		$user=$this->User->find('first',array("fields" => "User.*, Company.*, Branch.*","conditions"=>array('User.id'=>$userID)));
		$this->set('user', $user);
	}
	public function userchangepassword(){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Change Password').' | '.__(Configure::read('site_name')));
		$log = $this->Session->read('User');
		 if (isset($_POST['passwordsubmit'])){
		 	if (empty($this->data['User']['currentpassword'])) {
                $this->User->validationErrors['currentpassword'] = "Please Enter Current Password";
            }
            if (empty($this->data['User']['password'])) {
                $this->User->validationErrors['password'] = "Please Enter Password";
            }
            if (empty($this->data['User']['repassword'])) {
                $this->User->validationErrors['repassword'] = "Please Retype Password";
            }
            if (!empty($this->data['User']['currentpassword']) && !empty($this->data['User']['password']) && !empty($this->data['User']['repassword'])){
            	if ($this->User->Checkcurrentpassword($this->data, $log['userpassword'])) {
                    if ($this->data['User']['password'] == $this->data['User']['repassword']) {
                        $this->User->create();
                        $this->User->set($this->data);
                        $this->User->SavePassword($this->data);
                        $this->Session->setFlash('You\'ve successfully Change password.', 'default', array('class' => 'success'));
                        $this->redirect("userprofile");
                    } else {
                        $this->Session->setFlash('Password not same');
                    }
                } else {
                    $this->Session->setFlash('Current Password does not match');
                }
            } else {
                $this->set('errors', $this->User->invalidFields());
            }
		 }
	}
	public function userchangecreditlimit(){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Change Credit limit').' | '.__(Configure::read('site_name')));
		$users=$this->User->find(
			'list',
			array(
				"fields" => "User.user_fullname"
			)
		); 
		$this->set('users',$users);
		/*echo "<pre>";
		print_r($users);
		echo "</pre>";*/
		
		if(isset($_POST['changecreditlimitsubmit'])){
			if(!empty($this->data['User']['usercreditlimit'])){
				$this->User->create();
				$this->User->set($this->data);
			}
		}
	}
	public function usercreditlimitbyuser($userID=null){
		$this->layout = 'ajax';
		$User_id = @$this->request->data['User_id'];	
		$user_creditlimit=$this->User->find(
			'first',
			array(
				"fields" => array("id", "usercreditlimit"),
				"conditions"=>array('User.id'=>$User_id))
		);
		$this->set('user_creditlimit',$user_creditlimit);
	}


	public function userlistbygroup($groupID=null){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$GroupId = @$this->request->data['GroupId'];
		$users=$this->User->find('all',array("fields" => "User.*, Company.*, Group.*, Branch.*","conditions"=>array('User.group_id'=>$GroupId)));
		$userlist="";
		$userlist.="<option value=\"\">Select One</option>";
		foreach ($users as $Thisusers) {
			# code...
			$userlist.="<option value=\"{$Thisusers["User"]["id"]}\">{$Thisusers["User"]["user_fullname"]}</option>";
		}
		$userlist.="";
		//$this->set('users',$users);
		return $userlist;
	}

	public function salespersonbyuser($userID=null){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$userID = @$this->request->data['ClientId'];
		$users=$this->User->find('all',array("fields" => "User.salespersonid","conditions"=>array('User.id'=>$userID)));
		$salespersonid="";
		foreach ($users as $Thisusers) {
			# code...
			$salespersonid.="{$Thisusers["User"]["user_fullname"]}";
		}
		$salespersonid.="";
		//$this->set('users',$users);
		return $salespersonid;
	}

	public function usertotalamountbyid($id=null){
		$user=$this->Usertransaction->find(
			'all',
			array(
				"fields" => array("SUM(Usertransaction.transactionamount) AS Usertotalamount"),
				"conditions"=>array('Usertransaction.user_id'=>$id)
			)
		);
		return $user[0][0]["Usertotalamount"]?$user[0][0]["Usertotalamount"]:0;
	}
	// Start User Group Assign //
	public function usergroupmanage(){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Manage User Group Assign').' | '.__(Configure::read('site_name')));
		$userID = $this->Session->read('User.id');
		$group_id=$_SESSION["User"]["group_id"];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
                
                $usergroup_id = array();
                $groupId = array();
		$companyId = array();
		$branchId = array();
		$userId = array();
		if($group_id==1):
                        $groupId[] = array('');
			$companyId[] = array('');
			$branchId[] = array('');
			$userId[] = array('');
			$usergroup_id[] = array('');
		elseif($group_id==2): 
                        $groupId[] = array('NOT'=>array('Group.id'=>1)); 
			$companyId[] = array('Company.id'=>$company_id);
			$branchId[] = array('Branch.company_id'=>$company_id);
			$userId[] = array('User.company_id' => $company_id);
			$usergroup_id[] = array('Usergroup.company_id' => $company_id);
		else:
            $groupId[] = array('NOT'=>array('Group.id'=> array(1,2))); 
			$companyId[] = array('Company.id'=>$company_id);
			$branchId[] = array('Branch.id'=>$branch_id);
			$userId[] = array('User.company_id' => $company_id, 'User.branch_id' => $branch_id);
			$usergroup_id[] = array('Usergroup.branch_id'=>$branch_id, 'Usergroup.company_id'=>$company_id);
		endif;
                
        $group = $this->Group->find(
        	'list',
        	array(
        		"fields" => array("id","groupname"),
        		"conditions" => $groupId
        		)
        );
        $this->set('group',$group);
        $company=$this->Company->find(
        	'list',
        	array(
        		"fields" => array("id","company_name"),
        		"conditions" => $companyId
        		)
        );
        $this->set('company',$company);
        $branch=$this->Branch->find(
        	'list',
        	array(
        		"fields" => array("id","branch_name"),
        		"conditions" => $branchId
        		)
        );
        $this->set('branch',$branch);
        $user=$this->User->find(
        	'list',
        	array(
        		"fields" => array("id","user_fullname"),
        		"conditions" => $userId
        		)
        );
        $this->set('user',$user);
		$this->paginate = array(
			'fields' => 'Usergroup.*',
			//'order'  =>'Product.productname ASC',
			"conditions" => $usergroup_id,
			'limit' => 20
		);
		$usergroup = $this->paginate('Usergroup');
		$this->set('usergroup',$usergroup);
	}

	public function usergroupinsertupdate($id = null, $uuid=null){
		$this->layout = 'default';
		$this->set('title_for_layout', __('User Group Assign').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];

		$condition_user = array();
		$condition_group = array();
		if($group_id==1):
			$condition_user[] = array('');
			$condition_group[] = array('');
		elseif($group_id==2):
			$condition_user[] = array('User.company_id' => $company_id);
			//$condition_group[] = array('NOT'=>array('Group.id'=>1));
			$condition_user[] = array('User.group_id'=> array(4, 7));
			$condition_group[] = array('Group.id'=> array(4, 7));
		else:
			$condition_user[] = array('User.company_id' => $company_id, 'User.branch_id' => $branch_id);
			//$condition_group[] = array('NOT'=>array('Group.id'=>array(1, 2)));
			$condition_user[] = array('User.group_id'=> array(4, 7));
			$condition_group[] = array('Group.id'=> array(4, 7));
		endif;
		
		$user_list = $this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions" => $condition_user
			)
		);
		$this->set('user_list', $user_list);

		$group_list = $this->Group->find(
			'list',
			array(
				"fields" => array("id", "groupname"), 
				"conditions" => $condition_group
			)
		);
		$this->set('group_list', $group_list);

		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0) {
			$usergroup = $this->Usergroup->find('all', array("fields" => "Usergroup.*", "conditions" => array('Usergroup.id' => $id, 'Usergroup.usergroupuuid'=>$uuid)));
			$this->set('usergroup', $usergroup);
		}
	}

	public function usergroupinsertupdateaction($id=null, $uuid = null){
		$id = @$this->request->data['Users']['id'];
		$uuid = @$this->request->data['Users']['usergroupuuid'];
		$userID = $this->Session->read('User.id');
		//$company_id = $_SESSION["User"]["company_id"];
		$group_id = $this->request->data['Usergroup']['group_id'];
      	$user_id = $this->request->data['Usergroup']['user_id'];

		$result = $this->Usergroup->find('all', array("fields" => "Usergroup.*","conditions"=>array('Usergroup.group_id' => $group_id, 'Usergroup.user_id' => $user_id)));
		if(count($result) > 0){
      		$this->Session->setFlash(__("This User Group Already Exists"), 'default', array('class' => 'alert alert-warning'));
        	$this->redirect("/users/usergroupinsertupdate");  
      	} else{
      		if ($this->request->is('post')){
	      		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
	      			$this->request->data['Usergroup']['usergroupupdateid']=$userID;
					$this->request->data['Usergroup']['usergroupupdatedate']=date('Y-m-d H:i:s');
					$message = __('Update Successfully.');
					$this->Usergroup->id = $id;
	      		} else {
	      			$now = date('Y-m-d H:i:s');
	      			$this->request->data['Usergroup']['usergroupuuid']=String::uuid();
	      			$this->request->data['Usergroup']['company_id'] = $this->data['Users']['company_id'];
	      			//$this->request->data['Usergroup']['branch_id'] = $this->data['UsersBranchId'];
	      			$this->request->data['Usergroup']['branch_id'] = $this->data['Users']['branch_id'];
		      		$this->request->data['Usergroup']['usergroupinsertid'] = $userID;
		      		$this->request->data['Usergroup']['usergroupinsertdate'] = $now;
		      		$message = __('Save Successfully.');	
	      		}
	      		if ($this->Usergroup->save($this->data)) {
					$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				}
	      	}
	      	$this->redirect("usergroupmanage");
      	} 
	}

	public function usergroupdetailsbyid($id=null, $uuid=null){
		$this->layout='ajax';
		$usergroup=$this->Usergroup->find('all',array("fields" => "User.*, Group.*, Branch.*, Company.*, Usergroup.*", "conditions"=>array('Usergroup.id'=>$id,'Usergroup.usergroupuuid'=>$uuid)));
		$this->set('usergroup',$usergroup);
	}

	public function usergroupdelete($id=null, $uuid=null){
		$userID = $this->Session->read('User.id');
		if ($this->Usergroup->updateAll(array('usergroupdeleteid'=>$userID, 'usergroupdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'usergroupisactive'=>2), array('AND' => array('Usergroup.id'=>$id,'Usergroup.usergroupuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/users/usergroupmanage");
	}
        public function usergroupsearchbytext($Searchtext=null){
            $this->layout='ajax';
		//$Searchtext = @$this->request->data['Searchtext'];
        $UsergroupGroupId = @$this->request->data['UsergroupGroupId'];
        $UsergroupCompanyId = @$this->request->data['UsergroupCompanyId'];
        $UsergroupBranchId = @$this->request->data['UsergroupBranchId'];
        $UsergroupUserId = @$this->request->data['UsergroupUserId'];

        $company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$userId=$_SESSION["User"]["id"];

		$condition = array();
		if($group_id==1):
			$condition[] = array('');
		elseif($group_id==2):
			$condition[] = array('Usergroup.company_id'=>$company_id);
		else:
			$condition[] = array('Usergroup.branch_id'=>$branch_id, 'Usergroup.company_id'=>$company_id);
		endif;
        if(!empty($UsergroupGroupId) && $UsergroupGroupId!=''):
            $condition[] = array('Usergroup.group_id'=>$UsergroupGroupId);
        endif;
        if(!empty($UsergroupCompanyId) && $UsergroupCompanyId!=''):
            $condition[] = array('Usergroup.company_id'=>$UsergroupCompanyId);
        endif;
        if(!empty($UsergroupBranchId) && $UsergroupBranchId!=''):
            $condition[] = array('Usergroup.branch_id'=>$UsergroupBranchId);
        endif;
        if(!empty($UsergroupUserId) && $UsergroupUserId!=''):
            $condition[] = array('Usergroup.user_id'=>$UsergroupUserId);
        endif;

        $this->paginate = array(
			'fields' => array('Usergroup.*', 'Branch.*', 'User.*', 'Company.*', 'Group.*'), 
			'conditions' => $condition,
			//'order'  =>'User.id ASC',
			//'condition' =
			'limit' => 20
		);
		$usergroup = $this->paginate('Usergroup');
		$this->set('usergroup',$usergroup);
        }

	public function companybranchbyuser(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$json_array = array();
		$UsergroupUserId = @$this->request->data['UsergroupUserId'];
		$companyBranchQuery=$this->User->find(
			'first',
			array(
				"fields" => array("User.id","User.company_id", "User.branch_id"),
				"conditions" => array(
					'User.id'=>$UsergroupUserId
				)
			)
		);
		$companyID = @$companyBranchQuery["User"]["company_id"];
		$branchID = @$companyBranchQuery["User"]["branch_id"];
		$json_array["companyID"] = $companyID;
		$json_array["branchID"] = $branchID;
		return json_encode($json_array);
	}
	// End User Group Assign //

	public function notificationmanage(){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Notification').' | '.__(Configure::read('site_name')));
		$userlist = $this->User->find('all', array(
			"fields" => array("User.*"),
			"conditions"=>array('User.userisactive' => 1)
		));
		$this->set('userlist', $userlist);
	}

	// Start Useremergencycontact //
	public function useremergencycontactmanage(){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Manage Emergency Contact').' | '.__(Configure::read('site_name')));
		$userID = $this->Session->read('User.id');
		$group_id=$_SESSION["User"]["group_id"];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
                
        $usergroup_id = array();
        $condition_user = array();
        //$groupId = array();
		//$companyId = array();
		//$branchId = array();
		//$userId = array();
		if($group_id==1):
            //$groupId[] = array('');
			//$companyId[] = array('');
			//$branchId[] = array('');
			//$userId[] = array('');
			$usergroup_id[] = array('');
			$condition_user[] = array('');
		elseif($group_id==2): 
            //$groupId[] = array('NOT'=>array('Group.id'=>1)); 
			//$companyId[] = array('Company.id'=>$company_id);
			//$branchId[] = array('Branch.company_id'=>$company_id);
			//$userId[] = array('User.company_id' => $company_id);
			$usergroup_id[] = array('Usergroup.company_id' => $company_id);
			$condition_user[] = array('User.company_id' => $company_id);
		else:
            //$groupId[] = array('NOT'=>array('Group.id'=> array(1,2))); 
			//$companyId[] = array('Company.id'=>$company_id);
			//$branchId[] = array('Branch.id'=>$branch_id);
			//$userId[] = array('User.company_id' => $company_id, 'User.branch_id' => $branch_id);
			$usergroup_id[] = array('Usergroup.branch_id'=>$branch_id, 'Usergroup.company_id'=>$company_id);
			$condition_user[] = array('User.company_id' => $company_id, 'User.branch_id' => $branch_id);
		endif;
        
 		$department=$this->Department->find(
		'list',
		array(
		"fields" => array("id","department_name")
		)
		);
		$this->set('department',$department);
 
      $user_list=$this->User->find(
        	'list',
        	array(
        		"fields" => array("id","user_fullname"),
        		"conditions" => $condition_user
        		)
        );
        $this->set('user_list',$user_list);
		$this->paginate = array(
			'fields' => 'Useremergencycontact.*',
			//'order'  =>'Product.productname ASC',
			//"conditions" => $usergroup_id,
			'limit' => 20
		);
		$useremergencycontact = $this->paginate('Useremergencycontact');
		$this->set('useremergencycontact',$useremergencycontact);
	}
	public function useremergencycontactinsertupdate($id = null, $uuid=null){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Emergency Contact').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];

		$condition_user = array();
		$condition_group = array();
		if($group_id==1):
			$condition_user[] = array('');
			$condition_department[] = array();
		elseif($group_id==2):
			$condition_user[] = array('User.company_id' => $company_id);
			$condition_department[] = array('Department.company_id' => $company_id);
		else:
			$condition_user[] = array('User.company_id' => $company_id, 'User.branch_id' => $branch_id);
			$condition_department[] = array('Department.company_id' => $company_id);
		endif;

		$user_list = $this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				'conditions' => $condition_user
			)
		);
		$this->set('user_list', $user_list);
		
		$department_list = $this->Department->find(
			'list',
			array(
				"fields" => array("id", "departmentname"),
				"conditions" => $condition_department
			)
		);
		$this->set('department_list', $department_list);

		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0) {
			$useremergencycontact = $this->Useremergencycontact->find('all', array("fields" => "Useremergencycontact.*", "conditions" => array('Useremergencycontact.id' => $id, 'Useremergencycontact.useremergencycontactuuid'=>$uuid)));
			$this->set('useremergencycontact', $useremergencycontact);
		}
	}
	public function  useremergencycontactinsertupdateaction($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		$company_id = $_SESSION["User"]["company_id"];
		$branch_id = $_SESSION["User"]["branch_id"];

		$id = @$this->request->data['Users']['id'];
		$uuid = @$this->request->data['Users']['useremergencycontactuuid'];
		if ($this->request->is('post')) {
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
				$this->request->data['Useremergencycontact']['useremergencycontactupdateid']=$userID;
				$this->request->data['Useremergencycontact']['useremergencycontactupdatedate']=date('Y-m-d H:i:s');
				$message = __('Update Successfully.');
				$this->Useremergencycontact->id = $id;
			else:
				$this->request->data['Useremergencycontact']['useremergencycontactinsertid']=$userID;
				$this->request->data['Useremergencycontact']['useremergencycontactinsertdate']=date('Y-m-d H:i:s');
				$this->request->data['Useremergencycontact']['useremergencycontactuuid']=String::uuid();
				$message = __('Save Successfully.');
			endif;
			if ($this->Useremergencycontact->save($this->request->data, array('validate' => 'only'))) {
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				$this->redirect("/users/useremergencycontactmanage");
			}else{
				$errors = $this->Useremergencycontact->invalidFields();
				$this->set('errors', $this->Useremergencycontact->invalidFields());
				//$this->Session->setFlash('The post was not saved... Please try again!', 'default', array('class' => 'alert alert-warning'));
				$this->redirect("/users/useremergencycontactinsertupdate");
			}
		}
	}

    public function useremergencycontactlistbydepartmentid($companyID=null){

		$this->layout = 'ajax';
		$UseremergencycontactDepartmentId = @$this->request->data['UseremergencycontactDepartmentId'];
//		$DepartmentId = @$this->request->data['DepartmentId'];
		$this->set('UseremergencycontactDepartmentId',$UseremergencycontactDepartmentId);
		$user_list=$this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions"=>array('User.department_id'=>$UseremergencycontactDepartmentId, 'User.userisactive'=>1))
		);
		$this->set('user_list',$user_list);
	}


	public function useremergencycontactdetailsbyid($id=null,$uuid=null){
		 
		$this->layout='ajax';
		$useremergencycontact=$this->Useremergencycontact->find('all',array("fields" => "Useremergencycontact.*","conditions"=>array('Useremergencycontact.id'=>$id,'Useremergencycontact.useremergencycontactuuid'=>$uuid)));
		$this->set('useremergencycontact',$useremergencycontact);
	}
	public function useremergencycontactdelete($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		if ($this->Useremergencycontact->updateAll(array('useremergencycontactdeleteid'=>$userID, 'useremergencycontactdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'useremergencycontactisactive'=>2), array('AND' => array('Useremergencycontact.id'=>$id,'Useremergencycontact.useremergencycontactuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/users/useremergencycontactmanage");
	}
	public function useremergencycontactsearchbytext($Searchtext=null){
		$this->layout='ajax';
		$Searchtext = @$this->request->data['Searchtext'];
		$UseremergencycontactUserId = @$this->request->data['UseremergencycontactUserId'];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$userId=$_SESSION["User"]["id"];

		$useremergencycontact_id = array();
		/*if($group_id==1):
			$user_id[] = array('');
		elseif($group_id==2):
			$user_id[] = array('User.company_id'=>$company_id);
		else:
			$user_id[] = array('User.branch_id'=>$branch_id, 'User.company_id'=>$company_id, 'NOT'=>array('User.group_id'=>array(1, 2)));
		endif;
        */
        if(!empty($UseremergencycontactUserId) && $UseremergencycontactUserId!=''):
			$useremergencycontact_id[] = array('Useremergencycontact.user_id'=>$UseremergencycontactUserId);
		endif;
        if(!empty($Searchtext) && $Searchtext!=''):
            $useremergencycontact_id[] = array(
                'OR' => array(
                    'Useremergencycontact.useremergencycontactname LIKE ' => '%'.$Searchtext.'%',
                    'Useremergencycontact.useremergencycontactnamebn LIKE ' => '%'.$Searchtext.'%',
                    'Useremergencycontact.useremergencycontactrelationship LIKE ' => '%'.$Searchtext.'%',
                    'Useremergencycontact.useremergencycontacthomephone LIKE ' => '%'.$Searchtext.'%',
                    'Useremergencycontact.useremergencycontactmobilephone LIKE ' => '%'.$Searchtext.'%',
                    'Useremergencycontact.useremergencycontactofficephone LIKE ' => '%'.$Searchtext.'%'
                )
            );
        endif;

		$this->paginate = array(
			'fields' => array('Useremergencycontact.*'), 
			'conditions' => $useremergencycontact_id,
			'order'  =>'Useremergencycontact.id ASC',
			//'condition' =
			'limit' => 20
		);
		$useremergencycontact = $this->paginate('Useremergencycontact');
		$this->set('useremergencycontact',$useremergencycontact);
    }
	// End Useremergencycontact //

	// Start User Relationship //
	public function userrelationshipmanage(){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Manage Employee Relationship').' | '.__(Configure::read('site_name')));
		$userID = $this->Session->read('User.id');
		$group_id=$_SESSION["User"]["group_id"];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];

		$this->paginate = array(
			'fields' => 'Userrelationship.*',
			//'order'  =>'Product.productname ASC',
			//"conditions" => $usergroup_id,
			'limit' => 20
		);
		$userrelationship = $this->paginate('Userrelationship');
		$this->set('userrelationship',$userrelationship);
	}	
	public function userrelationshipinsertupdate($id = null, $uuid=null){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Employee Relationship').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];

		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0) {
			$userrelationship = $this->Userrelationship->find('all', array("fields" => "Userrelationship.*", "conditions" => array('Userrelationship.id' => $id, 'Userrelationship.userrelationshipuuid'=>$uuid)));
			$this->set('userrelationship', $userrelationship);
		}
	}
	public function  userrelationshipinsertupdateaction($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		$company_id = $_SESSION["User"]["company_id"];
		$branch_id = $_SESSION["User"]["branch_id"];

		$id = @$this->request->data['Users']['id'];
		$uuid = @$this->request->data['Users']['userrelationshipuuid'];
		if ($this->request->is('post')) {
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
				$this->request->data['Userrelationship']['userrelationshipupdateid']=$userID;
				$this->request->data['Userrelationship']['userrelationshipupdatedate']=date('Y-m-d H:i:s');
				$message = __('Update Successfully.');
				$this->Userrelationship->id = $id;
			else:
				$this->request->data['Userrelationship']['userrelationshipinsertid']=$userID;
				$this->request->data['Userrelationship']['userrelationshipinsertdate']=date('Y-m-d H:i:s');
				$this->request->data['Userrelationship']['userrelationshipuuid']=String::uuid();
				$message = __('Save Successfully.');
			endif;
			if ($this->Userrelationship->save($this->request->data, array('validate' => 'only'))) {
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				$this->redirect("/users/userrelationshipmanage");
			}else{
				$errors = $this->Userrelationship->invalidFields();
				$this->set('errors', $this->Userrelationship->invalidFields());
				//$this->Session->setFlash('The post was not saved... Please try again!', 'default', array('class' => 'alert alert-warning'));
				$this->redirect("/users/userrelationshipinsertupdate");
			}
		}
	}
	public function userrelationshipdetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$userrelationship=$this->Userrelationship->find('all',array("fields" => "Userrelationship.*","conditions"=>array('Userrelationship.id'=>$id,'Userrelationship.userrelationshipuuid'=>$uuid)));
		$this->set('userrelationship',$userrelationship);
	}
	public function userrelationshipdelete($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		if ($this->Userrelationship->updateAll(array('userrelationshipdeleteid'=>$userID, 'userrelationshipdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'userrelationshipisactive'=>2), array('AND' => array('Userrelationship.id'=>$id,'Userrelationship.userrelationshipuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/users/userrelationshipmanage");
	}
	public function userrelationshipsearchbytext($Searchtext=null){
		$this->layout='ajax';
		$Searchtext = @$this->request->data['Searchtext'];
		$userrelationship_id = array();
		if(!empty($Searchtext) && $Searchtext!=''):
			$userrelationship_id[] = array(
				'OR' => array(
					'Userrelationship.userrelationshipname LIKE ' => '%'.$Searchtext.'%'
				)
			);
		endif;

		$this->paginate = array(
			'fields' => 'Userrelationship.*',
			'order'  =>'Userrelationship.userrelationshipname ASC',
			"conditions"=>$userrelationship_id,
			'limit' => 20
		);
		$userrelationship = $this->paginate('Userrelationship');
		$this->set('userrelationship',$userrelationship);
	}
	// End User Relationship //
	
	// Start Userdependent //
	public function userdependentmanage(){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Manage Family Details').' | '.__(Configure::read('site_name')));
		$userID = $this->Session->read('User.id');
		$group_id=$_SESSION["User"]["group_id"];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
                
        $usergroup_id = array();
        $groupId = array();
		$companyId = array();
		$branchId = array();
		$userId = array();
		$condition_user = array();
		if($group_id==1):
            $groupId[] = array('');
			$companyId[] = array('');
			$branchId[] = array('');
			$userId[] = array('');
			$usergroup_id[] = array('');
			$condition_user[] = array('');
		elseif($group_id==2): 
            $groupId[] = array('NOT'=>array('Group.id'=>1)); 
			$companyId[] = array('Company.id'=>$company_id);
			$branchId[] = array('Branch.company_id'=>$company_id);
			$userId[] = array('User.company_id' => $company_id);
			$usergroup_id[] = array('Usergroup.company_id' => $company_id);
			$condition_user[] = array('User.company_id' => $company_id);
		else:
            $groupId[] = array('NOT'=>array('Group.id'=> array(1,2))); 
			$companyId[] = array('Company.id'=>$company_id);
			$branchId[] = array('Branch.id'=>$branch_id);
			$userId[] = array('User.company_id' => $company_id, 'User.branch_id' => $branch_id);
			$usergroup_id[] = array('Usergroup.branch_id'=>$branch_id, 'Usergroup.company_id'=>$company_id);
			$condition_user[] = array('User.company_id' => $company_id, 'User.branch_id' => $branch_id);
		endif;
        
       $user_list = $this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions" => $condition_user
			)
		);
		$this->set('user_list', $user_list);
		$user_relationship = $this->Userrelationship->find(
			'list',
			array(
				"fields" => array("id", "userrelationship_name")
			)
		);
		$this->set('user_relationship', $user_relationship);
		$this->paginate = array(
			'fields' => 'Userdependent.*',
			//'order'  =>'Product.productname ASC',
			//"conditions" => $usergroup_id,
			'limit' => 20
		);
		$userdependent = $this->paginate('Userdependent');
		$this->set('userdependent',$userdependent);
	}
	public function userdependentinsertupdate($id = null, $uuid=null){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Dependents').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];

		$condition_user = array();
		$condition_group = array();
		if($group_id==1):
			$condition_user[] = array('');
			$condition_group[] = array('');
		elseif($group_id==2):
			$condition_user[] = array('User.company_id' => $company_id);
			$condition_department[] = array('Department.company_id' => $company_id);
		else:
			$condition_user[] = array('User.company_id' => $company_id, 'User.branch_id' => $branch_id);
			$condition_department[] = array('Department.company_id' => $company_id);
		endif;
		
		$department_list = $this->Department->find(
			'list',
			array(
				"fields" => array("id", "departmentname"),
				"conditions" => $condition_department
			)
		);
		$this->set('department_list', $department_list);
		$user_list = $this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions" => $condition_user
			)
		);
		$this->set('user_list', $user_list);
		$user_relationship = $this->Userrelationship->find(
			'list',
			array(
				"fields" => array("id", "userrelationship_name")
			)
		);
		$this->set('user_relationship', $user_relationship);

		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0) {
			$userdependent = $this->Userdependent->find('all', array("fields" => "Userdependent.*", "conditions" => array('Userdependent.id' => $id, 'Userdependent.userdependentuuid'=>$uuid)));
			$this->set('userdependent', $userdependent);
		}
	}
	public function  userdependentinsertupdateaction($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		$company_id = $_SESSION["User"]["company_id"];
		$branch_id = $_SESSION["User"]["branch_id"];

		$id = @$this->request->data['Users']['id'];
		$uuid = @$this->request->data['Users']['userdependentuuid'];
		if ($this->request->is('post')) {
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
				$this->request->data['Userdependent']['userdependentupdateid']=$userID;
				$this->request->data['Userdependent']['userdependentupdatedate']=date('Y-m-d H:i:s');
				$message = __('Update Successfully.');
				$this->Userdependent->id = $id;	
			}	else{
				$this->request->data['Userdependent']['userdependentinsertid']=$userID;
				$this->request->data['Userdependent']['userdependentinsertdate']=date('Y-m-d H:i:s');
				$this->request->data['Userdependent']['userdependentuuid']=String::uuid();
				$message = __('Save Successfully.');
			}
			$count = $this->request->data["totlarow"];
			for($i = 1; $i<=$count; $i++){
				$this->request->data["Userdependent"]["user_id"] = $this->request->data["Userdependent"]["user_id"];
				$this->request->data["Userdependent"]["userdependentname"] = $this->request->data["Users"]["userdependentname{$i}"];
				$this->request->data["Userdependent"]["userdependentnamebn"] = $this->request->data["Users"]["userdependentnamebn{$i}"];
				$this->request->data["Userdependent"]["userrelationship_id"] = $this->request->data["Users"]["userrelationship_id{$i}"];
				$this->request->data["Userdependent"]["userdependentdob"] = $this->request->data["Users"]["userdependentdob{$i}"];
				$this->request->data["Userdependent"]["userdependentisactive"] = $this->request->data["Users"]["userdependentisactive{$i}"];
				if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
				} else{
					$this->Userdependent->create();
				}
				$this->Userdependent->save($this->data);
			}
			if ($this->Userdependent->save($this->request->data, array('validate' => 'only'))) {
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				$this->redirect("/users/userdependentmanage");
			}else{
				$errors = $this->Userdependent->invalidFields();
				$this->set('errors', $this->Userdependent->invalidFields());
				//$this->Session->setFlash('The post was not saved... Please try again!', 'default', array('class' => 'alert alert-warning'));
				$this->redirect("/users/userdependentinsertupdate");
			}
		}
	}

    public function userdependentlistbydepartmentid($companyID=null){

		$this->layout = 'ajax';
		$UserdependentDepartmentId = @$this->request->data['UserdependentDepartmentId'];
//		$DepartmentId = @$this->request->data['DepartmentId'];
		$this->set('UserdependentDepartmentId',$UserdependentDepartmentId);
		$user_list=$this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions"=>array('User.department_id'=>$UserdependentDepartmentId, 'User.userisactive'=>1))
		);
		$this->set('user_list',$user_list);
	}

	public function userdependentdetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$userdependent=$this->Userdependent->find('all',array("fields" => "Userdependent.*","conditions"=>array('Userdependent.id'=>$id,'Userdependent.userdependentuuid'=>$uuid)));
		$this->set('userdependent',$userdependent);
	}
	public function userdependentdelete($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		if ($this->Userdependent->updateAll(array('userdependentdeleteid'=>$userID, 'userdependentdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'userdependentisactive'=>2), array('AND' => array('Userdependent.id'=>$id,'Userdependent.userdependentuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/users/userdependentmanage");
	}
	public function userdependentsearchbytext($Searchtext=null){
		$this->layout='ajax';
		$Searchtext = @$this->request->data['Searchtext'];
		$UserdependentUserId = @$this->request->data['UserdependentUserId'];
		$UserdependentUserrelationshipId = @$this->request->data['UserdependentUserrelationshipId'];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$userId=$_SESSION["User"]["id"];

		$userdependent_id = array();
		/*if($group_id==1):
			$user_id[] = array('');
		elseif($group_id==2):
			$user_id[] = array('User.company_id'=>$company_id);
		else:
			$user_id[] = array('User.branch_id'=>$branch_id, 'User.company_id'=>$company_id, 'NOT'=>array('User.group_id'=>array(1, 2)));
		endif;
        */
        if(!empty($UserdependentUserId) && $UserdependentUserId!=''):
			$userdependent_id[] = array('Userdependent.user_id'=>$UserdependentUserId);
		endif;
		if(!empty($UserdependentUserrelationshipId) && $UserdependentUserrelationshipId!=''):
			$userdependent_id[] = array('Userdependent.userrelationship_id'=>$UserdependentUserrelationshipId);
		endif;
        if(!empty($Searchtext) && $Searchtext!=''):
            $userdependent_id[] = array(
                'OR' => array(
                    'Userdependent.userdependentname LIKE ' => '%'.$Searchtext.'%',
                    'Userdependent.userdependentnamebn LIKE ' => '%'.$Searchtext.'%',
                    'Userdependent.userdependentrelationship LIKE ' => '%'.$Searchtext.'%',
                    'Userdependent.userdependentdob LIKE ' => '%'.$Searchtext.'%'
                )
            );
        endif;

		$this->paginate = array(
			'fields' => array('Userdependent.*'), 
			'conditions' => $userdependent_id,
			'order'  =>'Userdependent.id ASC',
			//'condition' =
			'limit' => 20
		);
		$userdependent = $this->paginate('Userdependent');
		$this->set('userdependent',$userdependent);
    }
	// End Userdependent //
	// Start Usereducation //
	public function usereducationmanage(){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Manage Education').' | '.__(Configure::read('site_name')));
		$userID = $this->Session->read('User.id');
		$group_id=$_SESSION["User"]["group_id"];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
                
        $usereducation_id = array();
        $condition_user = array();
       	$condition_department = array();
		if($group_id==1):
			$condition_user[] = array('');
			$usereducation_id[] = array('');
			$condition_department[] = array('');
		elseif($group_id==2):
			$condition_user[] = array('User.company_id' => $company_id);
			$usereducation_id[] = array('Usereducation.usereducationisactive'=> array(0,1));
			$condition_department[] = array('Department.departmentisactive'=> array(0,1));
		else:
			$condition_user[] = array('User.company_id' => $company_id, 'User.branch_id' => $branch_id);
			$usereducation_id[] = array('Usereducation.usereducationisactive'=> array(1));
			$condition_department[] = array('Department.departmentisactive'=> array(1));
		endif;
        
       $department_list = $this->Department->find(
			'list',
			array(
				"fields" => array("id", "departmentname"),
				"conditions" => $condition_department
			)
		);
		$this->set('department_list', $department_list);

      $user_list = $this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions" => $condition_user
			)
		);
		$this->set('user_list', $user_list);

		$this->paginate = array(
			'fields' => 'Usereducation.*',
			//'order'  =>'Product.productname ASC',
			"conditions" => $usereducation_id,
			'limit' => 20
		);
		$usereducation = $this->paginate('Usereducation');
		$this->set('usereducation',$usereducation);
	}
	public function usereducationinsertupdate($id = null, $uuid=null){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Education').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];

		$condition_user = array();
		$condition_group = array();
		$condition_department = array();
		if($group_id==1):
			$condition_user[] = array('');
			$condition_group[] = array('');
		elseif($group_id==2):
			$condition_user[] = array('User.company_id' => $company_id);
			$condition_department[] = array('Department.company_id' => $company_id);
			$condition_user[] = array('NOT'=>array('User.group_id'=>1));
		else:
			$condition_user[] = array('User.company_id' => $company_id, 'User.branch_id' => $branch_id);
			$condition_department[] = array('Department.company_id' => $company_id);
			$condition_user[] = array('User.branch_id'=>$branch_id);
			$condition_user[] = array('User.group_id'=>$group_id);
		endif;
		
		$department_list = $this->Department->find(
			'list',
			array(
				"fields" => array("id", "departmentname"),
				"conditions" => $condition_department
			)
		);
		$this->set('department_list', $department_list);
		
		$user_list = $this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions" => $condition_user
			)
		);
		$this->set('user_list', $user_list);

		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0) {
			$usereducation = $this->Usereducation->find('all', array("fields" => "Usereducation.*", "conditions" => array('Usereducation.id' => $id, 'Usereducation.usereducationuuid'=>$uuid)));
			$this->set('usereducation', $usereducation);
		}
	}
	public function  usereducationinsertupdateaction($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		$company_id = $_SESSION["User"]["company_id"];
		$branch_id = $_SESSION["User"]["branch_id"];

		$id = @$this->request->data['Users']['id'];
		$uuid = @$this->request->data['Users']['usereducationuuid'];
		//pr($_POST);exit();
		if ($this->request->is('post')) {
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
				$this->request->data['Usereducation']['usereducationupdateid']=$userID;
				$this->request->data['Usereducation']['usereducationupdatedate']=date('Y-m-d H:i:s');
				$message = __('Update Successfully.');
				echo $this->Usereducation->id = $id;
			} else{
				$this->request->data['Usereducation']['usereducationinsertid']=$userID;
				$this->request->data['Usereducation']['usereducationinsertdate']=date('Y-m-d H:i:s');
				$this->request->data['Usereducation']['usereducationuuid']=String::uuid();
				//$this->Usereducation->create();
				$message = __('Save Successfully.');
			}
				$count = $this->request->data["totlarow"];
				for($i = 1; $i<=$count; $i++){
					$this->request->data["Usereducation"]["user_id"] = $this->request->data["Usereducation"]["user_id"];
					$this->request->data["Usereducation"]["usereducationdegreename"] = $this->request->data["Users"]["usereducationdegreename{$i}"];
					$this->request->data["Usereducation"]["usereducationgroup"] = $this->request->data["Users"]["usereducationgroup{$i}"];
					$this->request->data["Usereducation"]["usereducationinstitute"] = $this->request->data["Users"]["usereducationinstitute{$i}"];
					$this->request->data["Usereducation"]["usereducationpassing"] = $this->request->data["Users"]["usereducationpassing{$i}"];
					$this->request->data["Usereducation"]["usereducationboard"] = $this->request->data["Users"]["usereducationboard{$i}"];
					$this->request->data["Usereducation"]["usereducationduration"] = $this->request->data["Users"]["usereducationduration{$i}"];
					$this->request->data["Usereducation"]["usereducationresult"] = $this->request->data["Users"]["usereducationresult{$i}"];
					$this->request->data["Usereducation"]["usereducationachievement"] = $this->request->data["Users"]["usereducationachievement{$i}"];
					$this->request->data["Usereducation"]["usereducationisactive"] = $this->request->data["Users"]["usereducationisactive{$i}"];
					if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
					} else{
						$this->Usereducation->create();
					}
					$this->Usereducation->save($this->data);
				}
				//print_r($_POST); exit();
			if ($this->Usereducation->save($this->request->data, array('validate' => 'only'))) {
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				$this->redirect("/users/usereducationmanage");
			}else{
				$errors = $this->Usereducation->invalidFields();
				$this->set('errors', $this->Usereducation->invalidFields());
				//$this->Session->setFlash('The post was not saved... Please try again!', 'default', array('class' => 'alert alert-warning'));
				$this->redirect("/users/usereducationinsertupdate");
			}
		}
	}
    public function usereducationlistbydepartmentid($companyID=null){

		$this->layout = 'ajax';
		$UsereducationDepartmentId = @$this->request->data['UsereducationDepartmentId'];
//		$DepartmentId = @$this->request->data['DepartmentId'];
		$this->set('UsereducationDepartmentId',$UsereducationDepartmentId);
		$user_list=$this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions"=>array('User.department_id'=>$UsereducationDepartmentId, 'User.userisactive'=>1))
		);
		$this->set('user_list',$user_list);
	}
	public function usereducationdetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$usereducation=$this->Usereducation->find('all',array("fields" => "Usereducation.*","conditions"=>array('Usereducation.id'=>$id,'Usereducation.usereducationuuid'=>$uuid)));
		$this->set('usereducation',$usereducation);
	}
	public function usereducationdelete($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		if ($this->Usereducation->updateAll(array('usereducationdeleteid'=>$userID, 'usereducationdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'usereducationisactive'=>2), array('AND' => array('Usereducation.id'=>$id,'Usereducation.usereducationuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/users/usereducationmanage");
	}
	public function usereducationsearchbytext($Searchtext=null){
		$this->layout='ajax';
		$Searchtext = @$this->request->data['Searchtext'];
		$UsereducationUserId = @$this->request->data['UsereducationUserId'];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$userId=$_SESSION["User"]["id"];


        $usereducation_id = array();
        $condition_user = array();
       	$condition_department = array();
		if($group_id==1):
			$condition_user[] = array('');
			$usereducation_id[] = array('');
			$condition_department[] = array('');
		elseif($group_id==2):
			$condition_user[] = array('User.company_id' => $company_id);
			$usereducation_id[] = array('Usereducation.usereducationisactive'=> array(0,1));
			$condition_department[] = array('Department.departmentisactive'=> array(0,1));
		else:
			$condition_user[] = array('User.company_id' => $company_id, 'User.branch_id' => $branch_id);
			$usereducation_id[] = array('Usereducation.usereducationisactive'=> array(1));
			$condition_department[] = array('Department.departmentisactive'=> array(1));
		endif;
        
       $department_list = $this->Department->find(
			'list',
			array(
				"fields" => array("id", "departmentname"),
				"conditions" => $condition_department
			)
		);
		$this->set('department_list', $department_list);

      $user_list = $this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions" => $condition_user
			)
		);
		$this->set('user_list', $user_list);



		$usereducation_id = array();
        if(!empty($UsereducationUserId) && $UsereducationUserId!=''):
			$usereducation_id[] = array('Usereducation.user_id'=>$UsereducationUserId);
		endif;
        if(!empty($Searchtext) && $Searchtext!=''):
            $usereducation_id[] = array(
                'OR' => array(
                    'Usereducation.usereducationdegreename LIKE ' => '%'.$Searchtext.'%',
                    'Usereducation.usereducationgroup LIKE ' => '%'.$Searchtext.'%',
                    'Usereducation.usereducationinstitute LIKE ' => '%'.$Searchtext.'%',
                    'Usereducation.usereducationpassing LIKE ' => '%'.$Searchtext.'%',
                    'Usereducation.usereducationboard LIKE ' => '%'.$Searchtext.'%',
                    'Usereducation.usereducationresult LIKE ' => '%'.$Searchtext.'%',
                    'Usereducation.usereducationduration LIKE ' => '%'.$Searchtext.'%',
                    'Usereducation.usereducationachievement LIKE ' => '%'.$Searchtext.'%'
                )
            );
        endif;

		$this->paginate = array(
			'fields' => array('Usereducation.*'), 
			'conditions' => $usereducation_id,
			'order'  =>'Usereducation.id ASC',
			//'condition' =
			'limit' => 20
		);
		$usereducation = $this->paginate('Usereducation');
		$this->set('usereducation',$usereducation);
    }
	// End Usereducation //
    public function userlistbydepartmentid($companyID=null){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$UsereducationDepartmentId = @$this->request->data['UsereducationDepartmentId'];
//		$DepartmentId = @$this->request->data['DepartmentId'];
		$this->set('UsereducationDepartmentId',$UsereducationDepartmentId);
		$user_list=$this->User->find(
			'all',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions"=>array('User.department_id'=>$UsereducationDepartmentId, 'User.userisactive'=>1))
		);
		$this->set('user_list',$user_list);
			echo "<option value=\"\">".__("Employee Name")."</option>";
		foreach($user_list as $thisdistriclist) {
			echo "<option value=\"".$thisdistriclist['User']['id']."\">".$thisdistriclist['User']['user_fullname']."</option>";
		}
	}


	// Start Usercareerhistory //
	public function usercareerhistorymanage(){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Manage Career History').' | '.__(Configure::read('site_name')));
		$userID = $this->Session->read('User.id');
		$group_id=$_SESSION["User"]["group_id"];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
                
        $usercareerhistory_id = array();
        $condition_user = array();
        $groupId = array();
		$companyId = array();
		$branchId = array();
		$userId = array();
		if($group_id==1):
			$condition_user[] = array('');
			$usercareerhistory_id[] = array('');
		elseif($group_id==2):
			$condition_user[] = array('User.company_id' => $company_id);
			$usercareerhistory_id[] = array('Usercareerhistory.usercareerhistoryisactive'=> array(0,1));
		else:
			$condition_user[] = array('User.company_id' => $company_id, 'User.branch_id' => $branch_id);
			$usercareerhistory_id[] = array('Usercareerhistory.usercareerhistoryisactive'=> array(0,1));
		endif;
        
       $user_list = $this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions" => $condition_user
			)
		);
		$this->set('user_list', $user_list);
		$this->paginate = array(
			'fields' => 'Usercareerhistory.*',
			//'order'  =>'Product.productname ASC',
			"conditions" => $usercareerhistory_id,
			'limit' => 20
		);
		$usercareerhistory = $this->paginate('Usercareerhistory');
		$this->set('usercareerhistory',$usercareerhistory);
	}
	public function usercareerhistoryinsertupdate($id = null, $uuid=null){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Career History').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];

//        $usereducation_id = array();
        $condition_user = array();
       	$condition_department = array();
		if($group_id==1):
			$condition_user[] = array('');
//			$usereducation_id[] = array('');
			$condition_user[] = array('');
			$condition_department[] = array('');
		elseif($group_id==2):
			$condition_user[] = array('User.company_id' => $company_id);
			$usereducation_id[] = array('Usereducation.usereducationisactive'=> array(0,1));
			$condition_department[] = array('Department.departmentisactive'=> array(0,1));
		else:
			$condition_user[] = array('User.company_id' => $company_id, 'User.branch_id' => $branch_id);
			$usereducation_id[] = array('Usereducation.usereducationisactive'=> array(1));
			$condition_department[] = array('Department.departmentisactive'=> array(1));
		endif;
		
		$department_list = $this->Department->find(
			'list',
			array(
				"fields" => array("id", "departmentname"),
				"conditions" => $condition_department
			)
		);
		$this->set('department_list', $department_list);

		$user_list = $this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions" => $condition_user
			)
		);
		$this->set('user_list', $user_list);

		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0) {
			$usercareerhistory = $this->Usercareerhistory->find('all', array("fields" => "Usercareerhistory.*", "conditions" => array('Usercareerhistory.id' => $id, 'Usercareerhistory.usercareerhistoryuuid'=>$uuid)));
			$this->set('usercareerhistory', $usercareerhistory);
		}
	}
	public function  usercareerhistoryinsertupdateaction($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		$company_id = $_SESSION["User"]["company_id"];
		$branch_id = $_SESSION["User"]["branch_id"];

		$id = @$this->request->data['Users']['id'];
		$uuid = @$this->request->data['Users']['usercareerhistoryuuid'];
		if ($this->request->is('post')) {
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
				$this->request->data['Usercareerhistory']['usercareerhistoryupdateid']=$userID;
				$this->request->data['Usercareerhistory']['usercareerhistoryupdatedate']=date('Y-m-d H:i:s');
				$message = __('Update Successfully.');
				$this->Usercareerhistory->id = $id;
			} else{
				$this->request->data['Usercareerhistory']['usercareerhistoryinsertid']=$userID;
				$this->request->data['Usercareerhistory']['usercareerhistoryinsertdate']=date('Y-m-d H:i:s');
				$this->request->data['Usercareerhistory']['usercareerhistoryuuid']=String::uuid();
				$message = __('Save Successfully.');
			}
			$count = $this->request->data["totlarow"];
			for($i = 1; $i<=$count; $i++){
				$this->request->data["Usercareerhistory"]["user_id"] = $this->request->data["Usercareerhistory"]["user_id"];
				$this->request->data["Usercareerhistory"]["usercareerhistorycompamyname"] = $this->request->data["Users"]["usercareerhistorycompamyname{$i}"];
				$this->request->data["Usercareerhistory"]["usercareerhistorycompanytype"] = $this->request->data["Users"]["usercareerhistorycompanytype{$i}"];
				$this->request->data["Usercareerhistory"]["usercareerhistorylocation"] = $this->request->data["Users"]["usercareerhistorylocation{$i}"];
				$this->request->data["Usercareerhistory"]["usercareerhistorypositionheld"] = $this->request->data["Users"]["usercareerhistorypositionheld{$i}"];
				$this->request->data["Usercareerhistory"]["usercareerhistorydepartment"] = $this->request->data["Users"]["usercareerhistorydepartment{$i}"];
				$this->request->data["Usercareerhistory"]["usercareerhistoryresponsibilities"] = $this->request->data["Users"]["usercareerhistoryresponsibilities{$i}"];
				$this->request->data["Usercareerhistory"]["usercareerhistoryfromdate"] = $this->request->data["Users"]["usercareerhistoryfromdate{$i}"];
				$this->request->data["Usercareerhistory"]["usercareerhistorytodate"] = $this->request->data["Users"]["usercareerhistorytodate{$i}"];
				$this->request->data["Usercareerhistory"]["usercareerhistoryisactive"] = $this->request->data["Users"]["usercareerhistoryisactive{$i}"];
				if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
				} else{
					$this->Usercareerhistory->create();
				}
				$this->Usercareerhistory->save($this->data);	
			}
			if ($this->Usercareerhistory->save($this->request->data, array('validate' => 'only'))) {
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				$this->redirect("/users/usercareerhistorymanage");
			}else{
				$errors = $this->Usercareerhistory->invalidFields();
				$this->set('errors', $this->Usercareerhistory->invalidFields());
				//$this->Session->setFlash('The post was not saved... Please try again!', 'default', array('class' => 'alert alert-warning'));
				$this->redirect("/users/usercareerhistoryinsertupdate");
			}
		}
	}
    public function usercareerlistbydepartmentid($companyID=null){
		$this->layout = 'ajax';
		$UsercareerhistoryDepartmentId = @$this->request->data['UsercareerhistoryDepartmentId'];
//		$DepartmentId = @$this->request->data['DepartmentId'];
		$this->set('UsercareerhistoryDepartmentId',$UsercareerhistoryDepartmentId);
		$user_list=$this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions"=>array('User.department_id'=>$UsercareerhistoryDepartmentId, 'User.userisactive'=>1))
		);
		$this->set('user_list',$user_list);
	}
	public function usercareerhistorydetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$usercareerhistory=$this->Usercareerhistory->find('all',array("fields" => "Usercareerhistory.*","conditions"=>array('Usercareerhistory.id'=>$id,'Usercareerhistory.usercareerhistoryuuid'=>$uuid)));
		$this->set('usercareerhistory',$usercareerhistory);
	}
	public function usercareerhistorydelete($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		if ($this->Usercareerhistory->updateAll(array('usercareerhistorydeleteid'=>$userID, 'usercareerhistorydeletedate'=>"'".date('Y-m-d H:i:s')."'", 'usercareerhistoryisactive'=>2), array('AND' => array('Usercareerhistory.id'=>$id,'Usercareerhistory.usercareerhistoryuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/users/usercareerhistorymanage");
	}
	public function usercareerhistorysearchbytext($Searchtext=null){
		$this->layout='ajax';
		$Searchtext = @$this->request->data['Searchtext'];
		$UsercareerhistoryUserId = @$this->request->data['UsercareerhistoryUserId'];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$userId=$_SESSION["User"]["id"];

		$usercareerhistory_id = array();
        if(!empty($UsercareerhistoryUserId) && $UsercareerhistoryUserId!=''):
			$usercareerhistory_id[] = array('Usercareerhistory.user_id'=>$UsercareerhistoryUserId);
		endif;
        if(!empty($Searchtext) && $Searchtext!=''):
            $usercareerhistory_id[] = array(
                'OR' => array(
                    'Usercareerhistory.usercareerhistorycompamyname LIKE ' => '%'.$Searchtext.'%',
                    'Usercareerhistory.usercareerhistorycompanytype LIKE ' => '%'.$Searchtext.'%',
                    'Usercareerhistory.usercareerhistorylocation LIKE ' => '%'.$Searchtext.'%',
                    'Usercareerhistory.usercareerhistorypositionheld LIKE ' => '%'.$Searchtext.'%',
                    'Usercareerhistory.usercareerhistorydepartment LIKE ' => '%'.$Searchtext.'%',
                    'Usercareerhistory.usercareerhistoryresponsibilities LIKE ' => '%'.$Searchtext.'%',
                    'Usercareerhistory.usercareerhistoryfromdate LIKE ' => '%'.$Searchtext.'%',
                    'Usercareerhistory.usercareerhistorytodate LIKE ' => '%'.$Searchtext.'%'
                )
            );
        endif;

		$this->paginate = array(
			'fields' => array('Usercareerhistory.*'), 
			'conditions' => $usercareerhistory_id,
			'order'  =>'Usercareerhistory.id ASC',
			//'condition' =
			'limit' => 20
		);
		$usercareerhistory = $this->paginate('Usercareerhistory');
		$this->set('usercareerhistory',$usercareerhistory);
    }
	// End Usercareerhistory //

	// Start Userachievement //
    public function userachievementmanage(){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Manage Special Achievement').' | '.__(Configure::read('site_name')));
		$userID = $this->Session->read('User.id');
		$group_id=$_SESSION["User"]["group_id"];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
                
        $userachievement_id = array();
        $condition_user = array();
        $groupId = array();
		$companyId = array();
		$branchId = array();
		$userId = array();
		if($group_id==1):
			$userachievement_id[] = array('');
			$condition_group[] = array('');
		elseif($group_id==2):
			$condition_user[] = array('User.company_id' => $company_id);
			$userachievement_id[] = array('Userachievement.userachievementisactive'=> array(0,1));
		else:
			$condition_user[] = array('User.company_id' => $company_id, 'User.branch_id' => $branch_id);
			$userachievement_id[] = array('Userachievement.userachievementisactive'=> array(0,1));
		endif;
        
       $user_list = $this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions" => $condition_user
			)
		);
		$this->set('user_list', $user_list);
		$this->paginate = array(
			'fields' => 'Userachievement.*',
			//'order'  =>'Product.productname ASC',
			"conditions" => $userachievement_id,
			'limit' => 20
		);
		$userachievement = $this->paginate('Userachievement');
		$this->set('userachievement',$userachievement);
	}
	public function userachievementinsertupdate($id = null, $uuid=null){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Special Achievement').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];


		$condition_user[] = array('');
		$condition_department[] = array('');
		$condition_group = array('');
		if($group_id==1):
		elseif($group_id==2):
			$condition_user[] = array('User.company_id' => $company_id);
			$usereducation_id[] = array('Usereducation.usereducationisactive'=> array(0,1));
			$condition_department[] = array('Department.departmentisactive'=> array(0,1));
		else:
			$condition_user[] = array('User.company_id' => $company_id, 'User.branch_id' => $branch_id);
			$usereducation_id[] = array('Usereducation.usereducationisactive'=> array(1));
			$condition_department[] = array('Department.departmentisactive'=> array(1));
		endif;

		$department_list = $this->Department->find(
			'list',
			array(
				"fields" => array("id", "departmentname"),
				"conditions" => $condition_department
			)
		);
		$this->set('department_list', $department_list);
		
		$user_list = $this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions" => $condition_user
			)
		);
		$this->set('user_list', $user_list);

		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0) {
			$userachievement = $this->Userachievement->find('all', array("fields" => "Userachievement.*", "conditions" => array('Userachievement.id' => $id, 'Userachievement.userachievementuuid'=>$uuid)));
			$this->set('userachievement', $userachievement);
		}
	}
	public function  userachievementinsertupdateaction($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		$company_id = $_SESSION["User"]["company_id"];
		$branch_id = $_SESSION["User"]["branch_id"];

		$id = @$this->request->data['Users']['id'];
		$uuid = @$this->request->data['Users']['userachievementuuid'];
		//pr($_POST);exit();
		if ($this->request->is('post')) {
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
				$this->request->data['Userachievement']['userachievementupdateid']=$userID;
				$this->request->data['Userachievement']['userachievementupdatedate']=date('Y-m-d H:i:s');
				$message = __('Update Successfully.');
				echo $this->Userachievement->id = $id;
			} else{
				$this->request->data['Userachievement']['userachievementinsertid']=$userID;
				$this->request->data['Userachievement']['userachievementinsertdate']=date('Y-m-d H:i:s');
				$this->request->data['Userachievement']['userachievementuuid']=String::uuid();
				//$this->Userachievement->create();
				$message = __('Save Successfully.');
			}
			if ($this->Userachievement->save($this->request->data, array('validate' => 'only'))) {
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				$this->redirect("/users/userachievementmanage");
			}else{
				$errors = $this->Userachievement->invalidFields();
				$this->set('errors', $this->Userachievement->invalidFields());
				$this->redirect("/users/userachievementinsertupdate");
			}
		}
	}
    public function userachievementlistbydepartmentid($companyID=null){
		$this->layout = 'ajax';
		$UserachievementDepartmentId = @$this->request->data['UserachievementDepartmentId'];
//		$DepartmentId = @$this->request->data['DepartmentId'];
		$this->set('UserachievementDepartmentId',$UserachievementDepartmentId);
		$user_list=$this->User->find(
			'list',
			array(
				"fields" => array("id", "user_fullname"),
				"conditions"=>array('User.department_id'=>$UserachievementDepartmentId, 'User.userisactive'=>1))
		);
		$this->set('user_list',$user_list);
	}
	public function userachievementdetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$userachievement=$this->Userachievement->find('all',array("fields" => "Userachievement.*","conditions"=>array('Userachievement.id'=>$id,'Userachievement.userachievementuuid'=>$uuid)));
		$this->set('userachievement',$userachievement);
	}
	public function userachievementdelete($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		if ($this->Userachievement->updateAll(array('userachievementdeleteid'=>$userID, 'userachievementdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'userachievementisactive'=>2), array('AND' => array('Userachievement.id'=>$id,'Userachievement.userachievementuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/users/userachievementmanage");
	}
	public function userachievementsearchbytext($Searchtext=null){
		$this->layout='ajax';
		$Searchtext = @$this->request->data['Searchtext'];
		$UserachievementUserId = @$this->request->data['UserachievementUserId'];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$userId=$_SESSION["User"]["id"];

		$userachievement_id = array();
		/*if($group_id==1):
			$user_id[] = array('');
		elseif($group_id==2):
			$user_id[] = array('User.company_id'=>$company_id);
		else:
			$user_id[] = array('User.branch_id'=>$branch_id, 'User.company_id'=>$company_id, 'NOT'=>array('User.group_id'=>array(1, 2)));
		endif;
        */
        if(!empty($UserachievementUserId) && $UserachievementUserId!=''):
			$userachievement_id[] = array('Userachievement.user_id'=>$UserachievementUserId);
		endif;
        if(!empty($Searchtext) && $Searchtext!=''):
            $userachievement_id[] = array(
                'OR' => array(
                    'Userachievement.userachievementname LIKE ' => '%'.$Searchtext.'%',
                    'Userachievement.userachievementstatus LIKE ' => '%'.$Searchtext.'%',
                    'Userachievement.userachievementamount LIKE ' => '%'.$Searchtext.'%',
                    'Userachievement.userachievementdescription LIKE ' => '%'.$Searchtext.'%',
                    'Userachievement.userachievementdate LIKE ' => '%'.$Searchtext.'%'
                )
            );
        endif;

		$this->paginate = array(
			'fields' => array('Userachievement.*'), 
			'conditions' => $userachievement_id,
			'order'  =>'Userachievement.id ASC',
			//'condition' =
			'limit' => 20
		);
		$userachievement = $this->paginate('Userachievement');
		$this->set('userachievement',$userachievement);
    }
	// End Userachievement//

    public function userlifecyclemanage(){

		$this->layout = 'default';
		$this->set('title_for_layout', __('Employee Lifecycle').' | '.__(Configure::read('site_name')));
		$userID = $this->Session->read('User.id');
		$group_id=$_SESSION["User"]["group_id"];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];

		$user_id = array();
		$groupId = array();
		$companyId = array();
		$branchId = array();
		$departmentID = array();

		if($group_id==1):
		elseif($group_id==2):
			$user_id = array('User.company_id'=>$company_id);
			$departmentID[] = array('Department.company_id'=>$company_id);
		else:
			$user_id[] = array('User.company_id'=>$company_id);
			$user_id[] = array('User.branch_id'=>$company_id);
			$departmentID[] = array('Department.company_id'=>$company_id);
			$departmentID[] = array('Department.departmentisactive'=>1);
		endif;

      
		$departmentname = $this->Department->find('list',array(
			"fields" => array("Department.id","Department.departmentname"),
			'conditions' => array('Department.departmentisactive' => 1, $departmentID)
			));
		$this->set('departmentname', $departmentname);
 
        
 		$this->paginate = array(
			'fields' => 'User.*',
			'order'  =>'User.username ASC',
			"conditions" => $user_id,
			'limit' => 20
		);
		$userlifecycle = $this->paginate('User');
		$this->set('userlifecycle',$userlifecycle);
	}

	// End Userlifecycle//
	public function userlifecyclesearchbytext($Searchtext=null){
		$this->layout='ajax';
		$Searchtext = @$this->request->data['Searchtext'];
		$UserDepartmentId = @$this->request->data['UserDepartmentId'];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$userId=$_SESSION["User"]["id"];

		$department_id = array();
		if($group_id==1):
		elseif($group_id==2):
			$department_id[] = array('User.company_id'=>$company_id, 'NOT'=>array('User.group_id'=>1));
		else:
			$department_id[] = array('User.company_id'=>$company_id, 'NOT'=>array('User.group_id'=>array(1, 2)));
		endif;

        if(!empty($UserDepartmentId) && $UserDepartmentId!=''):
			$department_id[] = array('User.department_id'=>$UserDepartmentId);
		endif;
        if(!empty($Searchtext) && $Searchtext!=''):
            $department_id[] = array(
                'OR' => array(
                    'User.userfirstname LIKE ' => '%'.$Searchtext.'%',
                    'User.userlastname LIKE ' => '%'.$Searchtext.'%'
                )
            );
        endif;

		$this->paginate = array(
			'fields' => array('User.*'), 
			'conditions' => $department_id,
			'order'  =>'User.userpunchmachineid ASC',
			'limit' => 20
		);
		$userlifecycle = $this->paginate('User');
		$this->set('userlifecycle',$userlifecycle);
    }
	// End Userachievement//


	public function userlifecycledetailsbyid($id=null,$uuid=null){
//echo $id;
		$this->layout = 'iframe';
		$this->set('title_for_layout', __('Employee Lifecycle').' | '.__(Configure::read('site_name')));

 		$this->paginate = array(
			'fields' => 'User.*',
			"conditions"=>array('User.id'=>$id,'User.useruuid'=>$uuid),
			'limit' => 20
		);
		$userlifecycle = $this->paginate('User');
		$this->set('userlifecycle',$userlifecycle);

	}
//--------------------------Start Category (For Teacher)-------//
	public function categorymanage(){
	    $this->layout='default';
	    $this->set("title_for_layout", __("Category")." | ".__(Configure::read("site_name")));
	    $company_id=$_SESSION["User"]["company_id"];
	    $branch_id=$_SESSION["User"]["branch_id"];
	    $group_id=$_SESSION["User"]["group_id"];
	    $user_id=$_SESSION["User"]["id"];

	    $this->paginate = array(
	        'fields' => 'Category.*',
	        'order'  =>'Category.categoryname ASC',
	        'limit' => 20
	    );
	    $category = $this->paginate('Category');
	    $this->set('category',$category);
	}
	public function categoryinsertupdate($id=null,$uuid=null){
	    $this->layout='default';
	    $this->set('title_for_layout', __('Insert/Edit Category').' | '.__(Configure::read('site_name')));
	    if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
	        $category=$this->Category->find('all',array("fields" => "Category.*","conditions"=>array('Category.id'=>$id,'Category.categoryuuid'=>$uuid)));
	        $this->set('category',$category);
	    endif;
	}
	public function  categoryinsertupdateaction($id=null,$uuid=null){
	    $userID = $this->Session->read('User.id');
	    $company_id = $_SESSION["User"]["company_id"];
	    $branch_id = $_SESSION["User"]["branch_id"];

	    $id = @$this->request->data['Users']['id'];
	    $uuid = @$this->request->data['Users']['categoryuuid'];
	    $category_name = $this->request->data['Category']['categoryname'];
        if(!empty($id) && !empty($uuid)){
          $count_category_name = 0;
        }else{
          $count_category_name=$this->Category->find('all',array("fields" => "Category.*","conditions"=>array('Category.categoryname'=>$category_name)));
          $count_category_name = count( $count_category_name );
        }
        if($count_category_name > 0){
          $this->Session->setFlash(__("This Category Name Already Exists"), 'default', array('class' => 'alert alert-warning'));
          $this->redirect("/users/categoryinsertupdate");  
        } 	else{
	        	if ($this->request->is('post')) {
			       if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
			        	$this->request->data['Category']['categoryupdateid']=$userID;
			        	$this->request->data['Category']['categoryupdatedate']=date('Y-m-d H:i:s');
			        	$message = __('Update Successfully.');
			        	$this->Category->id = $id;
			        else:
			          	$this->request->data['Category']['categoryinsertid']=$userID;
			          	$this->request->data['Category']['categoryinsertdate']=date('Y-m-d H:i:s');
			          	$this->request->data['Category']['categoryuuid']=String::uuid();
			          	$message = __('Save Successfully.');
			        endif;
			        if ($this->Category->save($this->request->data, array('validate' => 'only'))) {
			          	$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
			          	$this->redirect("/users/categorymanage");
			        }else{
			          	$errors = $this->Category->invalidFields();
			          	$this->set('errors', $this->Category->invalidFields());
			          	$this->redirect("/users/categoryinsertupdate");
			        }
				}	
        }
	}
	public function categorydetailsbyid($id=null,$uuid=null){
	    $this->layout='ajax';
	    $category=$this->Category->find('all',array("fields" => "Category.*","conditions"=>array('Category.id'=>$id,'Category.categoryuuid'=>$uuid)));
	    $this->set('category',$category);
	}
	public function categorydelete($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		if ($this->Category->updateAll(array('categorydeleteid'=>$userID, 'categorydeletedate'=>"'".date('Y-m-d H:i:s')."'", 'categoryisactive'=>2), array('AND' => array('Category.id'=>$id,'Category.categoryuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/users/categorymanage");
	}
	public function categorysearchbytext($Searchtext=null){
		$this->layout='ajax';

		$Searchtext = @$this->request->data['Searchtext'];
		$category_id = array();
		if(!empty($Searchtext) && $Searchtext!=''):
			$category_id[] = array(
				'OR' => array(
					'Category.categoryname LIKE ' => '%'.$Searchtext.'%'
				)
			);
		else:
			$category_id[] = array();
		endif;

		$this->paginate = array(
			'fields' => 'Category.*',
			'order'  =>'Category.categoryname ASC',
			"conditions"=>$category_id,
			'limit' => 20
		);
		$category = $this->paginate('Category');
		$this->set('category',$category);
	}
//--------------------------End Category (For Teacher)-------//	
//--------------------------Start Subject (For Teacher)-------//
	public function subjectmanage(){
	    $this->layout='default';
	    $this->set("title_for_layout", __("Subject")." | ".__(Configure::read("site_name")));
	    $company_id=$_SESSION["User"]["company_id"];
	    $branch_id=$_SESSION["User"]["branch_id"];
	    $group_id=$_SESSION["User"]["group_id"];
	    $user_id=$_SESSION["User"]["id"];

	    $this->paginate = array(
	        'fields' => 'Subject.*',
	        'order'  =>'Subject.subjectname ASC',
	        'limit' => 20
	    );
	    $subject = $this->paginate('Subject');
	    $this->set('subject',$subject);
	}
	public function subjectinsertupdate($id=null,$uuid=null){
	    $this->layout='default';
	    $this->set('title_for_layout', __('Insert/Edit Subject').' | '.__(Configure::read('site_name')));
	    if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
	        $subject=$this->Subject->find('all',array("fields" => "Subject.*","conditions"=>array('Subject.id'=>$id,'Subject.subjectuuid'=>$uuid)));
	        $this->set('subject', $subject);
	    endif;
	}
	public function subjectinsertupdateaction($id=null,$uuid=null){
	    $userID = $this->Session->read('User.id');
	    $company_id = $_SESSION["User"]["company_id"];
	    $branch_id = $_SESSION["User"]["branch_id"];

	    $id = @$this->request->data['Users']['id'];
	    $uuid = @$this->request->data['Users']['subjectuuid'];
	    $subject_name = $this->request->data['Subject']['subjectname'];
        $subject_namebn = $this->request->data['Subject']['subjectnamebn'];
        if(!empty($id) && !empty($uuid)){
          $count_subject_name = 0;
        }else{
          $count_subject_name=$this->Subject->find('all',array("fields" => "Subject.*","conditions"=>array('Subject.subjectname'=>$subject_name,'Subject.subjectnamebn'=>$subject_namebn)));
          $count_subject_name = count( $count_subject_name );
        }
         if($count_subject_name > 0){
          $this->Session->setFlash(__("This Subject Name Already Exists"), 'default', array('class' => 'alert alert-warning'));
          $this->redirect("/users/subjectinsertupdate");  
        }else{
    		if ($this->request->is('post')) {
		       if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
		          $this->request->data['Subject']['subjectupdateid']=$userID;
		          $this->request->data['Subject']['subjectupdatedate']=date('Y-m-d H:i:s');
		          $message = __('Update Successfully.');
		          $this->Subject->id = $id;
		        else:
		          $this->request->data['Subject']['subjectinsertid']=$userID;
		          $this->request->data['Subject']['subjectinsertdate']=date('Y-m-d H:i:s');
		          $this->request->data['Subject']['subjectuuid']=String::uuid();
		          $message = __('Save Successfully.');
		        endif;
		        if ($this->Subject->save($this->request->data, array('validate' => 'only'))) {
		          $this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
		          $this->redirect("/users/subjectmanage");
		        }else{
		          $errors = $this->Subject->invalidFields();
		          $this->set('errors', $this->Subject->invalidFields());
		          //$this->Session->setFlash('The post was not saved... Please try again!', 'default', array('class' => 'alert alert-warning'));
		          $this->redirect("/users/subjectinsertupdate");
		        }
		    }    	
        }
	    
	}
	public function subjectdetailsbyid($id=null,$uuid=null){
	    $this->layout='ajax';
	    $subject=$this->Subject->find('all',array("fields" => "Subject.*","conditions"=>array('Subject.id'=>$id,'Subject.subjectuuid'=>$uuid)));
	    $this->set('subject', $subject);
	}
	public function subjectdelete($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		if ($this->Subject->updateAll(array('subjectdeleteid'=>$userID, 'subjectdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'subjectisactive'=>2), array('AND' => array('Subject.id'=>$id,'Subject.subjectuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/users/subjectmanage");
	}
	public function subjectsearchbytext($Searchtext=null){
		$this->layout='ajax';

		$Searchtext = @$this->request->data['Searchtext'];
		$subject_id = array();
		if(!empty($Searchtext) && $Searchtext!=''):
			$subject_id[] = array(
				'OR' => array(
					'Subject.subjectname LIKE ' => '%'.$Searchtext.'%',
					'Subject.subjectnamebn LIKE ' => '%'.$Searchtext.'%'
				)
			);
		else:
			$subject_id[] = array();
		endif;

		$this->paginate = array(
			'fields' => 'Subject.*',
			'order'  =>'Subject.subjectname ASC',
			"conditions"=>$subject_id,
			'limit' => 20
		);
		$subject = $this->paginate('Subject');
		$this->set('subject',$subject);
	}
//--------------------------End Subject (For Teacher)-------//	

}
