<?php
App::uses('AppModel', 'Model');
class Banktransaction extends AppModel {
	public $name = 'Banktransaction';
	public $usetables = 'banktransactions';
	var $belongsTo  = array(
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'banktransactioninsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'banktransactionupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'banktransactiondeleteid'
		),
		'User' => array(
			'fields' =>array('User.*'),
			'className'    => 'User',
			'foreignKey'    => 'user_id'
		),
		/*'Usergroup' => array(
			'fields' =>array('Usergroup.*'),
			'className'    => 'Usergroup',
			'foreignKey'    => false,
			'conditions'	=> 'Banktransaction.user_id=Usergroup.user_id'
		),*/
		'Client' => array(
			'fields' =>array('User.*'),
			'className'    => 'User',
			'foreignKey'    => 'client_id'
		),
		'Salesman' => array(
			'fields' =>array('User.*'),
			'className'    => 'User',
			'foreignKey'    => 'salesman_id'
		),
		'Coa' => array(
			'fields' =>array('Coa.*'),
			'className'    => 'Coa',
			'foreignKey'    => 'coa_id'
		),
		'Bank' => array(
			'fields' =>array('Bank.*'),
			'className'    => 'Bank',
			'foreignKey'    => 'bank_id'
		),
		'Bankbranch' => array(
			'fields' =>array('Bankaccounttype.*'),
			'className'    => 'Bankbranch',
			'foreignKey'    => 'bankbranch_id'
		),
		'Bankaccounttype' => array(
			'fields' =>array('Bankaccounttype.*'),
			'className'    => 'Bankaccounttype',
			'foreignKey'    => 'bankaccounttype_id'
		),
		'Bankaccount' => array(
			'fields' =>array('Bankaccount.*'),
			'className'    => 'Bankaccount',
			'foreignKey'    => 'bankaccount_id'
		),
		'Bankchequebook' => array(
			'fields' =>array('Bankchequebook.*'),
			'className'    => 'Bankchequebook',
			'foreignKey'    => 'bankchequebook_id'
		)
	);
	var $virtualFields = array(
		'banktransaction_particulars' => 'CONCAT(
			IF(Banktransaction.transactiontype_id=0, "", IF(Banktransaction.transactiontype_id=3,"Withdraw","Deposits")), 
			IF(Banktransaction.client_id=0, "", CONCAT("/", Client.userfirstname, " ", Client.usermiddlename, " ", Client.userlastname)), 
			IF(Banktransaction.user_id=0, CONCAT("/", Banktransaction.banktransactionothernote), CONCAT("/", User.userfirstname, " ", User.usermiddlename, " ", User.userlastname)), 
			IF(Banktransaction.coa_id=0, " ", CONCAT("/", Coa.coaname)), 
			IF(Banktransaction.bankaccount_id=0, " ", CONCAT("/", Bankaccount.bankaccountnumber)), 
			IF(Banktransaction.salesman_id=0, "", CONCAT("/", Salesman.userfirstname, " ", Salesman.usermiddlename, " ", Salesman.userlastname))
		)',
		'banktransaction_balance' => 'IF(Banktransaction.transactionamount=0, "0.00", ROUND(Banktransaction.transactionamount, 2))',
		'banktransaction_withdraw' => 'IF(Banktransaction.transactionamount<0,ROUND(Banktransaction.transactionamount, 2), "0.00")',
		'banktransaction_deposit' => 'IF(Banktransaction.transactionamount>0,ROUND(Banktransaction.transactionamount, 2), "0.00")',
		'banktransaction_chequenumber' => 'CONCAT( Banktransaction.banktransactionchequebookseries, " - ", Banktransaction.banktransactionchequesequencynumber )',
		'isActive' => 'IF(Banktransaction.banktransactionisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Banktransaction.banktransactionisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
	public $validate = array(
		'banktransactiondate' => array(
			'rule' => 'notEmpty',
			'message' => 'This Date field is required',
			'last' => true
		),
		'group_id' => array(
			'group_id_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This Group field is required',
				'last' => true
			)
		),
		'user_id' => array(
			'user_id_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This Bank Cheque Book Sequency End Number field is required',
				'last' => true
			)
		),
		'banktransactiontype_id' => array(
			'rule' => 'notEmpty',
			'message' => 'This Transaction Type field is required',
			'last' => true
		),
		'bank_id' => array(
			'rule' => 'notEmpty',
			'message' => 'This Bank Name field is required',
			'last' => true
		),		
		'bankaccount_id' => array(
			'rule' => 'notEmpty',
			'message' => 'This Bank Account field is required',
			'last' => true
		),		
		'banktransactionamount' => array(
			'rule' => 'notEmpty',
			'message' => 'This Amount field is required',
			'last' => true
		)
	);
}

?>