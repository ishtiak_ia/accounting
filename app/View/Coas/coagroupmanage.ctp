<?php
	echo "<h2>".__('Chart of Account Group'). $Utilitys->addurl($title=__("Add New Chart of Account Group"), $this->request["controller"], $page="coagroup")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Coagroup.coatype_id",
			array(
				'type'=>'select',
				"options"=>array($coatype),
				'empty'=>__('Select Chart of Account Type'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'style'=>'',
				'class'=> 'form-control'
			)
		)
		.$this->Form->input(
			"Coagroup.coagroup_id",
			array(
				'type'=>'select',
				"options"=>array($coagrouplist),
				'empty'=>__('Select Chart of Account Group'),
				'div'=>false,
				'tabindex'=>2,
				'label'=>false,
				'style'=>'',
				'class'=> 'form-control'
			)
		)
		."<span id=\"grouploading\"></span>"
		.$this->Form->input(
			"Coagroup.coagroupname",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'tabindex'=>3,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control'
			)
		);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"groupsarchloading\">
	";
			echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id=null);
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('CODE')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA Type')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA Group')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA Group BN')  => array('class' => 'highlight sortable')
						), 
						__('Active'), 
						__('Action')
					)
				);
				$coagrouprows = array();
				$countCoagroup = 0-1+$this->Paginator->counter('%start%');
				foreach($coagroup as $Thiscoagroup):
					$countCoagroup++;
					$coagrouprows[]= array(
						$countCoagroup,
						$Thiscoagroup["Coagroup"]["coagroupcode"],
						$Thiscoagroup["Coatype"]["coatypename"]."/".$Thiscoagroup["Coatype"]["coatypenamebn"],
						$Thiscoagroup["Coagroup"]["coagroupname"],$Thiscoagroup["Coagroup"]["coagroupnamebn"],
						$Utilitys->statusURL($this->request["controller"], 'coagroup', $Thiscoagroup["Coagroup"]["id"], 
							$Thiscoagroup["Coagroup"]["coagroupuuid"], $Thiscoagroup["Coagroup"]["coagroupisactive"]),
						$Utilitys->cusurl($this->request["controller"], 'coagroup', $Thiscoagroup["Coagroup"]["id"], 
							$Thiscoagroup["Coagroup"]["coagroupuuid"]));
				endforeach;
				echo $this->Html->tableCells(
					$coagrouprows
				);
			echo $Utilitys->tableend();
			echo $Utilitys->paginationcommon();
	echo"
		</div>
		<script>
			$(document).ready(function() {
				$(\"#CoagroupCoatypeId\").change(function(){
					var coatype_id = $(this).val();
					$(\"#groupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coagroupsearchbycoatype/'+coatype_id, {coatype_id:coatype_id},
					function(result) {
						$(\"#groupsarchloading\").html(result);
					});
					$(\"#grouploading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/listcoagroup/'+coatype_id, {coatype_id:coatype_id},
					function(result) {
						$(\"#CoagroupCoagroupId\").html(result);
						$(\"#grouploading\").html('');
					});
				});
				$(\"#CoagroupCoagroupId\").change(function(){
					var coatype_id = $(\"#CoagroupCoatypeId\").val();
					var coagroup_id = $(this).val();
					if(coatype_id !='' && coatype_id == 0){
						coatype_id = coatype_id;
					}else{
						coatype_id = 0;
					}
					$(\"#groupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coagroupsearchbycoatype/'+coatype_id+'/'+coagroup_id, {coatype_id:coatype_id,coagroup_id:coagroup_id},
					function(result) {
						$(\"#groupsarchloading\").html(result);
					});
				});
				$(\"#CoagroupCoagroupname\").keyup(function(){
					//var coatype_id = $(\"#CoagroupCoatypeId\").val();
					//var coagroup_id = $(\"#CoagroupCoagroupId\").val();
					var coagroupname = $(this).val();
					$(\"#groupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post('".$this->webroot."coas/coagroupsearchbycoatype/0/0/'+coagroupname, {coagroupname:coagroupname},
					function(result) {
						$(\"#groupsarchloading\").html(result);
					});
				});
			});
		</script>
	";
?>