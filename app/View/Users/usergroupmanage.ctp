<?php
echo "<h2>".__('User Group Assign List'). $Utilitys->addurl($title=__("Assign New User Group"), $this->request['controller'], $page="usergroup")."</h2>";
echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Usergroup.group_id",
			array(
				'type'=>'select',
				"options"=>array($group),
				'empty'=>__('Select Group'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
				)
			)
		.$this->Form->input(
			"Usergroup.company_id",
			array(
				'type'=>'select',
				"options"=>array($company),
				'class'=> 'form-control',
				'empty'=>__('Select Company'),
				'div'=>false,
				'tabindex'=>2,
				'label'=>false,
				'style'=>''
				)
			)
		.$this->Form->input(
			"Usergroup.branch_id",
			array(
				'type'=>'select',
				"options"=>array($branch),
				'class'=> 'form-control',
				'empty'=>__('Select Branch'),
				'div'=>false,
				'tabindex'=>3,
				'label'=>false,
				'style'=>''
				)
			)
                .$this->Form->input(
			"Usergroup.user_id",
			array(
				'type'=>'select',
				"options"=>array($user),
				'class'=> 'form-control',
				'empty'=>__('Select User'),
				'div'=>false,
				'tabindex'=>3,
				'label'=>false,
				'style'=>''
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"usergroupsarchloading\">
	";
echo $Utilitys->paginationcommon();
		echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'),
					array(
						__('User Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Group Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Company Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Branch Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Is Active')  => array('class' => 'highlight sortable')
					),
					__('Action')
				)
			);
			$usergrouprows = array();
			$countUserGroup = 0-1+$this->Paginator->counter('%start%');
			
			foreach($usergroup as $Thisusergroup):
				$countUserGroup++;
				$usergrouprows[]= array($countUserGroup,$Thisusergroup["Usergroup"]["user_fullname"],$Thisusergroup["Usergroup"]["group_name"], $Thisusergroup["Usergroup"]["company_name"], $Thisusergroup["Usergroup"]["branch_name"], $Utilitys->statusURL($this->request["controller"], 'usergroup', $Thisusergroup["Usergroup"]["id"], $Thisusergroup["Usergroup"]["usergroupuuid"], $Thisusergroup["Usergroup"]["usergroupisactive"]),$Utilitys->cusurl($this->request['controller'], 'usergroup', $Thisusergroup["Usergroup"]["id"], $Thisusergroup["Usergroup"]["usergroupuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$usergrouprows
			);
		echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
				</div>
		<script>
			$(document).ready(function() {
				$(\"#UsergroupGroupId\").change(function(){
					var UsergroupGroupId = $(\"#UsergroupGroupId\").val();
					var UsergroupCompanyId = $(\"#UsergroupCompanyId\").val();
					var UsergroupBranchId = $(\"#UsergroupBranchId\").val();
                                        var UsergroupUserId = $(\"#UsergroupUserId\").val();
					$(\"#usergroupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usergroupsearchbytext',
						{UsergroupGroupId:UsergroupGroupId,UsergroupCompanyId:UsergroupCompanyId,UsergroupBranchId:UsergroupBranchId, UsergroupUserId:UsergroupUserId},
						function(result) {
							$(\"#usergroupsarchloading\").html(result);
						}
					);
				});
				$(\"#UsergroupCompanyId\").change(function(){
					var UsergroupGroupId = $(\"#UsergroupGroupId\").val();
					var UsergroupCompanyId = $(\"#UsergroupCompanyId\").val();
					var UsergroupBranchId = $(\"#UsergroupBranchId\").val();
                                        var UsergroupUserId = $(\"#UsergroupUserId\").val();
					$(\"#usergroupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usergroupsearchbytext',
						{UsergroupGroupId:UsergroupGroupId,UsergroupCompanyId:UsergroupCompanyId,UsergroupBranchId:UsergroupBranchId, UsergroupUserId:UsergroupUserId},
						function(result) {
							$(\"#usergroupsarchloading\").html(result);
						}
					);
				});
				$(\"#UsergroupBranchId\").change(function(){
					var UsergroupGroupId = $(\"#UsergroupGroupId\").val();
					var UsergroupCompanyId = $(\"#UsergroupCompanyId\").val();
					var UsergroupBranchId = $(\"#UsergroupBranchId\").val();
                                        var UsergroupUserId = $(\"#UsergroupUserId\").val();
					$(\"#usergroupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usergroupsearchbytext',
						{UsergroupGroupId:UsergroupGroupId,UsergroupCompanyId:UsergroupCompanyId,UsergroupBranchId:UsergroupBranchId, UsergroupUserId:UsergroupUserId},
						function(result) {
							$(\"#usergroupsarchloading\").html(result);
						}
					);
				});
                                $(\"#UsergroupUserId\").change(function(){
					var UsergroupGroupId = $(\"#UsergroupGroupId\").val();
					var UsergroupCompanyId = $(\"#UsergroupCompanyId\").val();
					var UsergroupBranchId = $(\"#UsergroupBranchId\").val();
                                        var UsergroupUserId = $(\"#UsergroupUserId\").val();
					$(\"#usergroupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/usergroupsearchbytext',
						{UsergroupGroupId:UsergroupGroupId,UsergroupCompanyId:UsergroupCompanyId,UsergroupBranchId:UsergroupBranchId, UsergroupUserId:UsergroupUserId},
						function(result) {
							$(\"#usergroupsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>