<?php
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"3\">
				<h2>".__('Designation Information')."</h2>
			</td>
		</tr>
		<tr>
			<th>
				".__('Designation Name')."
			</th>
			<td>
				".$designation[0]["Designation"]["designation_name"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Company Name')."
			</th>
			<td>
				".$designation[0]["Designation"]["company_fullname"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Group Name')."
			</th>
			<td>
				".$designation[0]["Designation"]["group_name"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Is Parent')."
			</th>
			<td>
				".$designation[0]["Designation"]["isParent"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Sub Designation Of')."
			</th>
			<td>
				".$designation[0]["Designation"]["designationparent_name"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Active")."
			</th>
			<td>
				".$designation[0]["Designation"]["isActive"]."
			</td>
		</tr>
	";
	echo $Utilitys->tableend($class=null, $id=null);
?>