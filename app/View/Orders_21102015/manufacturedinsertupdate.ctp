<?php
//pr($_GET);
//pr($manufacturedproduct);
//pr($purchaseproduct);
echo $this->Form->create('Orders', array('action' => 'manufacturedinsertupdateaction', 'type' => 'file'));
	echo $this->Form->input('manufacturedproduct_id', array('type'=>'hidden', 'value'=>@$manufacturedproduct[0]['Orderdetail']['id']));
	echo $this->Form->input('purchasesproduct_id', array('type'=>'hidden', 'value'=>@$purchaseproduct[0]['Orderdetail']['id']));
	echo $this->Form->input('manufacturedproductmon', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
	echo $this->Form->input('manufacturedproductkg', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
	echo $this->Form->input('manufacturedproductgram', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
	echo $this->Form->input('manufacturedproductunitmon', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
	echo $this->Form->input('manufacturedproductunitkg', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
	echo $this->Form->input('manufacturedproductunitgram', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
	echo $this->Form->input('manufacturedproductdafultprice', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
	echo $this->Form->input('manufacturedproductstockunit', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
////////////////////////////////////////////////////
	echo $this->Form->input('purchaseproductmon', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
	echo $this->Form->input('purchaseproductkg', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
	echo $this->Form->input('purchaseproductgram', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
	echo $this->Form->input('purchaseproductunitmon', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
	echo $this->Form->input('purchaseproductunitkg', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
	echo $this->Form->input('purchaseproductunitgram', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
	echo $this->Form->input('purchaseproductdafultprice', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
	echo $this->Form->input('purchaseproductstockunit', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
/////////////////////////////////////////////////	
	echo"<h3>".__("Add New Manufactured Products")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-12\">";
			echo"<div class=\"row\">";
				echo"<div class=\"col-md-6\">";
					echo $this->Form->input(
						"Orderdetail.branch_id",
						array(
							'type'=>'select',
							"options"=>$salescenter,
							'empty'=>__('Select Stock Place'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>1,
							'label'=>__('Select Stock Place'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$manufacturedproduct[0]['Orderdetail']['branch_id'] ? $manufacturedproduct[0]['Orderdetail']['branch_id'] : 0)
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-6\">";
					echo " ";
				echo"</div>";
			echo"</div>";
			echo"<div class=\"row\">";
				echo"<div class=\"col-md-6\">";
					echo $this->Form->input(
						"Order.manufactureproduct_id",
						array(
							'type'=>'select',
							"options"=>$manufacturedproductlist,
							'empty'=>__('Select Manufactured Product'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>2,
							'label'=>__('Select Manufactured Product'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$manufacturedproduct[0]['Orderdetail']['product_id'] ? $manufacturedproduct[0]['Orderdetail']['product_id'] : 0)
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-6\">";
					echo "<span id=\"manufactureproductspan\"></span>";
				echo"</div>";
			echo"</div>";
			echo"<div class=\"row\">";
				echo"<div class=\"col-md-6\">";
					echo $this->Form->input(
						"Order.purchaseproduct_id",
						array(
							'type'=>'select',
							"options"=>$purchaseproductlist,
							'empty'=>__('Select Purchased Product'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>3,
							'label'=>__('Select Purchased Product'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$purchaseproduct[0]['Orderdetail']['product_id'] ? $purchaseproduct[0]['Orderdetail']['product_id'] : 0)
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-6\">";
					echo "<span id=\"purchaseproductspan\"></span>";
					echo "<span id=\"purchaseproductamountspan\"></span>";
				echo"</div>";
			echo"</div>";
			echo"<div class=\"row\">";
				echo"<div class=\"col-md-6\">";
					echo"<div class=\"col-md-2\">";
						echo $this->Form->input(
							"Orderdetail.quantity",
							array(
								'type' => 'text',
								'div' => array('class'=> 'form-group'),
								'label' => __('Quantity'),
								'tabindex'=>4,
								'placeholder' => __("Quantity"),
								'class'=> 'form-control',
								'data-validation-engine'=>'validate[required]',
								'style' => '',
								'value' => (@$manufacturedproduct[0]['Orderdetail']['quantity'] ? $manufacturedproduct[0]['Orderdetail']['quantity'] : 0)
							)
						);
					echo"</div>";
					echo"<div class=\"col-md-2\">";
					echo $this->Form->input(
						"Orderdetail.orderdetailmon",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('MON'),
							'tabindex'=>5,
							'placeholder' => __("MON"),
							'class'=> 'form-control',
							'style' => '',
							'readonly' => 'readonly',
							'value' => (@$manufacturedproduct[0]['Orderdetail']['orderdetailmon'] ? $manufacturedproduct[0]['Orderdetail']['orderdetailmon'] : 0)
						)
					);
					echo"</div>";
					echo"<div class=\"col-md-2\">";
						echo $this->Form->input(
							"Orderdetail.orderdetailkg",
							array(
								'type' => 'text',
								'div' => array('class'=> 'form-group'),
								'label' => __('Kilogram'),
								'tabindex'=>6,
								'placeholder' => __("Kilogram"),
								'class'=> 'form-control',
								'style' => '',
								'readonly' => 'readonly',
								'value' => (@$manufacturedproduct[0]['Orderdetail']['orderdetailkg'] ? $manufacturedproduct[0]['Orderdetail']['orderdetailkg'] : 0)
							)
						);
					echo"</div>";
					echo"<div class=\"col-md-2\">";
					echo $this->Form->input(
						"Orderdetail.orderdetailgram",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Gram'),
							'tabindex'=>7,
							'placeholder' => __("Gram"),
							'class'=> 'form-control',
							'style' => '',
							'readonly' => 'readonly',
							'value' => (@$manufacturedproduct[0]['Orderdetail']['orderdetailgram'] ? $manufacturedproduct[0]['Orderdetail']['orderdetailgram'] : 0)
						)
					);
					echo"</div>";
					echo"<div class=\"col-md-4\">";
					echo $this->Form->input(
						"manufacturedproducttransactionamount",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Total Price'),
							'tabindex'=>8,
							'placeholder' => __("Total Price"),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style' => '',
							'readonly' => 'readonly',
							'value' => (@$manufacturedproduct[0]['Orderdetail']['transactionamount'] ? $manufacturedproduct[0]['Orderdetail']['transactionamount'] : 0)
						)
					);
					echo $this->Form->input('purchaseproducttransactionamount', array('type'=>'hidden', 'value'=>@$banktransaction[0]['Banktransaction']['id']));
					echo"</div>";
				echo"</div>";
				echo"<div class=\"col-md-6\">";
					echo "<span id=\"totalquantityspan\"></span>";
				echo"</div>";
			echo"</div>";
		echo"</div>";
			echo $Utilitys->allformbutton('',$this->request["controller"],$page='manufactured');
	echo"</div>";
echo $this->Form->end();
echo"
	<script>
		$(document).ready(function() {
			OrderManufactureproduct();
			OrderPurchaseproduct();
			OrderdetailQuantity();

			$(\"#OrdersManufacturedinsertupdateactionForm\").validationEngine();
			
			$(\"#OrderdetailBranchId\").change(function(){
				OrderManufactureproduct();
			});

			$(\"#OrderManufactureproductId\").change(function(){
				OrderManufactureproduct();
			});

			$(\"#OrderPurchaseproductId\").change(function(){
				OrderManufactureproduct();
				OrderPurchaseproduct();
			});
			$(\"#OrderdetailQuantity\").keyup(function(){
				OrderManufactureproduct();
				OrderPurchaseproduct();
				OrderdetailQuantity();
			});
			$(\"#OrderdetailBranchId\").change(function(){
				var OrderdetailBranchId = $(this).val();
				$(\"#OrderBranchIdspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."purchases/manufactureproductlistbybranchid',
					data:{OrderdetailBranchId:OrderdetailBranchId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#OrderManufactureproductId\").html(result.manufactured_product_options);
						$(\"#OrderPurchaseproductId\").html(result.purchased_product_options);
						$(\"#\").html('');
					}
				});
			});
		});
		function OrderManufactureproduct(){
			var OrderdetailBranchId = $(\"#OrderdetailBranchId\").val();
			var OrderManufactureproductId = $(\"#OrderManufactureproductId\").val();
			$(\"#productidloadspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
			$.ajax({
			       	cache: false,
			       	type: 'POST',
				url: '".$this->webroot."orders/qtyunitpricebyproduct',
				data: {OrderdetailProductId:OrderManufactureproductId,OrderdetailBranchId:OrderdetailBranchId},					
				dataType: \"JSON\",
				success: function(result) {
					$(\"#OrderStock\").val(result.productquantity);
					$(\"#OrdersManufacturedproductstockunit\").val(result.productunit);
					$(\"#stockunit\").val(result.productunitprice);
					$(\"#OrderPrice\").val(result.producttotalprice);
					$(\"#OrderdetailProductCode\").val(result.productid);
					$(\"#OrdersManufacturedproductmon\").val(result.orderdetailmon);
					$(\"#OrdersManufacturedproductkg\").val(result.orderdetailkg);
					$(\"#OrdersManufacturedproductgram\").val(result.orderdetailgram);
					$(\"#OrdersManufacturedproductunitmon\").val(result.unitmon);
					$(\"#OrdersManufacturedproductunitkg\").val(result.unitkilogram);
					$(\"#OrdersManufacturedproductunitgram\").val(result.unitgram);
					$(\"#OrdersManufacturedproductdafultprice\").val(result.productdafultprice);
					$(\"#productidloadspan\").html('');
					$(\"#manufactureproductspan\").html('Stock : '+result.orderdetailmon+' MON / '+result.orderdetailkg+' KG / '+result.orderdetailgram+' Gram');
				}
			});
				OrderPurchaseproduct();
		}

		function OrderPurchaseproduct(){
			var OrderdetailBranchId = $(\"#OrderdetailBranchId\").val();
			var OrderPurchaseproductId = $(\"#OrderPurchaseproductId\").val();
			$(\"#productidloadspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
			$.ajax({
			       	cache: false,
			       	type: 'POST',
				url: '".$this->webroot."orders/qtyunitpricebyproduct',
				data: {OrderdetailProductId:OrderPurchaseproductId,OrderdetailBranchId:OrderdetailBranchId},					
				dataType: \"JSON\",
				success: function(result) {
					$(\"#OrderStock\").val(result.productquantity);
					$(\"#OrdersPurchaseproductstockunit\").val(result.productunit);
					$(\"#stockunit\").val(result.productunitprice);
					$(\"#OrderPrice\").val(result.producttotalprice);
					$(\"#OrderdetailProductCode\").val(result.productid);
					$(\"#OrdersPurchaseproductmon\").val(result.orderdetailmon);
					$(\"#OrdersPurchaseproductkg\").val(result.orderdetailkg);
					$(\"#OrdersPurchaseproductgram\").val(result.orderdetailgram);
					$(\"#OrdersPurchaseproductunitmon\").val(result.unitmon);
					$(\"#OrdersPurchaseproductunitkg\").val(result.unitkilogram);
					$(\"#OrdersPurchaseproductunitgram\").val(result.unitgram);
					$(\"#OrdersPurchaseproductdafultprice\").val(result.productdafultprice);
					$(\"#productidloadspan\").html('');
					$(\"#purchaseproductspan\").html('Stock : '+result.orderdetailmon+' MON / '+result.orderdetailkg+' KG / '+result.orderdetailgram+' Gram (Total amount:'+result.productdafultprice+' )');
				}
			});
				OrderdetailQuantity();
		}

		function OrderdetailQuantity(){
			var OrderdetailQuantity = $(\"#OrderdetailQuantity\").val();
			var OrdersManufacturedproductmon =$(\"#OrdersManufacturedproductmon\").val();
			var OrdersManufacturedproductkg =$(\"#OrdersManufacturedproductkg\").val();
			var OrdersManufacturedproductgram =$(\"#OrdersManufacturedproductgram\").val();

			var OrdersManufacturedproductunitmon =$(\"#OrdersManufacturedproductunitmon\").val();
			var OrdersManufacturedproductunitkg =$(\"#OrdersManufacturedproductunitkg\").val();
			var OrdersManufacturedproductunitgram =$(\"#OrdersManufacturedproductunitgram\").val();
			var OrdersManufacturedproductdafultprice =$(\"#OrdersManufacturedproductdafultprice\").val();

			var OrdersPurchaseproductmon =$(\"#OrdersPurchaseproductmon\").val();
			var OrdersPurchaseproductkg =$(\"#OrdersPurchaseproductkg\").val();
			var OrdersPurchaseproductgram =$(\"#OrdersPurchaseproductgram\").val();

			var totalmon = OrdersManufacturedproductunitmon * OrderdetailQuantity;
			var totalkg = OrdersManufacturedproductunitkg * OrderdetailQuantity;
			var totalgram = OrdersManufacturedproductunitgram * OrderdetailQuantity;

			var Rem_OrdersPurchaseproductmon = OrdersPurchaseproductmon - totalmon;
			var Rem_OrdersPurchaseproductkg = OrdersPurchaseproductkg - totalkg;
			var Rem_OrdersPurchaseproductgram = OrdersPurchaseproductgram - totalgram;

			var OrdersPurchaseproductunitmon = $(\"#OrdersPurchaseproductunitmon\").val();
			var OrdersPurchaseproductunitkg = $(\"#OrdersPurchaseproductunitkg\").val();
			var OrdersPurchaseproductunitgram = $(\"#OrdersPurchaseproductunitgram\").val();
			var OrdersPurchaseproductdafultprice = $(\"#OrdersPurchaseproductdafultprice\").val();

			var purchasetotalprice=0;
			if(OrdersPurchaseproductunitmon == 1){
				purchasetotalprice = totalmon*OrdersPurchaseproductdafultprice;
			}else if(OrdersPurchaseproductunitgram == 1){
				purchasetotalprice = totalgram*OrdersPurchaseproductdafultprice;
			}else{
				purchasetotalprice = totalkg*OrdersPurchaseproductdafultprice;
			}

			$(\"#purchaseproductamountspan\").html('total amount : ' +purchasetotalprice);
			$(\"#OrdersPurchaseproducttransactionamount\").val(purchasetotalprice);



			$(\"#totalquantityspan\").html('Stock : '+totalmon+' MON / '+totalkg+' KG / '+totalgram+' Gram<br>Total Remainig : '+Rem_OrdersPurchaseproductmon+' MON / '+Rem_OrdersPurchaseproductkg+' KG / '+Rem_OrdersPurchaseproductgram+' Gram<br>Total Amount : '+((OrderdetailQuantity*OrdersManufacturedproductdafultprice)/totalmon)+' Taka / '+((OrderdetailQuantity*OrdersManufacturedproductdafultprice)/totalkg)+' Taka / '+((OrderdetailQuantity*OrdersManufacturedproductdafultprice)/totalgram)+' Taka');
			$(\"#OrderdetailOrderdetailmon\").val(totalmon);
			$(\"#OrderdetailOrderdetailkg\").val(totalkg);
			$(\"#OrderdetailOrderdetailgram\").val(totalgram);
			$(\"#OrdersManufacturedproducttransactionamount\").val(OrderdetailQuantity*OrdersManufacturedproductdafultprice);
			if(Rem_OrdersPurchaseproductmon<0 || Rem_OrdersPurchaseproductkg<0 || Rem_OrdersPurchaseproductgram<0){
				alert('".__("Product Overflow")."');
			}
		}
	</script>

";
?>