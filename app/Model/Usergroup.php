<?php
App::uses('AppModel', 'Model');
class Usergroup extends AppModel {
	public $name = 'Usergroup';
	public $usetables = 'usergroups';

	var $belongsTo = array(
		'Company' => array(
			'fields' =>array('companyname', 'companynamebn'),
			'className'    => 'Company',
			'foreignKey'    => 'company_id'
		),
		'Branch' => array(
			'fields' =>array('branchname', 'branchnamebn'),
			'className'    => 'Branch',
			'foreignKey'    => 'branch_id'
		),
		'User' => array(
			'fields' =>array('User.*'),
			'className'    => 'User',
			'foreignKey'    => 'user_id'
		),
		'Group' => array(
			'fields' =>array('groupname'),
			'className'    => 'Group',
			'foreignKey'    => 'group_id'
		)
	);
	var $virtualFields = array(
		'company_name' => 'CONCAT(Company.companyname, " / ", Company.companynamebn)',
		'branch_name' => 'CONCAT(Branch.branchname, " / ", Branch.branchnamebn)',
		'user_name' => 'CONCAT(User.username)',
		'group_name' => 'CONCAT(Group.groupname)',
		'user_fullname' => 'CONCAT(User.userfirstname, "   ", User.usermiddlename, "   ", User.userlastname)',
		'isActive' => 'IF(Usergroup.usergroupisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Usergroup.usergroupisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
}