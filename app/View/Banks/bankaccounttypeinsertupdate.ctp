<?php 
	echo $this->Form->create('Banks', array('action' => 'bankaccounttypeinsertupdateaction'));
		echo $this->Form->inpute('id', array('type'=>'hidden', 'value'=>@$bankaccounttype[0]['Bankaccounttype']['id']));
		echo $this->Form->inpute('bankaccounttypeuuid', array('type'=>'hidden', 'value'=>@$bankaccounttype[0]['Bankaccounttype']['bankaccounttypeuuid']));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$bankaccounttype[0]['Bankaccounttype']['bankaccounttypeisactive'] ? $bankaccounttype[0]['Bankaccounttype']['bankaccounttypeisactive'] : 0), 
			'class' => 'form-inline'
		);

	echo"<h3>".__("Add Bank Account Type Information")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-8\">";
			echo $this->Form->input(
				"Bankaccounttype.bankaccounttypename",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Bank Account Type'),
					'tabindex'=>2,
					'placeholder' => __("Bank Account Type"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style' => '',
					'value' => @$bankaccounttype[0]['Bankaccounttype']['bankaccounttypename']
				)
			);
			echo $this->Form->input(
				"Bankaccounttype.bankaccounttypenamebn",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Bank Account Type BN'),
					'tabindex'=>3,
					'placeholder' => __("Bank Account Type BN"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style' => '',
					'value' => @$bankaccounttype[0]['Bankaccounttype']['bankaccounttypenamebn']
				)
			);
			echo"<div class=\"form-group\">";
				echo"<label>isActive?</label><br>";
				echo $this->Form->radio(
					'Bankaccounttype.bankaccounttypeisactive', 
					$options,
					$attributes
				);
			echo"</div>";
			echo $Utilitys->allformbutton('',$this->request["controller"],$page='bankbranch');
		echo"</div>";
	echo"</div>";
	echo $this->Form->end();
	echo"
	<script>
		$(document).ready(function() {
			$(\"#BanksBankaccounttypeinsertupdateactionForm\").validationEngine();
		});
	</script>
	";	
?>