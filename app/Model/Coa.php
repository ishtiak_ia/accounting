<?php
App::uses('AppModel', 'Model');
class Coa extends AppModel {
	public $name = 'Coa';
	public $usetables = 'coas';
	var $belongsTo = array(
		'Coatype' => array(
			'fields' =>array('coatypename','coatypenamebn'),
			'foreignKey'    => 'coatype_id'
		),
		'Coagroup' => array(
			'fields' =>array('coagroupname','coagroupnamebn'),
			'className'    => 'Coagroup',
			'foreignKey'    => 'coagroup_id'
			//'conditions'	=> 'Coagroup.id = Coa.coagroup_id'
		),
		'Coaclass' => array(
			'fields' =>array('coaclassname','coaclassnamebn'),
			'className'    => 'Coaclass',
			'foreignKey'    => 'coaclass_id'
		),
		'Coaclasssubclass' => array(
			'fields' =>array('coaclasssubclassname','coaclasssubclassnamebn'),
			'className'    => 'Coaclasssubclass',
			'foreignKey'    => 'coaclasssubclass_id'
		),
		'Company' => array(
			'fields' =>array('companyname','companynamebn'),
			'className'    => 'Company',
			'foreignKey'    => 'company_id'
		),
		'Branch' => array(
			'fields' =>array('branchcode','branchname','branchnamebn'),
			'className'    => 'Branch',
			'foreignKey'    => 'branch_id'
		),
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'coainsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'coaupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'coadeleteid'
		)
	);
	var $virtualFields = array(
		'coa_name' => 'CONCAT(Coa.coaname, " / ", Coa.coanamebn)',
		'coatype_name' => 'CONCAT(Coatype.coatypename, " / ", Coatype.coatypenamebn)',
		'coagroup_name' => 'CONCAT(Coagroup.coagroupname, " / ", Coagroup.coagroupnamebn)',
		'coaclass_name' => 'CONCAT(Coaclass.coaclassname, " / ", Coaclass.coaclassnamebn)',
		'coaclasssubclass_name' => 'CONCAT(Coaclasssubclass.coaclasssubclassname, " / ", Coaclasssubclass.coaclasssubclassnamebn)',
		'company_name' => 'CONCAT(Company.companyname, " / ", Company.companynamebn)',
		'branch_name' => 'CONCAT(Branch.branchname, " / ", Branch.branchnamebn)',
		'isActive' => 'IF(Coa.coaisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Coa.coaisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);

	public $validate = array(
		'coatype_id' => array(
			'coatype_id_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Type field is required',
				'last' => true
			)
		),
		'coagroup_id' => array(
			'coagroup_id_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Group field is required',
				'last' => true
			)
		),
		'coacode' => array(
			'coacode_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Code field is required',
				'last' => true
			)
		),
		'coaname' => array(
			'rule' => 'notEmpty',
			'message' => 'This COA Name field is required',
			'last' => true
		),
		'coanamebn' => array(
			'rule' => 'notEmpty',
			'message' => 'This COA Name field is required',
			'last' => true
		)
	);
}

?>
