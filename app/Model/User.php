<?php
//App::uses('AuthComponent', 'Controller/Component');
App::uses('AppModel', 'Model');
class User extends AppModel {
	public $name = 'User';
	public $usetables = 'users';
	var $belongsTo = array(
		'Company' => array(
			'fields' =>array('companyname', 'companynamebn'),
			'className'    => 'Company',
			'foreignKey'    => 'company_id'
		),
		'Branch' => array(
			'fields' =>array('branchname', 'branchnamebn'),
			'className'    => 'Branch',
			'foreignKey'    => 'branch_id'
		),
		'Group' => array(
			'fields' =>array('groupname'),
			'className'    => 'Group',
			'foreignKey'    => 'group_id'
		),
		'Department' => array(
			'fields' =>array('departmentname', 'departmentnamebn'),
			'className'    => 'Department',
			'foreignKey'    => 'department_id'
		),
		'Designation' => array(
			'fields' =>array('designationname', 'designationnamebn'),
			'className'    => 'Designation',
			'foreignKey'    => 'designation_id'
		),
		'Institute' => array(
			'fields' =>array('Institute.*'),
			'className'    => 'Institute',
			'foreignKey'    => 'institute_id'
		),
		'Institutetype' => array(
			'fields' =>array('Institutetype.*'),
			'className'    => 'Institutetype',
			'foreignKey'    => false,
			'conditions'	=> 'Institute.institutetype_id = Institutetype.id'
		),
		'Category' => array(
			'fields' =>array('categoryname'),
			'className'    => 'Category',
			'foreignKey'    => 'category_id'
		),
		'Subject' => array(
			'fields' =>array('subjectname', 'subjectnamebn'),
			'className'    => 'Subject',
			'foreignKey'    => 'subject_id'
		),
		'Division' => array(
			'fields' =>array('locationname', 'locationnamebn'),
			'className'    => 'Location',
			'foreignKey'    => false,
			'conditions'	=> 'Institute.divisionid = Division.id'
		),
		'District' => array(
			'fields' =>array('locationname', 'locationnamebn'),
			'className'    => 'Location',
			'foreignKey'    => false,
			'conditions'	=> 'Institute.districtid = District.id'
		),
		'Upazila' => array(
			'fields' =>array('locationname', 'locationnamebn'),
			'className'    => 'Location',
			'foreignKey'    => false,
			'conditions'	=> 'Institute.upazilaid = Upazila.id'
		),
		'Unionparishad' => array(
			'fields' =>array('locationname', 'locationnamebn'),
			'className'    => 'Location',
			'foreignKey'    => false,
			'conditions'	=> 'Institute.unionparishadid = Unionparishad.id'
		),
		'Para' => array(
			'fields' =>array('locationname', 'locationnamebn'),
			'className'    => 'Location',
			'foreignKey'    => false,
			'conditions'	=> 'Institute.paraid = Para.id'
		)
		/*,
		'Useremergencycontact' => array(
			'fields' =>array('useremergencycontactname', 'useremergencycontactrelationship', 'useremergencycontacthomephone', 'useremergencycontactmobilephone', 'useremergencycontactisactive'),
			'className'    => 'Useremergencycontact',
			'foreignKey'    => false,
			'conditions'	=> 'User.id = Useremergencycontact.user_id'
		),
		
		'Userdependent' => array(
			'fields' =>array('userdependentname', 'userrelationship_id', 'userdependentdob', 'userdependentisactive'),
			'className'    => 'Userdependent',
			'foreignKey'    => false,
			'conditions'	=> 'User.id = Userdependent.user_id'
		)
		*/
		
	);
	var $virtualFields = array(
		'user_fullname' => 'CONCAT(User.userfirstname, "   ", User.usermiddlename, "   ", User.userlastname)',
		'userpunchmachineid' => 'CONCAT(User.userpunchmachineid)',
		'user_mobile' => 'CONCAT(User.usermobileofficial)',
		'user_groupname' => 'Group.groupname',

		'salaryamount' => 'SELECT salaryamount FROM salaries WHERE salaries.user_id = User.id AND salaries.salaryisactive=1',
		'all_salaryamount' => 'SELECT GROUP_CONCAT(CONCAT("<tr><td>",salaryinsertdate,"</td><td>",salaryamount,"</td><td>")) FROM salaries WHERE salaries.user_id = User.id ORDER BY salaries.salaryinsertdate DESC',
		'all_grievances' => 'SELECT GROUP_CONCAT(CONCAT("<tr><td>",IF(grievancetitle="","N/A",grievancetitle),"</td><td>",IF(grievancedescription="","N/A",grievancedescription),"</td><td>",IF(grievanceaction="","N/A",grievanceaction),"</td><td>",grievancedate,"</td><td>")) FROM grievances WHERE grievances.user_id = User.id ORDER BY grievances.grievancedate DESC',
		'user_education' => 'SELECT GROUP_CONCAT(CONCAT("<tr><td>",usereducationdegreename,"</td><td>",usereducationgroup,"</td><td>",usereducationinstitute,"</td><td>",usereducationpassing,"</td><td>",usereducationboard,"</td><td>",usereducationresult,"</td><td>",usereducationduration,"</td></tr>")) FROM usereducations WHERE usereducations.user_id = User.id ORDER BY usereducations.usereducationpassing DESC',
		'user_rewards' => 'SELECT GROUP_CONCAT(CONCAT("<tr><td>",rewardname,"</td><td>",rewardamount,"</td><td>",rewarddescription,"</td><td>",rewarddate,"</td></tr>")) FROM rewards WHERE rewards.user_id = User.id ORDER BY rewards.rewarddate DESC',
		'company_name' => 'CONCAT(Company.companyname, " / ", Company.companynamebn)',
		'branch_name' => 'CONCAT(Branch.branchname, " / ", Branch.branchnamebn)',
		'category_name' => 'CONCAT(Category.categoryname)',
		'subject_fullname' => 'CONCAT(Subject.subjectname, " / ", Subject.subjectnamebn)',
		'department_name' => 'CONCAT(Department.departmentname, " / ", Department.departmentnamebn)',
		'designation_name' => 'CONCAT(Designation.designationname, " / ", Designation.designationnamebn)',
		'institute_name' => 'CONCAT(Institute.institutename, " / ", Institute.institutenamebn)',
		'user_details_address' => 'CONCAT(Para.locationname, " / ", Para.locationnamebn, " <br /> ", Unionparishad.locationname, " / ", Unionparishad.locationnamebn, " <br /> ", Upazila.locationname, " / ", Upazila.locationnamebn, " <br /> ", District.locationname, " / ", District.locationnamebn, " <br /> ", Division.locationname, " / ", Division.locationnamebn)',
		'user_total_age' => 'CONCAT("  (Now you are  ",YEAR(NOW())-YEAR(User.userdob)," Year ", IF(MONTH(NOW())>MONTH(User.userdob),MONTH(NOW())-MONTH(User.userdob),MONTH(User.userdob)-MONTH(NOW())), " Month ", IF(DAY(NOW())>DAY(User.userdob),DAY(NOW())-DAY(User.userdob),DAY(User.userdob)-DAY(NOW())), "Day\'s Old)")',
		'total_rating_point' => 'SELECT SUM(ratings.ratingvalue) FROM ratings WHERE ratings.user_id = User.id',
		'total_rating_row' => 'SELECT COUNT(*) FROM ratings WHERE ratings.user_id = User.id',
		'next_notification_date' => 'SELECT DATE_FORMAT(DATE(DATE_ADD(answers.answerinsertdate,INTERVAL 15 DAY)),"%d-%m-%Y") FROM answers WHERE answers.user_id = User.id ORDER BY answers.id DESC LIMIT 0,1',
		'next_notification_date_actual' => 'SELECT DATE(DATE_ADD(answers.answerinsertdate,INTERVAL 15 DAY)) FROM answers WHERE answers.user_id = User.id ORDER BY answers.id DESC LIMIT 0,1',

		'isActive' => 'IF(User.userisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(User.userisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
	public $validate = array(
		'username' => array(
			'username_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This field is required',
				'last' => true
			),
			'username_unique' => array(
				'rule' => 'isUnique',
				'message' => 'That username is already in use',
				'last' => true
			),
		),
		'email' => array(
		   'email_not_empty' => array(
			'rule' => 'notEmpty',
			'message' => 'This field is required',
			'last' => true
			),
			'email_unique' => array(
				'rule' => 'isUnique',
				'message' => 'This email is already in use',
				'last' => true
			),
			'email' => array(
				'rule' => 'email',
				'message' => 'Please Enter Valid Email',
				'last' => true
			)
		),
	); 
	function Checkcurrentpassword($data, $log) {
			if(md5($data['User']['currentpassword']) == $log){
				return true;
			}
			else
			{
				return false;
			}	
		
	}
	function SavePassword($data){
		$this->query("UPDATE users SET userpassword='". md5($data['User']['password']) ."' WHERE id ='".$data['Users']['id']."'");
		return true;	
	}
	public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['userpassword'])) {
			// $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
			$this->data[$this->alias]['userpassword'] = md5($this->data[$this->alias]['userpassword']);
		}
		return true;
	}
}

?>