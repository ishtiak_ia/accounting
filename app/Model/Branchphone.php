<?php

App::uses('AppModel', 'Model');

class Branchphone extends AppModel {

    public $name = 'Branchphone';
    public $usetables = 'branchphones';

    var $belongsTo = array(
	'Branch' => array(
			'fields' =>array('branchphoneno'),
			'className'    => 'Branch',
			'foreignKey'    => 'branch_id'
		)
	);
    
}

?>