<?php
App::import('Controller', 'Logins');
App::import('Controller', 'Users');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');

class InventoryreportsController extends AppController
{
    public $name = 'Inventoryreport';
    public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'JqueryValidation');
    public $components = array('RequestHandler', 'Session');
    public $uses = array('Company', 'Group', 'User', 'Usergroup',  'Journaltransaction', 'Coa',
         'Coas','Product', 'Productcategory', 'Producttype', 'Branch', 'Order','Orderdetail');

    var $Users;

    var $Logins;

    function beforeFilter()
    {
        $this->Logins =& new LoginsController;
        $this->Logins->constructClasses();
        $this->Logins->__validateLoginStatus();
        $this->Users =& new UsersController;
        $this->Users->constructClasses();


        $Utilitys = new UtilitysController;
        $this->set('Utilitys', $Utilitys);
        if (!isset($_SESSION["User"]["id"])) {
            $this->redirect("/logins/login");
        }
    }

    public function beforeRender()
    {
        $this->response->disableCache();//prevent useless warnings for Ajax
        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
        }
    }

    public function inventory(){
        $this->layout='default';
        $this->set('title_for_layout', __('Inventory').' | '.__(Configure::read('site_name')));

        $product_code=$this->Product->find(
                            'list',
                            array(
                                "fields" => array("id","productcode")
                            )
                        );
        $this->set('product_code',$product_code);

        $product_name=$this->Product->find(
            'list',
            array(
                "fields" => array("id","product_name")
            )
        );
        $this->set('product_name',$product_name);

        $product_category=$this->Productcategory->find(
            'list',
            array(
                "fields" => array("id","productcategory_name")
            )
        );
        $this->set('product_category',$product_category);

        $product_type=$this->Producttype->find(
            'list',
            array(
                "fields" => array("id","producttype_name")
            )
        );
        $this->set('product_type',$product_type);

        $location_code=$this->Branch->find(
            'list',
            array(
                "fields" => array("id","branchcode")
            )
        );
        $this->set('location_code',$location_code);

        $location_name=$this->Branch->find(
            'list',
            array(
                "fields" => array("id","branchname")
            )
        );
        $this->set('location_name',$location_name);

    }

    function inventorygenerate(){
        $this->layout = 'ajax';
        $opening_balance_purchase =array();
        $opening_balance_sales = array();
        $opening_balance_purchase_return = array();
        $opening_balance_sales_return =array();
        $net_change_purchase_return = array();
        $net_change_sales_return = array();
        $condition = array('Product.id=Orderdetail.product_id','Order.id=Orderdetail.transaction_id','Product.productcategory_id=Productcategory.id','Product.producttype_id=Producttype.id','Branch.id=Product.branch_id',
             'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']));
        if(!empty($this->request->data['product_id']))
             $condition = array('Product.id=Orderdetail.product_id','Order.id=Orderdetail.transaction_id','Product.productcategory_id=Productcategory.id','Product.producttype_id=Producttype.id','Branch.id=Product.branch_id','Product.id'=>$this->request->data['product_id'],
            'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']));
        if(!empty($this->request->data['branch_id']))
              $condition = array('Product.id=Orderdetail.product_id','Order.id=Orderdetail.transaction_id','Product.productcategory_id=Productcategory.id','Product.producttype_id=Producttype.id','Branch.id=Product.branch_id','Branch.id'=>$this->request->data['branch_id'],
                'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']));
       if(!empty($this->request->data['type_id']) && !empty($this->request->data['category_id']))
            $condition = array('Product.id=Orderdetail.product_id','Order.id=Orderdetail.transaction_id','Product.productcategory_id=Productcategory.id','Product.producttype_id=Producttype.id','Branch.id=Product.branch_id',
                'Product.productcategory_id'=>$this->request->data['category_id'], 'Product.producttype_id'=>$this->request->data['type_id'],'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']));
        if(!empty($this->request->data['type_id']))
            $condition = array('Product.id=Orderdetail.product_id','Order.id=Orderdetail.transaction_id','Product.productcategory_id=Productcategory.id','Product.producttype_id=Producttype.id','Branch.id=Product.branch_id',
                 'Product.producttype_id'=>$this->request->data['type_id'],'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']));
        if(!empty($this->request->data['category_id']))
            $condition = array('Product.id=Orderdetail.product_id','Order.id=Orderdetail.transaction_id','Product.productcategory_id=Productcategory.id','Product.producttype_id=Producttype.id','Branch.id=Product.branch_id',
                'Product.productcategory_id'=>$this->request->data['category_id'],'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']));



        $product_info=$this->Orderdetail->find(
            'all',array(
                "fields" => array(
                    'Orderdetail.product_id,
                     Orderdetail.id,
                     Orderdetail.branch_id,
                     Orderdetail.product_id,
                     Branch.branchname,
                     Product.productcode,
                     Product.productname,
                     Productcategory.productcategoryname,
                     Producttype.producttypename,
                     Orderdetail.price,
                     Orderdetail.unitprice,
                     Orderdetail.quantity,
                     Orderdetail.transactiontype_id,
                     Orderdetail.transactiondate'
                ),
                "conditions"=>$condition,
                "order"  =>'Orderdetail.transactiondate ASC',
            )
        );
        foreach($product_info as $productId){
            $opening_balance_purchase[] = $this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_opening_price,sum(Orderdetail.quantity) as total_opening_quantity",
                "conditions"=>array('Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Orderdetail.transactiondate <'=>$productId['Orderdetail']['transactiondate'],'Orderdetail.transactiontype_id'=>8)));
            $opening_balance_sales[] = $this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_opening_price,sum(Orderdetail.quantity) as total_opening_quantity",
                "conditions"=>array('Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Orderdetail.transactiondate <'=>$productId['Orderdetail']['transactiondate'],'Orderdetail.transactiontype_id'=>7)));
            //opening balance purchase return start
            $opening_balance_purchase_return[] = $this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_opening_price,sum(Orderdetail.quantity) as total_opening_quantity",
                "conditions"=>array('Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Orderdetail.transactiondate <'=>$productId['Orderdetail']['transactiondate'],'Orderdetail.transactiontype_id'=>13)));
            //opening balance purchase return end
            //opening balance sales return start
            $opening_balance_sales_return[] = $this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_opening_price,sum(Orderdetail.quantity) as total_opening_quantity",
                "conditions"=>array('Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Orderdetail.transactiondate <'=>$productId['Orderdetail']['transactiondate'],'Orderdetail.transactiontype_id'=>12)));
            //opening balance sales return end
            //net change purchase return start
            $net_change_purchase_return[] = $this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_opening_price,sum(Orderdetail.quantity) as total_opening_quantity",
                "conditions"=>array('Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>13)));
            //net change purchase return end
            //net change sales return start
            $net_change_sales_return[] = $this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_opening_price,sum(Orderdetail.quantity) as total_opening_quantity",
                "conditions"=>array('Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>12)));
            //net change sales return end

        }
        $this->set('product_info',$product_info);
        $this->set('opening_balance_purchase',$opening_balance_purchase);
        $this->set('opening_balance_sales',$opening_balance_sales);
        $this->set('opening_balance_purchase_return',$opening_balance_purchase_return);
        $this->set('opening_balance_sales_return',$opening_balance_sales_return);

        $company_info = $this->Company->find('all',array("fields" =>"Company.companyname,Company.companypresentaddress",
            "conditions"=>array('Company.id'=>$_SESSION['User']['company_id'])));
        $this->set('company_info',$company_info);
        $this->set('form_date',$this->request->data['from_date']);
        $this->set('to_date',$this->request->data['to_date']);


    }

    public function inventorywhole(){
        $this->layout='default';
        $this->set('title_for_layout', __('Inventory Whole').' | '.__(Configure::read('site_name')));

        $product_code=$this->Product->find(
            'list',
            array(
                "fields" => array("id","productcode")
            )
        );
        $this->set('product_code',$product_code);

        $product_name=$this->Product->find(
            'list',
            array(
                "fields" => array("id","product_name")
            )
        );
        $this->set('product_name',$product_name);

        $product_category=$this->Productcategory->find(
            'list',
            array(
                "fields" => array("id","productcategory_name")
            )
        );
        $this->set('product_category',$product_category);

        $product_type=$this->Producttype->find(
            'list',
            array(
                "fields" => array("id","producttype_name")
            )
        );
        $this->set('product_type',$product_type);

        $location_code=$this->Branch->find(
            'list',
            array(
                "fields" => array("id","branchcode")
            )
        );
        $this->set('location_code',$location_code);

        $location_name=$this->Branch->find(
            'list',
            array(
                "fields" => array("id","branchname")
            )
        );
        $this->set('location_name',$location_name);

    }

    function inventorywholegenerate(){
        $this->layout = 'ajax';
        $opening_balance_purchase = $opening_balance_sales = $opening_balance_sales_return = $opening_balance_purchase_return = $net_change_purchase_return = $net_change_sales_return= array();
        //$net_change_purchase = array();
        $condition = array('Product.id=Orderdetail.product_id','Order.id=Orderdetail.transaction_id','Product.productcategory_id=Productcategory.id','Product.producttype_id=Producttype.id','Branch.id=Product.branch_id',
            'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']));
        if(!empty($this->request->data['product_id']))
            $condition = array('Product.id=Orderdetail.product_id','Order.id=Orderdetail.transaction_id','Product.productcategory_id=Productcategory.id','Product.producttype_id=Producttype.id','Branch.id=Product.branch_id','Product.id'=>$this->request->data['product_id'],
                'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']));
        if(!empty($this->request->data['branch_id']))
            $condition = array('Product.id=Orderdetail.product_id','Order.id=Orderdetail.transaction_id','Product.productcategory_id=Productcategory.id','Product.producttype_id=Producttype.id','Branch.id=Product.branch_id','Branch.id'=>$this->request->data['branch_id'],
                'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']));
        if(!empty($this->request->data['type_id']) && !empty($this->request->data['category_id']))
            $condition = array('Product.id=Orderdetail.product_id','Order.id=Orderdetail.transaction_id','Product.productcategory_id=Productcategory.id','Product.producttype_id=Producttype.id','Branch.id=Product.branch_id','Branch.id'=>$this->request->data['branch_id'],
                'Product.productcategory_id'=>$this->request->data['category_id'], 'Product.producttype_id'=>$this->request->data['type_id'],'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']));
        $product_info=$this->Orderdetail->find(
            'all',array(
                "fields" => array(
                    'DISTINCT Orderdetail.product_id,
                     Product.productcode,
                     Product.productname,
                     Productcategory.productcategoryname,
                     Producttype.producttypename'
                ),
                "conditions"=>$condition,
                "order"  =>'Orderdetail.transactiondate ASC'
            )
        );
       foreach($product_info as $productId){

            $opening_balance_purchase[] = $this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_opening_price,sum(Orderdetail.quantity) as total_opening_quantity",
                "conditions"=>array('Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Orderdetail.transactiondate <'=>$this->request->data['from_date'],'Orderdetail.transactiontype_id'=>8)));
            $opening_balance_sales[] = $this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_opening_price,sum(Orderdetail.quantity) as total_opening_quantity",
                "conditions"=>array('Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Orderdetail.transactiondate <'=>$this->request->data['from_date'],'Orderdetail.transactiontype_id'=>7)));
            //opening balance purchase return start
            $opening_balance_purchase_return[] = $this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_opening_price,sum(Orderdetail.quantity) as total_opening_quantity",
                "conditions"=>array('Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Orderdetail.transactiondate <'=>$this->request->data['from_date'],'Orderdetail.transactiontype_id'=>13)));
            //opening balance purchase return end
            //opening balance sales return start
           $opening_balance_sales_return[] = $this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_opening_price,sum(Orderdetail.quantity) as total_opening_quantity",
                "conditions"=>array('Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Orderdetail.transactiondate <'=>$this->request->data['from_date'],'Orderdetail.transactiontype_id'=>12)));
            //opening balance sales return end
            //net change purchase return start
            $net_change_purchase_return[] = $this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_opening_price,sum(Orderdetail.quantity) as total_opening_quantity",
                "conditions"=>array('Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>13)));
            //net change purchase return end
            //net change sales return start
            $net_change_sales_return[] = $this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_opening_price,sum(Orderdetail.quantity) as total_opening_quantity",
                "conditions"=>array('Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>12)));
            //net change sales return end
           //net change purchase
           $net_change_purchase[] = $this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_net_change_price,sum(Orderdetail.quantity) as total_net_change_quantity",
               "conditions"=>array('Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>8)));
           //net change sales
           $net_change_sales[] =$this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_net_change_price,sum(Orderdetail.quantity) as total_net_change_quantity",
               "conditions"=>array('Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>7)));

       }
        $this->set('product_info',$product_info);
        $this->set('opening_balance_purchase',$opening_balance_purchase);
        $this->set('opening_balance_sales',$opening_balance_sales);
        $this->set('opening_balance_purchase_return',$opening_balance_purchase_return);
        $this->set('opening_balance_sales_return',$opening_balance_sales_return);
        $this->set('net_change_purchase',$net_change_purchase);
        $this->set('net_change_sales',$net_change_sales);
        $this->set('net_change_purchase_return',$net_change_purchase_return);
        $this->set('net_change_sales_return',$net_change_sales_return);

        $company_info = $this->Company->find('all',array("fields" =>"Company.companyname,Company.companypresentaddress",
            "conditions"=>array('Company.id'=>$_SESSION['User']['company_id'])));
        $this->set('company_info',$company_info);
        $this->set('form_date',$this->request->data['from_date']);
        $this->set('to_date',$this->request->data['to_date']);


    }

    public function itembasedreport(){
        $this->layout='default';
        $this->set('title_for_layout', __('Item Wise Sales Report').' | '.__(Configure::read('site_name')));

        $product_code=$this->Product->find(
            'list',
            array(
                "fields" => array("id","productcode")
            )
        );
        $this->set('product_code',$product_code);

        $product_name=$this->Product->find(
            'list',
            array(
                "fields" => array("id","product_name")
            )
        );
        $this->set('product_name',$product_name);

    }

    function itembasedreportgenerate(){
        $this->layout = 'ajax';
        $array_order =0;
        $opening_balance_purchase = $opening_balance_sales = $opening_balance_sales_return = $opening_balance_purchase_return = array();
        //$net_change_purchase = array();
        $condition = array('Product.id=Orderdetail.product_id','Order.id=Orderdetail.transaction_id','Product.productcategory_id=Productcategory.id','Product.producttype_id=Producttype.id','Branch.id=Product.branch_id',
            'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']));
        if(!empty($this->request->data['product_id']))
            $condition = array('Product.id=Orderdetail.product_id','Order.id=Orderdetail.transaction_id','Product.productcategory_id=Productcategory.id','Product.producttype_id=Producttype.id',
                'Branch.id=Product.branch_id','Product.id'=>$this->request->data['product_id'],'OR' => array(
                    array('Orderdetail.transactiontype_id'=>12),
                    array('Orderdetail.transactiontype_id'=>7)
                ),
                'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']));

         $product_info=$this->Orderdetail->find(
            'all',array(
                "fields" => array(
                    'DISTINCT  Order.client_id,
                     Orderdetail.product_id,
                     Product.productcode,
                     Product.productname,
                     Productcategory.productcategoryname,
                     Producttype.producttypename'
                ),
                "conditions"=>$condition,
                "order"  =>'Orderdetail.transactiondate ASC'
            )
        );

        foreach($product_info as $productId){
            $client[] = $this->User->find('all',array("fields" =>"User.userfirstname,
                                                        User.usermiddlename,
                                                        User.userlastname",
                        "conditions"=>array('User.id'=>$productId['Order']['client_id'])));

            //net change sales
            $net_change_sales[] =$this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_net_change_price,sum(Orderdetail.quantity) as total_net_change_quantity",
                "conditions"=>array('Order.id=Orderdetail.transaction_id','Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Order.client_id'=>$productId['Order']['client_id'],
                    'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>7)));
            //debug($this->Orderdetail->lastQuery());
             //die();
          //net change sales return start
            $net_change_sales_return[] = $this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_opening_price,sum(Orderdetail.quantity) as total_opening_quantity",
                "conditions"=>array('Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Order.client_id'=>$productId['Order']['client_id'],
                    'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>12)));
            //net change sales return end
            $array_order++;
         }

        $category_sell_summary =$this->Orderdetail->find('all',array("fields" =>"Productcategory.productcategoryname, Productcategory.id,sum(Orderdetail.price) as total_net_change_price,sum(Orderdetail.quantity) as total_net_change_quantity",
            "conditions"=>array('Order.id=Orderdetail.transaction_id','Orderdetail.product_id'=>$this->request->data['product_id'],
                'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>7),"group" => array("Productcategory.productcategoryname, Productcategory.id")));

        foreach($category_sell_summary as $categorySell_summary){
            $category_sell_return_summary =$this->Orderdetail->find('all',array("fields" =>"Productcategory.productcategoryname, Productcategory.id,sum(Orderdetail.price) as total_net_change_price,sum(Orderdetail.quantity) as total_net_change_quantity",
                "conditions"=>array('Order.id=Orderdetail.transaction_id','Orderdetail.product_id'=>$this->request->data['product_id'],'Productcategory.id'=>$categorySell_summary["Productcategory"]["id"],
                    'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>12),"group" => array("Productcategory.productcategoryname, Productcategory.id")));
            if(!empty($category_sell_return_summary)){
                foreach($category_sell_return_summary   as $category_sumarry){
                    $net_quantity = $categorySell_summary[0]["total_net_change_quantity"] - $category_sumarry[0]["total_net_change_quantity"];
                    $net_price = $categorySell_summary[0]["total_net_change_price"] - $category_sumarry[0]["total_net_change_price"];
                    $categorySellSumarry[] = array(
                        "category" => $categorySell_summary["Productcategory"]["productcategoryname"],
                        "quantity" => $categorySell_summary[0]["total_net_change_quantity"],
                        "return_quantity" => $category_sumarry[0]["total_net_change_quantity"],
                        "net_quantity" => $net_quantity,
                        "price" => $categorySell_summary[0]["total_net_change_price"],
                        "return_price" => $category_sumarry[0]["total_net_change_price"],
                        "net_price" => $net_price
                    );

                }//foreach($category_sell_return_summary

            }else{
                $categorySellSumarry[] = array(
                    "category" => $categorySell_summary["Productcategory"]["productcategoryname"],
                    "quantity" => $categorySell_summary[0]["total_net_change_quantity"],
                    "return_quantity" => 0,
                    "net_quantity" => $categorySell_summary[0]["total_net_change_quantity"],
                    "price" => $categorySell_summary[0]["total_net_change_price"],
                    "return_price" =>0,
                    "net_price" => $categorySell_summary[0]["total_net_change_price"]
                );

            }

        }

        $this->set('product_info',$product_info);
        $this->set('opening_balance_purchase',$opening_balance_purchase);
        $this->set('opening_balance_sales',$opening_balance_sales);
        $this->set('opening_balance_purchase_return',$opening_balance_purchase_return);
        $this->set('opening_balance_sales_return',$opening_balance_sales_return);
        $this->set('net_change_sales',$net_change_sales);
        $this->set('net_change_sales_return',$net_change_sales_return);
        $this->set('client_info',$client);
        $this->set('category_sell_summary',$categorySellSumarry);

        $company_info = $this->Company->find('all',array("fields" =>"Company.companyname,Company.companypresentaddress",
            "conditions"=>array('Company.id'=>$_SESSION['User']['company_id'])));
        $this->set('company_info',$company_info);
        $this->set('form_date',$this->request->data['from_date']);
        $this->set('to_date',$this->request->data['to_date']);



    }

    public function categorywisereport(){
        $this->layout='default';
        $this->set('title_for_layout', __('Category wise report').' | '.__(Configure::read('site_name')));

        $product_category=$this->Productcategory->find(
            'list',
            array(
                "fields" => array("id","productcategory_name")
            )
        );
        $this->set('product_category',$product_category);


    }

    public function categorywisereportgenerate(){
        $this->layout = 'ajax';
        $array_order =0;
        $net_change_sales_return =  $client = $net_change_sales = array();
        //$net_change_purchase = array();
        if(!empty($this->request->data['category_id']))
            $condition = array('Product.id=Orderdetail.product_id','Order.id=Orderdetail.transaction_id','Product.productcategory_id=Productcategory.id','Product.producttype_id=Producttype.id',
                'Branch.id=Product.branch_id','Product.productcategory_id'=>$this->request->data['category_id'],'OR' => array(
                    array('Orderdetail.transactiontype_id'=>12),
                    array('Orderdetail.transactiontype_id'=>7)
                ),
                'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']));

        $product_info=$this->Orderdetail->find(
            'all',array(
                "fields" => array(
                    'DISTINCT  Order.client_id,
                     Orderdetail.product_id,
                     Product.productcode,
                     Product.productname,
                     Productcategory.productcategoryname,
                     Producttype.producttypename'
                ),
                "conditions"=>$condition,
                "order"  =>'Orderdetail.transactiondate ASC'
            )
        );

        foreach($product_info as $productId){
            $client[] = $this->User->find('all',array("fields" =>"User.userfirstname,
                                                        User.usermiddlename,
                                                        User.userlastname",
                "conditions"=>array('User.id'=>$productId['Order']['client_id'])));
            //net change sales
            $net_change_sales[] =$this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_net_change_price,sum(Orderdetail.quantity) as total_net_change_quantity",
                "conditions"=>array('Order.id=Orderdetail.transaction_id','Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Order.client_id'=>$productId['Order']['client_id'],
                    'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>7)));
            //debug($this->Orderdetail->lastQuery());
            //die();
            //net change sales return start
            $net_change_sales_return[] = $this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_opening_price,sum(Orderdetail.quantity) as total_opening_quantity",
                "conditions"=>array('Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Order.client_id'=>$productId['Order']['client_id'],
                    'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>12)));
            //net change sales return end

        }
        $customer_sell_summary =$this->Orderdetail->find('all',array("fields" =>"Order.client_id,sum(Orderdetail.price) as total_net_change_price,sum(Orderdetail.quantity) as total_net_change_quantity",
            "conditions"=>array('Product.id=Orderdetail.product_id','Order.id=Orderdetail.transaction_id','Product.productcategory_id'=>$this->request->data['category_id'],
                'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>7),"group" => array("Order.client_id")));

        foreach($customer_sell_summary as $customerSell_summary){
            $customer[] = $this->User->find('all',array("fields" =>"User.userfirstname,
                                                        User.usermiddlename,
                                                        User.userlastname",
                "conditions"=>array('User.id'=>$customerSell_summary['Order']['client_id'])));
            $customer_name = $customer[$array_order][0]["User"]["userfirstname"].' '.$customer[$array_order][0]["User"]["usermiddlename"].' '.$customer[$array_order][0]["User"]["userlastname"];
            $customer_sell_return_summary =$this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_net_change_price,sum(Orderdetail.quantity) as total_net_change_quantity",
                "conditions"=>array('Product.id=Orderdetail.product_id','Order.id=Orderdetail.transaction_id','Product.productcategory_id'=>$this->request->data['category_id'],'Order.client_id'=>$customerSell_summary["Order"]["client_id"],
                    'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>12)));
            if(!empty($customer_sell_return_summary)){
                foreach($customer_sell_return_summary   as $customer_summary){
                    $net_quantity = $customerSell_summary[0]["total_net_change_quantity"] - $customer_summary[0]["total_net_change_quantity"];
                    $net_price = $customerSell_summary[0]["total_net_change_price"] - $customer_summary[0]["total_net_change_price"];
                    $customerSellSumarry[] = array(
                        "customer_id" => $customerSell_summary['Order']['client_id'],
                        "customer_name" => $customer_name,
                        "quantity" => $customerSell_summary[0]["total_net_change_quantity"],
                        "return_quantity" => $customer_summary[0]["total_net_change_quantity"],
                        "net_quantity" => $net_quantity,
                        "price" => $customerSell_summary[0]["total_net_change_price"],
                        "return_price" => $customer_summary[0]["total_net_change_price"],
                        "net_price" => $net_price
                    );

                }//foreach($customer_sell_return_summary

            }else{
                $customerSellSumarry[] = array(
                    "customer_id" => $customerSell_summary['Order']['client_id'],
                    "customer_name" => $customer_name,
                    "quantity" => $customerSell_summary[0]["total_net_change_quantity"],
                    "return_quantity" => 0,
                    "net_quantity" => $customerSell_summary[0]["total_net_change_quantity"],
                    "price" => $customerSell_summary[0]["total_net_change_price"],
                    "return_price" =>0,
                    "net_price" => $customerSell_summary[0]["total_net_change_price"]
                );

            }
            $array_order++;

        }

        $this->set('product_info',$product_info);
        $this->set('net_change_sales',$net_change_sales);
        $this->set('net_change_sales_return',$net_change_sales_return);
        $this->set('client_info',$client);
        $this->set('sell_summary',$customerSellSumarry);
        $company_info = $this->Company->find('all',array("fields" =>"Company.companyname,Company.companypresentaddress",
            "conditions"=>array('Company.id'=>$_SESSION['User']['company_id'])));
        $this->set('company_info',$company_info);
        $this->set('form_date',$this->request->data['from_date']);
        $this->set('to_date',$this->request->data['to_date']);


    }

    public function representativewisereport(){
        $this->layout='default';
        $this->set('title_for_layout', __('Representative wise report').' | '.__(Configure::read('site_name')));
        $group_id=$_SESSION["User"]["group_id"];
        $company_id=$_SESSION["User"]["company_id"];
        $branch_id=$_SESSION["User"]["branch_id"];
        $salesmanlist_id = array();
        $categorySellSumarry = array();

        if($group_id==2)
            $salesmanlist_id[] = array('User.company_id'=> $company_id);
        else{
            $salesmanlist_id[] = array('User.company_id'=> $company_id);
            $salesmanlist_id[] = array('User.branch_id'=> $branch_id);

        }

        $salesmanlist_id[] = array('User.group_id'=> 5);
        $representative_name_list = $this->User->find(
            'list',
            array(
                "fields" => array("User.id","user_fullname"),
                "conditions" =>$salesmanlist_id
            )
        );
        $this->set('representative_name_list',$representative_name_list);

        $representative_id_list = $this->User->find(
            'list',
            array(
                "fields" => array("User.id","User.id"),
                "conditions" =>$salesmanlist_id
            )
        );
        $this->set('representative_id_list',$representative_id_list);


    }

    public function representativewisereportgenerate(){
        $this->layout = 'ajax';
        $net_quantity = $net_price = 0;
        $net_change_sales_return =  $client = $net_change_sales = array();
        //$net_change_purchase = array();
        if(!empty($this->request->data['representative_id']))
            $condition = array('Product.id=Orderdetail.product_id','Order.id=Orderdetail.transaction_id','Product.productcategory_id=Productcategory.id','Product.producttype_id=Producttype.id',
                'Branch.id=Product.branch_id','Order.salesman_id'=>$this->request->data['representative_id'],'OR' => array(
                    array('Orderdetail.transactiontype_id'=>12),
                    array('Orderdetail.transactiontype_id'=>7)
                ),
                'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']));

        $product_info=$this->Orderdetail->find(
            'all',array(
                "fields" => array(
                    'DISTINCT  Order.client_id,
                     Orderdetail.product_id,
                     Product.productcode,
                     Product.productname,
                     Productcategory.productcategoryname,
                     Producttype.producttypename'
                ),
                "conditions"=>$condition,
                "order"  =>'Orderdetail.transactiondate ASC'
            )
        );

        foreach($product_info as $productId){
            $client[] = $this->User->find('all',array("fields" =>"User.userfirstname,
                                                        User.usermiddlename,
                                                        User.userlastname",
                "conditions"=>array('User.id'=>$productId['Order']['client_id'])));
            //net change sales
            $net_change_sales[] =$this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_net_change_price,sum(Orderdetail.quantity) as total_net_change_quantity",
                "conditions"=>array('Order.id=Orderdetail.transaction_id','Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Order.client_id'=>$productId['Order']['client_id'],
                    'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>7)));
            //debug($this->Orderdetail->lastQuery());
            //die();
            //net change sales return start
            $net_change_sales_return[] = $this->Orderdetail->find('all',array("fields" =>"sum(Orderdetail.price) as total_opening_price,sum(Orderdetail.quantity) as total_opening_quantity",
                "conditions"=>array('Orderdetail.product_id'=>$productId['Orderdetail']['product_id'],'Order.client_id'=>$productId['Order']['client_id'],
                    'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>12)));
            //net change sales return end

        }
       $category_sell_summary =$this->Orderdetail->find('all',array("fields" =>"Productcategory.productcategoryname, Productcategory.id,sum(Orderdetail.price) as total_net_change_price,sum(Orderdetail.quantity) as total_net_change_quantity",
            "conditions"=>array('Order.id=Orderdetail.transaction_id','Order.salesman_id'=>$this->request->data['representative_id'],
                'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>7),"group" => array("Productcategory.productcategoryname, Productcategory.id")));

        foreach($category_sell_summary as $categorySell_summary){
           $category_sell_return_summary =$this->Orderdetail->find('all',array("fields" =>"Productcategory.productcategoryname, Productcategory.id,sum(Orderdetail.price) as total_net_change_price,sum(Orderdetail.quantity) as total_net_change_quantity",
                    "conditions"=>array('Order.id=Orderdetail.transaction_id','Order.salesman_id'=>$this->request->data['representative_id'],'Productcategory.id'=>$categorySell_summary["Productcategory"]["id"],
                    'Orderdetail.transactiondate BETWEEN ? and ?' => array($this->request->data['from_date'],$this->request->data['to_date']),'Orderdetail.transactiontype_id'=>12),"group" => array("Productcategory.productcategoryname, Productcategory.id")));
            if(!empty($category_sell_return_summary)){
                foreach($category_sell_return_summary   as $category_sumarry){
                    $net_quantity = $categorySell_summary[0]["total_net_change_quantity"] - $category_sumarry[0]["total_net_change_quantity"];
                    $net_price = $categorySell_summary[0]["total_net_change_price"] - $category_sumarry[0]["total_net_change_price"];
                    $categorySellSumarry[] = array(
                        "category" => $categorySell_summary["Productcategory"]["productcategoryname"],
                        "quantity" => $categorySell_summary[0]["total_net_change_quantity"],
                        "return_quantity" => $category_sumarry[0]["total_net_change_quantity"],
                        "net_quantity" => $net_quantity,
                        "price" => $categorySell_summary[0]["total_net_change_price"],
                        "return_price" => $category_sumarry[0]["total_net_change_price"],
                        "net_price" => $net_price
                    );

                }//foreach($category_sell_return_summary

            }else{
                $categorySellSumarry[] = array(
                    "category" => $categorySell_summary["Productcategory"]["productcategoryname"],
                    "quantity" => $categorySell_summary[0]["total_net_change_quantity"],
                    "return_quantity" => 0,
                    "net_quantity" => $categorySell_summary[0]["total_net_change_quantity"],
                    "price" => $categorySell_summary[0]["total_net_change_price"],
                    "return_price" =>0,
                    "net_price" => $categorySell_summary[0]["total_net_change_price"]
                );

            }

         }

        $this->set('product_info',$product_info);
        $this->set('net_change_sales',$net_change_sales);
        $this->set('net_change_sales_return',$net_change_sales_return);
        $this->set('client_info',$client);
        $this->set('category_sell_summary',$categorySellSumarry);

        $company_info = $this->Company->find('all',array("fields" =>"Company.companyname,Company.companypresentaddress",
            "conditions"=>array('Company.id'=>$_SESSION['User']['company_id'])));
        $this->set('company_info',$company_info);
        $this->set('form_date',$this->request->data['from_date']);
        $this->set('to_date',$this->request->data['to_date']);


    }


}
