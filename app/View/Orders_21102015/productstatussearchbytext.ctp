<?php
	echo $Utilitys->paginationcommon();
		echo $Utilitys->tablestart($class=null, $id=null);
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Product')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Total Quantity')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Total Amount')  => array('class' => 'highlight sortable')
					)
				)
			);
			$productrows = array();
			$countProductstatus = 0-1+$this->Paginator->counter('%start%');
			foreach($orderdetail as $Thisorderdetail):
				$countProductstatus++;
				$productrows[]= array($countProductstatus,$Thisorderdetail["Orderdetail"]["orderdetail_productname"],array($Thisorderdetail[0]["TotalQuantity"],'colspan="1" align="right"'),array($Thisorderdetail[0]["TotalAmount"],'colspan="1" align="right"'));
			endforeach;
			echo $this->Html->tableCells(
				$productrows
			);
		echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var OrderdetailBranchId = $('#OrderdetailBranchId').val();
					var Searchtext = $('#OrderdetailSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext, OrderdetailBranchId:OrderdetailBranchId},
						async: true,
						beforeSend: function(){
							$(\"#productstatussarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#productstatussarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>