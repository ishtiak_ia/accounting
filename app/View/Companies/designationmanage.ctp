<?php
//pr($product);
	echo "<h2>".__('Designation'). $Utilitys->addurl($title=__("Add New Designation"), $this->request['controller'], $page="designation")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
		"Designation.group_id",
		array(
			'type'=>'select',
			"options"=>array($group),
			'empty'=>__('Select Group'),
			'div'=>false,
			'tabindex'=>1,
			'label'=>false,
			'class'=> 'form-control',
			'style'=>''
			)
		)
		.$this->Form->input(
			"Designation.company_id",
			array(
				'type'=>'select',
				"options"=>array($company),
				'class'=> 'form-control',
				'empty'=>__('Select Company'),
				'div'=>false,
				'tabindex'=>2,
				'label'=>false,
				'style'=>''
				)
			)
		.$this->Form->input(
			"Designation.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'placeholder' => __("Search Word"),
				'label' => false,
				'tabindex'=>5,
				'class'=> 'form-control'
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"designationsarchloading\">
	";
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
		echo $this->Html->tableHeaders(
			array(
				__('SL#'),
				array(
					__('Designation Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Designation Name BN')  => array('class' => 'highlight sortable')
				),
				array(
					__('Company')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Group')  => array('class' => 'highlight sortable')
				),
				array(
					__('Is Parent')  => array('class' => 'highlight sortable')
				),
				array(
					__('Is Active')  => array('class' => 'highlight sortable')
				),
				__('Action')
			)
		);
		$designationrows = array();
		$countDesignation = 0-1+$this->Paginator->counter('%start%');
		
		foreach($designations as $Thisdesignation):
			$countDesignation++;
			$designationrows[]= array($countDesignation,$Thisdesignation["Designation"]["designationname"],$Thisdesignation["Designation"]["designationnamebn"],$Thisdesignation["Designation"]["company_fullname"],$Thisdesignation["Designation"]["group_name"],$Thisdesignation["Designation"]["isParent"],$Utilitys->statusURL($this->request["controller"], 'designation', $Thisdesignation["Designation"]["id"], $Thisdesignation["Designation"]["designationuuid"], $Thisdesignation["Designation"]["designationisactive"]),$Utilitys->cusurl($this->request['controller'], 'designation', $Thisdesignation["Designation"]["id"], $Thisdesignation["Designation"]["designationuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$designationrows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();

	echo"
				</div>
		<script>
			$(document).ready(function() {
				$(\"#DesignationSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#designationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."companies/designationsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#designationsarchloading\").html(result);
						}
					);
				});
				$(\"#DesignationGroupId\").change(function(){
					var DesignationGroupId = $(\"#DesignationGroupId\").val();
					var DesignationCompanyId = $(\"#DesignationCompanyId\").val();
					$(\"#designationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."companies/designationsearchbytext',
						{DesignationGroupId:DesignationGroupId,DesignationCompanyId:DesignationCompanyId},
						function(result) {
							$(\"#designationsarchloading\").html(result);
						}
					);
				});
				$(\"#DesignationCompanyId\").change(function(){
					var DesignationGroupId = $(\"#DesignationGroupId\").val();
					var DesignationCompanyId = $(\"#DesignationCompanyId\").val();
					$(\"#designationsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."companies/designationsearchbytext',
						{DesignationGroupId:DesignationGroupId, DesignationCompanyId:DesignationCompanyId},
						function(result) {
							$(\"#designationsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>