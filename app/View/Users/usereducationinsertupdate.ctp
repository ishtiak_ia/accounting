<?php
echo $this->Form->create('Users', array('action' => 'usereducationinsertupdateaction', 'type' => 'file'));
	echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$usereducation[0]["Usereducation"]['id']));
	echo $this->Form->input('usereducationuuid', array('type'=>'hidden', 'value'=>@$usereducation[0]["Usereducation"]['usereducationuuid']?$usereducation[0]["Usereducation"]['usereducationuuid']:0, 'class'=>''));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'tabindex'=>10,
			'value' => (@$usereducation[0]['Usereducation']['usereducationisactive'] ? $usereducation[0]['Usereducation']['usereducationisactive'] : 0),
			'class' => 'form-inline'
		);
	echo"<h3>".__("Add Academic Qualification")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-6\">";
			echo $this->Form->input(
				"Usereducation.department_id",
				array(
					'type'=>'select',
					"options"=>array($department_list),
					'empty'=>__('Select Department'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>1,
					'label'=>__('Select Department&nbsp;<span style=\'color:#cc0000\'>*</span>'),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style'=>'',
					"selected" => (@$usereducation[0]['Usereducation']['department_id'] ? $usereducation[0]['Usereducation']['department_id'] : 0)
				)
			);
			echo "<div class=\"form-group\" id=\"usereducationchangediv\">";
				echo $this->Form->input(
					"Usereducation.user_id",
					array(
						'type'=>'select',
						"options"=>array($user_list),
						'empty'=>__('Select Employee'),
						'div'=>array('class'=>'form-group'),
						'tabindex'=>1,
						'label'=>__('Select Employee&nbsp;<span style=\'color:#cc0000\'>*</span>'),
						'class'=> 'form-control',
						'data-validation-engine'=>'validate[required]',
						'style'=>'',
						"selected" => (@$usereducation[0]['Usereducation']['user_id'] ? $usereducation[0]['Usereducation']['user_id'] : 0)
					)
				);
				echo "</div>";
			echo"<div class=\"row\" id=\"content\">";
				echo"<div class=\"col-md-11\" id=\"mydivcontent1\">";
					echo"<div class=\"row\">";
						echo"<div class=\"col-md-11\">";
echo $this->Form->input(
								"usereducationdegreename1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label' => __('Exam/Degree Title&nbsp;<span style=\'color:#cc0000\'>*</span>'),
									'tabindex'=>2,
									'placeholder' => __("Exam/Degree Title"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$usereducation[0]['Usereducation']['usereducationdegreename']
								)
							);
							echo $this->Form->input(
								"usereducationgroup1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label' => __('Concentration/Major/Group&nbsp;<span style=\'color:#cc0000\'>*</span>'),
									'tabindex'=>3,
									'placeholder' => __("Concentration/Major/Group"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$usereducation[0]['Usereducation']['usereducationgroup']
								)
							);
							echo $this->Form->input("usereducationinstitute1", array(
					            	'type' => 'text', 
					            	'div' => array('class'=>'form-group'), 
					            	'label'=> __('Institute Name&nbsp;<span style=\'color:#cc0000\'>*</span>'), 
					            	'tabindex'=>4,
					            	'style' => '',
					            	'placeholder' => __("Institute Name"),
					            	'data-validation-engine'=>'validate[required]',
					            	'value' =>(@$usereducation[0]['Usereducation']['usereducationinstitute']),
									'class'=> 'form-control'
					        	)
					        );
							echo $this->Form->input(
								"usereducationpassing1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label' => __('Year of Passing&nbsp;<span style=\'color:#cc0000\'>*</span>'),
									'tabindex'=>5,
									'placeholder' => __("Year of Passing"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$usereducation[0]['Usereducation']['usereducationpassing']
								)
							);
							echo $this->Form->input(
								"usereducationboard1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label' => __('Education Board'),
									'tabindex'=>6,
									'placeholder' => __("Education Board"),
									'class'=> 'form-control',
									'style' => '',
									'value' => @$usereducation[0]['Usereducation']['usereducationboard']
								)
							);
							echo $this->Form->input(
								"usereducationduration1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label' => __('Duration&nbsp;<span style=\'color:#cc0000\'>*</span>'),
									'tabindex'=>7,
									'placeholder' => __("Duration"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$usereducation[0]['Usereducation']['usereducationduration']
								)
							);
							echo $this->Form->input(
								"usereducationresult1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label' => __('Result&nbsp;<span style=\'color:#cc0000\'>*</span>'),
									'tabindex'=>8,
									'placeholder' => __("Result"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$usereducation[0]['Usereducation']['usereducationresult']
								)
							);
							echo $this->Form->input(
								"usereducationachievement1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label' => __('Achievement'),
									'tabindex'=>9,
									'placeholder' => __("Achievement"),
									'class'=> 'form-control',
									'style' => '',
									'value' => @$usereducation[0]['Usereducation']['usereducationachievement']
								)
							);
							echo"<div class=\"form-group\">";
								echo"<label>isActive?</label><br>";
								echo $this->Form->radio(
									'usereducationisactive1', 
									$options,
									$attributes
								);
							echo"</div>";	
						echo"</div>";								
					echo"</div>";		
				echo"</div>";
			echo"</div>";
				echo"<div class=\"row\">";
					echo"<div class=\"col-md-11\">";
						echo"<input type=\"button\" id=\"more_fields\" onclick=\"add_fields();\" value=\"Add More\" />";
						echo"<input type=\"button\" id=\"remove_fields\" onclick=\"remove_field();\" value=\"Remove More\" />";
						echo"<input type=\"hidden\" style=\"width:48px;\" name=\"totlarow\" id=\"totlarow\" value=\"1\" /> ";
						echo"
							<div id=\"locationsarchloading\">

							</div>
						";
						echo "<br />";
					echo $Utilitys->allformbutton('',$this->request["controller"],$page='usereducation');
				echo"</div>";
			echo"</div>";
		echo"</div>";	
	echo"</div>";
echo $this->Form->end();
echo"
<script>
	var room=0;
		function add_fields() {
			room = parseInt(document.getElementById(\"totlarow\").value);		
			room = room+1;
					
			$(\"#content\").append('<div class=\"col-md-11\" id=\"mydivcontent'+room+'\"><div class=\"row\"><div class=\"col-md-11\"><div class=\"form-group\"><label for=\"UsersUsereducationdegreename'+room+'\">Exam/Degree Title</label><input name=\"data[Users][usereducationdegreename'+room+']\" tabindex=\"2\" placeholder=\"Exam/Degree Title\" class=\"form-control\" data-validation-engine=\"validate[required]\" style=\"\" type=\"text\" id=\"UsersUsereducationdegreename'+room+'\"></div><div class=\"form-group\"><label for=\"UsersUsereducationgroup'+room+'\">Concentration/Major/Group</label><input name=\"data[Users][usereducationgroup'+room+']\" tabindex=\"3\" placeholder=\"Concentration/Major/Group\" class=\"form-control\" data-validation-engine=\"validate[required]\" style=\"\" type=\"text\" id=\"UsersUsereducationgroup'+room+'\"></div><div class=\"form-group\"><label for=\"UsersUsereducationinstitute'+room+'\">Institute Name</label><input name=\"data[Users][usereducationinstitute'+room+']\" tabindex=\"4\" style=\"\" placeholder=\"Institute Name\" data-validation-engine=\"validate[required]\" class=\"form-control\" type=\"text\" id=\"UsersUsereducationinstitute'+room+'\"></div><div class=\"form-group\"><label for=\"UsersUsereducationpassing'+room+'\">Year of Passing</label><input name=\"data[Users][usereducationpassing'+room+']\" tabindex=\"5\" placeholder=\"Year of Passing\" class=\"form-control\" data-validation-engine=\"validate[required]\" style=\"\" type=\"text\" id=\"UsersUsereducationpassing'+room+'\"></div><div class=\"form-group\"><label for=\"UsersUsereducationboard'+room+'\">Education Board</label><input name=\"data[Users][usereducationboard'+room+']\" tabindex=\"6\" placeholder=\"Education Board\" class=\"form-control\" style=\"\" type=\"text\" id=\"UsersUsereducationboard'+room+'\"></div><div class=\"form-group\"><label for=\"UsersUsereducationduration'+room+'\">Duration</label><input name=\"data[Users][usereducationduration'+room+']\" tabindex=\"7\" placeholder=\"Duration\" class=\"form-control\" data-validation-engine=\"validate[required]\" style=\"\" type=\"text\" id=\"UsersUsereducationduration'+room+'\"></div><div class=\"form-group\"><label for=\"UsersUsereducationresult'+room+'\">Result</label><input name=\"data[Users][usereducationresult'+room+']\" tabindex=\"8\" placeholder=\"Result\" class=\"form-control\" data-validation-engine=\"validate[required]\" style=\"\" type=\"text\" id=\"UsersUsereducationresult'+room+'\"></div><div class=\"form-group\"><label for=\"UsersUsereducationachievement'+room+'\">Achievement</label><input name=\"data[Users][usereducationachievement'+room+']\" tabindex=\"9\" placeholder=\"Achievement\" class=\"form-control\" style=\"\" type=\"text\" id=\"UsersUsereducationachievement'+room+'\"></div><div class=\"form-group\"><label>isActive?</label><br><input type=\"radio\" name=\"data[Users][usereducationisactive'+room+']\" id=\"UsersUsereducationisactive'+room+'\" tabindex=\"10\" value=\"1\" class=\"form-inline\"><label for=\"UsersUsereducationisactive'+room+'\">Yes</label><input type=\"radio\" name=\"data[Users][usereducationisactive'+room+']\" id=\"UsersUsereducationisactive'+room+'\" tabindex=\"10\" value=\"0\" class=\"form-inline\" checked=\"checked\"><label for=\"UsersUsereducationisactive'+room+'\">No</label></div></div></div></div');
			document.getElementById(\"totlarow\").value=room;

		}
		function remove_field(){  
			room = document.getElementById(\"totlarow\").value;
			var child = document.getElementById('mydivcontent'+room);
			var parent = document.getElementById('content');
			parent.removeChild(child);
			room = room-1;
			document.getElementById(\"totlarow\").value=room;
		}
	$(document).ready(function() {
		$(\"#UsersUsereducationinsertupdateactionForm\").validationEngine();
	});
	$(\"#UsereducationDepartmentId\").change(function(){
		var UsereducationDepartmentId = $(this).val();
		$(\"#usereducationchangediv\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
		$.post(
			'".$this->webroot."users/usereducationlistbydepartmentid',
			{UsereducationDepartmentId:UsereducationDepartmentId},
			function(result) {
				$(\"#usereducationchangediv\").html(result);
			}
		);
	});
</script>
	";
?>