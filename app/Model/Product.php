<?php
App::uses('AppModel', 'Model');
class Product extends AppModel {
	public $name = 'Product';
	public $usetables = 'products';

	var $belongsTo = array(
		'Productcategory' => array(
			'fields' =>array('productcategoryname', 'productcategorynamebn'),
			'className'    => 'Productcategory',
			'foreignKey'    => 'productcategory_id'
		),
		'Producttype' => array(
			'fields' =>array('producttypename', 'producttypenamebn'),
			'className'    => 'Producttype',
			'foreignKey'    => 'producttype_id'
		),
		'Company' => array(
			'fields' =>array('companyname', 'companynamebn'),
			'className'    => 'Company',
			'foreignKey'    => 'company_id'
		),
		'Branch' => array(
			'fields' =>array('branchname', 'branchnamebn'),
			'className'    => 'Branch',
			'foreignKey'    => 'branch_id'
		),
		/*'Coa' => array(
			'fields' =>array('Coa.coacode', 'Coa.coaname', 'Coa.coanamebn'),
			'className'    => 'Coa',
			'foreignKey'    => 'coa_id'
		),*/
		'Unit' => array(
			'fields' =>array('unitname', 'unitnamebn'),
			'className'    => 'Unit',
			'foreignKey'    => 'unit_id'
		)
	);
	var $virtualFields = array(
		'productcategory_name' => 'CONCAT(Productcategory.productcategoryname, " / ", Productcategory.productcategorynamebn)',
		'producttype_name' => 'CONCAT(Producttype.producttypename, " / ", Producttype.producttypenamebn)',
		'unit_name' => 'CONCAT(Unit.unitname, " / ", Unit.unitnamebn)',
		//'coa_name' => 'CONCAT(Coa.coaname, " / ", Coa.coanamebn)',
		'company_name' => 'CONCAT(Company.companyname, " / ", Company.companynamebn)',
		'product_name' => 'CONCAT(Product.productname, " / ", Product.productnamebn)',
		'product_totalquantity' => 'CONCAT((SELECT SUM(quantity) FROM orderdetails WHERE product_id = Product.id AND branch_id = Product.branch_id)," ",  Unit.unitname)',
		'product_totalquantity_bn' => 'CONCAT((SELECT SUM(quantity) FROM orderdetails WHERE product_id = Product.id AND branch_id = Product.branch_id)," ", Unit.unitnamebn )',
		/*'product_totalquantity' => 'CONCAT((SELECT IF(Unit.unitmon = 1, SUM(orderdetailmon), IF(Unit.unitkilogram = 1, SUM(orderdetailkg), IF(Unit.unitgram = 1, SUM(orderdetailgram), SUM(quantity)))) FROM orderdetails WHERE product_id = Product.id AND branch_id = Product.branch_id)," ",  Unit.unitname)',
		'product_totalquantity_bn' => 'CONCAT((SELECT IF(Unit.unitmon = 1, SUM(orderdetailmon), IF(Unit.unitkilogram = 1, SUM(orderdetailkg), IF(Unit.unitgram = 1, SUM(orderdetailgram), SUM(quantity)))) FROM orderdetails WHERE product_id = Product.id AND branch_id = Product.branch_id)," ", Unit.unitnamebn )',*/
		'format' => 'IF(Product.productformat = 1, "<span class=\"green\">High</span>", "<span class=\"red\">KG</span>")',
		'isActive' => 'IF(Product.productisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Product.productisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
		//'isActive' => 'IF(User.userisactive = 0, "<span class=\"orange\">Inactive</span>", IF(User.userisactive = 1, "<span class=\"green\">Active</span>", "<span class=\"red\">Deleted</span>"))'
	);
}