<script>
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })

    var max_fields1      = 10; //maximum input boxes allowed
    var wrapper1         = $(".input_fields_wrap1"); //Fields wrapper
    var add_button1      = $(".add_field_button1"); //Add button ID
   
    var y = 1; //initlal text box count
    $(add_button1).click(function(e){ //on add input button click
        e.preventDefault();
        if(y < max_fields1){ //max input box allowed
            y++; //text box increment
            $(wrapper1).append('<div><input type="text" name="mytext1[]"/><a href="#" class="remove_field1">Remove</a></div>'); //add input box
        }
    });
   
    $(wrapper1).on("click",".remove_field1", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })


    
});
</script>
<?php echo $this->Form->create('Companies', array('action' => 'companyinsertupdateaction', 'type' => 'file'));
       echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$company[0]['Company']['id']));
echo $this->Form->input('companyuuid', array('type'=>'hidden', 'value'=>@$company[0]['Company']['companyuuid'])); 
 ?>
<h3>Add Company Information</h3>
<?php echo $Utilitys->tablestart($class=null, $id=null);?>
        <tr>
            <!-- <td><?php //echo __(Name (English):);?></td> -->
            <td>Name (English):</td>
            <td><?php echo $this->Form->input("Company.companyname", array('type' => 'text', 'div' => false, 'label' => false, 'data-validation-engine'=>'validate[required]', 'value'=>@$company[0]['Company']['companyname']));
            $options = array(1 => __("Yes"), 0 =>__("No"));
            $attributes = array(
            'legend' => false, 
            'label' => true, 
            'value' => (@$company[0]['Company']['companyisactive'] ? $company[0]['Company']['companyisactive'] : 0), 
            'class' => 'form-inline'
        ); ?>

            </td>
        </tr>
        <tr>
            <td>Name (Bangla):</td>
            <td><?php echo $this->Form->input("Company.companynamebn", array('type' => 'text', 'div' => false, 'label' => false, 'data-validation-engine'=>'validate[required]', 'value'=>@$company[0]['Company']['companynamebn'])); ?>

            </td>
        </tr>
        <tr>
            <td>Address (present):</td>
            <td><?php echo $this->Form->input("Company.companypresentaddress", array('type' => 'text', 'div' => false, 'label' => false, 'data-validation-engine'=>'validate[required]', 'value'=>@$company[0]['Company']['companypresentaddress'])); ?>
            </td>
        </tr>
        <tr>
            <td>Address (permanent):</td>
            <td><?php echo $this->Form->input("Company.companypermanentaddress", array('type' => 'text', 'div' => false, 'label' => false, 'data-validation-engine'=>'validate[required]', 'value'=>@$company[0]['Company']['companypermanentaddress'])); ?>
            </td>
        </tr>
        <tr>
            <td>Is Active:</td>
            <td ><?php
                echo $this->Form->radio('Company.companyisactive', $options, $attributes);
                ?>
            </td>
        </tr>
        <tr>
            <td>Email Address:</td>
            <td>
                <div class="input_fields_wrap">
            <?php 
            if (isset($companyemail)){
                foreach ($companyemail as $thiscompanyemail) {
                    echo"
                        <div><input type=\"text\" name=\"mytext[]\" value=\"".$thiscompanyemail."\"><a href=\"#\" class=\"remove_field\">Remove</a></div>

                    ";
                }
                if($companyemail == NULL){?>
                <div><input type="text" name="mytext[]"></div>
          <?php  }
      }
            else{ ?>
                <div><input type="text" name="mytext[]"></div>
           <?php  }
            ?>
                
                </div>
                    <button class="add_field_button">Add More Fields</button>
            </td>
        </tr>

        <tr>
            <td>Phone:</td>
            <td>
                <div class="input_fields_wrap1">
            <?php 
             if (isset($companyphone)){
                foreach ($companyphone as $thiscompanyphone) {
                    echo"
                        <div><input type=\"text\" name=\"mytext1[]\" value=\"".$thiscompanyphone."\"><a href=\"#\" class=\"remove_field1\">Remove</a></div>

                    ";
                }
                if($companyphone == NULL){ ?>
                 <div><input type="text" name="mytext1[]"></div>
            <?php  }
            }
            else{
            ?>
             <div><input type="text" name="mytext1[]"></div>
             <?php }?>     
                
                </div>
                    <button class="add_field_button1">Add More Fields</button>
            </td>
        </tr>

         <!-- <tr>
            <td>Phone:</td>
            <td>
                <div class="input_fields_wrap1">
                    <div><input type="text" name="mytext1[]"></div>
                    <button class="add_field_button1">Add More Fields</button>
                    
                </div>
            </td>
        </tr> -->
        <tr>
            <td>Logo:</td>
            <td id="area"><?php echo $this->Form->file('Company.photo1',array('label'=>false,'div'=>false));  ?>
                 <input type="hidden" name="data[Company][photo2]" value="<?php echo @$company[0]['Company']['companylogo'];?>"  />
            </td>
        </tr>
        
        
        <tr>
            <td>&nbsp;</td>
            <td><?php echo $Utilitys->allformbutton('',$this->request["controller"],$page='company'); ?>
            </td>
        </tr>
<?php echo $Utilitys->tableend();?>
        <?php print $this->Form->end(); ?>   

<script type="text/javascript">
 jQuery(document).ready(function(){
    jQuery("#CompaniesCompanyinsertupdateactionForm").validationEngine();
 });
</script>         
