<?php
App::uses('AppModel', 'Model');
class Order extends AppModel {
	public $name = 'Order';
	public $usetables = 'orders';

	var $belongsTo = array(
		'Company' => array(
			'fields' =>array('companyname', 'companynamebn'),
			'className'    => 'Company',
			'foreignKey'    => 'company_id'
		),
		'Branch' => array(
			'fields' =>array('branchname', 'branchnamebn'),
			'className'    => 'Branch',
			'foreignKey'    => 'branch_id'
		),
		'User' => array(
			'fields' =>array('User.userfirstname', 'User.usermiddlename', 'User.userlastname'),
			'className'	=> 'User',
			'foreignKey'	=> false,
			'conditions'	=> 'Order.user_id = User.id'
		),
		'Client' => array(
			'fields' =>array('User.userfirstname', 'User.usermiddlename', 'User.userlastname'),
			'className'    => 'User',
			'foreignKey'	=> false,
			'conditions'	=> 'Order.client_id = Client.id'
		),
		'Salesman' => array(
			'fields' =>array('User.userfirstname', 'User.usermiddlename', 'User.userlastname'),
			'className'    => 'User',
			'foreignKey'	=> false,
			'conditions'	=> 'Order.salesman_id = Salesman.id'
		),
		'Coa' => array(
			'fields' =>array('Coa.coacode', 'Coa.coaname', 'Coa.coanamebn'),
			'className'    => 'Coa',
			'foreignKey'    => 'coa_id'
		),
		'Transactiontype' => array(
			'fields' =>array('Transactiontype.transactiontypename', 'Transactiontype.transactiontypenamebn'),
			'className'    => 'Transactiontype',
			'foreignKey'    => 'transactiontype_id'
		)
	);
	var $virtualFields = array(
		'order_date' => 'DATE(Order.orderdate)',
		'order_duedate' => 'DATE(Order.orderduepaymentdate)',
		'order_particulars' =>  'CONCAT(
			IF(Order.transactiontype_id=0, "", Transactiontype.transactiontypename), 
			IF(Order.client_id=0, "", CONCAT("/", Client.userfirstname, " ", Client.usermiddlename, " ", Client.userlastname)),
			IF(Order.coa_id=0, " ", CONCAT("/", Coa.coaname)),
			IF(Order.salesman_id=0, "", CONCAT("/", Salesman.userfirstname, " ", Salesman.usermiddlename, " ", Salesman.userlastname))
		)',
		'order_amount' 	=> 'ROUND(Order.ordertotal, 2)',
		'order_discountamount' => 'ROUND(Order.orderdiscount, 2)',
		'order_amountafterdiscount' => 'ROUND(Order.transactionamount, 2)',
		'order_paymentamount' => 'ROUND(Order.orderpayment, 2)',
		'order_dueamount' => 'ROUND(Order.orderdue, 2)',
		'order_amount' 	=> 'ROUND(Order.ordertotal, 2)',
		'order_balance' => 'IF(Order.transactionamount=0, "0.00", ROUND(Order.transactionamount, 2))',
		'order_withdraw' => 'IF(Order.transactionamount<0,ROUND(Order.transactionamount, 2), "0.00")',
		'order_deposit' => 'IF(Order.transactionamount>0,ROUND(Order.transactionamount, 2), "0.00")',
//		'order_chequenumber' =>'CONCAT( Banktransaction.banktransactionchequebookseries, " - ", Banktransaction.banktransactionchequesequencynumber )',
		'isActive' => 'IF(Order.orderisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Order.orderisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
	
}