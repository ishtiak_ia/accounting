<?php
	echo "<h2>".__('Subject'). $Utilitys->addurl($title=__("Add New Subject"), $this->request['controller'], $page="subject")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Subject.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"subjectsearchloading\">
	";
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Subject Name')  => array('class' => 'highlight sortable')
					),
					array(
						__('Subject Name BNG')  => array('class' => 'highlight sortable')
					),
					__('Active'), 
					__('Action')
				)
			);
			$subjectrows = array();
			$countSubject = 0-1+$this->Paginator->counter('%start%');
			foreach($subject as $Thissubject):
				$countSubject++;
				$subjectrows[]= array($countSubject,$Thissubject["Subject"]["subjectname"],$Thissubject["Subject"]["subjectnamebn"],$Utilitys->statusURL($this->request["controller"], 'subject', $Thissubject["Subject"]["id"], $Thissubject["Subject"]["subjectuuid"], $Thissubject["Subject"]["subjectisactive"]),$Utilitys->cusurl($this->request["controller"], 'subject', $Thissubject["Subject"]["id"], $Thissubject["Subject"]["subjectuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$subjectrows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();

	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				$(\"#SubjectSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#subjectsearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/subjectsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#subjectsearchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>