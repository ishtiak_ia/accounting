<?php
	echo "<h2>".__('Bank Cheque Book'). $Utilitys->addurl($title=__("Add New Bank Cheque Book"), $controller="banks", $page="bankchequebook")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Bankchequebook.bank_id",
			array(
				'type'=>'select',
				"options"=>array($bank),
				'empty'=>__('Select Bank'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
			)
		)
		.$this->Form->input(
			"Bankchequebook.bankbranch_id",
			array(
				'type'=>'select',
				"options"=>array($bankbranch),
				'empty'=>__('Select Branch'),
				'div'=>false,
				'tabindex'=>2,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
			)
		)
		.$this->Form->input(
			"Bankchequebook.bankaccounttype_id",
			array(
				'type'=>'select',
				"options"=>array($bankaccounttype),
				'empty'=>__('Select Account Type'),
				'div'=>false,
				'tabindex'=>3,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
			)
		)
		.$this->Form->input(
			"Bankchequebook.bankaccount_id",
			array(
				'type'=>'select',
				"options"=>array($bankaccount),
				'empty'=>__('Select Account'),
				'div'=>false,
				'tabindex'=>4,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
			)
		)
		.$this->Form->input(
			"Bankchequebook.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => ''
			)
		);
		$Utilitys->printbutton ($printableid='bankchequebooksarchloading', $searchfield=null);
	echo $Utilitys->filtertagend();
	echo"
		<div id=\"bankchequebooksarchloading\">
	";
		echo $Utilitys->paginationcommon();
		echo $Utilitys->tablestart($class=null, $id='printtable');
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('Account')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Cheque Series')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Cheque Sequency Form')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Cheque Sequency To')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Page')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Amount')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Bank')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Branch')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Account Type')  => array('class' => 'highlight sortable')
						), 
						__('Active'), 
						__('Action')
					)
				);
				$bankchequebookrows = array();
				$countBankchequebook = 0-1+$this->Paginator->counter('%start%');
				foreach($bankchequebook as $Thisbankchequebook):
					$countBankchequebook++;
					$bankchequebookrows[]= array(
						$countBankchequebook,
						$Utilitys->amountDetailsURL(
							$Thisbankchequebook["Bankchequebook"]["bankaccount_name"],
							$Thisbankchequebook["Bankchequebook"]["bankaccount_id"],
							"Banktransaction",
							"bankaccount_id"
							),
						$Thisbankchequebook["Bankchequebook"]["bankchequebookseries"],
						$Thisbankchequebook["Bankchequebook"]["bankchequebooksequencyfrom"],
						$Thisbankchequebook["Bankchequebook"]["bankchequebooksequencyto"],
						$Thisbankchequebook["Bankchequebook"]["bankchequebook_totalpage"],
						$Utilitys->amountDetailsURL(
							$Thisbankchequebook["Bankchequebook"]["bankchequebook_totalamount"],
							$Thisbankchequebook["Bankchequebook"]["id"],
							"Banktransaction",
							"bankchequebook_id"
							),
						$Thisbankchequebook["Bankchequebook"]["bank_name"],
						$Thisbankchequebook["Bankchequebook"]["bankbranch_name"],
						$Thisbankchequebook["Bankchequebook"]["bankaccounttype_name"],
						$Utilitys->statusURL(
							$this->request["controller"],
							'bankchequebook',
							$Thisbankchequebook["Bankchequebook"]["id"],
							$Thisbankchequebook["Bankchequebook"]["bankchequebookuuid"],
							$Thisbankchequebook["Bankchequebook"]["bankchequebookisactive"]
							),
						$Utilitys->cusurl(
							$this->request["controller"],
							'bankchequebook',
							$Thisbankchequebook["Bankchequebook"]["id"],
							$Thisbankchequebook["Bankchequebook"]["bankchequebookuuid"]
							)
						);
				endforeach;
				echo $this->Html->tableCells(
					$bankchequebookrows
				);
		echo $Utilitys->tableend();
		echo $Utilitys->paginationcommon();
	echo"
		</div>
		<script>
			$(document).ready(function() {
				$(\"#BankchequebookSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#bankchequebooksarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/bankchequebooksearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#bankchequebooksarchloading\").html(result);
						}
					);
				});
				$(\"#BankchequebookBankId\").change(function(){
					var BankchequebookBankId = $(\"#BankchequebookBankId\").val();
					var BankchequebookBankbranchId = $(\"#BankchequebookBankbranchId\").val();
					var BankchequebookBankaccounttypeId = $(\"#BankchequebookBankaccounttypeId\").val();
					var BankchequebookBankaccountId = $(\"#BankchequebookBankaccountId\").val();
					$(\"#bankchequebooksarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/bankchequebooksearchbytext',
						{BankchequebookBankId:BankchequebookBankId,BankchequebookBankbranchId:BankchequebookBankbranchId,BankchequebookBankaccounttypeId:BankchequebookBankaccounttypeId,BankchequebookBankaccountId:BankchequebookBankaccountId},
						function(result) {
							$(\"#bankchequebooksarchloading\").html(result);
						}
					);
				});
				$(\"#BankchequebookBankbranchId\").change(function(){
					var BankchequebookBankId = $(\"#BankchequebookBankId\").val();
					var BankchequebookBankbranchId = $(\"#BankchequebookBankbranchId\").val();
					var BankchequebookBankaccounttypeId = $(\"#BankchequebookBankaccounttypeId\").val();
					var BankchequebookBankaccountId = $(\"#BankchequebookBankaccountId\").val();
					$(\"#bankchequebooksarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/bankchequebooksearchbytext',
						{BankchequebookBankId:BankchequebookBankId,BankchequebookBankbranchId:BankchequebookBankbranchId,BankchequebookBankaccounttypeId:BankchequebookBankaccounttypeId,BankchequebookBankaccountId:BankchequebookBankaccountId},
						function(result) {
							$(\"#bankchequebooksarchloading\").html(result);
						}
					);
				});
				$(\"#BankchequebookBankaccounttypeId\").change(function(){
					var BankchequebookBankId = $(\"#BankchequebookBankId\").val();
					var BankchequebookBankbranchId = $(\"#BankchequebookBankbranchId\").val();
					var BankchequebookBankaccounttypeId = $(\"#BankchequebookBankaccounttypeId\").val();
					var BankchequebookBankaccountId = $(\"#BankchequebookBankaccountId\").val();
					$(\"#bankchequebooksarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/bankchequebooksearchbytext',
						{BankchequebookBankId:BankchequebookBankId,BankchequebookBankbranchId:BankchequebookBankbranchId,BankchequebookBankaccounttypeId:BankchequebookBankaccounttypeId,BankchequebookBankaccountId:BankchequebookBankaccountId},
						function(result) {
							$(\"#bankchequebooksarchloading\").html(result);
						}
					);
				});
				$(\"#BankchequebookBankaccountId\").change(function(){
					var BankchequebookBankId = $(\"#BankchequebookBankId\").val();
					var BankchequebookBankbranchId = $(\"#BankchequebookBankbranchId\").val();
					var BankchequebookBankaccounttypeId = $(\"#BankchequebookBankaccounttypeId\").val();
					var BankchequebookBankaccountId = $(\"#BankchequebookBankaccountId\").val();
					$(\"#bankchequebooksarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/bankchequebooksearchbytext',
						{BankchequebookBankId:BankchequebookBankId,BankchequebookBankbranchId:BankchequebookBankbranchId,BankchequebookBankaccounttypeId:BankchequebookBankaccounttypeId,BankchequebookBankaccountId:BankchequebookBankaccountId},
						function(result) {
							$(\"#bankchequebooksarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>
