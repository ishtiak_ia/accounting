<?php
	echo $Utilitys->paginationcommon();	
	echo $Utilitys->tablestart();
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('User Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('User Fullname')  => array('class' => 'highlight sortable')
				),
				array(
					__('Group Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Company Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Branch Name')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Joining Salary')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Joining Date')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Email')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Phone')  => array('class' => 'highlight sortable')
				),
				array(
					__('Is Active')  => array('class' => 'highlight sortable')
				),
				__('Action')
			)
		);
		$userrows = array();
		$countUser = 0-1+$this->Paginator->counter('%start%');

		foreach($users as $Thisuser):
			$countUser++;
			$userrows[]= array(
				$countUser,
				$Thisuser["User"]["username"],
				$Utilitys->userprofileView($Thisuser["User"]["user_fullname"], $Thisuser["User"]["id"], $Thisuser["User"]["useruuid"], "User"),
				$Thisuser["Group"]["groupname"],
				$Thisuser["Company"]["companyname"]. " / " .$Thisuser["Company"]["companynamebn"],
				$Thisuser["Branch"]["branchname"]. " / " .$Thisuser["Branch"]["branchnamebn"],
				$Thisuser["User"]["userjoiningsalary"],
				$Thisuser["User"]["userjoiningdate"],
				$Thisuser["User"]["useremailaddress"],
				$Thisuser["User"]["usertelephone"],
				$Utilitys->statusURL(
					$this->request["controller"], 
					'user', 
					$Thisuser["User"]["id"], 
					$Thisuser["User"]["useruuid"], 
					$Thisuser["User"]["userisactive"]
				),
				$Utilitys->cusurl(
					$this->request['controller'], 
					'user', 
					$Thisuser["User"]["id"], 
					$Thisuser["User"]["useruuid"]
				)
			);
		endforeach;
		echo $this->Html->tableCells(
			$userrows
		);
	echo $Utilitys->tableend();
			
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var UserGroupId = $(\"#UserGroupId\").val();
					var UserCompanyId = $(\"#UserCompanyId\").val();
					var UserBranchId = $(\"#UserBranchId\").val();
					var UserCategoryId = $(\"#UserCategoryId\").val();
					var UserDepartmentId = $(\"#UserDepartmentId\").val();
					var UserSubjectId = $(\"#UserSubjectId\").val();
					var UserDesignationId = $(\"#UserDesignationId\").val();
					var UserInstituteId = $(\"#UserInstituteId\").val();
					var Searchtext = $('#UserSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{UserGroupId:UserGroupId, UserCompanyId:UserCompanyId, UserBranchId:UserBranchId, UserCategoryId:UserCategoryId, UserDepartmentId:UserDepartmentId, UserSubjectId:UserSubjectId, UserDesignationId:UserDesignationId, UserInstituteId:UserInstituteId, Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#usersarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#usersarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>
