<?php
App::uses('AppModel', 'Model');
class Orderdetail extends AppModel {
	public $name = 'Orderdetail';
	public $usetables = 'orderdetails';

	var $belongsTo  = array(
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'orderdetailinsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'orderdetailupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'orderdetaildeleteid'
		),
		'Unit' => array(
			'fields' =>array('Unit.unitname', 'Unit.unitnamebn'),
			'className'	=> 'Unit',
			'foreignKey'	=> 'unit_id'
		),
		'Product' => array(
			'fields' =>array('Product.productname', 'Product.productcode'),
			'className'	=> 'Product',
			'foreignKey'	=> 'product_id'
		),
		'Productcategory' => array(
			'fields' =>array('Productcategory.productcategoryname', 'Productcategory.productcategorynamebn'),
			'className'	=>	'Productcategory',
			'foreignKey'	=>	false,
			'conditions'	=>	'Product.productcategory_id = Productcategory.id'
		),
		'Producttype' => array(
			'fields' =>array('Producttype.producttypename', 'Producttype.producttypenamebn'),
			'className'	=>	'Producttype',
			'foreignKey'	=>	false,
			'conditions'	=>	'Product.producttype_id = Producttype.id'
		),
		'Branch' => array(
			'fields' =>array('Branch.branchname', 'Branch.branchnamebn'),
			'className'    => 'Branch',
			'foreignKey'    => 'branch_id'
		),
		'Order' => array(
			'fields' =>array('Order.orderreferenceno'),
			'className'    => 'Order',
			'foreignKey'    => 'transaction_id'
		),
		'User' => array(
			'fields' =>array('User.userfirstname', 'User.usermiddlename', 'User.userlastname'),
			'className'	=> 'User',
			'foreignKey'	=> false,
			'conditions'	=> 'Order.user_id = User.id'
		),
		'Client' => array(
			'fields' =>array('User.userfirstname', 'User.usermiddlename', 'User.userlastname'),
			'className'    => 'User',
			'foreignKey'	=> false,
			'conditions'	=> 'Order.client_id = Client.id'
		),
		'Salesman' => array(
			'fields' =>array('User.userfirstname', 'User.usermiddlename', 'User.userlastname'),
			'className'    => 'User',
			'foreignKey'	=> false,
			'conditions'	=> 'Order.salesman_id = Salesman.id'
		),
		'Coa' => array(
			'fields' =>array('Coa.coacode', 'Coa.coaname', 'Coa.coanamebn'),
			'className'    => 'Coa',
			'foreignKey'    => 'coa_id'
		),
		'Transactiontype' => array(
			'fields' =>array('Transactiontype.transactiontypename', 'Transactiontype.transactiontypenamebn'),
			'className'    => 'Transactiontype',
			'foreignKey'    => 'transactiontype_id'
		)	
	);

	var $virtualFields = array(
		'orderdetail_date' => 'IF(Orderdetail.transactiondate="0000-00-00", DATE(Orderdetail.orderdetailinsertdate), DATE(Orderdetail.transactiondate))',
		'orderdetail_productname' => 'IF(Orderdetail.product_id=0, "", CONCAT(Product.productname,"(",Product.productcode,")"))',
		'orderdetail_particulars' =>  'CONCAT(
			IF(Orderdetail.transactiontype_id=0, " ", Transactiontype.transactiontypename), 
			IF(Orderdetail.transaction_id=0, " ",IF(Order.client_id=0, "", CONCAT("/", Client.userfirstname, " ", Client.usermiddlename, " ", Client.userlastname))), 
			IF(Orderdetail.transaction_id=0, IF(Orderdetail.orderdetailinsertid=0, "", CONCAT("/", Creator.userfirstname, " ", Creator.usermiddlename, " ", Creator.userlastname)),IF(Order.user_id=0, "", CONCAT("/", User.userfirstname, " ", User.usermiddlename, " ", User.userlastname))), 
			IF(Orderdetail.coa_id=0, " ", CONCAT("/", Coa.coaname)),
			IF(Orderdetail.transaction_id=0, " ",IF(Order.salesman_id=0, "", CONCAT("/", Salesman.userfirstname, " ", Salesman.usermiddlename, " ", Salesman.userlastname)))
		)',
		'orderdetail_branch' => 'Branch.branchname',
		'orderdetail_productquantity' => 'Orderdetail.quantity',
		'orderdetail_productmon' => 'Orderdetail.orderdetailmon',
		'orderdetail_productkg' => 'Orderdetail.orderdetailkg',
		'orderdetail_productgram' => 'Orderdetail.orderdetailgram',
		'order_amount' 	=> 'ROUND(Orderdetail.transactionamount, 2)',
		'order_discountamount' => 'ROUND(Orderdetail.discount, 2)',
		'orderdetail_balance' => 'IF(Orderdetail.transactionamount=0, "0.00", ROUND(Orderdetail.transactionamount, 2))',
		'orderdetail_withdraw' => 'IF(Orderdetail.transactionamount<0,ROUND(Orderdetail.transactionamount, 2), "0.00")',
		'orderdetail_deposit' => 'IF(Orderdetail.transactionamount>0,ROUND(Orderdetail.transactionamount, 2), "0.00")',
		//'orderdetail_chequenumber' =>'CONCAT( Banktransaction.banktransactionchequebookseries, " - ", Banktransaction.banktransactionchequesequencynumber )',
	);	
}