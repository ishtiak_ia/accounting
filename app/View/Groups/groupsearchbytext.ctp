<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
						echo $this->Html->tableHeaders(
							array(
								__('SL#'), 
								array(
									__('Group Name')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Is Parmanent')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Is Active')  => array('class' => 'highlight sortable')
								), 
								__('Action')
							)
						);
						$grouprows = array();
						$countGroup = 0-1+$this->Paginator->counter('%start%');
						foreach($groups as $Thisgroup):
							$countGroup++;
							$grouprows[]= array($countGroup,$Thisgroup["Group"]["groupname"],$Thisgroup["Group"]["groupispermanent"],$Utilitys->statusURL($this->request["controller"], 'group', $Thisgroup["Group"]["id"], $Thisgroup["Group"]["groupuuid"], $Thisgroup["Group"]["groupisactive"]),$Utilitys->cusurl($controller, 'Group', $Thisgroup["Group"]["id"], $Thisgroup["Group"]["groupuuid"]));
						endforeach;
						echo $this->Html->tableCells(
							$grouprows
						);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#GroupSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#groupsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#groupsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>