<?php
App::import('Controller', 'Logins');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');

class ProductsController extends AppController {
	public $name = 'Products';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'Form' => array('className' => 'BootstrapForm'));
	public $components = array('RequestHandler','Session', 'Image');
	public $uses = array('Product', 'Productbranch', 'Productcategory', 'Producttype', 'Unit', 'User', 'Company', 'Branch', 'Coa');

	var $Logins;
	function beforeFilter(){
		$this->Logins =& new LoginsController;
		$this->Logins->constructClasses();
		$this->Logins->__validateLoginStatus();
		//$this->recordActivity();
		// $this->Auth->allow('register');
		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	public  function beforeRender(){
		$this->response->disableCache();//prevent useless warnings for Ajax
		if($this->RequestHandler->isAjax()){
			Configure::write('debug', 0);
		}
	}

	//Start Product Type//
	public function producttypemanage(){
		$this->layout='default';
		$userID = $this->Session->read('User.id');
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$this->set('title_for_layout', __('Product Type').' | '.__(Configure::read('site_name')));

		$condition = array();
		if($group_id==1):
			$condition[] = array('');
		elseif($group_id==2):
			$condition[] = array('Producttype.company_id'=>$company_id);
		else:
			$condition[] = array('Producttype.company_id'=>$company_id);
			//$condition_producttype[] = array('Product.branch_id'=>$branch_id);
		endif;

		$this->paginate = array(
			'fields' => 'Producttype.*',
			'order'  =>'Producttype.id ASC',
			'conditions' => $condition,
			'limit' => 20
		);
		$producttype = $this->paginate('Producttype');
		$this->set('producttype',$producttype);
	}

	public function producttypeinsertupdate($id=null,$uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Insert/Edit Product Type').' | '.__(Configure::read('site_name')));
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
			$producttype=$this->Producttype->find('all',array("fields" => "Producttype.*","conditions"=>array('Producttype.id'=>$id,'Producttype.producttypeuuid'=>$uuid)));
			$this->set('producttype',$producttype);
		endif;
	}

	public function  producttypeinsertupdateaction($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		$company_id = $_SESSION["User"]["company_id"];
		$branch_id = $_SESSION["User"]["branch_id"];

		$id = @$this->request->data['Products']['id'];
		$uuid = @$this->request->data['Products']['producttypeuuid'];
		if ($this->request->is('post')) {
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
				$this->request->data['Producttype']['producttypeupdatetid']=$userID;
				$this->request->data['Producttype']['producttypeupdatedate']=date('Y-m-d H:i:s');
				$message = __('Update Successfully.');
				$this->Producttype->id = $id;
			else:
				$this->request->data['Producttype']['company_id']=$company_id;
				$this->request->data['Producttype']['branch_id']=$branch_id;
				$this->request->data['Producttype']['producttypeinsertid']=$userID;
				$this->request->data['Producttype']['producttypeinsertdate']=date('Y-m-d H:i:s');
				$this->request->data['Producttype']['producttypeuuid']=String::uuid();
				$message = __('Save Successfully.');
			endif;
			if ($this->Producttype->save($this->request->data, array('validate' => 'only'))) {
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				$this->redirect("/products/producttypemanage");
			}else{
				$errors = $this->Producttype->invalidFields();
				$this->set('errors', $this->Producttype->invalidFields());
				//$this->Session->setFlash('The post was not saved... Please try again!', 'default', array('class' => 'alert alert-warning'));
				$this->redirect("/products/producttypeinsertupdate");
			}
		}
	}
	public function producttypesearchbytext($Searchtext=null){
		$this->layout='ajax';

		$Searchtext = @$this->request->data['Searchtext'];
		$producttype_id = array();
		if(!empty($Searchtext) && $Searchtext!=''):
			$producttype_id[] = array(
				'OR' => array(
					'Producttype.producttypename LIKE ' => '%'.$Searchtext.'%',
					'Producttype.producttypenamebn LIKE ' => '%'.$Searchtext.'%'
				)
			);
		else:
			$producttype_id[] = array();
		endif;

		$this->paginate = array(
			'fields' => 'Producttype.*',
			'order'  =>'Producttype.producttypename ASC',
			"conditions"=>$producttype_id,
			'limit' => 20
		);
		$producttype = $this->paginate('Producttype');
		$this->set('producttype',$producttype);
	}
	public function producttypedetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$producttype=$this->Producttype->find('all',array("fields" => "Producttype.*","conditions"=>array('Producttype.id'=>$id,'Producttype.producttypeuuid'=>$uuid)));
		$this->set('producttype',$producttype);
	}
	public function producttypedelete($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		if ($this->Producttype->updateAll(array('producttypedeleteid'=>$userID, 'producttypedeletedate'=>"'".date('Y-m-d H:i:s')."'", 'producttypeisactive'=>2), array('AND' => array('Producttype.id'=>$id,'Producttype.producttypeuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/products/producttypemanage");
	}	
	//End Product Type//

	//Start Product Category//
	public function productcategorymanage(){
		$this->layout='default';
		$userID = $this->Session->read('User.id');
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$this->set('title_for_layout', __('Product Category').' | '.__(Configure::read('site_name')));

		$condition = array();
		if($group_id==1):
			$condition[] = array('');
		elseif($group_id==2):
			$condition[] = array('Productcategory.company_id'=>$company_id);
		else:
			$condition[] = array('Productcategory.company_id'=>$company_id);
			//$condition_productcategory[] = array('Product.branch_id'=>$branch_id);
		endif;

		$this->paginate = array(
			'fields' => 'Productcategory.*',
			'order'  =>'Productcategory.id ASC',
			'conditions' => $condition,
			'limit' => 20
		);
		$productcategory = $this->paginate('Productcategory');
		$this->set('productcategory',$productcategory);
	}
	public function productcategoryinsertupdate($id = null, $uuid=null) {
		$this->layout='default';
		$this->set('title_for_layout', __('Insert/Edit Product Category').' | '.__(Configure::read('site_name')));
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0) {
			$productcategory = $this->Productcategory->find('all', array("fields" => "Productcategory.*", "conditions" => array('Productcategory.id' => $id, 'Productcategory.productcategoryuuid'=>$uuid)));
			$this->set('productcategory', $productcategory);
		}
	}

	public function productcategoryinsertupdateaction($id=null, $uuid = null){
		$userID = $this->Session->read('User.id');
		$company_id = $_SESSION["User"]["company_id"];
		$branch_id = $_SESSION["User"]["branch_id"];
		$id = @$this->request->data['Products']['id'];
		$uuid = @$this->request->data['Products']['productcategoryuuid'];
		$productcategory_name = $this->request->data['Productcategory']['productcategoryname'];
        $productcategory_namebn = $this->request->data['Productcategory']['productcategorynamebn'];
        if(!empty($id) && !empty($uuid)){
          $count_productcategory_name = 0;
        }else{
          $count_productcategory_name=$this->Productcategory->find('all',array("fields" => "Productcategory.*","conditions"=>array('Productcategory.productcategoryname'=>$productcategory_name,'Productcategory.productcategorynamebn'=>$productcategory_namebn,'Productcategory.company_id'=>$company_id, 'Productcategory.branch_id'=>$branch_id)));
          $count_productcategory_name = count( $count_productcategory_name );
        }
        if($count_productcategory_name > 0){
          $this->Session->setFlash(__("This Productcategory Name Already Exists"), 'default', array('class' => 'alert alert-warning'));
          $this->redirect("/products/productcategoryinsertupdate");  
        } else{
        	if ($this->request->is('post')) {
				if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
					$this->request->data['Productcategory']['productcategoryupdateid']=$userID;
					$this->request->data['Productcategory']['productcategoryupdatedate']=date('Y-m-d H:i:s');
					$message = __('Update Successfully.');
					$this->Productcategory->id = $id;
				else:
					$now = date('Y-m-d H:i:s');
					$this->request->data['Productcategory']['company_id']=$company_id;
					$this->request->data['Productcategory']['branch_id']=$branch_id;
					$this->request->data['Productcategory']['productcategoryinsertid']=$userID;
					$this->request->data['Productcategory']['productcategoryinsertdate'] = $now;
					$this->request->data['Productcategory']['productcategoryuuid']=String::uuid();
					$message = __('Save Successfully.');
				endif;
				if ($this->Productcategory->save($this->data)) {
					$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				}
			}
			$this->redirect("/products/productcategorymanage");	
        }
	}
	
	public function productcategorysearchbytext($Searchtext=null){
		$this->layout='ajax';

		$Searchtext = @$this->request->data['Searchtext'];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$userId=$_SESSION["User"]["id"];
		$productcategory_id = array();
		if($group_id==1):
			$productcategory_id[] = array('');
		elseif($group_id==2):
			$productcategory_id[] = array('Productcategory.company_id'=>$company_id);
		else:
			$productcategory_id[] = array('Productcategory.branch_id'=>$branch_id, 'Productcategory.company_id'=>$company_id);
		endif;
		if(!empty($Searchtext) && $Searchtext!=''):
			$productcategory_id[] = array(
				'OR' => array(
					'Productcategory.productcategoryname LIKE ' => '%'.$Searchtext.'%',
					'Productcategory.productcategorynamebn LIKE ' => '%'.$Searchtext.'%'
				)
			);
		else:
			$productcategory_id[] = array();
		endif;

		$this->paginate = array(
			'fields' => 'Productcategory.*',
			'order'  =>'Productcategory.productcategoryname ASC',
			"conditions"=>$productcategory_id,
			'limit' => 20
		);
		$productcategory = $this->paginate('Productcategory');
		$this->set('productcategory',$productcategory);
	}

	public function productcategorydetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$productcategory=$this->Productcategory->find('all',array("fields" => "Productcategory.*","conditions"=>array('Productcategory.id'=>$id,'Productcategory.productcategoryuuid'=>$uuid)));
		$this->set('productcategory',$productcategory);
	}

	public function productcategorydelete($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		if ($this->Productcategory->updateAll(array('productcategorydeleteid'=>$userID, 'productcategorydeletedate'=>"'".date('Y-m-d H:i:s')."'", 'productcategoryisactive'=>2), array('AND' => array('Productcategory.id'=>$id,'Productcategory.productcategoryuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/products/productcategorymanage");
	}	
	//End Product Category//

	//Start Product //
	public function productmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Product').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];

		$condition_productcode = array();
		$condition_productcategory = array();
		if($group_id==1):
		elseif($group_id==2):
			$condition_productcode[] = array('Product.company_id'=>$company_id);
			$condition_productcategory[] = array('Productcategory.company_id'=>$company_id);
		else:
			$condition_productcode[] = array('Product.company_id'=>$company_id);
			$condition_productcode[] = array('Product.branch_id'=>$branch_id);
			$condition_productcategory[] = array('Productcategory.company_id'=>$company_id);
			//$condition_productcategory[] = array('Product.branch_id'=>$branch_id);
		endif;

		$productcategory=$this->Productcategory->find('list',
			array("fields" => array("Productcategory.id","productcategory_name"), 
				"conditions" => $condition_productcategory
				));
		$this->set('productcategory',$productcategory);

		$condition_producttype = array();
		if($group_id==1):
			$condition_producttype[] = array('');
		elseif($group_id==2):
			$condition_producttype[] = array('Producttype.company_id'=>$company_id);
		else:
			$condition_producttype[] = array('Producttype.company_id'=>$company_id);
			//$condition_producttype[] = array('Product.branch_id'=>$branch_id);
		endif;


		$producttype=$this->Producttype->find('list',
			array("fields" => array("Producttype.id","producttype_name")
					//"conditions" => $condition_producttype
				)
			);
		$this->set('producttype',$producttype);


		$condition_productunit = array();
		if($group_id==1):
			$condition_productunit[] = array('');
		elseif($group_id==2):
			$condition_productunit[] = array('Unit.company_id'=>$company_id);
		else:
			$condition_productunit[] = array('Unit.company_id'=>$company_id);
			//$condition_productunit[] = array('Product.branch_id'=>$branch_id);
		endif;

		$productunit=$this->Unit->find('list',
			array("fields" => array("Unit.id","unit_name"),
					"conditions" => $condition_productunit
				)
			);
		$this->set('productunit',$productunit);	

		$productcode=$this->Product->find('list',
			array("fields" => array("Product.id","Product.productcode"), 
				"conditions" => $condition_productcode
				));
		$this->set('productcode',$productcode);

		$condition_product = array();
		if($group_id==1):
			$condition_product[] = array('');
		elseif($group_id==2):
			$condition_product[] = array('Product.company_id'=>$company_id);
		else:
			$condition_product[] = array('Product.company_id'=>$company_id);
			//$condition_producttype[] = array('Product.branch_id'=>$branch_id);
		endif;

		$this->paginate = array(
			'fields' => 'Product.*, Productcategory.*, Producttype.*, Unit.*',
			'order'  =>'Product.productname ASC',
			"conditions" => $condition_product,
			'limit' => 20
		);
		$product = $this->paginate('Product');
		$this->set('product',$product);
	}
	public function productinsertupdate($id = null, $uuid = null){
		$this->layout='default';
		$userID = $this->Session->read('User.id');
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		//print_r($userID);
		//$menu_url = $this->Session->read('Menu.menuurl');
        
		$this->set('title_for_layout', __('Insert/Edit Product').' | '.__(Configure::read('site_name')));

		$condition_productcategory = array();
		if($group_id==1):
			$condition_productcategory[] = array('');
		elseif($group_id==2):
			$condition_productcategory[] = array('Productcategory.company_id'=>$company_id);
		else:
			$condition_productcategory[] = array('Productcategory.company_id'=>$company_id);
			//$condition_productcategory[] = array('Product.branch_id'=>$branch_id);
		endif;
		$productcategory =$this->Productcategory->find('list',array(
			"fields" => array("Productcategory.id","productcategory_name"),
			'conditions' => array('Productcategory.productcategoryisactive' => 1, $condition_productcategory)
			));
		$this->set('productcategory', $productcategory);

		$condition_producttype = array();
		if($group_id==1):
			$condition_producttype[] = array('');
		elseif($group_id==2):
			$condition_producttype[] = array('Producttype.company_id'=>$company_id);
		else:
			$condition_producttype[] = array('Producttype.company_id'=>$company_id);
			//$condition_producttype[] = array('Product.branch_id'=>$branch_id);
		endif;
		$producttype =$this->Producttype->find('list',array(
			"fields" => array("Producttype.id","producttype_name")
			//'conditions' => array('Producttype.producttypeisactive' => 1, $condition_producttype)
			));
		$this->set('producttype', $producttype);

		$condition_productunit = array();
		if($group_id==1):
			$condition_productunit[] = array('');
		elseif($group_id==2):
			$condition_productunit[] = array('Unit.company_id'=>$company_id);
		else:
			$condition_productunit[] = array('Unit.company_id'=>$company_id);
			//$condition_productunit[] = array('Product.branch_id'=>$branch_id);
		endif;
		$productunit = $this->Unit->find('list',array("fields" => array("Unit.id","unit_name"), "conditions" => $condition_productunit));
		$this->set('productunit', $productunit);
		//$productcompany = $this->Company->find('list',array("fields" => array("Company.id","Company.company_name")));
		//$this->set('productcompany', $productcompany);
		$coa=$this->Coa->find(
			'list',
			array(
				"fields" => array("id","coa_name"),
				"conditions" => array('NOT'=>array('Coa.coaisactive'=>array(2)), 'Coa.coagroup_id' => 5)
			)
		);
		$this->set('coa',$coa);
        if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0) {
            $product = $this->Product->find('all', array("fields" => "Product.*", "conditions" => array('Product.id' => $id, 'Product.productuuid'=>$uuid)));
            $this->set('product', $product);
        }
	}
	public function productbranchinsert($id){
		$userID = $this->Session->read('User.id'); 
		$groupID=$_SESSION["User"]["group_id"];
	  	$companyID = $this->Session->read('User.company_id');
      	$branch_id=$_SESSION["User"]["branch_id"];

      	$this->request->data['Productbranch']['company_id']=$companyID;
      	$this->request->data['Productbranch']['branch_id']=$branch_id;
      	$this->request->data['Productbranch']['product_id']=$id;
      	$this->request->data['Productbranch']['productbranchisactive'] = 1;
      	$this->request->data['Productbranch']['productbranchinsertid']= $userID;
      	$this->request->data['Productbranch']['productbranchinsertdate']= date('Y-m-d H:i:s');
      	$this->request->data['Productbranch']['productbranchuuid']=String::uuid();

      	$this->Productbranch->save($this->data);
	}
	public function productinsertupdateaction($id = null, $uuid = null){
		$id = @$this->request->data['Products']['id'];
		$uuid = @$this->request->data['Products']['productuuid'];
		$groupID=$_SESSION["User"]["group_id"];
        $userID = $this->Session->read('User.id');
        $company_id=$_SESSION["User"]["company_id"];
        $branch_id=$_SESSION["User"]["branch_id"];
        $productcode = $this->request->data['Product']['productcode'];
        $productname = $this->request->data['Product']['productname'];
        $productnamebn = $this->request->data['Product']['productnamebn'];
        $product_category = $this->request->data['Product']['productcategory_id'];

        
        if(!empty($id) && !empty($uuid)){
        	$countproductcode = 0;
        	$countproductname = 0;
        } else {
        	$countproductcode=$this->Product->find('all',array("fields" => "Product.*","conditions"=>array('Product.productcode'=>$productcode)));
        	$countproductcode = count($countproductcode);
        	$countproductname=$this->Product->find('all',array("fields" => "Product.*","conditions"=>array('Product.productname'=>$productname,'Product.productnamebn'=>$productnamebn, 'Product.productcategory_id'=>$product_category,)));
        	$countproductname = count($countproductname);
        	//$countproductcategory=$this->Product->find('all',array("fields" => "Product.*","conditions"=>array('Product.productcategory_id'=>$product_category)));
        	//$countproductcategory = count($countproductcategory);
        }
        if(($countproductcode > 0) && ($countproductname > 0) && ($countproductcategory > 0)){
    		$this->Session->setFlash(__("This Product Code, Product Name and Product Category Already Exists"), 'default', array('class' => 'alert alert-warning'));
			$this->redirect("/products/productinsertupdate");    	
        } elseif($countproductcode > 0){
        	$this->Session->setFlash(__("This Product Code Already Exists"), 'default', array('class' => 'alert alert-warning'));
			$this->redirect("/products/productinsertupdate");
        }elseif($countproductname > 0){
        	$this->Session->setFlash(__("This Product Name Already Exists"), 'default', array('class' => 'alert alert-warning'));
			$this->redirect("/products/productinsertupdate");
        } else{
        	if ($this->request->is('post')){
	        	if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
	        		$this->request->data['Product']['productupdateid']=$userID;
	                $this->request->data['Product']['productupdatedate']=date('Y-m-d H:i:s');
	                $message = __('Update Successfully.');
	                $this->Product->id = $id;
	                if($this->Product->save($this->data)){
	                	$image = $this->request->data['Product']['photo1']['name'];
	                	if (!empty($this->request->data['Product']['photo1']['name'])) {
		                    $image_path = $this->Image->upload_image_and_thumbnail($image, $this->data['Product']['photo1'], 600, 450, 150, 180, "product");
		                     if ($this->data['Product']['photo2'] != '') {
		                        unlink(WWW_ROOT . 'img/product/big/' . $this->data['Product']['photo2']);
		                        unlink(WWW_ROOT . 'img/product/small/' . $this->data['Product']['photo2']);
		                        unlink(WWW_ROOT . 'img/product/home/' . $this->data['Product']['photo2']);
		                    }
		                     if (isset($image_path)) {
		                        $this->Product->saveField('productimage', $image_path);
		                        $uploaded = true;
		                    }
		                }
	                }
	        	} else{
	        			$now = date('Y-m-d H:i:s');
	        			$this->request->data['Product']['company_id']=$company_id;
	        			$this->request->data['Product']['branch_id']=$branch_id;
			            $this->request->data['Product']['productinsertid']=$userID;
			            $this->request->data['Product']['productinsertdate'] = $now;
			            $this->request->data['Product']['productuuid']=String::uuid();
			            $message = __('Save Successfully.');
			            if ($this->Product->save($this->data)){
			            	$image = $this->request->data['Product']['photo1']['name'];
			            	if (!empty($this->request->data['Product']['photo1']['name'])) {
			            		$image_path = $this->Image->upload_image_and_thumbnail($image, $this->data['Product']['photo1'], 600, 450, 150, 180, "product");
			            		if (isset($image_path)) {
			                        $this->Product->saveField('productimage', $image_path);
			                        $uploaded = true;
			                    }
			            	}
			            }
	        	}
	        	if($this->Product->save($this->data)){
	        		$id = $this->Product->getLastInsertId();
	        		if($groupID == 3){
	        			$this->productbranchinsert($id);
	        		}
	        		$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
	        	}
	        }
	        $this->redirect("productmanage");	
        }
	}
	public function productdetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$product=$this->Product->find('all',array("fields" => "Product.*, Productcategory.*, Unit.*", "conditions"=>array('Product.id'=>$id,'Product.productuuid'=>$uuid)));
		$this->set('product',$product);
	}
	public function productsearchbytext($Searchtext=null){
		$this->layout = 'ajax';
		$userID = $this->Session->read('User.id');
		$group_id=$_SESSION["User"]["group_id"];
        $company_id=$_SESSION["User"]["company_id"];
        $branch_id=$_SESSION["User"]["branch_id"];

		$Searchtext = @$this->request->data['Searchtext'];
		$ProductProductcategoryId = @$this->request->data['ProductProductcategoryId'];
		$ProductProducttypeId = @$this->request->data['ProductProducttypeId'];
        $ProductUnitId = @$this->request->data['ProductUnitId'];
        $product_id = array();
        if($group_id==1):
			$product_id[] = array('');
		elseif($group_id==2):
			$product_id[] = array('Product.company_id'=>$company_id);
		else:
			$product_id[] = array('Product.branch_id'=>$branch_id, 'Product.company_id'=>$company_id);
		endif;
        if(!empty($ProductProductcategoryId) && $ProductProductcategoryId!=''):
            $product_id[] = array('Product.productcategory_id'=>$ProductProductcategoryId);
        endif;
        if(!empty($ProductProducttypeId) && $ProductProducttypeId!=''):
            $product_id[] = array('Product.producttype_id'=>$ProductProducttypeId);
        	//$product_id = array('Product.company_id'=>$company_id);
        endif;
        if(!empty($ProductUnitId) && $ProductUnitId!=''):
            $product_id[] = array('Product.unit_id'=>$ProductUnitId);
        endif;
        if(!empty($Searchtext) && $Searchtext!=''):
            $product_id[] = array(
                'OR' => array(
                    'Product.productname LIKE ' => '%'.$Searchtext.'%',
                    'Product.productnamebn LIKE ' => '%'.$Searchtext.'%',
                    'Productcategory.productcategoryname LIKE ' => '%'.$Searchtext.'%',
                    'Productcategory.productcategorynamebn LIKE ' => '%'.$Searchtext.'%',
                    'Producttype.producttypename LIKE ' => '%'.$Searchtext.'%',
                    'Producttype.producttypenamebn LIKE ' => '%'.$Searchtext.'%',
                    'Unit.unitname LIKE ' => '%'.$Searchtext.'%',
                    'Unit.unitnamebn LIKE ' => '%'.$Searchtext.'%',
                    'Product.productcode LIKE ' => '%'.$Searchtext.'%'
                )
            );
        else:
            $product_id[] = array();
        endif;

        $this->paginate = array(
            'fields' => 'Product.*, Productcategory.*, Producttype.*, Unit.*',
            'order'  =>'Product.productname ASC',
            "conditions"=>$product_id,
            'limit' => 20
        );
        $product = $this->paginate('Product');
        $this->set('product',$product);
	}
	public function productdelete($id = null,$uuid=null){
		$this->layout='ajax';
		$userID = $this->Session->read('User.id');
		$this->request->data['Product']['productdeleteid']=$userID;
		$this->request->data['Product']['productdeletedate'] = date('Y-m-d H:i:s');
		$this->request->data['Product']['productisactive']=2;
		$this->Product->id = $id;
		$this->Product->productuuid = $uuid;
		if ($this->Product->save($this->data)) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("productmanage");
	}


	//End Product //

	//Start Product Branch Assign//
	public function productbranchmanage(){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Manage Product Branch Assign').' | '.__(Configure::read('site_name')));
		$userID = $this->Session->read('User.id');
		$group_id=$_SESSION["User"]["group_id"];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		
		$Productbranch_id = array();
		$companyId = array();
		$branchId = array();
		$productId = array();
		if($group_id==1):
			$companyId[] = array('');
			$branchId[] = array('');
			$productId[] = array('');
			$Productbranch_id[] = array('');
		elseif($group_id==2):
			$companyId[] = array('Company.id'=>$company_id);
			$branchId[] = array('Branch.company_id'=>$company_id);
			$productId[] = array('Product.company_id' => $company_id);
			$Productbranch_id[] = array('Productbranch.company_id' => $company_id);
		else:
			$companyId[] = array('Company.id'=>$company_id);
			$branchId[] = array('Branch.id'=>$branch_id);
			$productId[] = array('Product.company_id' => $company_id, 'Product.branch_id' => $branch_id);
			$Productbranch_id[] = array('Productbranch.branch_id'=>$branch_id, 'Productbranch.company_id'=>$company_id);
		endif;
		/*$productbranch = $this->Productbranch->find(
			'list',
			array(
				"fields" => array("Productbranch.*"),
				"conditions" => array('Productbranch.company_id' => $company_id)
			)
		);
		$this->set('productbranch', $productbranch);*/

		$company=$this->Company->find(
        	'list',
        	array(
        		"fields" => array("id","company_name"),
        		"conditions" => $companyId
        		)
        );
        $this->set('company',$company);
        $branch=$this->Branch->find(
        	'list',
        	array(
        		"fields" => array("id","branch_name"),
        		"conditions" => $branchId
        		)
        );
        $this->set('branch',$branch);
        $product=$this->Product->find(
        	'list',
        	array(
        		"fields" => array("id","product_name"),
        		"conditions" => $productId
        		)
        );
        $this->set('product',$product);
		$this->paginate = array(
			'fields' => 'Productbranch.*',
			//'order'  =>'Product.productname ASC',
			"conditions" => $Productbranch_id,
			'limit' => 20
		);
		$productbranch = $this->paginate('Productbranch');
		$this->set('productbranch',$productbranch);
	}
	public function productbranchinsertupdate($id = null, $uuid=null){
		$this->layout = 'default';
		$this->set('title_for_layout', __('Product Branch Assign').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];

		$product_condition = array();
		$branch_condition = array();
		if($group_id==1):
			$product_condition[] = array('');
			$branch_condition[] = array('');
		elseif($group_id==2):
			$product_condition[] = array('Product.company_id' => $company_id);
			$branch_condition[] = array('Branch.company_id'=>$company_id);
		else:
			$product_condition[] = array('Product.company_id' => $company_id, 'Product.branch_id' => $branch_id);
			$branch_condition[] = array('Branch.company_id'=>$company_id, 'Branch.branch_id'=>$branch_id);
		endif;


		$product_list = $this->Product->find(
			'list',
			array(
				"fields" => array("id", "product_name"),
				"conditions" => $product_condition
			)
		);
		$this->set('product_list', $product_list);

		$branch_list = $this->Branch->find(
			'list',
			array(
				"fields" => array("id", "branch_name"), 
				"conditions" => $branch_condition
			)
		);
		$this->set('branch_list', $branch_list);
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0) {
			$productbranch = $this->Productbranch->find('all', array("fields" => "Productbranch.*", "conditions" => array('Productbranch.id' => $id, 'Productbranch.productbranchuuid'=>$uuid)));
			$this->set('productbranch', $productbranch);
		}
	}

	public function productbranchinsertupdateaction($id=null, $uuid = null){
		$id = @$this->request->data['Products']['id'];
		$uuid = @$this->request->data['Products']['productbranchuuid'];
		$userID = $this->Session->read('User.id');
		$company_id = $_SESSION["User"]["company_id"];
		$product_id = $this->request->data['Productbranch']['product_id'];
      	$branch_id = $this->request->data['Productbranch']['branch_id'];

      	$result = $this->Productbranch->find('all', array("fields" => "Productbranch.*","conditions"=>array('Productbranch.product_id' => $product_id, 'Productbranch.branch_id' => $branch_id)));
      	if(count($result) > 0){
      		$this->Session->setFlash(__("This Product Branch Already Exists"), 'default', array('class' => 'alert alert-warning'));
        	$this->redirect("/products/productbranchinsertupdate");  
      	} else {
      		if ($this->request->is('post')){
	      		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
	      			$this->request->data['Productbranch']['productbranchupdateid']=$userID;
					$this->request->data['Productbranch']['productbranchupdatedate']=date('Y-m-d H:i:s');
					$message = __('Update Successfully.');
					$this->Productbranch->id = $id;
	      		} else {
	      			$now = date('Y-m-d H:i:s');
		      		$this->request->data['Productbranch']['productbranchuuid']=String::uuid();
		      		$this->request->data['Productbranch']['company_id'] = $company_id;
		      		$this->request->data['Productbranch']['productbranchinsertid'] = $userID;
		      		$this->request->data['Productbranch']['productbranchinsertdate'] = $now;
		      		$message = __('Save Successfully.');	
	      		}
	      		if ($this->Productbranch->save($this->data)) {
					$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
				}
	      	}
	      	$this->redirect("productbranchmanage");
	      }
	}

	public function productbranchdetailsbyid($id=null, $uuid=null){
		$this->layout='ajax';
		$productbranch=$this->Productbranch->find('all',array("fields" => "Product.*, Branch.*, Company.*, Productbranch.*", "conditions"=>array('Productbranch.id'=>$id,'Productbranch.productbranchuuid'=>$uuid)));
		$this->set('productbranch',$productbranch);
	}

	public function productbranchdelete($id=null, $uuid=null){
		$userID = $this->Session->read('User.id');
		if ($this->Productbranch->updateAll(array('productbranchdeleteid'=>$userID, 'productbranchdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'productbranchisactive'=>2), array('AND' => array('Productbranch.id'=>$id,'Productbranch.productbranchuuid' => $uuid)))) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/products/productbranchmanage");
	}
	public function productbranchsearchbytext($Searchtext=null){
		$this->layout='ajax';
		//$Searchtext = @$this->request->data['Searchtext'];
        $ProductbranchCompanyId = @$this->request->data['ProductbranchCompanyId'];
        $ProductbranchBranchId = @$this->request->data['ProductbranchBranchId'];
        $ProductbranchProductId = @$this->request->data['ProductbranchProductId'];

        $company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$userId=$_SESSION["User"]["id"];

		$condition = array();
		if($group_id==1):
			$condition[] = array('');
		elseif($group_id==2):
			$condition[] = array('Productbranch.company_id'=>$company_id);
		else:
			$condition[] = array('Productbranch.branch_id'=>$branch_id, 'Productbranch.company_id'=>$company_id);
		endif;
        if(!empty($ProductbranchCompanyId) && $ProductbranchCompanyId!=''):
            $condition[] = array('Productbranch.company_id'=>$ProductbranchCompanyId);
        endif;
        if(!empty($ProductbranchBranchId) && $ProductbranchBranchId!=''):
            $condition[] = array('Productbranch.branch_id'=>$ProductbranchBranchId);
        endif;
        if(!empty($ProductbranchProductId) && $ProductbranchProductId!=''):
            $condition[] = array('Productbranch.product_id'=>$ProductbranchProductId);
        endif;

        $this->paginate = array(
			'fields' => array('Productbranch.*', 'Branch.*', 'Product.*', 'Company.*'), 
			'conditions' => $condition,
			//'order'  =>'User.id ASC',
			//'condition' =
			'limit' => 20
		);
		$productbranch = $this->paginate('Productbranch');
		$this->set('productbranch',$productbranch);


	}

	//End Product Branch Assign//

}