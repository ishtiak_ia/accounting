<?php
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"2\">
				<h2>".__('Emergency Contact Information')."</h2>
			</td>
		</tr>
			<tr>
				<th>
					".__('Employee Name')."
				</th>
				<td>
					".$useremergencycontact[0]["Useremergencycontact"]["user_fullname"]."
				</td>
			</tr>
			<tr>
				<th>
					".__("Emergency Contact Name")."
				</th>
				<td>
					".$useremergencycontact[0]["Useremergencycontact"]["useremergencycontactname"]."
				</td>
			</tr>
			<tr>
				<th>
					".__("Emergency Contact Name BN")."
				</th>
				<td>
					".$useremergencycontact[0]["Useremergencycontact"]["useremergencycontactnamebn"]."
				</td>
			</tr>
			<tr>
				<th>
					".__("Emergency Contact Relationship")."
				</th>
				<td>
					".$useremergencycontact[0]["Useremergencycontact"]["useremergencycontactrelationship"]."
				</td>
			</tr>
			<tr>
				<th>
					".__("Emergency Contact Home Phone")."
				</th>
				<td>
					".$useremergencycontact[0]["Useremergencycontact"]["useremergencycontacthomephone"]."
				</td>
			</tr>
			<tr>
				<th>
					".__("Emergency Contact Mobile Phone")."
				</th>
				<td>
					".$useremergencycontact[0]["Useremergencycontact"]["useremergencycontactmobilephone"]."
				</td>
			</tr>
			<tr>
				<th>
					".__("Emergency Contact Office Phone")."
				</th>
				<td>
					".$useremergencycontact[0]["Useremergencycontact"]["useremergencycontactofficephone"]."
				</td>
			</tr>
			<tr>
				<th>
					".__("Active")."
				</th>
				<td>
					".$useremergencycontact[0]["Useremergencycontact"]["isActive"]."
				</td>
			</tr>
	";
	echo $Utilitys->tableend();
?>