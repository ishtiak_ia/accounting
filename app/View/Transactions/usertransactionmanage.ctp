<?php
	echo "<h2>".__('User Transaction')."</h2>";
		echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id=null);
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('Transaction Amount')  => array('class' => 'highlight sortable')
						), 
						__('Action')
					)
				);
				$usertransactionrows = array();
				$countusertransaction = 0-1+$this->Paginator->counter('%start%');
				foreach($usertransaction as $Thisusertransaction):
					$countusertransaction++;
					$usertransactionrows[]= array($countusertransaction,$Thisusertransaction["Usertransaction"]["transactionamount"],$Utilitys->cusurl($this->request["controller"], 'orderdetail', $Thisusertransaction["Usertransaction"]["id"], $Thisusertransaction["Usertransaction"]["usertransactionuuid"]));
				endforeach;
				echo $this->Html->tableCells(
					$usertransactionrows
				);
			echo $Utilitys->tableend();
		echo $Utilitys->paginationcommon();
?>