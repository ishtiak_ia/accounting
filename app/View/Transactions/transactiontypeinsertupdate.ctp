<?php
echo $this->Form->create('Transactions', array('action' => 'transactiontypeinsertupdateaction', 'type' => 'file'));
	echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$transactiontype[0]['Transactiontype']['id']));
	echo $this->Form->input('transactiontypeuuid', array('type'=>'hidden', 'value'=>@$transactiontype[0]['Transactiontype']['transactiontypeuuid']));
	$options = array(1 => __("Yes"), 0 =>__("No"));
	echo"<h3>".__("Add New Transaction Type")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-12\">";
			echo"<div class=\"row\">";
				echo"<div class=\"col-md-4\">";
					echo $this->Form->input(
						"Transactiontype.transactiontypename",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Transaction Type'),
							'tabindex'=>1,
							'placeholder' => __("Transaction Type"),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style' => '',
							'value' =>@$transactiontype[0]['Transactiontype']['transactiontypename']
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-4\">";
					echo $this->Form->input(
						"Transactiontype.transactiontypenamebn",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Transaction Type BN'),
							'tabindex'=>1,
							'placeholder' => __("Transaction Type BN"),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style' => '',
							'value' =>@$transactiontype[0]['Transactiontype']['transactiontypenamebn']
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-4\">";
					echo "<label class=\"control-label\" for=\"TransactiontypeTransactiontypeisactive\">is Active</label>";
					echo $this->Form->input(
						"Transactiontype.transactiontypeisactive",
						array(
							'type' => 'radio',
							'div' => array('class'=> 'form-group'),
							'label' => true,
							'tabindex'=>1,
							'class'=> 'form-inline',
							'data-validation-engine'=>'validate[required]',
							'options' => $options,
							'legend' => false,
							'value' => (@$transactiontype[0]['Transactiontype']['transactiontypeisactive'] ? $transactiontype[0]['Transactiontype']['transactiontypeisactive'] : 0),

							'style' => ''
						)
					);
				echo"</div>";
			echo"</div>";
			echo $Utilitys->allformbutton('',$this->request["controller"],$page='transactiontype');
		echo"</div>";
	echo"</div>";
echo $this->Form->end();
echo"
<script>
	$(document).ready(function() {
		$(\"#TransactionsTransactiontypeinsertupdateactionForm\").validationEngine();
	});	
</script>
";
?>