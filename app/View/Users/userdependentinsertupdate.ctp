<?php
echo $this->Form->create('Users', array('action' => 'userdependentinsertupdateaction', 'type' => 'file'));
	echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$userdependent[0]["Userdependent"]['id']));
	echo $this->Form->input('userdependentuuid', array('type'=>'hidden', 'value'=>@$userdependent[0]["Userdependent"]['userdependentuuid']?$userdependent[0]["Userdependent"]['userdependentuuid']:0, 'class'=>''));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$userdependent[0]['Userdependent']['userdependentisactive'] ? $userdependent[0]['Userdependent']['userdependentisactive'] : 0),
			'class' => 'form-inline'
		);
	echo"<h3>".__("Add Family Details")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-6\">";
			echo $this->Form->input(
				"Userdependent.department_id",
				array(
					'type'=>'select',
					"options"=>array($department_list),
					'empty'=>__('Select Department'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>1,
					'label'=>__('Select Department&nbsp;<span style=\'color:#cc0000\'>*</span>'),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style'=>'',
					"selected" => (@$userdependent[0]['Userdependent']['department_id'] ? $userdependent[0]['Userdependent']['department_id'] : 0)
				)
			);
			echo "<div class=\"form-group\" id=\"userdependentchangediv\">";
			echo $this->Form->input(
				"Userdependent.user_id",
				array(
					'type'=>'select',
					"options"=>array($user_list),
					'empty'=>__('Select Employee'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>1,
					'label'=>__('Select Employee&nbsp;<span style=\'color:#cc0000\'>*</span>'),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style'=>'',
					"selected" => (@$userdependent[0]['Userdependent']['user_id'] ? $userdependent[0]['Userdependent']['user_id'] : 0)
				)
			);
            echo "</div>";
			echo"<div class=\"row\" id=\"content\">";
				echo"<div class=\"col-md-11\" id=\"mydivcontent1\">";
					echo"<div class=\"row\">";
						echo"<div class=\"col-md-11\">";
							echo $this->Form->input(
								"userdependentname1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label' => __('Name&nbsp;<span style=\'color:#cc0000\'>*</span>'),
									'tabindex'=>2,
									'placeholder' => __("Name"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$userdependent[0]['Userdependent']['userdependentname']
								)
							);
							echo $this->Form->input(
								"userdependentnamebn1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label' => __('Name (Bangla)&nbsp;<span style=\'color:#cc0000\'>*</span>'),
									'tabindex'=>3,
									'placeholder' => __("Name (Bangla)"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$userdependent[0]['Userdependent']['userdependentnamebn']
								)
							);
							//$relation = Set::enum('Child', array('Child' => 'Child', 'Other' => 'Other'));
							echo $this->Form->input(
								"userrelationship_id1",
								array(
									'type'=>'select',
									"options"=>array($user_relationship),
									'empty'=>__('Select Relationship'),
									'div'=>array('class'=>'form-group'),
									'tabindex'=>4,
									'label'=>__('Select Relationship&nbsp;<span style=\'color:#cc0000\'>*</span>'),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style'=>'',
									"selected" => (@$userdependent[0]['Userdependent']['userrelationship_id'] ? $userdependent[0]['Userdependent']['userrelationship_id'] : 0)
								)
							);
				            //echo "<div class=\"form-group\" id=\"relationchangediv\">";

				            //echo "</div>";
							echo $this->Form->input(
								"userdependentdob1",
								array(
									'type' => 'text',
									'div' => array('class'=> 'form-group'),
									'label' => __('Date of birth&nbsp;<span style=\'color:#cc0000\'>*</span>'),
									'tabindex'=>6,
									'placeholder' => __("Date of birth, Format(yyyy-mm-dd)"),
									'class'=> 'form-control',
									'data-validation-engine'=>'validate[required]',
									'style' => '',
									'value' => @$userdependent[0]['Userdependent']['userdependentdob']
								)
							);
							echo"<div class=\"form-group\">";
								echo"<label>isActive?</label><br>";
								echo $this->Form->radio(
									'userdependentisactive1', 
									$options,
									$attributes
								);
							echo"</div>";
						echo"</div>";	
					echo"</div>";								
				echo"</div>";
			echo"</div>";
			echo"<div class=\"row\">";
				echo"<div class=\"col-md-11\">";
					echo"<input type=\"button\" id=\"more_fields\" onclick=\"add_fields();\" value=\"Add More\" />";
						echo"<input type=\"button\" id=\"remove_fields\" onclick=\"remove_field();\" value=\"Remove More\" />";
						echo"<input type=\"hidden\" style=\"width:48px;\" name=\"totlarow\" id=\"totlarow\" value=\"1\" /> ";
						echo"
							<div id=\"locationsarchloading\">

							</div>
						";
						echo "<br />";
					echo $Utilitys->allformbutton('',$this->request["controller"],$page='userdependent');
				echo"</div>";
			echo"</div>";
		echo"</div>";
	echo"</div>";
echo $this->Form->end();
echo"
<script>
		function scheduletimepicker(fieldID, fieldName){
			$('#'+fieldName+fieldID).datetimepicker({
				format: \"hh:ii\",
				language: 'en',
				weekStart: 1,
				todayBtn: 1,
				autoclose: 1,
				todayHighlight: 1,
				startView: 0,
				minView: 0,
				forceParse: 0,
				startDate: '01/01/2012',
				endDate: FromEndDate,
				use24hours: true
			}).on('changeDate', function(selected){
				startDate = new Date(selected.date.valueOf());
				startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
				$('#'+fieldName+fieldID).datetimepicker('setStartDate', startDate);
				TimeDiff(fieldID);
			});
		}
		
		var room=0;
		function add_fields() {
			room = parseInt(document.getElementById(\"totlarow\").value);		
			room = room+1;
			
			$(\"#content\").append('<div id=\"mydivcontent'+room+'\" class=\"col-md-11\"><div class=\"row\"><div class=\"col-md-11\"><div class=\"form-group\"><label for=\"UsersUserdependentname'+room+'\">Name</label><input type=\"text\" id=\"UsersUserdependentname'+room+'\" style=\"\" data-validation-engine=\"validate[required]\" class=\"form-control\" placeholder=\"Name\" tabindex=\"2\" name=\"data[Users][userdependentname'+room+']\"></div><div class=\"form-group\"><label for=\"UsersUserdependentnamebn'+room+'\">Name (Bangla)</label><input type=\"text\" id=\"UsersUserdependentnamebn'+room+'\" style=\"\" data-validation-engine=\"validate[required]\" class=\"form-control\" placeholder=\"Name (Bangla)\" tabindex=\"3\" name=\"data[Users][userdependentnamebn'+room+']\"></div><div class=\"form-group\"><label for=\"UsersUserrelationshipId'+room+'\">Select Relationship</label><select id=\"UsersUserrelationshipId'+room+'\" style=\"\" data-validation-engine=\"validate[required]\" class=\"form-control\" tabindex=\"4\" name=\"data[Users][userrelationship_id'+room+']\"></select></div><div class=\"form-group\"><label for=\"UsersUserdependentdob'+room+'\">Date of birth</label><input type=\"text\" id=\"UsersUserdependentdob'+room+'\" style=\"\" data-validation-engine=\"validate[required]\" class=\"form-control\" placeholder=\"Date of birth, Format(yyyy-mm-dd)\" tabindex=\"6\" name=\"data[Users][userdependentdob'+room+']\"></div><div class=\"form-group\"><label>isActive?</label><br><input type=\"radio\" class=\"form-inline\" value=\"1\" id=\"UsersUserdependentisactive'+room+'\" name=\"data[Users][userdependentisactive'+room+']\"><label for=\"UsersUserdependentisactive'+room+'\">Yes</label><input type=\"radio\" checked=\"checked\" class=\"form-inline\" value=\"0\" id=\"UsersUserdependentisactive'+room+'\" name=\"data[Users][userdependentisactive'+room+']\"><label for=\"UsersUserdependentisactive'+room+'\">No</label></div></div></div></div>');
				document.getElementById(\"totlarow\").value=room;
				//scheduletimepicker(room, 'UsersUserdependentdob');
		}
			function remove_field(){  
				room = document.getElementById(\"totlarow\").value;
				var child = document.getElementById('mydivcontent'+room);
				var parent = document.getElementById('content');
				parent.removeChild(child);
				room = room-1;
				document.getElementById(\"totlarow\").value=room;
			}
			$(document).ready(function() {
				//scheduletimepicker(1, 'UsersUserdependentdob');
				var room=0;
				room = room+1;
				$(\"#UsersUserdependentinsertupdateactionForm\").validationEngine();
				var Userrelationship_Id = ".(@$userdependent[0]["Userdependent"]["userrelationship_id"]?$userdependent[0]["Userdependent"]["userrelationship_id"]:0).";
				if(Userrelationship_Id == 3){
					$(\"#UserdependentUserdependentrelationship\").show();
				} else{
					$(\"#UserdependentUserdependentrelationship\").hide();
				}
				$(\"#UsersUserdependentdob\"+room).datetimepicker({language: \"bn\",format: \"yyyy-mm-dd\", showMeridian: false , startView: 2, minView: 2, autoclose: 1
		});
				$(\"#UserdependentUserrelationshipId\").change(function(){
					var i=0;
					room = parseInt(document.getElementById(\"totlarow\").value);
					var Userrelationship_Id = $(this).val();
			
					if(Userrelationship_Id == 3){
					$(\"#UserdependentUserdependentrelationship1\").show();
					} else{
						$(\"#UserdependentUserdependentrelationship1\").hide();
					}
					for(i=1; i<=room; i++){	
						if(Userrelationship_Id == 3){
						$(\"#UserdependentUserdependentrelationship1\"+i).show();
						} else{
							$(\"#UserdependentUserdependentrelationship1\"+i).hide();
						}
					}	
				});
				$(\"#UserdependentDepartmentId\").change(function(){
					var UserdependentDepartmentId = $(this).val();
					$(\"#userdependentchangediv\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."users/userdependentlistbydepartmentid',
						{UserdependentDepartmentId:UserdependentDepartmentId},
						function(result) {
							$(\"#userdependentchangediv\").html(result);
						}
					);
				});
			});
		</script>
	";
?>