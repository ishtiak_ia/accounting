<?php
	echo "<h2>".__('Purchase Return Manage'). $Utilitys->addurl($title=__("Add New Purchase Return"), $this->request["controller"], $page="purchasereturn")."</h2>";
		echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id=null);
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('Order Date')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Order Particulars')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Total Amount')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Discount')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Amount After discount')  => array('class' => 'highlight sortable')
						),
						array(
							__('Payment')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Due')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Due Payment Date')  => array('class' => 'highlight sortable')
						),
						__('Action')
					)
				);
				$orderrows = array();
				$countorder = 0-1+$this->Paginator->counter('%start%');
				foreach($order as $Thisorder):
					$countorder++;
					$orderrows[]= array($countorder,$Thisorder["Order"]["order_date"],$Thisorder["Order"]["order_particulars"],array($Thisorder["Order"]["order_amount"],'colspan="1" align="right"'),array($Thisorder["Order"]["order_discountamount"],'colspan="1" align="right"'),array($Thisorder["Order"]["order_amountafterdiscount"],'colspan="1" align="right"'),array($Thisorder["Order"]["order_paymentamount"],'colspan="1" align="right"'),array($Thisorder["Order"]["order_dueamount"],'colspan="1" align="right"'),$Thisorder["Order"]["order_duedate"],$Utilitys->cusurl($this->request["controller"], 'purchasereturn', $Thisorder["Order"]["id"], $Thisorder["Order"]["orderuuid"]));
				endforeach;
				echo $this->Html->tableCells(
					$orderrows
				);
			echo $Utilitys->tableend();
		echo $Utilitys->paginationcommon();
	
?>