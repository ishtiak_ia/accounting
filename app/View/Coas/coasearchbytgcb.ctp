<?php
	echo"

		<div id=\"groupsarchloading\">
	";
			echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id=null);
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('Code')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA')  => array('class' => 'highlight sortable')
						), 
						array(
							__('COA BN')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Type')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Group')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Company')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Branch')  => array('class' => 'highlight sortable')
						), 
						__('Active'), 
						__('Action')
					)
				);
				
				$coarows = array();
				$countCoa = 0-1+$this->Paginator->counter('%start%');
				
				foreach($coa as $Thiscoa):
					$countCoa++;
					$coarows[]= array($countCoa,$Thiscoa["Coa"]["coacode"],$Utilitys->amountDetailsURL($Thiscoa["Coa"]["coaname"], $Thiscoa["Coa"]["id"], "Gltransaction","coa_id"),$Thiscoa["Coa"]["coanamebn"],$Thiscoa["Coa"]["coatype_name"],$Thiscoa["Coa"]["coagroup_name"],$Thiscoa["Coa"]["company_name"],$Thiscoa["Coa"]["branch_name"],$Thiscoa["Coa"]["isActive"],$Utilitys->cusurl($this->request["controller"], 'coa', $Thiscoa["Coa"]["id"], $Thiscoa["Coa"]["coauuid"]));
				endforeach;
				echo $this->Html->tableCells(
					$coarows
				);
			echo $Utilitys->tableend();
			echo $Utilitys->paginationcommon();
	echo"
		</div>		
	";
?>