<?php echo $this->Form->create('Units', array('action' => 'unitinsertupdateaction', 'type' => 'file'));
echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$units[0]['Unit']['id']));
echo $this->Form->input('unituuid', array('type'=>'hidden', 'value'=>@$units[0]['Unit']['unituuid']));
 ?>
<fieldset>
	<legend>Add Unit Information</legend>
	<table>
		<tr>
            <td>Unit Type</td>
            <td>
                <?php
                $option1 = array($unittypes);
                echo $this->Form->input("Unit.unittype_id", array(
                    'type' => 'select',
                     "options" => $option1, 
                     'empty' => 'Select Unit Type',
                     'div' => false,
                     'label' => false, 
                     "optgroup label" => false,
                     'style' => 'width: 160px;',
                    'selected'=> (@$units[0]['Unit']['unittype_id'] ? $units[0]['Unit']['unittype_id'] : 0)
                    ));
                ?>
            </td>
        </tr>
        <tr>
            <td>Unit Name:</td>
            <td><?php echo $this->Form->input("Unit.unitname", array('type' => 'text', 'div' => false, 'label' => false, 'value'=>@$units[0]['Unit']['unitname'])); ?>
            </td>
        </tr>
        <tr>
            <td>Unit Type Name BN:</td>
            <td><?php echo $this->Form->input("Unit.unitnamebn", array('type' => 'text', 'div' => false, 'label' => false, 'value'=>@$units[0]['Unit']['unitnamebn'])); ?>
            </td>
        </tr>
        <tr>
            <td>Unit Symbol:</td>
            <td><?php echo $this->Form->input("Unit.unitsymbol", array('type' => 'text', 'div' => false, 'label' => false, 'value'=>@$units[0]['Unit']['unitsymbol'])); ?>
            </td>
        </tr>
        <tr>
            <td>Unit Symbol BN:</td>
            <td><?php echo $this->Form->input("Unit.unitsymbolbn", array('type' => 'text', 'div' => false, 'label' => false, 'value'=>@$units[0]['Unit']['unitsymbolbn'])); ?>
            </td>
        </tr>
        <tr>
            <td>Unit Mon:</td>
            <td><?php echo $this->Form->input("Unit.unitmon", array('type' => 'text', 'div' => false, 'label' => false, 'value'=>@$units[0]['Unit']['unitmon'])); ?>
            </td>
        </tr>
        <tr>
            <td>Unit Kilogram:</td>
            <td><?php echo $this->Form->input("Unit.unitkilogram", array('type' => 'text', 'div' => false, 'label' => false, 'value'=>@$units[0]['Unit']['unitkilogram'])); ?>
            </td>
        </tr>
        <tr>
            <td>Unit Gram:</td>
            <td><?php echo $this->Form->input("Unit.unitgram", array('type' => 'text', 'div' => false, 'label' => false, 'value'=>@$units[0]['Unit']['unitgram'])); ?>
            </td>
        </tr>
        <tr>
            <td>Is Active :</td>
            <td><?php
                $options = array( 1 => __("Yes"), 0 => __("No"));
                $attributes = array(
                    'legend' => false, 
                    'value' => (@$units[0]['Unit']['unitisactive'] ? $units[0]['Unit']['unitisactive'] : 0), 
                        'class' => 'form-inline'
                    );
                echo $this->Form->radio('Unit.unitisactive', $options, $attributes);
                ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><?php echo $Utilitys->allformbutton('',$this->request["controller"],$page='unit');?>
            </td>
        </tr>
	</table>
</fieldset>
<?php echo $this->Form->end(); ?> 