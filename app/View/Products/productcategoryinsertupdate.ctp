<?php 
	echo"
		<table>
			<tr>
				<td>
					<h2>".__('Add Product Category Information')."</h2>
				</td>
			</tr>
			<tr>
				<td>
	";
	echo $this->Form->create('Products', array('action' => 'productcategoryinsertupdateaction'));
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$productcategory[0]['Productcategory']['id']));
		echo $this->Form->input('productcategoryuuid', array('type'=>'hidden', 'value'=>@$productcategory[0]['Productcategory']['productcategoryuuid']));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$productcategory[0]['Productcategory']['productcategoryisactive'] ? $productcategory[0]['Productcategory']['productcategoryisactive'] : 0), 
			'class' => 'form-inline'
		);
	echo $Utilitys->tablestart();	
	echo"
						<tr>
							<td>".__("Product Category Name")."</td>
							<td>
								".$this->Form->input(
									"Productcategory.productcategoryname",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'data-validation-engine'=>'validate[required]',
										'value' =>@$productcategory[0]['Productcategory']['productcategoryname']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("Product Category Name BN")."</td>
							<td>
								".$this->Form->input(
									"Productcategory.productcategorynamebn",
									array(
										'type' => 'text', 
										'div' => false, 
										'label' => false,
										'value' => @$productcategory[0]['Productcategory']['productcategorynamebn']
									)
								)."
							</td>
						</tr>
						<tr>
							<td>".__("isActive")."</td>
							<td>
								".$this->Form->radio(
									'Productcategory.productcategoryisactive', 
									$options, 
									$attributes
								)."
							</td>
						</tr>
						<tr>
							<td colspan=\"2\">
								".$Utilitys->allformbutton('',$this->request["controller"],$page='productcategory')."
							</td>
						</tr>
					</table>
	";
	echo $this->Form->end();
	echo"
				</td>
			</tr>
		</table>
	";
?>
<script type="text/javascript">
 jQuery(document).ready(function(){
    jQuery("#ProductsProductcategoryinsertupdateactionForm").validationEngine();
 });
</script>