<?php
App::uses('AppModel', 'Model');
class Shift extends AppModel {
	public $name = 'Shift';
	public $usetables = 'shifts';

	var $virtualFields = array(
		'shift_name' => 'Shift.shiftname',
		'isPermanent' => 'IF(Shift.shiftispermanent = 1, "<span class=\"green\">Yes</span>", "<span class=\"red\">No</span>")',
		'isActive' => 'IF(Shift.shiftisactive = 0, "<span class=\"button btn btn-sm btn-warning\"><span class=\"glyphicon glyphicon-remove-circle\" title=\"Inactive\"></span></span>", IF(Shift.shiftisactive = 1, "<span class=\"button btn btn-sm btn-success\"><span class=\"glyphicon glyphicon-ok-circle\" title=\"Active\"></span></span>", "<span class=\"button btn btn-sm btn-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span></span>"))'
	);
    
}

?>
