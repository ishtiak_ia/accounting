<?php
App::uses('AppModel', 'Model');
class Journaltransaction extends AppModel {
	public $name = 'Journaltransaction';
	public $usetables = 'journaltransactions';
	var $belongsTo  = array(
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'journaltransactioninsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'journaltransactionupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'journaltransactiondeleteid'
		),
		'Transactiontype' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'Transactiontype',
			'foreignKey'    => 'transactiontype_id'
		),
		'Company' => array(
			'fields' =>array('companyname', 'companynamebn'),
			'className'    => 'Company',
			'foreignKey'    => 'company_id'
		),
		'Branch' => array(
			'fields' =>array('branchname', 'branchnamebn'),
			'className'    => 'Branch',
			'foreignKey'    => 'branch_id'
		),
		'User' => array(
			'fields' =>array('User.userfirstname', 'User.usermiddlename', 'User.userlastname'),
			'className'    => 'User',
			'foreignKey'    => 'user_id'
		),
		'Salesman' => array(
			'fields' =>array('User.userfirstname', 'User.usermiddlename', 'User.userlastname'),
			'className'    => 'User',
			'foreignKey'    => 'salesman_id'
		),
		'Coa' => array(
			'fields' =>array('User.userfirstname', 'User.usermiddlename', 'User.userlastname'),
			'className'    => 'Coa',
			'foreignKey'    => 'coa_id'
		)
		/*'Bankbranch' => array(
			'fields' =>array('Bankaccounttype.*'),
			'className'    => 'Bankbranch',
			'foreignKey'    => 'bankbranch_id'
		),
		'Bankaccounttype' => array(
			'fields' =>array('Bankaccounttype.*'),
			'className'    => 'Bankaccounttype',
			'foreignKey'    => 'bankaccounttype_id'
		),
		'Bankaccount' => array(
			'fields' =>array('Bankaccount.*'),
			'className'    => 'Bankaccount',
			'foreignKey'    => 'bankaccount_id'
		),
		'Bankchequebook' => array(
			'fields' =>array('Bankchequebook.*'),
			'className'    => 'Bankchequebook',
			'foreignKey'    => 'bankchequebook_id'
		)*/
	);
	var $virtualFields = array(
		'journaltransaction_date' => 'DATE(Journaltransaction.transactiondate)',
		'journaltransaction_particulars' => 'CONCAT(
			IF(Journaltransaction.transactiontype_id=0, "", IF(Journaltransaction.transactiontype_id=4,"Withdraw","Deposits")), 
			IF(Journaltransaction.user_id=0, CONCAT("/", Journaltransaction.journaltransactionothernote), CONCAT("/", User.userfirstname, " ", User.usermiddlename, " ", User.userlastname)), 
			IF(Journaltransaction.coa_id=0, " ", CONCAT("/", Coa.coaname))
		)',
		'journaltransaction_balance' => 'IF(Journaltransaction.transactionamount=0, "0.00", ROUND(Journaltransaction.transactionamount, 2))',
		'journaltransaction_withdraw' => 'IF(Journaltransaction.transactionamount<0,ROUND(Journaltransaction.transactionamount, 2), "0.00")',
		'journaltransaction_deposit' => 'IF(Journaltransaction.transactionamount>0,ROUND(Journaltransaction.transactionamount, 2), "0.00")', 
		'journaltransaction_chequenumber' => 'CONCAT( Journaltransaction.journaltransactionchequebookseries, " - ", Journaltransaction.journaltransactionchequesequencynumber )',
		'isActive' => 'IF(Journaltransaction.journaltransactionisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Journaltransaction.journaltransactionisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
}

?>	