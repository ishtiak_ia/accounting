<?php
App::import('Controller', 'Logins');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');
class GroupsController extends AppController {
	public $name = 'Groups';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'Form' => array('className' => 'BootstrapForm'));
	public $components = array('RequestHandler','Session');
	public $uses = array('User', 'Usernarration', 'Group', 'Groupnarration', 'Menu', 'Menunarration', 'Privilege', 'Company',);

	var $Logins;
	
	function beforeFilter(){
	        $this->Logins =& new LoginsController;
	        $this->Logins->constructClasses();
	        $this->Logins->__validateLoginStatus();
	        $Utilitys = new UtilitysController;
	        $this->set('Utilitys',$Utilitys);
	        if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
    }
    public  function beforeRender(){
		$this->response->disableCache();//prevent useless warnings for Ajax
		if($this->RequestHandler->isAjax()){
			Configure::write('debug', 0);
		}
	}
	public function groupinsertupdate($id = null, $uuid=null) {
		$this->layout='default';
		$this->set('title_for_layout', __('Insert/Edit Group').' | '.__(Configure::read('site_name')));
        /*if(!empty($id) && $id!=0):
			$coatype=$this->Coatype->find('all',array("fields" => "Coatype.*","conditions"=>array('Coatype.id'=>$id)));
			$this->set('coatype',$coatype);
		endif;*/
		if(!empty($id) && $id!=0) {
            $group = $this->Group->find('all', array("fields" => "Group.*", "conditions" => array('Group.id' => $id, 'Group.groupuuid'=>$uuid)));
            $this->set('group', $group);
        }
	}

	public function groupaddeditaction($id=null, $uuid = null){
		$userID = $this->Session->read('User.id');
		$id = @$this->request->data['Groups']['id'];
        $uuid = @$this->request->data['Groups']['groupuuid'];
		if ($this->request->is('post')) {
			//print_r($_POST); exit();
			if(!empty($id) && $id!=0):
                $this->request->data['Group']['groupupdateid']=$userID;
                $this->request->data['Group']['groupupdatedate']=date('Y-m-d H:i:s');
                $message = __('Update Successfully.');
                $this->Group->id = $id;
               // $this->Session->setFlash('Update Successfully.', 'default', array('class' => 'alert alert-success'));
            else:
	            $now = date('Y-m-d H:i:s');
	        	$this->request->data['Group']['groupinsertid']=$userID;
	            $this->request->data['Group']['groupinsertdate'] = $now;
	            $this->request->data['Group']['groupuuid']=String::uuid();
	            $message = __('Save Successfully.');
            endif;
            if ($this->Group->save($this->data)) {
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
			}
		}
		$this->redirect("/groups/groupmanage");

	}

	public function groupmanage() {
		$this->layout = 'default';
		$this->set('title_for_layout', __('Group Manage').' | '.__(Configure::read('site_name')));
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$groupId = array();
		if($group_id==1):
			$groupId[] = array('');
		elseif($group_id==2):
			$groupId[] = array('Group.id'=>$group_id);
		else:
			//$groupId[] = array('User.company_id'=>$company_id);
			//$groupId[] = array('User.branch_id'=>$branch_id);
		endif;

		$this->paginate = array(
            'fields' => 'Group.*',
            'conditions' => $groupId,
            'order'  =>'Group.id ASC',
            'limit' => 20
        );
        $groups = $this->paginate('Group');
        $this->set('groups',$groups);
	}

	public function groupdetailsbyid($id=null,$uuid=null){
        $this->layout='ajax';
        $groups=$this->Group->find('all',array("fields" => "Group.*","conditions"=>array('Group.id'=>$id,'Group.groupuuid'=>$uuid)));
        $this->set('groups',$groups);
    }

    public function groupsearchbytext($Searchtext=null){
		$this->layout='ajax';

		$Searchtext = @$this->request->data['Searchtext'];
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$groupId=$_SESSION["User"]["group_id"];
		$userId=$_SESSION["User"]["id"];

		$group_id = array();
		if($groupId==1):
			$group_id[] = array('');
		elseif($groupId==2):
			$group_id[] = array('Group.id'=>$groupId);
		else:
			$group_id[] = array('Group.id'=>$groupId);
			//$user_id[] = array('User.branch_id'=>$branch_id);
		endif;
		if(!empty($Searchtext) && $Searchtext!=''):
			$group_id[] = array(
				'OR' => array(
					'Group.groupname LIKE ' => '%'.$Searchtext.'%'
				)
			);
		else:
			$group_id[] = array();
		endif;

		$this->paginate = array(
			'fields' => 'Group.*',
			'order'  =>'Group.groupname ASC',
			"conditions"=>$group_id,
			'limit' => 20
		);
		$groups = $this->paginate('Group');
		$this->set('groups',$groups);
	}

	public function groupdelete($id = null,$uuid=null) {
		$this->layout='ajax';
		$userID = $this->Session->read('User.id');
		$this->request->data['Group']['groupdeleteid']=$userID;
		$this->request->data['Group']['groupdeletedate'] = date('Y-m-d H:i:s');
		$this->request->data['Group']['groupisactive']=2;
		$this->Group->id = $id;
		$this->Group->groupuuid = $uuid;
		if ($this->Group->save($this->data)) {
			$this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect("/groups/groupmanage");
	}
}