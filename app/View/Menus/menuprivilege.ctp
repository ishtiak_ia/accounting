<script>
    var usertype = document.getElementById("MenuprivilegeGroupId");
    /*if (usertype.options[usertype.selectedIndex].value === 'Admin') {
        alert("Please select  usertype");
        // return false;
    }*/
    function get_menuprivilege_result()
    {

        //var usertype = document.getElementById("MenuprivilegeGroupId");
        /*if (usertype.options[usertype.selectedIndex].value === '') {
            alert("Please select  usertype");
            return false;
        }*/
        $(".privilege_search_result_main").hide();
        var menuprivilegeGroupId = $("#MenuprivilegeGroupId").val();
        $.ajax({
            type: "post",
            url: "<?php echo $this->base; ?>/menus/searchmenuprivilege/",
            data: {menuprivilegeGroupId: menuprivilegeGroupId},
            async: true,
            beforeSend: function()
            {
                $(".privilege_search_result").html('<div class="loading"></div>');
            },
            success: function(data) {
                $('.privilege_search_result').html(data);
            }
        });

    }
</script>

<?php echo $this->Form->create('Menus', array('default' => false, 'class'=>' ')); ?>
<fieldset>  
    <legend><h3>Choose User Type</h3></legend>
    <table width="100%" border="0">
        <tr>
            <td class="label">User Type:</td>
            <td>
                <?php
                $option1 = array($grouptypelist);
                echo $this->Form->input("Menuprivilege.group_id", array('type' => 'select', "options" => $option1, 'empty' => 'Select UserType', 'div' => false, 'label' => false, "optgroup label" => false, 'style' => ''));
                ?>
                <!--<script>
                    //document.getElementById('MenuprivilegeGroupId').value = '<?php echo $_REQUEST['$this->data[Menuprivilege][group_id]']; ?>';
                </script>-->
                <?php echo $this->Form->submit('Search', array('div' => false, 'align' => 'right', 'formnovalidate' => 'true', 'style' => '', 'onClick' => 'get_menuprivilege_result();')); ?>
            </td>


        </tr>
    </table>
</fieldset> 
<br>
<br>
<?php echo $this->form->end(); ?> 
<div class="privilege_search_result">

</div>
