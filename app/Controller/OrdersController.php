<?php
App::import('Controller', 'Logins');
App::import('Controller', 'Transactions');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');

class OrdersController extends AppController {

	public $name = 'Orders';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session');
	public $components = array('RequestHandler','Session', 'Image');
	public $uses = array('Coa','Company','Bankaccount','Bank','Location','Product', 'Productbranch','User', 'Usergroup', 'Branch', 'Order','Orderdetail','Unit', 'Journaltransaction', 'Gltransaction');
	var $Logins;
	var $Transactions;

	function beforeFilter(){
		$this->Logins =& new LoginsController;
		$this->Logins->constructClasses();
		$this->Logins->__validateLoginStatus();
		$this->Transactions =& new TransactionsController;
		$this->Transactions->constructClasses();
		$Utilitys = new UtilitysController;
		$this->set('Utilitys',$Utilitys);
		if(!isset($_SESSION["User"]["id"])){
			$this->redirect("/logins/login");
		}
	}

	public function orderdetailsaddeditaction($id){
		$transactiontype_id=$_SESSION["ST_SALESORDER"];
		$companyID = $this->Session->read('User.company_id');
		$branch_id=$_SESSION["User"]["branch_id"];
		$this->request->data['Orderdetail']['order_id']=$id;
		$this->request->data['Orderdetail']['company_id']=$companyID;
		$this->request->data['Orderdetail']['branch_id']=$branch_id;
		$this->request->data['Orderdetail']['productcode']=$this->data['OrderdetailProductCode'];
		$this->request->data['Orderdetail']['product_id']=$this->data['OrderdetailProductId'];
		$this->request->data['Orderdetail']['quantity']=$this->data['OrderdetailQuantity'];
		$this->request->data['Orderdetail']['unitprice']=$this->data['OrderdetailPrice'];
		$this->request->data['Orderdetail']['discount']=$this->data['OrderdetailDiscount'];
		$this->request->data['Orderdetail']['totalprice']=$this->data['OrderdetailAmount'];
		$this->request->data['Orderdetail']['orderdetailuuid']=String::uuid();
		$this->request->data['Orderdetail']['transactiontype_id']=$transactiontype_id;
		$this->request->data['Orderdetail']['coa_id']=$this->data['OrderdetailCoaId'];
		$this->request->data['Orderdetail']['orderdetailmon']=$this->data['OrderdetailUnitMon'];
		$this->request->data['Orderdetail']['orderdetailkg']=$this->data['OrderdetailUnitKg'];
		$this->request->data['Orderdetail']['orderdetailgram']=$this->data['OrderdetailUnitGram'];
		$this->request->data['Orderdetail']['orderdetailmonunitprice']=$this->data['OrderdetailMonUnitPrice'];
		$this->request->data['Orderdetail']['orderdetailkgunitprice']=$this->data['OrderdetailKgUnitPrice'];
		$this->request->data['Orderdetail']['orderdetailgramunitprice']=$this->data['OrderdetailGramUnitPrice'];
		$this->Orderdetail->save($this->data);
	}
	public function orderinsertupdatection($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		$companyID = $this->Session->read('User.company_id');
		$branch_id=$_SESSION["User"]["branch_id"];

		$id = @$this->request->data['Orders']['id'];
		$uuid = @$this->request->data['Orders']['orderuuid'];
		/*echo $id.'<br>';
		echo $uuid;
		die();*/
		$transactiontype_id=$_SESSION["ST_SALESORDER"];
		$comment = $this->request->data['Order']['ordercomment'];
		$orderreferenceno = $this->request->data["Order"]["orderreferenceno"];
		$client_id = $this->request->data["Order"]["client_id"];
		$salesman_id = $this->request->data["Order"]["salesman_id"];
		$transactiondate = $this->request->data["Order"]["orderdate"];
		$grand_total = $this->request->data['Order']['transactionamount'];
		$paid_amount = $this->request->data['Order']['orderpayment'];
		$user_coa = $this->request->data['Order']['coa_id'];
		//print_r($_POST); exit();
		/*print '<pre>';
		print_r($this->request->data["Orderdetail"]);
		print '</pre>';
		die();*/
		if($this->request->is('post')){
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){

				$this->request->data['Order']['company_id'] = $companyID;
				$this->request->data['Order']['user_id'] = $userID;
				$this->request->data['Order']['transactiontype_id'] = $transactiontype_id;

				$this->request->data['Order']['orderdate']=$transactiondate;
				$this->request->data['Order']['orderreferenceno']=$orderreferenceno;
				$this->request->data['Order']['clientcode'] = $this->request->data["Order"]["clientcode"];
				$this->request->data['Order']['client_id'] = $client_id;
				$this->request->data['Order']['branch_id'] = $this->request->data["Order"]["branch_id"];
				$this->request->data['Order']['salesmancode']= $this->request->data['Order']['salesmancode'];
				$this->request->data['Order']['salesman_id']= $salesman_id;

				$this->request->data['Order']['orderupdateid'] = $userID;
				$this->request->data['Order']['orderupdatedate']=date('Y-m-d H:i:s');

				//$this->request->data['Order']['ordercustomerphone']=$this->request->data["Order"]["ordercustomerphone"]?$this->request->data["Order"]["ordercustomerphone"]:0;
				$this->request->data['Order']['ordertotal']=$this->request->data["Order"]["ordertotal"];
				$this->request->data['Order']['orderdiscount']=$this->request->data['Order']['orderdiscount'];
				$this->request->data['Order']['priceafterdiscount']=$this->request->data['Order']['priceafterdiscount'];
				$this->request->data['Order']['vat']=$this->request->data['Order']['vat'];
				$this->request->data['Order']['vatvalue']=$this->request->data['Order']['vatvalue'];
				$this->request->data['Order']['transportation_cost']=$this->request->data['Order']['transportation_cost'];
				$this->request->data['Order']['less']=$this->request->data['Order']['less'];
				$this->request->data['Order']['transactionamount']=$grand_total;
				$this->request->data['Order']['orderpayment']=$paid_amount;
				$this->request->data['Order']['orderdue']=$this->request->data['Order']['orderdue'];
				$this->request->data['Order']['ordercomment']=$comment;
				$this->request->data['Order']['orderisactive'] = 1;
				$this->Order->id = $id;
				//$this->request->data['Order']['orderduepaymentdate']=$this->request->data['Order']['orderduepaymentdate']?$this->request->data['Order']['orderduepaymentdate']:"0000-00-00 00:00:00";

			}else{
				$this->request->data['Order']['orderuuid'] = String::uuid();
				$this->request->data['Order']['user_id'] = $userID;
				$this->request->data['Order']['company_id'] = $companyID;
				$this->request->data['Order']['transactiontype_id'] = $transactiontype_id;
				$this->request->data['Order']['orderinsertid'] = $userID;
				$this->request->data['Order']['orderinsertdate']=date('Y-m-d H:i:s');

				$this->request->data['Order']['orderdate']=$transactiondate;
				$this->request->data['Order']['orderreferenceno']=$orderreferenceno;
				$this->request->data['Order']['clientcode'] = $this->request->data["Order"]["clientcode"];
				$this->request->data['Order']['client_id'] = $client_id;
				$this->request->data['Order']['branch_id'] = $this->request->data["Order"]["branch_id"];
				$this->request->data['Order']['salesmancode']= $this->request->data['Order']['salesmancode'];
				$this->request->data['Order']['salesman_id']= $salesman_id;
				//$this->request->data['Order']['ordercustomerphone']=$this->request->data["Order"]["ordercustomerphone"]?$this->request->data["Order"]["ordercustomerphone"]:0;
//				$this->request->data['Order']['ordercustomerphone']=$this->request->data["Order"]["ordercustomerphone"]?$this->request->data["Order"]["ordercustomerphone"]:0;
				$this->request->data['Order']['ordertotal']=$this->request->data["Order"]["ordertotal"];
				$this->request->data['Order']['orderdiscount']=$this->request->data['Order']['orderdiscount'];
				$this->request->data['Order']['priceafterdiscount']=$this->request->data['Order']['priceafterdiscount'];
				$this->request->data['Order']['vat']=$this->request->data['Order']['vat'];
				$this->request->data['Order']['vatvalue']=$this->request->data['Order']['vatvalue'];
				$this->request->data['Order']['transportation_cost']=$this->request->data['Order']['transportation_cost'];
				$this->request->data['Order']['less']=$this->request->data['Order']['less'];
				$this->request->data['Order']['transactionamount']=$grand_total;
				$this->request->data['Order']['orderpayment']=$paid_amount;
				$this->request->data['Order']['orderdue']=$this->request->data['Order']['orderdue'];
				$this->request->data['Order']['ordercomment']=$comment;
				//$this->request->data['Order']['orderduepaymentdate']=$this->request->data['Order']['orderduepaymentdate']?$this->request->data['Order']['orderduepaymentdate']:"0000-00-00 00:00:00";
				$this->request->data['Order']['orderisactive'] = 1;

			}
		}
		if( $this->Order->save($this->data)){
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
				$transaction_id = @$this->request->data['Orders']['id'];
			} else {
				$transaction_id = $this->Order->getLastInsertId();
			}
			$Orderdetail_data = $this->Orderdetail->find(
				'all', array(
					'conditions' => array('Orderdetail.transaction_id' => $transaction_id),
				)
			);
			if(!empty($Orderdetail_data)){
				$this->Orderdetail->deleteAll(array('Orderdetail.transaction_id' => $transaction_id));
			}
			$count = $this->request->data["totlarow"];
//			$forTest = [];

			for($i = 1 ;$i<=$count; $i++){
//				$forTest[$i] = $this->request->data["Orderdetail"]["product_id{$i}"];
				$this->request->data['Orderdetail']['orderdetailuuid']=String::uuid();
				$this->request->data['Orderdetail']['transaction_id'] = $transaction_id;
				$this->request->data['Orderdetail']['company_id']=$companyID;
				$this->request->data['Orderdetail']['branch_id']=$this->request->data["Order"]["branch_id"];
				$this->request->data['Orderdetail']['transactiontype_id'] = $transactiontype_id;
				$this->request->data['Orderdetail']['product_id']=$this->request->data["Orderdetail"]["product_id{$i}"];
				$this->request->data['Orderdetail']['coa_id'] = $user_coa;
				$this->request->data['Orderdetail']['productcode'] = $this->request->data["Orderdetail"]["productcode{$i}"];
				$this->request->data['Orderdetail']['quantity'] = $this->request->data["Orderdetail"]["quantity{$i}"];
//				$this->request->data['Orderdetail']['unit_id'] = $this->request->data["Orderdetail"]["unit_id{$i}"];
				$this->request->data['Orderdetail']['unitprice'] = $this->request->data["Orderdetail"]["unitprice{$i}"];
				$this->request->data['Orderdetail']['discount'] = $this->request->data["Orderdetail"]["discount{$i}"];
				$this->request->data['Orderdetail']['discountvalue'] = $this->request->data["Orderdetail"]["discountvalue{$i}"];
				$this->request->data['Orderdetail']['price'] = $this->request->data["Orderdetail"]["price{$i}"];
				$this->request->data['Orderdetail']['transactionamount'] = $this->request->data["Orderdetail"]["transactionamount{$i}"];
				$this->request->data['Orderdetail']['transactiondate'] = $transactiondate;
				$this->request->data['Orderdetail']['orderdetailisactive'] = 1;
				$this->request->data['Orderdetail']['orderdetailinsertid'] = $userID;
				$this->request->data['Orderdetail']['orderdetailinsertdate'] = date('Y-m-d H:i:s');
				$this->Orderdetail->create();
				$this->Orderdetail->save($this->data);
			}
			$this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $coa_id=9, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=4, $salesman_id, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, $grand_total, $journaltransactionothernote=0, $isActive=1, $step=0);
			$this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $coa_id=999, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=4, $salesman_id, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, -$grand_total, $journaltransactionothernote=0, $isActive=1, $step=1);
			$this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $coa_id=9, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=4, $salesman_id, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, -$paid_amount, $journaltransactionothernote=0, $isActive=1, $step=2);
			$this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $user_coa, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=4, $salesman_id, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, $paid_amount, $journaltransactionothernote=0, $isActive=1, $step=3);
			$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $grand_total, $coa_id=9, $client_id, $group_id = 4, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=0);
			$this->Transactions->gltransaction($transaction_id, $transactiontype_id, -$grand_total, $coa_id=999, $client_id, $group_id = 4, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=1);
			$this->Transactions->gltransaction($transaction_id, $transactiontype_id, -$paid_amount, $coa_id=9, $client_id, $group_id = 4, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=2);
			$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $paid_amount, $user_coa, $client_id, $group_id = 4, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=3);
			$this->Session->setFlash("Save Successfully", "default", array("class" => "alert alert-success"));
		}

		$this->redirect("/orders/ordermanage");
	}


	public function orderinsertupdatection_old($id = null, $uuid = null){


		$company_id = $this->Session->read('User.company_id');
		$branch_id=$_SESSION["User"]["branch_id"];
		$transactiontype_id=$_SESSION["ST_SALESORDER"];
		$userID = $this->Session->read('User.id');
		//pr($_POST);exit();
		if ($this->request->is('post')) {
			
			$this->request->data['Order']['coa_id']=$this->data['OrderCoaId'];
			$this->request->data['Order']['transactiontype_id']=$transactiontype_id;
			$this->request->data['Order']['orderuuid']=String::uuid();
			$this->request->data['Order']['company_id']=$company_id;
			$this->request->data['Order']['branch_id']=@$this->data['OrderBranchId']?$this->data['OrderBranchId']:$branch_id;
			$this->request->data['Order']['user_id']=$userID;

			$this->request->data['Order']['client_id']=$this->data['OrderClientId'];
			$this->request->data['Order']['orderreferenceno']=$this->data['OrderReferenceno'];
			$this->request->data['Order']['orderdate']=$this->data['OrderOrderdate'];
			$this->request->data['Order']['salesman_id']=$this->data['OrderSalesmanId'];

			$this->request->data['Order']['ordertotal']=$this->data['OrderTotalAmount'];
			$this->request->data['Order']['orderdiscount']=$this->data['OrderBonusDiscount']?$this->data['OrderBonusDiscount']:0;
			$this->request->data['Order']['transactionamount']=$this->data['OrderGrandTotal'];
			$this->request->data['Order']['orderpayment']=$this->data['OrderPayment'];
			$this->request->data['Order']['orderdue']=$this->data['OrderDue']?$this->data['OrderDue']:0;
			$this->request->data['Order']['orderduepaymentdate']=$this->data['OrderDuePaymentdate']?$this->data['OrderDuePaymentdate']:"0000-00-00 00:00:00";

			$this->request->data['Order']['paymentterm_id']=$this->data['paymenttermId'];
			
			///bank
			$this->request->data['Order']['ordercheckdate']=@$this->data['checkdate']?$this->data['checkdate']:0;
			$this->request->data['Order']['bank_id']=@$this->data['bankId']?$this->data['bankId']:0;
			$this->request->data['Order']['bankaccount_id']=@$this->data['OrderbankAccountId']?$this->data['OrderbankAccountId']:0;
			$this->request->data['Order']['bankbranch_id']=@$this->data['BanktransactionBankbranchId']?$this->data['BanktransactionBankbranchId']:0;
			$this->request->data['Order']['bankaccounttype_id']=@$this->data['BanktransactionBankaccounttypeId']?$this->data['BanktransactionBankaccounttypeId']:0;
			$this->request->data['Order']['banktransaction_id']=$_SESSION["ST_BANKDEPOSIT"];
			$this->request->data['Order']['OrderCheckNo']=@$this->data['OrderCheckNo']?$this->data['OrderCheckNo']:0;

			////
			$this->request->data['Order']['shipping_id']=$this->data['OrderShippingId'];
			$this->request->data['Order']['location_id']=$this->data['OrderLocationId'];
			$this->request->data['Order']['ordercomment']=$this->data['OrderComment'];

			if( $this->Order->save($this->data)){
				$transaction_id = $this->Order->getLastInsertId();
				$transactionamount = @$this->data['OrderGrandTotal']?$this->data['OrderGrandTotal']:0;
				$payment = @$this->data['OrderPayment']?$this->data['OrderPayment']:0;
				$transactiondate = $this->data['OrderOrderdate'];

				$clientsuppler_id = $this->data['OrderClientId'];
				$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $transactionamount, 6, $clientsuppler_id, $group_id = 4, $product_id=0, $transactiondate, $isActive=1, 1);

				$this->Transactions->usertransaction($transaction_id, $transactiontype_id, -$transactionamount, 3, $clientsuppler_id, $clientsupplergroup_id=4, $transactiondate,  $isActive=1, 1);
				$this->Transactions->gltransaction($transaction_id, $transactiontype_id, -$transactionamount, 3, $clientsuppler_id, $group_id = 4, $product_id=0, $transactiondate, $isActive=1, 2);


				$this->Transactions->usertransaction($transaction_id, $transactiontype_id, $payment, 3, $clientsuppler_id, $clientsupplergroup_id=4, $transactiondate,  $isActive=1, 2);
				$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $payment, 3, $clientsuppler_id, $group_id = 4, $product_id=0, $transactiondate, $isActive=1, 3);

				if($this->data['paymenttermId']==1):
					$bank_id = $this->data['bankId'];
					$bankbranch_id = $this->data['BanktransactionBankbranchId'];
					$bankaccounttype_id = $this->data['BanktransactionBankaccounttypeId'];
					$bankaccount_id = $this->data['OrderbankAccountId'];
					$bankchequebook_id = 0;
					$OrderCheckNo=explode("-", $this->data['OrderCheckNo']);
					$chequebookseries = $OrderCheckNo[0];
					$chequesequencynumber = $OrderCheckNo[1];
					$salesman_id = $this->data['OrderSalesmanId'];
					$this->Transactions->banktransaction($bank_id, $bankbranch_id, $bankaccounttype_id, $bankaccount_id, $bankchequebook_id, $chequebookseries, $chequesequencynumber, $transactiondate, $banktransactionothernote=0, $transaction_id, $transactiontype_id, $payment, 1, $clientsuppler_id, $group_id = 4, $salesman_id, $product_id=0, $banktransactionnn_id=0, $isActive=1);
					$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $payment, 1, $clientsuppler_id, $group_id = 4, $product_id=0, $transactiondate, $isActive=1, 4);
				else:
					$this->Transactions->cashtransaction($transaction_id, $transactiontype_id, $payment, 2, $clientsuppler_id, $group_id = 4, $product_id=0, $transactiondate, $banktansaction_id=0, $isActive=1);
					$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $payment, 2, $clientsuppler_id, $group_id = 4, $product_id=0, $transactiondate, $isActive=1, 5);
				endif;

				$product_id = $this->data['OrderdetailProductId'];
				$quantity = $this->data['OrderdetailQuantity'];
				$unit_id = $this->data['OrderUnitId'];
				$discount = $this->data['OrderdetailDiscount'];
				$orderdetailmon = $this->data['OrderdetailUnitMon'];
				$orderdetailkg = $this->data['OrderdetailUnitKg'];
				$orderdetailgram = $this->data['OrderdetailUnitGram'];
				$orderdetailmonunitprice = $this->data['OrderdetailMonUnitPrice'];
				$orderdetailkgunitprice = $this->data['OrderdetailKgUnitPrice'];
				$orderdetailgramunitprice = $this->data['OrderdetailGramUnitPrice'];
				$paymentType = $this->data['paymenttermId'];
				$transactionamount = $this->data['OrderdetailAmount'];
				$branchid = @$this->data['OrderBranchId']?$this->data['OrderBranchId']:$branch_id;

				$this->Transactions->purchasdetailstransaction($product_id, $company_id, $branchid, $transaction_id, $transactiontype_id, 8, -$quantity, $unit_id, $discount, -$orderdetailmon, -$orderdetailkg, -$orderdetailgram, $orderdetailmonunitprice, $orderdetailkgunitprice, $orderdetailgramunitprice, -$transactionamount, $clientsuppler_id, $transactiondate, $paymentType, $userID, $isActive);
				$this->Transactions->gltransaction($transaction_id, $transactiontype_id, -$transactionamount, 8, $clientsuppler_id, $group_id = 4, $product_id, $transactiondate, $isActive=1, 6);


				$message = 'Save successfully';
				$this->Session->setFlash($message, 'default', array('class' => 'alert alert-alert alert-success'));
			}
			//$this->redirect("/orders/ordermanage");
			echo"<script>window.location = '".$this->webroot."orders/ordermanage';</script>";
		}else{
			//$this->redirect("/orders/orderinsertupdate");
			echo"<script>window.location = '".$this->webroot."orders/orderinsertupdate';</script>";
		}
	}

	public function orderinsertupdate($id = null, $uuid=null){
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$bankaccount_id = array();
		$productlist_id = array();
		$customerlist_id = array();
		$salescenter_id = array();
		$salesmanlist_id = array();
		if($group_id==1):
		elseif($group_id==2):
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$productlist_id[] = array('Product.company_id'=>$company_id);
			$customerlist_id[] = array('Usergroup.company_id'=> $company_id);
			$salescenter_id[] = array('Branch.company_id'=> $company_id);
			$salesmanlist_id[] = array('User.company_id'=> $company_id);
		else:
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.branch_id'=>$branch_id);
			$productlist_id[] = array('Product.company_id'=>$company_id);
			$productlist_id[] = array('Product.branch_id'=>$branch_id);
			$customerlist_id[] = array('Usergroup.company_id'=> $company_id);
			$customerlist_id[] = array('Usergroup.branch_id'=> $branch_id);
			$salescenter_id[] = array('Branch.company_id'=> $company_id);
			$salescenter_id[] = array('Branch.id'=> $branch_id);
			$salesmanlist_id[] = array('User.company_id'=> $company_id);
			$salesmanlist_id[] = array('User.branch_id'=> $branch_id);
		endif;
		$customerlist_id[] = array('Usergroup.group_id'=> 4);
		$customerlist_id[] = array('Usergroup.usergroupisactive'=> 1);
		$salesmanlist_id[] = array('User.group_id'=> 5);
		$customerlist = $this->Usergroup->find(
			'list',
			array(
				"fields" => array("Usergroup.user_id", "Usergroup.user_fullname"),
				"conditions" =>$customerlist_id,
				"order" => "Usergroup.user_fullname ASC",
				'recursive' => 1
			)
		);

		$customerId = $this->Usergroup->find(
			'list',
			array(
				"fields" => array("Usergroup.user_id", "Usergroup.user_id"),


				"conditions" =>$customerlist_id,
				"order" => "Usergroup.user_fullname ASC",
				'recursive' => 1
			)
		);


		$this->set('customerlist',$customerlist);
		$this->set('customerId',$customerId);



		$salescenter = $this->Branch->find(
			'list',
			array(
				"fields" => array("branch_name"),
				"conditions" =>$salescenter_id,
				"order" => "Branch.branch_name ASC"
			)
		);
		$this->set('salescenter',$salescenter);
		$salesmanlist = $this->User->find(
			'list',
			array(
				"fields" => array("User.id","user_fullname"),
				"conditions" =>$salesmanlist_id,
				"order" => "User.user_fullname ASC"
			)
		);
		$salesmanId = $this->User->find(
			'list',
			array(
				"fields" => array("User.id","User.id"),
				"conditions" =>$salesmanlist_id,
				"order" => "User.user_fullname ASC"
			)
		);
		$this->set('salesmanlist',$salesmanlist);
		$this->set('salesmanId',$salesmanId);

		//$productlist_id[] = array('Product.producttype_id'=>1);
		$productlist = $this->Product->find(
			'list',
			array(
				"fields" => array("Product.id","product_name"),
				"conditions" => $productlist_id,
				"order" => "Product.product_name ASC"
			)
		);

		$this->set('productlist',$productlist);

		$productcodelist = $this->Product->find(
			'list',
			array(
				"fields" => array("Product.id","Product.productcode"),
				"conditions" => $productlist_id,
				"order" => "Product.productcode ASC"
			)
		);
		$productNameList = $this->Product->find(
			'list',
			array(
				"fields" => array("Product.id","Product.product_name"),
				"conditions" => $productlist_id,
				"order" => "Product.productcode ASC"
			)
		);

		$this->set('productcodelist',$productcodelist);
		$this->set('productNameList',$productNameList);
		$locationlist = $this->Location->find(
			'list',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions" =>array('Location.locationstep'=>4),
				"order" => "Location.location_name ASC"
			)
		);
		$this->set('locationlist',$locationlist);
		$banklist = $this->Bankaccount->find(
			'list',
			array(
				"fields" => array("Bankaccount.bank_id","Bankaccount.bank_name"),
				"conditions" => $bankaccount_id,
				"recursive" => 1,
				"order" => "Bankaccount.bank_name ASC"
			)
		);
		$this->set('banklist',$banklist);
		$bankaccount=$this->Bankaccount->find(
			'all',
			array(
				"fields" => array("id","bankaccount_name","bankaccounttype_id","bankbranch_id"),
				"conditions" => $bankaccount_id,
				"order" => "Bankaccount.bankaccount_name ASC"
			)
		);
		$this->set('bankaccount',$bankaccount);
		$unitlist=$this->Unit->find(
			'list',
			array(
				"fields" => array("id","unit_name"),
				"conditions" =>array('Unit.unitisactive'=>1)
			)
		);
		$this->set('unitlist',$unitlist);
		$coa_code=$this->Coa->find(
			'list',
			array(
				"fields" => array("id","coacode"),
				"conditions" => array('NOT'=>array('Coa.coaisactive'=>array(2)))
			)
		);
		$this->set('coa_code',$coa_code);
		$coa=$this->Coa->find(
			'list',
			array(
				"fields" => array("id","coa_name"),
				"conditions" => array('NOT'=>array('Coa.coaisactive'=>array(2)))
			)
		);
		$this->set('coa',$coa);

		//get voucher no code start
		$current_month = date('Y-m');
		$voucher_no = $this->Journaltransaction->find('first', array('fields' => array("max(voucher_no) as voucher_no"),'conditions' => array('Journaltransaction.voucher_no LIKE' => 'S-'.$current_month.'%'))); 
		$this->set('voucher_no',$voucher_no);
		//get voucher no code end

		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
			$sales = $this->Order->find('all', array("fields" => "Order.*", "conditions" => array('Order.id' => $id, 'Order.orderuuid'=>$uuid)));
			$this->set('sales', $sales);

			$orderdetails_data = $this->Orderdetail->find('all', array('fields' => 'Orderdetail.*', 'conditions'=>array('Orderdetail.transaction_id'=>$id)));
			$this->set('orderdetails_data', $orderdetails_data);

			$product_code_edit = $this->Productbranch->find(
				'list',
				array(
					"fields" => array("Productbranch.id","Productbranch.product_code"),
					'conditions'=>array('Productbranch.branch_id'=> $sales[0]['Order']['branch_id']),
					'recursive' => 1
				)
			);
			$this->set('product_code_edit', $product_code_edit);

			$product_list_edit = $this->Productbranch->find(
				'list',
				array(
					"fields" => array("Productbranch.id","Productbranch.product_name"),
					'conditions'=>array('Productbranch.branch_id'=> $sales[0]['Order']['branch_id']),
					'recursive' => 1
				)
			);
			$this->set('product_list_edit', $product_list_edit);
		}
	}
	//----------------------------Start Sales Return ----------------------
	public function salesreturnmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Sales Return').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$condition = array();
		
		if($group_id==1):
		elseif($group_id==2):
			$condition[] = array('Order.company_id'=>$company_id);			
		else:
			$condition[] = array('Order.company_id'=>$company_id);
			$condition[] = array('Order.branch_id'=>$branch_id);			
		endif;
		$condition[] = array('Order.transactiontype_id'=>$_SESSION["ST_SALESRETURN"]);
		$this->paginate = array(
			'fields' => 'Order.*',
			'conditions'=> $condition,
			'order'  =>'Order.id DESC',
			'limit' => 20
		);
		$order = $this->paginate('Order');
		$this->set('order',$order);
	}
	public function salesreturninsertupdate($id = null, $uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Sales Return').' | '.__(Configure::read('site_name')));
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$bankaccount_id = array();
		$productlist_id = array();
		$customerlist_id = array();
		$salescenter_id = array();
		$salesmanlist_id = array();
		if($group_id==1):
		elseif($group_id==2):
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$productlist_id[] = array('Product.company_id'=>$company_id);
			$customerlist_id[] = array('Usergroup.company_id'=> $company_id);
			$salescenter_id[] = array('Branch.company_id'=> $company_id);
			$salesmanlist_id[] = array('User.company_id'=> $company_id);
		else:
			$bankaccount_id[] = array('Bankaccount.company_id'=>$company_id);
			$bankaccount_id[] = array('Bankaccount.branch_id'=>$branch_id);
			$productlist_id[] = array('Product.company_id'=>$company_id);
			$productlist_id[] = array('Product.branch_id'=>$branch_id);
			$customerlist_id[] = array('Usergroup.company_id'=> $company_id);
			$customerlist_id[] = array('Usergroup.branch_id'=> $branch_id);
			$salescenter_id[] = array('Branch.company_id'=> $company_id);
			$salescenter_id[] = array('Branch.id'=> $branch_id);
			$salesmanlist_id[] = array('User.company_id'=> $company_id);
			$salesmanlist_id[] = array('User.branch_id'=> $branch_id);
		endif;
		$customerlist_id[] = array('Usergroup.group_id'=> 4);
		$customerlist_id[] = array('Usergroup.usergroupisactive'=> 1);
		$salesmanlist_id[] = array('User.group_id'=> 5);
		$customerlist = $this->Usergroup->find(
			'list',
			array(
				"fields" => array("Usergroup.user_id", "Usergroup.user_fullname"),
				"conditions" =>$customerlist_id,
				"order" => "Usergroup.user_fullname ASC",
				'recursive' => 1
			)
		);

		$customerId = $this->Usergroup->find(
			'list',
			array(
				"fields" => array("Usergroup.user_id", "Usergroup.user_id"),


				"conditions" =>$customerlist_id,
				"order" => "Usergroup.user_fullname ASC",
				'recursive' => 1
			)
		);


		$this->set('customerlist',$customerlist);
		$this->set('customerId',$customerId);



		$salescenter = $this->Branch->find(
			'list',
			array(
				"fields" => array("branch_name"),
				"conditions" =>$salescenter_id,
				"order" => "Branch.branch_name ASC"
			)
		);
		$this->set('salescenter',$salescenter);
		$salesmanlist = $this->User->find(
			'list',
			array(
				"fields" => array("User.id","user_fullname"),
				"conditions" =>$salesmanlist_id,
				"order" => "User.user_fullname ASC"
			)
		);
		$salesmanId = $this->User->find(
			'list',
			array(
				"fields" => array("User.id","User.id"),
				"conditions" =>$salesmanlist_id,
				"order" => "User.user_fullname ASC"
			)
		);
		$this->set('salesmanlist',$salesmanlist);
		$this->set('salesmanId',$salesmanId);

		//$productlist_id[] = array('Product.producttype_id'=>1);
		$productlist = $this->Product->find(
			'list',
			array(
				"fields" => array("Product.id","product_name"),
				"conditions" => $productlist_id,
				"order" => "Product.product_name ASC"
			)
		);

		$this->set('productlist',$productlist);

		$productcodelist = $this->Product->find(
			'list',
			array(
				"fields" => array("Product.id","Product.productcode"),
				"conditions" => $productlist_id,
				"order" => "Product.productcode ASC"
			)
		);
		$productNameList = $this->Product->find(
			'list',
			array(
				"fields" => array("Product.id","Product.product_name"),
				"conditions" => $productlist_id,
				"order" => "Product.productcode ASC"
			)
		);

		$this->set('productcodelist',$productcodelist);
		$this->set('productNameList',$productNameList);
		$locationlist = $this->Location->find(
			'list',
			array(
				"fields" => array("Location.id","Location.location_name"),
				"conditions" =>array('Location.locationstep'=>4),
				"order" => "Location.location_name ASC"
			)
		);
		$this->set('locationlist',$locationlist);
		$banklist = $this->Bankaccount->find(
			'list',
			array(
				"fields" => array("Bankaccount.bank_id","Bankaccount.bank_name"),
				"conditions" => $bankaccount_id,
				"recursive" => 1,
				"order" => "Bankaccount.bank_name ASC"
			)
		);
		$this->set('banklist',$banklist);
		$bankaccount=$this->Bankaccount->find(
			'all',
			array(
				"fields" => array("id","bankaccount_name","bankaccounttype_id","bankbranch_id"),
				"conditions" => $bankaccount_id,
				"order" => "Bankaccount.bankaccount_name ASC"
			)
		);
		$this->set('bankaccount',$bankaccount);
		$unitlist=$this->Unit->find(
			'list',
			array(
				"fields" => array("id","unit_name"),
				"conditions" =>array('Unit.unitisactive'=>1)
			)
		);
		$this->set('unitlist',$unitlist);
		$coa_code=$this->Coa->find(
			'list',
			array(
				"fields" => array("id","coacode"),
				"conditions" => array('NOT'=>array('Coa.coaisactive'=>array(2)))
			)
		);
		$this->set('coa_code',$coa_code);
		$coa=$this->Coa->find(
			'list',
			array(
				"fields" => array("id","coa_name"),
				"conditions" => array('NOT'=>array('Coa.coaisactive'=>array(2)))
			)
		);
		$this->set('coa',$coa);

		//get voucher no code start
		$current_month = date('Y-m');
		$voucher_no = $this->Journaltransaction->find('first', array('fields' => array("max(voucher_no) as voucher_no"),'conditions' => array('Journaltransaction.voucher_no LIKE' => 'SR-'.$current_month.'%'))); 
		$this->set('voucher_no',$voucher_no);
		//get voucher no code end

		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
			$sales = $this->Order->find('all', array("fields" => "Order.*", "conditions" => array('Order.id' => $id, 'Order.orderuuid'=>$uuid)));
			$this->set('sales', $sales);

			$orderdetails_data = $this->Orderdetail->find('all', array('fields' => 'Orderdetail.*', 'conditions'=>array('Orderdetail.transaction_id'=>$id)));
			$this->set('orderdetails_data', $orderdetails_data);

			$product_code_edit = $this->Productbranch->find(
				'list',
				array(
					"fields" => array("Productbranch.id","Productbranch.product_code"),
					'conditions'=>array('Productbranch.branch_id'=> $sales[0]['Order']['branch_id']),
					'recursive' => 1
				)
			);
			$this->set('product_code_edit', $product_code_edit);

			$product_list_edit = $this->Productbranch->find(
				'list',
				array(
					"fields" => array("Productbranch.id","Productbranch.product_name"),
					'conditions'=>array('Productbranch.branch_id'=> $sales[0]['Order']['branch_id']),
					'recursive' => 1
				)
			);
			$this->set('product_list_edit', $product_list_edit);
		}
	}
	public function salesreturninsertupdateaction($id=null,$uuid=null){
		$userID = $this->Session->read('User.id');
		$companyID = $this->Session->read('User.company_id');
		$branch_id=$_SESSION["User"]["branch_id"];

		$id = @$this->request->data['Orders']['id'];
		$uuid = @$this->request->data['Orders']['orderuuid'];
		/*echo $id.'<br>';
		echo $uuid;
		die();*/
		$transactiontype_id=$_SESSION["ST_SALESRETURN"];
		$comment = $this->request->data['Order']['ordercomment'];
		$orderreferenceno = $this->request->data["Order"]["orderreferenceno"];
		$client_id = $this->request->data["Order"]["client_id"];
		$salesman_id = $this->request->data["Order"]["salesman_id"];
		$transactiondate = $this->request->data["Order"]["orderdate"];
		$grand_total = $this->request->data['Order']['transactionamount'];
		$paid_amount = $this->request->data['Order']['orderpayment'];
		$user_coa = $this->request->data['Order']['coa_id'];
		//print_r($_POST); exit();
		/*print '<pre>';
		print_r($this->request->data["Orderdetail"]);
		print '</pre>';
		die();*/
		if($this->request->is('post')){
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){

				$this->request->data['Order']['company_id'] = $companyID;
				$this->request->data['Order']['user_id'] = $userID;
				$this->request->data['Order']['transactiontype_id'] = $transactiontype_id;

				$this->request->data['Order']['orderdate']=$transactiondate;
				$this->request->data['Order']['orderreferenceno']=$orderreferenceno;
				$this->request->data['Order']['clientcode'] = $this->request->data["Order"]["clientcode"];
				$this->request->data['Order']['client_id'] = $client_id;
				$this->request->data['Order']['branch_id'] = $this->request->data["Order"]["branch_id"];
				$this->request->data['Order']['salesmancode']= $this->request->data['Order']['salesmancode'];
				$this->request->data['Order']['salesman_id']= $salesman_id;

				$this->request->data['Order']['orderupdateid'] = $userID;
				$this->request->data['Order']['orderupdatedate']=date('Y-m-d H:i:s');

				//$this->request->data['Order']['ordercustomerphone']=$this->request->data["Order"]["ordercustomerphone"]?$this->request->data["Order"]["ordercustomerphone"]:0;
				$this->request->data['Order']['ordertotal']=$this->request->data["Order"]["ordertotal"];
				$this->request->data['Order']['orderdiscount']=$this->request->data['Order']['orderdiscount'];
				$this->request->data['Order']['priceafterdiscount']=$this->request->data['Order']['priceafterdiscount'];
				$this->request->data['Order']['vat']=$this->request->data['Order']['vat'];
				$this->request->data['Order']['vatvalue']=$this->request->data['Order']['vatvalue'];
				$this->request->data['Order']['transportation_cost']=$this->request->data['Order']['transportation_cost'];
				$this->request->data['Order']['less']=$this->request->data['Order']['less'];
				$this->request->data['Order']['transactionamount']=$grand_total;
				$this->request->data['Order']['orderpayment']=$paid_amount;
				$this->request->data['Order']['orderdue']=$this->request->data['Order']['orderdue'];
				$this->request->data['Order']['ordercomment']=$comment;
				$this->request->data['Order']['orderisactive'] = 1;
				$this->Order->id = $id;
				//$this->request->data['Order']['orderduepaymentdate']=$this->request->data['Order']['orderduepaymentdate']?$this->request->data['Order']['orderduepaymentdate']:"0000-00-00 00:00:00";

			}else{
				$this->request->data['Order']['orderuuid'] = String::uuid();
				$this->request->data['Order']['user_id'] = $userID;
				$this->request->data['Order']['company_id'] = $companyID;
				$this->request->data['Order']['transactiontype_id'] = $transactiontype_id;
				$this->request->data['Order']['orderinsertid'] = $userID;
				$this->request->data['Order']['orderinsertdate']=date('Y-m-d H:i:s');

				$this->request->data['Order']['orderdate']=$transactiondate;
				$this->request->data['Order']['orderreferenceno']=$orderreferenceno;
				$this->request->data['Order']['clientcode'] = $this->request->data["Order"]["clientcode"];
				$this->request->data['Order']['client_id'] = $client_id;
				$this->request->data['Order']['branch_id'] = $this->request->data["Order"]["branch_id"];
				$this->request->data['Order']['salesmancode']= $this->request->data['Order']['salesmancode'];
				$this->request->data['Order']['salesman_id']= $salesman_id;
				//$this->request->data['Order']['ordercustomerphone']=$this->request->data["Order"]["ordercustomerphone"]?$this->request->data["Order"]["ordercustomerphone"]:0;
//				$this->request->data['Order']['ordercustomerphone']=$this->request->data["Order"]["ordercustomerphone"]?$this->request->data["Order"]["ordercustomerphone"]:0;
				$this->request->data['Order']['ordertotal']=$this->request->data["Order"]["ordertotal"];
				$this->request->data['Order']['orderdiscount']=$this->request->data['Order']['orderdiscount'];
				$this->request->data['Order']['priceafterdiscount']=$this->request->data['Order']['priceafterdiscount'];
				$this->request->data['Order']['vat']=$this->request->data['Order']['vat'];
				$this->request->data['Order']['vatvalue']=$this->request->data['Order']['vatvalue'];
				$this->request->data['Order']['transportation_cost']=$this->request->data['Order']['transportation_cost'];
				$this->request->data['Order']['less']=$this->request->data['Order']['less'];
				$this->request->data['Order']['transactionamount']=$grand_total;
				$this->request->data['Order']['orderpayment']=$paid_amount;
				$this->request->data['Order']['orderdue']=$this->request->data['Order']['orderdue'];
				$this->request->data['Order']['ordercomment']=$comment;
				//$this->request->data['Order']['orderduepaymentdate']=$this->request->data['Order']['orderduepaymentdate']?$this->request->data['Order']['orderduepaymentdate']:"0000-00-00 00:00:00";
				$this->request->data['Order']['orderisactive'] = 1;

			}
		}
		if( $this->Order->save($this->data)){
			if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
				$transaction_id = @$this->request->data['Orders']['id'];
			} else {
				$transaction_id = $this->Order->getLastInsertId();
			}
			$Orderdetail_data = $this->Orderdetail->find(
				'all', array(
					'conditions' => array('Orderdetail.transaction_id' => $transaction_id),
				)
			);
			if(!empty($Orderdetail_data)){
				$this->Orderdetail->deleteAll(array('Orderdetail.transaction_id' => $transaction_id));
			}
			$count = $this->request->data["totlarow"];
//			$forTest = [];

			for($i = 1 ;$i<=$count; $i++){
//				$forTest[$i] = $this->request->data["Orderdetail"]["product_id{$i}"];
				$this->request->data['Orderdetail']['orderdetailuuid']=String::uuid();
				$this->request->data['Orderdetail']['transaction_id'] = $transaction_id;
				$this->request->data['Orderdetail']['company_id']=$companyID;
				$this->request->data['Orderdetail']['branch_id']=$this->request->data["Order"]["branch_id"];
				$this->request->data['Orderdetail']['transactiontype_id'] = $transactiontype_id;
				$this->request->data['Orderdetail']['product_id']=$this->request->data["Orderdetail"]["product_id{$i}"];
				$this->request->data['Orderdetail']['coa_id'] = $user_coa;
				$this->request->data['Orderdetail']['productcode'] = $this->request->data["Orderdetail"]["productcode{$i}"];
				$this->request->data['Orderdetail']['quantity'] = $this->request->data["Orderdetail"]["quantity{$i}"];
//				$this->request->data['Orderdetail']['unit_id'] = $this->request->data["Orderdetail"]["unit_id{$i}"];
				$this->request->data['Orderdetail']['unitprice'] = $this->request->data["Orderdetail"]["unitprice{$i}"];
				$this->request->data['Orderdetail']['discount'] = $this->request->data["Orderdetail"]["discount{$i}"];
				$this->request->data['Orderdetail']['discountvalue'] = $this->request->data["Orderdetail"]["discountvalue{$i}"];
				$this->request->data['Orderdetail']['price'] = $this->request->data["Orderdetail"]["price{$i}"];
				$this->request->data['Orderdetail']['transactionamount'] = $this->request->data["Orderdetail"]["transactionamount{$i}"];
				$this->request->data['Orderdetail']['transactiondate'] = $transactiondate;
				$this->request->data['Orderdetail']['orderdetailisactive'] = 1;
				$this->request->data['Orderdetail']['orderdetailinsertid'] = $userID;
				$this->request->data['Orderdetail']['orderdetailinsertdate'] = date('Y-m-d H:i:s');
				$this->Orderdetail->create();
				$this->Orderdetail->save($this->data);
			}
//			print_r($forTest); die();
			$this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $coa_id=9, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=4, $salesman_id, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, -$grand_total, $journaltransactionothernote=0, $isActive=1, $step=0);
			$this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $coa_id=999, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=4, $salesman_id, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, $grand_total, $journaltransactionothernote=0, $isActive=1, $step=1);
			$this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $coa_id=9, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=4, $salesman_id, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, $paid_amount, $journaltransactionothernote=0, $isActive=1, $step=2);
			$this->Transactions->journaltransaction($bank_id=0, $bankbranch_id=0, $bankaccounttype_id=0, $bankaccount_id=0, $journaltransactiontype_id=0, $transactiontype_id, $transaction_id, $user_coa, $particular=null, $cf_code=0, $comment, $orderreferenceno, $client_id, $group_id=4, $salesman_id, $bankchequebook_id=0, $journaltransactionchequebookseries=0, $journaltransactionchequesequencynumber=0, $transactiondate, -$paid_amount, $journaltransactionothernote=0, $isActive=1, $step=3);
			$this->Transactions->gltransaction($transaction_id, $transactiontype_id, -$grand_total, $coa_id=9, $client_id, $group_id = 4, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=0);
			$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $grand_total, $coa_id=999, $client_id, $group_id = 4, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=1);
			$this->Transactions->gltransaction($transaction_id, $transactiontype_id, $paid_amount, $coa_id=9, $client_id, $group_id = 4, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=2);
			$this->Transactions->gltransaction($transaction_id, $transactiontype_id, -$paid_amount, $user_coa, $client_id, $group_id = 4, $product_id=0, $transactiondate, $orderreferenceno, $isActive=1, $step=3);
//			die();
			$this->Session->setFlash("Save Successfully", "default", array("class" => "alert alert-success"));
		}

		$this->redirect("/orders/salesreturnmanage");
	}


	//-------------------------------End Sales Return------------------------

	public function qtyunitpricebyproduct(){
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$json_array = array();
		$OrderdetailProductId = @$this->request->data['OrderdetailProductId'];
		$OrderdetailBranchId = @$this->request->data['OrderdetailBranchId'];
		$productquantityquery=$this->Orderdetail->find(
			'all',
			array(
				"fields" => array("SUM(Orderdetail.quantity) AS totalproductquantity, IF(orderdetailmon=0,0,SUM(orderdetailmon) ) AS orderdetailmon, IF(orderdetailkg=0,0,SUM(orderdetailkg) ) AS orderdetailkg, IF(orderdetailgram=0,0,SUM(orderdetailgram) ) AS orderdetailgram"),
				"conditions" => array(
					'Orderdetail.product_id'=>$OrderdetailProductId,
					'Orderdetail.branch_id'=>$OrderdetailBranchId
				)
			)
		);
		$producttotalpricequery=$this->Orderdetail->find(
			'all',
			array(
				"fields" => array("SUM(Orderdetail.transactionamount) AS producttotalprice"),
				"conditions" => array(
					'Orderdetail.product_id'=>$OrderdetailProductId,
					'Orderdetail.branch_id'=>$OrderdetailBranchId
				)
			)
		);
		$productcodequery=$this->Productbranch->find(
			'first',
			array(
				"fields" => array("Product.id","Product.productcode", "Product.unit_id", "Product.productprice", "Unit.*"),
				"conditions" => array(
					'Productbranch.product_id'=>$OrderdetailProductId,
					'Productbranch.branch_id'=>$OrderdetailBranchId
				)
			)
		);
		$productdafultprice=@$productcodequery["Product"]["productprice"]?$productcodequery["Product"]["productprice"]:0;
		$totalprices = @$producttotalpricequery[0][0]["producttotalprice"];
		$totalqtys = @$productquantityquery[0][0]["totalproductquantity"];
		if($totalprices==0){$totalprices=1;}
		if($totalqtys==0){$totalqtys=1;}
		$totalpriceCalculate= $totalprices/$totalqtys;
		$orderdetailmon = @$productquantityquery[0][0]["orderdetailmon"]?$productquantityquery[0][0]["orderdetailmon"]:0;
		$orderdetailkg = @$productquantityquery[0][0]["orderdetailkg"]?$productquantityquery[0][0]["orderdetailkg"]:0;
		$orderdetailgram = @$productquantityquery[0][0]["orderdetailgram"]?$productquantityquery[0][0]["orderdetailgram"]:0;
		$productquantity=@$productquantityquery[0][0]["totalproductquantity"]?$productquantityquery[0][0]["totalproductquantity"]:0;
		$producttotalprice=@$producttotalpricequery[0][0]["producttotalprice"]?$producttotalpricequery[0][0]["producttotalprice"]:0;
		$productunitprice="{$totalpriceCalculate}";
		$productcode=@$productcodequery["Product"]["productcode"]?$productcodequery["Product"]["productcode"]:0;
		$productid=@$productcodequery["Product"]["id"]?$productcodequery["Product"]["id"]:0;
		$productunit=@$productcodequery["Product"]["unit_id"]?$productcodequery["Product"]["unit_id"]:0;
		$unitmon=@$productcodequery["Unit"]["unitmon"]?$productcodequery["Unit"]["unitmon"]:0;
		$unitkilogram=@$productcodequery["Unit"]["unitkilogram"]?$productcodequery["Unit"]["unitkilogram"]:0;
		$unitgram=@$productcodequery["Unit"]["unitgram"]?$productcodequery["Unit"]["unitgram"]:0;
		$json_array["productunitprice"] = $productunitprice;
		$json_array["producttotalprice"] = $producttotalprice;
		$json_array["productquantity"] = $productquantity;
		$json_array["productcode"] = $productcode;
		$json_array["productid"] = $productid;
		$json_array["productunit"] = $productunit;
		$json_array["orderdetailmon"] = $orderdetailmon;
		$json_array["orderdetailkg"] = $orderdetailkg;
		$json_array["orderdetailgram"] = $orderdetailgram;
		$json_array["unitmon"] = $unitmon;
		$json_array["unitkilogram"] = $unitkilogram;
		$json_array["unitgram"] = $unitgram;
		$json_array["productdafultprice"] = $productdafultprice?$productdafultprice:$productunitprice;
		return json_encode($json_array);
	}
	public function manufacturedinsertupdate($id=null, $uuid=null){
		$this->layout='default';
		$this->set('title_for_layout', __('Manufactured Product').' | '.__(Configure::read('site_name')));
		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];
		$bankaccount_id = array();
		$productlist_id = array();
		$customerlist_id = array();
		$salescenter_id = array();
		$salesmanlist_id = array();
		if($group_id==1):
		elseif($group_id==2):
			$productlist_id[] = array('Product.company_id'=>$company_id);
			$salescenter_id[] = array('Branch.company_id'=> $company_id);
		else:
			$productlist_id[] = array('Product.company_id'=>$company_id);
			$productlist_id[] = array('Product.branch_id'=>$branch_id);
			$salescenter_id[] = array('Branch.company_id'=> $company_id);
			$salescenter_id[] = array('Branch.branch_id'=> $company_id);
		endif;

		$manufacturedproductlist_id[] = array('Product.producttype_id'=>1);
		$manufacturedproductlist = $this->Product->find(
			'list',
			array(
				"fields" => array("Product.id","product_name"),
				"conditions" => array($productlist_id,$manufacturedproductlist_id),
				"order" => "Product.product_name ASC"
			)
		);
		$this->set('manufacturedproductlist',$manufacturedproductlist);

		$purchaseproductlist_id[] = array('Product.producttype_id'=>4);
		$purchaseproductlist = $this->Product->find(
			'list',
			array(
				"fields" => array("Product.id","product_name"),
				"conditions" => array($productlist_id,$purchaseproductlist_id),
				"order" => "Product.product_name ASC"
			)
		);
		$this->set('purchaseproductlist',$purchaseproductlist);

		$salescenter = $this->Branch->find(
			'list',
			array(
				"fields" => array("branch_name"),
				"conditions" =>$salescenter_id,
				"order" => "Branch.branch_name ASC"
			)
		);
		$this->set('salescenter',$salescenter);
		if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
			$manufacturedproduct=$this->Orderdetail->find('all',array("fields" => "Orderdetail.*,Product.*","conditions"=>array('Orderdetail.id'=>$id,'Orderdetail.orderdetailuuid'=>$uuid)));
			$this->set('manufacturedproduct',$manufacturedproduct);
			$purchaseproduct=$this->Orderdetail->find('all',array("fields" => "Orderdetail.*,Product.*","conditions"=>array('Orderdetail.transaction_id'=>$id,'Orderdetail.transactiontype_id'=>$_SESSION["ST_MANURECEIVE"])));
			$this->set('purchaseproduct',$purchaseproduct);
		endif;
	}

	public function manufacturedinsertupdateaction(){
		$manufactureproduct_id = $this->request->data['Order']['manufactureproduct_id'];
		$purchaseproduct_id = $this->request->data['Order']['purchaseproduct_id'];
		$company_id = $_SESSION["User"]["company_id"];
		$branch_id = $this->request->data['Orderdetail']['branch_id'];
		$transaction_id = 0;
		$transactiontype_id = $_SESSION["ST_MANURECEIVE"];
		$coa_id = 8;
		$manufacturedproductstockunit = $this->request->data['Orders']['manufacturedproductstockunit'];
		$purchaseproductstockunit = $this->request->data['Orders']['purchaseproductstockunit'];
		$purchaseproductunitmon= $this->request->data['Orders']['purchaseproductunitmon'];
		$purchaseproductunitkg= $this->request->data['Orders']['purchaseproductunitkg'];
		$purchaseproductunitgram= $this->request->data['Orders']['purchaseproductunitgram'];
		$discount = 0;
		$orderdetailmon = $this->request->data['Orderdetail']['orderdetailmon'];
		$orderdetailkg = $this->request->data['Orderdetail']['orderdetailkg'];
		$orderdetailgram = $this->request->data['Orderdetail']['orderdetailgram'];
		$manufactureorderdetailmonunitprice = $this->request->data['Orders']['manufacturedproducttransactionamount']/$orderdetailmon;
		$manufactureorderdetailkgunitprice = $this->request->data['Orders']['manufacturedproducttransactionamount']/$orderdetailkg;
		$manufactureorderdetailgramunitprice = $this->request->data['Orders']['manufacturedproducttransactionamount']/$orderdetailgram;
		$manufacturedproducttransactionamount = $this->request->data['Orders']['manufacturedproducttransactionamount'];
		$purchaseproducttransactionamount = $this->request->data['Orders']['purchaseproducttransactionamount'];
		$manufacturequantity = $this->request->data['Orderdetail']['quantity'];
		/*if($purchaseproductstockunit == 1):
			$purchasequantity = $orderdetailmon;
		elseif($purchaseproductstockunit == 2):
			$purchasequantity = $orderdetailkg;
		else:
			$purchasequantity = $orderdetailgram;
		endif;*/
		if($purchaseproductunitmon == 1):
			$purchasequantity = $orderdetailmon;
		elseif($purchaseproductunitkg == 1):
			$purchasequantity = $orderdetailkg;
		else:
			$purchasequantity = $orderdetailgram;
		endif;
		$purchaseorderdetailmonunitprice = $this->request->data['Orders']['purchaseproducttransactionamount']/$orderdetailmon;
		$purchaseorderdetailkgunitprice = $this->request->data['Orders']['purchaseproducttransactionamount']/$orderdetailkg;
		$purchaseorderdetailgramunitprice = $this->request->data['Orders']['purchaseproducttransactionamount']/$orderdetailgram;
		$clientsuppler_id = 0;
		$transactiondate = date('Y-m-d H:i:s');
		$paymentType = 0;
		$user_id = $_SESSION["User"]["id"];
		$isActive = 1;
		$manufacturedorderdetailID = @$this->request->data['Orders']['manufacturedproduct_id']?$this->request->data['Orders']['manufacturedproduct_id']:0;
		$this->Orderdetail->id = $manufacturedorderdetailID;
		//pr($this->request->data);exit();
		$manufactureproductinsert = $this->Transactions->purchasdetailstransaction($manufactureproduct_id, $company_id, $branch_id, $transaction_id, $transactiontype_id, $coa_id, $manufacturequantity, $manufacturedproductstockunit, $discount, $orderdetailmon, $orderdetailkg, $orderdetailgram, $manufactureorderdetailmonunitprice, $manufactureorderdetailkgunitprice, $manufactureorderdetailgramunitprice, $manufacturedproducttransactionamount, $clientsuppler_id, $transactiondate, $paymentType, $user_id, $isActive);
		//pr($manufactureproductinsert);exit();
		$purchasesorderdetailID = @$this->request->data['Orders']['purchasesproduct_id']?$this->request->data['Orders']['purchasesproduct_id']:0;
		$this->Orderdetail->id = $purchasesorderdetailID;
		$transaction_id = $manufacturedorderdetailID?$manufacturedorderdetailID:$manufactureproductinsert;
		$purchaseproductinsert = $this->Transactions->purchasdetailstransaction($purchaseproduct_id, $company_id, $branch_id, $transaction_id, $transactiontype_id, $coa_id, -$purchasequantity, $purchaseproductstockunit, $discount, -$orderdetailmon, -$orderdetailkg, -$orderdetailgram, $purchaseorderdetailmonunitprice, $purchaseorderdetailkgunitprice, $purchaseorderdetailgramunitprice, -$purchaseproducttransactionamount, $clientsuppler_id, $transactiondate, $paymentType, $user_id, $isActive);
		$this->Session->setFlash(__('Save Successfully.'), 'default', array('class' => 'alert alert-success'));
		$this->redirect("/orders/manufacturedmanage");
	}
	
	public function ordermanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Order').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$condition = array();
		
		if($group_id==1):
		elseif($group_id==2):
			$condition[] = array('Order.company_id'=>$company_id);			
		else:
			$condition[] = array('Order.company_id'=>$company_id);
			$condition[] = array('Order.branch_id'=>$branch_id);			
		endif;
		$condition[] = array('Order.transactiontype_id'=>$_SESSION["ST_SALESORDER"]);
		$this->paginate = array(
			'fields' => 'Order.*',
			'conditions'=> $condition,
			'order'  =>'Order.id DESC',
			'limit' => 20
		);
		$order = $this->paginate('Order');
		$this->set('order',$order);

	
	}
	public function orderdetailmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Orderdetail').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$condition = array();
		
		if($group_id==1):
		elseif($group_id==2):
			$condition[] = array('Orderdetail.company_id'=>$company_id);
			
		else:
			$condition[] = array('Orderdetail.company_id'=>$company_id);
			$condition[] = array('Orderdetail.branch_id'=>$branch_id);
			
		endif;
		$this->paginate = array(
			'fields' => 'Orderdetail.*',
			'conditions'=> $condition ,
			'limit' => 20
		);
		$orderdetail = $this->paginate('Orderdetail');
		$this->set('orderdetail',$orderdetail);	
	}
	public function manufacturedmanage(){
		$this->layout='default';
		$this->set('title_for_layout', __('Order').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];

		$condition = array();		
		if($group_id==1):
		elseif($group_id==2):
			$condition[] = array('Orderdetail.company_id'=>$company_id);
		else:
			$condition[] = array('Orderdetail.company_id'=>$company_id);
			$condition[] = array('Orderdetail.branch_id'=>$branch_id);
		endif;
		$condition[] = array('Orderdetail.transactiontype_id'=>$_SESSION["ST_MANURECEIVE"]);
		$this->paginate = array(
			'fields' => 'Orderdetail.*',
			'conditions'=> $condition ,
			'order' => "Orderdetail.id ASC",
			'limit' => 20
		);
		$orderdetail = $this->paginate('Orderdetail');
		$this->set('orderdetail',$orderdetail);	
	}

	public function manufactureddetailsbyid($id=null,$uuid=null){
		$this->layout='ajax';
		$manufacturedproduct=$this->Orderdetail->find('first',array("fields" => "Orderdetail.*,Product.*","conditions"=>array('Orderdetail.id'=>$id,'Orderdetail.orderdetailuuid'=>$uuid)));
		$this->set('manufacturedproduct',$manufacturedproduct);
		$purchaseproduct=$this->Orderdetail->find('first',array("fields" => "Orderdetail.*,Product.*","conditions"=>array('Orderdetail.transaction_id'=>$id,'Orderdetail.transactiontype_id'=>$_SESSION["ST_MANURECEIVE"])));
		$this->set('purchaseproduct',$purchaseproduct);
	}

	public function productstatus(){
		$this->layout='default';
		$this->set('title_for_layout', __('Product Status').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];
		$branchid = array();		
		$condition = array();		
		$groupcondition = "";		
		if($group_id==1):
			$groupcondition .= 'Orderdetail.company_id,';
			$groupcondition .= 'Orderdetail.branch_id,';			
		elseif($group_id==2):
			$branchid[] = array('Company.id'=>$company_id);
			$branchid[] = array('Branch.branchisactive IN (0,1)');
			$condition[] = array('Orderdetail.company_id'=>$company_id);			
			$groupcondition .= 'Orderdetail.company_id,';			
		else:
			$branchid[] = array('Company.id'=>$company_id);
			$branchid[] = array('Branch.id'=>$branch_id);
			$branchid[] = array('Branch.branchisactive IN (1)');
			$condition[] = array('Orderdetail.company_id'=>$company_id);
			$condition[] = array('Orderdetail.branch_id'=>$branch_id);			
			$groupcondition .= 'Orderdetail.company_id,';
			$groupcondition .= 'Orderdetail.branch_id,';			
		endif;
		$groupcondition .= 'Orderdetail.product_id';			

		$branch=$this->Branch->find(
			'list',
			array(
				"fields" => array("Branch.id","branch_name"),
				"recursive" =>1,
				"conditions" => $branchid
			)
		);
		$this->set('branch',$branch);

		$this->paginate = array(
			'fields' =>array('Orderdetail.*', 'CONCAT(SUM(Orderdetail.quantity),"&nbsp;&nbsp; ",Unit.unitname) AS TotalQuantity', 'SUM(Orderdetail.transactionamount) AS TotalAmount'),
			'conditions'=> $condition ,
			'group' => array($groupcondition)
		);
		$orderdetail = $this->paginate('Orderdetail');
		$this->set('orderdetail',$orderdetail);	
	}

	public function productstatussearchbytext(){
		$this->layout='ajax';
		$this->set('title_for_layout', __('Product Status').' | '.__(Configure::read('site_name')));

		$company_id=$_SESSION["User"]["company_id"];
		$branch_id=$_SESSION["User"]["branch_id"];
		$group_id=$_SESSION["User"]["group_id"];
		$user_id=$_SESSION["User"]["id"];
		$Searchtext = @$this->request->data['Searchtext'];
		$OrderdetailBranchId = @$this->request->data['OrderdetailBranchId'];

		$branchid = array();		
		$condition = array();		
		$groupcondition = "";		
		if($group_id==1):
			$groupcondition .= 'Orderdetail.company_id,';
			$groupcondition .= 'Orderdetail.branch_id,';			
		elseif($group_id==2):
			$branchid[] = array('Company.id'=>$company_id);
			$branchid[] = array('Branch.branchisactive IN (0,1)');
			$condition[] = array('Orderdetail.company_id'=>$company_id);			
			$groupcondition .= 'Orderdetail.company_id,';			
		else:
			$branchid[] = array('Company.id'=>$company_id);
			$branchid[] = array('Branch.id'=>$branch_id);
			$branchid[] = array('Branch.branchisactive IN (1)');
			$condition[] = array('Orderdetail.company_id'=>$company_id);
			$condition[] = array('Orderdetail.branch_id'=>$branch_id);			
			$groupcondition .= 'Orderdetail.company_id,';
			$groupcondition .= 'Orderdetail.branch_id,';			
		endif;
		$groupcondition .= 'Orderdetail.product_id';

		if(!empty($OrderdetailBranchId) && $OrderdetailBranchId!=''):
			$condition[] = array('Orderdetail.branch_id'=>$OrderdetailBranchId);
		endif;
		if(!empty($Searchtext) && $Searchtext!=''):
			$condition[] = array(
				'OR' => array(
					'Product.productname LIKE ' => '%'.$Searchtext.'%',
					'Product.productnamebn LIKE ' => '%'.$Searchtext.'%',
					'Productcategory.productcategoryname LIKE ' => '%'.$Searchtext.'%',
					'Productcategory.productcategorynamebn LIKE ' => '%'.$Searchtext.'%',
					'Producttype.producttypename LIKE ' => '%'.$Searchtext.'%',
					'Producttype.producttypenamebn LIKE ' => '%'.$Searchtext.'%',
					'Unit.unitname LIKE ' => '%'.$Searchtext.'%',
					'Unit.unitnamebn LIKE ' => '%'.$Searchtext.'%',
					'Product.productcode LIKE ' => '%'.$Searchtext.'%'
				)
			);
		endif;

		$branch=$this->Branch->find(
			'list',
			array(
				"fields" => array("Branch.id","branch_name"),
				"recursive" =>1,
				"conditions" => $branchid
			)
		);
		$this->set('branch',$branch);

		$this->paginate = array(
			'fields' =>array('Orderdetail.*', 'CONCAT(SUM(Orderdetail.quantity),"&nbsp;&nbsp; ",Unit.unitname) AS TotalQuantity', 'SUM(Orderdetail.transactionamount) AS TotalAmount'),
			'conditions'=> $condition ,
			'group' => array($groupcondition)
		);
		$orderdetail = $this->paginate('Orderdetail');
		$this->set('orderdetail',$orderdetail);	
	}

	function ajaxOrder()
	{
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
	}

	/*function getClientName ($clientId)
	{
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;



		$customerlist_id = ['Usergroup.user_id'=> $clientId];

		$customerNameLlist = $this->Usergroup->find(
			'first',
			array(
				"fields" => array("Usergroup.user_fullname"),
				"conditions" =>$customerlist_id


			)
		);


		echo json_encode($customerNameLlist) ;
	}*/

	function getProduct ($branchId=null)
	{
		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;

		if(!is_null($branchId)){


			$productNameList=$this->Productbranch->find(
				'all',
				array(


					"conditions" => ['Productbranch.branch_id'=>$branchId]
				)
			);




		}




		return json_encode($productNameList);


	}
	/*public function orderView($orderId = null)
	{

		$orderId = $this->Order->getLastInsertId();
		$this->redirect("/orders/orderView/$orderId");
		$orderLIst = $this->Order->find(
			'all',
			array(

				"conditions" => ['Order.id'=>$orderId]
			)
		);
		$orderDetailLIst = $this->Orderdetail->find(
			'all',
			array(

				"conditions" => ['Order.id'=>$orderId]
			)
		);

		echo json_encode($orderLIst) ;die();

	}*/

	public function getStock($branchId,$productId)
	{

		$this->layout = 'ajax';
		$this->beforeRender();
		$this->autoRender = false;
		$soldItemQuantity = $this->Orderdetail->find(
			'all',
			[

				'fields'=>[

							"SUM(Orderdetail.quantity) As soldItemQuantity"],
							'conditions' => [

												'Orderdetail.transactiontype_id'=>7,
												'Orderdetail.branch_id'=>$branchId,
												'Orderdetail.product_id'=>$productId
						 					 ]
			]
		);

		$puchasedItemQuantity = $this->Orderdetail->find(
			'all',
			[

				'fields'=>[

							"SUM(Orderdetail.quantity) As purchaseItemQuantity"],
							'conditions' => [
												'Orderdetail.transactiontype_id'=>8,
												'Orderdetail.branch_id'=>$branchId,
												'Orderdetail.product_id'=>$productId
											]
						  ]
		);

		$salesReturn = $this->Orderdetail->find(
		'all',
		[

			'fields'=>[

							"SUM(Orderdetail.quantity) As salesReturn"],
							'conditions' => [

												'Orderdetail.transactiontype_id'=>12,
												'Orderdetail.branch_id'=>$branchId,
												'Orderdetail.product_id'=>$productId

											]
					   ]
	);
		$purchaseReturn = $this->Orderdetail->find(
			'all',
			[

				'fields'=>[

							"SUM(Orderdetail.quantity) As purchaseReturn"],
							'conditions' => [

												'Orderdetail.transactiontype_id'=>13,
												'Orderdetail.branch_id'=>$branchId,
												'Orderdetail.product_id'=>$productId
											]
						  ]
		);


		$stockSales = $soldItemQuantity[0][0]['soldItemQuantity'];
		$stockPurchase = $puchasedItemQuantity[0][0]['purchaseItemQuantity'];
		$stockSalesReturn = $salesReturn[0][0]['salesReturn'];
		$stockPurchaseReturn = $purchaseReturn[0][0]['purchaseReturn'];

		$stock = ($stockPurchase - $stockPurchaseReturn) - ($stockSales + $stockSalesReturn) ;
		return json_encode($stock);
	}

	public function salesreport()
	{


	}

	public function searchsalesreportbydate($from = null, $to = null)
	{
		$this->layout = 'ajax';

//		$this->autoRender = false;

		if (!is_null($from)) {

//			echo $from;die();

			$orders = $this->Order->find(
				'all',
				array(

					"conditions" => [
						'Date(Order.orderdate)'=>$from

					]
				)
			);

			/*$this->paginate = array(
				'fields' => 'Order.*',
				'order'  =>'Order.orderdate DESC',
				"conditions"=> ['Date(Order.orderdate)'=>$from],
				'limit' => 2
			);
			$orders = $this->paginate('Order');*/
			$this->set('orders',$orders);

		}

		if (!is_null($from) && !is_null($to)) {

			$orders = $this->Order->find(
				'all',
				array(

					"conditions" => [

						'Date(Order.orderdate) >='=>$from,
						'Date(Order.orderdate) <='=> $to

					]
				)
			);
			/*$this->paginate = array(
				'fields' => 'Order.*',
				'order'  =>'Order.orderdate DESC',
				"conditions" => [
					'Date(Order.orderdate) >='=>$from,
					'Date(Order.orderdate) <='=> $to
				],
				'limit' => 2
			);
			$orders = $this->paginate('Order');*/
			$this->set('orders',$orders);
		}

		/*echo "<pre>";

			print_r($salesInfo);

		echo "<pre>";*/

		/*'Date(Assignorder.assign_date)  >=' => $accountstartdate, 'Date(Assignorder.assign_date) <=' => $accountenddate*/




	}


}
?>