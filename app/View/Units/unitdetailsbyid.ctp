<?php
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"2\">
				<h2>".__('Unit')."</h2>
			</td>
		</tr>
		<tr>
			<th>
				".__('Unit Name')."
			</th>
			<td>
				".$units[0]["Unit"]["unit_name"]."
			</td>
		</tr>
		<tr>
			<th>
				".__('Unit Type')."
			</th>
			<td>
				".$units[0]["Unittype"]["unittypename"]."/".$units[0]["Unittype"]["unittypenamebn"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Unit Symbol")."
			</th>
			<td>
				".$units[0]["Unit"]["unit_symbol"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Mon")."
			</th>
			<td>
				".$units[0]["Unit"]["unitmon"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Kilogram")."
			</th>
			<td>
				".$units[0]["Unit"]["unitkilogram"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Gram")."
			</th>
			<td>
				".$units[0]["Unit"]["unitgram"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Active")."
			</th>
			<td>
				".$units[0]["Unit"]["isActive"]."
			</td>
		</tr>
	";
	echo $Utilitys->tableend();
?>