<?php
App::import('Controller', 'Logins');
App::uses('AppController', 'Controller');
App::uses('UtilitysController', 'Controller');

class CompaniesController extends AppController {

    public $name = 'Companies';
    public $helpers = array('Html', 'Form', 'Paginator', 'Session');
    public $components = array('RequestHandler','Session', 'Image');
    public $uses = array('Company', 'Companyphone', 'Companyphonenaration', 'Companyynaration', 'Companyemail', 'Companyemailnaration','Branch', 'Department', 'Designation', 'Group', 'Workingarea');
    var $Logins;
    // public function index() {
    //     $this->redirect("login");
    // }

  function beforeFilter(){
    $this->Logins =& new LoginsController;
    $this->Logins->constructClasses();
    $this->Logins->__validateLoginStatus();
    //$this->__validateLoginStatus();
    //$this->recordActivity();
    // $this->Auth->allow('register');
    $Utilitys = new UtilitysController;
    $this->set('Utilitys',$Utilitys);
    if(!isset($_SESSION["User"]["id"])){
      $this->redirect("/logins/login");
    }
  }

public function companysearchbytext($Searchtext=null){

    $this->layout = 'ajax';
    //$Searchtext = @$this->request->data['Searchtext'];
    $group_id=$_SESSION["User"]["group_id"];
    $company_id=$_SESSION["User"]["company_id"];
    $CompanyCompanyId = @$this->request->data['CompanyCompanyId'];
    
        //$ProductUnitId = @$this->request->data['ProductUnitId'];
        $company_id = array();
        if($group_id==1):
        $company_id[] = array('');
        elseif($group_id==2):
          $company_id[] = array('Company.id'=>$company_id);
        else:
          $company_id[] = array('Company.id'=>$company_id);
        endif;
        if(!empty($CompanyCompanyId) && $CompanyCompanyId!=''):
            $company_id = array('Company.id'=>$CompanyCompanyId);

        endif;
       
        // if(!empty($Searchtext) && $Searchtext!=''):
        //     $product_id[] = array(
        //         'OR' => array(
        //             'Product.productname LIKE ' => '%'.$Searchtext.'%',
        //             'Product.productnamebn LIKE ' => '%'.$Searchtext.'%',
        //             'Productcategory.productcategoryname LIKE ' => '%'.$Searchtext.'%',
        //             'Productcategory.productcategorynamebn LIKE ' => '%'.$Searchtext.'%',
        //             'Unit.unitname LIKE ' => '%'.$Searchtext.'%',
        //             'Unit.unitnamebn LIKE ' => '%'.$Searchtext.'%'
        //         )
        //     );
        // else:
        //     $product_id[] = array();
        // endif;

        $this->paginate = array(
            'fields' => 'Company.*',
            //'order'  =>'Product.productname ASC',
            "conditions"=>$company_id,
            'limit' => 20
        );
        $company = $this->paginate('Company');
       // print_r($company);exit();
        $this->set('company',$company);
  }

    public function companyinsertupdate($id = null, $uuid = null){
       if(!empty($id) && $id!=0):
            $company=$this->Company->find(
            'all',array("fields" => "Company.*","conditions"=>array('Company.id'=>$id,'Company.companyuuid'=>$uuid)));
            $this->set('company',$company);
          //  print_r($company);
            if($company !=null){
            $companyemail = explode(",", $company[0]['Company']['company_email']);
           // print_r($companyemail);
            $companyphone = explode(",", $company[0]['Company']['company_phone']);

            //$companyemail=$this->Companyemail->find('all',array("fields" => "Companyemail.*","conditions"=>array('Companyemail.companies_id'=>$id)));
            $this->set('companyemail',$companyemail);
          //  $companyphone=$this->Companyphone->find('all',array("fields" => "Companyphone.*","conditions"=>array('Companyphone.companies_id'=>$id)));
            $this->set('companyphone',$companyphone);
          }
        endif;
    }
    public function  companyinsertupdateaction($id=null, $uuid = null){
        $id = @$this->request->data['Companies']['id'];
        $uuid = @$this->request->data['Companies']['companyuuid'];
        $userID = $this->Session->read('User.id');
        $groupID=$_SESSION["User"]["group_id"];
        $company_id=$_SESSION["User"]["company_id"];
        $branch_id=$_SESSION["User"]["branch_id"];
        $company_name = $this->request->data['Company']['companyname'];
        $company_name_bn = $this->request->data['Company']['companynamebn'];
        if(!empty($id) && !empty($uuid)){
          $count_company_name = 0;
        }else{
          $count_company_name=$this->Company->find('all',array("fields" => "Company.*","conditions"=>array('Company.companyname'=>$company_name,'Company.companynamebn'=>$company_name_bn)));
          $count_company_name = count( $count_company_name);
        }
        if($count_company_name>0){
          $this->Session->setFlash(__("This Company Name Already Exists"), 'default', array('class' => 'alert alert-warning'));
          $this->redirect("/companies/companyinsertupdate");  
        }else {
          if ($this->request->is('post')) {
            if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
                $this->request->data['Company']['companyupdateid']=$userID;
                $this->request->data['Company']['companyupdatedate']=date('Y-m-d H:i:s');
                $message = __('Update Successfully.');
                $this->Company->id = $id;
                if($this->Company->save($this->data)){
                    $image = $this->request->data['Company']['photo1']['name'];
                if (!empty($this->request->data['Company']['photo1']['name'])) {
                    $image_path = $this->Image->upload_image_and_thumbnail($image, $this->data['Company']['photo1'], 600, 450, 150, 180, "company");
                     if ($this->data['Company']['photo2'] != '') {
                        unlink(WWW_ROOT . 'img/company/big/' . $this->data['Company']['photo2']);
                        unlink(WWW_ROOT . 'img/company/small/' . $this->data['Company']['photo2']);
                        unlink(WWW_ROOT . 'img/company/home/' . $this->data['Company']['photo2']);
                    }
                     if (isset($image_path)) {
                        $this->Company->saveField('companylogo', $image_path);
                        $uploaded = true;
                    }
                }
                $this->Session->setFlash('Update Successfully.', 'default', array('class' => 'alert alert-success'));
                $this->Companyphone->deleteAll(array('Companyphone.company_id' => $id), false);
                $this->Companyemail->deleteAll(array('Companyemail.company_id' => $id), false);
                if(!empty($this->request->data['mytext'])){
                     $count = count($this->request->data['mytext']);
                       for($i = 0 ;$i<$count; $i++){
                        $this->request->data['Companyemail']['companyemailaddress'] = $this->request->data['mytext'][$i];
                        $this->request->data['Companyemail']['company_id'] = $id;
                        $this->Companyemail->create();
                        $this->Companyemail->save($this->data);
                      }
                }
                if(!empty($this->request->data['mytext1'])){
                     $count1 = count($this->request->data['mytext1']);
                       for($i = 0 ;$i<$count1; $i++){
                        $this->request->data['Companyphone']['companyphoneno'] = $this->request->data['mytext1'][$i];
                        $this->request->data['Companyphone']['company_id'] = $id;
                        $this->Companyphone->create();
                        $this->Companyphone->save($this->data);
                      }
                }
              }
            }
            else{
                $now = date('Y-m-d H:i:s');
                $this->request->data['Company']['companyinsertid']=$userID;
                $this->request->data['Company']['companyinsertdate'] = $now;
                $this->request->data['Company']['companyuuid']=String::uuid();
                $message = __('Save Successfully.');

                if ($this->Company->save($this->data)) {
                    $image = $this->request->data['Company']['photo1']['name'];
                    if (!empty($this->request->data['Company']['photo1']['name'])) {
                        $image_path = $this->Image->upload_image_and_thumbnail($image, $this->data['Company']['photo1'], 600, 450, 150, 180, "company");
                        //  if ($this->data['Taxpayee']['photo2'] != '') {
                        //     unlink(WWW_ROOT . 'img/company/big/' . $this->data['Company']['photo2']);
                        //     unlink(WWW_ROOT . 'img/company/small/' . $this->data['Company']['photo2']);
                        //     unlink(WWW_ROOT . 'img/company/home/' . $this->data['Company']['photo2']);
                        // }
                         if (isset($image_path)) {
                            $this->Company->saveField('companylogo', $image_path);
                            $uploaded = true;
                        }
                    }if(empty($id) && empty($uuid)){
                    $insertedid = $this->Company->getLastInsertId();
                    $this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
                if(!empty($this->request->data['mytext'])){
                 $count = count($this->request->data['mytext']);
                   for($i = 0 ;$i<$count; $i++){
                    $this->request->data['Companyemail']['companyemailaddress'] = $this->request->data['mytext'][$i];
                    $this->request->data['Companyemail']['company_id'] = $insertedid;
                    $this->Companyemail->create();
                    $this->Companyemail->save($this->data);
                  }
                }
                 if(!empty($this->request->data['mytext1'])){
                 $count1 = count($this->request->data['mytext1']);
                   for($i = 0 ;$i<$count1; $i++){
                    $this->request->data['Companyphone']['companyphoneno'] = $this->request->data['mytext1'][$i];
                    $this->request->data['Companyphone']['company_id'] = $insertedid;
                    $this->Companyphone->create();
                    $this->Companyphone->save($this->data);
                  }
                }
            }
            }
          }
      }
         $this->redirect("companymanage");
        }
        

    }
    public function companymanage(){
      $company_id=$_SESSION["User"]["company_id"];
      $group_id=$_SESSION["User"]["group_id"];
      $user_id=$_SESSION["User"]["id"];
     // print_r($this->request);
        App::import('Controller', 'Utilitys');

        $Utilitys = new UtilitysController;
        $this->set('Utilitys',$Utilitys);
     
      $condition = array();
     
      if($group_id==1):
        elseif($group_id==2):
        $condition[] = array('Company.id'=>$company_id);
        else:
        $condition[] = array('Company.id'=>$company_id);
      endif;
        

      $company=$this->Company->find('list',
        array("fields" => array("Company.id","company_name"),
              "conditions" => $condition));
      $this->set('company',$company);
        $this->paginate = array(
            'fields' => 'Company.*',
            'conditions' => $condition,
            'order'  =>'Company.companyname ASC',
            'limit' => 20
        );
        $companies = $this->paginate('Company');
        $this->set('companies',$companies);

    }
    // public function companydelete($id=null){
    //     $userID = 1;
    //     $this->request->data['Company']['companydeleteid']=$userID;
    //     $this->request->data['Company']['companydeletedate']=date('Y-m-d H:i:s');
    //     $this->request->data['Company']['companyisactive']=2;
    //     $this->Company->id = $id;
    //     if ($this->Company->save($this->data)) {
    //         $this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'success'));
    //     }
    //     $this->redirect("companyview");
    // }
    public function companydelete($id=null){
      $this->set('id',$id);
    }
    public function addnaration($id=null){
     // $this->layout = 'default';
      // $userID = 1;
      // print_r($this->data);exit();
      //   $this->request->data['Company']['companydeleteid']=$userID;
      //   $this->request->data['Company']['companydeletedate']=date('Y-m-d H:i:s');
      //   $this->request->data['Company']['companyisactive']=2;
      //   $this->Company->id = $id;
      //   if ($this->Company->save($this->data)) {
      //       //$this->Companynaration->save($this->data);
      //       $this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'success'));
      //   }
      //   $this->redirect("companyview");
      $this->set('id',$id);
    }
    public function companyupdatestatus($id=null){
        $userID = 1;
        $this->request->data['Company']['companyupdateid'] = $userID;
        $this->request->data['Company']['companyupdatedate'] = date('Y-m-d H:i:s');
       print_r($this->data);
        $this->Company->id = $id;
        if ($this->Company->save($this->data)) {
            $this->Session->setFlash('Status change Successfully.', 'default', array('class' => 'success'));
        }
        $this->redirect("companymanage");
    }

  public function companydetailsbyid($id=null, $uuid = null){
     $company=$this->Company->find(
            'all',array("fields" => "Company.*","conditions"=>array('Company.id'=>$id,'Company.companyuuid'=>$uuid)));
            $this->set('company',$company);
  }

  public function listbranchbycompany($id=null){
    $this->layout = 'ajax';
    $this->beforeRender();
    $this->autoRender = false;
    $company_id = array();
    if(!empty($id) && $id!=0):
      $company_id[] = array('Branch.company_id' => $id);
    else:
      $company_id[] = array();
    endif;
    $branch=$this->Branch->find(
      'list',array(
        "fields" => array("Branch.id","branch_name"),
        "conditions" => $company_id
      )
    );
      echo "<option value=\"0\">Select Branch</option>";
    foreach($branch as $key => $val) {
      echo "<option value=\"".$key."\">".$val."</option>";
    }
  }
  //----------------------------Start Department----------------//
  public function departmentmanage(){
    $this->layout='default';
    $this->set('title_for_layout', __('Department').' | '.__(Configure::read('site_name')));

    $company_id=$_SESSION["User"]["company_id"];
    $branch_id=$_SESSION["User"]["branch_id"];
    $group_id=$_SESSION["User"]["group_id"];

    $condition_group = array();
    if($group_id==1):
      $condition_group[] = array('');
    elseif($group_id==2):
      $condition_group[] = array('NOT'=>array('Group.id'=>array(1, 2, 3, 4, 5, 6, 7)));
    else:
      $condition_group[] = array('NOT'=>array('Group.id'=>array(1, 2, 3, 4, 5, 6, 7)));
    endif;
     $group=$this->Group->find(
          'list',
          array(
            "fields" => array("id","groupname"),
            "conditions" => $condition_group
            )
        );
        $this->set('group',$group);

    $condition_company = array();
    if($group_id==1):
      $condition_company[] = array('');
    elseif($group_id==2):
      $condition_company[] = array('Company.id'=>$company_id);
    else:
      $condition_company[] = array('Company.id'=>$company_id);
    endif;
    $company=$this->Company->find(
          'list',
          array(
            "fields" => array("id","company_name"),
            "conditions" => $condition_company
            )
    );
    $this->set('company',$company);

    $condition_department = array();
    if($group_id==1):
      $condition_department[] = array('');
    elseif($group_id==2):
      $condition_department[] = array('Department.company_id'=>$company_id);
    else:
      $condition_department[] = array('Department.company_id'=>$company_id);
    endif;

    $this->paginate = array(
      'fields' => 'Department.*, Company.*, Group.*',
      'order'  =>'Department.departmentname ASC',
      "conditions" => $condition_department,
      'limit' => 20
    );
    $departments = $this->paginate('Department');
    $this->set('departments',$departments);

  }
  public function departmentinsertupdate($id = null, $uuid=null) {
    $this->layout='default';
    $userID = $this->Session->read('User.id');
    $company_id=$_SESSION["User"]["company_id"];
    $branch_id=$_SESSION["User"]["branch_id"];
    $group_id=$_SESSION["User"]["group_id"];
    $this->set('title_for_layout', __('Insert/Edit Department').' | '.__(Configure::read('site_name')));

   $condition_company = array();
    if($group_id==1):
      $condition_company[] = array('');
    elseif($group_id==2):
      $condition_company[] = array('Company.id'=>$company_id);
    else:
      $condition_company[] = array('Company.id'=>$company_id);
    endif;
    $company =$this->Company->find('list',array(
      "fields" => array("Company.id","company_name"),
      'conditions' => array('Company.companyisactive' => 1, $condition_company)
      ));
    $this->set('company', $company);

   $condition_group = array();
    if($group_id==1):
      $condition_group[] = array('');
    elseif($group_id==2):
      $condition_group[] = array('NOT'=>array('Group.id'=>array(1, 2, 3, 4, 5, 6, 7)));
    else:
      $condition_group[] = array('NOT'=>array('Group.id'=>array(1, 2, 3, 4, 5, 6, 7)));
    endif;
    $group =$this->Group->find('list',array(
      "fields" => array("Group.id","Group.groupname"),
      'conditions' => array('Group.groupisactive' => 1, $condition_group)
      ));
    $this->set('group', $group);

    if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
      $departments = $this->Department->find('all', array("fields" => "Department.*", "conditions" => array('Department.id' => $id, 'Department.departmentuuid'=>$uuid)));
      $this->set('departments', $departments);
    }
  }
  public function departmentinsertupdateaction(){
    $userID = $this->Session->read('User.id');
    $company_id = $_SESSION["User"]["company_id"];
    $branch_id = $_SESSION["User"]["branch_id"];
    $id = @$this->request->data['Companies']['id'];
    $uuid = @$this->request->data['Companies']['departmentuuid'];
    $department_name = @$this->request->data['Department']['departmentname'];
    $department_namebn = @$this->request->data['Department']['departmentnamebn'];
    $department_group = @$this->request->data['Department']['group_id'];
    $department_company = @$this->request->data['Department']['company_id'];
    if(!empty($id) && !empty($uuid)){
          $count_department_name = 0;
    }else{
          $count_department_name=$this->Department->find('all',array("fields" => "Department.*","conditions"=>array('Department.departmentname'=>$department_name,'Department.departmentnamebn'=>$department_namebn,'Department.group_id'=>$department_group,'Department.company_id'=>$department_company)));
          $count_department_name = count( $count_department_name );
    }
    if($count_department_name > 0){
          $this->Session->setFlash(__("This Department Name Already Exists"), 'default', array('class' => 'alert alert-warning'));
          $this->redirect("/companies/departmentinsertupdate");  
    } else{
          if ($this->request->is('post')) {
            if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
              $this->request->data['Department']['departmentupdateid']=$userID;
              $this->request->data['Department']['departmentupdatedate']=date('Y-m-d H:i:s');
              $message = __('Update Successfully.');
              $this->Department->id = $id;
            else:
              $now = date('Y-m-d H:i:s');
              $this->request->data['Department']['departmentinsertid']=$userID;
              $this->request->data['Department']['departmentinsertdate'] = $now;
              $this->request->data['Department']['departmentuuid']=String::uuid();
              $message = __('Save Successfully.');
            endif;
            if ($this->Department->save($this->data)) {
              $this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
            }
          }
        $this->redirect("/companies/departmentmanage"); 
    }
  }
  public function departmentdetailsbyid($id=null,$uuid=null){
    $this->layout='ajax';
    $department=$this->Department->find('all',array("fields" => "Department.*, Company.*, Group.*","conditions"=>array('Department.id'=>$id,'Department.departmentuuid'=>$uuid)));
    $this->set('department',$department);
  }
  public function departmentdelete($id=null,$uuid=null){
    $userID = $this->Session->read('User.id');
    if ($this->Department->updateAll(array('departmentdeleteid'=>$userID, 'departmentupdatedate'=>"'".date('Y-m-d H:i:s')."'", 'departmentisactive'=>2), array('AND' => array('Department.id'=>$id,'Department.departmentuuid' => $uuid)))) {
      $this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
    }
    $this->redirect("/companies/departmentmanage");
  }
  public function departmentsearchbytext($Searchtext=null){
    $this->layout='ajax';
    $Searchtext = @$this->request->data['Searchtext']; 
    $DepartmentGroupId = @$this->request->data['DepartmentGroupId'];
    $DepartmentCompanyId = @$this->request->data['DepartmentCompanyId'];

    $company_id=$_SESSION["User"]["company_id"];
    $branch_id=$_SESSION["User"]["branch_id"];
    $group_id=$_SESSION["User"]["group_id"];
    $userId=$_SESSION["User"]["id"];

    $condition = array();
    if($group_id==1):
      $condition[] = array('');
    elseif($group_id==2):
      $condition[] = array('Department.company_id'=>$company_id);
    else:
      $condition[] = array('Department.company_id'=>$company_id);
    endif;
        if(!empty($DepartmentGroupId) && $DepartmentGroupId!=''):
            $condition[] = array('Department.group_id'=>$DepartmentGroupId);
        endif;
        if(!empty($DepartmentCompanyId) && $DepartmentCompanyId!=''):
            $condition[] = array('Department.company_id'=>$DepartmentCompanyId);
        endif;
        if(!empty($Searchtext) && $Searchtext!=''):
            $condition[] = array(
                'OR' => array(
                    'Department.departmentname LIKE ' => '%'.$Searchtext.'%',
                    'Department.departmentnamebn LIKE ' => '%'.$Searchtext.'%'
                )
            );
        else:
            $condition[] = array();
        endif;

        $this->paginate = array(
            'fields' => 'Department.*, Company.*, Group.*',
            'order'  =>'Department.departmentname ASC',
            "conditions"=>$condition,
            'limit' => 20
        );
        $departments = $this->paginate('Department');
        $this->set('departments',$departments); 
  }
  //----------------------------End Department------------------//
  //----------------------------Start Designation----------------//
  public function designationmanage(){
    $this->layout='default';
    $this->set('title_for_layout', __('Designation').' | '.__(Configure::read('site_name')));

    $company_id=$_SESSION["User"]["company_id"];
    $branch_id=$_SESSION["User"]["branch_id"];
    $group_id=$_SESSION["User"]["group_id"];

    $condition_group = array();
    if($group_id==1):
      $condition_group[] = array('');
    elseif($group_id==2):
      $condition_group[] = array('NOT'=>array('Group.id'=>array(1, 2, 3, 4, 5, 6, 7)));
    else:
      $condition_group[] = array('NOT'=>array('Group.id'=>array(1, 2, 3, 4, 5, 6, 7)));
    endif;
     $group=$this->Group->find(
          'list',
          array(
            "fields" => array("id","groupname"),
            "conditions" => $condition_group
            )
        );
        $this->set('group',$group);

    $condition_company = array();
    if($group_id==1):
      $condition_company[] = array('');
    elseif($group_id==2):
      $condition_company[] = array('Company.id'=>$company_id);
    else:
      $condition_company[] = array('Company.id'=>$company_id);
    endif;
    $company=$this->Company->find(
          'list',
          array(
            "fields" => array("id","company_name"),
            "conditions" => $condition_company
            )
    );
    $this->set('company',$company);    

    $condition_designation = array();
    if($group_id==1):
      $condition_designation[] = array('');
    elseif($group_id==2):
      $condition_designation[] = array('Designation.company_id'=>$company_id);
    else:
      $condition_designation[] = array('Designation.company_id'=>$company_id);
    endif;

    $this->paginate = array(
      'fields' => 'Designation.*, Company.*, Group.*',
      'order'  =>'Designation.designationname ASC',
      "conditions" => $condition_designation,
      'limit' => 20
    );
    $designations = $this->paginate('Designation');
    $this->set('designations',$designations);

  }
  public function designationinsertupdate($id = null, $uuid=null) {
    $this->layout='default';
    $userID = $this->Session->read('User.id');
    $company_id=$_SESSION["User"]["company_id"];
    $branch_id=$_SESSION["User"]["branch_id"];
    $group_id=$_SESSION["User"]["group_id"];
    $this->set('title_for_layout', __('Insert/Edit Designation').' | '.__(Configure::read('site_name')));

    $condition_company = array();
    if($group_id==1):
      $condition_company[] = array('');
    elseif($group_id==2):
      $condition_company[] = array('Company.id'=>$company_id);
    else:
      $condition_company[] = array('Company.id'=>$company_id);
    endif;
    $company =$this->Company->find('list',array(
      "fields" => array("Company.id","company_name"),
      'conditions' => array('Company.companyisactive' => 1, $condition_company)
      ));
    $this->set('company', $company);

    $condition_group = array();
    if($group_id==1):
      $condition_group[] = array('');
    elseif($group_id==2):
      $condition_group[] = array('NOT'=>array('Group.id'=>array(1, 2, 3, 4, 5, 6, 7)));
    else:
      $condition_group[] = array('NOT'=>array('Group.id'=>array(1, 2, 3, 4, 5, 6, 7)));
    endif;
    $group =$this->Group->find('list',array(
      "fields" => array("Group.id","Group.groupname"),
      'conditions' => array('Group.groupisactive' => 1, $condition_group)
      ));
    $this->set('group', $group);
	
	$parentDesignation = $this->Designation->find(
      'list', array(
        "fields" => array('id', 'designation_name'),
        "conditions" => array('designationisparent' => 1),
      )
    );
    $this->set('parentDesignation', $parentDesignation);

    if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
      $designations = $this->Designation->find('all', array("fields" => "Designation.*", "conditions" => array('Designation.id' => $id, 'Designation.designationuuid'=>$uuid)));
      $this->set('designations', $designations);
    }
  }
  public function designationinsertupdateaction(){
    $userID = $this->Session->read('User.id');
    $company_id = $_SESSION["User"]["company_id"];
    $branch_id = $_SESSION["User"]["branch_id"];
    $id = @$this->request->data['Companies']['id'];
    $uuid = @$this->request->data['Companies']['designationuuid'];
    $designation_name = $this->request->data['Designation']['designationname'];
    $designation_namebn = $this->request->data['Designation']['designationnamebn'];
    $designation_company = $this->request->data['Designation']['company_id'];
    if(!empty($id) && !empty($uuid)){
          $count_designation_name = 0;
    }else{
          $count_designation_name=$this->Designation->find('all',array("fields" => "Designation.*","conditions"=>array('Designation.designationname'=>$designation_name,'Designation.designationnamebn'=>$designation_namebn,'Designation.company_id'=>$designation_company)));
          $count_designation_name = count( $count_designation_name );
    }
    if($count_designation_name > 0){
          $this->Session->setFlash(__("This Designation Name Already Exists"), 'default', array('class' => 'alert alert-warning'));
          $this->redirect("/companies/designationinsertupdate");  
    } else{
          if ($this->request->is('post')) {
            if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
              $this->request->data['Designation']['designationupdateid']=$userID;
              $this->request->data['Designation']['designationupdatedate']=date('Y-m-d H:i:s');
              $message = __('Update Successfully.');
              $this->Designation->id = $id;
            else:
              $now = date('Y-m-d H:i:s');
              $this->request->data['Designation']['designationinsertid']=$userID;
              $this->request->data['Designation']['designationinsertdate'] = $now;
              $this->request->data['Designation']['designationuuid']=String::uuid();
              $message = __('Save Successfully.');
            endif;
            if ($this->Designation->save($this->data)) {
              $this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
            }
          }
        $this->redirect("/companies/designationmanage"); 
    }
  }
  public function designationdetailsbyid($id=null,$uuid=null){
    $this->layout='ajax';
    $designation=$this->Designation->find('all',array("fields" => "Designation.*, Company.*, Group.*","conditions"=>array('Designation.id'=>$id,'Designation.designationuuid'=>$uuid)));
    $this->set('designation',$designation);
  }
  public function designationdelete($id=null, $uuid=null){
    $userID = $this->Session->read('User.id');
    if ($this->Designation->updateAll(array('designationdeleteid'=>$userID, 'designationdeletedate'=>"'".date('Y-m-d H:i:s')."'", 'designationisactive'=>2), array('AND' => array('Designation.id'=>$id,'Designation.designationuuid' => $uuid)))) {
      $this->Session->setFlash('Delete Successfully.', 'default', array('class' => 'alert alert-success'));
    }
    $this->redirect("/companies/designationmanage");
  }
  public function designationsearchbytext($Searchtext=null){
    $this->layout='ajax';
    $Searchtext = @$this->request->data['Searchtext']; 
    $DesignationGroupId = @$this->request->data['DesignationGroupId'];
    $DesignationCompanyId = @$this->request->data['DesignationCompanyId'];

    $company_id=$_SESSION["User"]["company_id"];
    $branch_id=$_SESSION["User"]["branch_id"];
    $group_id=$_SESSION["User"]["group_id"];
    $userId=$_SESSION["User"]["id"];

    $condition = array();
    if($group_id==1):
      $condition[] = array('');
    elseif($group_id==2):
      $condition[] = array('Designation.company_id'=>$company_id);
    else:
      $condition[] = array('Designation.company_id'=>$company_id);
    endif;
        if(!empty($DesignationGroupId) && $DesignationGroupId!=''):
            $condition[] = array('Designation.group_id'=>$DesignationGroupId);
        endif;
        if(!empty($DesignationCompanyId) && $DesignationCompanyId!=''):
            $condition[] = array('Designation.company_id'=>$DesignationCompanyId);
        endif;
        if(!empty($Searchtext) && $Searchtext!=''):
            $condition[] = array(
                'OR' => array(
                    'Designation.designationname LIKE ' => '%'.$Searchtext.'%',
                    'Designation.designationnamebn LIKE ' => '%'.$Searchtext.'%'
                )
            );
        else:
            $condition[] = array();
        endif;

        $this->paginate = array(
            'fields' => 'Designation.*, Company.*, Group.*',
            'order'  =>'Designation.designationname ASC',
            "conditions"=>$condition,
            'limit' => 20
        );
        $designations = $this->paginate('Designation');
        $this->set('designations',$designations); 
  }
//----------------------------End Designation------------------//

//----------------------------Start Workingarea------------------//
public function workingareamanage(){
    $this->layout='default';
    $this->set('title_for_layout', __('Workingarea').' | '.__(Configure::read('site_name')));

    $company_id=$_SESSION["User"]["company_id"];
    $branch_id=$_SESSION["User"]["branch_id"];
    $group_id=$_SESSION["User"]["group_id"];

    $condition_group = array();
    if($group_id==1):
      $condition_group[] = array('');
    elseif($group_id==2):
      $condition_group[] = array('NOT'=>array('Group.id'=>array(1, 2, 3, 4, 5, 6, 7)));
    else:
      $condition_group[] = array('NOT'=>array('Group.id'=>array(1, 2, 3, 4, 5, 6, 7)));
    endif;
     $group=$this->Group->find(
          'list',
          array(
            "fields" => array("id","groupname"),
            "conditions" => $condition_group
            )
        );
        $this->set('group',$group);

    $condition_company = array();
    if($group_id==1):
      $condition_company[] = array('');
    elseif($group_id==2):
      $condition_company[] = array('Company.id'=>$company_id);
    else:
      $condition_company[] = array('Company.id'=>$company_id);
    endif;
    $company=$this->Company->find(
          'list',
          array(
            "fields" => array("id","company_name"),
            "conditions" => $condition_company
            )
    );
    $this->set('company',$company);

    $condition_department = array();
    if($group_id==1):
      $condition_department[] = array('');
    elseif($group_id==2):
      $condition_department[] = array('Department.company_id'=>$company_id);
    else:
      $condition_department[] = array('Department.company_id'=>$company_id);
    endif;

    $this->paginate = array(
      'fields' => 'Workingarea.*',
      'order'  =>'Workingarea.workingareaname ASC',
      //"conditions" => $condition_department,
      'limit' => 20
    );
    $workingareas = $this->paginate('Workingarea');
    $this->set('workingareas',$workingareas);

  }
  public function workingareainsertupdate($id = null, $uuid=null) {
    $this->layout='default';
    $userID = $this->Session->read('User.id');
    $company_id=$_SESSION["User"]["company_id"];
    $branch_id=$_SESSION["User"]["branch_id"];
    $group_id=$_SESSION["User"]["group_id"];
    $this->set('title_for_layout', __('Insert/Edit Workingarea').' | '.__(Configure::read('site_name')));
    if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0){
      $workingareas = $this->Workingarea->find('all', array("fields" => "Workingarea.*", "conditions" => array('Workingarea.id' => $id, 'Workingarea.workingareauuid'=>$uuid)));
      $this->set('workingareas', $workingareas);
    }
  }
  public function workingareainsertupdateaction(){
    $userID = $this->Session->read('User.id');
    $company_id = $_SESSION["User"]["company_id"];
    $branch_id = $_SESSION["User"]["branch_id"];
    $id = @$this->request->data['Workingareas']['id'];
    $uuid = @$this->request->data['Workingareas']['workingareauuid'];
    $department_name = $this->request->data['Department']['departmentname'];
    $department_namebn = $this->request->data['Department']['departmentnamebn'];
    $department_company = $this->request->data['Department']['company_id'];
    if(!empty($id) && !empty($uuid)){
          $count_department_name = 0;
    }else{
          $count_department_name=$this->Department->find('all',array("fields" => "Department.*","conditions"=>array('Department.departmentname'=>$department_name,'Department.departmentnamebn'=>$department_namebn,'Department.company_id'=>$department_company)));
          $count_department_name = count( $count_department_name );
    }
    if($count_department_name > 0){
          $this->Session->setFlash(__("This Department Name Already Exists"), 'default', array('class' => 'alert alert-warning'));
          $this->redirect("/companies/departmentinsertupdate");  
    } else{
          if ($this->request->is('post')) {
            if(!empty($id) && $id!=0 && !empty($uuid) && $uuid!=0):
              $this->request->data['Department']['departmentupdateid']=$userID;
              $this->request->data['Department']['departmentupdatedate']=date('Y-m-d H:i:s');
              $message = __('Update Successfully.');
              $this->Department->id = $id;
            else:
              $now = date('Y-m-d H:i:s');
              $this->request->data['Department']['departmentinsertid']=$userID;
              $this->request->data['Department']['departmentinsertdate'] = $now;
              $this->request->data['Department']['departmentuuid']=String::uuid();
              $message = __('Save Successfully.');
            endif;
            if ($this->Department->save($this->data)) {
              $this->Session->setFlash($message, 'default', array('class' => 'alert alert-success'));
            }
          }
        $this->redirect("/companies/departmentmanage"); 
    }
  }
//----------------------------End Workingarea------------------//



}

?>
