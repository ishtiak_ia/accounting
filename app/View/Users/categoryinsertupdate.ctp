<?php
echo $this->Form->create('Users', array('action' => 'categoryinsertupdateaction', 'type' => 'file'));
	echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$category[0]["Category"]['id']));
	echo $this->Form->input('categoryuuid', array('type'=>'hidden', 'value'=>@$category[0]["Category"]['categoryuuid']?$category[0]["Category"]['categoryuuid']:0, 'class'=>''));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$category[0]['Category']['categoryisactive'] ? $category[0]['Category']['categoryisactive'] : 0),
			'class' => 'form-inline'
		);
	echo"<h3>".__("Add New Category")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-4\">";
			echo $this->Form->input(
				"Category.categoryname",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Category Name'),
					'tabindex'=>1,
					'placeholder' => __("Category Name"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style' => '',
					'value' => @$category[0]['Category']['categoryname']
				)
			);
			echo"<div class=\"form-group\">";
				echo"<label>isActive?</label><br>";
				echo $this->Form->radio(
					'Category.categoryisactive', 
					$options,
					$attributes
				);
			echo"</div>";
			echo $Utilitys->allformbutton('',$this->request["controller"],$page='category');
		echo"</div>";
	echo"</div>";
echo $this->Form->end();
?>
<script type="text/javascript">
 jQuery(document).ready(function(){
    jQuery("#UsersCategoryinsertupdateactionForm").validationEngine();
 });
</script>