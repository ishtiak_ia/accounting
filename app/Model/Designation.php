<?php 
App::uses('AppModel', 'Model');
class Designation extends AppModel {
	public $name = 'Designation';
	public $usetables = 'designations';

	var $belongsTo = array(
		'Group' => array(
			'fields' =>array('groupname'),
			'className'    => 'Group',
			'foreignKey'    => 'group_id'
		), 
		'Company' => array(
			'fields' =>array('Company.*'),
			'className'    => 'Company',
			'foreignKey'    => 'company_id'
		),
		'DesignationParent' => array(
			'fields' =>array('DesignationParent.Designationname AS Parentdesignationname'),
			'className'    => 'Designation',
			'foreignKey'    => false,
			'conditions'	=> array(
				'DesignationParent.id=Designation.designationparentid'
			)
		)
	);
	var $virtualFields = array(
		'designationparent_name' => 'CONCAT(DesignationParent.designationname, " / ", DesignationParent.designationname)',
		'designation_name' => 'CONCAT(Designation.designationname, " / ", Designation.designationnamebn)',
		'company_fullname' => 'CONCAT(Company.companyname, " / ", Company.companynamebn)',
		'group_name' => 'CONCAT(Group.groupname)',
		'isParent' => 'IF(Designation.designationisparent = 1, "<span class=\"green\">Yes</span>", "<span class=\"red\">No</span>")',
		'isActive' => 'IF(Designation.designationisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Designation.designationisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
}