$(function() {
    $("#OrderdetailQuantity").keyup(function(){
        var quantity = this.value; 
        if(quantity ==''){
            quantity = 0;
        }else{
            quantity = parseFloat(quantity);
        }
        var unit_price = $("#OrderdetailUnitprice").val();
        if(unit_price ==''){
            unit_price = 0;
        }else{
            unit_price = parseFloat(unit_price);
        }
        $("#OrderdetailPrice").val(quantity * unit_price);

        var discount_rate = $("#OrderdetailDiscount").val();
        if(discount_rate =='' || discount_rate == 'NaN'){
            discount_rate = 0;
        }else{
            discount_rate = parseFloat(discount_rate);
        }
        var price = $("#OrderdetailPrice").val();
        var discount_amount = ((price*discount_rate)/100);
        var amount = price - discount_amount;
        $("#OrderdetailTransactionamount").val(amount);
    })
    $("#OrderdetailUnitprice").keyup(function(){
        var unit_price = this.value; 
        if(unit_price ==''){
            unit_price = 0;
        }else{
            unit_price = parseFloat(unit_price);
        }
        var quantity = $("#OrderdetailQuantity").val();
        if(quantity ==''){
            quantity = 0;
        }else{
            quantity = parseFloat(quantity);
        }
        $("#OrderdetailPrice").val(quantity * unit_price);
        var discount_rate = $("#OrderdetailDiscount").val();
        if(discount_rate =='' || discount_rate == 'NaN'){
            discount_rate = 0;
        }else{
            discount_rate = parseFloat(discount_rate);
        }
        var price = $("#OrderdetailPrice").val();
        var discount_amount = ((price*discount_rate)/100);
        var amount = price - discount_amount;
        $("#OrderdetailTransactionamount").val(amount);
    })
    $("#OrderdetailDiscount").keyup(function(){
        var discount_rate = this.value;
        if(discount_rate =='' || discount_rate == 'NaN'){
            discount_rate = 0;
        }else{
            discount_rate = parseFloat(discount_rate);
        }
        var price = $("#OrderdetailPrice").val();
        var discount_amount = ((price*discount_rate)/100);
        var amount = price - discount_amount;
        $("#OrderdetailTransactionamount").val(amount);
    })
});    