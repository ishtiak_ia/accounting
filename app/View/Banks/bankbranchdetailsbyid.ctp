<?php
	echo $Utilitys->tablestart($class=null, $id=null);
	echo"
		<tr>
			<td colspan=\"2\">
				<h2>".__('Bank Branch Information')."</h2>
			</td>
		</tr>
		<tr>
			<th>
				".__('Bank')."
			</th>
			<td>
				".$bankbranch[0]["Bankbranch"]["bankbranchname"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Bank BN")."
			</th>
			<td>
				".$bankbranch[0]["Bankbranch"]["bankbranchnamebn"]."
			</td>
		</tr>
		<tr>
			<th>
				".__("Active")."
			</th>
			<td>
				".$bankbranch[0]["Bankbranch"]["isActive"]."
			</td>
		</tr>
	";
	echo $Utilitys->tableend();
?>