


<?php echo $this->Form->create('journalreports', array('default' => false, 'class'=>' ')); ?>
<fieldset>  
    <legend><h3>Voucher</h3></legend>
    <table  border="0">
        <tr>
            <td>Voucher No.:</td>
            <td>
                <?php
			       echo $this->Form->input(
						"Voucher.voucher_no",
						array(
							'type' => 'select',
							'div' => array('class'=> 'form-group'),
							'empty'=>__('Select Voucher NO.'),
							"options"=>array($voucher_no),
							'label' => false,
							'class'=> '',
							'style' => '',
							'onchange' =>'get_voucher();' 
						)
					);
                ?>
              </td>
         </tr>
    </table>
</fieldset> 
<?php echo $this->form->end(); ?> 
<br><br>
<?php
	echo "<div id='print_page' class='right-aligned-texts'>";
    echo $Utilitys->printbutton ($printableid="loadvoucher", $searchfield=null, $pageformat="c3", $orientation="l", $unit=null, $companyname=$_SESSION["User"]["company_name"], $tabletitle="yes", $tablemonth="yes", $tabledepartment="yes", $printsectoin="printtwotable");
    echo "</div>";

?>
<div id="loadvoucher">
  
</div>
<?php
echo"
		<script type='text/javascript'>
			function get_voucher(){	
					var Searchtext = $(\"#VoucherVoucherNo\").val();
					$(\"#print_page\").hide();
					$(\"#loadvoucher\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."journalreports/vouchergenerate',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#print_page\").show();
							$(\"#loadvoucher\").html(result);
						}
					);
			}	
			
		</script>
	";
	//$(\"#banksarchloading\").html(result);
?>

<script type="text/javascript">
$(document).ready(function() {
   $("#print_page").hide();
   $("#VoucherVoucherNo").select2();
 });
</script>
