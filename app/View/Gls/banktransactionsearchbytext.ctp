<?php
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart($class=null, $id=null);
		echo $this->Html->tableHeaders(
			array(
				__('SL#'), 
				array(
					__('Date')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Particulars')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Chq.No')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Withdraw')  => array('class' => 'highlight sortable')
				), 
				array(
					__('Deposits')  => array('class' => 'highlight sortable')
				), 
				__('Active'), 
				__('Action')
			)
		);
		$bankrows = array();
		$countBanktransaction = 00-1+$this->Paginator->counter('%start%');
		foreach($banktransaction as $Thisbanktransaction):
			$countBanktransaction++;
			$bankrows[]= array($countBanktransaction,$Thisbanktransaction["Banktransaction"]["transactiondate"],$Thisbanktransaction["Banktransaction"]["banktransaction_particulars"],$Thisbanktransaction["Banktransaction"]["banktransaction_chequenumber"],array($Thisbanktransaction["Banktransaction"]["banktransaction_withdraw"],'colspan="1" align="right"'),array($Thisbanktransaction["Banktransaction"]["banktransaction_deposit"],'colspan="1" align="right"'),$Utilitys->statusURL($this->request["controller"], 'banktransaction', $Thisbanktransaction["Banktransaction"]["id"], $Thisbanktransaction["Banktransaction"]["banktransactionuuid"], $Thisbanktransaction["Banktransaction"]["banktransactionisactive"]),$Utilitys->cusurl($this->request["controller"], 'banktransaction', $Thisbanktransaction["Banktransaction"]["id"], $Thisbanktransaction["Banktransaction"]["banktransactionuuid"]));
		endforeach;
		echo $this->Html->tableCells(
			$bankrows
		);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();
	echo"
		<script>
			$(document).ready(function() {
				$(\".paging a\").click(function(){
					var Searchtext = $('#BanktransactionSearchtext').val();
					$.ajax({
						type: \"post\",
						url:this.href,
						data:{Searchtext:Searchtext},
						async: true,
						beforeSend: function(){
							$(\"#banktransactionsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						},
						success: function(data){
							$(\"#banktransactionsarchloading\").html( data );
						}
					});
					return false;
				});
			});
		</script>
	";
?>