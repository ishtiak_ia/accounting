<?php
App::uses('AppController', 'Controller');
class UtilitysController extends AppController {
	public $name = 'Utilitys';
	public $helpers = array('Html', 'Form', 'Paginator', 'Session', 'Form' => array('className' => 'BootstrapForm'));
	public $components = array('RequestHandler','Session', 'Image');
	public $uses = array('Menu', 'Menuprivilege');

	public function addurl($title=null, $controller=null, $page=null){
		$Html = new HtmlHelper(new View(null));
		$AddUrl="";
		if(str_replace($controller.'/', '', @$_SESSION["".strtolower($page)."insertupdate"])==strtolower($page).'insertupdate'):
			$AddUrl.=$Html->link(
				$Html->tag(
						'span', '',
						array('class' => 'glyphicon glyphicon-plus')
					). __(" ".$title),
				array(
						'controller' => $controller,
						'action' => $page.'insertupdate/',
						'full_base' => true
				),
				array(
						'class' => 'button btn btn-primary pull-right',
						'style' => '',
						'escape'=>false,
						'title' => __($title)
				)
			);
		endif;
		//$AddUrl.=$this->printbutton ($printableid='printtable',$filename=$controller);
		return $AddUrl;
	}

	public function cancelbutton($title=null, $controller=null, $page=null){
		$Html = new HtmlHelper(new View(null));
		$Form = new FormHelper(new View(null));
		$CancelButton="";
		$URL=$Html->webroot.$controller."/".$page."manage";
		$CancelButton .=$Form->button(
			__($title),
			array(
				'div' => false,
				'align' => false,
				'formnovalidate' => false,
				'class'=>'btn btn-warning',
				'onClick'=>"cancelForm('".$URL."');",
				'type'=>'button'
			)
		);
		return $CancelButton;
	}

	public function resetbutton($title=null, $controller=null, $page=null){
		$Form = new FormHelper(new View(null));
		$ResetButton="";
		$ResetButton .=$Form->button(
			__($title),
			array(
				'div' => false,
				'align' => false,
				'formnovalidate' => false,
				'class'=>'btn btn-warning',
				'type'=>'reset'
			)
		);
		return $ResetButton;
	}

	public function submitbutton($title=null, $controller=null, $page=null){
		$Form = new FormHelper(new View(null));
		$SubmitButton="";
		$SubmitButton .=$Form->submit(
			__($title),
			array(
				'div' => false,
				'align' => 'right',
				'formnovalidate' => true,
				'class'=>'btn btn-primary'
			)
		)."
		".$Form->input(
			$page.'submit',
			array(
				'type' => 'hidden',
				'value'=>$page.'submit'
			)
		);
		return $SubmitButton;
	}

	//save and print button
	public function save_and_print_button($title=null, $controller=null, $page=null){
		$Form = new FormHelper(new View(null));
		$SubmitButton="";
		$SubmitButton .=$Form->submit(
			__($title),
			array(
				'div' => false,
				'align' => 'right',
				'formnovalidate' => true,
				'class'=>'btn btn-primary',
				'id'=>'journal_save_and_print'
			)
		)."
		".$Form->input(
			$page.'save_and_print',
			array(
				'type' => 'hidden',
				'value'=>''
			)
		);
		return $SubmitButton;
	}


	public function allformbutton($title=null, $controller=null, $page=null, $save_and_print=null){
		$Html = new HtmlHelper(new View(null));

		$allbutton = "";
		$allbutton .= " ";
		$allbutton .= $this->cancelbutton($title="Cancel", $controller, $page);
		$allbutton .= " ";
		$allbutton .= $this->resetbutton($title="Reset", $controller, $page);
		$allbutton .= " ";
		$allbutton .= $this->submitbutton($title="Save", $controller, $page);
		if(!empty($save_and_print)){
			$allbutton .= " ";
			$allbutton .= $this->save_and_print_button($title="Save and Print", $controller, $page);

		}

		return $allbutton;
	}

	public function cusurl($controller, $page, $id, $uuid, $transaction_id=null) {
		//echo "<pre>";print_r($_SESSION);echo "</pre>";
		$Html = new HtmlHelper(new View(null));
		$ActionUrl="";
		if($transaction_id>0):
		else:
		if(str_replace($controller.'/', '', @$_SESSION["".strtolower($page)."insertupdateaction"])==strtolower($page).'insertupdateaction'):
			$ActionUrl.=$Html->link(
				$Html->tag(
						'span', '',
						array('class' => 'glyphicon glyphicon-pencil')
					), 
				array(
						'controller' => $controller,
						'action' => $page.'insertupdate/'.$id.'/'.$uuid,
						'full_base' => true
					),  
				array(
						'class' => 'button btn btn-info btn-sm',
						'style' => '',
						'escape'=>false,
						'title' => __(" EDIT")
					)
			);
			$ActionUrl .= " ";
		endif;
		if(str_replace($controller.'/', '', @$_SESSION["".strtolower($page)."detailsbyid"])==strtolower($page).'detailsbyid'):
			$ActionUrl .= $Html->link(
				$Html->tag(
					'span', 
					'', 
					array(
						'class' => 'glyphicon glyphicon-book'
					)
				),
				array(
					'controller' => $controller,
					'action' => $page.'detailsbyid/'.$id.'/'.$uuid.'/ajax?ajax=true&width='.($_SESSION["Screenwidth"]-200).'&height='.($_SESSION["Screenheight"]+400).'',
					'full_base' => true
				),
				array(
					'class' => 'button btn btn-primary btn-sm',
					'style' => '', 
					'rel'=>"prettyPhoto[ajax]details{$id}",
					'escape'=>false,
					'title' => __(" DETAILS ")
				)
			);
			$ActionUrl .= " ";
		endif;
		if(str_replace($controller.'/', '', @$_SESSION["".strtolower($page)."delete"])==strtolower($page).'delete'):
			$ActionUrl .= $Html->link(
					$Html->tag(
						'span',
						'',
						array(
							'class' => 'glyphicon glyphicon-trash'
						)
					),
					array(
						'controller' => $controller,
						//'action' => $page.'delete/'.$id.'/'.$uuid.'/ajax?ajax=true&width=750&height=500',
						'action' => $page.'delete/'.$id.'/'.$uuid.'',
						'full_base' => true
					),
					array(
						'class' => 'button btn btn-sm btn-danger',
						'style' => '',
						//'rel'=>"prettyPhoto[ajax]delete{$id}",
						'escape'=>false,
						'title' => __(" DELETE ")
					)
				);
			$ActionUrl .= "";
		endif;
		endif;
		return $ActionUrl;
	}

	public function numberLangConverter($lan=null, $replaceNumber){
		$bn_digits = array('০','১','২','৩','৪','৫','৬','৭','৮','৯');
		if($lan=='bn'):
			$changeDigite = str_replace(range(0, 9),$bn_digits, $replaceNumber);
		else:
			$changeDigite = str_replace($bn_digits, range(0, 9),$replaceNumber);
		endif;
		return $changeDigite;
	}

	public function dateLangConverter($lan=null, $replaceNumber){
		$engArray = array(
			1,2,3,4,5,6,7,8,9,0,
			'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December',
			'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
			'am', 'pm'
		);
		$bangArray = array(
			'১','২','৩','৪','৫','৬','৭','৮','৯','০',
			'জানুয়ারি', 'ফেব্রুয়ারি', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'আগস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর',
			'জানু.', 'ফেব্রু.', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'আগস্ট', 'সেপ্টে.', 'অক্টো.', 'নভে.', 'ডিসে.',
			'সকাল', 'দুপুর'
		);
		if($lan=='bn'):
			$changeDigite = str_replace($engArray, $bangArray, $replaceNumber);
		else:
			$changeDigite = str_replace($bangArray, $engArray,$replaceNumber);
		endif;
		return $changeDigite;
	}
	public function tablestart( $class = null , $id = null){
		$table_start = "<div class=\"row\" id=\"printsection\">";
		$table_start .= "<div class=\"col-md-12\">";
		$table_start .= "<div class=\"table-responsive\">";
		$table_start .= "<table class=\"table table-bordered table-hover table-striped ".$class."\" id=\"".$id."\">";
		return $table_start;
	}
	public function tableend( ){
		$table_end =  "</table>";
		$table_end .= "</div> <!-- /.table-responsive -->";
		$table_end .= "</div> <!-- /.col-md-12 -->";
		$table_end .= "</div> <!-- /.row -->";
		return $table_end;
	}
	public function paginationcommon(){
		$Paginator = new PaginatorHelper(new View(null));
		$Form = new FormHelper(new View(null));
		$paginationcommon = "";

		$paginationcommon .= '<div id="pagination-data" class="pagination pull-left">';
			//$paginationcommon .= $Paginator->counter(array('format' => __(' %pages% of %page%')));
			$paginationcommon .= $Paginator->counter(array('format' => __('Total <strong>%end% of %count%</strong> Data')));
		$paginationcommon .= '</div> <!-- /#pagination-data .pagination pull-left -->';

		/*$model = ucfirst(Inflector::singularize($this->params['controller']));
		$hasPages = ($this->params['paging'][$model]['pageCount'] > 1);
		//source: http://www.blog.zahidur.com/how-to-get-model-name-in-cakephp/
		*/
		
		$paginationcommon .= '<div class="pagination-right pull-right">';
			$paginationcommon .= '<ul class="pagination paging">';
				//$paginationcommon .= $this->Paginator->options(array('update' => '#banksarchloading','before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => true)),'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => true))));
				$paginationcommon .= $Paginator->prev( '«', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li' ) );
				$paginationcommon .= $Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) );
				$paginationcommon .= $Paginator->next( '»', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li' ) );
			$paginationcommon .= '</ul>';
			//$paginationcommon .= $Paginator->counter(array('format' => __(' %pages% of %page%')));
			//$paginationcommon .= $this->Js->writeBuffer();
		$paginationcommon .= '</div> <!-- .pagination-right -->';
		//$paginationcommon .= $Form->inpute('totalrowcount', array('type'=>'hidden', 'value'=>@$Paginator->counter(array('format' => __(' %count%')))));

		$paginationcommon .= '<div class="clearfix"></div> <!--after pagination -->';
		return $paginationcommon;
	}

	public function statusURL($controller, $page, $id, $uuid, $activeStatus, $transaction_id=null){
		$Html = new HtmlHelper(new View(null));
		$ActionUrl = "";
		if((int)$activeStatus == 2){
			$title = __('Deleted');
			$tag = $Html->tag(
					'span', '',
					array('class' => 'glyphicon glyphicon-ban-circle')
				). $title;
			$class = "label label-danger";
		}elseif((int)$activeStatus == 1){
			$title = __('Active');
			$tag = $Html->tag(
					'span', '',
					array('class' => 'glyphicon glyphicon-ok')
				). $title;
			$class = "label label-success";
		}else{
			$title = __('Inactive');
			$tag = $Html->tag(
					'span', '',
					array('class' => 'glyphicon glyphicon-remove')
				). $title;
			$class = "label label-warning";
		}
		/*switch ($activeStatus) {
			case 1:
				$title = __('Active');
				$tag = $Html->tag(
						'span', '',
						array('class' => 'glyphicon glyphicon-ok')
					). $title;
				$class = "label label-success";
				break;
			
			case 2:
				$title = __('Deleted');
				$tag = $Html->tag(
						'span', '',
						array('class' => 'glyphicon glyphicon-ban-circle')
					). $title;
				$class = "label label-danger";
				break;
			
			default:
				$title = __('Inactive');
				$tag = $Html->tag(
						'span', '',
						array('class' => 'glyphicon glyphicon-remove')
					). $title;
				$class = "label label-warning";
				break;
		}*/
		if($transaction_id>0):
		else:
		$ActionUrl .= $Html->link(
				$tag,
				array('controller' => 'Customs',
						'action' => 'statuschangnaration/'.$controller.'/'.$page.'/'.$id.'/'.$uuid.'/'.$activeStatus.'/ajax?ajax=true&width=750&height=500',
						'full_base' => true
				),
				array(
					'class' => $class,
					'style' => '',
					'rel'=>"prettyPhoto[ajax]{$id}{$activeStatus}",
					'escape'=>false,
					'title' => $title
				)
			);
		endif;
		$ActionUrl .= "";
		return $ActionUrl;
	}

	public function amountDetailsURL($title, $id, $model, $searchfield){
		$Html = new HtmlHelper(new View(null));
		$ActionUrl = "";
		$title = __($title);
		$tag = $Html->tag(
				'span', '',
				array('class' => 'glyphicon glyphicon-zoom-in')
			). $title;
		$class = "label label-info";
		$ActionUrl .= $Html->link(
				$tag,
				array('controller' => 'Customs',
						'action' => 'amountdetailsbyid/'.$id.'/'.$model.'/'.$searchfield.'?iframe=true&width=100%&height=100%',
						'full_base' => true
				),
				array(
					'class' => $class,
					'style' => '',
					'rel'=>"prettyPhoto[iframe]{$id}amount",
					'escape'=>false
				)
			);
		$ActionUrl .= "";
		return $ActionUrl;
	}

	public function filtertagstart(){
		$filtertagstart = "";
		$filtertagstart .= "
			<div class=\"panel-group\" id=\"accordion\">
				<div class=\"panel panel-default\">
					<div class=\"panel-heading\">
						<h4 class=\"panel-title\">
							<a href=\"#content-1\" class=\"center-block collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\"><span class=\"glyphicon glyphicon-filter\"></span> ".__('Filter')." <span class=\"glyphicon glyphicon-chevron-down pull-right\"></span>
							</a>
						</h4>
					</div>
					<div class=\"panel-collapse out collapse\" id=\"content-1\" aria-expanded=\"false\" style=\"height: 0px;\">
						<div class=\"panel-body form-inline\">
							<!-- contetn placement -->
		";
		return $filtertagstart;
	}
	public function filtertagend(){
		$filtertagend = "";
		$filtertagend .="
							<!-- contetn placement -->
						</div>
					</div>
					<!-- /.panel-collapse -->
				</div>
				<!-- /.panel.panel-default -->
			</div>
			<!-- /.panel-group#accordion -->
		";
		return $filtertagend;
	}

	function printbutton ($printableid='printtable', $searchfield=null, $pageformat=null, $orientation=null, $unit=null, $companyname=null, $tabletitle=null, $tablemonth=null, $tabledepartment=null, $printsectoin=null){
		$Html = new HtmlHelper(new View(null));
		echo"
		<div class=\"btn-group\">
			<button class=\"btn btn-warning btn-sm dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-bars\"></i> Export Table Data</button>
			<ul class=\"dropdown-menu \" role=\"menu\">
				<!--<li><a href=\"#\" onClick =\"$('#".$printableid."').tableExport({type:'csv',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> ".$Html->image("csv.png", array('fullBase' => true, 'width'=>'24px'))." CSV</a></li>
				<li><a href=\"#\" onClick =\"$('#".$printableid."').tableExport({type:'txt',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> ".$Html->image("txt.png", array('fullBase' => true, 'width'=>'24px'))." TXT</a></li>
				<li class=\"divider\"></li>-->
				<li><a href=\"#\" onClick =\"$('#".$printableid."').tableExport({type:'excel',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> ".$Html->image("xls.png", array('fullBase' => true, 'width'=>'24px'))." XLS</a></li>
				<!--<li><a href=\"#\" onClick =\"$('#".$printableid."').tableExport({type:'doc',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> ".$Html->image("word.png", array('fullBase' => true, 'width'=>'24px'))." Word</a></li>
				<li><a href=\"#\" onClick =\"$('#".$printableid."').tableExport({type:'powerpoint',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> ".$Html->image("ppt.png", array('fullBase' => true, 'width'=>'24px'))." PowerPoint</a></li>-->
				<li class=\"divider\"></li>
				<li><a href=\"#\" onClick =\"$('#".$printableid."').tableExport({type:'png',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> ".$Html->image("png.png", array('fullBase' => true, 'width'=>'24px'))." PNG</a></li>
				<li><a href=\"#\" onClick =\"$('#".$printableid."').tableExport({type:'pdf',pdfFontSize:'14',escape:'false', pageformat:'".@$pageformat."', orientation:'".@$orientation."', unit:'".@$unit."', printsectoin:'".@$printsectoin."', companyname:'".@$companyname."', tabletitle:'".@$tabletitle."', tablemonth:'".@$tablemonth."', tabledepartment:'".@$tabledepartment."', tableName:'".str_replace("_id", "", $searchfield)."'});\"> ".$Html->image("pdf.png", array('fullBase' => true, 'width'=>'24px'))." PDF</a></li>
				<li class=\"divider\"></li>
				<li><a href=\"#\" onClick =\"$('#".$printableid."').print({iframe:false,noPrintSelector : '.pagination-right'});\"> ".$Html->image("printer.png", array('fullBase' => true, 'width'=>'24px'))." Print</a></li>
			</ul>
		</div>
		";
	}
	public function userprofileView($title, $id, $uuid, $model){
		$Html = new HtmlHelper(new View(null));
		$ActionUrl = "";
		$title = __($title);
		$tag = $Html->tag(
				'span', '',
				array('class' => 'glyphicon glyphicon-zoom-in')
			). $title;
		$class = "label label-info";
		$ActionUrl .= $Html->link(
				$tag,
				array('controller' => 'Users',
						'action' => 'userprofileviewbyid/'.$id.'/'.$uuid.'/'.$model.'/'.'?iframe=true&width=100%&height=100%',
						'full_base' => true

				),
				array(
					'class' => 'button btn btn-primary btn-sm',
					'style' => '', 
					'rel'=>"prettyPhoto[iframe]details{$id}",
					'escape'=>false,
					'title' => __(" Profile ")
				)			);
		$ActionUrl .= "";
		return $ActionUrl;
	}

	public function jobcartView($title, $id, $uuid, $model){
		$Html = new HtmlHelper(new View(null));
		$ActionUrl = "";
		$title = __($title);
		$tag = $Html->tag(
				'span', '',
				array('class' => 'glyphicon glyphicon-zoom-in')
			). $title;
		$class = "label label-info";
		$ActionUrl .= $Html->link(
				$tag,
				array('controller' => 'Payrolls',
						'action' => 'payrolljobcartdetailsbyid/'.$id.'/'.$uuid.'/'.$model.'/'.'?iframe=true&width=100%&height=100%',
						'full_base' => true

				),
				array(
					'class' => 'button btn btn-primary btn-sm',
					'style' => '', 
					'rel'=>"prettyPhoto[iframe]details{$id}",
					'escape'=>false,
					'title' => __(" JOB CARD ")
				)			);
		$ActionUrl .= "";
		return $ActionUrl;
	}


	public function userlifecycleView($title, $id, $uuid, $model){
		$Html = new HtmlHelper(new View(null));
		$ActionUrl = "";
		$title = __($title);
		$tag = $Html->tag(
				'span', '',
				array('class' => 'glyphicon glyphicon-zoom-in')
			). $title;
		$class = "label label-info";
		$ActionUrl .= $Html->link(
				$tag,
				array('controller' => 'Users',
						'action' => 'userlifecycledetailsbyid/'.$id.'/'.$uuid.'/'.$model.'/'.'?iframe=true&width=100%&height=100%',
						'full_base' => true

				),
				array(
					'class' => 'button btn btn-primary btn-sm',
					'style' => '', 
					'rel'=>"prettyPhoto[iframe]details{$id}",
					'escape'=>false,
					'title' => __(" EMPLOYEE LIFE CYCLE ")
				)			);
		$ActionUrl .= "";
		return $ActionUrl;
	}


	public function paysleepView($title, $id, $uuid, $model){
		$Html = new HtmlHelper(new View(null));
		$ActionUrl = "";
		$title = __($title);
		$tag = $Html->tag(
				'span', '',
				array('class' => 'glyphicon glyphicon-zoom-in')
			). $title;
		$class = "label label-info";
		$ActionUrl .= $Html->link(
				$tag,
				array('controller' => 'Payrolls',
					'action' => 'payrollpaysleepdetailsbyid/'.$id.'/'.$uuid.'/'.$model.'/'.'?iframe=true&width=100%&height=100%',
						'full_base' => true

				),
				array(
					'class' => 'button btn btn-primary btn-sm',
					'style' => '', 
					'rel'=>"prettyPhoto[iframe]details{$id}",
					'escape'=>false,
					'title' => __(" PAY SLIP ")
				)			);
		$ActionUrl .= "";
		return $ActionUrl;
	}


	public function overtimeView($title, $id, $uuid, $model){
		$Html = new HtmlHelper(new View(null));
		$ActionUrl = "";
		$title = __($title);
		$tag = $Html->tag(
				'span', '',
				array('class' => 'glyphicon glyphicon-zoom-in')
			). $title;
		$class = "label label-info";
		$ActionUrl .= $Html->link(
				$tag,
				array('controller' => 'Payrolls',
					'action' => 'payrollovertimedetailsbyid/'.$id.'/'.$uuid.'/'.$model.'/'.'?iframe=true&width=100%&height=100%',
						'full_base' => true

				),
				array(
					'class' => 'button btn btn-primary btn-sm',
					'style' => '', 
					'rel'=>"prettyPhoto[iframe]details{$id}",
					'escape'=>false,
					'title' => __(" OVER TIME ")
				)			);
		$ActionUrl .= "";
		return $ActionUrl;
	}

	public function topSheet($title, $id, $uuid, $model){
		$Html = new HtmlHelper(new View(null));
		$ActionUrl = "";
		$title = __($title);
		$tag = $Html->tag(
				'span', '',
				array('class' => 'glyphicon glyphicon-zoom-in')
			). $title;
		$class = "label label-info";
		$ActionUrl .= $Html->link(
				$tag,
				array('controller' => 'Payrolls',
					'action' => 'payrolltopsheetdetailsbyid/'.$id.'/'.$uuid.'/'.$model.'/'.'?iframe=true&width=100%&height=100%',
						'full_base' => true

				),
				array(
					'class' => 'button btn btn-primary btn-sm',
					'style' => '', 
					'rel'=>"prettyPhoto[iframe]details{$id}",
					'escape'=>false,
					'title' => __(" Top Sheet ")
				)			);
		$ActionUrl .= "";
		return $ActionUrl;
	}

	public function countDepartmentUser($title, $id, $uuid, $model){
		$Html = new HtmlHelper(new View(null));
		$ActionUrl = "";
		$title = __($title);
		$tag = $Html->tag(
				'span', '',
				array('class' => '')
			). $title;
		$class = "label label-info";
		$ActionUrl .= $Html->link(
				$tag,
				array('controller' => 'dashboards',
					'action' => 'dashboarddetailsbyid/'.$id.'/'.$uuid.'/'.$model.'/'.'?iframe=true&width=100%&height=100%',
						'full_base' => true

				),
				array(
					'class' => 'button btn btn-primary btn-sm',
					'style' => '', 
					'rel'=>"prettyPhoto[iframe]details{$id}",
					'escape'=>false,
					'title' => __(" Department Details ")
				)			);
		$ActionUrl .= "";
		return $ActionUrl;
	}

	function convert_number_to_words($number) {
		$hyphen      = '';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			100000              => 'Lak',
//			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
		);

		if (!is_numeric($number)) {
			return false;
		}

		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
			);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}

		$string = $fraction = null;

		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}

		switch (true) {
			case $number < 21:
				$string = $dictionary[$number];
			break;
			case $number < 100:
				$tens   = ((int) ($number / 10)) * 10;
				$units  = $number % 10;
				$string = $dictionary[$tens];
				if ($units) {
					$string .= $hyphen . $dictionary[$units];
				}
			break;
			case $number < 1000:
				$hundreds  = $number / 100;
				$remainder = $number % 100;
				$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
				if ($remainder) {
					$string .= $conjunction . $this->convert_number_to_words($remainder);
				}
			break;
			default:
				$baseUnit = pow(1000, floor(log($number, 1000)));
				$numBaseUnits = (int) ($number / $baseUnit);
				$remainder = $number % $baseUnit;
				$string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
				if ($remainder) {
					$string .= $remainder < 100 ? $conjunction : $separator;
					$string .= $this->convert_number_to_words($remainder);
				}
			break;
		}

		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}

		return $string;
	}

}
