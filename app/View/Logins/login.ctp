<?php //echo $this->Session->flash('auth'); ?>
<?php echo $this->Form->create('Logins', array('action' => 'login'));?>

<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
		<div class="well well-lg form-horizontal">

			<div class="well well-sm text-center login-branding">
				<?php echo $this->html->image('http://amanattrading.com/img/logo.png', array('alt'=> 'Amanat Trading Accounting Software')); ?>
			</div>
			
			<h3 class="text-center">USER LOGIN</h3>
			<?php echo $this->Session->flash(); ?>
			<div class="form-group">
				<div class="col-sm-10 col-sm-offset-1 input-group">
					<div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
					<?php echo $this->Form->input('User.username',array('label'=>false,'div'=>false,'class'=>'form-control', 'data-validation-engine'=>'validate[required]', 'placeholder'=>'username' )); ?>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-10 col-sm-offset-1 input-group">
				<div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
					<?php echo $this->Form->input('User.password',array('label'=>false,'div'=>false,'class'=>'form-control', 'data-validation-engine'=>'validate[required]', 'placeholder'=>'password' )); ?>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-10 col-sm-offset-1 text-center">
					<?php echo $this->Form->submit('Login', array('div' => false, 'align' => 'right', 'formnovalidate' => true, 'class' => 'btn btn-primary btn-block')); ?>
					<input type="hidden" name="loginsubmit" />
					<br>
					<p><a href="#">Forgot password?</a></p>
				</div>
			</div>
			<div class="clearfix"></div>

		</div>
		<!-- /.well well-lg -->
		<p class="site-credit text-center">
            &copy; innotech 2010-<?php echo date('Y'); ?>. Powered by: <a href="http://innotech.com.bd/" target="_blank" title="Developed by Innotech">
            	<?php echo $this->html->image('/img/innotech-monogram.png', array('alt'=> 'Logo of innotech', 'width' => '80')); ?>
            </a>
        </p>
	</div>
	<!-- /.col-sm-4 col-sm-offset-3 -->
</div>

<?php 
	echo $this->Form->input("screenwidth", array("type" => "hidden"));
	echo $this->Form->input("screenheight", array("type" => "hidden"));
	echo $this->Form->end(); 
?>
<?php
	echo"
	<script>
		$(document).ready(function() {
			$(\"#LoginsLoginForm\").validationEngine();
			$(\"#LoginsScreenwidth\").val($(window).width());
			$(\"#LoginsScreenheight\").val($(window).height());
		});
	</script>
	";
?>
