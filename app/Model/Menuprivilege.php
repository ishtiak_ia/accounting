<?php
App::uses('AppModel', 'Model');
class Menuprivilege extends AppModel {
	public $name = 'Menuprivilege';
	public $usetables = 'menuprivileges';
	var $belongsTo  = array(
		/*'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'privilegeinsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'privilegeupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'priviligedeleteid'
		),*/
		'Menu' => array(
			'fields' =>array('Menu.*'),
			'className'    => 'Menu',
			'foreignKey'    => 'menu_id'
		)
	);
}

?>