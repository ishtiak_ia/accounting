<?php
App::uses('AppModel', 'Model');
class Group extends AppModel {
	public $name = 'Group';
	public $usetables = 'groups';

	var $virtualFields = array(
		'isPermanent' => 'IF(Group.groupispermanent = 1, "<span class=\"green\">Yes</span>", "<span class=\"red\">No</span>")',
		'isActive' => 'IF(Group.groupisactive = 0, "<span class=\"button btn btn-sm btn-warning\"><span class=\"glyphicon glyphicon-remove-circle\" title=\"Inactive\"></span></span>", IF(Group.groupisactive = 1, "<span class=\"button btn btn-sm btn-success\"><span class=\"glyphicon glyphicon-ok-circle\" title=\"Active\"></span></span>", "<span class=\"button btn btn-sm btn-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span></span>"))'
	);
    
}

?>