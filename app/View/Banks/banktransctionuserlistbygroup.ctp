<?php
	if($GroupId!=999):
		/*if($GroupId==4):
			echo $this->Form->input(
				"Banktransaction.client_id",
				array(
					'type'=>'select',
					"options"=>array($user),
					'empty'=>__('Select Client'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>3,
					'label'=>__('Select Client'),
					'class'=> 'form-control',
					'style'=>''
				)
			);
		elseif($GroupId==5):
			echo $this->Form->input(
				"Banktransaction.salesman_id",
				array(
					'type'=>'select',
					"options"=>array($user),
					'empty'=>__('Select Salesman'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>3,
					'label'=>__('Select Salesman'),
					'class'=> 'form-control',
					'style'=>''
				)
			);
		else:*/
			echo $this->Form->input(
				"Banktransaction.user_id",
				array(
					'type'=>'select',
					"options"=>array($user),
					'empty'=>__('Select Member'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>3,
					'label'=>__('Select Member'),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style'=>'',
					"selected" => $transactionothernote
				)
			);
		//endif;
	else:
		echo $this->Form->input(
			"Banktransaction.banktransactionothernote",
			array(
				'type' => 'text',
				'div' => array('class'=> 'form-group'),
				'label' => __('Description'),
				'tabindex'=>6,
				'placeholder' => __("Description"),
				'class'=> 'form-control',
				'data-validation-engine'=>'validate[required]',
				'style' => '',
				'value' => $transactionothernote
			)
		);
	endif;
	echo"
		<script>
		$(document).ready(function() {
			$(\"#BanktransactionUserId\").change(function(){
				var ClientId = $(this).val();
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."banks/salespersonbyuser',
					data:{ClientId:ClientId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#BanktransactionSalesmanId\").val(result.salesperson_id);
						$(\"#BanksUsertotalamount\").val(result.usertotalamount);
					}
				});
			});
		});
		</script>
	";
?>