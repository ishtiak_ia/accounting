<?php
	echo "<h2>".__('Unit'). $Utilitys->addurl($title=__("Add New Unit"), $this->request['controller'], $page="unit")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
		"Unit.unittype_id",
		array(
			'type'=>'select',
			"options"=>array($unittypes),
			'empty'=>__('Select Unit Type'),
			'div'=>false,
			'tabindex'=>1,
			'label'=>false,
			'class'=> 'form-control',
			'style'=>''
			)
		)
		.$this->Form->input(
			"Unit.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'placeholder' => __("Search Word"),
				'label' => false,
				'tabindex'=>5,
				'class'=> 'form-control'
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"unitsarchloading\">
	";
	echo $Utilitys->paginationcommon();
					echo $Utilitys->tablestart();
						echo $this->Html->tableHeaders(
							array(
								__('SL#'), 
								 
								array(
									__('Unit Name')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Unit Name BN')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Unit Type')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Unit Symbol')  => array('class' => 'highlight sortable')
								), 
								array(
									__('Mon')  => array('class' => 'highlight sortable')
								),
								array(
									__('Kilogram')  => array('class' => 'highlight sortable')
								),
								array(
									__('Gram')  => array('class' => 'highlight sortable')
								),
								array(
									__('Is Active')  => array('class' => 'highlight sortable')
								),
								__('Action')
							)
						);
						$unitrows = array();
						$countUnit = 0-1+$this->Paginator->counter('%start%');
						foreach($units as $Thisunit):
							$countUnit++;
							$unitrows[]= array($countUnit,$Thisunit["Unit"]["unitname"],$Thisunit["Unit"]["unitnamebn"],$Thisunit["Unittype"]["unittypename"]. "/" . $Thisunit["Unittype"]["unittypenamebn"],$Thisunit["Unit"]["unit_symbol"],array(number_format($Thisunit["Unit"]["unitmon"],10),'colspan="1" align="right"'),array(number_format($Thisunit["Unit"]["unitkilogram"],10),'colspan="1" align="right"'),array(number_format($Thisunit["Unit"]["unitgram"],10),'colspan="1" align="right"'),$Utilitys->statusURL($this->request["controller"], 'unit', $Thisunit["Unit"]["id"], $Thisunit["Unit"]["unituuid"], $Thisunit["Unit"]["unitisactive"]),$Utilitys->cusurl($this->request['controller'], 'unit', $Thisunit["Unit"]["id"], $Thisunit["Unit"]["unituuid"]));
						endforeach;
						echo $this->Html->tableCells(
							$unitrows
						);
					echo $Utilitys->tableend();
				echo $Utilitys->paginationcommon();

	echo"
				</div>
		<script>
			$(document).ready(function() {
				$(\"#UnitSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#unitsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."units/unitsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#unitsarchloading\").html(result);
						}
					);
				});
				$(\"#UnitUnittypeId\").change(function(){
					var UnitUnittypeId = $(\"#UnitUnittypeId\").val();
					$(\"#unitsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."units/unitsearchbytext',
						{UnitUnittypeId:UnitUnittypeId},
						function(result) {
							$(\"#unitsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>