<?php
App::uses('AppModel', 'Model');
class Coatype extends AppModel {
	public $name = 'Coatype';
	public $usetables = 'coatypes';
	var $belongsTo  = array(
		'Creator' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'coatypeinsertid'
		),
		'Modifier' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'coatypeupdateid'
		),
		'Deleter' => array(
			'fields' =>array('user_fullname'),
			'className'    => 'User',
			'foreignKey'    => 'coatypedeleteid'
		)
	);
	var $virtualFields = array(
		'coatype_name' => 'CONCAT(Coatype.coatypename, " / ", Coatype.coatypenamebn)',
		'isActive' => 'IF(Coatype.coatypeisactive = 0, "<span class=\"label label-warning\"><span class=\"glyphicon glyphicon-remove\" title=\"Inactive\"></span> INACTIVE</span>", IF(Coatype.coatypeisactive = 1, "<span class=\"label label-success\"><span class=\"glyphicon glyphicon-ok\" title=\"Active\"></span> ACTIVE</span>", "<span class=\"label label-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span> DELETED</span>"))'
	);
	public $validate = array(
		'coatypename' => array(
			'coatypename_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Type Name field is required',
				'last' => true
			)
		),
		'coatypenamebn' => array(
			'coatypenamebn_not_empty' => array(
				'rule' => 'notEmpty',
				'message' => 'This COA Type Name BN field is required',
				'last' => true
			)
		)
	);
}

?>