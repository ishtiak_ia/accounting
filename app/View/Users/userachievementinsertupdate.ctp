<?php
echo $this->Form->create('Users', array('action' => 'userachievementinsertupdateaction', 'type' => 'file'));
	echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$userachievement[0]["Userachievement"]['id']));
	echo $this->Form->input('userachievementuuid', array('type'=>'hidden', 'value'=>@$userachievement[0]["Userachievement"]['userachievementuuid']?$userachievement[0]["Userachievement"]['userachievementuuid']:0, 'class'=>''));
		$options = array(1 => __("Yes"), 0 =>__("No"));
		$attributes = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$userachievement[0]['Userachievement']['userachievementisactive'] ? $userachievement[0]['Userachievement']['userachievementisactive'] : 0),
			'class' => 'form-inline'
		);
		$option_status = array(1 => __("Reward"), 0 => __("Punishment"));
		$attribute_status = array(
			'legend' => false, 
			'label' => true, 
			'value' => (@$userachievement[0]['Userachievement']['userachievementstatus'] ? $userachievement[0]['Userachievement']['userachievementstatus'] : 0),
			'class' => 'form-inline'
		);
	echo"<h3>".__("Add Special Achievement")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-4\">";
			echo $this->Form->input(
				"Userachievement.department_id",
				array(
					'type'=>'select',
					"options"=>array($department_list),
					'empty'=>__('Select Department'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>1,
					'label'=>__('Select Department&nbsp;<span style=\'color:#cc0000\'>*</span>'),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style'=>'',
					"selected" => (@$userachievement[0]['Userachievement']['department_id'] ? $userachievement[0]['Userachievement']['department_id'] : 0)
				)
			);
			echo "<div class=\"form-group\" id=\"userachievementchangediv\">";
			echo $this->Form->input(
				"Userachievement.user_id",
				array(
					'type'=>'select',
					"options"=>array($user_list),
					'empty'=>__('Select Employee'),
					'div'=>array('class'=>'form-group'),
					'tabindex'=>1,
					'label'=>__('Select Employee &nbsp;<span style=\'color:#cc0000\'>*</span>'),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style'=>'',
					"selected" => (@$userachievement[0]['Userachievement']['user_id'] ? $userachievement[0]['Userachievement']['user_id'] : 0)
				)
			);
			echo "</div>";
			echo $this->Form->input(
				"Userachievement.userachievementname",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Achievement Name&nbsp;<span style=\'color:#cc0000\'>*</span>'),
					'tabindex'=>2,
					'placeholder' => __("Achievement Name"),
					'class'=> 'form-control',
					'data-validation-engine'=>'validate[required]',
					'style' => '',
					'value' => @$userachievement[0]['Userachievement']['userachievementname']
				)
			);
			echo"<div class=\"form-group\">";
				echo"<label>Achievement Type</label><br>";
				echo $this->Form->radio(
					"Userachievement.userachievementstatus",
					$option_status,
					$attribute_status
				);
			echo "</div>";
			echo $this->Form->input(
				"Userachievement.userachievementamount",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Amount'),
					'tabindex'=>1,
					'placeholder' => __("Amount"),
					'class'=> 'form-control',
					'style' => '',
					'value' => @$userachievement[0]['Userachievement']['userachievementamount']
				)
			);
			echo $this->Form->input(
				"Userachievement.userachievementdescription",
				array(
					'type' => 'textarea',
					'div' => array('class'=> 'form-group'),
					'label' => __('Desctiption'),
					'tabindex'=>1,
					'placeholder' => __("Desctiption"),
					'class'=> 'form-control',
					'style' => '',
					'value' => @$userachievement[0]['Userachievement']['userachievementdescription']
				)
			);
			echo $this->Form->input(
				"Userachievement.userachievementdate",
				array(
					'type' => 'text',
					'div' => array('class'=> 'form-group'),
					'label' => __('Date &nbsp;<span style=\'color:#cc0000\'>*</span>'),
					'tabindex'=>1,
					'placeholder' => __("Date, Format(yyyy-mm-dd)"),
					'data-validation-engine'=>'validate[required]',
					'class'=> 'form-control',
					'value' => @$userachievement[0]['Userachievement']['userachievementdate']
				)
			);
			echo"<div class=\"form-group\">";
				echo"<label>isActive?</label><br>";
				echo $this->Form->radio(
					'Userachievement.userachievementisactive', 
					$options,
					$attributes
				);
			echo"</div>";
			echo $Utilitys->allformbutton('',$this->request["controller"],$page='userachievement');
		echo"</div>";
	echo"</div>";
echo $this->Form->end();
echo"
<script>
			$(document).ready(function() {
				$(\"#UsersUserachievementinsertupdateactionForm\").validationEngine();
				$(\"#UserachievementUserachievementdate\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd\", showMeridian: false , startView: 2, minView: 2, autoclose: 1
		});
			});
	$(\"#UserachievementDepartmentId\").change(function(){
		var UserachievementDepartmentId = $(this).val();
		$(\"#userachievementchangediv\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
		$.post(
			'".$this->webroot."users/userachievementlistbydepartmentid',
			{UserachievementDepartmentId:UserachievementDepartmentId},
			function(result) {
				$(\"#userachievementchangediv\").html(result);
			}
		);
	});
		</script>
	";
?>