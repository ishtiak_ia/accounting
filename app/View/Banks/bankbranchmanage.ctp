<?php
	echo "<h2>".__('Bank Branch'). $Utilitys->addurl($title=__("Add New Bank Branch"), $this->request["controller"], $page="bankbranch")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Bankbranch.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'tabindex'=>1,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
			)
		);
		$Utilitys->printbutton ($printableid='bankbranchsarchloading', $searchfield=null);
	echo $Utilitys->filtertagend();
	echo"
		<div id=\"bankbranchsarchloading\">
	";
		echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id='printtable');
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('Branch')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Branch BN')  => array('class' => 'highlight sortable')
						), 
						__('Active'), 
						__('Action')
					)
				);
				$bankbranchrows = array();
				$countBankbranch = 0-1+$this->Paginator->counter('%start%');
				foreach($bankbranch as $Thisbankbranch):
					$countBankbranch++;
					$bankbranchrows[]= array($countBankbranch,$Thisbankbranch["Bankbranch"]["bankbranchname"],$Thisbankbranch["Bankbranch"]["bankbranchnamebn"],$Utilitys->statusURL($this->request["controller"], 'Bankbranch', $Thisbankbranch["Bankbranch"]["id"], $Thisbankbranch["Bankbranch"]["bankbranchuuid"],$Thisbankbranch["Bankbranch"]["bankbranchisactive"]),$Utilitys->cusurl($this->request["controller"], 'Bankbranch', $Thisbankbranch["Bankbranch"]["id"], $Thisbankbranch["Bankbranch"]["bankbranchuuid"]));
				endforeach;
				echo $this->Html->tableCells(
					$bankbranchrows
				);
			echo $Utilitys->tableend();
		echo $Utilitys->paginationcommon();
	echo"
		</div>
		<script>
			$(document).ready(function() {
				$(\"#BankbranchSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#bankbranchsarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."banks/bankbranchsearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#bankbranchsarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>
