<?php
	echo "<h2>".__('Product Type'). $Utilitys->addurl($title=__("Add New Product Type"), $this->request['controller'], $page="producttype")."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Producttype.searchtext",
			array(
				'type' => 'text',
				'div' => false,
				'label' => false,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => 'width:100%;'
				)
			);
	echo $Utilitys->filtertagend();
	echo "
		<div id=\"producttypesarchloading\">
	";
	echo $Utilitys->paginationcommon();
	echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Product Type Name')  => array('class' => 'highlight sortable')
					), 
					array(
						__('Product Type Name BN')  => array('class' => 'highlight sortable')
					), 
					__('Active'), 
					__('Action')
				)
			);
			$producttyperows = array();
			$countProducttype = 0-1+$this->Paginator->counter('%start%');
			foreach($producttype as $Thisproducttype):
				$countProducttype++;
				$producttyperows[]= array($countProducttype,$Thisproducttype["Producttype"]["producttypename"],$Thisproducttype["Producttype"]["producttypenamebn"],$Utilitys->statusURL($this->request["controller"], 'producttype', $Thisproducttype["Producttype"]["id"], $Thisproducttype["Producttype"]["producttypeuuid"], $Thisproducttype["Producttype"]["producttypeisactive"]),$Utilitys->cusurl($this->request["controller"], 'producttype', $Thisproducttype["Producttype"]["id"], $Thisproducttype["Producttype"]["producttypeuuid"]));
			endforeach;
			echo $this->Html->tableCells(
				$producttyperows
			);
	echo $Utilitys->tableend();
	echo $Utilitys->paginationcommon();

	echo"
				</td>
			</tr>
		</table>
		<script>
			$(document).ready(function() {
				$(\"#ProducttypeSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#producttypesarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."products/producttypesearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#producttypesarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>