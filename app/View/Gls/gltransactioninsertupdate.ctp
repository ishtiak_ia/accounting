<?PHP
//echo $gltransaction[0]["Gltransaction"]["user"];
/*echo "<pre>";
print_r($journaltransaction);
echo "</pre>";
*/
$debit = $credit = 0;
if(!isset($id))
	$id = '';
$count = 0;
if(!empty($id)){
	if($journaltransaction[0]['Journaltransaction']['transactionamount']>0)
	  $debit = $journaltransaction[0]['Journaltransaction']['transactionamount']; 
	else{
		$credit_array = explode('-', $debit);
		$credit = $credit_array[0];
	}
		
}

if(empty($voucher_no[0]['voucher_no']))
	$voucher_no = "BP-".date('Y-m')."-0001";
else{
	$voucher_no_array = explode("-", $voucher_no[0]['voucher_no']);
	$voucher_no_new_last_dgits = $voucher_no_array[3]+1;
	if(strlen($voucher_no_new_last_dgits)!=4){
		$add_digits = 4 - strlen($voucher_no_new_last_dgits);
		if($add_digits == 3)
			$voucher_no_new_last_dgits = '000'.$voucher_no_new_last_dgits;
		else if($add_digits == 2)
			$voucher_no_new_last_dgits = '00'.$voucher_no_new_last_dgits;
		else if($add_digits == 1)
			$voucher_no_new_last_dgits = '0'.$voucher_no_new_last_dgits;

	}
	$voucher_no = "BP-".date('Y-m')."-$voucher_no_new_last_dgits";

}
	

if(empty($id))
	echo $this->Form->create('Gls', array('action' => 'gltransactioninsertupdateaction', 'type' => 'file'));
else
	echo $this->Form->create('Gls', array('action' => 'gltransactionupdateaction', 'type' => 'file'));

	echo $this->Form->input('Journaltransaction.id', array('type'=>'hidden', 'value'=>@$journaltransaction[0]['Journaltransaction']['id']?$journaltransaction[0]['Journaltransaction']['id']:0, 'class'=>''));
	echo $this->Form->input('Gltransaction.salesman_id', array('type'=>'hidden', 'value'=>@$gltransaction[0]['Gltransaction']['salesman_id']?$gltransaction[0]['Gltransaction']['salesman_id']:0, 'class'=>''));
	$group[999] = "Others";
	$attributes = array(
		'legend' => false, 
		'label' => true, 
		'value' => '', 
		'class' => 'form-inline'
	);
	if(@$gltransaction[0]['User']['id'] == 0 && @$id !=0):
		$gltransaction[0]['User']['group_id']=999;
	endif;
	echo"<h3>".__("Add New Journal Transaction")."</h3>";
	echo"<div class=\"row\">";
		echo"<div class=\"col-md-11\">";
			echo"<div class=\"row\">";
				echo"<div class=\"col-md-4\">";
					echo $this->Form->input(
						"Gltransaction.transactiondate",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Date'),
							'tabindex'=>1,
							'placeholder' => __("Date"),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style' => '',
							'value' => @$journaltransaction[0]["Journaltransaction"]["transactiondate"]
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-4\">";
					echo $this->Form->input(
						"Gltransaction.voucher_no",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Voucher NO.'),
							'tabindex'=>2,
							'placeholder' => '',
							'readonly' => 'readonly',
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style' => '',
							'value' => @$voucher_no
						)
					);
				echo"</div>";
			 echo"</div>";
			echo"</div>";
			echo "<table><tr><td><table id='content'>";
				echo"<tr><td>";
					echo $this->Form->input(
						"Gltransaction.group_id1",
						array(
							'type'=>'select',
							"options"=>$group,
							'empty'=>__('Select Group'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>2,
							'label'=>__('Group'),
							'class'=> 'form-control',
							'onchange'=>'get_member(1);',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							 "selected" => (@$journaltransaction[0]['Journaltransaction']['group_id'] ? $journaltransaction[0]['Journaltransaction']['group_id'] : 0)
						)
					);
				echo"</td>";
				echo"<td id=\"divuserid1\">";
					echo $this->Form->input(
						"Gltransaction.user_id1",
						array(
							'type'=>'select',
							"options"=>array($user),
							'empty'=>__('Select Member'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>3,
							'label'=>__('Member'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$journaltransaction[0]['Journaltransaction']['user_id']?@$journaltransaction[0]['Journaltransaction']['user_id']:0)
						)
					);
					echo"</td>";
					echo"<td id=\"divnoteid1\" style=\"display:none;\">";
					echo $this->Form->input(
						"Gltransaction.gltransactionothernote1",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Description'),
							'tabindex'=>6,
							'placeholder' => __("Description"),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style' => '',
							'value' => @$transactionothernote
						)
					);
					echo "</td>
						  <td>
								".$this->Form->input(
									"Gltransaction.code_id1",
									array(
										'type'=>'select',
										'options'=>array($coa_code),
										'empty'=>__('Select Code'),
										'div'=> array('class'=> 'form-group'),
										'tabindex'=>7,
										'label'=>'Code',
										'class'=> '',
										'style'=>'width:120px',
										'onchange'=>'select_coa(1);',
										'data-validation-engine'=>'validate[required]',
										'selected'=> (@$journaltransaction[0]['Journaltransaction']['coatype_id'] ? $journaltransaction[0]['Journaltransaction']['coatype_id'] : 0)
									)
								)."
							</td>";
					echo "<td>";
					echo $this->Form->input(
						"Gltransaction.coa_id1",
						array(
							'type'=>'select',
							"options"=>array($coa),
							'empty'=>__('Select Chart of Accounts'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>4,
							'label'=>__('Chart of Accounts'),
							'onchange'=>'select_coa_code(1);',
							'class'=> '',
							'data-validation-engine'=>'validate[required]',
							'style'=>'width:150px;',
							"selected" => (@$journaltransaction[0]['Journaltransaction']['coa_id'] ? $journaltransaction[0]['Journaltransaction']['coa_id'] : 0)
						)
					);
				echo"</td>";
				
			/*echo"<div class=\"row\">";
				echo"<div class=\"col-md-4\">";
					echo $this->Form->input(
						"Gltransaction.transactiontype_id",
						array(
							'type'=>'select',
							"options"=>array($_SESSION["ST_BANKDEPOSIT"]=>"Bank Deposit",$_SESSION["ST_BANKPAYMENT"]=>"Bank Withdraw",$_SESSION["ST_CASHDEPOSIT"]=>"Cash Deposit",$_SESSION["ST_CASHPAYMENT"]=>"Cash Withdraw"),
							'empty'=>__('Select Transaction Type'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>7,
							'label'=>__('Select Type'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$gltransaction[0]['Glransaction']['transactiontype_id'] ? $gltransaction[0]['Gltransaction']['transactiontype_id'] : 0)
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-8\">";
				echo"<div class=\"col-md-3\">";
					echo $this->Form->input(
						"paymentrecive",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Payment/Recived'),
							'tabindex'=>8,
							'placeholder' => __("Payment/Recived"),
							'readonly' => 'readonly',
							'class'=> 'form-control',
							'style' => '',
							'value'=>0.00
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-3\">";
					echo $this->Form->input(
						"usertotalamount",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Client/Suppler/User'),
							'tabindex'=>8,
							'placeholder' => __("Client/Suppler/User"),
							'readonly' => 'readonly',
							'class'=> 'form-control',
							'style' => '',
							'value'=>0.00
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-2\">";
					echo $this->Form->input(
						"remainigbalance",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Remainig'),
							'tabindex'=>9,
							'placeholder' => __("Remainig"),
							'readonly' => 'readonly',
							'class'=> 'form-control',
							'style' => ''
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-4\">";
					echo $this->Form->input(
						"userremainigbalance",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Client/Suppler/User Remainig'),
							'tabindex'=>9,
							'placeholder' => __("Client/Suppler/User Remainig"),
							'readonly' => 'readonly',
							'class'=> 'form-control',
							'style' => ''
						)
					);
				echo"</div>";
				echo"</div>";
			echo"</div>";
			echo"<div class=\"row\" id=\"bankrow\">";
				echo"<div class=\"col-md-3\">";
					echo $this->Form->input(
						"Gltransaction.bank_id",
						array(
							'type'=>'select',
							"options"=>array($bank),
							'empty'=>__('Select Bank'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>4,
							'label'=>__('Select Bank'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$gltransaction[0]['Gltransaction']['bank_id'] ? $gltransaction[0]['Gltransaction']['bank_id'] : 0)
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-3\">";
					echo $this->Form->input(
						"Gltransaction.bankaccount_id",
						array(
							'type'=>'select',
							"options"=>array($bankaccount),
							'empty'=>__('Select Account'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>4,
							'label'=>__('Select Account'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$gltransaction[0]['Gltransaction']['bankaccount_id'] ? $gltransaction[0]['Gltransaction']['bankaccount_id'] : 0)
						)
					);
				echo"</div>";
				echo"<div class=\"col-md-3\">";
					echo $this->Form->input(
						"Gltransaction.banktransactionchequebookseries",
						array(
							'type'=>'select',
							"options"=>array($bankchequebook),
							'empty'=>__('Select Series'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>5,
							'label'=>__('Select Series'),
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$gltransaction[0]['Gltransaction']['bankchequebook_id'] ? $banktransaction[0]['Gltransaction']['bankchequebook_id'] : 0)
						)
					);
					echo"<span id=\"GltransactionBankaccountIdspan\"></span>";
				echo"</div>";
				echo"<div class=\"col-md-3\">";
					echo $this->Form->input(
						"Gltransaction.banktransactionchequesequencynumber",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' => __('Cheque Number'),
							'tabindex'=>6,
							'placeholder' => __("Cheque Number"),
							'readonly' => 'readonly',
							'class'=> 'form-control',
							'style' => '',
							"value" => (@$gltransaction[0]['Banktransaction']['gltransaction_chequenumber'] ? $gltransaction[0]['Gltransaction']['gltransaction_chequenumber'] : '')
						)
					);
					echo"<span id=\"GltransactionGltransactionchequebookseriesspan\"></span>";
				echo"</div>";
			echo"</div>";*/
			//echo"<div class=\"row\">";
			if(empty($id)){
				echo"<td>";
				echo $this->Form->input(
					"Gltransaction.particulars1",
					array(
						'type' => 'text',
						'div' => array('class'=> 'form-group'),
						'label' => __('Particulars'),
						'tabindex'=>10,
						'placeholder' => __("Particulars"),
						'class'=> 'form-control',
						'style' => 'width:150px;',
						"value" => (@$journaltransaction[0]['Journaltransaction']['particulars'] ? str_replace("-", "", $journaltransaction[0]['Journaltransaction']['particulars']) : '')
					)
				);
				echo"</td>";
				echo"<td>";
				echo $this->Form->input(
					"Gltransaction.debit1",
					array(
						'type' => 'text',
						'div' => array('class'=> 'form-group'),
						'label' => __('Debit'),
						'tabindex'=>11,
						'placeholder' => __("Amount"),
						'class'=> 'form-control',
						'data-validation-engine'=>'validate[required]',
						'onkeyup' => 'check_debit_credit(1);',
						'style' => '',
						"value" => (@$journaltransaction[0]['Journaltransaction']['transactionamount'] ? str_replace("-", "", $journaltransaction[0]['Journaltransaction']['transactionamount']) : 0)
					)
				);
				echo"</td>";
				echo "<td>".$this->Form->input(
					"Gltransaction.credit1",
					array(
						'type' => 'text',
						'div' => array('class'=> 'form-group'),
						'label' => __('Credit'),
						'tabindex'=>12,
						'placeholder' => __("Amount"),
						'class'=> 'form-control',
						'onkeyup' => 'check_debit_credit(1);',
						'data-validation-engine'=>'validate[required]',
						'style' => '',
						"value" => (@$journaltransaction[0]['Gltransaction']['transactionamount'] ? str_replace("-", "", $journaltransaction[0]['Gltransaction']['transactionamount']) : 0)
					)
				)."</td>";
				echo "<td>
						".$this->Form->input(
							"Gltransaction.cf_code1",
							array(
								'type'=>'text',
								'div'=> array('class'=> 'form-group'),
								'tabindex'=>13,
								'label'=>'CF Code',
								'class'=> 'form-control',
								'style'=>'',
								'value'=> (@$journaltransaction[0]['Journaltransaction']['cf_code'] ? $journaltransaction[0]['Journaltransaction']['cf_code'] : '')
							)
						)."
					</td>";
				echo"</tr>";
			}
			if(!empty($id)){
					$credit_disable = $debit_disable = '';
					foreach ($journaltransaction as $transactions_key => $transactions_arr) {

						  $count++;
	
						  if($transactions_arr['Journaltransaction']['transactionamount']>0){
						 		 $debit = $transactions_arr['Journaltransaction']['transactionamount'];
						 		 $credit = 0;
						 		 $credit_disable = 'disabled';
						 		 $debit_disable = '';


						 }else{
						 	$credit = str_replace('-', '', $transactions_arr['Journaltransaction']['transactionamount']);
						 	$debit =0;
						 	$debit_disable = 'disabled';
						 	$credit_disable = '';

						 }

						 if($count == 1){
						 	$coa_label = 'Select Chart of Accounts';
						 	$debit_label = 'Debit';
						 	$credit_label = 'Credit';

						 }else{
						 	$coa_label = '';
						 	$debit_label = '';
						 	$credit_label = '';

						 }
					echo"<tr><td class=\"col-md-4\">";
					echo $this->Form->input(
						"Gltransaction.coa_id{$count}",
						array(
							'type'=>'select',
							"options"=>array($coa),
							'empty'=>__('Select Chart of Accounts'),
							'div'=>array('class'=>'form-group'),
							'tabindex'=>4,
							'label'=>$coa_label,
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style'=>'',
							"selected" => (@$transactions_arr['Journaltransaction']['coa_id'] ? $transactions_arr['Journaltransaction']['coa_id'] : 0)
						)
					);
				echo"</td>";
				echo"<td class=\"col-md-3\">";
				echo $this->Form->input(
					"Gltransaction.debit{$count}",
					array(
						'type' => 'text',
						'div' => array('class'=> 'form-group'),
						'label' => $debit_label,
						'tabindex'=>10,
						'placeholder' => __("Amount"),
						'class'=> 'form-control',
						'data-validation-engine'=>'validate[required]',
						'onkeyup' => 'check_debit_credit('.$count.');',
						'style' => '',
						"disabled" => $debit_disable,
						"value" => (@$debit)
					)
				);
				echo"</td>";
				echo"<td class=\"col-md-3\">";
				echo $this->Form->input(
					"Gltransaction.credit{$count}",
					array(
						'type' => 'text',
						'div' => array('class'=> 'form-group'),
						'label' => $credit_label,
						'tabindex'=>10,
						'placeholder' => __("Amount"),
						'class'=> 'form-control',
						'onkeyup' => 'check_debit_credit('.$count.');',
						'data-validation-engine'=>'validate[required]',
						'style' => '',
						"disabled" => $credit_disable,
						"value" => (@$credit)
					)
				);
				echo"</td></tr>";
				
			   }
			   

			}
			
			if($count==0)
				$count=1;

			echo "</table>
			         <tr>
			            <td>
			              <textarea rows='3' cols='50' placeholder='Description' name='data[Gltransaction][description]' id='GltransactionDescription' ></textarea>
			         	 
			         	</td>
			         </tr>
					 <tr>
						<td colspan='10'>
							<input type='button' id='more_fields' onclick='add_fields()' value='Add More' />
							<input type='button' id='anc_rem' value='Remove More' />
							<input type='hidden' style='width:48px;' name='totalrow' id='totalrow' value='{$count}' />
						</td>
				</tr>
				</td>
			  </tr>
			  <tr>
			 	<td id='show_message'></td>
			  </tr>
			 ";
			echo "<tr><td id='buttons'>".$Utilitys->allformbutton('',$this->request["controller"],$page='gltransaction',1)."</td></tr></table>";
		echo"</div>";
	echo"</div>";
	echo $this->Form->end();
	/*\"".$journaltransaction[0]['Journaltransaction']['transactionamount']."\"*/
	/*
	 var total_transactions='';
			total_transactions = ".(@$total_transactions).";
			if(total_transactions!=''){
				var coa_id;
				for(var i=2;i<=total_transactions;i++){
				  $(\"#more_fields\").trigger( \"click\" );
				  $(\"#Gltransaction_coa_id\"+\"i\").val();
				}
			}*/
	echo"
	<script>
		$(document).ready(function() {
			var id = '';
			if(calculate())
			  $(\"#buttons\").show();
			else
			  $(\"#buttons\").hide();	

			
			GltransactionUserId();
			$(\"#GlsGltransactioninsertupdateactionForm\").validationEngine();
			var totalRemainig = 0.00;
			var banktransactiontotalamount = 0.00;
			var bankaccount_id = ".(@$banktransaction[0]['Banktransaction']['bankaccount_id'] ? $banktransaction[0]['Banktransaction']['bankaccount_id'] : 0).";
			var ST_BANKDEPOSIT = ".$_SESSION['ST_BANKDEPOSIT'].";
			var ST_BANKPAYMENT = ".$_SESSION['ST_BANKPAYMENT'].";
			banktransactiontotalamount = ".(@$banktransaction[0]['Banktransaction']['transactionamount'] ? $banktransaction[0]['Banktransaction']['transactionamount'] : 0).";

			if(ST_BANKDEPOSIT == ".(@$banktransaction[0]['Banktransaction']['transactiontype_id'] ? $banktransaction[0]['Banktransaction']['transactiontype_id'] : 0)."){
				$(\"#BanktransactionBanktransactionchequesequencynumber\").removeAttr(\"readonly\");
				$(\"#BanktransactionBanktransactionchequebookseries\").attr(\"readonly\",\"readonly\");
				$(\"#BanktransactionBanktransactionchequebookseries\").attr(\"disabled\",\"disabled\");
				$(\"#BanktransactionBanktransactionchequebookseries\").removeAttr(\"data-validation-engine\");
			}else{
				$(\"#BanktransactionBanktransactionchequesequencynumber\").attr(\"readonly\",\"readonly\");
				$(\"#BanktransactionBanktransactionchequebookseries\").removeAttr(\"readonly\");
				$(\"#BanktransactionBanktransactionchequebookseries\").removeAttr(\"disabled\");

				$(\"#BanktransactionBanktransactionchequebookseries\").attr(\"data-validation-engine\",\"validate[required]\");
			}
			if(bankaccount_id!=0){
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."banks/bankchequebookseriesbyaccount',
					data:{BankaccountId:bankaccount_id},
					dataType: \"JSON\",
					success: function(result) {
						//$(\"#BanktransactionBanktransactionchequebookseries\").html(result.bankchequebook_options);
						$(\"#BanksPaymentrecive\").val(result.banktransactiontotalamount-banktransactiontotalamount);
						$(\"#BanksRemainigbalance\").val(result.banktransactiontotalamount);
						//$(\"#BanktransactionBankId\").val(result.bank_id);
						$(\"#BanktransactionBankbranchId\").val(result.bankbranch_id);
						$(\"#BanktransactionBankaccounttypeId\").val(result.bankaccounttype_id);
						$(\"#BanktransactionBankaccountIdspan\").html('');
					}
				});
			}			

			$(\"#GltransactionTransactiondate\").datetimepicker({language: \"bn\",format: \"yyyy-mm-dd HH:mm:ss\", use24hours: true, showMeridian: false, startView: 2, minView: 2, autoclose: 1});

			$(\"#GltransactionGroupId\").change(function(){
				GltransactionGroupId();
			});

			$(\"#GltransactionUserId\").change(function(){
				GltransactionUserId();
			});

			$(\"#BanktransactionUserId\").change(function(){
				var ClientId = $(this).val();
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."banks/salespersonbyuser',
					data:{ClientId:ClientId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#BanktransactionSalesmanId\").val(result.salesperson_id);
						$(\"#BanksUsertotalamount\").val(result.usertotalamount);
					}
				});
			});

			$(\"#BanktransactionBankId\").change(function(){
				var BankaccountId = $(this).val();
				$(\"#BanktransactionBankaccountIdspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."banks/bankchequebookseriesbybank',
					data:{BankaccountId:BankaccountId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#BanktransactionBankaccountId\").html(result.bankchequebook_options);
						$(\"#BanktransactionBankaccountIdspan\").html('');
					}
				});
			});

			$(\"#BanktransactionBankaccountId\").change(function(){
				var BankId = $(\"#BanktransactionBankId\").val();
				var BankaccountId = $(this).val();
				$(\"#BanktransactionBankaccountIdspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
					type: \"POST\",
					url: '".$this->webroot."banks/bankchequebookseriesbyaccount',
					data:{BankId:BankId,BankaccountId:BankaccountId},
					dataType: \"JSON\",
					success: function(result) {
						$(\"#BanktransactionBanktransactionchequebookseries\").html(result.bankchequebook_options);
						$(\"#BanksPaymentrecive\").val(result.banktransactiontotalamount);
						//$(\"#BanktransactionBankId\").val(result.bank_id);
						$(\"#BanktransactionBankbranchId\").val(result.bankbranch_id);
						$(\"#BanktransactionBankaccounttypeId\").val(result.bankaccounttype_id);
						$(\"#BanktransactionBankaccountIdspan\").html('');
					}
				});
			});

			$(\"#BanktransactionBanktransactionchequebookseries\").change(function(){
				var BanktransactionBanktransactionchequesequencynumber = $(this).val();
				$(\"#BanktransactionBanktransactionchequebookseriesspan\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.ajax({
				       	cache: false,
				       	type: 'POST',
					url: '".$this->webroot."banks/bankchequebooksequencybyseries',
					data: {BanktransactionBanktransactionchequesequencynumber:BanktransactionBanktransactionchequesequencynumber},					
					dataType: \"JSON\",
					success: function(result) {
						$(\"#BanktransactionBanktransactionchequesequencynumber\").val(result.banktransaction_options);
						$(\"#BanksId\").val(result.banktransaction_id);
						$(\"#BanksBanktransactionuuid\").val(result.banktransaction_uuid);
						$(\"#BanktransactionBanktransactionchequebookseriesspan\").html('');
					}
				});
			});
			
			$(\"#BanktransactionBanktransactionamount\").keyup(function(){
				var BanktransactionBanktransactionamount = $(this).val();
				var BanksPaymentrecive = $(\"#BanksPaymentrecive\").val();
				if(BanksPaymentrecive == \"\"){
					BanksPaymentrecive = 0.00;
				}
				var BanktransactionBanktransactiontypeId = $(\"#BanktransactionBanktransactiontypeId\").val();
				if(BanktransactionBanktransactiontypeId==ST_BANKDEPOSIT){
					totalRemainig = parseFloat(BanksPaymentrecive) + parseFloat(BanktransactionBanktransactionamount);
					$(\"#BanktransactionBanktransactionchequesequencynumber\").removeAttr(\"readonly\");
				}else if(BanktransactionBanktransactiontypeId==ST_BANKPAYMENT){
					totalRemainig = parseFloat(BanksPaymentrecive) - parseFloat(BanktransactionBanktransactionamount);
				}else{
					alert('".__("Please Select Transaction Type")."');
					totalRemainig = 0.00;
				}
				$(\"#BanksRemainigbalance\").val(totalRemainig);
			});
			$(\"#BanktransactionBanktransactiontypeId\").change(function(){
				var totalRemainig = 0.00;
				var BanktransactionBanktransactionamount = $(\"#BanktransactionBanktransactionamount\").val();
				var BanksPaymentrecive = $(\"#BanksPaymentrecive\").val();
				var BanktransactionBanktransactiontypeId = $(this).val();
				if(BanktransactionBanktransactiontypeId==ST_BANKDEPOSIT){
					totalRemainig = parseFloat(BanksPaymentrecive) + parseFloat(BanktransactionBanktransactionamount);
					$(\"#BanktransactionBanktransactionchequesequencynumber\").removeAttr(\"readonly\");
					$(\"#BanktransactionBanktransactionchequebookseries\").attr(\"readonly\",\"readonly\");
					$(\"#BanktransactionBanktransactionchequebookseries\").attr(\"disabled\",\"disabled\");
					$(\"#BanktransactionBanktransactionchequebookseries\").removeAttr(\"data-validation-engine\");
				}else if(BanktransactionBanktransactiontypeId==ST_BANKPAYMENT){
					totalRemainig = parseFloat(BanksPaymentrecive) - parseFloat(BanktransactionBanktransactionamount);
					$(\"#BanktransactionBanktransactionchequesequencynumber\").attr(\"readonly\",\"readonly\");
					$(\"#BanktransactionBanktransactionchequebookseries\").removeAttr(\"readonly\");
					$(\"#BanktransactionBanktransactionchequebookseries\").removeAttr(\"disabled\");
					$(\"#BanktransactionBanktransactionchequebookseries\").attr(\"data-validation-engine\",\"validate[required]\");
				}else{
					alert('".__("Please Select Transaction Type")."');
					totalRemainig = 0.00;
					$(\"#BanktransactionBanktransactionchequesequencynumber\").attr(\"readonly\",\"readonly\");
					$(\"#BanktransactionBanktransactionchequebookseries\").removeAttr(\"readonly\");
					$(\"#BanktransactionBanktransactionchequebookseries\").removeAttr(\"disabled\");
				}
				$(\"#BanksRemainigbalance\").val(totalRemainig);
			});
		});
		function GltransactionGroupId(room){
			var GroupId = $(\"#GltransactionGroupId\"+room).val();
			var GltransactionGltransactionothernote = '".(@$gltransaction[0]['Gltransaction']['gltransactionothernote'] ? $gltransaction[0]['Gltransaction']['gltransactionothernote'] : '')."';
			var userid=  '".(@$journaltransaction[0]['Journaltransaction']['user_id'] ? $journaltransaction[0]['Journaltransaction']['user_id'] : 0)."';
			if(userid!=0){
				userothernote = userid;
			}else{
				userothernote = GltransactionGltransactionothernote;
			}
			if(GroupId==5){
				$(\"#GltransactionSalesmanId\"+room).attr(\"disabled\", \"disabled\");
			}else{
				$(\"#GltransactionSalesmanId\"+room).removeAttr(\"disabled\");
			}
			if(GroupId==999){
				$(\"#GltransactionGltransactionothernote\"+room).removeAttr(\"disabled\");
				$(\"#GltransactionGltransactionothernote\"+room).removeAttr(\"display\");
				$(\"#divnoteid\"+room).show();
				$(\"#GltransactionUserId\"+room).attr(\"disabled\", \"disabled\");
				$(\"#divuserid\"+room).hide();
			}else{
				$(\"#GltransactionUserId\"+room).removeAttr(\"disabled\");
				$(\"#GltransactionUserId\"+room).removeAttr(\"display\");
				$(\"#divuserid\"+room).show();
				$(\"#GltransactionGltransactionothernote\"+room).attr(\"disabled\", \"disabled\");
				$(\"#divnoteid\"+room).hide();
			}
			$.ajax({
				type: \"POST\",
				url: '".$this->webroot."gls/gltransctionuserlistbygroup',
				data:{GroupId:GroupId,GltransactionGltransactionothernote:GltransactionGltransactionothernote},
				dataType: \"JSON\",
				success: function(result) {
					$(\"#GltransactionUserId\"+room).html(result.user_option);
					$(\"#GltransactionUserId\"+room).val(userid);
				}
			});
		}
		function GltransactionUserId(){
			var ClientId = $(\"#GltransactionUserId\").val();
			$.ajax({
				type: \"POST\",
				url: '".$this->webroot."banks/salespersonbyuser',
				data:{ClientId:ClientId},
				dataType: \"JSON\",
				success: function(result) {
					$(\"#GltransactionSalesmanId\").val(result.salesperson_id);
					$(\"#GlsUsertotalamount\").val(result.usertotalamount);
				}
			});
		}
	</script>
	";

    //code to print coa start
	$print_str = '';
	$print_str .=  "<option value=''>Select Chart of Account</option>";
	foreach ($coa as $key => $value) {
		$print_str .=  "<option value=\"".$key."\">".$value."</option>";
	}
	//code to print coa end

	//code to print group start
	$print_group = '';
	$print_group .=  "<option value=''>Select Group</option>";
	foreach ($group as $groups_key => $groups) {
      $print_group .=  "<option value=\"".$groups_key."\">".$groups."</option>";
    }
    //code to print group end

    //code to print user start
    $print_user = '';
    $print_user .=  "<option value=''>Select Member</option>";
	foreach ($user as $user_key => $userr) {
      $print_user .=  "<option value=\"".$user_key."\">".$userr."</option>";
    }
     //code to print user end

    //code to print coa start
    $print_coa_code = '';
    $print_coa_code .=  "<option value=''>Select Code</option>";
	foreach ($coa_code as $coa_code_key => $code) {
      $print_coa_code .=  "<option value=\"".$coa_code_key."\">".$code."</option>";
    }


       
 ?>
<script  type="text/javascript">
		$(document).ready(function() {
			$("#GltransactionCoaId1").select2();
			$("#GltransactionCodeId1").select2();
		}); 
		var room=0;
		function add_fields() {
			var coa_array = <?php echo json_encode($print_str); ?>;
			var group_array = <?php echo json_encode($print_group); ?>;
			var user_array = <?php echo json_encode($print_user); ?>;
			var coa_code_array = <?php echo json_encode($print_coa_code); ?>;
			room = parseInt(document.getElementById("totalrow").value);	
			room = room+1;
			var debit = <?php echo $debit; ?>;
			$("#content").append('<tr  id="mydivcontent'+room+'" class="form-group"><td><div class="form-group"><select name="data[Gltransaction][group_id'+room+']" id="GltransactionGroupId'+room+'" class="form-control"  onchange="get_member(room)" data-validation-engine="validate[required]"></select></div></td><td id="divnoteid'+room+'" style="display:none;"><div class="form-group"><input name="data[Gltransaction][gltransactionothernote'+room+']" tabindex="6" placeholder="Description" class="form-control" data-validation-engine="validate[required]" style="" type="text" id="GltransactionGltransactionothernote'+room+'"/></div></td><td id="divuserid'+room+'"><div class="form-group"><select name="data[Gltransaction][user_id'+room+']" id="GltransactionUserId'+room+'" class="form-control"  data-validation-engine="validate[required]"></select></div></td><td><div class="form-group"><select name="data[Coaclasssubclass][coatype_code_id'+room+']" id="GltransactionCodeId'+room+'" onchange="select_coa(room);" class="select-and-load" style="width:120px;" data-validation-engine="validate[required]"></select></div></td><td><div class="form-group"><select name="data[Gltransaction][coa_id'+room+']" id="GltransactionCoaId'+room+'" onchange="select_coa_code(room);" class="select-and-load"  style="width:150px;" data-validation-engine="validate[required]"></select></div></td><td><div class="form-group"><input type="text" name="data[Gltransaction][particulars'+room+']" id="GltransactionParticulars'+room+'" class="form-control" style="width:150px;" placeholder="Particulars" /></div></td><td><div class="form-group"><input type="text" name="data[Gltransaction][debit'+room+']" id="GltransactionDebit'+room+'" class="form-control" value="0" onkeyup="check_debit_credit(room); " data-validation-engine="validate[required]"/></div></td><td><div class="form-group"><input type="text" name="data[Gltransaction][credit'+room+']" id="GltransactionCredit'+room+'" class="form-control" value="0"  onkeyup="check_debit_credit(room);" data-validation-engine="validate[required]"/></div></td><td><div class="form-group"><input type="text" name="data[Gltransaction][cf_code'+room+']" id="GltransactionCfCode'+room+'" class="form-control" /></div></td></tr>');

			$("#GltransactionGroupId"+room).html(group_array);
			$("#GltransactionCoaId"+room).html(coa_array);
			$("#GltransactionCoaId"+room).select2();
			$("#GltransactionUserId"+room).html(user_array);
			$("#GltransactionCodeId"+room).html(coa_code_array);
			$("#GltransactionCodeId"+room).select2();
			
			document.getElementById("totalrow").value=room;
		}
		
$("#anc_rem").click(function(){
	if($('#content tr').size()>1){
 		$('#content tr:last-child').remove();
			room = room-1;
			document.getElementById("totalrow").value=room;
 	}else{
 		alert('One row should be present in table');
 	}
 });

 function select_coa(room){
 	var coa_code_id = $("#GltransactionCodeId"+room).val(),coa;
 	$('#GltransactionCoaId'+room+' option[value='+coa_code_id+']').attr('selected', 'selected');
 	coa = $('#GltransactionCoaId'+room+' option:selected').text();
	$('#s2id_GltransactionCoaId'+room+' .select2-chosen' ).text(coa);
 }

 function select_coa_code(room){
 	var coa = $("#GltransactionCoaId"+room).val(),coa;
 	$('#GltransactionCodeId'+room+' option[value='+coa+']').attr('selected', 'selected');
 	coa = $('#GltransactionCodeId'+room+' option:selected').text();
	$('#s2id_GltransactionCodeId'+room+' .select2-chosen' ).text(coa);
}

 function check_debit_credit(room){
 	if($("#GltransactionDebit"+room).val()>0){
 		$("#GltransactionCredit"+room).attr("disabled","disabled");
 		$("#GltransactionCredit"+room).val(0);

 	}else{
 		$("#GltransactionCredit"+room).attr("disabled",false) ;
 		
	}

 	if($("#GltransactionCredit"+room).val()>0){
 		$("#GltransactionDebit"+room).attr("disabled","disabled"); 
        $("#GltransactionDebit"+room).val(0);

 	}else
 		$("#GltransactionDebit"+room).attr("disabled",false); 

    if(calculate()){
    	$("#buttons").show();
    	$("#show_message").text('');

    }else{
    	$("#buttons").hide();
    	$("#show_message").text("The sum of debit and credit have to be equal").css('color', 'red');

    }
 }

 function calculate(){
	var totalrow = $("#totalrow").val();
 	var sum_of_debit=0, sum_of_credit=0;
 	
 	for(var i=1;i<=totalrow;i++){
 		sum_of_debit = parseInt(sum_of_debit) + parseInt($("#GltransactionDebit"+i).val());
 		sum_of_credit = parseInt(sum_of_credit) + parseInt($("#GltransactionCredit"+i).val());
    }
   
    if(sum_of_debit == sum_of_credit && (sum_of_debit != 0 && sum_of_credit!=0))
    	return true;
    else
    	return false;

 }

 function get_member(data){
 	var room = data;
 	GltransactionGroupId(room);
			
 }

$("#journal_save_and_print").click(function(){
 	 $("#gltransactionsave_and_print").val(1);
 });


</script>

