


<?php echo $this->Form->create('journalreports', array('default' => false, 'class'=>' ')); ?>
<fieldset>  
    <legend><h3>General Ledger</h3></legend>
    <table  border="0">
        <table  border="0">
        <tr>
            <td>Chart of Account:</td>
            <td>
                <?php
                 echo $this->Form->input(
									"Gltransaction.code_id",
									array(
										'type'=>'select',
										'options'=>array($coa_code),
										'empty'=>__('Select Code'),
										'div'=> array('class'=> 'form-group'),
										'tabindex'=>7,
										'label'=>'',
										'class'=> '',
										'style'=>'width:250px;',
										'data-validation-engine'=>'validate[required]'
									)
						 );
                ?>
             </td>
             <td>&nbsp;</td>
             <td>
                <?php
			      echo $this->Form->input(
									"Generalledger.coa",
									array(
										'type'=>'select',
										'options'=>array($coa_name),
										'empty'=>__('Select Chart of Account'),
										'div'=> array('class'=> 'form-group'),
										'class'=> '',
										'label'=>'',
										'style'=>'width:250px;',
										'data-validation-engine'=>'validate[required]'
									)
						 );
                ?>
              </td>
            </tr>
            <tr>
            	<td>Date:</td>
            	<td>
            	<?php
            		echo $this->Form->input(
						"Generalledger.fromdate",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' =>'',
							'placeholder' => 'From',
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style' => 'width:250px;'
						)
					);
				?>
			 </td>
			 <td>&nbsp;</td>
			 <td>
			 <?php
      			echo $this->Form->input(
						"Generalledger.todate",
						array(
							'type' => 'text',
							'div' => array('class'=> 'form-group'),
							'label' =>'',
							'placeholder' => 'To',
							'class'=> 'form-control',
							'data-validation-engine'=>'validate[required]',
							'style' => ''
						)
					);
			?>
		    </td>
        </tr>
        <tr>
        	<td>
        		 <?php 
                	echo $this->Form->submit('View', array('div' => array('class'=> 'form-group'), 'align' => 'right', 'formnovalidate' => 'true', 'class'=> '', 'onClick' => 'get_voucher();')); 
                ?>
            </td>
        </tr>
   </table>
</fieldset> 
<br>
<br>
<?php echo $this->form->end(); ?>

<?php
	echo "<div id='print_page' class='right-aligned-texts'>";
    echo $Utilitys->printbutton ($printableid="loadvoucher", $searchfield=null, $pageformat="c3", $orientation="l", $unit=null, $companyname=$_SESSION["User"]["company_name"], $tabletitle="yes", $tablemonth="yes", $tabledepartment="yes", $printsectoin="printtwotable");
    echo "</div>";

?>

<div id="loadvoucher">
  
</div>

<?php
echo"
		<script>
			function get_voucher(){	
					var coa_id = $(\"#GltransactionCodeId\").val();
					var from_date = $(\"#GeneralledgerFromdate\").val();
					var to_date = $(\"#GeneralledgerTodate\").val();
					if(coa_id!='' && from_date!='' && to_date!=''){
						$(\"#print_page\").hide();
						$(\"#loadvoucher\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
						$.post(
							'".$this->webroot."journalreports/generalledgergenerate',
							{coa_id:coa_id,from_date:from_date,to_date:to_date},
							function(result) {
								$(\"#print_page\").show();
								$(\"#loadvoucher\").html(result);
							}
						);

					}else
						alert('Chart of Account, Form date and To date can not be empty');
			}

		</script>
	";
	//$(\"#banksarchloading\").html(result);
?>

<script type="text/javascript">
$(document).ready(function() {
	$("#GltransactionCodeId").select2();
	$("#GeneralledgerCoa").select2();
	$("#print_page").hide();
  	$("#GeneralledgerFromdate").datetimepicker({language: "bn",format: "yyyy-mm-dd", use24hours: true, showMeridian: false, startView: 2, minView: 2, autoclose: 1});	

  	$("#GeneralledgerTodate").datetimepicker({language: "bn",format: "yyyy-mm-dd", use24hours: true, showMeridian: false, startView: 2, minView: 2, autoclose: 1});

  	$("#GltransactionCodeId").change(function(){
		var coa_code = $(this).val(),coa;
        $('#GeneralledgerCoa option[value='+coa_code+']').attr('selected', 'selected');
        coa = $('#GeneralledgerCoa option:selected').text();
        $('#s2id_GeneralledgerCoa .select2-chosen').text(coa);
        
    });

    $("#GeneralledgerCoa").change(function(){
		var coa_name = $(this).val(),coa;
        $('#GltransactionCodeId option[value='+coa_name+']').attr('selected', 'selected');
        coa = $('#GltransactionCodeId option:selected').text();
        $('#s2id_GltransactionCodeId .select2-chosen').text(coa);
        
    });
	
});
</script>
