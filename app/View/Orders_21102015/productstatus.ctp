<?php
//pr($orderdetail);
	echo "<h2>".__('Product Status')."</h2>";
	echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Orderdetail.branch_id",
			array(
				'type'=>'select',
				"options"=>array($branch),
				'empty'=>__('Select Branch'),
				'div'=>array('class'=>'form-group'),
				'tabindex'=>1,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
			)
		)
		.$this->Form->input(
			"Orderdetail.searchtext",
			array(
				'type' => 'text',
				'div'=>array('class'=>'form-group'),
				'label' => false,
				'tabindex'=>2,
				'placeholder' => __("Search Word"),
				'class'=> 'form-control',
				'style' => ''
			)
		);
	echo $Utilitys->filtertagend();
	echo"
		<div id=\"productstatussarchloading\">
	";
		echo $Utilitys->paginationcommon();
			echo $Utilitys->tablestart($class=null, $id=null);
				echo $this->Html->tableHeaders(
					array(
						__('SL#'), 
						array(
							__('Product')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Total Quantity')  => array('class' => 'highlight sortable')
						), 
						array(
							__('Total Amount')  => array('class' => 'highlight sortable')
						)
					)
				);
				$productrows = array();
				$countProductstatus = 0;
				foreach($orderdetail as $Thisorderdetail):
					$countProductstatus++;
					$productrows[]= array($countProductstatus,$Thisorderdetail["Orderdetail"]["orderdetail_productname"],array($Thisorderdetail[0]["TotalQuantity"],'colspan="1" align="right"'),array($Thisorderdetail[0]["TotalAmount"],'colspan="1" align="right"'));
				endforeach;
				echo $this->Html->tableCells(
					$productrows
				);
			echo $Utilitys->tableend();
		echo $Utilitys->paginationcommon();
	echo"
		</div>
		<script>
			$(document).ready(function() {
				$(\"#OrderdetailSearchtext\").keyup(function(){
					var Searchtext = $(this).val();
					$(\"#productstatussarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."orders/productstatussearchbytext',
						{Searchtext:Searchtext},
						function(result) {
							$(\"#productstatussarchloading\").html(result);
						}
					);
				});
				$(\"#OrderdetailBranchId\").change(function(){
					var OrderdetailBranchId = $(\"#OrderdetailBranchId\").val();
					$(\"#productstatussarchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."orders/productstatussearchbytext',
						{OrderdetailBranchId:OrderdetailBranchId},
						function(result) {
							$(\"#productstatussarchloading\").html(result);
						}
					);
				});
			});
		</script>
	";
?>