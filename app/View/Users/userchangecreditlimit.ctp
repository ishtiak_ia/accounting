<h2>Change Credit Limit</h2>
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('Users', array('action' => 'userchangecreditlimit'));?>
<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>@$users[0]['User']['id'])); ?>
<table class="table table-bordered table-hover">
	<tr>
		<td>User Name</td>
		<td><?php echo $this->Form->input(
			"User.id",
			array(
				'type'=>'select',
				"options"=>array($users),
				'empty'=>__('Select User'),
				'div'=>false,
				'tabindex'=>1,
				'label'=>false,
				'class'=> 'form-control',
				'style'=>''
				)
			) ?>
			<?php //echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$userId)); ?>
		</td>
	</tr>
	<tr>
		<td>Credit Limit</td>
		<td>
			<div id="usercreditlimitdiv">
			<?php echo $this->Form->input(
				"User.usercreditlimit",
				array(
					'type'=>'text',
					'div'=>false,
					'label'=>false,
					'value' =>@$user_creditlimit['User']['usercreditlimit']
				)
			);
			?>
			</div>
		</td>
	</tr>
	<tr>
		<td>New Credit Limit</td>
		<td><?php echo $this->Form->input("User.repassword",array('type'=>'password','div'=>false,'label'=>false));?>
			<input type="hidden" name="changecreditlimitsubmit" />
		</td>
	</tr>
	<tr>
	    <td></td>
	    <td><?php echo $this->Form->submit('Change', array('div' => false,'align'=>'right','formnovalidate'=>true));?></td>
	</tr>
</table>
<?php echo $this->Form->end(); ?>
<?php 
echo "
<script>
	jQuery(document).ready(function() {
		jQuery(\"#UserId\").change(function(){
			var User_id = $(this).val();
			$(\"#usercreditlimitdiv\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
				$.post(
					'".$this->webroot."users/usercreditlimitbyuser',
					{User_id:User_id},
					function(result) {
						$(\"#usercreditlimitdiv\").html(result);
					}
				);
		});
	});	
</script>
";
?>