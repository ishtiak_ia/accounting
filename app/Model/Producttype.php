<?php
App::uses('AppModel', 'Model');
class Producttype extends AppModel {
	public $name = 'Producttype';
	public $usetables = 'producttypes';

	var $virtualFields = array(
		'producttype_name' => 'CONCAT(Producttype.producttypename, " / ", Producttype.producttypenamebn)',
		'isActive' => 'IF(Producttype.producttypeisactive = 0, "<span class=\"button btn btn-sm btn-warning\"><span class=\"glyphicon glyphicon-remove-circle\" title=\"Inactive\"></span></span>", IF(Producttype.producttypeisactive = 1, "<span class=\"button btn btn-sm btn-success\"><span class=\"glyphicon glyphicon-ok-circle\" title=\"Active\"></span></span>", "<span class=\"button btn btn-sm btn-danger\" title=\"Deleted\"><span class=\"glyphicon glyphicon-ban-circle\" title=\"Deleted\"></span></span>"))'
	);
}