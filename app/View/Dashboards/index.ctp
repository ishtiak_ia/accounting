<?php
	//pr($otCountbycompany);
	echo "<div class=\"row\">";
	echo "Welcome to Amanat Trading.";
	echo "</div>";
	echo "<div class=\"row\">";
		echo "<div class=\"col-md-12 text-center\">";
			echo"<div id=\"employeeCompanyDispaly\" style=\"width: auto; height: 500px;\"></div>";
		echo "</div>";
		echo "<div class=\"col-md-6 text-center\">";
			echo"<div id=\"employeeBranchDispaly\" style=\"width: auto; height: 500px;\"></div>";
		echo "</div>";
		echo "<div class=\"col-md-6 text-center\">";
			echo"<div id=\"employeeDepartmentDispaly\" style=\"width: auto; height: 500px;\"></div>";
		echo "</div>";
		echo "<div class=\"col-md-12 text-center\">";
			echo"<div id=\"otCompanyDispaly\" style=\"width: auto; height: 500px;\"></div>";
		echo "</div>";
		echo "<div class=\"col-md-6 text-center\">";
			echo"<div id=\"otBranchDispaly\" style=\"width: auto; height: 500px;\"></div>";
		echo "</div>";
		echo "<div class=\"col-md-6 text-center\">";
			echo"<div id=\"otDepartmentDispaly\" style=\"width: auto; height: 500px;\"></div>";
		echo "</div>";
	echo "</div>";
?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
	// google.load("visualization", "1", {packages:["corechart"]});
	// google.setOnLoadCallback(employeeDepartment);
	// google.setOnLoadCallback(employeeCompany);
	// google.setOnLoadCallback(employeeBranch);
	// google.setOnLoadCallback(otDepartment);
	// google.setOnLoadCallback(otCompany);
	// google.setOnLoadCallback(otBranch);
	function employeeCompany() {
		var data = google.visualization.arrayToDataTable([
			<?php
				echo"['Company','Total Employee', 'tooltip']";
				foreach($employeeCountbycompany as $ThisemployeeCountbycompany):
					echo",['".$ThisemployeeCountbycompany["User"]["company_name"]."',".$ThisemployeeCountbycompany[0]["Totalemployee"].",'".$ThisemployeeCountbycompany[0]["Totalemployee"]." test']";
				endforeach;
			?>
		]);

		var options = {
			title: 'Total Employee By Company ',
			is3D: true,
		};

		var chart = new google.visualization.PieChart(document.getElementById('employeeCompanyDispaly'));
		chart.draw(data, options);
	}
	function employeeBranch() {
		var data = google.visualization.arrayToDataTable([
			<?php
				echo"['Company','Total Employee']";
				foreach($employeeCountbybranch as $ThisemployeeCountbybranch):
					echo",['".$ThisemployeeCountbybranch["User"]["branch_name"]."',".$ThisemployeeCountbybranch[0]["Totalemployee"]."]";
				endforeach;
			?>
		]);

		var options = {
			title: 'Total Employee By Branch ',
			is3D: true,
		};

		var chart = new google.visualization.PieChart(document.getElementById('employeeBranchDispaly'));
		chart.draw(data, options);
	}
	function employeeDepartment() {
		var data = google.visualization.arrayToDataTable([
			<?php
				echo"['Department','Total Employee']";
				foreach($employeeCountbydepartment as $ThisemployeeCountbydepartment):
					echo",['".$ThisemployeeCountbydepartment["User"]["department_name"]."',".$ThisemployeeCountbydepartment[0]["Totalemployee"]."]";
				endforeach;
			?>
		]);

		var options = {
			title: 'Total Employee By Department ',
			is3D: true,
		};

		var chart = new google.visualization.PieChart(document.getElementById('employeeDepartmentDispaly'));
		chart.draw(data, options);
	}
	function otCompany() {
		var data = google.visualization.arrayToDataTable([
			<?php
				echo"['Company','Total Overtime']";
				foreach($otCountbycompany as $ThisotCountbycompany):
					echo",['".$ThisotCountbycompany["Tbljobcardsummery"]["company_name"]."',".$ThisotCountbycompany[0]["Totalot"]."]";
				endforeach;
			?>
		]);

		var options = {
			title: 'Total Overtime By Company ',
			is3D: true,
		};

		var chart = new google.visualization.PieChart(document.getElementById('otCompanyDispaly'));
		chart.draw(data, options);
	}
	function otBranch() {
		var data = google.visualization.arrayToDataTable([
			<?php
				echo"['Company','Total Overtime']";
				foreach($otCountbybranch as $ThisotCountbybranch):
					echo",['".$ThisotCountbybranch["Tbljobcardsummery"]["branch_name"]."',".$ThisotCountbybranch[0]["Totalot"]."]";
				endforeach;
			?>
		]);

		var options = {
			title: 'Total Overtime By Branch ',
			is3D: true,
		};

		var chart = new google.visualization.PieChart(document.getElementById('otBranchDispaly'));
		chart.draw(data, options);
	}
	function otDepartment() {
		var data = google.visualization.arrayToDataTable([
			<?php
				echo"['Department','Total Overtime']";
				foreach($otCountbydepartment as $ThisotCountbydepartment):
					echo",['".$ThisotCountbydepartment["Tbljobcardsummery"]["Department"]."',".$ThisotCountbydepartment[0]["Totalot"]."]";
				endforeach;
			?>
		]);

		var options = {
			title: 'Total Overtime By Department ',
			is3D: true,
		};

		var chart = new google.visualization.PieChart(document.getElementById('otDepartmentDispaly'));
		chart.draw(data, options);
	}
</script>

