<?php
$debit = $credit ='';
$sum_of_debit = $sum_of_credit = $total_balance = 0;
$opening_balance = 0+$total_debit[0][0]['total_debit']+$total_credit[0][0]['total_credit'];
	echo "<div class='center-aligned-texts'><h1>".$company_info[0]["Company"]["companyname"]."</h1></div>";
    echo "<div class='center-aligned-texts'><b>".$company_info[0]["Company"]["companypresentaddress"]."</b></div>";
    echo "<br><br>";
   echo "<div  class='left-aligned-texts'><b>Date From:</b>".$form_date."<b> To: </b>".$to_date."</div><br>";
   	
	echo $Utilitys->tablestart($class=null, $id=null);
	echo $this->Html->tableHeaders(
		array(
			array(
				__('Code')  => array('class' => 'highlight sortable')
			), 
			array(
				__('Name')  => array('class' => 'highlight sortable')
			),
			array(
				__('Opening Balance Debit')  => array('class' => '')
			),
			array(
				__('Opening Balance Credit')  => array('class' => '')
			),
			array(
				__('Net Change Debit')  => array('class' => '')
			),
			array(
				__('Net Change Credit')  => array('class' => '')
			),
            array(
                __('Closing Balance Debit')  => array('class' => '')
            ),
            array(
                __('Closing Balance Credit')  => array('class' => '')
            )
		)
	);

echo $displaytable;
echo $Utilitys->tableend();
		

?>

<script type="text/javascript">
	$(document).ready(function() {
		var profit_or_loss = <?php echo $profit_or_loss;?>;
		if(profit_or_loss>0)
		  	$("#retained_earning_credit").text(profit_or_loss);
		else{
			profit_or_loss = profit_or_loss.toString();
			$("#retained_earning_debit").text(profit_or_loss.replace("-",""));
			$("#retained_earning_debit").addClass("bold-right-aligned-text")
		}

	});
</script>
