<?php
//print_r($department);
	$TotalLeave=$count_departmentuser-$count_departmentuserpresent;
	echo "<div class=\"row\">";
		echo "<div class=\"col-md-12 text-center font-bold\">";
			echo "Department:".$department["Department"]["departmentname"];
		echo "</div>";
		echo "<div class=\"col-md-12 text-center font-bold\">";
			echo "<div class=\"col-md-4\">";
				echo "Total Employee:".$count_departmentuser;
			echo "</div>";
			echo "<div class=\"col-md-4\">";
				echo "Total Present(today):".$count_departmentuserpresent;
			echo "</div>";
			echo "<div class=\"col-md-4\">";
				echo "Total Leave(today):".$TotalLeave;
			echo "</div>";
		echo "</div>";
		echo "<div class=\"col-md-12 text-center font-bold\">";
			echo "<div class=\"col-md-4\">";
				echo "Total Award(this month):".$count_rewards;
			echo "</div>";
			echo "<div class=\"col-md-4\">";
				echo "Total Grivences(this month):".$count_grievances;
			echo "</div>";
			echo "<div class=\"col-md-4\">";
				echo "Total special achievements(this month):".$count_userachievements;
			echo "</div>";
		echo "</div>";
	echo "</div>";
	//pr($jobcardsummeries);
	//pr($payroll);
/*
	if($DisplayLayout=='ajax'):
	else:
		echo $Utilitys->filtertagstart();
		echo $this->Form->input(
			"Employeeinouttime.formdate",
			array(
				'type' => 'text',
				'div' => array('class'=> 'form-group'),
				'label' => false,
				'tabindex'=>4,
				'placeholder' => __("Form Date"),
				'class'=> 'form-control',
				'style' => ''
			)
		);
$searchfield="";
echo"
        <div class=\"btn-group\">
            <button class=\"btn btn-warning btn-sm dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-bars\"></i> Export Table Data</button>
            <ul class=\"dropdown-menu \" role=\"menu\">
                <li><a href=\"#\" onClick =\"$('#departmenttimesearchloading').tableExport({type:'png',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> <img src='".$this->webroot."js/icons/png.png' width='24px'> PNG</a></li>
                <li><a href=\"#\" onClick =\"$('#departmenttimesearchloading').tableExport({type:'pdf',pdfFontSize:'7',escape:'false',tableName:'".str_replace("_id", "", $searchfield)."'});\"> <img src='".$this->webroot."js/icons/pdf.png' width='24px'> PDF</a></li>
            </ul>
        </div>  
    ";
		echo $Utilitys->filtertagend();
	endif;
	
	echo "
		<div id=\"departmenttimesearchloading\">
	";
	if(count($payroll)>0):
		$first_day_this_month = date('d-m-Y',strtotime($jobcardsummeries["Tbljobcardsummery"]["YearOFYear"].'-'.$jobcardsummeries["Tbljobcardsummery"]["MonthOFYear"].'-01')); 
		$last_day_this_month  = date('t-m-Y',strtotime($jobcardsummeries["Tbljobcardsummery"]["YearOFYear"].'-'.$jobcardsummeries["Tbljobcardsummery"]["MonthOFYear"].'-01'));
		$month_name = date("F", mktime(0, 0, 0, $jobcardsummeries["Tbljobcardsummery"]["MonthOFYear"], 15));
	
		echo "<div class=\"row\">";
			echo "<div class=\"col-md-12\">";
				echo "<div class=\"row\">";
					echo"<div class=\"col-md-12 text-center\"><h2>".__('Jobcard Report')."</h2></div>";
				echo"</div>";
				echo "<div class=\"row\">";
					echo"<div class=\"col-md-12 text-center\"><h4><b>For the month of:</b>&nbsp;<b>".$month_name.",".$jobcardsummeries["Tbljobcardsummery"]["YearOFYear"]."</b>&nbsp;&nbsp;</h4></div>";
				echo"</div>";
				echo "<div class=\"row\">";
					echo "<div class=\"col-md-4\"><b>Emp. ID:</b>&nbsp;".
					$payroll[0]["Tblemployeeinouttime"]["employeeid"]."</div>";
					echo"<div class=\"col-md-4\"><b>Name:</b>&nbsp;".
					$payroll[0]["Tblemployeeinouttime"]["user_fullname"]."</div>";
					echo"<div class=\"col-md-4\"><b>Wages:</b>&nbsp;".
					$jobcardsummeries["Tbljobcardsummery"]["SALARY"]."</div>";

					echo "<div class=\"col-md-4\"><b>Section:</b>&nbsp;".
					$jobcardsummeries["Tbljobcardsummery"]["Department"]."/".$jobcardsummeries["Tbljobcardsummery"]["DepartmentBN"]."</div>";
					echo"<div class=\"col-md-4\"><b>Designation:</b>&nbsp;".
					$jobcardsummeries["Tbljobcardsummery"]["Designation"]."/".$jobcardsummeries["Tbljobcardsummery"]["DesignationBN"]."</div>";
					echo"<div class=\"col-md-4\"><b>Grade:</b>&nbsp;</div>";
				echo "</div>";
			echo "</div>";
		echo "</div>";
	
		echo $Utilitys->paginationcommon();
		echo $Utilitys->tablestart();
			echo $this->Html->tableHeaders(
				array(
					__('SL#'), 
					array(
						__('Date')  => array('class' => 'highlight sortable')
					),
					array(
						__('In')  => array('class' => 'highlight sortable')
					),
					array(
						__('Out')  => array('class' => 'highlight sortable')
					),
					array(
						__('In Sts.')  => array('class' => 'highlight sortable')
					),
					array(
						__('Late')  => array('class' => 'highlight sortable')
					),
					array(
						__('Launch')  => array('class' => 'highlight sortable')
					),
					array(
						__('L. Sts.')  => array('class' => 'highlight sortable')
					),
					array(
						__('M. Sts.')  => array('class' => 'highlight sortable')
					),
					array(
						__('O. Late.')  => array('class' => 'highlight sortable')
					),
					array(
						__('OT.')  => array('class' => 'highlight sortable')
					)
				)
			);
			$payrollrows = array();
			$countPayrollrows = 0-1+$this->Paginator->counter('%start%');
			foreach($payroll as $Thispayroll):
				$countPayrollrows++;
				$DateCalendar=substr($Thispayroll["Tblemployeeinouttime"]["INTIME"], 0,10);
				$Intime=substr($Thispayroll["Tblemployeeinouttime"]["INTIME"], 11,5);
				IF($Intime=='00:00'){
					$ConvertIntime=substr($Thispayroll["Tblemployeeinouttime"]["INTIME"], 11,5);
				}else{
					$ConvertIntime=date_format(date_create($Thispayroll["Tblemployeeinouttime"]["INTIME"]), 'g:i A');
				}

				$Outtime=substr($Thispayroll["Tblemployeeinouttime"]["OUTTIME"], 11,5);
				IF($Outtime=='00:00'){
					$ConvertOuttime=substr($Thispayroll["Tblemployeeinouttime"]["OUTTIME"], 11,5);
				}else{
					$ConvertOuttime=date_format(date_create($Thispayroll["Tblemployeeinouttime"]["OUTTIME"]), 'g:i A');
				}
			
				$payrollrows[]= array(
						array($countPayrollrows,' align="right"'),
						array($DateCalendar,' align="center"'),
						array($ConvertIntime,' align="center"'),
						array($ConvertOuttime,' align="center"'),
						array($Thispayroll["Tblemployeeinouttime"]["INSTATUS"],' align="center"'),
						array($Thispayroll["Tblemployeeinouttime"]["LATETIME"],' align="center"'),
						array($Thispayroll["Tblemployeeinouttime"]["LUNCHLATETIME"],' align="center"'),
						array($Thispayroll["Tblemployeeinouttime"]["LUNCHSTATUS"],' align="center"'),
						array($Thispayroll["Tblemployeeinouttime"]["MISSSTATUS"],' align="center"'),
						array($Thispayroll["Tblemployeeinouttime"]["OUTSTATUS"],' align="center"'),
						array($Thispayroll["Tblemployeeinouttime"]["ACTUALTOTALOTHM"],' align="center"')
				);
			endforeach;
			echo $this->Html->tableCells(
				$payrollrows
			);
			$LateValue=$jobcardsummeries["Tbljobcardsummery"]["TotalLateTimeHours"];
			$LaunchValue=$jobcardsummeries["Tbljobcardsummery"]["TotalLunchLateTimeHours"];
			$OTValue=$jobcardsummeries["Tbljobcardsummery"]["TotalOverTimeHours"];
			echo $this->Html->tableCells(
				array(
					array(
						array("",'colspan="4" align="left"'),
						array("Total",'colspan="1" align="center"'),
						array(str_replace("-", "", $LateValue),'colspan="1" align="center"'),
						array(str_replace("-", "", $LaunchValue),'colspan="1" align="center"'),
						array("",'colspan="3" align="right"'),
						array(str_replace("-", "", $OTValue),'colspan="1" align="center"')
					)
				)
			);
		$TotalLeave=$jobcardsummeries["Tbljobcardsummery"]["WeeklyHoliday"]+$jobcardsummeries["Tbljobcardsummery"]["GovtHoliday"];
		echo $Utilitys->tableend();
		echo $Utilitys->paginationcommon();
		echo "<div class=\"row\">";
			echo "<div class=\"col-md-12\">";
				echo "<div class=\"row\">";
					echo "<div class=\"col-md-6\"><b>Present:</b>&nbsp;".
					$jobcardsummeries["Tbljobcardsummery"]["TotalPresent"]."</div>";
					echo"<div class=\"col-md-6\"><b>Late:</b>&nbsp;".
					$jobcardsummeries["Tbljobcardsummery"]["TotalIntimeLate"]."</div>";

					echo"<div class=\"col-md-6\"><b>Holidays:</b>&nbsp;".
					$TotalLeave."</div>";
					echo "<div class=\"col-md-6\"><b>Launch Late:</b>&nbsp;".
					$jobcardsummeries["Tbljobcardsummery"]["TotalLunchLate"]."</div>";

					echo"<div class=\"col-md-6\"><b>Leave:</b>&nbsp;".
					$jobcardsummeries["Tbljobcardsummery"]["TotalPermissionAbsent"]."</div>";
					echo"<div class=\"col-md-6\"><b>Out Late:</b>&nbsp;";
					echo $jobcardsummeries["Tbljobcardsummery"]["TotalOutTimeLate"]."</div>";

					echo"<div class=\"col-md-6\"><b>Absent:</b>&nbsp;".
					$jobcardsummeries["Tbljobcardsummery"]["TotalWithoutPermissionAbsent"]."</div>";
					echo"<div class=\"col-md-6\"><b>Missing Late:</b>&nbsp;".
					$jobcardsummeries["Tbljobcardsummery"]["TotalMissStatus"]."</div>";
				echo "</div>";
			echo "</div>";
	endif;
	echo "</div>";

	if($DisplayLayout=='ajax'):
	else:
	echo"	<script>
			$(document).ready(function() {
				var startDate = new Date('01/01/1980');
				var FromEndDate = new Date();
				var ToEndDate = new Date();
				ToEndDate.setDate(ToEndDate.getDate()+365);

				$('#EmployeeinouttimeFormdate').datetimepicker({
					format: \"yyyy-mm\",
					language: 'en',
					weekStart: 1,
					todayBtn: 1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					minView: 2,
					forceParse: 0,
					startDate: '01/01/2012',
					endDate: FromEndDate
				}).on('changeDate', function(selected){
					startDate = new Date(selected.date.valueOf());
					startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));

					var EmployeeinouttimeFormdate = $(this).val();
					$(\"#departmenttimesearchloading\").html('<img alt=\"\" src=\"".$this->webroot."img/indicator.gif\" />');
					$.post(
						'".$this->webroot."Payrolls/payrolljobcartdetailsbyid/".$id."',
						{EmployeeinouttimeFormdate:EmployeeinouttimeFormdate,DisplayLayout:'ajax'},
						function(result) {
							$(\"#departmenttimesearchloading\").html(result);
						}
					);
				});

				
			});
		</script>
	";
	endif;
*/
?>
